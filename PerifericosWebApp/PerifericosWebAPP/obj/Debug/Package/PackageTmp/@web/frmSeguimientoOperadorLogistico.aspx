﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_web_frmSeguimientoOperadorLogistico" Codebehind="frmSeguimientoOperadorLogistico.aspx.cs" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True" ></asp:ScriptManager>
    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Aceptar" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlMenuSegOperadorLogistico" runat="server">
        <div class="Botones">
            <asp:ImageButton ID="imgbBuscar" runat="server" 
                ImageUrl="~/Images/BuscarAccion.png" onclick="imgbBuscar_Click" 
                ToolTip="Buscar" />
            <asp:ImageButton ID="imgbNuevo" runat="server" ImageUrl="~/Images/Nuevo.png" 
                onclick="imgbNuevo_Click" ToolTip="Nuevo" />
            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png" 
                onclick="imgbEditar_Click" Visible="False" ToolTip="Editar" />
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlAccionesSegOperadorLogistico" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server" 
                ImageUrl="~/Images/buscar.png" onclick="imgbEncontrar_Click" 
                ToolTip="Ejecutar Busqueda" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png" 
                onclick="imgbGravar_Click" ToolTip="Guardar" />
            <asp:ImageButton ID="imgbCancelar" runat="server" 
                ImageUrl="~/Images/cancelar.png" onclick="imgbCancelar_Click" 
                ToolTip="Cancelar" />
            <asp:ImageButton ID="imgbEliminar" runat="server" 
                ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar" OnClick="imgbEliminar_Click" />
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlSegOperadorLogistico" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto">
            <tr>
                <td>
                    <asp:Label ID="lblDepartamento" runat="server" Text="Departamento"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlDepartamento" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlDepartamento_SelectedIndexChanged">
                    </asp:DropDownList>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblMunicipio" runat="server" Text="Municipio"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlMunicipio" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    
                    <asp:Label ID="lblTaller" runat="server" Text="Taller"></asp:Label>
                    
                </td>
                <td>
                 
                    <asp:DropDownList ID="ddlTaller" runat="server">
                    </asp:DropDownList>
                 
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFecha" runat="server" Text="Fecha del Taller"></asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtFechaTaller" runat="server" ></asp:TextBox>
   <%--                 <asp:ImageButton ID="btnCalS1" runat="server"  ImageUrl="~/images/calendario.gif" ToolTip="Calendario" />
                    <asp:CalendarExtender ID="CalendarExtender3" runat="server" 
                        PopupButtonID="btnCalS1"  Format="dd/MM/yyyy" CssClass="MyCalendar" 
                        TargetControlID="txtFechaTaller"  > </asp:CalendarExtender> 
                    <asp:RegularExpressionValidator ID="REV_Fecha1" runat="server" 
                        ControlToValidate="txtFechaTaller" ErrorMessage="Formato inválido dd/mm/aaaa" 
                        
                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblObjetoTaller" runat="server" Text="Objeto"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtObjeto" runat="server" MaxLength="250"  placeholder="Ingrese el objeto del seguimiento, máximo 250 caracteres" TextMode="MultiLine" 
                        Width="100%"></asp:TextBox>
                      <asp:RegularExpressionValidator runat="server" ID="valInput"
    ControlToValidate="txtObjeto"
    ValidationExpression="^[\s\S]{0,250}$"
    ErrorMessage="Ingrese Máximo 250 Caracteres" ForeColor="Red"
    Display="Dynamic">*Ingrese Máximo 250 Caracteres</asp:RegularExpressionValidator>  
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblResponsable" runat="server" Text="Responsable"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlResponsable" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblNombreOperadorLogistico" runat="server" 
                        Text="Operador Logístico"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtOperadorLogistico" runat="server" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblValorFacturaSinIva" runat="server" 
                        Text="Valor Factura Antes de IVA"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtValorAntesIva" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="REVSinIva" runat="server" 
                        ControlToValidate="txtValorAntesIva" 
                        ErrorMessage="Formato inválido Solo se aceptan números y separador decimal &quot;.&quot;" 
                        ValidationExpression="^[0-9]\d*(\.\d+)?$" Width="50%"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblIva" runat="server" Text="Valor IVA"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtValorIva" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="REVIva" runat="server" 
                        ControlToValidate="txtValorIva" 
                        ErrorMessage="Formato inválido Solo se aceptan números y separador decimal &quot;.&quot;" 
                        ValidationExpression="^[0-9]\d*(\.\d+)?$" Width="50%"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblValorComision" runat="server" Text="Valor Comisión"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtValorComision" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="REVSinIva1" runat="server" 
                        ControlToValidate="txtValorComision" 
                        ErrorMessage="Formato inválido Solo se aceptan números y separador decimal &quot;.&quot;" 
                        ValidationExpression="^[0-9]\d*(\.\d+)?$" Width="50%"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblValorTotal" runat="server" Text="Valor Total"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtValorTotal" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="REVSinIva2" runat="server" 
                        ControlToValidate="txtValorTotal" 
                        ErrorMessage="Formato inválido Solo se aceptan números y separador decimal &quot;.&quot;" 
                        ValidationExpression="^[0-9]\d*(\.\d+)?$" Width="50%"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblValorSaldo" runat="server" Text="Saldo"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtValorSaldo" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="REVSinIva3" runat="server" 
                        ControlToValidate="txtValorSaldo" 
                        ErrorMessage="Formato inválido Solo se aceptan números y separador decimal &quot;.&quot;" 
                        ValidationExpression="^[0-9]\d*(\.\d+)?$" Width="50%"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFactura" runat="server" Text="Factura No"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFactura" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFechaPago" runat="server" Text="FechaPago"></asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtFechaPago" runat="server"></asp:TextBox>
<%--                                        <asp:ImageButton ID="btnCalS2" runat="server" 
                        ImageUrl="~/images/calendario.gif" ToolTip="Calendario" />
                    <asp:CalendarExtender ID="CalendarExtender2" runat="server"  
                        Format="dd/MM/yyyy" PopupButtonID="btnCalS2" TargetControlID="txtFechaPago"  CssClass="MyCalendar" > </asp:CalendarExtender> 

                    <asp:RegularExpressionValidator ID="REV_Fecha2" runat="server" 
                        ControlToValidate="txtFechaPago" 
                        ErrorMessage="Formato inválido dd/mm/aaaa" 
                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>--%>

                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblOrdenPagoSIIF" runat="server" Text="No Orden Pago SIIF"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtOrdenPagoSIIF" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblActa" runat="server" Text="No Acta"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtActa" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFechaActa" runat="server" Text="Fecha de Acta"></asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtFechaActa" runat="server"></asp:TextBox>
<%--                    <asp:ImageButton ID="btnCalS3" runat="server" 
                        ImageUrl="~/images/calendario.gif" ToolTip="Calendario" /> 
                    <asp:CalendarExtender ID="CalendarExtender" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnCalS3" TargetControlID="txtFechaActa"  CssClass="MyCalendar" > </asp:CalendarExtender> 
                    <asp:RegularExpressionValidator ID="REV_Fecha3" runat="server" 
                        ControlToValidate="txtFechaActa" 
                        ErrorMessage="Formato inválido dd/mm/aaaa" 
                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>--%>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>


      <asp:Panel ID="PnlEventoTaller" runat="server">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvEventoTaller" runat="server" AutoGenerateColumns="False" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
                    onrowcommand="gvEventoTaller_RowCommand" 
                    onpageindexchanging="gvEventoTaller_PageIndexChanging">
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="evetal_Id" HeaderText="evetal_Id" />
                        <asp:BoundField DataField="valtal_Id" 
                            HeaderText="valtal_Id" />
                        <asp:BoundField DataField="nomMunicipio" 
                            HeaderText="Municipio" />
                        <asp:BoundField DataField="tal_Nombre" 
                            HeaderText="Taller" />
                        <asp:BoundField DataField="evetal_Objeto" 
                            HeaderText="Objeto" />
                        <asp:BoundField DataField="Responsable" 
                            HeaderText="Responsable" />
                        <asp:BoundField DataField="valtal_ValorTotal" 
                            HeaderText="Valor Total" />
                        <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                            ImageUrl="~/Images/seleccionar.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="Label1" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>




</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#' + '<%= txtFechaTaller.ClientID %>').datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
            $('#' + '<%= txtFechaActa.ClientID %>').datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
            $('#' + '<%= txtFechaPago.ClientID %>').datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
        });
    </script>
</asp:Content>
