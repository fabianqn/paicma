﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmEncuesta" Codebehind="frmEncuesta.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .contacto input[type='text'], .contacto textarea, .contacto select {
            text-transform: none !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">

    <asp:Panel ID="pnlTitulo" runat="server" Width="90%">
        <div align="left">
            <asp:Label ID="lblTitulo" runat="server" Text="CREAR ENCABEZADO DE ENCUESTA"></asp:Label>
        </div>
    </asp:Panel>



    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto">
                <tr>
                    <td>
                        <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" Text="Aceptar" OnClick="btnOk_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlMenuVariable" runat="server">
        <div class="Botones">
            <%--            <asp:ImageButton ID="imgbBuscar" runat="server" 
                ImageUrl="~/Images/BuscarAccion.png" ToolTip="Buscar" 
                onclick="imgbBuscar_Click" />--%>
            <asp:ImageButton ID="imgbNuevo" runat="server" ImageUrl="~/Images/Nuevo.png"
                ToolTip="Nuevo" OnClick="imgbNuevo_Click" />
            <%--            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png" 
                Visible="False" ToolTip="Editar" onclick="imgbEditar_Click" />--%>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAccionesVariable" runat="server" Visible="False">
        <div class="Botones">
            <%--            <asp:ImageButton ID="imgbEncontrar" runat="server" 
                ImageUrl="~/Images/buscar.png" ToolTip="Ejecutar Busqueda" 
                onclick="imgbEncontrar_Click" />--%>
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png"
                ToolTip="Guardar" OnClick="imgbGravar_Click" />
            <asp:ImageButton ID="imgbCancelar" runat="server"
                ImageUrl="~/Images/cancelar.png" ToolTip="Cancelar"
                OnClick="imgbCancelar_Click" />
            <%--            <asp:ImageButton ID="imgbEliminar" runat="server" 
                ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar" 
                onclick="imgbEliminar_Click" />--%>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlBuscar" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto">
                <tr>
                    <td>ID Victima:<span style='color: white;'>*</span></td>
                    <td style='width: 150px'>
                        <asp:TextBox ID='txtvictim_guid' runat='server' MaxLength='100' Width='100%'></asp:TextBox>
                        <asp:Button ID='btnConsultar' runat='server' Text='Consultar' Width='90px' OnClick="btnConsultar_Click" />

                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </td>
                </tr>

            </table>


            <div class="contacto3" id="lista" runat="server" visible="false">
                 <asp:GridView ID="gridVictimas" runat="server" AutoGenerateColumns="False" style="margin-left: 0px" OnRowCommand="gridVictimas_RowCommand" OnPreRender="gridVictimas_PreRender" CssClass="mGrid">
                            <Columns>
                                <asp:BoundField DataField="ID_IMSMAVictima" HeaderText="ID Victima" />
                                <asp:BoundField DataField="NumeroIdentificacion" HeaderText="Documento" />
                                <asp:BoundField DataField="Nombres" HeaderText="Nombre" />
                                <asp:ButtonField ButtonType="Button" CommandName="seleccionar" Text="Seleccionar" />
                            </Columns>
                        </asp:GridView>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlEncuestaNuevo" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto">
                <tr>
                    <td>
                        <asp:Button ID="btnVolverLista" runat="server" Text="Volver a la Lista"  OnClick="btnVolverLista_Click" />
                    </td>
            
                </tr>
                <tr>
                    <td>Roll:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:TextBox ID='txtroll' runat='server' MaxLength='3' Width='80%'></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                            ControlToValidate="txtroll" ErrorMessage="*Ingrese Valores Numericos del 0 al 255"
                            ForeColor="Red"
                            ValidationExpression="^(([0-1]?[0-9]?[0-9])|([2][0-4][0-9])|(25[0-5]))$">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Cedula:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 150px'>
                        <asp:TextBox ID='txtcedula' runat='server' MaxLength='20' Width='80%'></asp:TextBox>
                        <asp:RegularExpressionValidator ID="regexFortxtNoIdentificacion" runat="server"
                            ControlToValidate="txtcedula" ErrorMessage="*Ingrese Valores Numericos"
                            ForeColor="Red"
                            ValidationExpression="^[0-9]*">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Nombre:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 150px'>
                        <asp:TextBox ID='txtnombre' runat='server' MaxLength='150' Width='80%'></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Edad:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:TextBox ID='txtedad' runat='server' MaxLength='3' Width='80%'></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                            ControlToValidate="txtedad" ErrorMessage="*Ingrese Valores Numericos del 0 al 255"
                            ForeColor="Red"
                            ValidationExpression="^(([0-1]?[0-9]?[0-9])|([2][0-4][0-9])|(25[0-5]))$">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Dia:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <%--<asp:TextBox ID='txtdia' runat='server' MaxLength='20' Width='80%'></asp:TextBox>--%>
                        <select name="day" id='txtdia' size="1" runat="server" style="width: 50%">
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Mes:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <%--<asp:TextBox ID='txtmes' runat='server' MaxLength='20' Width='80%'></asp:TextBox>--%>
                        <select name="month" ID='txtmes' runat='server' size="1">
                            <option value="01">Enero</option>
                            <option value="02">Febrero</option>
                            <option value="03">Marzo</option>
                            <option value="04">Abril</option>
                            <option value="05">Mayo</option>
                            <option value="06">Junio</option>
                            <option value="07">Julio</option>
                            <option value="08">Agosto</option>
                            <option value="09">Septiembre</option>
                            <option value="10">Octubre</option>
                            <option value="11">Noviembre</option>
                            <option value="12">Diciembre</option>
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Anio:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:TextBox ID='txtanio' runat='server' MaxLength='20' Width='80%'></asp:TextBox>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator100" runat="server"
                            ControlToValidate="txtanio" ErrorMessage="*Ingrese un año válido"
                            ForeColor="Red"
                            ValidationExpression="^(19|20)\d{2}$">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td></td>
                </tr>
                <tr>
                     <td>Departamento1:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:DropDownList ID="ddlDepartamento" runat="server" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                   <td>Municipio1:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:DropDownList ID="ddlMunicipio" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Vereda:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 150px'>
                        <asp:TextBox ID='txtvereda' runat='server' MaxLength='150' Width='80%'></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Corregimiento:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 150px'>
                        <asp:TextBox ID='txtcorregimiento' runat='server' MaxLength='150' Width='80%'></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>DireccionVivienda:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 200px'>
                        <asp:TextBox ID='txtdireccionVivienda' runat='server' MaxLength='150' Width='80%'></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Area:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:TextBox ID='txtarea' runat='server' MaxLength='3' Width='80%'></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                            ControlToValidate="txtarea" ErrorMessage="*Ingrese Valores Numericos del 0 al 255"
                            ForeColor="Red"
                            ValidationExpression="^(([0-1]?[0-9]?[0-9])|([2][0-4][0-9])|(25[0-5]))$">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Estrato:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:TextBox ID='txtestrato' runat='server' MaxLength='3' Width='80%'></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                            ControlToValidate="txtestrato" ErrorMessage="*Ingrese Valores Numericos del 0 al 255"
                            ForeColor="Red"
                            ValidationExpression="^(([0-1]?[0-9]?[0-9])|([2][0-4][0-9])|(25[0-5]))$">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Total_hogares:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:TextBox ID='txttotal_hogares' runat='server' MaxLength='3' Width='80%'></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server"
                            ControlToValidate="txttotal_hogares" ErrorMessage="*Ingrese Valores Numericos del 0 al 255"
                            ForeColor="Red"
                            ValidationExpression="^(([0-1]?[0-9]?[0-9])|([2][0-4][0-9])|(25[0-5]))$">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Num_personas_hogar:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:TextBox ID='txtnum_personas_hogar' runat='server' MaxLength='3' Width='80%'></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server"
                            ControlToValidate="txtnum_personas_hogar" ErrorMessage="*Ingrese Valores Numericos del 0 al 255"
                            ForeColor="Red"
                            ValidationExpression="^(([0-1]?[0-9]?[0-9])|([2][0-4][0-9])|(25[0-5]))$">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Num_personas_vivienda:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:TextBox ID='txtnum_personas_vivienda' runat='server' MaxLength='3' Width='80%'></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server"
                            ControlToValidate="txtnum_personas_vivienda" ErrorMessage="*Ingrese Valores Numericos del 0 al 255"
                            ForeColor="Red"
                            ValidationExpression="^(([0-1]?[0-9]?[0-9])|([2][0-4][0-9])|(25[0-5]))$">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 200px'>
                        <asp:TextBox ID='txtemail' runat='server' MaxLength='150' Width='80%'></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server"
                            ControlToValidate="txtemail" ErrorMessage="*Ingrese Email Válido"
                            ForeColor="Red"
                            ValidationExpression="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$">
                        </asp:RegularExpressionValidator>

                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Resultado_final:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:TextBox ID='txtresultado_final' runat='server' MaxLength='10' Width='80%'></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
              

                  <tr>
                   <td>Departamento2:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:DropDownList ID="ddlDepartamento2" runat="server" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlDepartamento2_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                   <td>Municipio2:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:DropDownList ID="ddlMunicipio2" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Pais:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:DropDownList ID="ddlPaises" runat="server">
                        </asp:DropDownList>
                     
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Sexo:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <select id="ddlSexo" runat="server">
                            <option value="1" >Femenino</option>
                            <option value="2" >Masculino</option>
                        </select>

                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Nom_enc:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:TextBox ID='txtnom_enc' runat='server' MaxLength='150' Width='80%'></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Autorizacion:</td>
                    <td style='width: 50px'><span style='color: white;'>*</span></td>
                    <td style='width: 100px'>
                        <asp:TextBox ID='txtautorizacion' runat='server' MaxLength='3' Width='80%'></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                            ControlToValidate="txtautorizacion" ErrorMessage="*Ingrese Valores Numericos del 0 al 255"
                            ForeColor="Red"
                            ValidationExpression="^(([0-1]?[0-9]?[0-9])|([2][0-4][0-9])|(25[0-5]))$">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td></td>
                    <td></td>
                </tr>

            </table>
        </div>
    </asp:Panel>



</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#' + '<%= gridVictimas.ClientID %>').DataTable();
        });
    </script>
</asp:Content>
