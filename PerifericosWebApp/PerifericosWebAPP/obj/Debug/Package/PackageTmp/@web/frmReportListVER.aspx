﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmReportListVER" Codebehind="frmReportListVER.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <div class="contacto2">
         <h1>Listado de Reportes</h1>
        <asp:GridView ID="gridReportesVer" runat="server" CssClass="mGrid" AutoGenerateColumns="False" onrowcommand="gridReportesVer_RowCommand" OnPreRender="gridReportesVer_PreRender">
            <Columns>
                <asp:BoundField DataField="id" HeaderText="Id" />
                <asp:BoundField DataField="nombreReporte" HeaderText="Nombre" />
                <asp:BoundField DataField="fechaCreacion" HeaderText="Fecha Creación" DataFormatString="{0:yyyy-MM-dd HH:mm:ss}"/>
                <asp:BoundField DataField="fechaModificacion" HeaderText="Fecha Modificación" DataFormatString="{0:yyyy-MM-dd HH:mm:ss}" />
                <asp:BoundField DataField="segusu_login" HeaderText="Usuario Modificación"  />
                <asp:ButtonField Text="Ver" CommandName="verReporte" ButtonType="Button" />

            </Columns>
        </asp:GridView>



    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">
     <script type="text/javascript">
        $(document).ready(function () {
            $('#' + '<%= gridReportesVer.ClientID %>').DataTable();
    } );
   </script>
</asp:Content>

