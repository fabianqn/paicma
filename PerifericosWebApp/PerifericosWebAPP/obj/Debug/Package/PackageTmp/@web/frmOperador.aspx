﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_web_frmOperador" Codebehind="frmOperador.aspx.cs" %>


<%--<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .auto-style1 {
            height: 21px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">

    <asp:Panel ID="pnlEliminacion" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto">
                <tr>
                    <td>
                        <asp:Label ID="lblEliminacion" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSi" runat="server" Text="SI" OnClick="btnSi_Click" />
                        <asp:Button ID="btnNo" runat="server" Text="NO" OnClick="btnNo_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto">
                <tr>
                    <td>
                        <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" Text="Aceptar" OnClick="btnOk_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlMenuOperador" runat="server">
        <div class="Botones">
            <asp:ImageButton ID="imgbBuscar" runat="server"
                ImageUrl="~/Images/BuscarAccion.png" OnClick="imgbBuscar_Click"
                ToolTip="Buscar" />
            <asp:ImageButton ID="imgbNuevo" runat="server" ImageUrl="~/Images/Nuevo.png"
                OnClick="imgbNuevo_Click" ToolTip="Nuevo" />
            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png"
                Visible="False" OnClick="imgbEditar_Click" ToolTip="Editar" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAccionesOperador" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server"
                ImageUrl="~/Images/buscar.png" OnClick="imgbEncontrar_Click"
                ToolTip="Ejecutar Consulta" Style="width: 32px" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png"
                OnClick="imgbGravar_Click" ToolTip="Guardar" />
            <asp:ImageButton ID="imgbCancelar" runat="server"
                ImageUrl="~/Images/cancelar.png" OnClick="imgbCancelar_Click"
                ToolTip="Cancelar" />
            <asp:ImageButton ID="imgbEliminar" runat="server"
                ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar"
                OnClick="imgbEliminar_Click" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlOperadorNuevo" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto">
                <tr>
                    <td>

                        <asp:Label ID="lblDocumento" runat="server" Text="Documento"></asp:Label>

                    </td>
                    <td>

                        <asp:FileUpload ID="fuDocumento" runat="server" />

                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblObservaciones" runat="server" Text="Observación"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine"
                            MaxLength="500" Width="90%" Height="50px" placeholder="Ingrese una observacion"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblGrupo" runat="server" Text="Grupo" Visible="False"></asp:Label>
                    </td>
                    <td colspan="2">
                        <asp:DropDownList ID="ddlGrupo" runat="server" Visible="False">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="auto-style1">
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="PnlOperadorGrilla" runat="server">
        <div class="formulario2">
            <table class="contacto2">
                <tr>
                    <td align="center">
                        <asp:GridView ID="gvOperador" runat="server" AutoGenerateColumns="False"
                            Width="100%" CssClass="mGrid" PagerStyle-CssClass="pgr"
                            AlternatingRowStyle-CssClass="alt"
                            OnRowCommand="gvOperador_RowCommand" OnPreRender="gvOperador_PreRender">
                            <AlternatingRowStyle CssClass="alt" />
                            <Columns>
                                <asp:BoundField DataField="geop_Id" HeaderText="geop_Id" />
                                <asp:BoundField DataField="gruOpe_Nombre" HeaderText="Grupo" />
                                <asp:BoundField DataField="NombreDocumento"
                                    HeaderText="Documento" />
                                <asp:BoundField DataField="extDocumento"
                                    HeaderText="Extención" />
                                <asp:BoundField DataField="fecha"
                                    HeaderText="Fecha" dataformatstring="{0:yyyy-MM-dd HH:mm:ss}" htmlencode="false" />
                                <asp:BoundField DataField="usuarioOperador" HeaderText="Usuario" />
                                <asp:BoundField DataField="EstadoRevision"
                                    HeaderText="Estado" />
                                <asp:ButtonField ButtonType="Image" CommandName="Seleccion"
                                    ImageUrl="~/Images/seleccionar.png" />
                            </Columns>
                            <PagerStyle CssClass="pgr" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorGv" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlOperadorGestionNuevo" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto">
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Documento"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtDocumento" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFecha" runat="server" Text="Fecha de Carga"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFechaCarga" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Observación"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtObservacionRevision" runat="server" TextMode="MultiLine"
                            Enabled="False" Height="50px" Width="90%" MaxLength="250" placeholder="Ingrese una observacion, Máximo 250 Caracteres"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:ImageButton ID="imbDocumento" runat="server"
                            ImageUrl="~/Images/buscar.png" OnClick="imbDocumento_Click"
                            ToolTip="Descargar" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblRuta" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="Label3" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlGestionRealizada" runat="server" Visible="False">
        <div class="formulario2">
            <table class="contacto2">
                <tr>
                    <td align="center">
                        <asp:GridView ID="gvActividadesOperador" runat="server" AutoGenerateColumns="False"
                            Width="100%" CssClass="mGrid" PagerStyle-CssClass="pgr" 
                            AlternatingRowStyle-CssClass="alt"
                            OnRowCommand="gvActividadesOperador_RowCommand" OnPreRender="gvActividadesOperador_PreRender"
                            >
                            <AlternatingRowStyle CssClass="alt" />
                            <Columns>
                                <asp:BoundField DataField="seop_Id" HeaderText="seop_Id" />
                                <asp:BoundField DataField="fechaRevision"
                                    HeaderText="Fecha Revisión" dataformatstring="{0:yyyy-MM-dd HH:mm:ss}" htmlencode="false" />
                                <asp:BoundField DataField="AccionRealizada"
                                    HeaderText="Accion" />
                                <asp:BoundField DataField="seop_NombreDocumento"
                                    HeaderText="Documento" />
                                <asp:BoundField DataField="usuarioOperador" HeaderText="Usuario" />
                                <asp:BoundField DataField="EstadoRevision"
                                    HeaderText="Estado" />
                                <asp:BoundField DataField="ObservacionRevision" HeaderText="Observaciones" />
                                <asp:ButtonField ButtonType="Image" CommandName="Ver"
                                    ImageUrl="~/Images/seleccionar.png" HeaderText="Ver" />
                            </Columns>
                            <PagerStyle CssClass="pgr" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorGvActividades" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlBuscar" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto">
                <tr>
                    <td>

                        <asp:Label ID="lblNombreGrupo" runat="server" Text="Grupo"></asp:Label>

                    </td>
                    <td colspan="2">

                        <asp:DropDownList ID="ddlNombreGrupo" runat="server" Visible="False">
                        </asp:DropDownList>

                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblNombreDocumento" runat="server" Text="Nombre Documento"></asp:Label>
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="txtNombreDocumento" runat="server" MaxLength="20"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblNombreEstado" runat="server" Text="Estado"></asp:Label>
                    </td>
                    <td colspan="2">
                        <asp:DropDownList ID="ddlEstado" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFechaFiltro" runat="server" Text="Fecha"></asp:Label>
                    </td>
                    <td>
                        <div>
                            <div>
                                <asp:Label ID="Label4" runat="server" Text="Desde"></asp:Label>
                            </div>
                             <div>
                                <asp:TextBox ID="txtDesde" runat="server" Width="40%"></asp:TextBox>
                            </div>
                        </div>
                    </td>
                    <td>
                         <div>
                            <div>
                                 <asp:Label ID="lblhasta" runat="server" Text="Hasta"></asp:Label>
                            </div>
                             <div>
                                <asp:TextBox ID="txtHasta" runat="server" Width="40%"></asp:TextBox>
                            </div>
                        </div>
                    </td>
                    <td style="text-align: left">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblObservacion" runat="server" Text="Observacion"></asp:Label>
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="txtObservacionDocumento" runat="server" MaxLength="50"
                            TextMode="MultiLine" Width="90%" Height="50px"></asp:TextBox>
                    </td>
                    <td></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">
                        <asp:ScriptManager ID="ScriptManager1" runat="server">
                        </asp:ScriptManager>
                    </td>
                    <td>&nbsp;</td>
                </tr>

            </table>
        </div>
    </asp:Panel>





</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#' + '<%= gvOperador.ClientID %>').DataTable();
            $('#' + '<%= gvActividadesOperador.ClientID %>').DataTable();
            $('#' + '<%= txtDesde.ClientID %>').datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
            $('#' + '<%= txtHasta.ClientID %>').datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
        });
    </script>
</asp:Content>
