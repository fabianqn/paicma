﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_web_frmCargaDinamica" Codebehind="frmCargaDinamica.aspx.cs" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">       
    <script type="text/javascript" language="javascript">
        function ShowPopup() {
            $('#mask').show();
            $('#<%=pnlpopup.ClientID %>').show();
        }
        function HidePopup() {
            $('#mask').hide();
            $('#<%=pnlpopup.ClientID %>').hide();
        }
        $(".btnClose").live('click', function () {
            HidePopup();
        });
    </script>
    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
                <tr>
                    <td>
                        <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server"  Text="Aceptar" onclick="btnOk_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlTitulo" runat="server" Width="90%" >
        <div align="left">
            <asp:Label ID="lblTitulo" runat="server" Text="Carga Dinámica"></asp:Label>
        </div>
    </asp:Panel>



        <asp:Panel ID="pnlAccionesCargaDinamica" runat="server" Visible="False">
            <div class="Botones">
                <asp:ImageButton ID="imgbGuardar" runat="server" ImageUrl="~/Images/guardar.png" onclick="imgbGuardar_Click"  />
                <asp:ImageButton ID="imgbCancelar" runat="server" ImageUrl="~/Images/cancelar.png" onclick="imgbCancelar_Click"  />
            </div>
        </asp:Panel>


            <asp:Panel ID="pnlNuevaCarga" runat="server">
                    <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblOpcion" runat="server" Text="Seleccione la tabla a cargar:"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlOpciones" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlOpciones_SelectedIndexChanged" >
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td></td>
                <td align="left">
                    <asp:FileUpload ID="fuCargarArchivos" runat="server" Visible="False" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">Lotes Cargados</td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="width: 100%; height: 400px; overflow: scroll">
                    <asp:GridView ID="gvLotes" runat="server" AutoGenerateColumns="False" ForeColor="Black" OnRowCommand="gvLotes_RowCommand" PageSize="3">
                        <Columns>
                            <asp:BoundField DataField="fechaCarga" HeaderText="Fecha Carga" />
                            <asp:ButtonField ButtonType="Button" CommandName="BorrarLote" Text="Borrar" />
                            <asp:ButtonField ButtonType="Button" CommandName="DetalleLote" Text="Ver detalle" />
                        </Columns>
                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    </asp:GridView>
                        </div>
                </td>
            </tr>
        </table>
        </div>
            </asp:Panel>

    <div id="mask">
    </div>
      <asp:Panel ID="pnlpopup" runat="server"  BackColor="White" 
             Style="z-index:111;background-color: White; position: absolute; left: 35%; top: 12%; border: outset 2px gray;padding:5px;display:none">
            <%--<table width="100%" style="width: 100%; height: 100%;" cellpadding="0" cellspacing="5">--%>
                <%--<tr>--%>
                    <%--<td colspan="2">--%>
                        <div style="width: 100%; height: 400px; overflow: scroll">
                            <asp:GridView ID="gvDetalleLote" runat="server" AutoGenerateColumns="True" ForeColor="Black" OnRowCommand="gvLotes_RowCommand">
                                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                            <asp:Button ID="ButtonCerrarP" runat="server"  Text="Aceptar" onclick="ButtonCerrarP_Click" />
                        </div>
                    <%--</td>--%>
                <%--</tr>--%>
            <%--</table>--%>
        </asp:Panel>
</asp:Content>

