﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmWFListadoCoordinaciones" Codebehind="frmWFListadoCoordinaciones.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

     <div class="contacto2" id="principal" runat="server">
        <h1>Listado de Coordinaciones</h1>
        <br />
        <asp:Button ID="btnNuevaCoordinacion" runat="server" Text="Nueva Coordinación" OnClick="btnNuevaCoordinacion_Click"  />
        <hr />
        <asp:GridView ID="gvCoordinaciones" CssClass="mGrid" runat="server" AutoGenerateColumns="False" OnRowCommand="gvCoordinaciones_RowCommand" OnPreRender="gvCoordinaciones_PreRender">
            <Columns>
                <asp:BoundField DataField="nombre" HeaderText="Nombre Coordinación" />
                <asp:BoundField DataField="fechaCreacion" HeaderText="Creación" />
                <asp:BoundField DataField="idUnicoCoordinacion" HeaderText="id" />
                <asp:ButtonField ButtonType="Button" Text="Editar" CommandName="EditarCoordinacion"  />
                <%--<asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="~/Images/eliminar.png"
                            CommandName="Delete" OnClientClick="return confirm('¿Estas Seguro que queires eliminar este Plan?');"
                            AlternateText="EliminarPlan" />               
                    </ItemTemplate>
                </asp:TemplateField>  --%>
            </Columns>
        </asp:GridView>
      </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">
</asp:Content>

