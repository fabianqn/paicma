﻿<%@ page title="" language="C#" masterpagefile="~/Plantillas/sisPAICMA.master" autoeventwireup="true" inherits="_dmin_frmGrupoBiblioteca" Codebehind="frmGrupoBiblioteca.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <asp:Panel ID="pnlEliminacion" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
                <tr>
                    <td>
                        <asp:Label ID="lblEliminacion" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSi" runat="server"  Text="SI" OnClick="btnSi_Click" />
                        <asp:Button ID="btnNo" runat="server"  Text="NO" OnClick="btnNo_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
                <tr>
                    <td>
                        <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" Text="Aceptar" OnClick="btnOk_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlMenuGrupoBiblioteca" runat="server">
        <div class="Botones">
            <asp:ImageButton ID="imgbBuscar" runat="server" ImageUrl="~/Images/BuscarAccion.png" ToolTip="Buscar" OnClick="imgbBuscar_Click" />
            <asp:ImageButton ID="imgbNuevo" runat="server"  ImageUrl="~/Images/Nuevo.png" ToolTip="Nuevo" OnClick="imgbNuevo_Click" />
            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png" Visible="False" ToolTip="Editar" OnClick="imgbEditar_Click" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAccionesGrupoBiblioteca" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server" ImageUrl="~/Images/buscar.png" ToolTip="Ejecutar Busqueda" OnClick="imgbEncontrar_Click" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png" ToolTip="Guardar" OnClick="imgbGravar_Click" />
            <asp:ImageButton ID="imgbCancelar" runat="server" ImageUrl="~/Images/cancelar.png" ToolTip="Cancelar" OnClick="imgbCancelar_Click" />
            <asp:ImageButton ID="imgbEliminar" runat="server" ImageUrl="~/Images/eliminar.png" OnClick="imgbEliminar_Click" ToolTip="Eliminar" />
            <asp:ImageButton ID="imgbNuevaCarpeta" runat="server" ImageUrl="~/Images/NuevoCampo.png" ToolTip="Nueva Carpeta" Visible="False" OnClick="imgbNuevaCarpeta_Click" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlGrupoBibliotecaNuevo" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblNombreGrupo" runat="server" Text="Grupo"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNombreGrupo" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDescripcion" runat="server" Text="Descripción"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDescripcion" runat="server" MaxLength="250" TextMode="MultiLine" Height="50px" Width="90%" placeholder="Ingrese la descripción, Máximo 250 caracteres"></asp:TextBox>
                          <asp:RegularExpressionValidator runat="server" ID="valInput"
    ControlToValidate="txtDescripcion"
    ValidationExpression="^[\s\S]{0,250}$"
    ErrorMessage="Ingrese Máximo 250 Caracteres" ForeColor="Red"
    Display="Dynamic">*Ingrese Máximo 250 Caracteres</asp:RegularExpressionValidator>           
                </td>
                
            </tr>
   
            <tr>
                <td>
                    <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlEstado" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="PnlGrupoBibliotecaGrilla" runat="server">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
                <td align="center">
                    <asp:GridView ID="gvGrupoBiblioteca" runat="server" AutoGenerateColumns="False" width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" AllowPaging="True" OnPageIndexChanging="gvGrupoBiblioteca_PageIndexChanging" OnRowCommand="gvGrupoBiblioteca_RowCommand" PageSize="15" >
                        <AlternatingRowStyle CssClass="alt" />
                            <Columns>
                                <asp:BoundField DataField="grubib_Id" HeaderText="grubib_Id" />
                                <asp:BoundField DataField="grubib_Nombre" HeaderText="Biblioteca" />
                                <asp:BoundField DataField="grubib_Descripcion" HeaderText="Descripción" />
                                <asp:BoundField DataField="grubib_FechaCreacion" HeaderText="Fecha de Creación" />
                                <asp:BoundField DataField="est_Descripcion" HeaderText="Estado" />
                                <asp:ButtonField ButtonType="Image" CommandName="Seleccion" ImageUrl="~/Images/seleccionar.png" />
                            </Columns>
                        <PagerStyle CssClass="pgr" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblErrorGv" runat="server"></asp:Label>
                </td>
            </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlCarpeta" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblCarpeta" runat="server" Text="Carpeta"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCarpeta" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblErroCarpeta" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        </div>
    </asp:Panel>

      <asp:Panel ID="pnlBibliotecaCarpetaGrilla" runat="server" Visible="False">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
                <td align="center">
                    <asp:GridView ID="gvBibliotecaCapeta" runat="server" AutoGenerateColumns="False" width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" AllowPaging="True" PageSize="5" >
                        <AlternatingRowStyle CssClass="alt" />
                            <Columns>
                                <asp:BoundField DataField="bibCar_Id" HeaderText="bibCar_Id" />
                                <asp:BoundField DataField="bibCar_Descripcion" HeaderText="Carpeta" />
                                <asp:BoundField DataField="bibCar_FechaCreacion" HeaderText="Fecha Creación" />
                            </Columns>
                        <PagerStyle CssClass="pgr" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </td>
            </tr>
            </table>
        </div>
    </asp:Panel>

</asp:Content>

