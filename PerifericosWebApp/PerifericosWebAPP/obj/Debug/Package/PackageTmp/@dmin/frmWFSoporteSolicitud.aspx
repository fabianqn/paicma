﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmWFSoporteSolicitud" Codebehind="frmWFSoporteSolicitud.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">
 
     <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Aceptar" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <div class="contacto2" id="principal" runat="server">
        <h1>Soportes</h1>
        <hr />
        <asp:GridView ID="gvSoporteSolicitud" runat="server" CssClass="mGrid" AutoGenerateColumns="False" OnRowCommand="gvSoporteSolicitud_RowCommand" OnPreRender="gvSoporteSolicitud_PreRender">
            <Columns>
                <asp:BoundField DataField="idSoporteSol" HeaderText="ID Soporte" />
                <asp:BoundField DataField="nombreArchivo" HeaderText="Nombre" />
                <asp:BoundField DataField="tipoArchivo" HeaderText="Tipo Archivo" />
                <asp:ButtonField ButtonType="Button" Text="Ver" CommandName="VerSoporte" />
                <asp:ButtonField ButtonType="Button" Text="Editar" CommandName="EditarSoporte" />
                <asp:ButtonField ButtonType="Button" Text="Eliminar" CommandName="EliminarSoporte" />
            </Columns>
        </asp:GridView>
         <asp:Label ID="lblRespuestaInterna" runat="server" Visible="false"></asp:Label>
        <hr />
        <table style="width: 100%" id="TablaNuevo" runat="server">
            <tr>
                <td>
                    <h1 id="mensajeCrear" runat="server">Agregar Soporte</h1>
                </td>
                <td>
                    <asp:ImageButton ID="imgNuevoSoporte" runat="server"
                        ImageUrl="~/Images/NuevoCampo.png" ToolTip="Nuevo Soporte"
                        OnClick="imgNuevoSoporte_Click" Visible="true" />
                </td>
            </tr>
             <tr>
                    <td colspan="2">
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
        </table>
        <table style="width: 100%" id="soportesTabla" runat="server" visible="false">
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Nombre"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Descripcion"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDescripcion" runat="server" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Archivo"></asp:Label>
                </td>
                <td>
                    <input type="file" id="archivoSoporte"  name="archivoSoporte" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr />
                    <div class="Botones">
                        <%--<input type="submit" id="btnCargar" value="Cargar" runat="server" />--%>
                        <asp:Button ID="btnGuardarAgregar" runat="server" Text="Guardar y agregar otro soporte" OnClick="btnGuardarAgregar_Click" />
                        <asp:Button ID="btnGuardarCompletar" runat="server" Text="Guardar y Completar" OnClick="btnGuardarCompletar_Click" />
                        <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
                    </div>
                </td>
            </tr>
        </table>
       <asp:Panel ID="panelSoloVer" runat="server" Visible="true">
        <div class="Botones">
            <asp:Button ID="btnCancelarSoloVer" runat="server" Text="Continuar y Finalizar" OnClick="btnCancelar_Click" />
        </div>
    </asp:Panel>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="Server">
     <script type="text/javascript">
         function newDoc() {
             window.location.assign("frmWFSolicitudComision.aspx")
         }
         $(document).ready(function () {
             $('#' + '<%= gvSoporteSolicitud.ClientID %>').DataTable();
         });
   </script>
</asp:Content>

