﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmUsuario" Codebehind="frmUsuario.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            height: 38px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <asp:Panel ID="pnlEliminacion" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblEliminacion" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSi" runat="server"  Text="SI" onclick="btnSi_Click" />
                        <asp:Button ID="btnNo" runat="server"  Text="NO" onclick="btnNo_Click" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" Text="Aceptar" onclick="btnOk_Click" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlMenuUsuario" runat="server">
        <div class="Botones">
            <asp:ImageButton ID="imgbBuscar" runat="server" 
                ImageUrl="~/Images/BuscarAccion.png" ToolTip="Buscar" 
                onclick="imgbBuscar_Click" />
            <asp:ImageButton ID="imgbNuevo" runat="server"  ImageUrl="~/Images/Nuevo.png" 
                ToolTip="Nuevo" onclick="imgbNuevo_Click" />
            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png" 
                Visible="False" ToolTip="Editar" onclick="imgbEditar_Click" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAccionesUsuario" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server" 
                ImageUrl="~/Images/buscar.png" ToolTip="Ejecutar Busqueda" 
                onclick="imgbEncontrar_Click" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png" 
                ToolTip="Guardar" onclick="imgbGravar_Click" />
            <asp:ImageButton ID="imgbCancelar" runat="server" 
                ImageUrl="~/Images/cancelar.png" ToolTip="Cancelar" 
                onclick="imgbCancelar_Click" />
            <asp:ImageButton ID="imgbEliminar" runat="server" 
                ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar" 
                onclick="imgbEliminar_Click" />
        </div>
    </asp:Panel>

            <asp:Panel ID="pnlUsuarioNuevo" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblTipoConexion" runat="server" Text="Tipo de Autenticación"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTipoConexion" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlTipoConexion_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUsuario" runat="server" Text="Usuario"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtUsuario" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblContraseña" runat="server" Text="Contraseña" Visible="False"></asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtPassword" runat="server" MaxLength="20" TextMode="Password" 
                        Visible="False" Width="150px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="lblCorreo" runat="server" Text="Correo"></asp:Label>
                </td>
                <td class="auto-style1">
                    <asp:TextBox ID="txtCorreo" ClientIDMode="Static" runat="server" MaxLength="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                </td>
                <td class="auto-style1">
                    <asp:DropDownList ID="ddlEstado" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="PnlUsuarioGrilla" runat="server">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvUsuario" runat="server" AutoGenerateColumns="False" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
                    onrowcommand="gvUsuario_RowCommand" OnPageIndexChanging="gvUsuario_PageIndexChanging" PageSize="20">
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="segusu_Id" HeaderText="segusu_Id" />
                        <asp:BoundField DataField="segusu_login" 
                            HeaderText="Usuario" />
                        <asp:BoundField DataField="est_Descripcion" 
                            HeaderText="Estado" />
                        <asp:BoundField DataField="segCon_Descripcion" HeaderText="Tipo de Conexión" />
                        <asp:BoundField DataField="segusu_FechaCreacion" 
                            HeaderText="Fecha de Creación" />
                        <asp:BoundField DataField="segusu_Correo" HeaderText="Correo" />
                        <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                            ImageUrl="~/Images/seleccionar.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorGv" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="Server">
    <script type="text/javascript">
        <%--$(document).ready(function () {
            $('#'+'<%=txtCorreo.ClientID %>').blur(function () {
                var regex = new RegExp("^$|^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$");
                var str = $(this).val();
                if (regex.test(str)) {
                    return true;
                } else {
                    $(this).val("");
                    $(this).focus();
                    return false;
                }
            });
        });--%>
    </script>
</asp:Content>