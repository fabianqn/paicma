﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmDropdownCreate" Codebehind="frmDropdownCreate.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">
      <div class="Botones">

    <asp:ImageButton ID="imgbGrabar" runat="server" ImageUrl="~/Images/guardar.png" 
                ToolTip="Guardar" onclick="imgbGrabar_Click"  />
            <asp:ImageButton ID="imgbCancelar" runat="server" ImageUrl="~/Images/cancelar.png" ToolTip="Cancelar" onclick="imgbCancelar_Click" />
          <asp:ImageButton ID="imgbNuevoCampo" runat="server" 
                ImageUrl="~/Images/NuevoCampo.png" ToolTip="Nuevo Campo" 
                onclick="imgbNuevoCampo_Click" Visible="true"  />
    </div>
  <asp:Panel ID="OpcionesDropDown" runat="server" Visible="true">
      <asp:GridView ID="gvOpcionesDisponibles" runat="server" AutoGenerateColumns="False" Visible="false" Width="60%" CssClass="mGrid" PagerStyle-CssClass="pgr"
                            AlternatingRowStyle-CssClass="alt">
          <Columns>
              <asp:BoundField DataField="camValue" HeaderText="Valor" />
              <asp:BoundField DataField="camDesc" HeaderText="Descripcion" />
          </Columns>
      </asp:GridView>
                <table class="contacto" runat="server" ID="Opciones">
                    </table>
      <asp:Label ID="lblError" runat="server" Text="Label" Visible="false"></asp:Label>
</asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">
</asp:Content>

