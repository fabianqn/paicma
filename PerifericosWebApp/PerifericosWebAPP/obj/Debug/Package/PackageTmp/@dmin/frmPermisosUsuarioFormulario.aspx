﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_PermisoUsuarioFormulario" Codebehind="frmPermisosUsuarioFormulario.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <asp:Panel ID="pnlAccionesDinamico" runat="server" Visible="true">
        <div class="Botones">
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png"
                ToolTip="Guardar" OnClick="imgbGravar_Click" />
            <asp:ImageButton ID="imgbCancelar" runat="server" ImageUrl="~/Images/cancelar.png" ToolTip="Cancelar" OnClick="imgbCancelar_Click" />
        </div>

     
    </asp:Panel>


    <div class="contacto2" style="width: 88%; float:left" id="funcion" runat="server">
         <h1>Permisos de Usuarios para la tabla:  <%= Session["NombreTablaCAM"].ToString() %></h1>
       <div style="width:40%; float:left">
           <h2>Disponibles</h2>
           <asp:ListBox Runat="server" ID="ListBoxUsuarios" SelectionMode="Multiple" Height="150px"  Width="100%"></asp:ListBox>
       </div>
       <div style="width:20%; float:left">
           <h2> &nbsp;</h2>
            <div style="width:100%; height: 73px;">
               <asp:Button Runat="server" ID="btnMoveLeft" Text="<<" OnClick="btnMoveLeft_Click" Height="75px" Width="95%" ></asp:Button>
            </div>
            <div style="width:100%;">
			    <asp:Button Runat="server" ID="btnMoveRight" Text=">>" OnClick="btnMoveRight_Click" Height="75px" Width="95%"></asp:Button>
            </div>
       </div>
       <div style="width:40%; float:left">
           <h2>Seleccionados</h2>
           <asp:ListBox Runat="server" ID="ListBoxUsuariosSelecciondos" SelectionMode="Multiple" Height="150px" Width="100%" ></asp:ListBox>
       </div>

   
    </div>
    <div class="contacto2" style="width: 88%; float:left" runat="server" id="mensajes" visible="false">
      <asp:Label ID="LabelMsgCorrecto" runat="server" Text="Registro Exitoso, usuarios permitidos actualizados" Visible="false"></asp:Label>
     <asp:Label ID="LabelMsgError" runat="server" Text="Ocurrio un problema al actualizar los permisos, Intente Nuevamente" Visible="false"></asp:Label>
        <asp:Label ID="LabelMsgBaseDatos" runat="server" Text="La Conexion con la base de datos ha fallado, Intente Nuevamente en unos minutos" Visible="false"></asp:Label>
          <asp:Label ID="LabelMsgUsuarios" runat="server" Text="Seleccione al menos un usuario para otorgarle permisos" Visible="false"></asp:Label>
     <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>
   </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">
</asp:Content>

