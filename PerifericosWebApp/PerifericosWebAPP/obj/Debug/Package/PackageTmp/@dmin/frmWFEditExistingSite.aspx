﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmWFEditExistingSite" Codebehind="frmWFEditExistingSite.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">


    <div class="contacto2">
        <h1>Editando Sitios de Intervención </h1>
        <hr />
        <table style="width:100%">
            <tbody>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Nuevo Departamento"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlistDepartamento" runat="server" OnSelectedIndexChanged="ddlistDepartamento_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                    </td>
                </tr>
                 <tr>
                    <td>
                         <asp:Label ID="Label2" runat="server" Text="Nuevo Municipio"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlistMunicipio" runat="server"></asp:DropDownList>

                    </td>
                </tr>
                <tr>
                    <td>
                         <asp:Label ID="Label3" runat="server" Text="Nueva Ubicación"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtUbicacion" runat="server" TextMode="MultiLine"></asp:TextBox>

                    </td>
                </tr>
                  <tr>
                <td colspan="2">
                    <hr />
                    <div class="Botones">

                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
                                    <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
                    </div>
                </td>

            </tr>


            </tbody>           
        </table>


    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="Server">
    <script type="text/javascript">
        function newDoc() {
            window.location.assign("frmWFSolicitudComision.aspx")
        }
       
   </script>
</asp:Content>

