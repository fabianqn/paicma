﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmPermisos" Codebehind="frmPermisos.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <asp:Panel ID="pnlEliminacion" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblEliminacion" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSi" runat="server"  Text="SI" onclick="btnSi_Click" />
                        <asp:Button ID="btnNo" runat="server"  Text="NO" onclick="btnNo_Click" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" Text="Aceptar" onclick="btnOk_Click" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlMenuPermiso" runat="server">
        <div class="Botones">
            <asp:ImageButton ID="imgbBuscar" runat="server" 
                ImageUrl="~/Images/BuscarAccion.png" ToolTip="Buscar" 
                onclick="imgbBuscar_Click" />
            <asp:ImageButton ID="imgbNuevo" runat="server"  ImageUrl="~/Images/Nuevo.png" 
                ToolTip="Nuevo" onclick="imgbNuevo_Click" Visible="False" />
            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png" 
                Visible="False" ToolTip="Editar" onclick="imgbEditar_Click" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAccionesPermiso" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server" 
                ImageUrl="~/Images/buscar.png" ToolTip="Ejecutar Busqueda" 
                onclick="imgbEncontrar_Click" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png" 
                ToolTip="Guardar" onclick="imgbGravar_Click"/>
            <asp:ImageButton ID="imgbCancelar" runat="server" 
                ImageUrl="~/Images/cancelar.png" ToolTip="Cancelar" 
                onclick="imgbCancelar_Click" />
            <asp:ImageButton ID="imgbNuevoPermiso" runat="server" 
                ImageUrl="~/Images/NuevoCampo.png" onclick="imgbNuevoPermiso_Click" 
                ToolTip="Nuevo Permiso" Visible="False" />
            <asp:ImageButton ID="imgbEliminar" runat="server" 
                ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar" 
                onclick="imgbEliminar_Click" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlPermisoNuevo" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblTipoConexion" runat="server" Text="Tipo de Autenticación"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTipoConexion" runat="server" >
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUsuario" runat="server" Text="Usuario"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtUsuario" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlEstado" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="gvFormularioPermisos" runat="server" AutoGenerateColumns="False" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
                    onrowcommand="gvFormularioPermisos_RowCommand" 
                        onpageindexchanging="gvFormularioPermisos_PageIndexChanging">
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="segusuper_Id" HeaderText="segusuper_Id" />
                        <asp:BoundField DataField="segfor_DescripcionMenu" 
                            HeaderText="Formulario" />
                        <asp:BoundField DataField="Consultar" 
                            HeaderText="Consultar" />
                        <asp:BoundField DataField="Crear" HeaderText="Crear" />
                        <asp:BoundField DataField="Modificar" 
                            HeaderText="Modificar" />
                        <asp:BoundField DataField="Eliminar" HeaderText="Eliminar" />
                        <asp:BoundField DataField="Imprimir" HeaderText="Imprimir" />
                        <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                            ImageUrl="~/Images/seleccionar.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>    
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        </div>
    </asp:Panel>

        <asp:Panel ID="PnlUsuarioGrilla" runat="server">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvUsuario" runat="server" AutoGenerateColumns="False" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
                    onrowcommand="gvUsuario_RowCommand" 
                    onpageindexchanging="gvUsuario_PageIndexChanging">
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="segusu_Id" HeaderText="segusu_Id" />
                        <asp:BoundField DataField="segusu_login" 
                            HeaderText="Usuario" />
                        <asp:BoundField DataField="est_Descripcion" 
                            HeaderText="Estado" />
                        <asp:BoundField DataField="segCon_Descripcion" HeaderText="Tipo de Conexión" />
                        <asp:BoundField DataField="segusu_FechaCreacion" 
                            HeaderText="Fecha de Creación" />
                        <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                            ImageUrl="~/Images/seleccionar.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorGv" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

      <asp:Panel ID="pnlEditarPermisos" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblFormulario" runat="server" Text="Formulario"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFormulario" runat="server" MaxLength="20"></asp:TextBox>
                    <asp:DropDownList ID="ddlFormulario" runat="server" Visible="False">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblConsultar" runat="server" Text="Consultar"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlConsultar" runat="server">
                        <asp:ListItem Value="NO">NO</asp:ListItem>
                        <asp:ListItem>SI</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCrear" runat="server" Text="Crear"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlCrear" runat="server">
                        <asp:ListItem Value="NO">NO</asp:ListItem>
                        <asp:ListItem>SI</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblModificar" runat="server" Text="Modificar"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlModificar" runat="server">
                        <asp:ListItem Value="NO">NO</asp:ListItem>
                        <asp:ListItem>SI</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblEliminar" runat="server" Text="Eliminar"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlEliminar" runat="server">
                        <asp:ListItem Value="NO">NO</asp:ListItem>
                        <asp:ListItem>SI</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblImprimir" runat="server" Text="Imprimir"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlImprimir" runat="server">
                        <asp:ListItem Value="NO">NO</asp:ListItem>
                        <asp:ListItem>SI</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblErrorNuevo" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        </div>
    </asp:Panel>

</asp:Content>

