﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmWFBeneficiarios" Codebehind="frmWFBeneficiarios.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">

     <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Aceptar" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <div class="contacto2" id="principal" runat="server">
        <h1>Legalización de Beneficiarios</h1>
        <hr />
        <asp:GridView ID="gvBeneficiario" CssClass="mGrid" runat="server" AutoGenerateColumns="False" OnRowCommand="gvBeneficiarios_RowCommand" OnPreRender="gvBeneficios_PreRender">
            <Columns>
                <asp:BoundField DataField="nombre" HeaderText="Beneficiario" />
                <asp:BoundField DataField="cantBeneficiada" HeaderText="Cantidad" />
                <asp:BoundField DataField="comentario" HeaderText="comentario" />
                <asp:ButtonField ButtonType="Button" Text="Editar" CommandName="EditarBeneficiario"  />
                <asp:ButtonField ButtonType="Button" Text="Eliminar" CommandName="EliminarBeneficiario" />
                <asp:TemplateField ShowHeader="False">
                   <ItemTemplate>
                      <asp:HiddenField runat="server" ID="HiddenFieldID" Value='<%# Eval("idBeneficioLega") %>'   > </asp:HiddenField>                      
                   </ItemTemplate>
                </asp:TemplateField>  
            </Columns>
            </asp:GridView>
        <div style="width: 100%">
            <hr />
            <table style="width: 100%" id="PlanesDinamicos" runat="server">
                <tr>
                    <td>
                        <h1 id="mensajeCrear" runat="server">Agregar beneficiario</h1>
                    </td>
                    <td>
                        <asp:ImageButton ID="imgbNuevoPlan" runat="server"
                            ImageUrl="~/Images/NuevoCampo.png" ToolTip="Nuevo Campo"
                            OnClick="imgbNuevoPlan_Click" Visible="true" />
                    </td>
                     <td>
                        <asp:ImageButton ID="imgbEliminarPlan" runat="server"
                            ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar Ultimmo Campo"
                            OnClick="imgbEliminarPlan_Click" Visible="true" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                     <hr />

                    </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
            </table>
        </div>

        <asp:Panel ID="panelBotones" runat="server">
        <div class="Botones">
            <asp:Button ID="btnGuardarCont" runat="server" Text="Guardar y Continuar" OnClick="btnGuardarCont_Click" />
            <asp:Button ID="btnGuardarSalir" runat="server" Text="Guardar y Salir" OnClick="btnGuardarSalir_Click" Visible="false"/>
            <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
        </div>
    </asp:Panel>
        <asp:Panel ID="panelSoloVer" runat="server" Visible="false">
        <div class="Botones">
            <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
       <%--     <asp:Button ID="btnCancelarSoloVer" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" />--%>
        </div>
    </asp:Panel>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="Server">
        <script type="text/javascript">
            function newDoc() {
                window.location.assign("frmWFSolicitudComision.aspx")
            }
        $(document).ready(function () {
            $('#' + '<%= gvBeneficiario.ClientID %>').DataTable();
        });
   </script>
</asp:Content>

