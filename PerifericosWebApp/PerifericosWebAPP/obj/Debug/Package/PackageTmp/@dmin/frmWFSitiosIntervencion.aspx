﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmWFSitiosIntervencion" Codebehind="frmWFSitiosIntervencion.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">
    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Aceptar" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <div class="contacto2" id="principal" runat="server">
        <h1>Sitios de Intervención</h1>
        <hr />
        <asp:GridView ID="gvSitiosIntervencion" CssClass="mGrid" runat="server" AutoGenerateColumns="False" OnRowCommand="gvSitiosIntervencion_RowCommand" OnPreRender ="gvSitiosIntervencion_PreRender">
            <Columns>
                <asp:BoundField DataField="idSitiosSol" HeaderText="ID Sitios" />
                <asp:BoundField DataField="Departamento" HeaderText="Departamento" />
                <asp:BoundField DataField="Municipio" HeaderText="Municipio" />
                <asp:BoundField DataField="Ubicacion" HeaderText="Ubicacion" />
                <asp:ButtonField ButtonType="Button" Text="Editar" CommandName="EditarSitio"  />
                <asp:ButtonField ButtonType="Button" Text="Eliminar" CommandName="EliminarSitio" />
            </Columns>
         </asp:GridView>
         <asp:Label ID="lblRespuestaInterna" runat="server" Visible="false"></asp:Label>
        <div style="width: 100%">
            <table style="width: 100%" id="SitiosDinamicos" runat="server">
                <tr>
                    <td>
                        <h1 id="mensajeCrear" runat="server">Agregar Sitios</h1>

                    </td>
                    <td>
                        <asp:ImageButton ID="imgbNuevoSitio" runat="server"
                            ImageUrl="~/Images/NuevoCampo.png" ToolTip="Nuevo Campo"
                            OnClick="imgbNuevoSitio_Click" Visible="true" />
                    </td>
                     <td>
                        <asp:ImageButton ID="imgbEliminarSitio" runat="server"
                            ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar Ultimmo Campo"
                            OnClick="imgbEliminarSitio_Click" Visible="true" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                     <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                    </td>
                </tr>
            </table>
        </div>

        <asp:Panel ID="panelBotones" runat="server">
        <div class="Botones">
            <asp:Button ID="btnGuardarCont" runat="server" Text="Guardar y Continuar" OnClick="btnGuardarCont_Click" />
            <%--<asp:Button ID="btnGuardarSalir" runat="server" Text="Guardar y Salir" OnClick="btnGuardarSalir_Click"/>--%>
       

             <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
        </div>
    </asp:Panel>
          <asp:Panel ID="panelSoloVer" runat="server" Visible="false">
        <div class="Botones">
            <asp:Button ID="btnCancelarSoloVer" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" />
        </div>
    </asp:Panel>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="Server">
    <script type="text/javascript">
        function newDoc() {
            window.location.assign("frmWFSolicitudComision.aspx")
        }
        $(document).ready(function () {
            $('#' + '<%= gvSitiosIntervencion.ClientID %>').DataTable();
        });


        // funcion cuando seleccione  un indicador 
        $(".Departamento").change(function () {
            var valorDep = $(this).val();


            // var ordenDep = $(this).data("orden");
            var id = $(this).attr("id");
            //alert(id)
            var cadenaAnterior = parseInt(id.replace(/[^0-9\.]/g, ''), 10);
            var cadenaPrevia = id.substring(id.length - 1, id.length);
            //alert(cadenaPrevia);
            var numero = parseInt(cadenaAnterior, 10);
            //alert(numero);

            var nombreMunicipioSustituir = id.replace('ddlDepartamento', 'ddlMunicipio');
            //var nombreMunicipioCortar = nombreMunicipioSustituir.replace(cadenaAnterior, '');
            //alert(numero);

            //var idMun = nombreMunicipioSustituir + numero;
            var idMun = nombreMunicipioSustituir;
            var optDep;


            // $("#" + idMun).disabled = true;

            var control = document.getElementById(idMun);
            control.removeAttribute("disabled");

            if (valorDep != "0") {

                $("#" + idMun + " option").each(function (index, val) {
                    var $el = $(this);
                    if ($el.val() == "0") {
                        $el.attr("selected", true);
                    }
                    optDep = $el.data("indicador")
                    if (optDep != valorDep) {
                        if ($(this).is('option') && (!$(this).parent().is('span')))
                            $(this).wrap((navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') ? '<span>' : null).hide();
                    } else {
                        if (navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') {
                            if (this.nodeName.toUpperCase() === 'OPTION') {
                                var span = $(this).parent();
                                var opt = this;
                                if ($(this).parent().is('span')) {
                                    $(opt).show();
                                    $(span).replaceWith(opt);
                                }
                            }
                        } else {
                            $(this).show(); //all other browsers use standard .show()
                        }
                    }
                });
            } else {
                $("#" + idMun).val("0");
                $("#" + idMun + " option").each(function () {
                    if ($(this).is('option') && (!$(this).parent().is('span')))
                        $(this).wrap((navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') ? '<span>' : null).hide();
                });
            }

        });


   </script>
</asp:Content>

