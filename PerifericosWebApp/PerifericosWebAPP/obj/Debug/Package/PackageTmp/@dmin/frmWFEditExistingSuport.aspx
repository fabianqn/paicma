﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmWFEditExistingSuport" Codebehind="frmWFEditExistingSuport.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">
    <div class="contacto2">
        <h1>Editando Soporte</h1> 
        <hr />
        <table style="width: 100%" id="soportesTabla" runat="server">
            <tr>
                <td>
                    &nbsp;<asp:Label ID="Label1" runat="server" Text="Nombre"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Descripcion"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDescripcion" runat="server" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Archivo"></asp:Label>
                </td>
                <td>
                    <input type="file" id="archivoReporte" name="archivoReporte" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr />
                    <div class="Botones">

                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
                                   <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
                    </div>
                </td>

            </tr>
        </table>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">
    <script type="text/javascript">
        function newDoc() {
            window.location.assign("frmWFSolicitudComision.aspx")
        }

   </script>
</asp:Content>

