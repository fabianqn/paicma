﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmReportList" Codebehind="frmReportList.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <div class="contacto2">
        <div class="Botones">
            <asp:ImageButton ID="imgbNuevo" runat="server" ImageUrl="~/Images/Nuevo.png"
                ToolTip="Nuevo" OnClick="imgbNuevo_Click" Style="height: 32px" />
        </div>
         <h1>Listado de Reportes</h1>
        <asp:GridView ID="gridReportes" runat="server" CssClass="mGrid" AutoGenerateColumns="False" onrowcommand="gridReportes_RowCommand" OnPreRender="gridReportes_PreRender">
            <Columns>
                <asp:BoundField DataField="id" HeaderText="Id" />
                <asp:BoundField DataField="nombreReporte" HeaderText="Nombre" />
                <asp:BoundField DataField="fechaCreacion" HeaderText="Fecha Creación" DataFormatString="{0:yyyy-MM-dd HH:mm:ss}" />
                <asp:BoundField DataField="fechaModificacion" HeaderText="Fecha Modificación" DataFormatString="{0:yyyy-MM-dd HH:mm:ss}" />
                <asp:BoundField DataField="segusu_login" HeaderText="Usuario Modificación" />
                <asp:ButtonField Text="Ver" CommandName="verReporte" ButtonType="Button" />
                <asp:ButtonField Text="Editar" CommandName="editarReporte" ButtonType="Button"/>
                <asp:ButtonField Text="Permisos" CommandName="permisosReporte" ButtonType="Button"/>

            </Columns>
        </asp:GridView>



    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">
     <script type="text/javascript">
        $(document).ready(function () {
            // $.fn.dataTable.moment('dd/MM/yyyy hh:mm:ss tt');
            //$.fn.dataTable.moment('DD/MM/YYYY');
            //$('#contenido_gvBibliotecasAsignadas').DataTable({
            //    "columnDefs": [
            //        { "type": "datetime-moment", targets: 3 }
            //    ]
            //});

            $('#'+'<%=gridReportes.ClientID %>').DataTable();


     
    } );
   </script>
</asp:Content>

