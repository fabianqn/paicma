﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmWFEditExistingPlan" Codebehind="frmWFEditExistingPlan.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">


    <div class="contacto2">
        <h1>Editando Plan </h1>
        <hr />
        <table style="width:100%">
            <tbody>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Nueva Estrategia"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlistEstrategia" runat="server" OnSelectedIndexChanged="ddlistEstrategia_SelectedIndexChanged1" AutoPostBack="true" ></asp:DropDownList>
                         <asp:RequiredFieldValidator id="RequiredFieldValidator6" runat="server"
                              ControlToValidate="ddlistEstrategia"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red" Width="100%">
                            </asp:RequiredFieldValidator>

                    </td>
                </tr>
                    <tr>
                    <td>
                         <asp:Label ID="Label3" runat="server" Text="Nuevo Indicador"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlistIndicador" runat="server" OnSelectedIndexChanged="ddlistIndicador_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                         <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server"
                              ControlToValidate="ddlistIndicador"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red" Width="100%">
                            </asp:RequiredFieldValidator>

                    </td>
                </tr>

                 <tr>
                    <td>
                         <asp:Label ID="Label2" runat="server" Text="Nueva Actividad"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlistActividad" runat="server"></asp:DropDownList>
                         <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server"
                              ControlToValidate="ddlistActividad"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red" Width="100%">
                            </asp:RequiredFieldValidator>

                    </td>
                </tr>
            
                 <tr>
                <td colspan="2">
                    <hr />
                    <div class="Botones">

                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
                         <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
      <%--                  <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" />--%>
                    </div>
                </td>

            </tr>


            </tbody>           
        </table>


    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="Server">
     <script type="text/javascript">
         function newDoc() {
             window.location.assign("frmWFSolicitudComision.aspx")
         }

   </script>
</asp:Content>

