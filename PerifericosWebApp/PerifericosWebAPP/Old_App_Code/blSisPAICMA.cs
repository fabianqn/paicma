﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Presidencia;
using System.DirectoryServices;
using System.Text;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using System.Globalization;



/// <summary>
/// Descripción breve de blSisPAICMA
/// </summary>
/// 
public class Controles
{
    public int TipoControl { get; set; }
    public string NombreCampo { get; set; }
    public string Label { get; set; }
    public int Orden { get; set; }
    public int LongitudMax { get; set; }
    public int idTabla { get; set; }
    public bool checkShowTable { get; set; }
};

public class blSisPAICMA : Controller
{
    public string conexionSeguridad = ConfigurationManager.ConnectionStrings["PAICMA"].ConnectionString;
    public bool PerteneceDominio(string strDominio, string strUsuario, string strPassword)
    {
        DirectorySearcher directorySearcher = new DirectorySearcher(new DirectoryEntry("LDAP://" + strDominio)
        {
            Username = strDominio + "\\" + strUsuario,
            Password = strPassword
        });
        try
        {
            if (directorySearcher.FindOne() == null)
            {
                bool result = false;
                return result;
            }
        }
        catch (Exception)
        {
            bool result = false;
            return result;
        }
        return true;
    }
    public bool fnt_ConsultaAplicativo_bol(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " Seg_Aplicativo ";
        string strCondicion = " segApl_Nombre = 'SisPAICMA' ";
        string strOrden = " segApl_Nombre ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public string CargarArchivo(string strNomSP)
    {
        string text = base.llamarSpReturn(strNomSP);
        if (text == "")
        {
            return text;
        }
        return text;
    }
    public bool ReiniciaIndice(string strNomSP, string strParametro)
    {
        return base.llamarSpX(strNomSP, strParametro);
    }
    public bool fntConsultaDominioConexion_bol(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " Seg_Conexion ";
        string strCondicion = " segCon_Descripcion = 'Directorio Activo' ";
        string strOrden = " segCon_Descripcion ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaCriteriosEvaluar_bol(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " (select   ROW_NUMBER() OVER(ORDER BY A.Id) AS indice, A.* FROM tmp_AG2 A ) result ";
        string empty = string.Empty;
        string strOrden = "indice";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaCategoria_bol(string dataset, string strCategoriaDescripcion)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Categoria ";
        string strCondicion = " cat_descripcion = @strCategoriaDescripcion";
        string strOrden = "cat_descripcion";
        base.clearParameters();
        base.setParameters("@strCategoriaDescripcion", System.Data.SqlDbType.NVarChar, strCategoriaDescripcion);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTmpMunicipiosPriorizados(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " (select ROW_NUMBER() OVER(ORDER BY A.Id) AS indice, A.* from tmp_ue1 A ) result ";
        string empty = string.Empty;
        string strOrden = "indice";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaMunicipios(string dataset, int intOp, string strNombreMunicipio)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Municipio ";
        string strCondicion = "nomMunicipio = (SELECT LTRIM(RTRIM(MUNICIPIO)) FROM (SELECT  @strNombreMunicipio MUNICIPIO) RESULT) ";
        string strOrden = "nomMunicipio";
        base.clearParameters();
        base.setParameters("@strNombreMunicipio", System.Data.SqlDbType.NVarChar, strNombreMunicipio);
        if (intOp == 1)
        {
            strCondicion = "codDepartamentoAlf2 = @strNombreMunicipio ";
        }
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaDepartamento_bol(string dataset, int intOp, string strNombreDepartamento)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Departamento d ";
        string strCondicion = " d.nomDepartamento = @strNombreDepartamento";
        string strOrden = " d.nomDepartamento ";
        base.clearParameters();
        base.setParameters("@strNombreDepartamento", System.Data.SqlDbType.NVarChar, strNombreDepartamento);
        if (intOp == 1)
        {
            strCondicion = string.Empty;
        }
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaVereda_bol(string dataset, string strNombreVereda)
    {
        string strCampos = " * ";
        string strTabla = " tbl_vereda v ";
        string strCondicion = " v.vereda = @strNombreVereda";
        string strOrden = " v.vereda ";
        base.clearParameters();
        base.setParameters("@strNombreVereda", System.Data.SqlDbType.NVarChar, strNombreVereda);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTodasLasVereda_bol(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_vereda v ";
        string strCondicion = "1 = 1";
        string strOrden = " v.vereda ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaEncuestado_bol(string dataset, string strNumeroIdentificacion)
    {
        string strCampos = " * ";
        string strTabla = " tbl_encuestado e ";
        string strCondicion = " e.identificacionNacionalPersona = @strNumeroIdentificacion";
        string strOrden = " e.enc_Id ";
        base.clearParameters();
        base.setParameters("@strNumeroIdentificacion", System.Data.SqlDbType.NVarChar, strNumeroIdentificacion);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaDatosEncuesta_bol(string dataset, string strTema, string strFecha, int IntIdCapacitador, int intIdProcedencia, string strCodigoProccedencia)
    {
        string strCampos = " * ";
        string strTabla = " tbl_datosEncuesta ";
        string strCondicion = " nomTema = @strTema AND fecha = @strFecha AND cap_Id = @IntIdCapacitador AND  encu_Procedencia = @intIdProcedencia AND encu_CodigoProcedencia = @strCodigoProccedencia";
        string strOrden = " datEnc_Id ";
        base.clearParameters();
        base.setParameters("@strTema", System.Data.SqlDbType.NVarChar, strTema);
        base.setParameters("@strFecha", System.Data.SqlDbType.Date, strFecha);
        base.setParameters("@IntIdCapacitador", System.Data.SqlDbType.Int, IntIdCapacitador);
        base.setParameters("@intIdProcedencia", System.Data.SqlDbType.Int, intIdProcedencia);
        base.setParameters("@strCodigoProccedencia", System.Data.SqlDbType.NVarChar, strCodigoProccedencia);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTipoIdentificacion_bol(string dataset, string strTipoIdentificacion, int intOP)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Tipo T ";
        string strCondicion = " T.tip_Categoria = 'identificacion' AND T.tip_Descripcion = @strTipoIdentificacion";
        string strOrden = " T.tip_Descripcion ";
        base.clearParameters();
        base.setParameters("@strTipoIdentificacion", System.Data.SqlDbType.NVarChar, strTipoIdentificacion);
        if (intOP == 1)
        {
            strCondicion = "T.tip_Categoria = 'identificacion'";
        }
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaGenero_bol(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Tipo ";
        string strCondicion = " tip_Categoria = 'Genero' ";
        string strOrden = " tip_Descripcion ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaEtnia_bol(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Etnia ";
        string empty = string.Empty;
        string strOrden = " nomGrupoPoblacional ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaNivelFormacion_bol(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Formacion ";
        string empty = string.Empty;
        string strOrden = " for_id ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaEncuestaTmp_bol(string dataset)
    {
        string strCampos = " Id ID, erm1 Item, erm2 Identificacion, erm3 NIdentificacion, erm4 Nombre, erm5 Apellido, erm6 Edad, erm7 Pregunta, erm8 Respuesta, erm9 RespuestaCorr ";
        string strTabla = " tmp_erm2 r ";
        string strCondicion = " r.Id > (select e.Id from tmp_erm2 e where e.erm1 = 'ITEM')";
        string strOrden = " Id ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaVictimaTmp_bol(string dataset)
    {
        string strCampos = " a.av10, a.av11, a.av8, a.av9, a.av12, a.av13, a.av14, a.av15, a.av16, a.av21, a.av34 ";
        string strTabla = " tmp_av1 a ";
        string empty = string.Empty;
        string strOrden = " a.av11";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaProcedenciaEncuestaTmp_bol(string dataset)
    {
        string strCampos = " e.id ID, e.erm1 titulo, e.erm2 procedencia ";
        string strTabla = " tmp_erm2 e ";
        string strCondicion = " e.erm1 in ('DEPARTAMENTO','MUNICIPIO','VEREDA') ";
        string strOrden = " e.id ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTemaEncuestaTmp_bol(string dataset)
    {
        string strCampos = " e.id ID, e.erm1 titulo, e.erm2 descripcion ";
        string strTabla = " tmp_erm2 e ";
        string strCondicion = " e.erm1 in ('TEMA','FECHA') ";
        string strOrden = " e.id ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaApoyoGestionPlanOperativoTmp_bol(string dataset)
    {
        string strCampos = " Id as Id , C.com_Id IdComponente, ag4 ano,ag5 Mes, ag6 FechaInicio, ag7 FechaFinal, ag10 AcciionesAdelantadas, ag11 EvidenciasAcciones, ag12 Resultados,  E.ebi_ID  IdEBI, Apo.actPO_Id  IdActividadPO,  D.codDepartamentoAlf2 Departamento, ag9 Municipios, ag13 CompromisosPaicma, Es1.est_Id EstadoCompromiso, ag15 FechaFinCompromiso, res1.res_Id IdResponsablePAICMA, ag19 CompromisoOtros, Es2.est_Id EstadoCompromisoOtos, ag21 FechaFinCompromisoOtros, res2.res_Id IdResponsableOtros   ";
        string strTabla = " tmp_AG3 AS A INNER JOIN tbl_Componente AS C ON A.ag1 = C.com_Nombre INNER JOIN tbl_EBI AS E ON A.ag2 = E.ebi_Proyecto INNER JOIN tbl_ActividadPO AS Apo ON A.ag3 = Apo.actPO_Descripcion INNER JOIN tbl_Departamento AS D ON A.ag8 = D.nomDepartamento INNER JOIN tbl_Responsable AS res1 ON A.ag16 + ' ' + A.ag17 = res1.nombrePersona + ' ' + res1.ApellidoPersona INNER JOIN tbl_Responsable AS res2 ON A.ag22 + ' ' + A.ag23 = res2.nombrePersona + ' ' + res2.ApellidoPersona LEFT OUTER JOIN tbl_Estado AS Es1 ON A.ag14 = Es1.est_Descripcion LEFT OUTER JOIN tbl_Estado AS Es2 ON A.ag20 = Es2.est_Descripcion  ";
        string empty = string.Empty;
        string strOrden = " Id ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaSeguimientoPO_bol(string dataset, int intIdComponente, int intAno, string strMes, string strFechaInicio, string strFechaFinal, string strsAccionesAdelantadas, string strEvidenciasAdelantadas, string strResultados, int intIdEbi)
    {
        string strCampos = " * ";
        string strTabla = " tbl_SeguimientoPO ";
        string strCondicion = " com_Id = @intIdComponente AND ano = @intAno AND mes = @strMes AND fechaInicio = @strFechaInicio AND fechaFinal = @strFechaFinal AND segPO_AccionesAdelantadas = @strsAccionesAdelantadas AND segPO_EvidenciasAdelantadas = @strEvidenciasAdelantadas AND segPO_Resultados = @strResultados AND ebi_Id = @intIdEbi   ";
        string strOrden = " com_Id  ";
        base.clearParameters();
        base.setParameters("@intIdComponente", System.Data.SqlDbType.Int, intIdComponente);
        base.setParameters("@intAno", System.Data.SqlDbType.Int, intAno);
        base.setParameters("@strMes", System.Data.SqlDbType.NVarChar, strMes);
        base.setParameters("@strFechaInicio", System.Data.SqlDbType.NVarChar, strFechaInicio);
        base.setParameters("@strFechaFinal", System.Data.SqlDbType.NVarChar, strFechaFinal);
        base.setParameters("@strsAccionesAdelantadas", System.Data.SqlDbType.NVarChar, strsAccionesAdelantadas);
        base.setParameters("@strEvidenciasAdelantadas", System.Data.SqlDbType.NVarChar, strEvidenciasAdelantadas);
        base.setParameters("@strResultados", System.Data.SqlDbType.NVarChar, strResultados);
        base.setParameters("@intIdEbi", System.Data.SqlDbType.Int, intIdEbi);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaDatosGeneralesTmp_bol(string dataset)
    {
        string strCampos = " top 2 gt1, gt2 ";
        string strTabla = " tmp_GT1 ";
        string empty = string.Empty;
        string strOrden = " id ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaMaxMatrizIntervencion_bol(string dataset)
    {
        string strCampos = " max(m.matint_CodigoInforme) idCodigoInforme ";
        string strTabla = " tbl_MatrizIntervencion m ";
        string empty = string.Empty;
        string strOrden = " idCodigoInforme ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaActividadesTmpGT1_bol(string dataset)
    {
        string strCampos = " DISTINCT g.gt10 Actividad, g.gt7 FechaInicio, g.gt8 FechaFinal, A.acti_Id  ";
        string strTabla = " tmp_GT1 AS g LEFT OUTER JOIN tbl_Actividad AS A ON g.gt10 = A.actividadOcupacionalPersona AND g.gt7 = A.fechaInicio AND g.gt8 = A.fechaFinal ";
        string empty = string.Empty;
        string strOrden = " g.gt10 ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaCompromisosTmpGT1_bol(string dataset)
    {
        string strCampos = " DISTINCT g.gt15 Compromisos, E.est_Id estado, g.gt7 FechaInicio, null fechaFinal, R.res_Id  IdAsesor, null IdSegPO, C.comp_Id ";
        string strTabla = " tmp_GT1 AS g INNER JOIN tbl_Responsable AS R ON g.gt12 = R.nombrePersona + ' ' + R.ApellidoPersona LEFT OUTER JOIN tbl_Compromiso AS C ON g.gt15 = C.comp_Descripcion AND R.res_Id = C.res_Id  CROSS JOIN tbl_Estado AS E ";
        string strCondicion = " E.est_Descripcion = 'PENDIENTE' ";
        string strOrden = " g.gt15 ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaMatrizIntervencionTmpGT1_bol(string dataset)
    {
        string strCampos = " g.Id, g.gt7 FechaInicio, g.gt8 FechaFinal, g.gt6 Mes, g.gt9 ObjetoComision, A.acti_Id IdActividad, Co.comp_Id IdCompromisos, R.res_Id  IdAsesor, c.com_Id IdComponente, g.gt13 Soporte, g.gt14 Resultados, g.gt4 MunicipioIntervenido, g.gt5 ComunidadImpactada  ";
        string strTabla = " tmp_GT1 g, tbl_Componente C, tbl_Responsable R, tbl_Actividad A, tbl_Compromiso Co ";
        string strCondicion = " g.gt11 = C.com_Nombre AND g.gt12 = (R.nombrePersona + ' ' + R.ApellidoPersona) AND g.gt10 = A.actividadOcupacionalPersona AND g.gt15 = Co.comp_Descripcion AND R.res_Id = co.res_Id AND g.gt7 = Co.fechaInicio ";
        string strOrden = " g.Id ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaMatrizIntervencion_bol(string dataset, int intIdCodigoInforme, string strObservacion, string strFecha, string strFechaInicio, string strFechaFinal, string strMes, string strObjetoComision, int intIdActividad, int intIdCompromiso, int intIdResponsable, int intIdComponente, string strSoporte, string strResultado)
    {
        string strCampos = " * ";
        string strTabla = " tbl_MatrizIntervencion ";
        string strCondicion = " matint_CodigoInforme = @intIdCodigoInforme AND observacion = @strObservacion AND fecha = @strFecha AND fechaInicio = @strFechaInicio AND fechaFinal = @strFechaFinal AND mes = @strMes AND matint_ObjetoComision = @strObjetoComision AND acti_Id = @intIdActividad AND comp_Id = @intIdCompromiso AND res_Id = @intIdResponsable AND com_Id = @intIdComponente AND matint_Soporte = @strSoporte AND matint_Resultado = @strResultado ";
        string strOrden = " matint_Id ";
        base.clearParameters();
        base.setParameters("@intIdCodigoInforme", System.Data.SqlDbType.NVarChar, intIdCodigoInforme);
        base.setParameters("@strObservacion", System.Data.SqlDbType.NVarChar, strObservacion);
        base.setParameters("@strFecha", System.Data.SqlDbType.NVarChar, strFecha);
        base.setParameters("@strFechaInicio", System.Data.SqlDbType.NVarChar, strFechaInicio);
        base.setParameters("@strFechaFinal", System.Data.SqlDbType.NVarChar, strFechaFinal);
        base.setParameters("@strMes", System.Data.SqlDbType.NVarChar, strMes);
        base.setParameters("@strObjetoComision", System.Data.SqlDbType.NVarChar, strObjetoComision);
        base.setParameters("@intIdActividad", System.Data.SqlDbType.NVarChar, intIdActividad);
        base.setParameters("@intIdCompromiso", System.Data.SqlDbType.NVarChar, intIdCompromiso);
        base.setParameters("@intIdResponsable", System.Data.SqlDbType.NVarChar, intIdResponsable);
        base.setParameters("@intIdComponente", System.Data.SqlDbType.NVarChar, intIdComponente);
        base.setParameters("@strSoporte", System.Data.SqlDbType.NVarChar, strSoporte);
        base.setParameters("@strResultado", System.Data.SqlDbType.NVarChar, strResultado);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaComunidad_bol(string dataset, string strComunidad)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Comunidad ";
        string strCondicion = " comu_Nombre = (SELECT LTRIM(RTRIM(COMUNIDAD)) FROM (SELECT  @strComunidad COMUNIDAD) RESULT) ";
        string strOrden = " comu_Id ";
        base.clearParameters();
        base.setParameters("@strComunidad", System.Data.SqlDbType.NVarChar, strComunidad);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaUsuario_bol(string dataset, string strUsuario)
    {
        string strCampos = " u.segusu_Id, C.cnxDominio, C.segCon_Descripcion, u.segusu_login, u.segusu_Password, e.est_Descripcion ";
        string strTabla = " Seg_Usuario u, tbl_Estado e, Seg_Conexion c ";
        string strCondicion = " u.est_Id = e.est_Id AND U.segcon_Id = C.segcon_Id AND u.segusu_login = @strUsuario";
        string strOrden = " u.segusu_Id ";
        base.clearParameters();
        base.setParameters("@strUsuario", System.Data.SqlDbType.NVarChar, strUsuario);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaSoloUsuario_bol(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " Seg_Usuario u";
        string empty = string.Empty;
        string strOrden = " u.segusu_Id ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaEstadoSeguimientoOperador_bol(string dataset)
    {
        string strCampos = " est_Id, est_Descripcion, tip_Id ";
        string strTabla = " tbl_Estado ";
        string strCondicion = " tip_Id IN (select tip_Id from tbl_Tipo WHERE  tip_Descripcion = 'ActividadOperador') ";
        string strOrden = " est_Descripcion ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaMenu_bol(string dataset, int intIdUsuario, int intIdTipoFormulario)
    {
        string strCampos = " up.segfor_Id, F.segfor_Nombre, F.segfor_Orden, F.segfor_DescripcionMenu, F.segfor_origen ";
        string strTabla = " Seg_UsuarioPermiso up, Seg_Formulario F ";
        string strCondicion = " UP.segfor_Id = F.segfor_Id AND UP.segusu_Id = @intIdUsuario AND f.tip_Id = @intIdTipoFormulario ";
        string strOrden = " segfor_Orden ";
        base.clearParameters();
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        base.setParameters("@intIdTipoFormulario", System.Data.SqlDbType.Int, intIdTipoFormulario);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaPermisosUsuarioFormulario_bol(string dataset, int intIdUsuario, string strNombreFormulario)
    {
        string strCampos = " S.segusu_Id, S.segfor_Id, S.segusuper_Consulta Buscar, S.segusuper_Creacion Nuevo, S.segusuper_Modificar Editar, S.segusuper_Eliminar Eliminar, S.segusuper_Impresion Imprimir, F.segfor_Nombre ";
        string strTabla = " Seg_UsuarioPermiso S, Seg_Formulario F ";
        string strCondicion = " S.segfor_Id = F.segfor_Id AND S.segusu_Id = @intIdUsuario AND F.segfor_Nombre = @strNombreFormulario ";
        string strOrden = " segusu_Id ";
        base.clearParameters();
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        base.setParameters("@strNombreFormulario", System.Data.SqlDbType.NVarChar, strNombreFormulario);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaCategoria_bol(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Categoria ";
        string empty = string.Empty;
        string strOrden = " cat_descripcion ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaVocativo_bol(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Vocativo ";
        string empty = string.Empty;
        string strOrden = " voc_Descripcion ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaCargo_bol(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Cargo ";
        string empty = string.Empty;
        string strOrden = " cargo ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTipoTelefonoYcorreo(string dataset, string strOpcion)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Tipo ";
        string strCondicion = "tip_Categoria = @strOpcion";
        string strOrden = " tip_Descripcion ";
        base.clearParameters();
        base.setParameters("@strOpcion", System.Data.SqlDbType.NVarChar, strOpcion);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTipoTelefonos(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Tipo ";
        string strCondicion = "tip_Categoria = 'TipoTelefono'";
        string strOrden = " tip_Descripcion ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTipo(string dataset, string strOpcion)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Tipo ";
        string strCondicion = "tip_Categoria = @strOpcion";
        string strOrden = " tip_Descripcion ";
        base.clearParameters();
        base.setParameters("@strOpcion", System.Data.SqlDbType.NVarChar, strOpcion);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaInstitucion(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Institucion  ";
        string empty = string.Empty;
        string strOrden = " ins_Nombre ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaPersona(string dataset, int intIdVocativo, string strNombre, string strApellido, int intIdCargo, int intIdCategoria, string strDireccion, int intIdTipoDireccion, string strCodMunicipio, int intIdInstitucion, int intOP, int intIdTipoIdentificacion, string strNumeroIdentificacion)
    {
        string strCampos = " * ";
        string strTabla = "  tbl_Persona ";
        string strCondicion = " voc_Id = @intIdVocativo AND nombrePersona = @strNombre AND apellidoPersona = @strApellido AND car_Id = @intIdCargo AND cat_Id = @intIdCategoria AND direccion = @strDireccion AND  tip_Id = @intIdTipoDireccion AND codMunicipioAlf5 = @strCodMunicipio AND ins_Id = @intIdInstitucion ";
        string strOrden = " nombrePersona ";
        base.clearParameters();
        base.setParameters("@intIdVocativo", System.Data.SqlDbType.Int, intIdVocativo);
        base.setParameters("@strNombre", System.Data.SqlDbType.NVarChar, strNombre);
        base.setParameters("@strApellido", System.Data.SqlDbType.NVarChar, strApellido);
        base.setParameters("@intIdCargo", System.Data.SqlDbType.Int, intIdCargo);
        base.setParameters("@intIdCategoria", System.Data.SqlDbType.Int, intIdCategoria);
        base.setParameters("@strDireccion", System.Data.SqlDbType.NVarChar, strDireccion);
        base.setParameters("@intIdTipoDireccion", System.Data.SqlDbType.Int, intIdTipoDireccion);
        base.setParameters("@strCodMunicipio", System.Data.SqlDbType.NVarChar, strCodMunicipio);
        base.setParameters("@intIdInstitucion", System.Data.SqlDbType.Int, intIdInstitucion);
        base.setParameters("@intIdTipoIdentificacion", System.Data.SqlDbType.Int, intIdTipoIdentificacion);
        base.setParameters("@strNumeroIdentificacion", System.Data.SqlDbType.NVarChar, strNumeroIdentificacion);
        if (intOP == 1)
        {
            if (intIdTipoIdentificacion != 0)
            {
                if (strNumeroIdentificacion != string.Empty)
                {
                    strCondicion = "tip_Identificacion = @intIdTipoIdentificacion AND identificacionNacionalPersona = @strNumeroIdentificacion";
                }
                else
                {
                    strCondicion = " tip_Identificacion = @intIdTipoIdentificacion ";
                }
            }
            else if (strNumeroIdentificacion != string.Empty)
            {
                strCondicion = " identificacionNacionalPersona = @strNumeroIdentificacion ";
            }
        }
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaRubroPresupuestal(string dataset)
    {
        string strCampos = " rubpre_Id, ltrim(rtrim(rubpre_codigo)) + ' - ' + ltrim(rtrim(rubpre_Descripcion)) RubroPresupuestal ";
        string strTabla = " tbl_RubroPresupuestal ";
        string empty = string.Empty;
        string strOrden = " rubpre_Id ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaProyecto(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Proyecto ";
        string empty = string.Empty;
        string strOrden = " nomProyecto ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTipoProyecto(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_tipo ";
        string strCondicion = " tip_Categoria = 'proyecto' ";
        string strOrden = " tip_Descripcion ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaRecursosProyecto(string dataset, int intIdRubroPresupuestal, int intIdTipoProyecto, int intIdProyecto, string strValorTotalProyecto)
    {
        string strCampos = " * ";
        string strTabla = " tbl_RecursoProyecto ";
        string strCondicion = " rubpre_Id = @intIdRubroPresupuestal AND tip_Id = @intIdTipoProyecto AND pro_Id = @intIdProyecto AND recpro_ValorTotal = @strValorTotalProyecto AND recpro_ValorComprometido = 0 AND recpro_ValorDisponible = @strValorTotalProyecto  ";
        string strOrden = " recpro_Id  ";
        base.clearParameters();
        base.setParameters("@intIdRubroPresupuestal", System.Data.SqlDbType.Int, intIdRubroPresupuestal);
        base.setParameters("@intIdTipoProyecto", System.Data.SqlDbType.Int, intIdTipoProyecto);
        base.setParameters("@intIdProyecto", System.Data.SqlDbType.Int, intIdProyecto);
        base.setParameters("@strValorTotalProyecto", System.Data.SqlDbType.Float, strValorTotalProyecto);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaActividadRecursosProyecto(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Actividad ";
        string empty = string.Empty;
        string strOrden = " acti_Id ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaEstadoActividadesProyectosRecursos(string dataset)
    {
        string strCampos = " E.est_Id, E.est_Descripcion ";
        string strTabla = " tbl_Estado E, tbl_Tipo T ";
        string strCondicion = "e.tip_Id = T.tip_Id AND T.tip_Descripcion = 'ActividadProyecto'";
        string strOrden = " E.est_Descripcion ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaActividadesGrilla(string dataset, int intIdRecursoProyecto, int intOP, int intIdActividadProyecto)
    {
        string strCampos = " AP.actpro_Id, AP.acti_Id, A.actividadOcupacionalPersona, AP.recpro_Id, AP.actpro_Valorproyectado, AP.actpro_ValorContratado, AP.actpro_ValorDisponible, AP.est_Id, E.est_Descripcion ";
        string strTabla = " tbl_ActividadProyecto AP, tbl_Actividad A, tbl_Estado E ";
        string strCondicion = " AP.acti_Id = A.acti_Id AND AP.est_Id = E.est_Id AND AP.recpro_Id = @intIdRecursoProyecto ";
        string strOrden = " AP.actpro_Id ";
        base.clearParameters();
        base.setParameters("@intIdRecursoProyecto", System.Data.SqlDbType.NVarChar, intIdRecursoProyecto);
        base.setParameters("@intIdActividadProyecto", System.Data.SqlDbType.NVarChar, intIdActividadProyecto);
        if (intOP == 1)
        {
            strCondicion = " AP.acti_Id = A.acti_Id AND AP.est_Id = E.est_Id AND actpro_Id = @intIdActividadProyecto";
        }
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaProyectosGrilla(string dataset, int intOp, int intIdCodigoPresupuesal, int intIdProyecto, int intIdTipoProyecto, string strNoContrato, string strFechaContrato, int intIdTipoIdentificacion, string strNumeroIdentificacion, float flbValorTotalProyecto, int intIdRecursoProyecto)
    {
        string strCampos = " RP.recpro_Id, RP.rubpre_Id, rtrim(ltrim(R.rubpre_codigo)) + ' - '+ rtrim(ltrim(R.rubpre_Descripcion)) RubroPresupuestal, P.pro_Id, P.nomProyecto, RP.tip_Id, T.tip_Descripcion, RP.recpro_ValorTotal, RP.recpro_ValorComprometido, RP.recpro_ValorDisponible, PE.nombrePersona + ' ' + PE.apellidoPersona Contratista, RP.recpro_NContrato, RP.recpro_fechaContrato ";
        string strTabla = " tbl_RecursoProyecto RP, tbl_Proyecto P, tbl_RubroPresupuestal R, tbl_Tipo T, tbl_Persona PE ";
        string text = " RP.pro_Id = P.pro_Id AND RP.rubpre_Id = R.rubpre_Id AND RP.tip_Id = T.tip_Id AND RP.per_Id = PE.per_Id ";
        string strOrden = " RP.recpro_Id ";
        base.clearParameters();
        base.setParameters("@intIdCodigoPresupuesal", System.Data.SqlDbType.Int, intIdCodigoPresupuesal);
        base.setParameters("@intIdProyecto", System.Data.SqlDbType.Int, intIdProyecto);
        base.setParameters("@intIdTipoProyecto", System.Data.SqlDbType.Int, intIdTipoProyecto);
        base.setParameters("@strNoContrato", System.Data.SqlDbType.NVarChar, strNoContrato);
        base.setParameters("@strFechaContrato", System.Data.SqlDbType.NVarChar, strFechaContrato);
        base.setParameters("@intIdTipoIdentificacion", System.Data.SqlDbType.Int, intIdTipoIdentificacion);
        base.setParameters("@strNumeroIdentificacion", System.Data.SqlDbType.NVarChar, strNumeroIdentificacion);
        base.setParameters("@flbValorTotalProyecto", System.Data.SqlDbType.Float, flbValorTotalProyecto);
        base.setParameters("@intIdRecursoProyecto", System.Data.SqlDbType.Int, intIdRecursoProyecto);
        if (intOp == 1)
        {
            if (intIdCodigoPresupuesal != 0)
            {
                text += " AND RP.rubpre_Id = @intIdCodigoPresupuesal ";
            }
            if (intIdProyecto != 0)
            {
                text += " AND P.pro_Id = @intIdProyecto ";
            }
            if (intIdTipoProyecto != 0)
            {
                text += " AND RP.tip_Id = @intIdTipoProyecto ";
            }
            if (strNoContrato != string.Empty)
            {
                text += " AND RP.recpro_NContrato = @strNoContrato ";
            }
            if (strFechaContrato != string.Empty)
            {
                text += " AND RP.recpro_fechaContrato = @strFechaContrato ";
            }
            if (intIdTipoIdentificacion != 0)
            {
                if (strNumeroIdentificacion != string.Empty)
                {
                    text += " AND RP.per_Id IN (select per_Id from tbl_Persona p where p.identificacionNacionalPersona = @strNumeroIdentificacion AND  P.tip_Identificacion = @intIdTipoIdentificacion) ";
                }
                else
                {
                    text += " AND RP.per_Id IN (select per_Id from tbl_Persona p where P.tip_Identificacion = @intIdTipoIdentificacion) ";
                }
            }
            else if (strNumeroIdentificacion != string.Empty)
            {
                text += " AND RP.per_Id IN (select per_Id from tbl_Persona p where p.identificacionNacionalPersona = @strNumeroIdentificacion ) ";
            }
            if (flbValorTotalProyecto != 0f)
            {
                text += " AND RP.recpro_ValorTotal = @flbValorTotalProyecto ";
            }
        }
        if (intOp == 2)
        {
            strCampos = " RP.recpro_Id, RP.rubpre_Id, rtrim(ltrim(R.rubpre_codigo)) + ' - '+ rtrim(ltrim(R.rubpre_Descripcion)) RubroPresupuestal, P.pro_Id, P.nomProyecto, RP.tip_Id, T.tip_Descripcion, RP.recpro_ValorTotal, RP.recpro_ValorComprometido, RP.recpro_ValorDisponible, PE.identificacionNacionalPersona, PE.tip_Identificacion, PE.nombrePersona + ' ' + PE.apellidoPersona Contratista, RP.recpro_NContrato, RP.recpro_fechaContrato, PE.per_Id  ";
            text += " AND RP.recpro_Id = @intIdRecursoProyecto ";
        }
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaResponsables(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Responsable ";
        string empty = string.Empty;
        string strOrden = " nombrePersona ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTaller(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Taller ";
        string empty = string.Empty;
        string strOrden = " tal_Nombre ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaEventoTaller(string dataset, string strIdMunicipio, int intIdTaller, string strFechaTaller, string strObjeto, int intIdResponsable)
    {
        string strCampos = " * ";
        string strTabla = " tbl_EventoTaller ";
        string strCondicion = " codMunicipioAlf5 = @strIdMunicipio AND tal_Id = @intIdTaller AND fecha = @strFechaTaller AND evetal_Objeto = @strObjeto AND res_Id = @intIdResponsable ";
        string strOrden = " evetal_Id ";
        base.clearParameters();
        base.setParameters("@strIdMunicipio", System.Data.SqlDbType.NVarChar, strIdMunicipio);
        base.setParameters("@intIdTaller", System.Data.SqlDbType.Int, intIdTaller);
        base.setParameters("@strFechaTaller", System.Data.SqlDbType.Date, strFechaTaller);
        base.setParameters("@strObjeto", System.Data.SqlDbType.NVarChar, strObjeto);
        base.setParameters("@intIdResponsable", System.Data.SqlDbType.Int, intIdResponsable);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaEventoTalleGrilla(string dataset, int intOP, string strIdDepartamento, string strIdMunicipio, int intIdTaller, string strFechaTaller, string strObjeto, int intIdResponsable, string strOperadorLogistico, int intIdEventoTaller)
    {
        string strCampos = " E.evetal_Id, V.valtal_Id, D.codDepartamentoAlf2, D.nomDepartamento, E.codMunicipioAlf5, M.nomMunicipio, E.tal_Id, T.tal_Nombre, E.fecha, E.evetal_Objeto, E.res_Id, R.nombrePersona + ' ' + R.ApellidoPersona Responsable, V.valtal_ValorSinIva, V.valtal_ValorIva, V.valtal_ValorComision, V.valtal_ValorTotal, V.valtal_Saldo, V.valtal_Factura, V.fecha AS Expr5, V.valtal_NOrdenPagoSIIF, V.valtal_NActa, V.FechaActa, E.evetal_operadorLogistico ";
        string strTabla = " tbl_EventoTaller E,tbl_Municipio M, tbl_Taller T, tbl_Responsable R, tbl_ValorTaller V, tbl_Departamento D ";
        string text = " E.codMunicipioAlf5 = M.codMunicipioAlf5 AND E.tal_Id = T.tal_Id AND E.res_Id = R.res_Id AND E.evetal_Id = V.evetal_Id AND M.codDepartamentoAlf2 = D.codDepartamentoAlf2 ";
        string strOrden = " E.evetal_Id ";
        base.clearParameters();
        base.setParameters("@strIdDepartamento", System.Data.SqlDbType.NVarChar, strIdDepartamento);
        base.setParameters("@strIdMunicipio", System.Data.SqlDbType.NVarChar, strIdMunicipio);
        base.setParameters("@intIdTaller", System.Data.SqlDbType.Int, intIdTaller);
        base.setParameters("@strFechaTaller", System.Data.SqlDbType.NVarChar, strFechaTaller);
        base.setParameters("@strObjeto", System.Data.SqlDbType.NVarChar, "%" + strObjeto + "%");
        base.setParameters("@intIdResponsable", System.Data.SqlDbType.Int, intIdResponsable);
        base.setParameters("@strOperadorLogistico", System.Data.SqlDbType.NVarChar, "%" + strOperadorLogistico + "%");
        base.setParameters("@intIdEventoTaller", System.Data.SqlDbType.Int, intIdEventoTaller);
        if (intOP == 1)
        {
            if (strIdDepartamento != string.Empty)
            {
                text += " AND D.codDepartamentoAlf2 = @strIdDepartamento ";
            }
            if (strIdMunicipio != string.Empty)
            {
                text += " AND E.codMunicipioAlf5 = @strIdMunicipio ";
            }
            if (intIdTaller != 0)
            {
                text += " AND E.tal_Id = @intIdTaller ";
            }
            if (strFechaTaller != string.Empty)
            {
                text += " AND E.fecha = @strFechaTaller  ";
            }
            if (strObjeto != string.Empty)
            {
                text += " AND E.evetal_Objeto LIKE @strObjeto ";
            }
            if (intIdResponsable != 0)
            {
                text += " AND E.res_Id = @intIdResponsable ";
            }
            if (strOperadorLogistico != string.Empty)
            {
                text += " AND E.evetal_operadorLogistico like @strOperadorLogistico  ";
            }
        }
        if (intOP == 2)
        {
            text += " AND E.evetal_Id = @intIdEventoTaller ";
        }
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaPersonaGrilla(string dataset, int intOp, int intIdCategoria, int intIdVocativo, int intIdCargo, string strNombre, string strApellido, int intIdInstitucion, string strIdDepartamento, string strIdMunicipio, int intIdPersona)
    {
        string strCampos = " P.per_Id ,C.cat_descripcion, CA.cargo , P.nombrePersona + ' ' +  P.apellidoPersona Persona, I.ins_Nombre, D.nomDepartamento, M.nomMunicipio, V.voc_Descripcion, p.per_paginaWeb ";
        string strTabla = " tbl_Persona P, tbl_Categoria C, tbl_Institucion I, tbl_Municipio M, tbl_Departamento D, tbl_Cargo CA, tbl_Vocativo V ";
        string text = " P.cat_Id = C.cat_Id AND P.ins_Id = I.ins_Id AND P.codMunicipioAlf5 = M.codMunicipioAlf5 AND M.codDepartamentoAlf2 = D.codDepartamentoAlf2 AND P.car_Id = CA.car_Id AND P.voc_Id = V.voc_Id  ";
        string strOrden = " Persona ";
        base.clearParameters();
        base.setParameters("@intIdCategoria", System.Data.SqlDbType.Int, intIdCategoria);
        base.setParameters("@intIdVocativo", System.Data.SqlDbType.Int, intIdVocativo);
        base.setParameters("@intIdCargo", System.Data.SqlDbType.Int, intIdCargo);
        base.setParameters("@strNombre", System.Data.SqlDbType.NVarChar, '%' + strNombre + '%');
        base.setParameters("@strApellido", System.Data.SqlDbType.NVarChar, '%' + strApellido + '%');
        base.setParameters("@intIdInstitucion", System.Data.SqlDbType.Int, intIdInstitucion);
        base.setParameters("@strIdDepartamento", System.Data.SqlDbType.NVarChar, strIdDepartamento);
        base.setParameters("@strIdMunicipio", System.Data.SqlDbType.NVarChar, strIdMunicipio);
        base.setParameters("@intIdPersona", System.Data.SqlDbType.Int, intIdPersona);
        if (intOp == 1)
        {
            if (intIdCategoria != 0)
            {
                text += " AND C.cat_Id = @intIdCategoria ";
            }
            if (intIdVocativo != 0)
            {
                text += " AND V.voc_Id = @intIdVocativo ";
            }
            if (intIdCargo != 0)
            {
                text += " AND P.car_Id = @intIdCargo ";
            }
            if (strNombre != string.Empty)
            {
                text += " AND P.nombrePersona LIKE  @strNombre ";
            }
            if (strApellido != string.Empty)
            {
                text += " AND P.apellidoPersona LIKE  @strApellido ";
            }
            if (intIdInstitucion != 0)
            {
                text += " AND P.ins_Id = @intIdInstitucion ";
            }
            if (strIdDepartamento != string.Empty)
            {
                text += " AND M.codDepartamentoAlf2 = @strIdDepartamento ";
            }
            if (strIdMunicipio != string.Empty)
            {
                text += " AND P.codMunicipioAlf5 = @strIdMunicipio ";
            }
        }
        if (intOp == 2)
        {
            strCampos = " P.per_Id, C.cat_Id, C.cat_descripcion, V.voc_Id, V.voc_Descripcion,CA.car_Id, CA.cargo, P.nombrePersona, P.apellidoPersona, I.ins_Id, I.ins_Nombre, D.codDepartamentoAlf2, D.nomDepartamento, M.codMunicipioAlf5 ,M.nomMunicipio, P.direccion, td.tip_Id ,TD.tip_Descripcion TipoDescripcion, p.identificacionNacionalPersona, TI.tip_Descripcion, TI.tip_Id ";
            strTabla = " tbl_Persona AS P INNER JOIN tbl_Categoria AS C ON P.cat_Id = C.cat_Id INNER JOIN tbl_Institucion AS I ON P.ins_Id = I.ins_Id INNER JOIN tbl_Municipio AS M ON P.codMunicipioAlf5 = M.codMunicipioAlf5 INNER JOIN tbl_Departamento AS D ON M.codDepartamentoAlf2 = D.codDepartamentoAlf2 INNER JOIN tbl_Cargo AS CA ON P.car_Id = CA.car_Id INNER JOIN tbl_Vocativo AS V ON P.voc_Id = V.voc_Id INNER JOIN tbl_Tipo AS TD ON P.tip_Id = TD.tip_Id LEFT OUTER JOIN tbl_Tipo AS TI ON P.tip_Identificacion = TI.tip_Id ";
            text = " P.per_Id = @intIdPersona ";
            strOrden = " P.per_Id ";
        }
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTelefono(string dataset, int intIdPersona, int intTipo, int intIdTelefono)
    {
        string strCampos = " T.tel_Id, T.numTelefono, T.tip_Id, Tp.tip_Descripcion, TPT.tip_Id TipoTelefono, TPT.tip_Descripcion AS DescTipoTelefono ";
        string strTabla = " tbl_Telefono T, tbl_Tipo Tp, tbl_Tipo TPT ";
        string text = " T.tip_Id = TP.tip_Id AND T.tip_IdTipoTelefono = TPT.tip_Id AND per_Id = @intIdPersona ";
        string strOrden = " T.tel_Id ";
        base.clearParameters();
        base.setParameters("@intIdPersona", System.Data.SqlDbType.Int, intIdPersona);
        base.setParameters("@intIdTelefono", System.Data.SqlDbType.Int, intIdTelefono);
        if (intTipo == 1)
        {
            text += " AND tel_Id = @intIdTelefono ";
        }
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaCorreoElectronico(string dataset, int intIdPersona, int intOp, int IdCorreoElectronico)
    {
        string strCampos = " * ";
        string strTabla = " tbl_CorreoElectronico C, tbl_Tipo T ";
        string text = " c.tip_Id = t.tip_Id AND per_Id = @intIdPersona  ";
        string strOrden = " c.corele_Id ";
        base.clearParameters();
        base.setParameters("@intIdPersona", System.Data.SqlDbType.Int, intIdPersona);
        base.setParameters("@IdCorreoElectronico", System.Data.SqlDbType.Int, IdCorreoElectronico);
        if (intOp == 1)
        {
            text += " AND corele_Id = @IdCorreoElectronico ";
        }
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTipoTelefono(string dataset, string strTipoCelular)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Tipo ";
        string strCondicion = " tip_Categoria = 'TipoTelefono' AND tip_Descripcion = @strTipoCelular";
        string strOrden = " tip_Id ";
        base.clearParameters();
        base.setParameters("@strTipoCelular", System.Data.SqlDbType.NVarChar, strTipoCelular);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaGestionOperador(string dataset, int intOp, int intIdUsuarioOperador, int intIdGestionOperador, string strNombreArchivo, string strObservacionArchivo, int intIdTipoActividad, string strIdEstado, string strNombreGrupo, string strEstado, string strFechaDesde, string strFechaHasta)
    {
        string strCampos = " G.geop_Id, G.geop_rutaDocumento RUTA, G.fecha, G.geop_NombreDocumento NombreDocumento, G.geop_extDocumento extDocumento, G.segusu_Id IdUsuarioOperador, U.segusu_login usuarioOperador, G.geop_Observaciones Observaciones, S.seop_Id, S.fecha AS fechaRevision, S.segusu_Id AS IdUsuarioRevision, U2.segusu_login usuarioRevision, S.tip_Id IdTipoActividadRealizada, T.tip_Descripcion AccionRealizada, S.seop_Observacion ObservacionRevision, S.est_Id, CASE WHEN E.est_Descripcion IS NULL THEN 'POR REVISAR' ELSE E.est_Descripcion END EstadoRevision, S.seop_RutaDocumento , S.seop_NombreDocumento, S.seop_ExtDocumento, dbo.OperadorRevision(G.geop_Id) IdSeguimientoOperador, O.gruOpe_Nombre ";
        string strTabla = " tbl_GestionOperador AS G LEFT OUTER JOIN tbl_SeguimientoOperador AS S ON G.geop_Id = S.geop_Id LEFT OUTER JOIN Seg_Usuario AS U ON S.segusu_Id = U.segusu_Id LEFT OUTER JOIN Seg_Usuario AS U2 ON S.segusu_Id = U2.segusu_Id\tLEFT OUTER JOIN tbl_Tipo AS T ON S.tip_Id = T.tip_Id LEFT OUTER JOIN tbl_Estado AS E ON S.est_Id = E.est_Id INNER JOIN tbl_GrupoOperador AS O ON G.gruOpe_Id = O.gruOpe_Id INNER JOIN  tbl_GrupoUsuario AS GU ON O.gruOpe_Id = GU.gruOpe_Id ";
        string text = " GU.segUsu_Id = @intIdUsuarioOperador";
        string strOrden = " G.fecha desc ";
        base.clearParameters();
        base.setParameters("@intIdUsuarioOperador", System.Data.SqlDbType.Int, intIdUsuarioOperador);
        base.setParameters("@intIdGestionOperador", System.Data.SqlDbType.Int, intIdGestionOperador);
        base.setParameters("@strNombreArchivo", System.Data.SqlDbType.NVarChar, "%" + strNombreArchivo + "%");
        base.setParameters("@strObservacionArchivo", System.Data.SqlDbType.NVarChar, "%" + strObservacionArchivo + "%");
        base.setParameters("@intIdTipoActividad", System.Data.SqlDbType.Int, intIdTipoActividad);
        base.setParameters("@strIdEstado", System.Data.SqlDbType.NVarChar, strIdEstado);
        base.setParameters("@strNombreGrupo", System.Data.SqlDbType.NVarChar, "%" + strNombreGrupo + "%");
        base.setParameters("@strEstado", System.Data.SqlDbType.NVarChar, strEstado);
        base.setParameters("@strFechaDesde", System.Data.SqlDbType.NVarChar, strFechaDesde);
        base.setParameters("@strFechaHasta", System.Data.SqlDbType.NVarChar, strFechaHasta);
        if (intOp == 1)
        {
            strTabla = " tbl_GestionOperador AS G LEFT OUTER JOIN tbl_SeguimientoOperador AS S ON dbo.OperadorRevision(G.geop_Id) = S.seop_Id LEFT OUTER JOIN Seg_Usuario AS U ON G.segusu_Id = U.segusu_Id LEFT OUTER JOIN Seg_Usuario AS U2 ON S.segusu_Id = U2.segusu_Id\tLEFT OUTER JOIN tbl_Tipo AS T ON S.tip_Id = T.tip_Id LEFT OUTER JOIN tbl_Estado AS E ON S.est_Id = E.est_Id ";
            text = string.Empty;
        }
        if (intOp == 2)
        {
            text = " G.geop_Id = @intIdGestionOperador ";
            text += " group by G.geop_Id, G.geop_rutaDocumento , G.fecha, G.geop_NombreDocumento , G.geop_extDocumento , G.segusu_Id , U.segusu_login , G.geop_Observaciones , S.seop_Id, S.fecha , S.segusu_Id, E.est_Descripcion, U2.segusu_login , S.tip_Id , T.tip_Descripcion , S.seop_Observacion , S.est_Id, S.seop_RutaDocumento , S.seop_NombreDocumento, S.seop_ExtDocumento, dbo.OperadorRevision(G.geop_Id) , O.gruOpe_Nombre ";
        }
        if (intOp == 3)
        {
            strTabla = " tbl_GestionOperador AS G LEFT OUTER JOIN tbl_SeguimientoOperador AS S ON dbo.OperadorRevision(G.geop_Id) = S.seop_Id LEFT OUTER JOIN Seg_Usuario AS U ON G.segusu_Id = U.segusu_Id LEFT OUTER JOIN Seg_Usuario AS U2 ON S.segusu_Id = U2.segusu_Id\tLEFT OUTER JOIN tbl_Tipo AS T ON S.tip_Id = T.tip_Id LEFT OUTER JOIN tbl_Estado AS E ON S.est_Id = E.est_Id INNER JOIN tbl_GrupoOperador AS O ON G.gruOpe_Id = O.gruOpe_Id INNER JOIN  tbl_GrupoUsuario AS GU ON O.gruOpe_Id = GU.gruOpe_Id ";
            text = " GU.segUsu_Id = @intIdUsuarioOperador ";
        }
        if (intOp == 4)
        {
            strCampos = " G.geop_Id, G.geop_rutaDocumento RUTA, G.fecha, G.geop_NombreDocumento NombreDocumento, G.geop_extDocumento extDocumento, G.segusu_Id IdUsuarioOperador, U.segusu_login usuarioOperador, G.geop_Observaciones Observaciones, S.seop_Id, S.fecha AS fechaRevision, S.segusu_Id AS IdUsuarioRevision, U2.segusu_login usuarioRevision, S.tip_Id IdTipoActividadRealizada, T.tip_Descripcion AccionRealizada, S.seop_Observacion ObservacionRevision, S.est_Id, CASE WHEN E.est_Descripcion IS NULL THEN 'POR REVISAR' ELSE E.est_Descripcion END EstadoRevision, S.seop_RutaDocumento , S.seop_NombreDocumento, S.seop_ExtDocumento, G.geop_Id IdSeguimientoOperador, O.gruOpe_Nombre ";
            strTabla = " tbl_GestionOperador AS G LEFT OUTER JOIN tbl_SeguimientoOperador AS S ON G.geop_Id = S.seop_Id LEFT OUTER JOIN Seg_Usuario AS U ON G.segusu_Id = U.segusu_Id LEFT OUTER JOIN Seg_Usuario AS U2 ON S.segusu_Id = U2.segusu_Id\tLEFT OUTER JOIN tbl_Tipo AS T ON S.tip_Id = T.tip_Id LEFT OUTER JOIN tbl_Estado AS E ON S.est_Id = E.est_Id INNER JOIN tbl_GrupoOperador AS O ON G.gruOpe_Id = O.gruOpe_Id ";
            text = string.Empty;
            if (strNombreArchivo != string.Empty)
            {
                text += " G.geop_NombreDocumento LIKE @strNombreArchivo  ";
            }
            if (strObservacionArchivo != string.Empty)
            {
                if (text != string.Empty)
                {
                    text += " AND G.geop_Observaciones LIKE @strObservacionArchivo ";
                }
                else
                {
                    text += " G.geop_Observaciones LIKE @strObservacionArchivo ";
                }
            }
            if (strNombreGrupo != string.Empty)
            {
                if (text != string.Empty)
                {
                    text += " AND O.gruOpe_Nombre LIKE @strNombreGrupo ";
                }
                else
                {
                    text += " O.gruOpe_Nombre LIKE @strNombreGrupo ";
                }
            }
            if (strEstado != string.Empty)
            {
                strTabla = " tbl_GestionOperador AS G LEFT OUTER JOIN tbl_SeguimientoOperador AS S ON dbo.OperadorRevision(G.geop_Id) = S.seop_Id LEFT OUTER JOIN Seg_Usuario AS U ON G.segusu_Id = U.segusu_Id LEFT OUTER JOIN Seg_Usuario AS U2 ON S.segusu_Id = U2.segusu_Id\tLEFT OUTER JOIN tbl_Tipo AS T ON S.tip_Id = T.tip_Id LEFT OUTER JOIN tbl_Estado AS E ON S.est_Id = E.est_Id INNER JOIN tbl_GrupoOperador AS O ON G.gruOpe_Id = O.gruOpe_Id INNER JOIN  tbl_GrupoUsuario AS GU ON O.gruOpe_Id = GU.gruOpe_Id ";
                if (strEstado == "Por revisar")
                {
                    text = " E.est_Descripcion is null ";
                }
                else
                {
                    text = " E.est_Descripcion = @strEstado ";
                }
            }
            if (strFechaDesde != string.Empty)
            {
                if (text != string.Empty)
                {
                    text += " AND G.fecha >= @strFechaDesde ";
                }
                else
                {
                    text += " G.fecha >= @strFechaDesde ";
                }
            }
            if (strFechaHasta != string.Empty)
            {
                if (text != string.Empty)
                {
                    text += " AND G.fecha <= @strFechaHasta ";
                }
                else
                {
                    text += " G.fecha <= @strFechaHasta ";
                }
            }
        }
        if (intOp == 5)
        {
            strTabla = " tbl_GestionOperador AS G LEFT OUTER JOIN tbl_SeguimientoOperador AS S ON dbo.OperadorRevision(G.geop_Id) = S.seop_Id LEFT OUTER JOIN Seg_Usuario AS U ON G.segusu_Id = U.segusu_Id LEFT OUTER JOIN Seg_Usuario AS U2 ON S.segusu_Id = U2.segusu_Id\tLEFT OUTER JOIN tbl_Tipo AS T ON S.tip_Id = T.tip_Id LEFT OUTER JOIN tbl_Estado AS E ON S.est_Id = E.est_Id ";
            text = " AND gu.segUsu_Supervizar = 1 ";
            if (intIdTipoActividad != 0)
            {
                text += " AND S.tip_Id = @intIdTipoActividad ";
            }
            if (strIdEstado != string.Empty)
            {
                text += " AND S.est_Id = @strIdEstado ";
            }
            if (strObservacionArchivo != string.Empty)
            {
                text += " AND S.seop_Observacion LIKE @strObservacionArchivo ";
            }
        }
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaGestionOperadorSeguimiento(string dataset, int intOp, int intIdUsuarioOperador, int intIdGestionOperador, string strNombreArchivo, string strObservacionArchivo, string strActividad, string strIdEstado, string strNombreGrupo)
    {
        string strCampos = " G.geop_Id, G.geop_rutaDocumento RUTA, G.fecha, G.geop_NombreDocumento NombreDocumento, G.geop_extDocumento extDocumento, S.segusu_Id IdUsuarioOperador, U.segusu_login usuarioOperador, G.geop_Observaciones Observaciones, S.seop_Id, S.fecha AS fechaRevision, S.segusu_Id AS IdUsuarioRevision, U2.segusu_login usuarioRevision, S.tip_Id IdTipoActividadRealizada, T.tip_Descripcion AccionRealizada, S.seop_Observacion ObservacionRevision, S.est_Id, CASE WHEN E.est_Descripcion IS NULL THEN 'POR REVISAR' ELSE E.est_Descripcion END EstadoRevision, S.seop_RutaDocumento , S.seop_NombreDocumento, S.seop_ExtDocumento, dbo.OperadorRevision(G.geop_Id) IdSeguimientoOperador, O.gruOpe_Nombre ";
        string strTabla = " tbl_GestionOperador AS G LEFT OUTER JOIN tbl_SeguimientoOperador AS S ON G.geop_Id = S.geop_Id LEFT OUTER JOIN Seg_Usuario AS U ON S.segusu_Id = U.segusu_Id LEFT OUTER JOIN Seg_Usuario AS U2 ON S.segusu_Id = U2.segusu_Id\tLEFT OUTER JOIN tbl_Tipo AS T ON S.tip_Id = T.tip_Id LEFT OUTER JOIN tbl_Estado AS E ON S.est_Id = E.est_Id INNER JOIN tbl_GrupoOperador AS O ON G.gruOpe_Id = O.gruOpe_Id INNER JOIN  tbl_GrupoUsuario AS GU ON O.gruOpe_Id = GU.gruOpe_Id ";
        string text = " GU.segUsu_Id = @intIdUsuarioOperador AND gu.segUsu_Supervizar = 1";
        string strOrden = " G.fecha desc ";
        base.clearParameters();
        base.setParameters("@intIdUsuarioOperador", System.Data.SqlDbType.Int, intIdUsuarioOperador);
        base.setParameters("@intIdGestionOperador", System.Data.SqlDbType.Int, intIdGestionOperador);
        base.setParameters("@strNombreArchivo", System.Data.SqlDbType.NVarChar, "%" + strNombreArchivo + "%");
        base.setParameters("@strObservacionArchivo", System.Data.SqlDbType.NVarChar, "%" + strObservacionArchivo + "%");
        base.setParameters("@strActividad", System.Data.SqlDbType.NVarChar, strActividad);
        base.setParameters("@strIdEstado", System.Data.SqlDbType.NVarChar, strIdEstado);
        base.setParameters("@strNombreGrupo", System.Data.SqlDbType.NVarChar, strNombreGrupo);
        if (intOp == 1)
        {
            strTabla = " tbl_GestionOperador AS G LEFT OUTER JOIN tbl_SeguimientoOperador AS S ON dbo.OperadorRevision(G.geop_Id) = S.seop_Id LEFT OUTER JOIN Seg_Usuario AS U ON G.segusu_Id = U.segusu_Id LEFT OUTER JOIN Seg_Usuario AS U2 ON S.segusu_Id = U2.segusu_Id\tLEFT OUTER JOIN tbl_Tipo AS T ON S.tip_Id = T.tip_Id LEFT OUTER JOIN tbl_Estado AS E ON S.est_Id = E.est_Id ";
            text = string.Empty;
        }
        if (intOp == 2)
        {
            text = " G.geop_Id = @intIdGestionOperador AND gu.segUsu_Supervizar = 1 ";
            text += " group by G.geop_Id, G.geop_rutaDocumento , G.fecha, G.geop_NombreDocumento , G.geop_extDocumento , G.segusu_Id , U.segusu_login , G.geop_Observaciones , S.seop_Id, S.fecha , S.segusu_Id, E.est_Descripcion, U2.segusu_login , S.tip_Id , T.tip_Descripcion , S.seop_Observacion , S.est_Id, S.seop_RutaDocumento , S.seop_NombreDocumento, S.seop_ExtDocumento, dbo.OperadorRevision(G.geop_Id) , O.gruOpe_Nombre ";
        }
        if (intOp == 3)
        {
            strCampos = " G.geop_Id, G.geop_rutaDocumento RUTA, G.fecha, G.geop_NombreDocumento NombreDocumento, G.geop_extDocumento extDocumento, S.segusu_Id IdUsuarioOperador, U.segusu_login usuarioOperador, G.geop_Observaciones Observaciones, S.seop_Id, S.fecha AS fechaRevision, S.segusu_Id AS IdUsuarioRevision, U2.segusu_login usuarioRevision, S.tip_Id IdTipoActividadRealizada, T.tip_Descripcion AccionRealizada, S.seop_Observacion ObservacionRevision, S.est_Id, CASE WHEN E.est_Descripcion IS NULL THEN 'POR REVISAR' ELSE E.est_Descripcion END EstadoRevision, S.seop_RutaDocumento , S.seop_NombreDocumento, S.seop_ExtDocumento, dbo.OperadorRevision(G.geop_Id) IdSeguimientoOperador, O.gruOpe_Nombre ";
            strTabla = " tbl_GestionOperador AS G LEFT OUTER JOIN tbl_SeguimientoOperador AS S ON dbo.OperadorRevision(G.geop_Id) = S.seop_Id LEFT OUTER JOIN Seg_Usuario AS U ON S.segusu_Id = U.segusu_Id LEFT OUTER JOIN Seg_Usuario AS U2 ON S.segusu_Id = U2.segusu_Id\tLEFT OUTER JOIN tbl_Tipo AS T ON S.tip_Id = T.tip_Id LEFT OUTER JOIN tbl_Estado AS E ON S.est_Id = E.est_Id INNER JOIN tbl_GrupoOperador AS O ON G.gruOpe_Id = O.gruOpe_Id INNER JOIN  tbl_GrupoUsuario AS GU ON O.gruOpe_Id = GU.gruOpe_Id ";
            text = " GU.segUsu_Id = @intIdUsuarioOperador AND gu.segUsu_Supervizar = 1 ";
        }
        if (intOp == 4)
        {
            strCampos = " G.geop_Id, G.geop_rutaDocumento RUTA, G.fecha, G.geop_NombreDocumento NombreDocumento, G.geop_extDocumento extDocumento, G.segusu_Id IdUsuarioOperador, U.segusu_login usuarioOperador, G.geop_Observaciones Observaciones, S.seop_Id, S.fecha AS fechaRevision, S.segusu_Id AS IdUsuarioRevision, U2.segusu_login usuarioRevision, S.tip_Id IdTipoActividadRealizada, T.tip_Descripcion AccionRealizada, S.seop_Observacion ObservacionRevision, S.est_Id, CASE WHEN E.est_Descripcion IS NULL THEN 'POR REVISAR' ELSE E.est_Descripcion END EstadoRevision, S.seop_RutaDocumento , S.seop_NombreDocumento, S.seop_ExtDocumento, G.geop_Id IdSeguimientoOperador, O.gruOpe_Nombre ";
            strTabla = " tbl_GestionOperador AS G LEFT OUTER JOIN tbl_SeguimientoOperador AS S ON G.geop_Id = S.seop_Id LEFT OUTER JOIN Seg_Usuario AS U ON G.segusu_Id = U.segusu_Id LEFT OUTER JOIN Seg_Usuario AS U2 ON S.segusu_Id = U2.segusu_Id\tLEFT OUTER JOIN tbl_Tipo AS T ON S.tip_Id = T.tip_Id LEFT OUTER JOIN tbl_Estado AS E ON S.est_Id = E.est_Id INNER JOIN tbl_GrupoOperador AS O ON G.gruOpe_Id = O.gruOpe_Id ";
            text = string.Empty;
            if (strNombreArchivo != string.Empty)
            {
                text += " G.geop_NombreDocumento LIKE @strNombreArchivo AND gu.segUsu_Supervizar = 1  ";
            }
            if (strObservacionArchivo != string.Empty)
            {
                if (text != string.Empty)
                {
                    text += " AND G.geop_Observaciones LIKE @strObservacionArchivo ";
                }
                else
                {
                    text += " G.geop_Observaciones LIKE @strObservacionArchivo AND gu.segUsu_Supervizar = 1 ";
                }
            }
        }
        if (intOp == 5)
        {
            strTabla = "    tbl_GestionOperador AS G LEFT OUTER JOIN (select * from tbl_SeguimientoOperador AS s where S.seop_Id = dbo.OperadorRevision(S.geop_Id)) AS S ON  G.geop_Id = S.geop_Id  LEFT OUTER JOIN Seg_Usuario AS U ON G.segusu_Id = U.segusu_Id LEFT OUTER JOIN Seg_Usuario AS U2 ON S.segusu_Id = U2.segusu_Id\tLEFT OUTER JOIN tbl_Tipo AS T ON S.tip_Id = T.tip_Id LEFT OUTER JOIN tbl_Estado AS E ON S.est_Id = E.est_Id INNER JOIN tbl_GrupoOperador AS O ON G.gruOpe_Id = O.gruOpe_Id INNER JOIN  tbl_GrupoUsuario AS GU ON O.gruOpe_Id = GU.gruOpe_Id ";
            if (strNombreGrupo != string.Empty)
            {
                text += " AND O.gruOpe_Nombre = @strNombreGrupo ";
            }
            if (strIdEstado != string.Empty)
            {
                if (strIdEstado == "Por revisar")
                {
                    text += " AND E.est_Descripcion is null ";
                }
                else
                {
                    text += " AND E.est_Descripcion = @strIdEstado ";
                }
            }
            if (strActividad != string.Empty)
            {
                if (strActividad == "Sin Actividad")
                {
                    text += " AND T.tip_Descripcion is null ";
                }
                else
                {
                    text += " AND T.tip_Descripcion = @strActividad ";
                }
            }
        }
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTablas(string dataset, int intActivacion, int intOP, string strNombreTabla, int intIdTabla)
    {
        string lstrCampos = " T.admTab_Id, SUBSTRING(T.admTab_NombreTabla,6,50) admTab_NombreTabla, T.admTab_AbreviaturaTabla, T.admTab_NumeroCampos, T.admTab_fecha, T.segusu_Id, T.admTab_Activacion, T.admTab_Observacion, U.segusu_login ";
        string lstrTabla = " Seg_AdmTablas T, Seg_Usuario U ";
        string lstrCondicion = " T.segusu_Id = U.segusu_Id AND admTab_Activacion = @intActivacion ";
        string lstrOrden = " admTab_NombreTabla ";

        this.clearParameters();
        base.setParameters("@intActivacion", SqlDbType.Int, intActivacion);
        base.setParameters("@strNombreTabla", SqlDbType.NVarChar, "%" + strNombreTabla + "%");
        base.setParameters("@intIdTabla", SqlDbType.Int, intIdTabla);

        if (intOP == 1)
        {
            lstrCondicion = string.Empty;
            if (intActivacion != 3)
            {
                lstrCondicion = " T.segusu_Id = U.segusu_Id AND admTab_Activacion = @intActivacion ";
                if (strNombreTabla != string.Empty)
                {
                    lstrCondicion = lstrCondicion + " AND admTab_NombreTabla LIKE @strNombreTabla ";
                }
            }
            else
            {
                if (strNombreTabla != string.Empty)
                {
                    lstrCondicion = " T.segusu_Id = U.segusu_Id AND  admTab_NombreTabla LIKE @strNombreTabla ";
                }

            }
        }


        if (intOP == 2)
        {
            lstrCampos = " T.admTab_Id, SO.NAME, T.admTab_AbreviaturaTabla, T.admTab_Observacion, T.admTab_fecha, SC.column_id, SC.name AS Campos, ST.name AS TipoDato, SC.max_length, SC.precision, SC.scale, CASE WHEN SC.is_nullable = 1 THEN 'SI' ELSE 'NO' END is_nullable ";
            lstrTabla = " sys.objects SO INNER JOIN sys.columns SC ON SO.OBJECT_ID = SC.OBJECT_ID INNER JOIN SYS.types ST ON SC.system_type_id = ST.system_type_id INNER JOIN Seg_AdmTablas T ON SO.NAME = T.admTab_NombreTabla ";
            lstrCondicion = " SO.TYPE = 'U' AND T.admTab_Id = @intIdTabla AND ST.name NOT IN ('sysname') AND SC.column_id NOT IN (1)  ";
            lstrOrden = " SC.column_id ";
        }


        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntEliminarLoteCargado(string nombreTabla, string lote)
    {
        string lstrTabla = " " + nombreTabla + " ";
        string lstrCondicion = " fechaCarga = @fechaCarga ";

        this.clearParameters();
        setParameters("@fechaCarga", SqlDbType.NVarChar, lote);

        if (Eliminar(lstrTabla, lstrCondicion, false))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Boolean fntConsultaLotesCargados(string dataset, string nombreTabla)
    {
        string lstrCampos = " fechaCarga ";
        string lstrTabla = " " + nombreTabla + " ";
        string lstrCondicion = string.Empty;
        string groupBy = " fechaCarga ";
        string lstrOrden = " fechaCarga DESC ";

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, groupBy, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    public Boolean fntConsultaDetalleLote(string dataset, string nombreTabla, string fechaCarga)
    {
        string lstrCampos = " * ";
        string lstrTabla = " " + nombreTabla + " ";
        string lstrCondicion = " fechaCarga = @fechaCarga";
        string lstrOrden = string.Empty;

        base.clearParameters();
        base.setParameters("@fechaCarga", SqlDbType.NVarChar, fechaCarga);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public bool fntConsultaCampoTablas(string dataset, string strNombreTabla)
    {
        string lstrCampos = " SO.NAME, SC.NAME, t.Name ";
        string lstrTabla = " sys.objects SO INNER JOIN sys.columns SC ON SO.OBJECT_ID = SC.OBJECT_ID INNER JOIN  sys.types t ON sc.user_type_id = t.user_type_id";
        string lstrCondicion = " SO.TYPE = 'U' AND SO.NAME = @strNombreTabla ";
        string lstrOrden = string.Empty;

        base.clearParameters();
        base.setParameters("@strNombreTabla", SqlDbType.NVarChar, strNombreTabla);


        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }
    public bool fntConsultaDocumentosCargados(string dataset, int intOP, string strDocumento, int intIdUsuario)
    {
        string strCampos = " * ";
        string strTabla = " dbo.Seg_CargaDocumento c, Seg_Usuario U ";
        string text = " C.segusu_Id = u.segusu_Id ";
        string strOrden = " c.carDoc_FechaCarga ";
        base.clearParameters();
        base.setParameters("@strDocumento", System.Data.SqlDbType.NVarChar, "%" + strDocumento + "%");
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        if (intOP == 1)
        {
            if (strDocumento != string.Empty)
            {
                text += " AND C.carDoc_NombreDocumento LIKE @strDocumento ";
            }
            if (intIdUsuario != 0)
            {
                text += " AND U.segusu_Id = @intIdUsuario ";
            }
        }
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaCantidadSeguimientoOperador(string dataset, int intGestionOperador)
    {
        string strCampos = " COUNT(*) cantidad ";
        string strTabla = " tbl_SeguimientoOperador ";
        string strCondicion = " geop_Id in (select geop_Id from tbl_GestionOperador where geop_Id = @intGestionOperador) ";
        string strOrden = " cantidad ";
        base.clearParameters();
        base.setParameters("@intGestionOperador", System.Data.SqlDbType.Int, intGestionOperador);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaCantidadRegistrosTablaDinamica(string dataset, string strTablaDinamica)
    {
        string strCampos = " COUNT(*) Cantidad ";
        string empty = string.Empty;
        string strOrden = " Cantidad ";
        return base.Consultar(strCampos, strTablaDinamica, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaUsuarioAdmin(string dataset, int intOp, int intIdUsuario, int intTipoConexion, string strUsuario, int intIdEstado, string correoUsuario)
    {
        string strCampos = " U.segusu_Id, U.segcon_Id, C.segCon_Descripcion, U.segusu_login, U.segusu_Password, U.est_Id, E.est_Descripcion, U.segusu_IdCreacion, U.segusu_FechaCreacion, U.per_Id, u.segusu_Correo ";
        string strTabla = " Seg_Usuario U, tbl_estado E, Seg_Conexion C ";
        string text = " u.est_Id = E.est_Id AND U.segcon_Id = C.segcon_Id AND U.segApl_id = 1 ";
        string strOrden = " segusu_login ";
        base.clearParameters();
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        base.setParameters("@intTipoConexion", System.Data.SqlDbType.Int, intTipoConexion);
        base.setParameters("@strUsuario", System.Data.SqlDbType.NVarChar, "%" + strUsuario + "%");
        base.setParameters("@strCorreo", System.Data.SqlDbType.NVarChar, "%" + correoUsuario + "%");
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        if (intOp == 1)
        {
            text += " AND U.segusu_Id = @intIdUsuario ";
        }
        if (intOp == 2)
        {
            if (intTipoConexion > 0)
            {
                text += " AND U.segcon_Id = @intTipoConexion ";
            }
            if (strUsuario != string.Empty)
            {
                text += " AND U.segusu_login LIKE @strUsuario ";
            }
            if (intIdEstado > 0)
            {
                text += " AND U.est_Id = @intIdEstado ";
            }
            if (correoUsuario != string.Empty)
            {
                text += " AND U.segusu_Correo LIKE @strCorreo ";
            }
        }
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaFormatos(string dataset)
    {
        string strCampos = " F.for_Id, F.for_Descripcion, F.for_Documento, F.for_Actividad, F.com_Id, C.com_Nombre ";
        string strTabla = " Seg_Formato F, tbl_Componente C ";
        string strCondicion = " F.com_Id = C.com_Id AND F.for_Actividad = 1 ";
        string strOrden = " C.com_Nombre ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaManuales(string dataset)
    {
        string strCampos = " F.for_Id, F.for_Descripcion, F.for_Documento, F.for_Actividad, F.com_Id ";
        string strTabla = " Seg_Formato F ";
        string strCondicion = " F.for_Actividad = 1 AND F.com_Id = 0 ";
        string strOrden = " F.for_Actividad ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaExpresionSeguridad(string dataset, string strTextoPalabra)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " Seg_ExpresionCampo ";
        string strCondicion = " exca_Texto = @strTextoPalabra AND (exca_Texto LIKE '%SELECT%' OR exca_Texto LIKE '%FROM%' OR exca_Texto LIKE '%WHERE%' OR exca_Texto LIKE '%DELETE%' OR exca_Texto LIKE '%ORDER BY%' OR exca_Texto LIKE '%CREATE%' OR exca_Texto LIKE '%DROP%' OR exca_Texto LIKE '%GROUP%') ";
        string strOrden = " CANTIDAD ";
        base.clearParameters();
        base.setParameters("@strTextoPalabra", System.Data.SqlDbType.NVarChar, strTextoPalabra);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTipoConexion(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " Seg_Conexion ";
        string strCondicion = " 1 = 1  ";
        string strOrden = " segCon_Descripcion ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaEstadoTipoUsuario(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Estado E, tbl_Tipo T ";
        string strCondicion = " e.tip_Id = T.tip_Id AND T.tip_Descripcion = 'Usuario'  ";
        string strOrden = " est_Descripcion ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaEstadoBiblioteca(string dataset, string strEstado)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Estado E, tbl_Tipo T ";
        string text = " e.tip_Id = T.tip_Id AND T.tip_Categoria = 'Bibliotecas'  ";
        string strOrden = " est_Descripcion ";
        if (strEstado != string.Empty)
        {
            text += " AND est_Descripcion = @strEstado ";
        }
        base.clearParameters();
        base.setParameters("@strEstado", System.Data.SqlDbType.NVarChar, strEstado);
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaSoloUsuarioCantidad_bol(string dataset, string strUsuario)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " Seg_Usuario u";
        string strCondicion = "segusu_login = @strUsuario ";
        string strOrden = " CANTIDAD ";
        base.clearParameters();
        base.setParameters("@strUsuario", System.Data.SqlDbType.NVarChar, strUsuario);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaPermisoUsuarioFormulario_bol(string dataset, int intIdUsuario)
    {
        string strCampos = " U.segusu_Id IdUsuario, P.segusuper_Id , P.segfor_Id IdFormulario, F.segfor_DescripcionMenu, CASE WHEN P.segusuper_Consulta = 1 THEN 'SI' ELSE 'NO' END Consultar, CASE WHEN P.segusuper_Creacion = 1 THEN 'SI' ELSE 'NO' END Crear, CASE WHEN P.segusuper_Modificar = 1 THEN 'SI' ELSE 'NO' END Modificar, CASE WHEN P.segusuper_Eliminar = 1 THEN 'SI' ELSE 'NO' END Eliminar, CASE WHEN P.segusuper_Impresion = 1 THEN 'SI' ELSE 'NO' END Imprimir ";
        string strTabla = " Seg_Usuario U, Seg_UsuarioPermiso P, Seg_Formulario F ";
        string strCondicion = " U.segusu_Id = P.segusu_Id AND P.segfor_Id = F.segfor_Id AND U.segusu_Id = @intIdUsuario ";
        string strOrden = " F.segfor_origen, F.segfor_Orden ";
        base.clearParameters();
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaFormularioNoAsignados_bol(string dataset, int intIdUsuario)
    {
        string strCampos = " * ";
        string strTabla = " Seg_Formulario F ";
        string strCondicion = " F.segfor_Id NOT IN (SELECT P.segfor_Id FROM Seg_Usuario U, Seg_UsuarioPermiso P, Seg_Formulario F WHERE U.segusu_Id = P.segusu_Id AND P.segfor_Id = F.segfor_Id AND U.segusu_Id = @intIdUsuario) AND F.segApl_id = 1 ";
        string strOrden = " f.segfor_Nombre ";
        base.clearParameters();
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaMaxIdTablaDinamica_bol(string dataset, string strTabla)
    {
        string strCampos = " CASE WHEN max(id) IS NULL THEN 1 ELSE max(id) + 1 END ID ";
        string empty = string.Empty;
        string strOrden = " ID ";
        return base.Consultar(strCampos, strTabla, empty, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTelefonosPersona_bol(string dataset, int intIdPersona)
    {
        string strCampos = " Tl.tel_Id, Tl.per_Id, Tl.numTelefono, Tl.tip_IdTipoTelefono, Tp.tip_Descripcion AS Tipo, T.tip_Id AS Procedencia, T.tip_Descripcion DescProcedencia, T.tip_Categoria  ";
        string strTabla = " tbl_Telefono Tl, tbl_Tipo T, tbl_Tipo Tp ";
        string strCondicion = " tl.tip_Id = T.tip_Id AND Tl.tip_IdTipoTelefono = TP.tip_Id AND Tl.per_Id = @intIdPersona ";
        string strOrden = " Tp.tip_Descripcion, Tl.numTelefono ";
        base.clearParameters();
        base.setParameters("@intIdPersona", System.Data.SqlDbType.Int, intIdPersona);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaCorreoPersona_bol(string dataset, int intIdPersona)
    {
        string strCampos = " C.corele_Id, C.per_Id, C.direccionCorreoElectronico, C.tip_Id, T.tip_Descripcion ";
        string strTabla = " tbl_CorreoElectronico C, tbl_Tipo T ";
        string strCondicion = " C.tip_Id = T.tip_Id AND C.per_Id = @intIdPersona ";
        string strOrden = " C.tip_Id, C.direccionCorreoElectronico ";
        base.clearParameters();
        base.setParameters("@intIdPersona", System.Data.SqlDbType.Int, intIdPersona);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaGrupoOperador_bol(string dataset, int intOp, int intIdGrupoOperador, string strNombreGrupo, string strDescripcionGrupo, int intIdEstado)
    {
        string strCampos = " G.gruOpe_Id, G.gruOpe_Nombre, G.gruOpe_Descripcion, G.gruOpe_FechaCreacion, G.segUsu_Id, G.est_Id, e.est_Descripcion  ";
        string strTabla = " tbl_GrupoOperador G, tbl_Estado E ";
        string text = " g.est_Id = e.est_Id ";
        string strOrden = " G.gruOpe_Nombre ";
        base.clearParameters();
        base.setParameters("@intIdGrupoOperador", System.Data.SqlDbType.Int, intIdGrupoOperador);
        base.setParameters("@strNombreGrupo", System.Data.SqlDbType.NVarChar, "%" + strNombreGrupo + "%");
        base.setParameters("@strDescripcionGrupo", System.Data.SqlDbType.NVarChar, "%" + strDescripcionGrupo + "%");
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        if (intOp == 1)
        {
            text += " AND gruOpe_Id = @intIdGrupoOperador";
        }
        if (intOp == 2)
        {
            if (strNombreGrupo != string.Empty)
            {
                text += " AND G.gruOpe_Nombre LIKE @strNombreGrupo ";
            }
            if (strDescripcionGrupo != string.Empty)
            {
                text += " AND G.gruOpe_Descripcion LIKE @strDescripcionGrupo ";
            }
            if (intIdEstado > 0)
            {
                text += " AND G.est_Id = @intIdEstado ";
            }
        }
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaGrupoBiblioteca_bol(string dataset, int intOp, int intIdGrupoBiblioteca, string strNombreGrupoBiblioteca, string strDescripcionGrupoBiblioteca, int intIdEstado)
    {
        string strCampos = " B.grubib_Id, B.grubib_Nombre, B.grubib_Descripcion, B.grubib_FechaCreacion, B.SegUsu_Id, B.est_Id, E.est_Descripcion ";
        string strTabla = " tbl_GrupoBiblioteca B, tbl_Estado E ";
        string text = " B.est_Id = E.est_Id ";
        string strOrden = " B.grubib_Nombre ";
        base.clearParameters();
        base.setParameters("@intIdGrupoBiblioteca", System.Data.SqlDbType.Int, intIdGrupoBiblioteca);
        base.setParameters("@strNombreGrupoBiblioteca", System.Data.SqlDbType.NVarChar, "%" + strNombreGrupoBiblioteca + "%");
        base.setParameters("@strDescripcionGrupoBiblioteca", System.Data.SqlDbType.NVarChar, "%" + strDescripcionGrupoBiblioteca + "%");
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        if (intOp == 1)
        {
            text += " AND grubib_Id = @intIdGrupoBiblioteca";
        }
        if (intOp == 2)
        {
            if (strNombreGrupoBiblioteca != string.Empty)
            {
                text += " AND B.grubib_Nombre LIKE @strNombreGrupoBiblioteca ";
            }
            if (strDescripcionGrupoBiblioteca != string.Empty)
            {
                text += " AND B.grubib_Descripcion LIKE @strDescripcionGrupoBiblioteca ";
            }
            if (intIdEstado > 0)
            {
                text += " AND B.est_Id = @intIdEstado ";
            }
        }
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaBibliotecaCarpeta_bol(string dataset, int intIdGrupoBiblioteca)
    {
        string strCampos = " * ";
        string strTabla = " tbl_BibliotecaCarpetas ";
        string strCondicion = " grubib_Id = @intIdGrupoBiblioteca ";
        string strOrden = " bibCar_FechaCreacion ";
        base.clearParameters();
        base.setParameters("@intIdGrupoBiblioteca", System.Data.SqlDbType.Int, intIdGrupoBiblioteca);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaExistenciaGrupoOperador_bol(string dataset, string strGrupoOperador)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " tbl_GrupoOperador G ";
        string strCondicion = " g.gruOpe_Nombre = @strGrupoOperador ";
        string strOrden = " CANTIDAD ";
        base.clearParameters();
        base.setParameters("@strGrupoOperador", System.Data.SqlDbType.NVarChar, strGrupoOperador);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaExistenciaGrupoBiblioteca_bol(string dataset, string strGrupoBiblioteca)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " tbl_GrupoBiblioteca B ";
        string strCondicion = " B.grubib_Nombre = @strGrupoBiblioteca ";
        string strOrden = " CANTIDAD ";
        base.clearParameters();
        base.setParameters("@strGrupoBiblioteca", System.Data.SqlDbType.NVarChar, strGrupoBiblioteca);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaCantdadDocumentosGrupoOperador_bol(string dataset, int intIdGrupoOperador)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " tbl_GrupoOperador g, tbl_GestionOperador O ";
        string strCondicion = " g.gruOpe_Id = o.gruOpe_Id AND g.gruOpe_Id = @intIdGrupoOperador ";
        string strOrden = " CANTIDAD ";
        base.clearParameters();
        base.setParameters("@intIdGrupoOperador", System.Data.SqlDbType.Int, intIdGrupoOperador);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaCantdadDocumentosGrupoBiblioteca_bol(string dataset, int intIdGrupoBiblioteca)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " tbl_GrupoBiblioteca GB, tbl_Biblioteca B ";
        string strCondicion = " gb.grubib_Id = B.grubib_Id AND gb.grubib_Id = @intIdGrupoBiblioteca ";
        string strOrden = " CANTIDAD ";
        base.clearParameters();
        base.setParameters("@intIdGrupoBiblioteca", System.Data.SqlDbType.Int, intIdGrupoBiblioteca);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaUsuarioGrupoOperador_bol(string dataset, int intIdGrupoOperador)
    {
        string strCampos = " GU.gruUsu_Id, GU.gruOpe_Id, GU.segUsu_Id, GU.gruUsu_FechaCreacion, U.segusu_login, U.est_Id, E.est_Descripcion, CASE WHEN gu.segUsu_Supervizar = 0 THEN 'NO' ELSE 'SI' END Supervisar  ";
        string strTabla = " tbl_GrupoUsuario GU, Seg_Usuario U, tbl_Estado E ";
        string strCondicion = " GU.segUsu_Id = U.segusu_Id AND U.est_Id = E.est_Id AND GU.gruOpe_Id = @intIdGrupoOperador ";
        string strOrden = " U.segusu_login ";
        base.clearParameters();
        base.setParameters("@intIdGrupoOperador", System.Data.SqlDbType.Int, intIdGrupoOperador);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaUsuarioGrupoBiblioteca_bol(string dataset, int intIdGrupoBiblioteca)
    {
        string strCampos = " B.bibUsu_Id, B.grubib_Id, B.segUsu_Id, B.bibUsu_FechaCreacion, U.segusu_login, U.est_Id, E.est_Descripcion ";
        string strTabla = " tbl_BibliotecaUsuario B, Seg_Usuario U, tbl_Estado E ";
        string strCondicion = " B.segUsu_Id = U.segUsu_Id AND U.est_Id = e.est_Id AND B.grubib_Id  = @intIdGrupoBiblioteca ";
        string strOrden = " U.segusu_login ";
        base.clearParameters();
        base.setParameters("@intIdGrupoBiblioteca", System.Data.SqlDbType.Int, intIdGrupoBiblioteca);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaUsuariosDisponiblesGrupoOperador_bol(string dataset, int intIdGrupoOperador)
    {
        string strCampos = " U.segusu_Id, U.segusu_login  ";
        string strTabla = " Seg_Usuario U, Seg_Aplicativo A ";
        string strCondicion = " U.segApl_id = A.segApl_id AND A.segApl_Nombre = 'SisPAICMA' AND U.segUsu_Id NOT IN (select  GU.segUsu_Id FROM tbl_GrupoUsuario GU, Seg_Usuario U WHERE GU.segUsu_Id = U.segusu_Id AND GU.gruOpe_Id = @intIdGrupoOperador) ";
        string strOrden = " U.segusu_login ";
        base.clearParameters();
        base.setParameters("@intIdGrupoOperador", System.Data.SqlDbType.Int, intIdGrupoOperador);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaUsuariosDisponiblesGrupoBiblioteca_bol(string dataset, int intIdGrupoBiblioteca)
    {
        string strCampos = " U.segusu_Id, U.segusu_login  ";
        string strTabla = " Seg_Usuario U, Seg_Aplicativo A ";
        string strCondicion = " U.segApl_id = A.segApl_id AND A.segApl_Nombre = 'SisPAICMA' AND U.segUsu_Id NOT IN (SELECT  B.segUsu_Id FROM tbl_BibliotecaUsuario B, Seg_Usuario U WHERE B.segUsu_Id = U.segUsu_Id AND B.grubib_Id  = @intIdGrupoBiblioteca) ";
        string strOrden = " U.segusu_login ";
        base.clearParameters();
        base.setParameters("@intIdGrupoBiblioteca", System.Data.SqlDbType.Int, intIdGrupoBiblioteca);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaGruposDisponiblesUsuario_bol(string dataset, int intIdUsuario)
    {
        string strCampos = " O.gruOpe_Id, O.gruOpe_Nombre ";
        string strTabla = " tbl_GrupoUsuario U, tbl_GrupoOperador O ";
        string strCondicion = " U.gruOpe_Id = O.gruOpe_Id AND U.segUsu_Id = @intIdUsuario  ";
        string strOrden = " O.gruOpe_Nombre ";
        base.clearParameters();
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaBibliotecasAsignadas_bol(string dataset, int intIdUsuario)
    {
        string strCampos = " B.bibUsu_Id, B.grubib_Id, B.bibUsu_FechaCreacion,  GP.grubib_Nombre, GP.grubib_Descripcion, GP.grubib_FechaCreacion, E.est_Id, E.est_Descripcion ";
        string strTabla = " tbl_BibliotecaUsuario B, tbl_GrupoBiblioteca GP, tbl_Estado E  ";
        string strCondicion = " B.grubib_Id = GP.grubib_Id AND GP.est_Id = E.est_Id AND E.est_Descripcion = 'ACTIVO' AND B.segUsu_Id = @intIdUsuario ";
        string strOrden = " B.bibUsu_FechaCreacion DESC, GP.grubib_Nombre ASC ";
        base.clearParameters();
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaDocumentosBiblioteca_bol(string dataset, int intIdGrupoBiblioteca, string strEstado, int intCodigoDocumento)
    {
        string strCampos = " B.bib_Id, B.grubib_Id, B.segusu_Id, B.bib_NombreDocumento, B.bib_ExtDocumento, B.bib_Observaciones, B.bib_FechaCreacion, E.est_Descripcion, U.segusu_login, b.bib_Version, bib_CodigoDocumento, bc.bibCar_Descripcion ";
        string strTabla = " tbl_Biblioteca AS B INNER JOIN tbl_Estado AS E ON B.est_Id = E.est_Id INNER JOIN Seg_Usuario AS U ON B.segusu_Id = U.segusu_Id LEFT OUTER JOIN tbl_BibliotecaCarpetas AS bc ON B.bibCar_Id = bc.bibCar_Id ";
        string text = " B.grubib_Id = @intIdGrupoBiblioteca ";
        string strOrden = " B.bib_FechaCreacion DESC, B.bib_NombreDocumento DESC";
        if (strEstado != string.Empty)
        {
            text += " AND E.est_Descripcion = @strEstado ";
        }
        if (intCodigoDocumento > 0)
        {
            text += " AND bib_CodigoDocumento = @intCodigoDocumento ";
        }
        base.clearParameters();
        base.setParameters("@intIdGrupoBiblioteca", System.Data.SqlDbType.Int, intIdGrupoBiblioteca);
        base.setParameters("@strEstado", System.Data.SqlDbType.NVarChar, strEstado);
        base.setParameters("@intCodigoDocumento", System.Data.SqlDbType.Int, intCodigoDocumento);
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaDatosVictima_bol(string dataset, int intOp, int intIdVictima)
    {
        string strCampos = " V.vic_Id, V.tip_Id, T.tip_Descripcion, V.identificacionNacionalPersona, V.nombrePersona, V.apellidoPersona, V.numTelefono, V.direccion, V.fecha, V.edad, V.tip_IdGenero, G.tip_Descripcion AS Genero, V.etn_Id, E.nomGrupoPoblacional, V.for_Id, F.nivelEducativoPersona ";
        string strTabla = " tbl_Victima AS V INNER JOIN tbl_Tipo AS T ON V.tip_Id = T.tip_Id INNER JOIN tbl_Tipo AS G ON V.tip_IdGenero = G.tip_Id INNER JOIN tbl_Formacion AS F ON V.for_Id = F.for_Id LEFT OUTER JOIN tbl_Etnia E ON V.etn_Id = E.etn_Id  ";
        string strCondicion = " 1=1 ";
        string strOrden = " V.nombrePersona ";
        base.clearParameters();
        base.setParameters("@intIdVictima", System.Data.SqlDbType.Int, intIdVictima);
        if (intOp == 1)
        {
            strCondicion = " V.vic_Id = @intIdVictima ";
        }
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaDatosVictimaAcabadaDeCrear_bol(string dataset, int intIdtipoIdentificacion, int intNumeroIdentificacion, string strNombre, string strApellido, string strTelefono, string strDireccion, string strFecha, int intEdad, int intIdGenero, int intIdEtnia, int intIdFormacion)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Victima V ";
        string strCondicion = " tip_Id = @intIdtipoIdentificacion AND identificacionNacionalPersona = @intNumeroIdentificacion AND nombrePersona = @strNombre AND apellidoPersona = @strApellido AND numTelefono = @strTelefono AND direccion = @strDireccion AND fecha = @strFecha AND edad = @intEdad AND tip_IdGenero = @intIdGenero AND etn_Id = @intIdEtnia AND for_Id = @intIdFormacion ";
        string strOrden = " V.nombrePersona  ";
        base.clearParameters();
        base.setParameters("@intIdtipoIdentificacion", System.Data.SqlDbType.Int, intIdtipoIdentificacion);
        base.setParameters("@intNumeroIdentificacion", System.Data.SqlDbType.Int, intNumeroIdentificacion);
        base.setParameters("@strNombre", System.Data.SqlDbType.NVarChar, strNombre);
        base.setParameters("@strApellido", System.Data.SqlDbType.NVarChar, strApellido);
        base.setParameters("@strTelefono", System.Data.SqlDbType.NVarChar, strTelefono);
        base.setParameters("@strDireccion", System.Data.SqlDbType.NVarChar, strDireccion);
        base.setParameters("@strFecha", System.Data.SqlDbType.NVarChar, strFecha);
        base.setParameters("@intEdad", System.Data.SqlDbType.Int, intEdad);
        base.setParameters("@intIdGenero", System.Data.SqlDbType.Int, intIdGenero);
        base.setParameters("@intIdEtnia", System.Data.SqlDbType.Int, intIdEtnia);
        base.setParameters("@intIdFormacion", System.Data.SqlDbType.Int, intIdFormacion);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaAccidenteVictima_bol(string dataset, int intIdVictima)
    {
        string strCampos = " monseg_Id, vic_Id, M.ver_Id, V.vereda, M.arepob_Id, A.arepob_Nombre, monseg_SitioAccidente,  monseg_Condicion, M.est_Id, E.est_Descripcion, monseg_ActividadAccidente, CASE WHEN monseg_AmputacionInicial = 1 THEN 'SI' ELSE 'NO' END AmputacionInicial, CASE WHEN monseg_AmputacionFin = 1 THEN 'SI' ELSE 'NO' END AmputacionFin, CASE WHEN monseg_Herida = 1 THEN 'SI' ELSE 'NO' END Herida, CASE WHEN monseg_Esquirla = 1 THEN 'SI' ELSE 'NO' END Esquirla, CASE WHEN monseg_Quemadura = 1 THEN 'SI' ELSE 'NO' END Quemadura, CASE WHEN monseg_Fractura = 1 THEN 'SI' ELSE 'NO' END Fractura, CASE WHEN monseg_Infeccion = 1 THEN 'SI' ELSE 'NO' END Infeccion, CASE WHEN monseg_AfectacionVisual = 1 THEN 'SI' ELSE 'NO' END AfectacionVisual, CASE WHEN monseg_AfectacionAuditiva = 1 THEN 'SI' ELSE 'NO' END AfectacionAuditiva, DiagnosticoMedico, monseg_DescripcionHechos, monseg_Gestion ";
        string strTabla = " tbl_MonitoreoSeguimiento M, tbl_Vereda V, tbl_AreaPoblacional A, tbl_Estado E ";
        string strCondicion = " M.ver_Id = V.ver_Id AND M.arepob_Id = A.arepob_Id AND M.est_Id = E.est_Id AND vic_Id = @intIdVictima ";
        string strOrden = " monseg_Id ";
        base.clearParameters();
        base.setParameters("@intIdVictima", System.Data.SqlDbType.Int, intIdVictima);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaAreaPoblacional_bol(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_AreaPoblacional ";
        string strCondicion = " 1 = 1 ";
        string strOrden = " arepob_Nombre ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaEstadoVictima_bol(string dataset)
    {
        string strCampos = " * ";
        string strTabla = " tbl_Estado E, tbl_Tipo T ";
        string strCondicion = " E.tip_Id = T.tip_Id AND tip_Categoria = 'victima' ";
        string strOrden = " est_Descripcion ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaRptOperador(string dataset, int intIdGrupoOperador)
    {
        string strCampos = " g.gruOpe_Id, g.gruOpe_Nombre, g.gruOpe_Descripcion, g.gruOpe_FechaCreacion, g.segUsu_Id, u.segusu_login,  g.est_Id, e.est_Descripcion, gop.geop_Id, gop.geop_rutaDocumento, gop.fecha, gop.geop_NombreDocumento, gop.geop_extDocumento,  gop.geop_Observaciones ";
        string strTabla = " tbl_GrupoOperador g, tbl_GrupoUsuario gu, Seg_Usuario u, tbl_GestionOperador gop, tbl_Estado e ";
        string text = " g.gruOpe_Id = gu.gruOpe_Id AND gu.segUsu_Id = u.segUsu_Id AND g.gruOpe_Id = gop.gruOpe_Id AND gop.segusu_Id = U.segusu_Id AND g.est_Id = e.est_Id  ";
        string strOrden = " G.gruOpe_Nombre ";
        if (intIdGrupoOperador > 0)
        {
            text += " AND g.gruOpe_Id = @intIdGrupoOperador ";
        }
        base.clearParameters();
        base.setParameters("@intIdGrupoOperador", System.Data.SqlDbType.Int, intIdGrupoOperador);
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaRptBiblioteca(string dataset, int intIdGrupoBiblioteca)
    {
        string strCampos = " GB.grubib_Id, GB.grubib_Nombre, GB.grubib_Descripcion, GB.grubib_FechaCreacion, GB.est_Id, E.est_Descripcion, B.bib_Id, B.bib_NombreDocumento, B.bib_ExtDocumento, B.bib_Observaciones, ED.est_Descripcion DescDocumento, B.bib_FechaCreacion, B.bib_Version, B.bib_CodigoDocumento, U.segusu_Id, U.segusu_login ";
        string strTabla = " tbl_GrupoBiblioteca GB, tbl_Biblioteca B, Seg_Usuario U, tbl_Estado E, tbl_Estado ED ";
        string text = " GB.grubib_Id = B.grubib_Id AND B.segusu_Id = U.segusu_Id AND GB.est_Id = E.est_Id AND B.est_Id = ED.est_Id ";
        string strOrden = " GB.grubib_Id ";
        if (intIdGrupoBiblioteca > 0)
        {
            text += " AND GB.grubib_Id = @intIdGrupoBiblioteca ";
        }
        base.clearParameters();
        base.setParameters("@intIdGrupoBiblioteca", System.Data.SqlDbType.Int, intIdGrupoBiblioteca);
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaCorreo(string dataset, int intIdCorreo)
    {
        string strCampos = " * ";
        string strTabla = " seg_Correo ";
        string strCondicion = " 1 = 1 ";
        string strOrden = " segCor_Id ";
        base.clearParameters();
        base.setParameters("@intIdCorreo", System.Data.SqlDbType.Int, intIdCorreo);
        if (intIdCorreo > 0)
        {
            strCondicion = " segCor_Id = @intIdCorreo ";
        }
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaTablaDinamicaRpt(string dataset, string strTabla, string strCampos)
    {
        string strCondicion = " 1 = 1 ";
        string empty = string.Empty;
        return base.Consultar(strCampos, strTabla, strCondicion, empty, ref dataset, false, "");
    }
    public bool fntConsultaGrupoFuncionariosOperadores(string dataset, int intIdGrupoOperador)
    {
        string strCampos = " gop.gruOpe_Nombre, gop.gruOpe_Descripcion, u.segusu_login,  u.est_Id, E.est_Descripcion, u.segusu_Correo ";
        string strTabla = " tbl_GrupoOperador gop, tbl_GrupoUsuario gu, Seg_Usuario u, tbl_Estado e  ";
        string strCondicion = " gop.gruOpe_Id = gu.gruOpe_Id AND gu.segUsu_Id = U.segUsu_Id AND U.est_Id = E.est_Id AND E.est_Descripcion = 'ACTIVO' AND gop.gruOpe_Id = @intIdGrupoOperador ";
        string strOrden = " u.segusu_Correo ";
        base.clearParameters();
        base.setParameters("@intIdGrupoOperador", System.Data.SqlDbType.Int, intIdGrupoOperador);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaGrupoFuncionariosDocumento(string dataset, int intIdGestionOperador)
    {
        string strCampos = " g.geop_NombreDocumento, gop.gruOpe_Descripcion, U.segusu_Correo ";
        string strTabla = " tbl_GestionOperador AS G, tbl_GrupoOperador gop, tbl_GrupoUsuario gu, Seg_Usuario u  ";
        string strCondicion = " g.gruOpe_Id = gop.gruOpe_Id AND gop.gruOpe_Id = gu.gruOpe_Id AND gu.segUsu_Id = U.segusu_Id AND geop_Id = @intIdGestionOperador ";
        string strOrden = " U.segusu_Correo ";
        base.clearParameters();
        base.setParameters("@intIdGestionOperador", System.Data.SqlDbType.Int, intIdGestionOperador);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaGrupoFuncionariosBiblioteca(string dataset, int intIdGrupoBiblioteca)
    {
        string strCampos = " gb.grubib_Nombre, gb.grubib_Descripcion,  u.segusu_login, u.est_Id, e.est_Descripcion, u.segusu_Correo ";
        string strTabla = " tbl_GrupoBiblioteca gb, tbl_BibliotecaUsuario bu, Seg_Usuario u, tbl_Estado e ";
        string strCondicion = " gb.grubib_Id = bu.grubib_Id AND bu.segUsu_Id = u.segusu_Id AND U.est_Id = E.est_Id AND E.est_Descripcion = 'ACTIVO' AND gb.grubib_Id = @intIdGrupoBiblioteca ";
        string strOrden = " u.segusu_Correo ";
        base.clearParameters();
        base.setParameters("@intIdGrupoBiblioteca", System.Data.SqlDbType.Int, intIdGrupoBiblioteca);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaGrupoOperadorReport(string dataset)
    {
        string strCampos = " gruOpe_Id, gruOpe_Nombre ";
        string strTabla = " tbl_GrupoOperador ";
        string strCondicion = " 1 = 1 ";
        string strOrden = " gruOpe_Nombre ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntConsultaGrupoBibliotecaReport(string dataset)
    {
        string strCampos = " grubib_Id, grubib_Nombre ";
        string strTabla = " tbl_GrupoBiblioteca ";
        string strCondicion = " 1 = 1 ";
        string strOrden = " grubib_Nombre ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    public bool fntIngresarCriterio_bol(string strCategoriaDescripcion)
    {
        string strCampos = "cat_descripcion";
        string strTabla = "tbl_Categoria";
        string strValores = "@strCategoriaDescripcion";
        string strCondicion = "cat_descripcion = @strCategoriaDescripcion";
        base.clearParameters();
        base.setParameters("@strCategoriaDescripcion", System.Data.SqlDbType.NVarChar, strCategoriaDescripcion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarCriterioEvaluar_bol(int intIdTabulacionEncuesta, int intIdCategoria, int intItem, string strPregunta, string strCalificacion)
    {
        string strCampos = "tabenc_Id, cat_Id, crieva_Item, crieva_Pregunta, crieva_Calificacion";
        string strTabla = "tbl_CriterioEvaluar";
        string strValores = "@intIdTabulacionEncuesta, @intIdCategoria, @intItem, @strPregunta, @strCalificacion";
        string strCondicion = "tabenc_Id = @intIdTabulacionEncuesta AND cat_Id = @intIdCategoria AND crieva_Item = @intItem AND crieva_Pregunta = @strPregunta AND crieva_Calificacion = @strCalificacion";
        base.clearParameters();
        base.setParameters("@intIdTabulacionEncuesta", System.Data.SqlDbType.Int, intIdTabulacionEncuesta);
        base.setParameters("@intIdCategoria", System.Data.SqlDbType.Int, intIdCategoria);
        base.setParameters("@intItem", System.Data.SqlDbType.Int, intItem);
        base.setParameters("@strPregunta", System.Data.SqlDbType.NVarChar, strPregunta);
        base.setParameters("@strCalificacion", System.Data.SqlDbType.NVarChar, strCalificacion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarObservacionCriterio_bol(int intIdTabulacionEncuesta, string strObservacion)
    {
        string strCampos = "tabenc_Id, observacion";
        string strTabla = "tbl_ObservacionCriterio";
        string strValores = "@intIdTabulacionEncuesta, @strObservacion";
        string strCondicion = "tabenc_Id = @intIdTabulacionEncuesta AND observacion = @strObservacion";
        base.clearParameters();
        base.setParameters("@intIdTabulacionEncuesta", System.Data.SqlDbType.Int, intIdTabulacionEncuesta);
        base.setParameters("@strObservacion", System.Data.SqlDbType.NVarChar, strObservacion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarMunicipioPriorizado_bol(string strIdMunicipio, int intIdDatosPriorizacion)
    {
        string strCampos = " codMunicipioAlf5, datPri_Id ";
        string strTabla = "tbl_MunicipioPriorizado";
        string strValores = "@strIdMunicipio, @intIdDatosPriorizacion";
        string strCondicion = "codMunicipioAlf5 = @strIdMunicipio AND datPri_Id = @intIdDatosPriorizacion";
        base.clearParameters();
        base.setParameters("@strIdMunicipio", System.Data.SqlDbType.NVarChar, strIdMunicipio);
        base.setParameters("@intIdDatosPriorizacion", System.Data.SqlDbType.Int, intIdDatosPriorizacion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarDatosEncuesta_bol(string strTema, string strFecha, int IntIdCapacitador, int intIdProcedencia, string strCodigoProccedencia)
    {
        string strCampos = " nomTema, fecha, cap_Id, encu_Procedencia, encu_CodigoProcedencia ";
        string strTabla = " tbl_DatosEncuesta ";
        string strValores = " @strTema, @strFecha, @IntIdCapacitador, @intIdProcedencia, @strCodigoProccedencia";
        string strCondicion = " nomTema = @strTema AND fecha = @strFecha AND cap_Id = @IntIdCapacitador AND  encu_Procedencia = @intIdProcedencia AND encu_CodigoProcedencia = @strCodigoProccedencia";
        base.clearParameters();
        base.setParameters("@strTema", System.Data.SqlDbType.NVarChar, strTema);
        base.setParameters("@strFecha", System.Data.SqlDbType.Date, strFecha);
        base.setParameters("@IntIdCapacitador", System.Data.SqlDbType.Int, IntIdCapacitador);
        base.setParameters("@intIdProcedencia", System.Data.SqlDbType.Int, intIdProcedencia);
        base.setParameters("@strCodigoProccedencia", System.Data.SqlDbType.NVarChar, strCodigoProccedencia);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarEncuestado_bol(int intTipoIdentificacion, string strNoIdentificacion, string strNombre, string strApellidos, int intEdad)
    {
        string strCampos = " tip_Id, identificacionNacionalPersona, nomPersona, enc_Apellidos, edad ";
        string strTabla = " tbl_Encuestado ";
        string strValores = " @intTipoIdentificacion, @strNoIdentificacion, @strNombre, @strApellidos, @intEdad ";
        string strCondicion = " tip_Id = @intTipoIdentificacion AND identificacionNacionalPersona = @strNoIdentificacion AND nomPersona = @strNombre AND enc_Apellidos = @strApellidos AND edad = @intEdad";
        base.clearParameters();
        base.setParameters("@intTipoIdentificacion", System.Data.SqlDbType.Int, intTipoIdentificacion);
        base.setParameters("@strNoIdentificacion", System.Data.SqlDbType.NVarChar, strNoIdentificacion);
        base.setParameters("@strNombre", System.Data.SqlDbType.NVarChar, strNombre);
        base.setParameters("@strApellidos", System.Data.SqlDbType.NVarChar, strApellidos);
        base.setParameters("@intEdad", System.Data.SqlDbType.Int, intEdad);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarEncuesta_bol(int intIdDatosEncuesta, int intIdEncuestado, string strPregunta, string strRespuesta, string strRespuestaCorrecta)
    {
        string strCampos = " datEnc_Id, enc_Id, encu_Pregunta, respuesta, encu_RtaSeleccionada ";
        string strTabla = " tbl_Encuesta ";
        string strValores = " @intIdDatosEncuesta, @intIdEncuestado, @strPregunta, @strRespuesta, @strRespuestaCorrecta  ";
        string strCondicion = " datEnc_Id = @intIdDatosEncuesta AND enc_Id = @intIdEncuestado AND encu_Pregunta = @strPregunta AND respuesta = @strRespuesta AND encu_RtaSeleccionada = @strRespuestaCorrecta";
        base.clearParameters();
        base.setParameters("@intIdDatosEncuesta", System.Data.SqlDbType.Int, intIdDatosEncuesta);
        base.setParameters("@intIdEncuestado", System.Data.SqlDbType.Int, intIdEncuestado);
        base.setParameters("@strPregunta", System.Data.SqlDbType.NVarChar, strPregunta);
        base.setParameters("@strRespuesta", System.Data.SqlDbType.NVarChar, strRespuesta);
        base.setParameters("@strRespuestaCorrecta", System.Data.SqlDbType.NVarChar, strRespuestaCorrecta);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarObservacionEncuesta_bol(string strObservacion, int intIdDatosEncuesta)
    {
        string strCampos = " observacion, datEnc_id ";
        string strTabla = " tbl_ObservacionEncuesta ";
        string strValores = " @strObservacion, @intIdDatosEncuesta ";
        string strCondicion = " observacion = @strObservacion AND datEnc_id = @intIdDatosEncuesta ";
        base.clearParameters();
        base.setParameters("@strObservacion", System.Data.SqlDbType.NVarChar, strObservacion);
        base.setParameters("@intIdDatosEncuesta", System.Data.SqlDbType.Int, intIdDatosEncuesta);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarSeguimientoPO_bol(int intIdComponente, int intAno, string strMes, string strFechaInicio, string strFechaFinal, string strsAccionesAdelantadas, string strEvidenciasAdelantadas, string strResultados, int intIdEbi)
    {
        string strCampos = " com_Id, ano, mes, fechaInicio, fechaFinal, segPO_AccionesAdelantadas, segPO_EvidenciasAdelantadas, segPO_Resultados, ebi_Id ";
        string strTabla = " tbl_SeguimientoPO ";
        string strValores = " @intIdComponente, @intAno, @strMes, @strFechaInicio, @strFechaFinal, @strsAccionesAdelantadas, @strEvidenciasAdelantadas, @strResultados, @intIdEbi ";
        string strCondicion = " com_Id = @intIdComponente AND ano = @intAno AND mes = @strMes AND fechaInicio = @strFechaInicio AND fechaFinal = @strFechaFinal AND segPO_AccionesAdelantadas = @strsAccionesAdelantadas AND segPO_EvidenciasAdelantadas = @strEvidenciasAdelantadas AND segPO_Resultados = @strResultados AND ebi_Id = @intIdEbi ";
        base.clearParameters();
        base.setParameters("@intIdComponente", System.Data.SqlDbType.Int, intIdComponente);
        base.setParameters("@intAno", System.Data.SqlDbType.Int, intAno);
        base.setParameters("@strMes", System.Data.SqlDbType.NVarChar, strMes);
        base.setParameters("@strFechaInicio", System.Data.SqlDbType.NVarChar, strFechaInicio);
        base.setParameters("@strFechaFinal", System.Data.SqlDbType.NVarChar, strFechaFinal);
        base.setParameters("@strsAccionesAdelantadas", System.Data.SqlDbType.NVarChar, strsAccionesAdelantadas);
        base.setParameters("@strEvidenciasAdelantadas", System.Data.SqlDbType.NVarChar, strEvidenciasAdelantadas);
        base.setParameters("@strResultados", System.Data.SqlDbType.NVarChar, strResultados);
        base.setParameters("@intIdEbi", System.Data.SqlDbType.Int, intIdEbi);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarSeguimientoPOVsActividad_bol(int intIdActividadPO, int IntIdSeguimientoPO)
    {
        string strCampos = " actPO_Id, segPO_Id ";
        string strTabla = " tbl_SeguimientoPO_Actividad ";
        string strValores = " @intIdActividadPO, @IntIdSeguimientoPO ";
        string strCondicion = " actPO_Id = @intIdActividadPO AND segPO_Id = @IntIdSeguimientoPO ";
        base.clearParameters();
        base.setParameters("@intIdActividadPO", System.Data.SqlDbType.Int, intIdActividadPO);
        base.setParameters("@IntIdSeguimientoPO", System.Data.SqlDbType.Int, IntIdSeguimientoPO);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarCompromisoPO_bol(string strCompromiso, int intIdEstado, string strObservacion, string strFechaInicio, string strFechaFinal, int intIdResponsable, int intIdSeguimintoPO)
    {
        string strCampos = " comp_Descripcion, est_Id, observacion, fechaInicio, fechaFinal, res_Id, segPO_Id ";
        string strTabla = " tbl_Compromiso ";
        string strValores = " @strCompromiso, @intIdEstado, @strObservacion, @strFechaInicio, @strFechaFinal, @intIdResponsable, @intIdSeguimintoPO ";
        string strCondicion = " comp_Descripcion = @strCompromiso AND est_Id = @intIdEstado AND observacion = @strObservacion AND fechaInicio = @strFechaInicio AND fechaFinal = @strFechaFinal AND res_Id = @intIdResponsable AND segPO_Id = @intIdSeguimintoPO ";
        base.clearParameters();
        base.setParameters("@strCompromiso", System.Data.SqlDbType.NVarChar, strCompromiso);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        base.setParameters("@strObservacion", System.Data.SqlDbType.NVarChar, strObservacion);
        base.setParameters("@strFechaInicio", System.Data.SqlDbType.Date, strFechaInicio);
        base.setParameters("@strFechaFinal", System.Data.SqlDbType.Date, strFechaFinal);
        base.setParameters("@intIdResponsable", System.Data.SqlDbType.Int, intIdResponsable);
        base.setParameters("@intIdSeguimintoPO", System.Data.SqlDbType.Int, intIdSeguimintoPO);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarActividad_bol(string strActividad, string strFechaInicio, string strFechaFinal)
    {
        string strCampos = " actividadOcupacionalPersona, fechaInicio, fechaFinal ";
        string strTabla = " tbl_Actividad ";
        string strValores = " @strActividad, @strFechaInicio, @strFechaFinal ";
        string strCondicion = " actividadOcupacionalPersona = @strActividad AND fechaInicio = @strFechaInicio AND fechaFinal = @strFechaFinal";
        base.clearParameters();
        base.setParameters("@strActividad", System.Data.SqlDbType.NVarChar, strActividad);
        base.setParameters("@strFechaInicio", System.Data.SqlDbType.Date, strFechaInicio);
        base.setParameters("@strFechaFinal", System.Data.SqlDbType.Date, strFechaFinal);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarCompromisoGT_bol(string strCompromiso, int intIdEstado, string strFechaInicio, int intIdResponsable)
    {
        string strCampos = " comp_Descripcion, est_Id, observacion, fechaInicio, fechaFinal, res_Id, segPO_Id ";
        string strTabla = " tbl_Compromiso ";
        string strValores = " @strCompromiso, @intIdEstado, ' ' ,@strFechaInicio, null, @intIdResponsable, 0 ";
        string strCondicion = " comp_Descripcion = @strCompromiso AND est_Id = @intIdEstado AND fechaInicio = @strFechaInicio AND res_Id = @intIdResponsable ";
        base.clearParameters();
        base.setParameters("@strCompromiso", System.Data.SqlDbType.NVarChar, strCompromiso);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        base.setParameters("@strFechaInicio", System.Data.SqlDbType.Date, strFechaInicio);
        base.setParameters("@intIdResponsable", System.Data.SqlDbType.Int, intIdResponsable);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarMatrizIntervencion_bol(int intIdCodigoInforme, string strObservacion, string strFecha)
    {
        string strCampos = "  matint_CodigoInforme, observacion, fecha, fechaInicio, fechaFinal, mes, matint_ObjetoComision, acti_Id, comp_Id, res_Id, com_Id, matint_Soporte, matint_Resultado ";
        string strTabla = " tbl_MatrizIntervencion ";
        string strCampos2 = " @intIdCodigoInforme, @strObservacion, @strFecha, g.gt7 FechaInicio, g.gt8 FechaFinal, g.gt6 Mes, g.gt9 ObjetoComision, A.acti_Id IdActividad, Co.comp_Id IdCompromisos, R.res_Id  IdAsesor, c.com_Id IdComponente, g.gt13 Soporte, g.gt14 Resultados ";
        string strTabla2 = " tmp_GT1 g, tbl_Componente C, tbl_Responsable R, tbl_Actividad A, tbl_Compromiso Co ";
        string strCondicion = " g.gt11 = C.com_Nombre AND g.gt12 = (R.nombrePersona + ' ' + R.ApellidoPersona) AND g.gt10 = A.actividadOcupacionalPersona AND g.gt15 = Co.comp_Descripcion AND R.res_Id = co.res_Id AND g.gt7 = Co.fechaInicio ";
        base.clearParameters();
        base.setParameters("@intIdCodigoInforme", System.Data.SqlDbType.Int, intIdCodigoInforme);
        base.setParameters("@strObservacion", System.Data.SqlDbType.NVarChar, strObservacion);
        base.setParameters("@strFecha", System.Data.SqlDbType.Date, strFecha);
        base.iniciarTransaccion();
        if (base.InsertarSelect(strTabla, strCampos, strCampos2, strTabla2, strCondicion, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarMatrizIntervencionR_bol(int intIdCodigoInforme, string strObservacion, string strFecha, string strFechaInicio, string strFechaFinal, string strMes, string strObjetoComision, int intIdActividad, int intIdCompromiso, int intIdResponsable, int intIdComponente, string strSoporte, string strResultado)
    {
        string strCampos = " matint_CodigoInforme, observacion, fecha, fechaInicio, fechaFinal, mes, matint_ObjetoComision, acti_Id, comp_Id, res_Id, com_Id, matint_Soporte, matint_Resultado  ";
        string strTabla = " tbl_MatrizIntervencion ";
        string strValores = " @intIdCodigoInforme, @strObservacion, @strFecha, @strFechaInicio, @strFechaFinal, @strMes, @strObjetoComision, @intIdActividad, @intIdCompromiso, @intIdResponsable, @intIdComponente, @strSoporte, @strResultado ";
        string strCondicion = " matint_CodigoInforme = @intIdCodigoInforme AND observacion = @strObservacion AND fecha = @strFecha AND fechaInicio = @strFechaInicio AND fechaFinal = @strFechaFinal AND mes = @strMes AND matint_ObjetoComision = @strObjetoComision AND acti_Id = @intIdActividad AND comp_Id = @intIdCompromiso AND res_Id = @intIdResponsable AND com_Id = @intIdComponente AND matint_Soporte = @strSoporte AND matint_Resultado = @strResultado ";
        base.clearParameters();
        base.setParameters("@intIdCodigoInforme", System.Data.SqlDbType.NVarChar, intIdCodigoInforme);
        base.setParameters("@strObservacion", System.Data.SqlDbType.NVarChar, strObservacion);
        base.setParameters("@strFecha", System.Data.SqlDbType.NVarChar, strFecha);
        base.setParameters("@strFechaInicio", System.Data.SqlDbType.NVarChar, strFechaInicio);
        base.setParameters("@strFechaFinal", System.Data.SqlDbType.NVarChar, strFechaFinal);
        base.setParameters("@strMes", System.Data.SqlDbType.NVarChar, strMes);
        base.setParameters("@strObjetoComision", System.Data.SqlDbType.NVarChar, strObjetoComision);
        base.setParameters("@intIdActividad", System.Data.SqlDbType.NVarChar, intIdActividad);
        base.setParameters("@intIdCompromiso", System.Data.SqlDbType.NVarChar, intIdCompromiso);
        base.setParameters("@intIdResponsable", System.Data.SqlDbType.NVarChar, intIdResponsable);
        base.setParameters("@intIdComponente", System.Data.SqlDbType.NVarChar, intIdComponente);
        base.setParameters("@strSoporte", System.Data.SqlDbType.NVarChar, strSoporte);
        base.setParameters("@strResultado", System.Data.SqlDbType.NVarChar, strResultado);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarMunicipiosImpactados_bol(string strIdMunicipio, int intIdMatrizIntervencion)
    {
        string strCampos = " codMunicipioAlf5, matint_Id ";
        string strTabla = " tbl_MunicipiosImpactados ";
        string strValores = " @strIdMunicipio, @intIdMatrizIntervencion ";
        string strCondicion = " codMunicipioAlf5 = @strIdMunicipio AND matint_Id = @intIdMatrizIntervencion ";
        base.clearParameters();
        base.setParameters("@strIdMunicipio", System.Data.SqlDbType.NVarChar, strIdMunicipio);
        base.setParameters("@intIdMatrizIntervencion", System.Data.SqlDbType.Int, intIdMatrizIntervencion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarComuidadImpactados_bol(int intIdComunidad, int intIdMatrizIntervencion)
    {
        string strCampos = " comu_Id, matint_Id ";
        string strTabla = " tbl_ComunidadImpactada ";
        string strValores = " @intIdComunidad, @intIdMatrizIntervencion ";
        string strCondicion = " comu_Id = @intIdComunidad AND matint_Id = @intIdMatrizIntervencion ";
        base.clearParameters();
        base.setParameters("@intIdComunidad", System.Data.SqlDbType.Int, intIdComunidad);
        base.setParameters("@intIdMatrizIntervencion", System.Data.SqlDbType.Int, intIdMatrizIntervencion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarPersona_bol(int intIdVocativo, string strNombre, string strApellido, int intIdCargo, int intIdCategoria, string strDireccion, int intIdTipoDireccion, string strCodMunicipio, int intIdInstitucion, int intIdTipoIdentificacion, int intNumeroIdentificacion)
    {
        string strCampos = " voc_Id, tip_Identificacion, identificacionNacionalPersona, nombrePersona, apellidoPersona, car_Id, cat_Id, direccion, tip_Id, codMunicipioAlf5, ins_Id, per_paginaWeb ";
        string strTabla = " tbl_Persona ";
        string strValores = " @intIdVocativo, @intIdTipoIdentificacion, @intNumeroIdentificacion, @strNombre, @strApellido, @intIdCargo, @intIdCategoria, @strDireccion, @intIdTipoDireccion, @strCodMunicipio, @intIdInstitucion, '' ";
        string strCondicion = " voc_Id = @intIdVocativo AND nombrePersona = @strNombre AND apellidoPersona = @strApellido AND car_Id = @intIdCargo AND cat_Id = @intIdCategoria AND direccion = @strDireccion AND  tip_Id = @intIdTipoDireccion AND codMunicipioAlf5 = @strCodMunicipio AND ins_Id = @intIdInstitucion ";
        base.clearParameters();
        base.setParameters("@intIdVocativo", System.Data.SqlDbType.Int, intIdVocativo);
        base.setParameters("@intIdTipoIdentificacion", System.Data.SqlDbType.Int, intIdTipoIdentificacion);
        base.setParameters("@intNumeroIdentificacion", System.Data.SqlDbType.Int, intNumeroIdentificacion);
        base.setParameters("@strNombre", System.Data.SqlDbType.NVarChar, strNombre);
        base.setParameters("@strApellido", System.Data.SqlDbType.NVarChar, strApellido);
        base.setParameters("@intIdCargo", System.Data.SqlDbType.Int, intIdCargo);
        base.setParameters("@intIdCategoria", System.Data.SqlDbType.Int, intIdCategoria);
        base.setParameters("@strDireccion", System.Data.SqlDbType.NVarChar, strDireccion);
        base.setParameters("@intIdTipoDireccion", System.Data.SqlDbType.Int, intIdTipoDireccion);
        base.setParameters("@strCodMunicipio", System.Data.SqlDbType.NVarChar, strCodMunicipio);
        base.setParameters("@intIdInstitucion", System.Data.SqlDbType.Int, intIdInstitucion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarTelefono_bol(int intIdPersona, string strNumTelefono, int intIdTipoTelefono, int intTipoCategoriaTelefono)
    {
        string strCampos = " per_Id, numTelefono, tip_Id, tip_idTipoTelefono ";
        string strTabla = " tbl_Telefono ";
        string strValores = " @intIdPersona, @strNumTelefono, @intIdTipoTelefono, @intTipoCategoriaTelefono ";
        string strCondicion = " per_Id = @intIdPersona AND numTelefono = @strNumTelefono AND tip_Id = @intIdTipoTelefono AND tip_idTipoTelefono = @intTipoCategoriaTelefono";
        base.clearParameters();
        base.setParameters("@intIdPersona", System.Data.SqlDbType.Int, intIdPersona);
        base.setParameters("@strNumTelefono", System.Data.SqlDbType.NVarChar, strNumTelefono);
        base.setParameters("@intIdTipoTelefono", System.Data.SqlDbType.Int, intIdTipoTelefono);
        base.setParameters("@intTipoCategoriaTelefono", System.Data.SqlDbType.Int, intTipoCategoriaTelefono);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarCorreoElectronico_bol(int intIdPersona, string strEmail, int intIdTipoEmail)
    {
        string strCampos = " per_Id, direccionCorreoElectronico, tip_Id ";
        string strTabla = " tbl_CorreoElectronico ";
        string strValores = " @intIdPersona, @strEmail, @intIdTipoEmail ";
        string strCondicion = " per_Id = @intIdPersona AND direccionCorreoElectronico = @strEmail AND tip_Id = @intIdTipoEmail ";
        base.clearParameters();
        base.setParameters("@intIdPersona", System.Data.SqlDbType.Int, intIdPersona);
        base.setParameters("@strEmail", System.Data.SqlDbType.NVarChar, strEmail);
        base.setParameters("@intIdTipoEmail", System.Data.SqlDbType.Int, intIdTipoEmail);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarRecursosProyecto_bol(int intIdRubroPresupuestal, int intIdTipoProyecto, int intIdProyecto, string strValorTotalProyecto, string strNoContrato, string strFechaContrato, int intIdContratista)
    {
        string strCampos = " rubpre_Id, tip_Id, pro_Id, recpro_ValorTotal, recpro_ValorComprometido, recpro_ValorDisponible, recpro_NContrato, recpro_fechaContrato, per_Id ";
        string strTabla = " tbl_RecursoProyecto ";
        string strValores = " @intIdRubroPresupuestal, @intIdTipoProyecto, @intIdProyecto, @strValorTotalProyecto, 0, @strValorTotalProyecto, @strNoContrato, @strFechaContrato, @intIdContratista ";
        string strCondicion = " rubpre_Id = @intIdRubroPresupuestal AND tip_Id = @intIdTipoProyecto AND pro_Id = @intIdProyecto AND recpro_ValorTotal = @strValorTotalProyecto AND recpro_ValorComprometido = 0 AND recpro_ValorDisponible = @strValorTotalProyecto AND recpro_NContrato = @strNoContrato AND recpro_fechaContrato = @strFechaContrato AND per_Id = @intIdContratista ";
        base.clearParameters();
        base.setParameters("@intIdRubroPresupuestal", System.Data.SqlDbType.Int, intIdRubroPresupuestal);
        base.setParameters("@intIdTipoProyecto", System.Data.SqlDbType.Int, intIdTipoProyecto);
        base.setParameters("@intIdProyecto", System.Data.SqlDbType.Int, intIdProyecto);
        base.setParameters("@strValorTotalProyecto", System.Data.SqlDbType.Float, strValorTotalProyecto);
        base.setParameters("@strNoContrato", System.Data.SqlDbType.NVarChar, strNoContrato);
        base.setParameters("@strFechaContrato", System.Data.SqlDbType.Date, strFechaContrato);
        base.setParameters("@intIdContratista", System.Data.SqlDbType.Int, intIdContratista);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarActividadesRecursosProyecto_bol(int intIdActividad, int intIdRecursoProyecto, string strValorProyectado, string strValorContratado, string strValorDisponible, int intIdEstado)
    {
        string strCampos = " acti_Id, recpro_Id, actpro_Valorproyectado, actpro_ValorContratado, actpro_ValorDisponible, est_Id ";
        string strTabla = " tbl_ActividadProyecto ";
        string strValores = " @intIdActividad, @intIdRecursoProyecto, @strValorProyectado, @strValorContratado, @strValorDisponible, @intIdEstado ";
        string strCondicion = " acti_Id = @intIdActividad AND recpro_Id = @intIdRecursoProyecto AND  actpro_Valorproyectado = @strValorProyectado AND actpro_ValorContratado = @strValorContratado AND actpro_ValorDisponible = @strValorDisponible AND est_Id = @intIdEstado ";
        base.clearParameters();
        base.setParameters("@intIdActividad", System.Data.SqlDbType.Int, intIdActividad);
        base.setParameters("@intIdRecursoProyecto", System.Data.SqlDbType.Int, intIdRecursoProyecto);
        base.setParameters("@strValorProyectado", System.Data.SqlDbType.NVarChar, strValorProyectado);
        base.setParameters("@strValorContratado", System.Data.SqlDbType.NVarChar, strValorContratado);
        base.setParameters("@strValorDisponible", System.Data.SqlDbType.NVarChar, strValorDisponible);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarEventoTaller_bol(string strIdMunicipio, int intIdTaller, string strFechaTaller, string strObjeto, int intIdResponsable, string strOperadorLogistico)
    {
        string strCampos = " codMunicipioAlf5, tal_Id, fecha, evetal_Objeto, res_Id, evetal_operadorLogistico ";
        string strTabla = " tbl_EventoTaller ";
        string strValores = " @strIdMunicipio, @intIdTaller, @strFechaTaller, @strObjeto, @intIdResponsable, @strOperadorLogistico ";
        string strCondicion = " codMunicipioAlf5 = @strIdMunicipio AND tal_Id = @intIdTaller AND fecha = @strFechaTaller AND evetal_Objeto = @strObjeto AND res_Id = @intIdResponsable AND evetal_operadorLogistico = @strOperadorLogistico";
        base.clearParameters();
        base.setParameters("@strIdMunicipio", System.Data.SqlDbType.NVarChar, strIdMunicipio);
        base.setParameters("@intIdTaller", System.Data.SqlDbType.Int, intIdTaller);
        base.setParameters("@strFechaTaller", System.Data.SqlDbType.Date, strFechaTaller);
        base.setParameters("@strObjeto", System.Data.SqlDbType.NVarChar, strObjeto);
        base.setParameters("@intIdResponsable", System.Data.SqlDbType.Int, intIdResponsable);
        base.setParameters("@strOperadorLogistico", System.Data.SqlDbType.NVarChar, strOperadorLogistico);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarValorTaller_bol(int intIdEventoTaller, string strValorSinIva, string strValorIva, string strValorComision, string strValorTotal, string strSaldo, string strFactura, string strFechaPago, string strNOrdenPagoSIIF, string strActa, string strFechaActa)
    {
        string strCampos = " evetal_Id, valtal_ValorSinIva, valtal_ValorIva, valtal_ValorComision, valtal_ValorTotal, valtal_Saldo, valtal_Factura, fecha, valtal_NOrdenPagoSIIF, valtal_NActa, FechaActa ";
        string strTabla = " tbl_ValorTaller ";
        string strValores = " @intIdEventoTaller, @strValorSinIva, @strValorIva, @strValorComision, @strValorTotal, @strSaldo, @strFactura, @strFechaPago, @strNOrdenPagoSIIF, @strActa, @strFechaActa ";
        string strCondicion = " evetal_Id = @intIdEventoTaller AND valtal_ValorSinIva = @strValorSinIva AND valtal_ValorIva = @strValorIva AND valtal_ValorComision = @strValorComision AND valtal_ValorTotal = @strValorTotal AND valtal_Saldo = @strSaldo AND valtal_Factura = @strFactura AND fecha = @strFechaPago AND valtal_NOrdenPagoSIIF = @strNOrdenPagoSIIF AND valtal_NActa = @strActa AND FechaActa = @strFechaActa ";
        base.clearParameters();
        base.setParameters("@intIdEventoTaller", System.Data.SqlDbType.Int, intIdEventoTaller);
        base.setParameters("@strValorSinIva", System.Data.SqlDbType.NVarChar, strValorSinIva);
        base.setParameters("@strValorIva", System.Data.SqlDbType.NVarChar, strValorIva);
        base.setParameters("@strValorComision", System.Data.SqlDbType.NVarChar, strValorComision);
        base.setParameters("@strValorTotal", System.Data.SqlDbType.NVarChar, strValorTotal);
        base.setParameters("@strSaldo", System.Data.SqlDbType.NVarChar, strSaldo);
        base.setParameters("@strFactura", System.Data.SqlDbType.NVarChar, strFactura);
        base.setParameters("@strFechaPago", System.Data.SqlDbType.Date, strFechaPago);
        base.setParameters("@strNOrdenPagoSIIF", System.Data.SqlDbType.NVarChar, strNOrdenPagoSIIF);
        base.setParameters("@strActa", System.Data.SqlDbType.NVarChar, strActa);
        base.setParameters("@strFechaActa", System.Data.SqlDbType.Date, strFechaActa);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarGestionOperador_bol(string strRutaFinal, string strNombreDocumento, string strExtDocumento, int intIdUsuario, string strObservacion, int intIdGrupoOperador)
    {
        string strCampos = " geop_rutaDocumento, fecha, geop_NombreDocumento, geop_extDocumento, segusu_Id, geop_Observaciones, gruOpe_Id  ";
        string strTabla = " tbl_GestionOperador ";
        string strValores = " @strRutaFinal, GETDATE(), @strNombreDocumento, @strExtDocumento, @intIdUsuario, @strObservacion, @intIdGrupoOperador ";
        string strCondicion = " geop_rutaDocumento = @strRutaFinal AND geop_NombreDocumento = @strNombreDocumento AND  geop_extDocumento = @strExtDocumento AND segusu_Id = @intIdUsuario AND geop_Observaciones = @strObservacion AND gruOpe_Id = @intIdGrupoOperador  ";
        base.clearParameters();
        base.setParameters("@strRutaFinal", System.Data.SqlDbType.NVarChar, strRutaFinal);
        base.setParameters("@strNombreDocumento", System.Data.SqlDbType.NVarChar, strNombreDocumento);
        base.setParameters("@strExtDocumento", System.Data.SqlDbType.NVarChar, strExtDocumento);
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        base.setParameters("@strObservacion", System.Data.SqlDbType.NVarChar, strObservacion);
        base.setParameters("@intIdGrupoOperador", System.Data.SqlDbType.NVarChar, intIdGrupoOperador);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarDocumentoBiblioteca_bol(int intIdGrupoBiblioteca, int intIdUsuario, string strNombreDocumento, string strExtDocumento, string strObservacion, int intIdEstado, int intIdCodigoBiblioteca, int intIdVersion, int intIdCarpeta)
    {
        string strCampos = " grubib_Id, segusu_Id, bib_NombreDocumento, bib_ExtDocumento, bib_Observaciones, est_Id, bib_FechaCreacion, bib_Version, bib_CodigoDocumento, bibCar_Id ";
        string strTabla = " tbl_Biblioteca ";
        string strValores = " @intIdGrupoBiblioteca, @intIdUsuario,  @strNombreDocumento, @strExtDocumento,  @strObservacion, @intIdEstado, GETDATE(), @intIdVersion, @intIdCodigoBiblioteca, @intIdCarpeta ";
        string strCondicion = " grubib_Id = @intIdGrupoBiblioteca AND segusu_Id = @intIdUsuario AND bib_NombreDocumento = @strNombreDocumento AND bib_ExtDocumento = @strExtDocumento AND bib_Observaciones = @strObservacion";
        base.clearParameters();
        base.setParameters("@intIdGrupoBiblioteca", System.Data.SqlDbType.Int, intIdGrupoBiblioteca);
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        base.setParameters("@strNombreDocumento", System.Data.SqlDbType.NVarChar, strNombreDocumento);
        base.setParameters("@strExtDocumento", System.Data.SqlDbType.NVarChar, strExtDocumento);
        base.setParameters("@strObservacion", System.Data.SqlDbType.NVarChar, strObservacion);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.NVarChar, intIdEstado);
        base.setParameters("@intIdCodigoBiblioteca", System.Data.SqlDbType.NVarChar, intIdCodigoBiblioteca);
        base.setParameters("@intIdVersion", System.Data.SqlDbType.NVarChar, intIdVersion);
        base.setParameters("@intIdCarpeta", System.Data.SqlDbType.Int, intIdCarpeta);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarSeguimientoOperador_bol(int intIdGestionOperador, int intIdUsuarioRevision, int intIdActividad, string strObservacion, int intIdEstado, string strRutaDocumento, string strNombreDocumento, string strExt)
    {
        string strCampos = " geop_Id, fecha, segusu_Id, tip_Id, seop_Observacion, est_Id, seop_RutaDocumento, seop_NombreDocumento, seop_ExtDocumento ";
        string strTabla = " tbl_SeguimientoOperador ";
        string strValores = " @intIdGestionOperador, GETDATE(), @intIdUsuarioRevision, @intIdActividad, @strObservacion, @intIdEstado, @strRutaDocumento, @strNombreDocumento, @strExt ";
        string strCondicion = " geop_Id = @intIdGestionOperador AND segusu_Id = @intIdUsuarioRevision AND tip_Id = @intIdActividad AND seop_Observacion = @strObservacion AND est_Id = @intIdEstado AND seop_RutaDocumento = @strRutaDocumento AND  seop_NombreDocumento = @strNombreDocumento AND seop_ExtDocumento = @strExt";
        base.clearParameters();
        base.setParameters("@intIdGestionOperador", System.Data.SqlDbType.Int, intIdGestionOperador);
        base.setParameters("@intIdUsuarioRevision", System.Data.SqlDbType.Int, intIdUsuarioRevision);
        base.setParameters("@intIdActividad", System.Data.SqlDbType.Int, intIdActividad);
        base.setParameters("@strObservacion", System.Data.SqlDbType.NVarChar, strObservacion);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        base.setParameters("@strRutaDocumento", System.Data.SqlDbType.NVarChar, strRutaDocumento);
        base.setParameters("@strNombreDocumento", System.Data.SqlDbType.NVarChar, strNombreDocumento);
        base.setParameters("@strExt", System.Data.SqlDbType.NVarChar, strExt);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarAdmTablas_bol(string strNombreTabla, string strAbreviatura, int intNumeroCampos, int intIdUsuario, string strObservacion)
    {
        string strCampos = " admTab_NombreTabla, admTab_AbreviaturaTabla, admTab_NumeroCampos, admTab_fecha, segusu_Id, admTab_Activacion, admTab_Observacion ";
        string strTabla = " Seg_AdmTablas ";
        string strValores = " @strNombreTabla, @strAbreviatura,  @intNumeroCampos, GETDATE(), @intIdUsuario, 1, @strObservacion ";
        string strCondicion = " admTab_NombreTabla = @strNombreTabla AND admTab_AbreviaturaTabla = @strAbreviatura AND admTab_NumeroCampos = @intNumeroCampos AND segusu_Id =  @intIdUsuario AND admTab_Activacion = 1 AND admTab_Observacion = @strObservacion ";
        base.clearParameters();
        base.setParameters("@strNombreTabla", System.Data.SqlDbType.NVarChar, strNombreTabla);
        base.setParameters("@strAbreviatura", System.Data.SqlDbType.NVarChar, strAbreviatura);
        base.setParameters("@intNumeroCampos", System.Data.SqlDbType.Int, intNumeroCampos);
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        base.setParameters("@strObservacion", System.Data.SqlDbType.NVarChar, strObservacion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarExpresionCampo_bol(string strTextoCampo)
    {
        string strCampos = " exca_Texto ";
        string strTabla = " Seg_ExpresionCampo ";
        string strValores = " @strTextoCampo ";
        string strCondicion = " exca_Texto = @strTextoCampo ";
        base.clearParameters();
        base.setParameters("@strTextoCampo", System.Data.SqlDbType.NVarChar, strTextoCampo);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarCargaDocumentos(string strModulo, string strOpcion, string strNombreDocumento, int intIdUsuario)
    {
        string strCampos = " carDoc_Modulo, carDoc_Opcion, carDoc_NombreDocumento, carDoc_FechaCarga, segusu_Id  ";
        string strTabla = " Seg_CargaDocumento ";
        string strValores = " @strModulo, @strOpcion, @strNombreDocumento, GETDATE(), @intIdUsuario";
        string strCondicion = " carDoc_Modulo = @strModulo AND carDoc_Opcion = @strOpcion AND carDoc_NombreDocumento = @strNombreDocumento AND segusu_Id = @intIdUsuario ";
        base.clearParameters();
        base.setParameters("@strModulo", System.Data.SqlDbType.NVarChar, strModulo);
        base.setParameters("@strOpcion", System.Data.SqlDbType.NVarChar, strOpcion);
        base.setParameters("@strNombreDocumento", System.Data.SqlDbType.NVarChar, strNombreDocumento);
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.NVarChar, intIdUsuario);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarUsuarioNew_bol(int intIdTipoConexion, string strUsuario, string strPassword, int intIdEstado, int IntIdUsuario, int intIdAplicativo, string strCorreo)
    {
        string strCampos = " segcon_Id, segusu_login, segusu_Password, est_Id, segusu_IdCreacion, segusu_FechaCreacion, segApl_id, per_Id, segusu_Correo ";
        string strTabla = " Seg_Usuario ";
        string strValores = " @intIdTipoConexion, @strUsuario, @strPassword, @intIdEstado, @IntIdUsuario, @fechaActual,  @intIdAplicativo, '', @strCorreo ";
        string strCondicion = " segcon_Id = @intIdTipoConexion AND segusu_login = @strUsuario AND segusu_Password = @strPassword AND est_Id = @intIdEstado AND segusu_IdCreacion = @IntIdUsuario ";
        base.clearParameters();
        base.setParameters("@intIdTipoConexion", System.Data.SqlDbType.Int, intIdTipoConexion);
        base.setParameters("@strUsuario", System.Data.SqlDbType.NVarChar, strUsuario);
        base.setParameters("@strPassword", System.Data.SqlDbType.NVarChar, strPassword);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        base.setParameters("@IntIdUsuario", System.Data.SqlDbType.Int, IntIdUsuario);
        base.setParameters("@fechaActual", System.Data.SqlDbType.DateTime, DateTime.Now);
        base.setParameters("@intIdAplicativo", System.Data.SqlDbType.Int, intIdAplicativo);
        base.setParameters("@strCorreo", System.Data.SqlDbType.NVarChar, strCorreo);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarNewPermiso_bol(int intIdUsuario, int intIdFormulario, int intConsulta, int intCreacion, int intModificar, int intEliminar, int intImpresion, int intIdUsuAdmin)
    {
        string strCampos = " segusu_Id, segfor_Id, segusuper_Consulta, segusuper_Creacion, segusuper_Modificar, segusuper_Eliminar, segusuper_Impresion, segusuper_FechaCreacion, segusuper_IdCreacion ";
        string strTabla = " Seg_UsuarioPermiso ";
        string strValores = " @intIdUsuario, @intIdFormulario, @intConsulta, @intCreacion, @intModificar, @intEliminar, @intImpresion, GETDATE(), @intIdUsuAdmin ";
        string strCondicion = " segusu_Id = @intIdUsuario AND segfor_Id = @intIdFormulario AND segusuper_Consulta = @intConsulta AND segusuper_Creacion = @intCreacion AND segusuper_Modificar = @intModificar AND segusuper_Eliminar = @intEliminar AND segusuper_Impresion = @intImpresion AND segusuper_IdCreacion = @intIdUsuAdmin ";
        base.clearParameters();
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        base.setParameters("@intIdFormulario", System.Data.SqlDbType.Int, intIdFormulario);
        base.setParameters("@intConsulta", System.Data.SqlDbType.Int, intConsulta);
        base.setParameters("@intCreacion", System.Data.SqlDbType.Int, intCreacion);
        base.setParameters("@intModificar", System.Data.SqlDbType.Int, intModificar);
        base.setParameters("@intEliminar", System.Data.SqlDbType.Int, intEliminar);
        base.setParameters("@intImpresion", System.Data.SqlDbType.Int, intImpresion);
        base.setParameters("@intIdUsuAdmin", System.Data.SqlDbType.Int, intIdUsuAdmin);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarTablaDinamica2_bol(string CamposDinamicos, string TablaDinamica, string strValoresExcel, int intMaxId)
    {
        string lstrCampos = CamposDinamicos;
        string lstrTabla = TablaDinamica;
        string lstrValores = strValoresExcel;
        string lstrCondicion = "id = @intMaxId";

        base.clearParameters();
        base.setParameters("@intMaxId", SqlDbType.Int, intMaxId);

        base.iniciarTransaccion();
        if (Insertar(lstrTabla, lstrCampos, lstrValores, lstrCondicion, true, true))
        {
            commit();
            return true;
        }
        else
        {
            rollback();
            return false;
        }
    }
    public bool fntIngresarGrupoOperador_bol(string strNombreGrupo, string strDEscripcion, int intIdUsuario, int intIdEstado)
    {
        string strCampos = " gruOpe_Nombre, gruOpe_Descripcion, gruOpe_FechaCreacion, segUsu_Id, est_Id ";
        string strTabla = " tbl_GrupoOperador ";
        string strValores = " @strNombreGrupo, @strDEscripcion, GETDATE(), @intIdUsuario, @intIdEstado ";
        string strCondicion = " gruOpe_Nombre = @strNombreGrupo AND gruOpe_Descripcion = @strDEscripcion AND segUsu_Id = @intIdUsuario AND est_Id = @intIdEstado ";
        base.clearParameters();
        base.setParameters("@strNombreGrupo", System.Data.SqlDbType.NVarChar, strNombreGrupo);
        base.setParameters("@strDEscripcion", System.Data.SqlDbType.NVarChar, strDEscripcion);
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarGrupoBiblioteca_bol(string strNombreGrupo, string strDEscripcion, int intIdUsuario, int intIdEstado)
    {
        string strCampos = " grubib_Nombre, grubib_Descripcion, grubib_FechaCreacion, SegUsu_Id, est_Id ";
        string strTabla = " tbl_GrupoBiblioteca ";
        string strValores = " @strNombreGrupo, @strDEscripcion, GETDATE(), @intIdUsuario, @intIdEstado ";
        string strCondicion = " grubib_Nombre = @strNombreGrupo AND grubib_Descripcion = @strDEscripcion AND SegUsu_Id = @intIdUsuario AND est_Id = @intIdEstado  ";
        base.clearParameters();
        base.setParameters("@strNombreGrupo", System.Data.SqlDbType.NVarChar, strNombreGrupo);
        base.setParameters("@strDEscripcion", System.Data.SqlDbType.NVarChar, strDEscripcion);
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarBibliotecaCarpeta_bol(string strDescripcion, int intIdGrupoBiblioteca, int intIdUsu)
    {
        string strCampos = " bibCar_Descripcion, bibCar_FechaCreacion, grubib_Id, SegUsu_Id ";
        string strTabla = " tbl_BibliotecaCarpetas ";
        string strValores = " @strDescripcion, GETDATE(), @intIdGrupoBiblioteca, @intIdUsu ";
        string strCondicion = " bibCar_Descripcion = @strDescripcion AND grubib_Id = @intIdGrupoBiblioteca  AND SegUsu_Id = @intIdUsu ";
        base.clearParameters();
        base.setParameters("@strDescripcion", System.Data.SqlDbType.NVarChar, strDescripcion);
        base.setParameters("@intIdGrupoBiblioteca", System.Data.SqlDbType.Int, intIdGrupoBiblioteca);
        base.setParameters("@intIdUsu", System.Data.SqlDbType.Int, intIdUsu);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarUsuarioGrupoOperador_bol(int intIdGrupoOperador, int intIdUsuario, int intIdUsuCreacion, int intSupervisar)
    {
        string strCampos = " gruOpe_Id, segUsu_Id, gruUsu_FechaCreacion, segUsu_Creacion, segUsu_Supervizar";
        string strTabla = " tbl_GrupoUsuario ";
        string strValores = " @intIdGrupoOperador, @intIdUsuario, GETDATE(), @intIdUsuCreacion, @intSupervisar ";
        string strCondicion = " gruOpe_Id = @intIdGrupoOperador AND segUsu_Id = @intIdUsuario AND segUsu_Creacion = @intIdUsuCreacion ";
        base.clearParameters();
        base.setParameters("@intIdGrupoOperador", System.Data.SqlDbType.NVarChar, intIdGrupoOperador);
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.NVarChar, intIdUsuario);
        base.setParameters("@intIdUsuCreacion", System.Data.SqlDbType.NVarChar, intIdUsuCreacion);
        base.setParameters("@intSupervisar", System.Data.SqlDbType.NVarChar, intSupervisar);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarUsuarioGrupoBiblioteca_bol(int intIdGrupoBiblioteca, int intIdUsuario, int intIdUsuCreacion)
    {
        string strCampos = " grubib_Id, segUsu_Id, bibUsu_FechaCreacion, bibUsu_IdCreacion ";
        string strTabla = " tbl_BibliotecaUsuario ";
        string strValores = " @intIdGrupoBiblioteca, @intIdUsuario, GETDATE(), @intIdUsuCreacion ";
        string strCondicion = " grubib_Id = @intIdGrupoBiblioteca AND segUsu_Id = @intIdUsuario AND bibUsu_IdCreacion = @intIdUsuCreacion ";
        base.clearParameters();
        base.setParameters("@intIdGrupoBiblioteca", System.Data.SqlDbType.NVarChar, intIdGrupoBiblioteca);
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.NVarChar, intIdUsuario);
        base.setParameters("@intIdUsuCreacion", System.Data.SqlDbType.NVarChar, intIdUsuCreacion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarDatosGeneralesVictima_bol(int intIdTipoIdentidicacion, int intNumeroIdentificacion, string strNombre, string strApellido, string strTelefono, string strDireccion, string strFecha, int intEdad, int intIdGenero, int intIdEtnia, int intIdNivelFormacion)
    {
        string strCampos = " tip_Id, identificacionNacionalPersona, nombrePersona, apellidoPersona, numTelefono, direccion, fecha, edad, tip_IdGenero, etn_Id, for_Id ";
        string strTabla = " tbl_Victima ";
        string strValores = " @intIdTipoIdentidicacion, @intNumeroIdentificacion, @strNombre, @strApellido, @strTelefono, @strDireccion, @strFecha, @intEdad, @intIdGenero, @intIdEtnia, @intIdNivelFormacion  ";
        string strCondicion = " tip_Id = @intIdTipoIdentidicacion AND identificacionNacionalPersona = @intNumeroIdentificacion AND nombrePersona = @strNombre AND apellidoPersona = @strApellido AND numTelefono = @strTelefono AND direccion = @strDireccion AND fecha = @strFecha AND edad = @intEdad AND tip_IdGenero = @intIdGenero AND etn_Id = @intIdEtnia AND for_Id = @intIdNivelFormacion";
        base.clearParameters();
        base.setParameters("@intIdTipoIdentidicacion", System.Data.SqlDbType.Int, intIdTipoIdentidicacion);
        base.setParameters("@intNumeroIdentificacion", System.Data.SqlDbType.Int, intNumeroIdentificacion);
        base.setParameters("@strNombre", System.Data.SqlDbType.NVarChar, strNombre);
        base.setParameters("@strApellido", System.Data.SqlDbType.NVarChar, strApellido);
        base.setParameters("@strTelefono", System.Data.SqlDbType.NVarChar, strTelefono);
        base.setParameters("@strDireccion", System.Data.SqlDbType.NVarChar, strDireccion);
        base.setParameters("@strFecha", System.Data.SqlDbType.NVarChar, strFecha);
        base.setParameters("@intEdad", System.Data.SqlDbType.Int, intEdad);
        base.setParameters("@intIdGenero", System.Data.SqlDbType.Int, intIdGenero);
        base.setParameters("@intIdEtnia", System.Data.SqlDbType.Int, intIdEtnia);
        base.setParameters("@intIdNivelFormacion", System.Data.SqlDbType.Int, intIdNivelFormacion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarDatosAccidenteVictima_bol(int intIdVictima, int intIdVereda, int intIdAreaPoblacional, string strSitioAccidente, string strCondicion, int intIdEstado, string strActividadAccidente, int intAmputacionInicial, int intAmputacionFin, int intHerida, int intEsquirla, int intQuemadura, int intFractura, int intInfeccion, int intAfectacionVisual, int intAfectacionAuditiva, string strDiagnosticoMedico, string strDescripcionHechos, string strGestion)
    {
        string strCampos = " ver_Id, arepob_Id, monseg_SitioAccidente, vic_Id, monseg_Condicion, est_Id, monseg_ActividadAccidente, monseg_AmputacionInicial, monseg_AmputacionFin, monseg_Herida, monseg_Esquirla, monseg_Quemadura, monseg_Fractura, monseg_Infeccion, monseg_AfectacionVisual, monseg_AfectacionAuditiva, DiagnosticoMedico, monseg_DescripcionHechos, monseg_Gestion ";
        string strTabla = " tbl_MonitoreoSeguimiento ";
        string strValores = " @intIdVereda, @intIdAreaPoblacional, @strSitioAccidente, @intIdVictima, @strCondicion, @intIdEstado, @strActividadAccidente, @intAmputacionInicial, @intAmputacionFin, @intHerida, @intEsquirla, @intQuemadura, @intFractura, @intInfeccion, @intAfectacionVisual, @intAfectacionAuditiva, @strDiagnosticoMedico, @strDescripcionHechos, @strGestion ";
        string strCondicion2 = " ver_Id = @intIdVereda AND arepob_Id = @intIdAreaPoblacional AND monseg_SitioAccidente = @strSitioAccidente AND monseg_Condicion = @strCondicion AND est_Id = @intIdEstado AND monseg_ActividadAccidente = @strActividadAccidente AND monseg_AmputacionInicial = @intAmputacionInicial AND monseg_AmputacionFin = @intAmputacionFin AND monseg_Herida = @intHerida AND monseg_Esquirla = @intEsquirla AND monseg_Quemadura = @intQuemadura AND monseg_Fractura = @intFractura AND monseg_Infeccion = @intInfeccion AND monseg_AfectacionVisual = @intAfectacionVisual AND monseg_AfectacionAuditiva = @intAfectacionAuditiva AND DiagnosticoMedico = @strDiagnosticoMedico AND monseg_DescripcionHechos = @strDescripcionHechos AND monseg_Gestion = @strGestion ";
        base.clearParameters();
        base.setParameters("@intIdVictima", System.Data.SqlDbType.Int, intIdVictima);
        base.setParameters("@intIdVereda", System.Data.SqlDbType.Int, intIdVereda);
        base.setParameters("@intIdAreaPoblacional", System.Data.SqlDbType.Int, intIdAreaPoblacional);
        base.setParameters("@strSitioAccidente", System.Data.SqlDbType.NVarChar, strSitioAccidente);
        base.setParameters("@strCondicion", System.Data.SqlDbType.NVarChar, strCondicion);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        base.setParameters("@strActividadAccidente", System.Data.SqlDbType.NVarChar, strActividadAccidente);
        base.setParameters("@intAmputacionInicial", System.Data.SqlDbType.Int, intAmputacionInicial);
        base.setParameters("@intAmputacionFin", System.Data.SqlDbType.Int, intAmputacionFin);
        base.setParameters("@intHerida", System.Data.SqlDbType.Int, intHerida);
        base.setParameters("@intEsquirla", System.Data.SqlDbType.Int, intEsquirla);
        base.setParameters("@intQuemadura", System.Data.SqlDbType.Int, intQuemadura);
        base.setParameters("@intFractura", System.Data.SqlDbType.Int, intFractura);
        base.setParameters("@intInfeccion", System.Data.SqlDbType.Int, intInfeccion);
        base.setParameters("@intAfectacionVisual", System.Data.SqlDbType.Int, intAfectacionVisual);
        base.setParameters("@intAfectacionAuditiva", System.Data.SqlDbType.Int, intAfectacionAuditiva);
        base.setParameters("@strDiagnosticoMedico", System.Data.SqlDbType.NVarChar, strDiagnosticoMedico);
        base.setParameters("@strDescripcionHechos", System.Data.SqlDbType.NVarChar, strDescripcionHechos);
        base.setParameters("@strGestion", System.Data.SqlDbType.NVarChar, strGestion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion2, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarCorreo_bol(string strServer, int intPort, string strUser, string StrPassword, string strNombre, int intEstado)
    {
        string strCampos = " segCor_Server, segCor_Port, segCor_User, segCor_Password, segCor_Nombre, segCor_Estado ";
        string strTabla = " seg_Correo ";
        string strValores = " @strServer, @intPort, @strUser, @StrPassword, @strNombre, @intEstado ";
        string strCondicion = " segCor_Server = @strServer AND segCor_Port = @intPort AND segCor_User = @strUser ";
        base.clearParameters();
        base.setParameters("@strServer", System.Data.SqlDbType.NVarChar, strServer);
        base.setParameters("@intPort", System.Data.SqlDbType.Int, intPort);
        base.setParameters("@strUser", System.Data.SqlDbType.NVarChar, strUser);
        base.setParameters("@StrPassword", System.Data.SqlDbType.NVarChar, StrPassword);
        base.setParameters("@strNombre", System.Data.SqlDbType.NVarChar, strNombre);
        base.setParameters("@intEstado", System.Data.SqlDbType.NVarChar, intEstado);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntEliminarTitulosTmpGT1_bol()
    {
        string strTabla = " tmp_GT1 ";
        string strCondicion = " id in (SELECT top 3 id FROM tmp_GT1 ORDER BY id); ";
        return base.Eliminar(strTabla, strCondicion, false);
    }
    public bool fntEliminarPersona_bol(int intIdPersona)
    {
        string strTabla = " tbl_Persona ";
        string strCondicion = " per_Id = @intIdPersona ";
        base.clearParameters();
        base.setParameters("@intIdPersona", System.Data.SqlDbType.Int, intIdPersona);
        return base.Eliminar(strTabla, strCondicion, false);
    }
    public bool fntEliminarTablaTemporal_bol(string strTablaTemporar)
    {
        string strCondicion = " 1 = 1 ";
        return base.Eliminar(strTablaTemporar, strCondicion, false);
    }
    public bool fntEliminarGestionOperador(int intIdGestionOperador)
    {
        string strTabla = " tbl_GestionOperador ";
        string strCondicion = " geop_Id =  @intIdGestionOperador ";
        base.clearParameters();
        base.setParameters("@intIdGestionOperador", System.Data.SqlDbType.Int, intIdGestionOperador);
        return base.Eliminar(strTabla, strCondicion, false);
    }
    public bool fntEliminarRegistroTabla(int intIdAdministracionTabla)
    {
        string strTabla = " Seg_AdmTablas ";
        string strCondicion = " admTab_Id = @intIdAdministracionTabla ";
        base.clearParameters();
        base.setParameters("@intIdAdministracionTabla", System.Data.SqlDbType.Int, intIdAdministracionTabla);
        return base.Eliminar(strTabla, strCondicion, false);
    }
    public bool fntEliminarTablaExpresionCampo_bol()
    {
        string strTabla = " Seg_ExpresionCampo ";
        string strCondicion = " 1 = 1 ";
        return base.Eliminar(strTabla, strCondicion, false);
    }
    public bool fntEliminarUsuario_bol(int intIdUsuario)
    {
        string strTabla = " Seg_Usuario ";
        string strCondicion = " segusu_Id = @intIdUsuario ";
        base.clearParameters();
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        return base.Eliminar(strTabla, strCondicion, false);
    }
    public bool fntEliminarPermisoFormulario_bol(int intTipo, int intIdUsuario, int intIdPermisoFormulario)
    {
        string strTabla = " Seg_UsuarioPermiso ";
        string text = " segusu_Id = @intIdUsuario";
        base.clearParameters();
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        base.setParameters("@intIdPermisoFormulario", System.Data.SqlDbType.Int, intIdPermisoFormulario);
        if (intTipo == 1)
        {
            text += " AND segusuper_Id = @intIdPermisoFormulario";
        }
        return base.Eliminar(strTabla, text, false);
    }
    public bool fntEliminarGrupoOperador_bol(int intIdGrupoOperador)
    {
        string strTabla = " tbl_GrupoOperador ";
        string strCondicion = " gruOpe_Id = @intIdGrupoOperador ";
        base.clearParameters();
        base.setParameters("@intIdGrupoOperador", System.Data.SqlDbType.Int, intIdGrupoOperador);
        return base.Eliminar(strTabla, strCondicion, false);
    }
    public bool fntEliminarGrupoBiblioteca_bol(int intIdGrupoBiblioteca)
    {
        string strTabla = " tbl_GrupoBiblioteca ";
        string strCondicion = " grubib_Id = @intIdGrupoBiblioteca ";
        base.clearParameters();
        base.setParameters("@intIdGrupoBiblioteca", System.Data.SqlDbType.Int, intIdGrupoBiblioteca);
        return base.Eliminar(strTabla, strCondicion, false);
    }
    public bool fntEliminarGrupoUsuario_bol(int intIdGrupoUsuario)
    {
        string strTabla = " tbl_GrupoUsuario  ";
        string strCondicion = " gruUsu_Id = @intIdGrupoUsuario ";
        base.clearParameters();
        base.setParameters("@intIdGrupoUsuario", System.Data.SqlDbType.Int, intIdGrupoUsuario);
        return base.Eliminar(strTabla, strCondicion, false);
    }
    public bool fntEliminarGrupoUsuarioBiblioteca_bol(int intIdGrupoUsuarioBiblioteca)
    {
        string strTabla = " tbl_BibliotecaUsuario  ";
        string strCondicion = " bibUsu_Id = @intIdGrupoUsuarioBiblioteca ";
        base.clearParameters();
        base.setParameters("@intIdGrupoUsuarioBiblioteca", System.Data.SqlDbType.Int, intIdGrupoUsuarioBiblioteca);
        return base.Eliminar(strTabla, strCondicion, false);
    }
    public bool fntEliminarTodosUsuariosGrupo_bol(int intIdGrupoOperador)
    {
        string strTabla = " tbl_GrupoUsuario  ";
        string strCondicion = " gruOpe_Id = @intIdGrupoOperador ";
        base.clearParameters();
        base.setParameters("@intIdGrupoOperador", System.Data.SqlDbType.Int, intIdGrupoOperador);
        return base.Eliminar(strTabla, strCondicion, false);
    }
    public bool fntEliminarTodosUsuariosGrupoBiblioteca_bol(int intIdGrupoBiblioteca)
    {
        string strTabla = " tbl_BibliotecaUsuario  ";
        string strCondicion = " grubib_Id = @intIdGrupoBiblioteca ";
        base.clearParameters();
        base.setParameters("@intIdGrupoBiblioteca", System.Data.SqlDbType.Int, intIdGrupoBiblioteca);
        return base.Eliminar(strTabla, strCondicion, false);
    }


    /// <summary>
    /// ☻☻ eliminar compromiso 
    /// </summary>
    /// <param name="idCompromiso"></param>
    /// <returns></returns>
    public bool fntEliminarCompromiso(Int64 idCompromiso)
    {
        string strTabla = " WFSol_Compromisos ";
        string strCondicion = " idCompromiso = @idCompromiso ";
        base.clearParameters();
        base.setParameters("@idCompromiso", System.Data.SqlDbType.BigInt, idCompromiso);
        return base.Eliminar(strTabla, strCondicion, false);
    }


    /// <summary>
    /// ☻☻ metodo para elimianr  los benificiarios 
    /// </summary>
    /// <param name="idBeneficioLega"></param>
    /// <returns></returns>
    public bool fntEliminarBeneficiario(Int64 idBeneficioLega)
    {
        string strTabla = " WFSol_BeneficioLegalizacion ";
        string strCondicion = " idBeneficioLega = @idBeneficioLega ";
        base.clearParameters();
        base.setParameters("@idBeneficioLega", System.Data.SqlDbType.BigInt, idBeneficioLega);
        return base.Eliminar(strTabla, strCondicion, false);
    }








    public bool fntModificarPersona_bol(int intIdPersona, int intIdCategoria, int intIdVocativo, int intIdCargo, string strNombre, string strApellido, int intIdInstitucion, string strIdMunicipio, string strDireccion, int intIdTipoDireccion)
    {
        string strTabla = " tbl_Persona ";
        string strCamposValores = " cat_Id = @intIdCategoria, voc_Id = @intIdVocativo, car_Id = @intIdCargo, nombrePersona = @strNombre, apellidoPersona = @strApellido, ins_Id = @intIdInstitucion, codMunicipioAlf5 = @strIdMunicipio, direccion = @strDireccion, tip_Id = @intIdTipoDireccion ";
        string strCondicion = " per_Id = @intIdPersona ";
        base.clearParameters();
        base.setParameters("@intIdPersona", System.Data.SqlDbType.Int, intIdPersona);
        base.setParameters("@intIdCategoria", System.Data.SqlDbType.Int, intIdCategoria);
        base.setParameters("@intIdVocativo", System.Data.SqlDbType.Int, intIdVocativo);
        base.setParameters("@intIdCargo", System.Data.SqlDbType.Int, intIdCargo);
        base.setParameters("@strNombre", System.Data.SqlDbType.NVarChar, strNombre);
        base.setParameters("@strApellido", System.Data.SqlDbType.NVarChar, strApellido);
        base.setParameters("@intIdInstitucion", System.Data.SqlDbType.Int, intIdInstitucion);
        base.setParameters("@strIdMunicipio", System.Data.SqlDbType.NVarChar, strIdMunicipio);
        base.setParameters("@strDireccion", System.Data.SqlDbType.NVarChar, strDireccion);
        base.setParameters("@intIdTipoDireccion", System.Data.SqlDbType.NVarChar, intIdTipoDireccion);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntModificarTelefonoPersona_bol(int intIdPersona, int intIdOp, string strNumeroTelefono, int intIdTipoTelefono, int intIdTelefono, int intEspecificacionTelefono)
    {
        string strTabla = " tbl_Telefono ";
        string strCamposValores = " numTelefono = @strNumeroTelefono, tip_Id = @intIdTipoTelefono, tip_IdTipoTelefono = @intEspecificacionTelefono";
        string text = " per_Id = @intIdPersona ";
        base.clearParameters();
        base.setParameters("@intIdPersona", System.Data.SqlDbType.Int, intIdPersona);
        base.setParameters("@strNumeroTelefono", System.Data.SqlDbType.NVarChar, strNumeroTelefono);
        base.setParameters("@intIdTipoTelefono", System.Data.SqlDbType.Int, intIdTipoTelefono);
        base.setParameters("@intIdTelefono", System.Data.SqlDbType.Int, intIdTelefono);
        base.setParameters("@intEspecificacionTelefono", System.Data.SqlDbType.Int, intEspecificacionTelefono);
        if (intIdOp == 1)
        {
            text += " AND tip_idtipoTelefono in (select tip_Id from tbl_Tipo where tip_Descripcion = 'Telefono')";
        }
        if (intIdOp == 2)
        {
            text += " AND tip_idtipoTelefono in (select tip_Id from tbl_Tipo where tip_Descripcion = 'Celular') ";
        }
        if (intIdOp == 3)
        {
            text += " AND tel_Id = @intIdTelefono ";
        }
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, text, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntModificarCorreoPersona_bol(int intIdPersona, string strCorreoElectronico, int intIdTipoCorreoElectronico, int intIdCorreoElectronico)
    {
        string strTabla = " tbl_CorreoElectronico ";
        string strCamposValores = " direccionCorreoElectronico = @strCorreoElectronico, tip_Id = @intIdTipoCorreoElectronico ";
        string strCondicion = " per_Id = @intIdPersona AND corele_Id = @intIdCorreoElectronico ";
        base.clearParameters();
        base.setParameters("@intIdPersona", System.Data.SqlDbType.Int, intIdPersona);
        base.setParameters("@strCorreoElectronico", System.Data.SqlDbType.NVarChar, strCorreoElectronico);
        base.setParameters("@intIdTipoCorreoElectronico", System.Data.SqlDbType.Int, intIdTipoCorreoElectronico);
        base.setParameters("@intIdCorreoElectronico", System.Data.SqlDbType.Int, intIdCorreoElectronico);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntModificarRecursoProyecto_bol(int intIdRecursoProyecto, int intIdRubroPresupuestal, int intIdProyecto, int intIdTipoProyecto, string strValorTotal, string strNoContrado, string strFechaContrato, int intIdPersona)
    {
        string strTabla = " tbl_RecursoProyecto ";
        string strCamposValores = " rubpre_Id = @intIdRubroPresupuestal, pro_Id = @intIdProyecto, tip_Id = @intIdTipoProyecto, recpro_ValorTotal = @strValorTotal, recpro_NContrato  = @strNoContrado, recpro_fechaContrato = @strFechaContrato, per_Id = @intIdPersona ";
        string strCondicion = " recpro_Id = @intIdRecursoProyecto";
        base.clearParameters();
        base.setParameters("@intIdRecursoProyecto", System.Data.SqlDbType.Int, intIdRecursoProyecto);
        base.setParameters("@intIdRubroPresupuestal", System.Data.SqlDbType.Int, intIdRubroPresupuestal);
        base.setParameters("@intIdProyecto", System.Data.SqlDbType.Int, intIdProyecto);
        base.setParameters("@intIdTipoProyecto", System.Data.SqlDbType.Int, intIdTipoProyecto);
        base.setParameters("@strValorTotal", System.Data.SqlDbType.Float, strValorTotal);
        base.setParameters("@strNoContrado", System.Data.SqlDbType.NVarChar, strNoContrado);
        base.setParameters("@strFechaContrato", System.Data.SqlDbType.Date, strFechaContrato);
        base.setParameters("@intIdPersona", System.Data.SqlDbType.Int, intIdPersona);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntModificarActividadProyecto_bol(int intIdActividadProyecto, int intIdActividad, string strValorProyectado, string strValorContratado, string strValorDisponible, int intIdEstado)
    {
        string strTabla = " tbl_ActividadProyecto ";
        string strCamposValores = " acti_Id = @intIdActividad, actpro_Valorproyectado = @strValorProyectado, actpro_ValorContratado = @strValorContratado, actpro_ValorDisponible = @strValorDisponible, est_Id = @intIdEstado ";
        string strCondicion = " actpro_Id = @intIdActividadProyecto ";
        base.clearParameters();
        base.setParameters("@intIdActividadProyecto", System.Data.SqlDbType.Int, intIdActividadProyecto);
        base.setParameters("@intIdActividad", System.Data.SqlDbType.Int, intIdActividad);
        base.setParameters("@strValorProyectado", System.Data.SqlDbType.Float, strValorProyectado);
        base.setParameters("@strValorContratado", System.Data.SqlDbType.Float, strValorContratado);
        base.setParameters("@strValorDisponible", System.Data.SqlDbType.Float, strValorDisponible);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Float, intIdEstado);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntModificarEventoTaller_bol(int intIdEventoTaller, string strIdMunicipio, int intIdTaller, string strFecha, string strObjeto, int intIdResponsable, string strOperadorLogistico)
    {
        string strTabla = " tbl_EventoTaller ";
        string strCamposValores = " codMunicipioAlf5 = @strIdMunicipio, tal_Id = @intIdTaller, fecha = @strFecha, evetal_Objeto = @strObjeto, res_Id = @intIdResponsable, evetal_operadorLogistico = @strOperadorLogistico ";
        string strCondicion = " evetal_Id = @intIdEventoTaller ";
        base.clearParameters();
        base.setParameters("@intIdEventoTaller", System.Data.SqlDbType.Int, intIdEventoTaller);
        base.setParameters("@strIdMunicipio", System.Data.SqlDbType.NVarChar, strIdMunicipio);
        base.setParameters("@intIdTaller", System.Data.SqlDbType.Int, intIdTaller);
        base.setParameters("@strFecha", System.Data.SqlDbType.Date, strFecha);
        base.setParameters("@strObjeto", System.Data.SqlDbType.NVarChar, strObjeto);
        base.setParameters("@intIdResponsable", System.Data.SqlDbType.Int, intIdResponsable);
        base.setParameters("@strOperadorLogistico", System.Data.SqlDbType.NVarChar, strOperadorLogistico);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntModificarValorTaller_bol(int intIdEventoTaller, string strValorSinIva, string strValorIva, string strValorComision, string strValorTotal, string strSaldo, string strFactura, string strFechaPago, string strNOrdenPagoSIIF, string strActa, string strFechaActa)
    {
        string strTabla = " tbl_ValorTaller ";
        string strCamposValores = " valtal_ValorSinIva = @strValorSinIva, valtal_ValorIva = @strValorIva, valtal_ValorComision = @strValorComision, valtal_ValorTotal = @strValorTotal, valtal_Saldo = @strSaldo, valtal_Factura = @strFactura, fecha = @strFechaPago, valtal_NOrdenPagoSIIF = @strNOrdenPagoSIIF, valtal_NActa = @strActa, FechaActa = @strFechaActa ";
        string strCondicion = " evetal_Id = @intIdEventoTaller ";
        base.clearParameters();
        base.setParameters("@intIdEventoTaller", System.Data.SqlDbType.Int, intIdEventoTaller);
        base.setParameters("@strValorSinIva", System.Data.SqlDbType.NVarChar, strValorSinIva);
        base.setParameters("@strValorIva", System.Data.SqlDbType.NVarChar, strValorIva);
        base.setParameters("@strValorComision", System.Data.SqlDbType.NVarChar, strValorComision);
        base.setParameters("@strValorTotal", System.Data.SqlDbType.NVarChar, strValorTotal);
        base.setParameters("@strSaldo", System.Data.SqlDbType.NVarChar, strSaldo);
        base.setParameters("@strFactura", System.Data.SqlDbType.NVarChar, strFactura);
        base.setParameters("@strFechaPago", System.Data.SqlDbType.Date, strFechaPago);
        base.setParameters("@strNOrdenPagoSIIF", System.Data.SqlDbType.NVarChar, strNOrdenPagoSIIF);
        base.setParameters("@strActa", System.Data.SqlDbType.NVarChar, strActa);
        base.setParameters("@strFechaActa", System.Data.SqlDbType.Date, strFechaActa);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntModificarUsuario_bol(string strPassword, int intIdEstado, int intIdUsuario, string strCorreo)
    {
        string strTabla = " Seg_Usuario ";
        string text = " est_Id = @intIdEstado, segusu_Correo = @strCorreo  ";
        string strCondicion = " segusu_Id = @intIdUsuario ";
        base.clearParameters();
        base.setParameters("@strPassword", System.Data.SqlDbType.NVarChar, strPassword);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        base.setParameters("@strCorreo", System.Data.SqlDbType.NVarChar, strCorreo);
        if (strPassword != string.Empty)
        {
            text += " , segusu_Password = @strPassword ";
        }
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, text, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntModificarPermisoFormulario_bol(int intIdSeguridadUsuarioFormulario, int intConsulta, int intCreacion, int intModificar, int intEliminar, int intImpresion)
    {
        string strTabla = " Seg_UsuarioPermiso ";
        string strCamposValores = " segusuper_Consulta = @intConsulta , segusuper_Creacion = @intCreacion , segusuper_Modificar = @intModificar , segusuper_Eliminar = @intEliminar , segusuper_Impresion = @intImpresion ";
        string strCondicion = " segusuper_Id = @intIdSeguridadUsuarioFormulario ";
        base.clearParameters();
        base.setParameters("@intIdSeguridadUsuarioFormulario", System.Data.SqlDbType.Int, intIdSeguridadUsuarioFormulario);
        base.setParameters("@intConsulta", System.Data.SqlDbType.Int, intConsulta);
        base.setParameters("@intCreacion", System.Data.SqlDbType.Int, intCreacion);
        base.setParameters("@intModificar", System.Data.SqlDbType.Int, intModificar);
        base.setParameters("@intEliminar", System.Data.SqlDbType.Int, intEliminar);
        base.setParameters("@intImpresion", System.Data.SqlDbType.Int, intImpresion);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntModificarGrupoOperador_bol(int intIdGrupoOperador, string strNombreGrupo, string strDescripcion, int intIdEstado)
    {
        string strTabla = " tbl_GrupoOperador ";
        string strCamposValores = " gruOpe_Nombre = @strNombreGrupo , gruOpe_Descripcion = @strDescripcion, est_Id = @intIdEstado ";
        string strCondicion = " gruOpe_Id = @intIdGrupoOperador ";
        base.clearParameters();
        base.setParameters("@intIdGrupoOperador", System.Data.SqlDbType.Int, intIdGrupoOperador);
        base.setParameters("@strNombreGrupo", System.Data.SqlDbType.NVarChar, strNombreGrupo);
        base.setParameters("@strDescripcion", System.Data.SqlDbType.NVarChar, strDescripcion);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntModificarGrupoBiblioteca_bol(int intIdGrupoBiblioteca, string strNombreGrupo, string strDescripcion, int intIdEstado)
    {
        string strTabla = " tbl_GrupoBiblioteca  ";
        string strCamposValores = " grubib_Nombre = @strNombreGrupo , grubib_Descripcion = @strDescripcion, est_Id = @intIdEstado ";
        string strCondicion = " grubib_Id = @intIdGrupoBiblioteca ";
        base.clearParameters();
        base.setParameters("@intIdGrupoBiblioteca", System.Data.SqlDbType.Int, intIdGrupoBiblioteca);
        base.setParameters("@strNombreGrupo", System.Data.SqlDbType.NVarChar, strNombreGrupo);
        base.setParameters("@strDescripcion", System.Data.SqlDbType.NVarChar, strDescripcion);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntModificarDatosGeneralesVictima_bol(int intIdVictima, int intIdTipoDocumento, string strNumeroIdentidicacion, string strNombre, string strApellido, string strNumTelefono, string strDireccion, string strFecha, int intEdad, int intIdGenero, int intIdEtnia)
    {
        string strTabla = " tbl_Victima ";
        string strCamposValores = " tip_Id = @intIdTipoDocumento, identificacionNacionalPersona = @strNumeroIdentidicacion, nombrePersona = @strNombre, apellidoPersona = @strApellido, numTelefono = @strNumTelefono, direccion = @strDireccion, fecha = @strFecha, edad = @intEdad, tip_IdGenero = @intIdGenero, etn_Id = @intIdEtnia ";
        string strCondicion = " vic_Id = @intIdVictima ";
        base.clearParameters();
        base.setParameters("@intIdVictima", System.Data.SqlDbType.Int, intIdVictima);
        base.setParameters("@intIdTipoDocumento", System.Data.SqlDbType.Int, intIdTipoDocumento);
        base.setParameters("@strNumeroIdentidicacion", System.Data.SqlDbType.NVarChar, strNumeroIdentidicacion);
        base.setParameters("@strNombre", System.Data.SqlDbType.NVarChar, strNombre);
        base.setParameters("@strApellido", System.Data.SqlDbType.NVarChar, strApellido);
        base.setParameters("@strNumTelefono", System.Data.SqlDbType.NVarChar, strNumTelefono);
        base.setParameters("@strDireccion", System.Data.SqlDbType.NVarChar, strDireccion);
        base.setParameters("@strFecha", System.Data.SqlDbType.NVarChar, strFecha);
        base.setParameters("@intEdad", System.Data.SqlDbType.Int, intEdad);
        base.setParameters("@intIdGenero", System.Data.SqlDbType.Int, intIdGenero);
        base.setParameters("@intIdEtnia", System.Data.SqlDbType.Int, intIdEtnia);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntModificarDatosAccidenteVictima_bol(int intIdVictima, int intIdVereda, int intIdAreaPoblacional, string strSitioAccidente, string strCondicion, int intIdEstado, string strActividadAccidente, int intAmputacionInicial, int intAmputacionFin, int intHerida, int intEsquirla, int intQuemadura, int intFractura, int intInfeccion, int intAfectacionVisual, int intAfectacionAuditiva, string strDiagnosticoMedico, string strDescripcionHechos, string strGestion)
    {
        string strTabla = " tbl_MonitoreoSeguimiento  ";
        string strCamposValores = " ver_Id = @intIdVereda, arepob_Id = @intIdAreaPoblacional, monseg_SitioAccidente = @strSitioAccidente, monseg_Condicion = @strCondicion, est_Id = @intIdEstado, monseg_ActividadAccidente = @strActividadAccidente, monseg_AmputacionInicial = @intAmputacionInicial, monseg_AmputacionFin = @intAmputacionFin, monseg_Herida = @intHerida, monseg_Esquirla = @intEsquirla, monseg_Quemadura = @intQuemadura, monseg_Fractura = @intFractura, monseg_Infeccion = @intInfeccion, monseg_AfectacionVisual = @intAfectacionVisual, monseg_AfectacionAuditiva = @intAfectacionAuditiva, DiagnosticoMedico = @strDiagnosticoMedico, monseg_DescripcionHechos = @strDescripcionHechos, monseg_Gestion = @strGestion ";
        string strCondicion2 = " vic_Id = @intIdVictima ";
        base.clearParameters();
        base.setParameters("@intIdVictima", System.Data.SqlDbType.Int, intIdVictima);
        base.setParameters("@intIdVereda", System.Data.SqlDbType.Int, intIdVereda);
        base.setParameters("@intIdAreaPoblacional", System.Data.SqlDbType.Int, intIdAreaPoblacional);
        base.setParameters("@strSitioAccidente", System.Data.SqlDbType.NVarChar, strSitioAccidente);
        base.setParameters("@strCondicion", System.Data.SqlDbType.NVarChar, strCondicion);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        base.setParameters("@strActividadAccidente", System.Data.SqlDbType.NVarChar, strActividadAccidente);
        base.setParameters("@intAmputacionInicial", System.Data.SqlDbType.Int, intAmputacionInicial);
        base.setParameters("@intAmputacionFin", System.Data.SqlDbType.Int, intAmputacionFin);
        base.setParameters("@intHerida", System.Data.SqlDbType.Int, intHerida);
        base.setParameters("@intEsquirla", System.Data.SqlDbType.Int, intEsquirla);
        base.setParameters("@intQuemadura", System.Data.SqlDbType.Int, intQuemadura);
        base.setParameters("@intFractura", System.Data.SqlDbType.Int, intFractura);
        base.setParameters("@intInfeccion", System.Data.SqlDbType.Int, intInfeccion);
        base.setParameters("@intAfectacionVisual", System.Data.SqlDbType.Int, intAfectacionVisual);
        base.setParameters("@intAfectacionAuditiva", System.Data.SqlDbType.Int, intAfectacionAuditiva);
        base.setParameters("@strDiagnosticoMedico", System.Data.SqlDbType.NVarChar, strDiagnosticoMedico);
        base.setParameters("@strDescripcionHechos", System.Data.SqlDbType.NVarChar, strDescripcionHechos);
        base.setParameters("@strGestion", System.Data.SqlDbType.NVarChar, strGestion);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion2, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntModificarVersionBiblioteca_bol(int intIdBiblioteca, int intIdEstado)
    {
        string strTabla = " tbl_Biblioteca ";
        string strCamposValores = " est_Id = @intIdEstado ";
        string strCondicion = " bib_Id = @intIdBiblioteca ";
        base.clearParameters();
        base.setParameters("@intIdBiblioteca", System.Data.SqlDbType.Int, intIdBiblioteca);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.Int, intIdEstado);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntModificarCorreo_bol(int intIdCorreo, string strServidor, int intPuerto, string strUsuario, string strPassword, string strNombre, int intIdEstado)
    {
        string strTabla = " seg_Correo ";
        string strCamposValores = " segCor_Server = @strServidor, segCor_Port = @intPuerto, segCor_User = @strUsuario, segCor_Password = @strPassword, segCor_Nombre = @strNombre, segCor_Estado = @intIdEstado ";
        string strCondicion = " segCor_Id = @intIdCorreo ";
        base.clearParameters();
        base.setParameters("@intIdCorreo", System.Data.SqlDbType.Int, intIdCorreo);
        base.setParameters("@strServidor", System.Data.SqlDbType.NVarChar, strServidor);
        base.setParameters("@intPuerto", System.Data.SqlDbType.Int, intPuerto);
        base.setParameters("@strUsuario", System.Data.SqlDbType.NVarChar, strUsuario);
        base.setParameters("@strPassword", System.Data.SqlDbType.NVarChar, strPassword);
        base.setParameters("@strNombre", System.Data.SqlDbType.NVarChar, strNombre);
        base.setParameters("@intIdEstado", System.Data.SqlDbType.NVarChar, intIdEstado);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }
    public bool fntIngresarERM1_bol(string strRutaArchivo)
    {
        string strCampos = " erm1, erm2, erm3, erm4, erm5, erm6, erm7, erm8, erm9, erm10, erm11, erm12, erm13, erm14, erm15, erm16, erm17, erm18, erm19  ";
        string strTabla = " ERM1 ";
        string strCampos2 = " * ";
        string text = string.Empty;
        string strCondicion = "1=1";
        text = "OPENROWSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=";
        text += strRutaArchivo;
        text += ";HDR=NO', 'SELECT * FROM [Hoja1$]')";
        base.clearParameters();
        base.setParameters("@strRutaArchivo", System.Data.SqlDbType.NVarChar, strRutaArchivo);
        base.iniciarTransaccion();
        if (base.InsertarSelect(strTabla, strCampos, strCampos2, text, strCondicion, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarERM2_bol(string strRutaArchivo)
    {
        string strCampos = " erm1, erm2, erm3, erm4, erm5, erm6, erm7, erm8, erm9  ";
        string strTabla = " tmp_ERM2 ";
        string strCampos2 = " * ";
        string text = string.Empty;
        string strCondicion = "1=1";
        text = "OPENROWSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=";
        text += strRutaArchivo;
        text += ";HDR=NO', 'SELECT * FROM [Hoja1$]')";
        base.clearParameters();
        base.setParameters("@strRutaArchivo", System.Data.SqlDbType.NVarChar, strRutaArchivo);
        base.iniciarTransaccion();
        if (base.InsertarSelect(strTabla, strCampos, strCampos2, text, strCondicion, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarERM3_bol(string strRutaArchivo)
    {
        string strCampos = " erm1, erm2, erm3, erm4, erm5, erm6, erm7, erm8, erm9, erm10, erm11, erm12, erm13, erm14 ";
        string strTabla = " tmp_ERM3 ";
        string strCampos2 = " * ";
        string text = string.Empty;
        string strCondicion = "1=1";
        text = "OPENROWSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=";
        text += strRutaArchivo;
        text += ";HDR=NO', 'SELECT * FROM [Hoja1$]')";
        base.clearParameters();
        base.setParameters("@strRutaArchivo", System.Data.SqlDbType.NVarChar, strRutaArchivo);
        base.iniciarTransaccion();
        if (base.InsertarSelect(strTabla, strCampos, strCampos2, text, strCondicion, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarERM4_bol(string strRutaArchivo)
    {
        string strCampos = " erm1, erm2, erm3, erm4, erm5, erm6, erm7, erm8, erm9, erm10, erm11, erm12, erm13, erm14 ";
        string strTabla = " tmp_ERM4 ";
        string strCampos2 = " * ";
        string text = string.Empty;
        string strCondicion = "1=1";
        text = "OPENROWSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=";
        text += strRutaArchivo;
        text += ";HDR=NO', 'SELECT * FROM [Hoja1$]')";
        base.clearParameters();
        base.setParameters("@strRutaArchivo", System.Data.SqlDbType.NVarChar, strRutaArchivo);
        base.iniciarTransaccion();
        if (base.InsertarSelect(strTabla, strCampos, strCampos2, text, strCondicion, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarAG2_bol(string strRutaArchivo)
    {
        string strCampos = " ag1, ag2, ag3, ag4, ag5, ag6, ag7, ag8, ag9, ag10, ag11, ag12, ag13, ag14 ";
        string strTabla = " tmp_AG2 ";
        string strCampos2 = " * ";
        string text = string.Empty;
        string strCondicion = "1=1";
        text = "OPENROWSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=";
        text += strRutaArchivo;
        text += ";HDR=NO', 'SELECT * FROM [Hoja1$]')";
        base.clearParameters();
        base.setParameters("@strRutaArchivo", System.Data.SqlDbType.NVarChar, strRutaArchivo);
        base.iniciarTransaccion();
        if (base.InsertarSelect(strTabla, strCampos, strCampos2, text, strCondicion, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarAG3_bol(string strRutaArchivo)
    {
        string strCampos = " ag1, ag2, ag3, ag4, ag5, ag6, ag7, ag8, ag9, ag10, ag11, ag12, ag13, ag14, ag15, ag16, ag17, ag18, ag19, ag20, ag21, ag22, ag23, ag24 ";
        string strTabla = " tmp_AG3 ";
        string strCampos2 = " * ";
        string text = string.Empty;
        string strCondicion = "1=1";
        text = "OPENROWSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=";
        text += strRutaArchivo;
        text += ";HDR=NO', 'SELECT * FROM [Hoja1$]')";
        base.clearParameters();
        base.setParameters("@strRutaArchivo", System.Data.SqlDbType.NVarChar, strRutaArchivo);
        base.iniciarTransaccion();
        if (base.InsertarSelect(strTabla, strCampos, strCampos2, text, strCondicion, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarAV1_bol(string strRutaArchivo)
    {
        string strCampos = " av1, av2, av3, av4, av5, av6, av7, av8, av9, av10, av11, av12, av13, av14, av15, av16, av17, av18, av19, av20, av21, av22, av23, av24, av25, av26, av27, av28, av29, av30, av31, av32, av33, av34, av35, av36, av37 ";
        string strTabla = " tmp_AV1 ";
        string strCampos2 = " * ";
        string text = string.Empty;
        string strCondicion = "1=1";
        text = "OPENROWSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=";
        text += strRutaArchivo;
        text += ";HDR=NO', 'SELECT * FROM [Hoja1$]')";
        base.clearParameters();
        base.setParameters("@strRutaArchivo", System.Data.SqlDbType.NVarChar, strRutaArchivo);
        base.iniciarTransaccion();
        if (base.InsertarSelect(strTabla, strCampos, strCampos2, text, strCondicion, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarGT1_bol(string strRutaArchivo)
    {
        string strCampos = " gt1, gt2, gt3, gt4, gt5, gt6, gt7, gt8, gt9, gt10, gt11, gt12, gt13, gt14, gt15 ";
        string strTabla = " tmp_GT1 ";
        string strCampos2 = " * ";
        string text = string.Empty;
        string strCondicion = "1=1";
        text = "OPENROWSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=";
        text += strRutaArchivo;
        text += ";HDR=NO', 'SELECT * FROM [Hoja1$]')";
        base.clearParameters();
        base.setParameters("@strRutaArchivo", System.Data.SqlDbType.NVarChar, strRutaArchivo);
        base.iniciarTransaccion();
        if (base.InsertarSelect(strTabla, strCampos, strCampos2, text, strCondicion, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarUE1_bol(string strRutaArchivo)
    {
        string strCampos = " ue1, ue2, ue3 ";
        string strTabla = " tmp_UE1 ";
        string strCampos2 = " * ";
        string text = string.Empty;
        string strCondicion = "1=1";
        text = "OPENROWSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=";
        text += strRutaArchivo;
        text += ";HDR=NO', 'SELECT * FROM [Hoja1$]')";
        base.clearParameters();
        base.setParameters("@strRutaArchivo", System.Data.SqlDbType.NVarChar, strRutaArchivo);
        base.iniciarTransaccion();
        if (base.InsertarSelect(strTabla, strCampos, strCampos2, text, strCondicion, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarUE2_bol(string strRutaArchivo)
    {
        string strCampos = " ue1, ue2, ue3, ue4, ue5, ue6, ue7, ue8, ue9, ue10, ue11, ue12, ue13, ue14, ue15, ue16, ue17, ue18, ue19, ue20, ue21 ";
        string strTabla = " tmp_UE2 ";
        string strCampos2 = " * ";
        string text = string.Empty;
        string strCondicion = "1=1";
        text = "OPENROWSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=";
        text += strRutaArchivo;
        text += ";HDR=NO', 'SELECT * FROM [Hoja1$]')";
        base.clearParameters();
        base.setParameters("@strRutaArchivo", System.Data.SqlDbType.NVarChar, strRutaArchivo);
        base.iniciarTransaccion();
        if (base.InsertarSelect(strTabla, strCampos, strCampos2, text, strCondicion, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntIngresarTablaDinamica_bol(string strRutaArchivo, string strTabla, string strCampos)
    {
        string strCampos2 = " * ";
        string text = string.Empty;
        string strCondicion = "1=1";
        text = "OPENROWSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=";
        text += strRutaArchivo;
        text += ";HDR=NO', 'SELECT * FROM [Hoja1$]')";
        base.clearParameters();
        base.setParameters("@strRutaArchivo", System.Data.SqlDbType.NVarChar, strRutaArchivo);
        base.iniciarTransaccion();
        if (base.InsertarSelect(strTabla, strCampos, strCampos2, text, strCondicion, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntValidarSeguridadEMR1(string dataset)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " ERM1 ";
        string text = " erm1 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm2 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm3 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm4 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm5 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm6 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm7 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm8 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm9 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm10 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm11 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm12 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm13 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm14 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm15 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm16 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm17 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm18 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm19 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        string strOrden = " CANTIDAD ";
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntValidarSeguridadEMR2(string dataset)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " tmp_ERM2 ";
        string text = " erm1 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm2 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm3 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm4 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm5 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm6 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm7 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm8 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm9 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        string strOrden = " CANTIDAD ";
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntValidarSeguridadEMR3(string dataset)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " tmp_ERM3 ";
        string text = " erm1 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm2 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm3 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm4 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm5 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm6 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm7 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm8 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm9 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm10 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm11 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm12 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm13 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm14 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        string strOrden = " CANTIDAD ";
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntValidarSeguridadEMR4(string dataset)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " tmp_ERM4 ";
        string text = " erm1 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm2 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm3 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm4 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm5 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm6 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm7 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm8 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm9 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm10 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm11 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm12 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm13 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR erm14 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        string strOrden = " CANTIDAD ";
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntValidarSeguridadAG2(string dataset)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " tmp_AG2 ";
        string text = " ag1 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag2 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag3 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag4 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag5 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag6 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag7 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag8 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag9 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag10 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag11 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag12 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag13 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag14 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        string strOrden = " CANTIDAD ";
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntValidarSeguridadAG3(string dataset)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " tmp_AG3 ";
        string text = " ag1 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag2 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag3 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag4 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag5 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag6 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag7 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag8 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag9 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag10 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag11 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag12 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag13 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag14 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag15 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag16 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag17 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag18 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag19 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag20 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag21 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag22 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag23 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ag24 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        string strOrden = " CANTIDAD ";
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntValidarSeguridadAV1(string dataset)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " tmp_AV1 ";
        string text = " av1 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av2 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av3 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av4 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av5 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av6 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av7 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av8 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av9 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av10 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av11 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av12 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av13 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av14 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av15 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av16 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av17 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av18 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av19 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av20 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av21 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av22 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av23 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av24 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av25 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av26 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av27 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av28 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av29 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av30 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av31 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av32 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av33 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av34 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av35 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av36 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR av37 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        string strOrden = " CANTIDAD ";
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntValidarSeguridadGT1(string dataset)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " tmp_GT1 ";
        string text = " gt1 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR gt2 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR gt3 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR gt4 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR gt5 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR gt6 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR gt7 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR gt8 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR gt9 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR gt10 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR gt11 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR gt12 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR gt13 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR gt14 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR gt15 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        string strOrden = " CANTIDAD ";
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntValidarSeguridadUE1(string dataset)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " tmp_UE1 ";
        string text = " ue1 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue2 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue3 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        string strOrden = " CANTIDAD ";
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntValidarSeguridadUE2(string dataset)
    {
        string strCampos = " COUNT(*) CANTIDAD ";
        string strTabla = " tmp_UE2 ";
        string text = " ue1 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue2 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue3 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue4 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue5 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue6 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue7 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue8 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue9 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue10 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue11 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue12 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue13 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue14 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue15 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue16 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue17 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue18 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue19 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue20 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        text += " OR ue21 IN ('SELECT', 'WHERE', 'FROM', 'DELETE', 'ORDER', 'ALTER', 'DROP', 'CREATE', 'EXEC') ";
        string strOrden = " CANTIDAD ";
        return base.Consultar(strCampos, strTabla, text, strOrden, ref dataset, false, "");
    }
    public bool fntCrearTablaNueva_bol(string strTabla, string strCampos)
    {
        string lstrTabla = strTabla;
        string lstrCampos = "[id] [bigint] IDENTITY(1,1) NOT NULL, [fechaCarga] [nvarchar](30) NOT NULL, ";
        lstrCampos = lstrCampos + "  " + strCampos;

        lstrCampos = lstrCampos + " CONSTRAINT [PK_" + strTabla + "] PRIMARY KEY CLUSTERED ( [id] ASC ) ";


        base.iniciarTransaccion();
        if (NewCreateTable(lstrTabla, lstrCampos, true))
        {
            commit();
            return true;
        }
        else
        {
            rollback();
            return false;
        }
    }
    public bool fntEliminarTablaNueva_bol(string strTabla)
    {
        base.iniciarTransaccion();
        if (base.NewDropTable(strTabla, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    public bool fntConsultarMaximoCodigoDocumento(string dataset)
    {
        string strCampos = " MAX(bib_CodigoDocumento) CODIGO ";
        string strTabla = " tbl_Biblioteca  ";
        string strCondicion = " 1=1 ";
        string strOrden = " CODIGO ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }

    #region FORMULARIOSDINAMICOS
    //===============================================SECCION NUEVA FORMULARIOS ANIBAL  JUN-2015=======================================
    //Inserta en la tabla de adminstracion de creacion de nuevos formularios(Tablas)
    //fntInsertarFormAdmTab_bol
    //IN: parametros a insertar
    //OUT: Boolean para confirmar la insercion
    public bool fntInsertarFormAdmTab_bol(string strNombreTabla, string strAbreviatura, int intNumeroCampos, int intIdUsuario, string strObservacion, bool relVictima, int filtro)
    {
        string strCampos = " admTab_NombreTabla, admTab_AbreviaturaTabla, admTab_NumeroCampos, admTab_fecha, segusu_Id, admTab_Activacion, admTab_Observacion , admTab_RelVictima , admTab_FiltroSegundo ";
        string strTabla = " Form_AdmTablas ";
        string strValores = " @strNombreTabla, @strAbreviatura,  @intNumeroCampos, GETDATE(), @intIdUsuario, 1, @strObservacion, @admTab_RelVictima, @admTab_FiltroSegundo ";
        string strCondicion = " admTab_NombreTabla = @strNombreTabla AND admTab_AbreviaturaTabla = @strAbreviatura AND admTab_NumeroCampos = @intNumeroCampos AND segusu_Id =  @intIdUsuario AND admTab_Activacion = 1 AND admTab_Observacion = @strObservacion AND admTab_RelVictima = @admTab_RelVictima AND admTab_FiltroSegundo = @admTab_FiltroSegundo ";
        base.clearParameters();
        base.setParameters("@strNombreTabla", System.Data.SqlDbType.NVarChar, strNombreTabla);
        base.setParameters("@strAbreviatura", System.Data.SqlDbType.NVarChar, strAbreviatura);
        base.setParameters("@intNumeroCampos", System.Data.SqlDbType.Int, intNumeroCampos);
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        base.setParameters("@strObservacion", System.Data.SqlDbType.NVarChar, strObservacion);
        base.setParameters("@admTab_RelVictima", System.Data.SqlDbType.Bit, relVictima);
        base.setParameters("@admTab_FiltroSegundo", System.Data.SqlDbType.Int, filtro);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }


    //public bool fntIngresarAdmTablas_bol(string strNombreTabla, string strAbreviatura, int intNumeroCampos, int intIdUsuario, string strObservacion)
    //{
    //    string strCampos = " admTab_NombreTabla, admTab_AbreviaturaTabla, admTab_NumeroCampos, admTab_fecha, segusu_Id, admTab_Activacion, admTab_Observacion ";
    //    string strTabla = " Seg_AdmTablas ";
    //    string strValores = " @strNombreTabla, @strAbreviatura,  @intNumeroCampos, GETDATE(), @intIdUsuario, 1, @strObservacion ";
    //    string strCondicion = " admTab_NombreTabla = @strNombreTabla AND admTab_AbreviaturaTabla = @strAbreviatura AND admTab_NumeroCampos = @intNumeroCampos AND segusu_Id =  @intIdUsuario AND admTab_Activacion = 1 AND admTab_Observacion = @strObservacion ";
    //    base.clearParameters();
    //    base.setParameters("@strNombreTabla", System.Data.SqlDbType.NVarChar, strNombreTabla);
    //    base.setParameters("@strAbreviatura", System.Data.SqlDbType.NVarChar, strAbreviatura);
    //    base.setParameters("@intNumeroCampos", System.Data.SqlDbType.Int, intNumeroCampos);
    //    base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
    //    base.setParameters("@strObservacion", System.Data.SqlDbType.NVarChar, strObservacion);
    //    base.iniciarTransaccion();
    //    if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
    //    {
    //        base.commit();
    //        return true;
    //    }
    //    base.rollback();
    //    return false;
    //}

    //Elimina de la tabla el registro cuando un formulario es borrado
    //fntInsertarFormAdmTab_bol
    //IN: ID a eliminar
    //OUT: Boolean para confirmar la eliminacion (FISICA)
    public bool fntEliminarFilaAdmTab_bol(int intIdAdministracionTabla)
    {
        string strTabla = " Form_AdmTablas ";
        string strCondicion = " admTab_Id = @intIdAdministracionTabla ";
        base.clearParameters();
        base.setParameters("@intIdAdministracionTabla", System.Data.SqlDbType.Int, intIdAdministracionTabla);
        return base.Eliminar(strTabla, strCondicion, false);
    }



    //Consulta la tabla FormAdm y la enlaza con usuario para conseguir el listado de los formulario creados por usuario
    //fntConsultaAdmTab_bol
    //IN: DataSet donde volcara la informacion y los parametros de filtro
    //OUT: DataSet con la data y retorna un bool de exito
    public bool fntConsultaAdmTab_bol(string dataset, int intActivacion, int intOP, string strNombreTabla, int intIdTabla)
    {
        string lstrCampos = " T.admTab_Id, SUBSTRING(T.admTab_NombreTabla,6,50) admTab_NombreTabla, T.admTab_AbreviaturaTabla, T.admTab_NumeroCampos, T.admTab_fecha, T.segusu_Id, T.admTab_Activacion, T.admTab_Observacion, U.segusu_login ";
        string lstrTabla = " Form_AdmTablas T, Seg_Usuario U ";
        string lstrCondicion = " T.segusu_Id = U.segusu_Id AND admTab_Activacion = @intActivacion ";
        string lstrOrden = " admTab_NombreTabla ";

        base.clearParameters();
        setParameters("@intActivacion", SqlDbType.Int, intActivacion);
        setParameters("@strNombreTabla", SqlDbType.NVarChar, "%" + strNombreTabla + "%");
        setParameters("@intIdTabla", SqlDbType.Int, intIdTabla);

        if (intOP == 1)
        {
            lstrCondicion = string.Empty;
            if (intActivacion != 3)
            {
                lstrCondicion = " T.segusu_Id = U.segusu_Id AND admTab_Activacion = @intActivacion ";
                if (strNombreTabla != string.Empty)
                {
                    lstrCondicion = lstrCondicion + " AND admTab_NombreTabla LIKE @strNombreTabla ";
                }
            }
            else
            {
                if (strNombreTabla != string.Empty)
                {
                    lstrCondicion = " T.segusu_Id = U.segusu_Id AND  admTab_NombreTabla LIKE @strNombreTabla ";
                }

            }
        }

        if (intOP == 2)
        {
            lstrCampos = " T.admTab_Id, SO.NAME, T.admTab_AbreviaturaTabla, T.admTab_Observacion, T.admTab_fecha, SC.column_id, SC.name AS Campos, ST.name AS TipoDato, SC.max_length, SC.precision, SC.scale, CASE WHEN SC.is_nullable = 1 THEN 'SI' ELSE 'NO' END is_nullable ";
            lstrTabla = " sys.objects SO INNER JOIN sys.columns SC ON SO.OBJECT_ID = SC.OBJECT_ID INNER JOIN SYS.types ST ON SC.system_type_id = ST.system_type_id INNER JOIN Form_AdmTablas T ON SO.NAME = T.admTab_NombreTabla ";
            lstrCondicion = " SO.TYPE = 'U' AND T.admTab_Id = @intIdTabla AND ST.name NOT IN ('sysname') AND SC.column_id NOT IN (1)  ";
            lstrOrden = " SC.column_id ";
        }

        if (intOP == 3)
        {
            lstrCampos = " T.admTab_Id, SO.NAME, T.admTab_AbreviaturaTabla, T.admTab_Observacion, T.admTab_fecha, SC.column_id, SC.name AS Campos, ST.name AS TipoDato, SC.max_length, SC.precision, SC.scale, CASE WHEN SC.is_nullable = 1 THEN 'SI' ELSE 'NO' END is_nullable ";
            lstrTabla = " sys.objects SO INNER JOIN sys.columns SC ON SO.OBJECT_ID = SC.OBJECT_ID INNER JOIN SYS.types ST ON SC.system_type_id = ST.system_type_id INNER JOIN Form_AdmTablas T ON SO.NAME = T.admTab_NombreTabla ";
            lstrCondicion = " SO.TYPE = 'U' AND T.admTab_Id = @intIdTabla AND ST.name NOT IN ('sysname') AND SC.column_id NOT IN (1)  ";
            lstrOrden = " SC.column_id ";
        }


        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    //Inserta en la tabla FormCampos lo que seria la lista de controles para renderizar en la creacion de formularios
    //fntInsertarFormCampos_bol
    //IN: Modelo de Controles con todo los campos
    //OUT: retorna un bool de exito
    public bool fntInsertarFormCampos_bol(Controles _controles, int idTabla)
    {
        string strCampos = " camForm_TipoControl, camForm_NombreCampo, camForm_Label, camForm_Orden, camForm_LongitudMax, camForm_admTab_Id, camForm_ShowGrid  ";
        string strTabla = " Form_CampoTabla ";
        string strValores = " @intTipoControl, @strNombreCampo,  @strLabel, @intOrden, @intLogitud, @intIdTabla, @showInGrid ";
        string strCondicion = " camForm_TipoControl = @intTipoControl AND camForm_NombreCampo = @strNombreCampo AND camForm_Label = @strLabel AND camForm_Orden =  @intOrden AND camForm_LongitudMax = @intLogitud AND camForm_admTab_Id = @intIdTabla AND camForm_ShowGrid = @showInGrid ";
        base.clearParameters();
        base.setParameters("@intTipoControl", System.Data.SqlDbType.SmallInt, _controles.TipoControl);
        base.setParameters("@strNombreCampo", System.Data.SqlDbType.NVarChar, _controles.NombreCampo);
        base.setParameters("@strLabel", System.Data.SqlDbType.NVarChar, _controles.Label);
        base.setParameters("@intOrden", System.Data.SqlDbType.Int, _controles.Orden);
        base.setParameters("@intLogitud", System.Data.SqlDbType.Int, _controles.LongitudMax);
        base.setParameters("@intIdTabla", System.Data.SqlDbType.Int, idTabla);
        base.setParameters("@showInGrid", System.Data.SqlDbType.Bit, _controles.checkShowTable);
        base.iniciarTransaccion();



        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }


    //Consulta la lista de Datos que tiene la tabla.
    //fntInsertarFormCampos_bol
    //IN: Modelo de Controles con todo los campos
    //OUT: retorna un bool de exito
    public Boolean fntConsultaDatosForm(string dataset, string nombreTabla)
    {
        string lstrCampos = " fechaCarga ";
        string lstrTabla = " " + nombreTabla + " ";
        string lstrCondicion = string.Empty;
        string groupBy = " fechaCarga ";
        string lstrOrden = " fechaCarga DESC ";

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, groupBy, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }



    public Boolean fntConsultaDatosFormCompleto(string dataset, string nombreTabla, bool relVictima, bool relMunicipio, string nombreCampoMunicipio, string abreTab)
    {
        string lstrCampos = string.Empty;
        string lstrTabla = string.Empty;
        string lstrCondicion = string.Empty;
        //string groupBy = " fechaCarga ";
        string lstrOrden = string.Empty;

        if (relVictima && relMunicipio)
        {
            lstrCampos = " V.ID_IMSMAVictima as " + abreTab + "_idVictimaSP, V.Nombres as " + abreTab + "_NombreVictimaSP, V.Apellidos as " + abreTab + "_ApellidoVictimaSP, D.nomDepartamento as " + abreTab + "_nomDepartamentoSP , M.nomMunicipio as " + abreTab + "_nomMunicipioSP, T.* ";
            lstrTabla = nombreTabla + " as T, Victima as V, tbl_Departamento as D, tbl_Municipio as M ";
            lstrCondicion = " T." + abreTab + "_IdVictimaRel = V.ID_IMSMAVictima and T." + abreTab + "_" + nombreCampoMunicipio + " = M.codMunicipioAlf5 AND M.codDepartamentoAlf2 = D.codDepartamentoAlf2 ";
            lstrOrden = "  V.Nombres DESC ";
        }
        else
        {
            if (relVictima)
            {
                lstrCampos = " V.ID_IMSMAVictima as " + abreTab + "_idVictimaSP, V.Nombres as " + abreTab + "_NombreVictimaSP,  V.Apellidos as " + abreTab + "_ApellidoVictimaSP, T.* ";
                lstrTabla = nombreTabla + " as T, Victima as V ";
                lstrCondicion = " T." + abreTab + "_IdVictimaRel = V.ID_IMSMAVictima ";
                lstrOrden = "  V.Nombres DESC ";
            }
            else
            {
                if (relMunicipio)
                {
                    lstrCampos = " D.nomDepartamento as " + abreTab + "_nomDepartamentoSP, M.nomMunicipio as " + abreTab + "_nomMunicipioSP,  T.*  ";
                    lstrTabla = nombreTabla + " as T, tbl_Departamento as D, tbl_Municipio as M ";
                    lstrCondicion = " T." + abreTab + "_" + nombreCampoMunicipio + " = M.codMunicipioAlf5 AND M.codDepartamentoAlf2 = D.codDepartamentoAlf2  ";
                    lstrOrden = " M.nomMunicipio DESC ";
                }
                else
                {
                    lstrCampos = " T.* ";
                    lstrTabla = " " + nombreTabla + " as T ";
                    lstrCondicion = string.Empty;
                    lstrOrden = string.Empty;
                }
            }
        }
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    public Boolean fntConsultaDatosFormCompletoPorFecha(string dataset, string nombreTabla, string fechaD, string fechaH, bool relVictima, bool relMunicipio, string nombreCampoMunicipio, string abreTab)
    {

        string lstrCampos = string.Empty;
        string lstrTabla = string.Empty;
        string lstrCondicion = string.Empty;
        //string groupBy = " fechaCarga ";
        string lstrOrden = string.Empty;

        if (relVictima && relMunicipio)
        {
            lstrCampos = " V.ID_IMSMAVictima as " + abreTab + "_idVictimaSP, V.Nombres as " + abreTab + "_NombreVictimaSP, V.Apellidos as " + abreTab + "_ApellidoVictimaSP, D.nomDepartamento as " + abreTab + "_nomDepartamentoSP , M.nomMunicipio as " + abreTab + "_nomMunicipioSP, T.* ";
            lstrTabla = nombreTabla + " as T, Victima as V, tbl_Departamento as D, tbl_Municipio as M ";
            lstrCondicion = " T." + abreTab + "_IdVictimaRel = V.ID_IMSMAVictima and T." + abreTab + "_" + nombreCampoMunicipio + " = M.codMunicipioAlf5 AND M.codDepartamentoAlf2 = D.codDepartamentoAlf2 ";
            lstrOrden = "  V.Nombres DESC ";
        }
        else
        {
            if (relVictima)
            {
                lstrCampos = " V.ID_IMSMAVictima as " + abreTab + "_idVictimaSP, V.Nombres as " + abreTab + "_NombreVictimaSP,  V.Apellidos as " + abreTab + "_ApellidoVictimaSP, T.* ";
                lstrTabla = nombreTabla + " as T, Victima as V ";
                lstrCondicion = " T." + abreTab + "_IdVictimaRel = V.ID_IMSMAVictima ";
                lstrOrden = "  V.Nombres DESC ";
            }
            else
            {
                if (relMunicipio)
                {
                    lstrCampos = " D.nomDepartamento as " + abreTab + "_nomDepartamentoSP, M.nomMunicipio as " + abreTab + "_nomMunicipioSP,  T.*  ";
                    lstrTabla = nombreTabla + " as T, tbl_Departamento as D, tbl_Municipio as M ";
                    lstrCondicion = " T." + abreTab + "_" + nombreCampoMunicipio + " = M.codMunicipioAlf5 AND M.codDepartamentoAlf2 = D.codDepartamentoAlf2  ";
                    lstrOrden = " M.nomMunicipio DESC ";
                }
                else
                {
                    lstrCampos = " T.* ";
                    lstrTabla = " " + nombreTabla + " as T ";
                    lstrCondicion = string.Empty;
                    lstrOrden = string.Empty;
                }
            }
        }
        if (fechaD.Trim() != string.Empty || fechaH.Trim() != string.Empty)
        {
            if (lstrCondicion == string.Empty)
            {
                lstrCondicion = " convert(date, substring(fechaCarga, 1,10), 103) >= @fechaD AND convert(date, substring(fechaCarga, 1,10), 103) <= @fechaH ";
            }
            else
            {
                lstrCondicion = " AND convert(date, substring(fechaCarga, 1,10), 103) >= @fechaD AND convert(date, substring(fechaCarga, 1,10), 103) <= @fechaH ";
            }

            base.clearParameters();
            base.setParameters("@fechaD", System.Data.SqlDbType.Date, fechaD);
            base.setParameters("@fechaH", System.Data.SqlDbType.Date, fechaH);
        }
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    //------------------ Consultar los tipos de filtros-----------------
    public Boolean fntConsultaFiltroPrimer(string dataset)
    {
        string lstrCampos = " id , primerFil_Descripcion ";
        string lstrTabla = " Form_PrimerFiltro ";
        string lstrCondicion = string.Empty;
        //string groupBy = " fechaCarga ";
        string lstrOrden = " id ";

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    //------------------ Consultar los tipos de filtros-----------------
    public Boolean fntConsultaSegundoFiltro(string dataset, string primerFiltro)
    {
        string lstrCampos = " fs.id , fs.segundoFil_Descripcion ";
        string lstrTabla = " Form_SegundoFiltro fs INNER JOIN Form_SegundoFiltroEnPrimer sfp ON fs.id = sfp.id_SegundoFiltro  ";
        string lstrCondicion = " fs.id = sfp.id_SegundoFiltro AND  sfp.id_PrimerFiltro = @primerFiltro ";
        string lstrOrden = " fs.id ";
        base.clearParameters();
        setParameters("@PrimerFiltro", SqlDbType.Int, Convert.ToInt32(primerFiltro));
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    //------------------ Consultar los tipos de filtros-----------------
    public Boolean fntConsultaTablaAdm(string dataset, string segundoFiltro, int idUsuario)
    {
        string lstrCampos = " fadt.admTab_NombreTabla , fadt.admTab_Id  ";
        string lstrTabla = " Form_AdmTablas fadt, Form_UsuarioPermisoEditar fupe";
        string lstrCondicion = " fadt.admTab_FiltroSegundo = @segundoFiltro AND fupe.usuper_admTab_Id = fadt.admTab_Id AND fupe.usuper_segusu_Id = @idUsuario ";
        //string groupBy = " fechaCarga ";
        string lstrOrden = " admTab_Id ";
        base.clearParameters();
        setParameters("@segundoFiltro", SqlDbType.Int, Convert.ToInt32(segundoFiltro));
        setParameters("@idUsuario", SqlDbType.Int, idUsuario);
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntConsultaTodoPorId(string dataset, string nombreTabla, string nombreID, int valorId, string campoOrden)
    {
        string lstrCampos = " * ";
        string lstrTabla = " " + nombreTabla + " ";
        string lstrCondicion = nombreID + " = " + "@" + nombreID;
        //string groupBy = " fechaCarga ";
        string lstrOrden = " " + campoOrden + " ";
        base.clearParameters();
        base.setParameters("@" + nombreID, SqlDbType.Int, valorId);

        if (!base.Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntConsultaVictimaPorId(string dataset, string nombreTabla, string nombreID, string valorId, string campoOrden)
    {
        string lstrCampos = " * ";
        string lstrTabla = " " + nombreTabla + " ";
        string lstrCondicion = nombreID + " = " + "@" + nombreID;
        //string groupBy = " fechaCarga ";
        string lstrOrden = " " + campoOrden + " ";
        base.clearParameters();
        setParameters("@" + nombreID, SqlDbType.NVarChar, valorId);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    public Boolean fntConsultaTablaPorId(string dataset, string nombreTabla, string nombreID, int valorId)
    {
        string lstrCampos = " * ";
        string lstrTabla = " " + nombreTabla + " ";
        string lstrCondicion = nombreID + " = " + "@" + nombreID;
        //string groupBy = " fechaCarga ";
        string lstrOrden = string.Empty;
        base.clearParameters();
        setParameters("@" + nombreID, SqlDbType.Int, valorId);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    public Boolean fntConsultaTodoPorLike(string dataset, string nombreTabla, string nombreID, string valorId)
    {
        string lstrCampos = " * ";
        string lstrTabla = " " + nombreTabla + " ";
        string lstrCondicion = nombreID + " LIKE  " + "'%" + valorId + "%'";
        //string groupBy = " fechaCarga ";
        string lstrOrden = string.Empty;
        base.clearParameters();
        setParameters("@" + nombreID, SqlDbType.NVarChar, valorId);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }
    //Consulta la lista de los atributos que tiene una tabla.
    //fntConsultaDetalleCampos
    //IN: Modelo de Controles con todo los campos
    //OUT: retorna un bool de exito
    public Boolean fntConsultaDetalleCampos(string dataset, string nombreTabla, int tablaAsociada)
    {
        string lstrCampos = " * ";
        string lstrTabla = " " + nombreTabla + " ";
        string lstrCondicion = " camForm_admTab_Id = @tablaAsociada";
        string lstrOrden = " camForm_Orden ASC , camForm_TipoControl DESC ";

        base.clearParameters();
        setParameters("@tablaAsociada", SqlDbType.Int, tablaAsociada);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    //Consulta la lista de los atributos que tiene una tabla.
    //fntConsultaDetalleCampos
    //IN: Modelo de Controles con todo los campos
    //OUT: retorna un bool de exito
    public Boolean fntConsultaDetalleCamposLista(string dataset, string nombreTabla, int tablaAsociada)
    {
        string lstrCampos = " * ";
        string lstrTabla = " " + nombreTabla + " ";
        string lstrCondicion = " camForm_admTab_Id = @tablaAsociada AND camForm_ShowGrid = 1";
        string lstrOrden = " camForm_Orden ASC , camForm_Label ASC ";

        base.clearParameters();
        setParameters("@tablaAsociada", SqlDbType.Int, tablaAsociada);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }




    public Boolean fntConsultaIdTablaCreada(string dataset, string nombreTabla)
    {
        string lstrCampos = " admTab_Id ";
        string lstrTabla = " Form_AdmTablas ";
        string lstrCondicion = " admTab_NombreTabla = @nombreTabla";
        string groupBy = " admTab_Id ";
        string lstrOrden = " admTab_Id DESC ";
        base.clearParameters();
        setParameters("@nombreTabla", SqlDbType.NVarChar, nombreTabla);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, groupBy, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }
    //public bool fntInsertarFormDatos_bol(Controles _controles, int idTabla)
    //{
    //    string strCampos = " camForm_TipoControl, camForm_NombreCampo, camForm_Label, camForm_Orden, camForm_LongitudMax, camForm_admTab_Id ";
    //    string strTabla = " Form_CampoTabla ";
    //    string strValores = " @intTipoControl, @strNombreCampo,  @strLabel, @intOrden, @intLogitud, @intIdTabla ";
    //    string strCondicion = " camForm_TipoControl = @intTipoControl AND camForm_NombreCampo = @strNombreCampo AND camForm_Label = @strLabel AND camForm_Orden =  @intOrden AND camForm_LongitudMax = @intLogitud AND camForm_admTab_Id = @intIdTabla ";
    //    base.clearParameters();
    //    base.setParameters("@intTipoControl", System.Data.SqlDbType.SmallInt, _controles.TipoControl);
    //    base.setParameters("@strNombreCampo", System.Data.SqlDbType.NVarChar, _controles.NombreCampo);
    //    base.setParameters("@strLabel", System.Data.SqlDbType.NVarChar, _controles.Label);
    //    base.setParameters("@intOrden", System.Data.SqlDbType.Int, _controles.Orden);
    //    base.setParameters("@intLogitud", System.Data.SqlDbType.Int, _controles.LongitudMax);
    //    base.setParameters("@intIdTabla", System.Data.SqlDbType.Int, idTabla);
    //    base.iniciarTransaccion();
    //    if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
    //    {
    //        base.commit();
    //        return true;
    //    }
    //    base.rollback();
    //    return false;
    //}

    public bool fntInsertarDatosDinamicos_bol(List<System.Web.UI.Control> ListaControles, string NombreTabla, bool esVictima, string idVictima, string abreviatura)
    {
        string strTabla = " " + NombreTabla + " ";
        string strCampos = " fechaCarga ";
        string strValores = " @fechaCarga ";
        string strCondicion = " fechaCarga = @fechaCarga ";
        int i = 0;
        base.clearParameters();
        DateTime FechaActual = new DateTime();
        FechaActual = DateTime.Now;
        base.setParameters("@fechaCarga", System.Data.SqlDbType.NVarChar, FechaActual.ToString());
        for (i = 0; i < ListaControles.Count(); i++)
        {
            System.Web.UI.Control _control = ListaControles[i];

            switch (_control.GetType().Name.ToString())
            {
                case "TextBox":
                    TextBox tb = (TextBox)_control;
                    if (!string.IsNullOrEmpty(tb.Text))
                    {

                        strValores = strValores + " , " + "@" + tb.Attributes["data-nombre"].ToString() + " ";
                        strCampos = strCampos + " , " + tb.Attributes["data-nombre"].ToString() + " ";
                        strCondicion = strCondicion + " AND " + tb.Attributes["data-nombre"].ToString() + " = " + "@" + tb.Attributes["data-nombre"].ToString() + " ";
                        if (tb.Attributes["data-DbType"].ToString() == "int")
                        {
                            base.setParameters("@" + tb.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.Int, Convert.ToInt32(tb.Text.Trim()));
                        }
                        if (tb.Attributes["data-DbType"].ToString() == "nvarchar")
                        {
                            base.setParameters("@" + tb.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.NVarChar, tb.Text.Trim().ToString());
                        }
                        if (tb.Attributes["data-DbType"].ToString() == "decimal")
                        {
                            CultureInfo culture = new CultureInfo("en-US");
                            base.setParameters("@" + tb.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.Decimal, Convert.ToDecimal(tb.Text.Trim(), culture));
                        }
                        if (tb.Attributes["data-DbType"].ToString() == "datetime")
                        {
                            base.setParameters("@" + tb.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.DateTime, Convert.ToDateTime(tb.Text.Trim()));
                        }
                    }

                    break;
                case "CheckBox":
                    CheckBox ch = (CheckBox)_control;

                    strValores = strValores + " , " + "@" + ch.Attributes["data-nombre"].ToString() + " ";
                    strCampos = strCampos + " , " + ch.Attributes["data-nombre"].ToString() + " ";
                    strCondicion = strCondicion + " AND " + ch.Attributes["data-nombre"].ToString() + " = " + "@" + ch.Attributes["data-nombre"].ToString() + " ";

                    base.setParameters("@" + ch.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.Bit, ch.Checked);
                    break;
                case "DropDownList":
                    DropDownList dd = (DropDownList)_control;

                    strValores = strValores + " , " + "@" + dd.Attributes["data-nombre"].ToString() + " ";
                    strCampos = strCampos + " , " + dd.Attributes["data-nombre"].ToString() + " ";
                    strCondicion = strCondicion + " AND " + dd.Attributes["data-nombre"].ToString() + " = " + "@" + dd.Attributes["data-nombre"].ToString() + " ";

                    base.setParameters("@" + dd.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.NVarChar, dd.SelectedValue.ToString());
                    break;
                case "Calendar":
                    System.Web.UI.WebControls.Calendar calFec = (System.Web.UI.WebControls.Calendar)_control;
                    strValores = strValores + " , " + "@" + calFec.Attributes["data-nombre"].ToString() + " ";
                    strCampos = strCampos + " , " + calFec.Attributes["data-nombre"].ToString() + " ";
                    strCondicion = strCondicion + " AND " + calFec.Attributes["data-nombre"].ToString() + " = " + "@" + calFec.Attributes["data-nombre"].ToString() + " ";

                    base.setParameters("@" + calFec.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.DateTime, calFec.SelectedDate);
                    break;

            }
        }

        if (esVictima)
        {
            strValores = strValores + ", @" + abreviatura + "_IdVictimaRel ";
            strCampos = strCampos + ", " + abreviatura + "_IdVictimaRel ";
            strCondicion = strCondicion + " AND " + abreviatura + "_IdVictimaRel = @" + abreviatura + "_IdVictimaRel ";
            base.setParameters("@" + abreviatura + "_IdVictimaRel", System.Data.SqlDbType.NVarChar, idVictima);
        }

        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }

    public bool fntModificarFormulario_bol(List<System.Web.UI.Control> ListaControles, string NombreTabla, int condicion)
    {
        string strCamposValores = " ";
        string strTabla = " " + NombreTabla + " ";
        //string strTabla = " tbl_Persona ";
        //string strCamposValores = " cat_Id = @intIdCategoria, voc_Id = @intIdVocativo, car_Id = @intIdCargo, nombrePersona = @strNombre, apellidoPersona = @strApellido, ins_Id = @intIdInstitucion, codMunicipioAlf5 = @strIdMunicipio, direccion = @strDireccion, tip_Id = @intIdTipoDireccion ";
        string strCondicion = " id = @intId ";
        base.clearParameters();
        base.setParameters("@intId", System.Data.SqlDbType.Int, condicion);
        int i = 0;
        bool primero = false;
        for (i = 0; i < ListaControles.Count(); i++)
        {
            System.Web.UI.Control _control = ListaControles[i];

            switch (_control.GetType().Name.ToString())
            {
                case "TextBox":
                    TextBox tb = (TextBox)_control;
                    //if (!string.IsNullOrEmpty(tb.Text))
                    //{
                    if (i == 0)
                    {
                        strCamposValores = strCamposValores + tb.Attributes["data-nombre"].ToString() + " = " + "@" + tb.Attributes["data-nombre"].ToString() + " ";
                        primero = true;
                    }
                    else
                    {
                        if (!primero)
                        {
                            strCamposValores = strCamposValores + tb.Attributes["data-nombre"].ToString() + " = " + "@" + tb.Attributes["data-nombre"].ToString() + " ";
                            primero = true;
                        }
                        else
                        {
                            strCamposValores = strCamposValores + " , " + tb.Attributes["data-nombre"].ToString() + " = " + "@" + tb.Attributes["data-nombre"].ToString() + " ";
                        }
                    }

                    if (tb.Attributes["data-DbType"].ToString() == "int")
                    {
                        if (!string.IsNullOrEmpty(tb.Text.Trim()))
                        {
                            base.setParameters("@" + tb.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.Int, Convert.ToInt32(tb.Text.Trim()));
                        }
                        else
                        {
                            base.setParameters("@" + tb.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.Int, 0);
                        }

                    }
                    if (tb.Attributes["data-DbType"].ToString() == "nvarchar")
                    {
                        if (!string.IsNullOrEmpty(tb.Text.Trim()))
                        {
                            base.setParameters("@" + tb.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.NVarChar, tb.Text.Trim().ToString());
                        }
                        else
                        {
                            base.setParameters("@" + tb.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.NVarChar, "");
                        }

                    }
                    if (tb.Attributes["data-DbType"].ToString() == "decimal")
                    {
                        if (!string.IsNullOrEmpty(tb.Text.Trim()))
                        {
                            CultureInfo culture = new CultureInfo("en-US");
                            base.setParameters("@" + tb.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.Decimal, Convert.ToDecimal(tb.Text.Trim(), culture));
                        }
                        else
                        {
                            base.setParameters("@" + tb.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.Decimal, 0);
                        }

                    }
                    if (tb.Attributes["data-DbType"].ToString() == "datetime")
                    {
                        if (tb.Text.Trim() != string.Empty && tb.Text.Trim() != null)
                        {
                            base.setParameters("@" + tb.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.DateTime, Convert.ToDateTime(tb.Text.Trim()));
                        }
                        else
                        {
                            base.setParameters("@" + tb.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.DateTime, "1900-01-01 12:00:00");
                        }
                    }
                    // }
                    //else
                    //{
                    //    if (i == 0)
                    //    {
                    //        strCamposValores = strCamposValores + tb.Attributes["data-nombre"].ToString() + " = " + "@" + tb.Attributes["data-nombre"].ToString() + " ";
                    //        base.setParameters("@" + tb.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.NVarChar, string.Empty);
                    //    }
                    //    else
                    //    {
                    //        strCamposValores = strCamposValores + " , " + tb.Attributes["data-nombre"].ToString() + " = " + "@" + tb.Attributes["data-nombre"].ToString() + " ";
                    //    }

                    //}

                    break;
                case "CheckBox":
                    CheckBox ch = (CheckBox)_control;
                    if (i == 0)
                    {
                        strCamposValores = strCamposValores + ch.Attributes["data-nombre"].ToString() + " = " + "@" + ch.Attributes["data-nombre"].ToString() + " ";
                    }
                    else
                    {
                        strCamposValores = strCamposValores + " , " + ch.Attributes["data-nombre"].ToString() + " = " + "@" + ch.Attributes["data-nombre"].ToString() + " ";
                    }

                    base.setParameters("@" + ch.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.Bit, ch.Checked);
                    break;
                case "DropDownList":
                    DropDownList dd = (DropDownList)_control;

                    if (i == 0)
                    {
                        strCamposValores = strCamposValores + dd.Attributes["data-nombre"].ToString() + " = " + "@" + dd.Attributes["data-nombre"].ToString() + " ";
                    }
                    else
                    {
                        strCamposValores = strCamposValores + " , " + dd.Attributes["data-nombre"].ToString() + " = " + "@" + dd.Attributes["data-nombre"].ToString() + " ";
                    }

                    base.setParameters("@" + dd.Attributes["data-nombre"].ToString(), System.Data.SqlDbType.NVarChar, dd.SelectedValue.ToString());
                    break;

            }
        }

        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }

    public Boolean fntConsultaListadoControles(string dataset, int tablaAsociada)
    {
        string lstrCampos = " cf.camForm_IdCampo ,  cf.camForm_NombreCampo , tp.tipCon_DescControl , cf.camForm_LongitudMax , cf.camForm_Orden ";
        string lstrTabla = " Form_CampoTabla cf INNER JOIN Form_TipoControlForm tp ON cf.camForm_TipoControl = tp.tipCon_IdTipoControl  ";
        string lstrCondicion = " cf.camForm_TipoControl = tp.tipCon_IdTipoControl AND cf.camForm_admTab_Id = @tablaAsociada ";
        string lstrOrden = " cf.camForm_Orden ASC ";

        base.clearParameters();
        base.setParameters("@tablaAsociada", System.Data.SqlDbType.Int, tablaAsociada);
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public bool fntInsertarOpcionesLista_bol(int idCampoRelacion, string valor, string descripcion)
    {
        string strCampos = " IdCampoRelacion , camValue , camDesc ";
        string strTabla = " Form_OptionsList ";
        string strValores = " @IdCampoRelacion , @camValue ,  @camDesc ";
        string strCondicion = " IdCampoRelacion = @IdCampoRelacion AND camValue = @camValue AND camDesc = @camDesc  ";
        base.clearParameters();
        base.setParameters("@IdCampoRelacion", System.Data.SqlDbType.Int, idCampoRelacion);
        base.setParameters("@camValue", System.Data.SqlDbType.NVarChar, valor);
        base.setParameters("@camDesc", System.Data.SqlDbType.NVarChar, descripcion);

        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }

    public bool fntConsultarPaises_bol(string dataset)
    {
        string strCampos = " P.* ";
        string strTabla = " sch_Proyectos.TBL_Pais P ";
        string strCondicion = " 1 = 1 ";
        string strOrden = " P.nomPais ";
        base.clearParameters();
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }

    public bool fntConsultarUsuarios_bol(string dataset)
    {
        string strCampos = " U.* ";
        string strTabla = " Seg_Usuario U, Seg_Aplicativo A ";
        string strCondicion = " U.segApl_id = A.segApl_id AND A.segApl_Nombre = 'SisPAICMA' ";
        string strOrden = " U.segusu_login ";
        base.clearParameters();
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }

    public bool fntConsultarUsuariosXid_bol(string dataset, int intIdUsuario)
    {
        string strCampos = " U.* ";
        string strTabla = " Seg_Usuario U, Seg_Aplicativo A ";
        string strCondicion = " U.segApl_id = A.segApl_id AND A.segApl_Nombre = 'SisPAICMA' AND U.segusu_Id = @intIdUsuario ";
        string strOrden = " U.segusu_login ";
        base.clearParameters();
        base.setParameters("@intIdUsuario", System.Data.SqlDbType.Int, intIdUsuario);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }


    public bool fntEliminarPermisosUsuarios_bol(int intIdFormulario)
    {
        string strTabla = " Form_UsuarioPermisoEditar ";
        string strCondicion = " usuper_admTab_Id = @usuper_amdTab_Id ";
        base.clearParameters();
        base.setParameters("@usuper_amdTab_Id", System.Data.SqlDbType.Int, intIdFormulario);
        return base.Eliminar(strTabla, strCondicion, false);
    }

    public bool fntConsultarUsuariosPermitidos_bol(string dataset, int intIdFormulario)
    {
        string strCampos = " UP.usuper_segusu_Id, U.segusu_login ";
        string strTabla = " Form_UsuarioPermisoEditar UP, Seg_Usuario U ";
        string strCondicion = " U.segusu_Id = UP.usuper_segusu_Id AND UP.usuper_admTab_Id = @intIdFormulario ";
        string strOrden = " U.segusu_login ";
        base.clearParameters();
        base.setParameters("@intIdFormulario", System.Data.SqlDbType.Int, intIdFormulario);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }

    public bool fntConsultarDepartamentos(string dataset)
    {
        string strCampos = " DP.codDepartamentoAlf2, DP.nomDepartamento  ";
        string strTabla = " tbl_Departamento DP ";
        string strCondicion = "";
        string strOrden = " DP.nomDepartamento ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }

    public bool fntConsultarMunicipios(string dataset)
    {
        string strCampos = " MU.codMunicipioAlf5 , MU.nomMunicipio , MU.codDepartamentoAlf2 ";
        string strTabla = " tbl_Municipio MU ";
        string strCondicion = "";
        string strOrden = " MU.nomMunicipio ";
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    /// <summary>
    /// ☻☻ consultar los municipios por departamentos 
    /// </summary>
    /// <param name="dataset"></param>
    /// <param name="idDepartamento"></param>
    /// <returns></returns>
    public bool fntConsultarMunicipiosporDepartamento(string dataset, string idDepartamento)
    {
        string lstrCampos = " codMunicipioAlf5 , nomMunicipio , codDepartamentoAlf2 ";
        string lstrTabla = " tbl_Municipio";
        string lstrCondicion = " codDepartamentoAlf2 = @codDepartamentoAlf2 ";
        string lstrOrden = " nomMunicipio DESC ";
        base.clearParameters();
        base.setParameters("@codDepartamentoAlf2", SqlDbType.Int, idDepartamento);
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    public bool fntInsertarLogEmail_bol(string remitente, string destinatarios, string titulo, string mensaje, string moduloEnvio, DateTime fechaEnvio, bool estado, string fallidos)
    {
        string strCampos = " Remitente , Destinatarios , Titulo , Mensaje , ModEnvio , FechaEnvio , Estado , Fallidos ";
        string strTabla = " Form_LogEmail ";
        string strValores = " @remitente, @destinatarios, @titulo, @mensaje, @moduloEnvio, @fechaEnvio, @estado, @fallidos ";
        string strCondicion = " Remitente = @remitente AND Destinatarios = @destinatarios AND Titulo = @titulo AND Mensaje = @mensaje AND ModEnvio =  @moduloEnvio AND FechaEnvio = @fechaEnvio AND Estado = @estado AND Fallidos = @fallidos ";
        base.clearParameters();
        base.setParameters("@remitente", System.Data.SqlDbType.NVarChar, remitente);
        base.setParameters("@destinatarios", System.Data.SqlDbType.NVarChar, destinatarios);
        base.setParameters("@titulo", System.Data.SqlDbType.NVarChar, titulo);
        base.setParameters("@mensaje", System.Data.SqlDbType.NVarChar, mensaje);
        base.setParameters("@moduloEnvio", System.Data.SqlDbType.NVarChar, moduloEnvio);
        base.setParameters("@fechaEnvio", System.Data.SqlDbType.DateTime, fechaEnvio);
        base.setParameters("@estado", System.Data.SqlDbType.Bit, estado);
        base.setParameters("@fallidos", System.Data.SqlDbType.NVarChar, fallidos);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }


    #region ConfiguracionReportesCargados
    //Inserta en la tabla FormCampos lo que seria la lista de controles para renderizar en la creacion de formularios
    //fntInsertarFormCampos_bol
    //IN: Modelo de Controles con todo los campos
    //OUT: retorna un bool de exito
    public bool fntInsertarFormReportes_bol(string nombreReporte, string descripcionReporte, bool estatus, int clasificacion, byte[] reporteBinario, string rutaReporte, int usuarioCreacion, int usuarioModificacion)
    {
        string strCampos = " nombreReporte, descripcionReporte, fechaCreacion, fechaModificacion, estatus, clasificacion, reporteBinario, rutaReporte, usuarioCreacion, usuarioModificacion  ";
        string strTabla = " Form_ReportesCargados ";
        string strValores = " @nombreReporte, @descripcionReporte,  @fechaCreacion, @fechaModificacion, @estatus, @clasificacion, @reporteBinario, @rutaReporte, @usuarioCreacion, @usuarioModificacion ";
        string strCondicion = " nombreReporte = @nombreReporte AND descripcionReporte = @descripcionReporte AND fechaCreacion =  @fechaCreacion AND fechaModificacion = @fechaModificacion AND estatus = @estatus AND clasificacion =  @clasificacion AND reporteBinario = @reporteBinario AND rutaReporte = @rutaReporte AND usuarioCreacion = @usuarioCreacion AND usuarioModificacion = @usuarioModificacion ";
        base.clearParameters();
        base.setParameters("@nombreReporte", System.Data.SqlDbType.NVarChar, nombreReporte);
        base.setParameters("@descripcionReporte", System.Data.SqlDbType.NVarChar, descripcionReporte);
        base.setParameters("@fechaCreacion", System.Data.SqlDbType.DateTime, DateTime.Now);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, DateTime.Now);
        base.setParameters("@estatus", System.Data.SqlDbType.Bit, estatus);
        base.setParameters("@clasificacion", System.Data.SqlDbType.Int, clasificacion);
        base.setParameters("@reporteBinario", System.Data.SqlDbType.VarBinary, reporteBinario);
        base.setParameters("@rutaReporte", System.Data.SqlDbType.NVarChar, rutaReporte);
        base.setParameters("@usuarioCreacion", System.Data.SqlDbType.Int, usuarioCreacion);
        base.setParameters("@usuarioModificacion", System.Data.SqlDbType.Int, usuarioModificacion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }
    // Modificar Reportes Creados
    public bool fntModificarFormReportes_bol(string nombreReporte, string descripcionReporte, bool estatus, int clasificacion, byte[] reporteBinario, string rutaReporte, int usuarioCreacion, int usuarioModificacion, int idReporte)
    {
        string strTabla = " Form_ReportesCargados ";
        string strCamposValores = " nombreReporte = @nombreReporte, descripcionReporte = @descripcionReporte, fechaModificacion = @fechaModificacion, estatus = @estatus, clasificacion = @clasificacion, reporteBinario = @reporteBinario, rutaReporte = @rutaReporte,  usuarioModificacion = @usuarioModificacion ";
        string strCondicion = " id = @idReporte ";
        base.clearParameters();
        base.setParameters("@nombreReporte", System.Data.SqlDbType.NVarChar, nombreReporte);
        base.setParameters("@descripcionReporte", System.Data.SqlDbType.NVarChar, descripcionReporte);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, DateTime.Now);
        base.setParameters("@estatus", System.Data.SqlDbType.Bit, estatus);
        base.setParameters("@clasificacion", System.Data.SqlDbType.Int, clasificacion);
        base.setParameters("@reporteBinario", System.Data.SqlDbType.VarBinary, reporteBinario);
        base.setParameters("@rutaReporte", System.Data.SqlDbType.NVarChar, rutaReporte);
        base.setParameters("@usuarioModificacion", System.Data.SqlDbType.Int, usuarioModificacion);
        base.setParameters("@idReporte", System.Data.SqlDbType.Int, idReporte);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }


    public bool fntInsertarPermisosUsuario_bol(int idUsuario, int idFormulario)
    {
        string strCampos = " usuper_segusu_Id , usuper_admTab_Id  ";
        string strTabla = " Form_UsuarioPermisoEditar ";
        string strValores = " @idUsuario , @idFormulario ";
        string strCondicion = " usuper_segusu_Id = @idUsuario AND usuper_admTab_Id = @idFormulario  ";
        base.clearParameters();
        base.setParameters("@idUsuario", System.Data.SqlDbType.Int, idUsuario);
        base.setParameters("@idFormulario", System.Data.SqlDbType.Int, idFormulario);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }







    //------------------ Consultar los tipos de filtros-----------------
    public Boolean fntConsultaReporte(string dataset, int ModoConsulta, int idReporte, int clasificacion)
    {
        string lstrCampos = " rc.*, u.segusu_login ";
        string lstrTabla = " Form_ReportesCargados rc, Seg_Usuario u";
        string lstrCondicion = " rc.usuarioModificacion = segusu_Id ";
        string lstrOrden = string.Empty;
        base.clearParameters();
        //modoconsulta == 0 Nothing (Bring everything)
        if (ModoConsulta == 1)
        {
            //by Id
            lstrCondicion = lstrCondicion + " AND rc.id = @idReporte ";
            setParameters("@idReporte", SqlDbType.Int, idReporte);
        }
        if (ModoConsulta == 2)
        {

            //by clasificacion
            lstrCondicion = lstrCondicion + " AND rc.clasificacion = @clasificacion ";
            setParameters("@clasificacion", SqlDbType.Int, clasificacion);

        }
        if (ModoConsulta == 3)
        {
            //by id and clasificacion
            lstrCondicion = lstrCondicion + " AND rc.id = @idReporte AND rc.clasificacion = @clasificacion ";
            setParameters("@idReporte", SqlDbType.Int, idReporte);
            setParameters("@clasificacion", SqlDbType.Int, clasificacion);
        }
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    public Boolean fntConsultaReporteXUsuario(string dataset, int ModoConsulta, int idReporte, int clasificacion, int idUsuario)
    {
        string lstrCampos = " rc.*, u.segusu_login ";
        string lstrTabla = " Form_ReportesCargados rc, Seg_Usuario u, Form_UsuarioPermisoVerReport upv";
        string lstrCondicion = " rc.usuarioModificacion = segusu_Id AND rc.id = upv.usuper_reporte_Id AND upv.usuper_segusu_Id = @idUsuario ";
        string lstrOrden = string.Empty;
        base.clearParameters();
        setParameters("@idUsuario", SqlDbType.Int, idUsuario);
        //modoconsulta == 0 Nothing (Bring everything)
        if (ModoConsulta == 1)
        {
            //by Id
            lstrCondicion = lstrCondicion + " AND rc.id = @idReporte ";
            setParameters("@idReporte", SqlDbType.Int, idReporte);
        }
        if (ModoConsulta == 2)
        {

            //by clasificacion
            lstrCondicion = lstrCondicion + " AND rc.clasificacion = @clasificacion ";
            setParameters("@clasificacion", SqlDbType.Int, clasificacion);

        }
        if (ModoConsulta == 3)
        {
            //by id and clasificacion
            lstrCondicion = lstrCondicion + " AND rc.id = @idReporte AND rc.clasificacion = @clasificacion ";
            setParameters("@idReporte", SqlDbType.Int, idReporte);
            setParameters("@clasificacion", SqlDbType.Int, clasificacion);
        }



        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public bool fntInsertarPermisosReporte_bol(int idUsuario, int idReporte)
    {
        string strCampos = " usuper_segusu_Id , usuper_reporte_Id  ";
        string strTabla = " Form_UsuarioPermisoVerReport ";
        string strValores = " @idUsuario , @idReporte ";
        string strCondicion = " usuper_segusu_Id = @idUsuario AND usuper_reporte_Id = @idReporte  ";
        base.clearParameters();
        base.setParameters("@idUsuario", System.Data.SqlDbType.Int, idUsuario);
        base.setParameters("@idReporte", System.Data.SqlDbType.Int, idReporte);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }

    public bool fntEliminarPermisosReporte_bol(int idReporte)
    {
        string strTabla = " Form_UsuarioPermisoVerReport ";
        string strCondicion = " usuper_reporte_Id = @usuper_reporte_Id ";
        base.clearParameters();
        base.setParameters("@usuper_reporte_Id", System.Data.SqlDbType.Int, idReporte);
        return base.Eliminar(strTabla, strCondicion, false);
    }

    public bool fntConsultarReportePermitidos_bol(string dataset, int idReporte)
    {
        string strCampos = " UP.usuper_segusu_Id, U.segusu_login ";
        string strTabla = " Form_UsuarioPermisoVerReport UP, Seg_Usuario U ";
        string strCondicion = " U.segusu_Id = UP.usuper_segusu_Id AND UP.usuper_reporte_Id = @idReporte ";
        string strOrden = " U.segusu_login ";
        base.clearParameters();
        base.setParameters("@idReporte", System.Data.SqlDbType.Int, idReporte);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }
    #endregion configurarReportes

    #region FuncionesFaltantes
    //Elimina de la tabla el Evento Taller un Seguimiento Operador
    //fntInsertarFormAdmTab_bol
    //IN: ID a eliminar
    //OUT: Boolean para confirmar la eliminacion (FISICA)
    public bool fntEliminarSegimientoOperador_bol(int idEventoTaller)
    {
        string strTabla = " tbl_EventoTaller ";
        string strCondicion = " evetal_Id = @idEventoTaller ";
        base.clearParameters();
        base.setParameters("@idEventoTaller", System.Data.SqlDbType.Int, idEventoTaller);
        return base.Eliminar(strTabla, strCondicion, false);
    }




    #endregion FuncionesFaltantes




    #endregion FORMULARIOSDINAMICOS
    //Caracterizacion
    #region FUNCIONESCARACTERIZACION

    /// <summary>
    /// Consulta una persona en la base de victimas y caracterizacion
    /// </summary>
    /// <autor>Cifras y Conceptos Junio2015</autor>
    /// <param name="dataset"></param>
    /// <param name="intOp"></param>
    /// <param name="strNoIdentificacion"></param>
    /// <param name="strNombre"></param>
    /// <param name="strApellido"></param>
    /// <returns></returns>
    public Boolean fntConsultaCaracterizacionGrilla(string dataset, string strIdVictima, string strNoIdentificacion, string strNombre, string strApellido)
    {
        // busqueda en la base de victimas
        string lstrCampos = "v.ID_IMSMAVictima, v.numeroidentificacion as id, v.nombres + ' ' + v.apellidos as nombres,v.direccion as direccion,v.Departamento as departamento,v.Municipio as municipio, dbo.TieneCaracterizacion(v.numeroidentificacion) as tienecaract";
        string lstrTabla = " Victima v ";
        string lstrCondicion = " ";
        string lstrOrden = " v.numeroidentificacion ";
        bool firstparam = false;

        base.clearParameters();

        setParameters("@strIdVictima", SqlDbType.NVarChar, '%' + strIdVictima + '%');
        setParameters("@strNoIdentificacion", SqlDbType.NVarChar, '%' + strNoIdentificacion + '%');
        setParameters("@strNombre", SqlDbType.NVarChar, '%' + strNombre + '%');
        setParameters("@strApellido", SqlDbType.NVarChar, '%' + strApellido + '%');

        if (!string.IsNullOrWhiteSpace(strIdVictima))
        {
            lstrCondicion += " v.ID_IMSMAVictima LIKE @strIdVictima ";
            firstparam = true;
        }

        if (!string.IsNullOrWhiteSpace(strNoIdentificacion))
        {
            if (firstparam)
                lstrCondicion += " AND ";
            else
                firstparam = true;

            lstrCondicion += " v.numeroidentificacion LIKE @strNoIdentificacion ";
        }

        if (!string.IsNullOrWhiteSpace(strNombre))
        {
            if (firstparam)
                lstrCondicion += " AND ";
            else
                firstparam = true;

            lstrCondicion += " v.nombres LIKE  @strNombre ";
        }

        if (!string.IsNullOrWhiteSpace(strApellido))
        {
            if (firstparam)
                lstrCondicion += " AND ";
            else
                firstparam = true;

            lstrCondicion += " v.apellidos LIKE  @strApellido ";
        }

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// Consulta el detalle de encuesta
    /// </summary>
    /// <param name="dataset"></param>
    /// <param name="idEncuesta"></param>
    /// <returns></returns>
    public Boolean fntConsultaEncuestaGrilla(string dataset, int idEncuesta)
    {
        // busqueda en la base de victimas
        string lstrCampos = "e.*";
        string lstrTabla = " car_Encuesta e ";
        string lstrCondicion = " ";
        string lstrOrden = " e.idEncuesta ";

        base.clearParameters();

        setParameters("@idEncuesta", SqlDbType.Int, idEncuesta);

        lstrCondicion += " e.idEncuesta = @idEncuesta ";


        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// Consulta el detalle de una encuesta
    /// </summary>
    /// <param name="dataset"></param>
    /// <param name="idEncuesta"></param>
    /// <returns></returns>
    public Boolean fntConsultaDetalleEncuestaGrilla(string dataset, int idEncuesta)
    {
        // busqueda en la base de victimas
        string lstrCampos = " v.nombrevariable, v.preguntaformulario, vxv.identificadorvalor, vxv.textovalor ";
        string lstrTabla = " car_DetalleEncuesta d, car_variable v, car_valorxvariable vxv ";
        string lstrCondicion = " v.idvariable = d.idvariable AND d.idvalorxvariable = vxv.idvalorxvariable ";
        string lstrOrden = " d.idvariable ";

        base.clearParameters();

        setParameters("@idEncuesta", SqlDbType.Int, idEncuesta);

        lstrCondicion += " AND d.idEncuesta = @idEncuesta ";


        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    /// <summary>
    /// Consulta el detalle del nucleo por victima
    /// </summary>
    /// <param name="dataset"></param>
    /// <param name="strIdVictima"></param>
    /// <returns></returns>
    public Boolean fntConsultaDetalleVictimaGrilla(string dataset, string strIdVictima)
    {
        // busqueda en la base de victimas
        string lstrCampos = "v.*";
        string lstrTabla = " Victima v ";
        string lstrCondicion = string.Empty;
        string lstrOrden = " v.numeroidentificacion ";
        if (!string.IsNullOrWhiteSpace(strIdVictima))
        {
            lstrCondicion += " v.ID_IMSMAVictima LIKE @strIdVictima ";
        }
        else
        {
            lstrCondicion = " ";
        }
        base.clearParameters();
        setParameters("@strIdVictima", SqlDbType.NVarChar, '%' + strIdVictima + '%');
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntIngresarRespuestaNew_bol(int idEncuesta, string respuesta, int? idValorXVariable, int idVariable)
    {
        string lstrCampos = " idEncuesta, respuesta, idValorXVariable, idVariable ";
        string lstrTabla = " car_DetalleEncuesta ";
        string lstrValores = " @idEncuesta, @respuesta, @idValorXVariable, int idVariable";
        string lstrCondicion = " idEncuesta = @idEncuesta ";
        base.clearParameters();
        setParameters("@idEncuesta", SqlDbType.Int, idEncuesta);
        setParameters("@respuesta", SqlDbType.NVarChar, respuesta);
        setParameters("@idValorXVariable", SqlDbType.Int, idValorXVariable);
        setParameters("@idVariable", SqlDbType.Int, idVariable);
        base.iniciarTransaccion();
        if (Insertar(lstrTabla, lstrCampos, lstrValores, lstrCondicion, true, true))
        {
            commit();
            return true;
        }
        else
        {
            rollback();
            return false;
        }
    }

    public bool fntConsultaEncuestasPendientes(string dataset)
    {
        string lstrCampos = " idEncuesta, cedula";
        string lstrTabla = " car_Encuesta ";
        string lstrCondicion = " idencuesta not in (select distinct idEncuesta from car_DetalleEncuesta) ";
        string lstrOrden = " cedula, nombre ";
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntIngresarEncuesta_bol(
         int _roll, string _cedula, string _nombre, int _edad, string _dia, string _mes, string _anio, int _departamento1,
         int _municipio1, string _vereda, string _corregimiento, string _direccionVivienda, int _area, int _estrato, int _total_hogares,
         int _num_personas_hogar, int _num_personas_vivienda, string _email, string _resultado_final, int _departamento2, int _municipio2,
         int _pais, int _sexo, string _nom_enc, int _autorizacion, string _victim_guid)
    {
        string lstrCampos = " roll,cedula,nombre,edad,dia,mes,anio,departamento1,municipio1,vereda,corregimiento,direccionVivienda,area,estrato,total_hogares,num_personas_hogar,num_personas_vivienda,email,resultado_final,departamento2,municipio2,pais,sexo,nom_enc,autorizacion,victim_guid ";
        string lstrTabla = " car_Encuesta ";
        string lstrValores = " @roll, @cedula, @nombre, @edad, @dia, @mes, @anio, @departamento1, @municipio1, @vereda, @corregimiento, @direccionVivienda, @area, @estrato, @total_hogares, @num_personas_hogar, @num_personas_vivienda, @email, @resultado_final, @departamento2, @municipio2, @pais, @sexo, @nom_enc, @autorizacion, @victim_guid ";
        string lstrCondicion = " cedula = @cedula";

        base.clearParameters();
        base.setParameters("@roll", SqlDbType.Int, _roll);
        base.setParameters("@cedula", SqlDbType.NVarChar, _cedula);
        base.setParameters("@nombre", SqlDbType.NVarChar, _nombre);
        base.setParameters("@edad", SqlDbType.Int, _edad);
        base.setParameters("@dia", SqlDbType.NVarChar, _dia);
        base.setParameters("@mes", SqlDbType.NVarChar, _mes);
        base.setParameters("@anio", SqlDbType.NVarChar, _anio);
        base.setParameters("@departamento1", SqlDbType.Int, _departamento1);
        base.setParameters("@municipio1", SqlDbType.Int, _municipio1);
        base.setParameters("@vereda", SqlDbType.NVarChar, _vereda);
        base.setParameters("@corregimiento", SqlDbType.NVarChar, _corregimiento);
        base.setParameters("@direccionVivienda", SqlDbType.NVarChar, _direccionVivienda);
        base.setParameters("@area", SqlDbType.Int, _area);
        base.setParameters("@estrato", SqlDbType.Int, _estrato);
        base.setParameters("@total_hogares", SqlDbType.Int, _total_hogares);
        base.setParameters("@num_personas_hogar", SqlDbType.Int, _num_personas_hogar);
        base.setParameters("@num_personas_vivienda", SqlDbType.Int, _num_personas_vivienda);
        base.setParameters("@email", SqlDbType.NVarChar, _email);
        base.setParameters("@resultado_final", SqlDbType.NVarChar, _resultado_final);
        base.setParameters("@departamento2", SqlDbType.Int, _departamento2);
        base.setParameters("@municipio2", SqlDbType.Int, _municipio2);
        base.setParameters("@pais", SqlDbType.Int, _pais);
        base.setParameters("@sexo", SqlDbType.Int, _sexo);
        base.setParameters("@nom_enc", SqlDbType.NVarChar, _nom_enc);
        base.setParameters("@autorizacion", SqlDbType.Int, _autorizacion);
        base.setParameters("@victim_guid", SqlDbType.NVarChar, _victim_guid);

        base.iniciarTransaccion();
        if (Insertar(lstrTabla, lstrCampos, lstrValores, lstrCondicion, true, true))
        {
            commit();
            return true;
        }
        else
        {
            rollback();
            return false;
        }
    }

    #endregion FUNCIONESCARACTERIZACION

    #region FUNCIONESEXPORT
    public Boolean fntConsultaMatrizVictimas_bol(string dataset)
    {
        string lstrCampos = " v.* ";
        string lstrTabla = " Victima v";
        string lstrCondicion = "";
        string lstrOrden = "";


        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }
    #endregion FUNCIONESEXPORT

    #region FUNCIONESVARIABLES
    public bool fntConsultaVariablesAdmin(string dataset, string strNombreVariable)
    {
        string lstrCampos = "v.*";
        string lstrTabla = " car_Variable v ";
        string lstrCondicion = "";
        string lstrOrden = " v.idVariable ";

        base.clearParameters();
        if (!string.IsNullOrWhiteSpace(strNombreVariable))
        {
            setParameters("@strNombreVariable", SqlDbType.NVarChar, strNombreVariable);
            lstrCondicion += " v.NombreVariable = @strNombreVariable ";
        }

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntModificarVariable_bol(int intVariable, string strVariable, string strPregunta)
    {
        string lstrTabla = " car_Variable ";
        string lstrCamposValores = " nombreVariable = @strVariable, preguntaFormulario = @strPregunta ";
        string lstrCondicion = " idVariable = @intVariable ";

        base.clearParameters();
        setParameters("@strVariable", SqlDbType.NVarChar, strVariable);
        setParameters("@strPregunta", SqlDbType.NVarChar, strPregunta);
        setParameters("@intVariable", SqlDbType.Int, intVariable);

        base.iniciarTransaccion();
        if (!Modificar(lstrTabla, lstrCamposValores, lstrCondicion, true))
        {
            rollback();
            return false;
        }
        else
        {
            commit();
            return true;
        }
    }

    public Boolean fntConsultaVariable_bol(string dataset, int idVariable)
    {
        string lstrCampos = " v.* ";
        string lstrTabla = " car_Variable v";
        string lstrCondicion = "v.idVariable = @idVariable ";
        string lstrOrden = "";

        base.clearParameters();
        setParameters("@idVariable", SqlDbType.Int, idVariable);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    public Boolean fntConsultaValorXVariable_bol(string dataset, int idVariable)
    {
        string lstrCampos = " v.* ";
        string lstrTabla = " car_ValorXVariable v";
        string lstrCondicion = "v.idValorXVariable = @idVariable ";
        string lstrOrden = "";

        base.clearParameters();
        setParameters("@idVariable", SqlDbType.Int, idVariable);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntConsultaSoloVariableCantidad_bol(string dataset, string strVariable)
    {
        string lstrCampos = " COUNT(*) CANTIDAD ";
        string lstrTabla = " car_Variable v";
        string lstrCondicion = "v.nombreVariable = @strVariable ";
        string lstrOrden = " CANTIDAD ";

        base.clearParameters();
        setParameters("@strVariable", SqlDbType.NVarChar, strVariable);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntIngresarVariableNew_bol(string strNombreVariable, string strPreguntaFormulario)
    {
        string lstrCampos = " nombreVariable, preguntaFormulario ";
        string lstrTabla = " car_Variable ";
        string lstrValores = " @strNombreVariable, @strPreguntaFormulario ";
        string lstrCondicion = " nombreVariable = @strNombreVariable";

        base.clearParameters();
        base.setParameters("@strNombreVariable", SqlDbType.NVarChar, strNombreVariable);
        base.setParameters("@strPreguntaFormulario", SqlDbType.NVarChar, strPreguntaFormulario);

        base.iniciarTransaccion();
        if (Insertar(lstrTabla, lstrCampos, lstrValores, lstrCondicion, true, true))
        {
            commit();
            return true;
        }
        else
        {
            rollback();
            return false;
        }
    }

    public Boolean fntEliminarVariable_bol(int intidVariable)
    {
        string lstrTabla = " car_Variable ";
        string lstrCondicion = " idVariable = @intidVariable ";

        base.clearParameters();
        setParameters("@intidVariable", SqlDbType.Int, intidVariable);

        if (Eliminar(lstrTabla, lstrCondicion, false))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #endregion FUNCIONESVARIABLES

    #region FUNCIONESOPCIONESVARIABLES
    public bool fntConsultaOpcionesVariablesAdmin(string dataset, string strNombreVariable)
    {
        string lstrCampos = "vxv.*, v.nombreVariable";
        string lstrTabla = " car_ValorXVariable vxv, car_Variable v ";
        string lstrCondicion = " v.idVariable = vxv.idVariable ";
        string lstrOrden = " vxv.idVariable, vxv.identificadorValor ";

        base.clearParameters();
        if (!string.IsNullOrWhiteSpace(strNombreVariable))
        {
            setParameters("@strNombreVariable", SqlDbType.NVarChar, strNombreVariable);
            lstrCondicion += " AND v.NombreVariable = @strNombreVariable ";
        }

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntModificarOpcionesVariables_bol(int intidValorXVariable, int intidVariable, int intidentificadorValor, string strtextoValor)
    {
        string lstrTabla = " car_ValorXVariable ";
        string lstrCamposValores = " idVariable = @intidVariable, identificadorValor = @intidentificadorValor, textoValor = @strtextoValor ";
        string lstrCondicion = " idValorXVariable = @intidValorXVariable ";

        base.clearParameters();
        base.setParameters("@intidValorXVariable", SqlDbType.Int, intidValorXVariable);
        base.setParameters("@intidVariable", SqlDbType.Int, intidVariable);
        base.setParameters("@intidentificadorValor", SqlDbType.Int, intidentificadorValor);
        base.setParameters("@strtextoValor", SqlDbType.NVarChar, strtextoValor);

        base.iniciarTransaccion();
        if (!Modificar(lstrTabla, lstrCamposValores, lstrCondicion, true))
        {
            rollback();
            return false;
        }
        else
        {
            commit();
            return true;
        }
    }

    public Boolean fntConsultaOpcionesVariables_bol(string dataset, int idValorXVariable)
    {
        string lstrCampos = " v.* ";
        string lstrTabla = " car_ValorXVariable v";
        string lstrCondicion = "v.idValorXVariable = @idValorXVariable ";
        string lstrOrden = "";

        base.clearParameters();
        base.setParameters("@idValorXVariable", SqlDbType.Int, idValorXVariable);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntConsultaSoloOpcionesVariablesCantidad_bol(string dataset, int intidentificadorValor, string strtextoValor)
    {
        string lstrCampos = " COUNT(*) CANTIDAD ";
        string lstrTabla = " car_ValorXVariable v";
        string lstrCondicion = " identificadorValor = @intidentificadorValor AND v.textoValor = @strtextoValor ";
        string lstrOrden = " CANTIDAD ";

        base.clearParameters();
        base.setParameters("@intidentificadorValor", SqlDbType.Int, intidentificadorValor);
        base.setParameters("@strtextoValor", SqlDbType.NVarChar, strtextoValor);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntIngresarOpcionesVariablesNew_bol(int intidVariable, int intidentificadorValor, string strtextoValor)
    {
        string lstrCampos = " idVariable , identificadorValor , textoValor ";
        string lstrTabla = " car_ValorXVariable ";
        string lstrValores = " @intidVariable, @intidentificadorValor, @strtextoValor ";
        string lstrCondicion = " idVariable = @intidVariable AND  identificadorValor = @intidentificadorValor AND textoValor = @strtextoValor ";

        base.clearParameters();
        base.setParameters("@intidVariable", SqlDbType.Int, intidVariable);
        base.setParameters("@intidentificadorValor", SqlDbType.Int, intidentificadorValor);
        base.setParameters("@strtextoValor", SqlDbType.NVarChar, strtextoValor);

        base.iniciarTransaccion();
        if (Insertar(lstrTabla, lstrCampos, lstrValores, lstrCondicion, true, true))
        {
            commit();
            return true;
        }
        else
        {
            rollback();
            return false;
        }
    }

    public Boolean fntEliminarOpcionesVariables_bol(int idValorXVariable)
    {
        string lstrTabla = " car_ValorXVariable ";
        string lstrCondicion = " idValorXVariable = @idValorXVariable ";

        base.clearParameters();
        base.setParameters("@idValorXVariable", SqlDbType.Int, idValorXVariable);

        if (Eliminar(lstrTabla, lstrCondicion, false))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #endregion FUNCIONESOPCIONESVARIABLES

    public Boolean fntConsultarSeguimientoEspecifico(string dataset, int idSeguimiento)
    {
        string lstrCampos = " seop_RutaDocumento ";
        string lstrTabla = " tbl_SeguimientoOperador ";
        string lstrCondicion = " seop_Id = @idSeguimiento";
        string lstrOrden = " seop_Id DESC ";
        this.clearParameters();
        setParameters("@idSeguimiento", SqlDbType.Int, idSeguimiento);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    #region FUNCIONESWFSolicitud

    public Boolean fntConsultaTodo(string dataset, string nombreTabla, string campoOrden)
    {
        string lstrCampos = " * ";
        string lstrTabla = " " + nombreTabla + " ";
        string lstrCondicion = " 1 = 1 ";
        //string groupBy = " fechaCarga ";
        string lstrOrden = " " + campoOrden + " ";
        base.clearParameters();
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntConsultarPorEstado(string dataset, string nombreTabla, string nombreCampoEstado, string estado, string campoOrden)
    {
        string lstrCampos = " * ";
        string lstrTabla = " " + nombreTabla + " ";
        string lstrCondicion = nombreCampoEstado + " = " + "@" + nombreCampoEstado;
        //string groupBy = " fechaCarga ";
        string lstrOrden = " " + campoOrden + " ";
        base.clearParameters();
        base.setParameters("@" + nombreCampoEstado, SqlDbType.NVarChar, estado);
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    /// <summary>
    /// ☻☻ metodo para buscar los planes de una solicitud ☻☻
    /// </summary>
    /// <param name="dataset"></param>
    /// <param name="idSolicitud"></param>
    /// <param name="modo"></param>
    /// <param name="idPlan"></param>
    /// <returns></returns>
    public Boolean fntConsultarPlanes(string dataset, Int64 idSolicitud, string modo, Int64 idPlan)
    {
        string lstrCampos = " pa.idPlanAccion, pa.idEstrategia, pa.idActividad, pa.idIndicador, es.nombreEstrategia as Estrategia, ac.nombreActividad as Actividad, ac.descripcionActividad as Descripcion, ind.nombreIndicador as Indicador, pa.fechaModificacion, pa.idSolicitudRef  ,ind.valorIndicador , planej.ejecucionPorcentaje, planej.planEjecutado";
        StringBuilder condicion = new StringBuilder();

        condicion.Append(" WFSol_PlanAccion as pa  INNER JOIN   WFSol_Estrategia as es on pa.idEstrategia = es.idEstrategia  ");
        condicion.Append(" inner join WFSol_Actividad as ac on   pa.idActividad = ac.idActividad ");
        condicion.Append("  inner join WFSol_Indicador as ind  on pa.idIndicador = ind.idIndicador ");
        condicion.Append(" left join WFSol_PlanEjecutado as planej on pa.idPlanAccion = planej.idPlanAccionRef");
        condicion.AppendFormat(" where pa.idSolicitudRef = {0}", idSolicitud);
        string lstrOrden = " pa.fechaModificacion DESC ";
        base.clearParameters();
        base.setParameters("@idSolicitud", System.Data.SqlDbType.BigInt, idSolicitud);
        if (modo == "id")
        {
            condicion.Append("AND pa.idPlanAccion = @idPlan");
            base.setParameters("@idPlan", System.Data.SqlDbType.BigInt, idPlan);
        }

        string lstrTabla = condicion.ToString();

        if (!Consultar(lstrCampos, lstrTabla, string.Empty, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntConsultarSitios(string dataset, Int64 idSolicitud, string modo, Int64 idSitio)
    {
        string lstrCampos = " ss.idSitiosSol, ss.idDepartamento, ss.idMunicipio, dep.nomDepartamento as Departamento, mun.nomMunicipio as Municipio, ss.ubicacionEspecifica as Ubicacion, ss.fechaModificacion as Fecha, ss.idSolicitudRef ";
        string lstrTabla = " WFSol_SitiosSol as ss, tbl_Departamento as dep, tbl_Municipio as mun ";
        string lstrCondicion = " ss.idDepartamento = dep.codDepartamentoAlf2 AND ss.idMunicipio = mun.codMunicipioAlf5 AND ss.idSolicitudRef = @idSolicitud ";
        //string groupBy = " fechaCarga ";
        string lstrOrden = " ss.fechaModificacion DESC ";
        base.clearParameters();
        base.setParameters("@idSolicitud", System.Data.SqlDbType.BigInt, idSolicitud);
        if (modo == "id")
        {
            lstrCondicion = lstrCondicion + " AND ss.idSitiosSol = @idSitio ";
            base.setParameters("@idSitio", System.Data.SqlDbType.BigInt, idSitio);
        }
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntConsultarSoporte(string dataset, Int64 idSolicitud, string modo, Int64 idSoporte)
    {
        string lstrCampos = " ss.* ";
        string lstrTabla = " WFSol_SoporteSol as ss";
        string lstrCondicion = " ss.idSolicitudRef = @idSolicitud ";
        //string groupBy = " fechaCarga ";
        string lstrOrden = " ss.fechaModificacion DESC ";
        base.clearParameters();
        base.setParameters("@idSolicitud", System.Data.SqlDbType.BigInt, idSolicitud);
        if (modo == "id")
        {
            lstrCondicion = lstrCondicion + " AND ss.idSoporteSol = @idSoporte ";
            base.setParameters("@idSoporte", System.Data.SqlDbType.BigInt, idSoporte);
        }
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    /// <summary>
    ///☻☻ metodo  para buscar los beneficiarios  por legalizacion 
    /// </summary>
    /// <param name="dataset"></param>
    /// <param name="modo"></param>
    /// <param name="idLegalizacion"></param>
    /// <param name="idBeneficioLega"></param>
    /// <returns></returns>
    public Boolean fntConsultarBeneficiarios(string dataset, string modo, Int64 idLegalizacion, Int64 idBeneficioLega)
    {
        string lstrCampos = "bnl.*,bnf.nombre ,bnf.idBeneficio ";
        StringBuilder condicion = new StringBuilder();

        condicion.Append(" WFSol_Legalizacion as bn  INNER JOIN   WFSol_BeneficioLegalizacion as bnl on bnl.idLegalizacion = bn.idLegalizacion  ");
        condicion.Append(" INNER JOIN  WFSol_Beneficio as bnf on bnf.idBeneficio = bnl.idBeneficio");



        condicion.AppendFormat(" where bn.idLegalizacion = {0}", idLegalizacion);
        string lstrOrden = " bnl.fechaCreacion DESC ";
        base.clearParameters();
        base.setParameters("@idLegalizacion", System.Data.SqlDbType.BigInt, idLegalizacion);
        if (modo == "id")
        {
            condicion.Append("AND bnl.idBeneficioLega = @idBeneficioLega");
            base.setParameters("@idBeneficioLega", System.Data.SqlDbType.BigInt, idBeneficioLega);
        }

        string lstrTabla = condicion.ToString();

        if (!Consultar(lstrCampos, lstrTabla, string.Empty, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    public Boolean fntConsultarCompromisos(string dataset, string modo, Int64 idLegalizacion, Int64 idCompromiso)
    {
        string lstrCampos = "Cpro.* ";
        StringBuilder condicion = new StringBuilder();

        condicion.Append(" WFSol_Legalizacion as bn  INNER JOIN   WFSol_Compromisos as Cpro on Cpro.idLegalizacionRef = bn.idLegalizacion  ");
        condicion.AppendFormat(" where bn.idLegalizacion = {0}", idLegalizacion);
        string lstrOrden = " Cpro.fechaCreacion DESC ";
        base.clearParameters();
        base.setParameters("@idLegalizacion", System.Data.SqlDbType.BigInt, idLegalizacion);
        if (modo == "id")
        {
            condicion.Append("AND Cpro.idCompromiso = @idCompromiso");
            base.setParameters("@idCompromiso", System.Data.SqlDbType.BigInt, idCompromiso);
        }

        string lstrTabla = condicion.ToString();

        if (!Consultar(lstrCampos, lstrTabla, string.Empty, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }









    public Boolean fntConsultarSoporteLegalizacion(string dataset, Int64 idSolicitud, string modo, Int64 idSoporte)
    {
        string lstrCampos = " ss.* ";
        string lstrTabla = " WFSol_SoporteSolLegalizacion as ss";
        string lstrCondicion = " ss.idSolicitudRef = @idSolicitud ";
        //string groupBy = " fechaCarga ";
        string lstrOrden = " ss.fechaModificacion DESC ";
        base.clearParameters();
        base.setParameters("@idSolicitud", System.Data.SqlDbType.BigInt, idSolicitud);
        if (modo == "id")
        {
            lstrCondicion = lstrCondicion + " AND ss.idSoporteSol = @idSoporte ";
            base.setParameters("@idSoporte", System.Data.SqlDbType.BigInt, idSoporte);
        }
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// ☻☻ validara por id Soporte
    /// </summary>
    /// <param name="dataset"></param>
    /// <param name="idSolicitud"></param>
    /// <param name="modo"></param>
    /// <param name="idSoporte"></param>
    /// <returns></returns>
    public Boolean fntConsultarSoporteXId(string dataset, string modo, Int64 idSoporte)
    {
        string lstrCampos = " ss.* ";
        string lstrTabla = " WFSol_SoporteSol as ss";
        string lstrCondicion = " ss.idSoporteSol = @idSoporteSol ";
        //string groupBy = " fechaCarga ";
        string lstrOrden = " ss.fechaModificacion DESC ";
        base.clearParameters();
        base.setParameters("@idSoporteSol", System.Data.SqlDbType.BigInt, idSoporte);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    ///// <summary>
    ///// ☻☻ validara por id Soporte
    ///// </summary>
    ///// <param name="dataset"></param>
    ///// <param name="idSolicitud"></param>
    ///// <param name="modo"></param>
    ///// <param name="idSoporte"></param>
    ///// <returns></returns>
    //public Boolean fntConsultarSoporteXIdLegalizacion(string dataset, string modo, Int64 idSoporte)
    //{
    //    string lstrCampos = " ss.* ";
    //    string lstrTabla = " WFSol_SoporteSolLegalizacion as ss";
    //    string lstrCondicion = " ss.idSoporteSol = @idSoporteSol ";
    //    //string groupBy = " fechaCarga ";
    //    string lstrOrden = " ss.fechaModificacion DESC ";
    //    base.clearParameters();
    //    base.setParameters("@idSoporteSol", System.Data.SqlDbType.BigInt, idSoporte);

    //    if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
    //    {
    //        return false;
    //    }
    //    return true;
    //}

    /// <summary>
    /// ☻☻  metodo para consultar el soporte de legalizacion por el id del soporte 
    /// </summary>
    /// <param name="dataset"></param>
    /// <param name="modo"></param>
    /// <param name="idSoporte"></param>
    /// <returns></returns>
    public Boolean fntConsultarSoporteXIdLegalizacion(string dataset, string modo, Int64 idSoporte)
    {
        string lstrCampos = " ss.* ";
        string lstrTabla = " WFSol_SoporteSolLegalizacion as ss";
        string lstrCondicion = " ss.idSoporteSol = @idSoporteSol ";
        //string groupBy = " fechaCarga ";
        string lstrOrden = " ss.fechaModificacion DESC ";
        base.clearParameters();
        base.setParameters("@idSoporteSol", System.Data.SqlDbType.BigInt, idSoporte);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;


    }





    public Boolean fntConsultarSolicitud(string dataset, string modo, Int64 idSolicitud)
    {
        string lstrCampos = " sc.* , e.nombre as nombreestado ";
        string lstrTabla = " WFSol_SolicitudComision as sc, WFSol_Estados as e ";
        string lstrCondicion = " sc.idEstadoActual = e.idEstado ";
        //string groupBy = " fechaCarga ";
        string lstrOrden = " sc.fechaModificacion DESC ";
        if (modo == "id")
        {
            lstrCondicion = lstrCondicion + " AND sc.idSolicitud = @idSolicitud ";
            base.clearParameters();
            base.setParameters("@idSolicitud", SqlDbType.NVarChar, idSolicitud);
        }
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntConsultaSolicitudPorLike(string dataset, string nombreID, string valorId, int idUsuariocreo)
    {
        string lstrCampos = " sc.* , e.nombre as nombreestado ";
        string lstrTabla = " WFSol_SolicitudComision as sc, WFSol_Estados as e ";
        string lstrCondicion = " sc.idEstadoActual = e.idEstado AND " + nombreID + " LIKE  " + "'%" + valorId + "%'" + " AND sc.idUsuarioCreacion = @idUsuarioCreacion AND ((sc.idEstadoActual = @IdestadoActual) OR (sc.idEstadoActual = @otroEstadoActual) OR (sc.idEstadoActual = @otroAdminEstadoActual))  ";
        //string groupBy = " fechaCarga ";
        string lstrOrden = string.Empty;
        base.clearParameters();
        base.setParameters("@" + nombreID, SqlDbType.NVarChar, valorId);
        base.setParameters("@idUsuarioCreacion", SqlDbType.Int, idUsuariocreo);
        base.setParameters("@IdestadoActual", SqlDbType.Int, EstadosSolicitud.AprobadoPorDARPLegalizar);
        base.setParameters("@otroEstadoActual", SqlDbType.Int, EstadosSolicitud.LegRechazadaCoordinador);
        base.setParameters("@otroAdminEstadoActual", SqlDbType.Int, EstadosSolicitud.LegRechazadaAdministrador);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    public Boolean fntConsultarPorLegalizar(string dataset, int idUsuariocreo)
    {
        string lstrCampos = " sc.* , e.nombre as nombreestado ";
        string lstrTabla = " WFSol_SolicitudComision as sc, WFSol_Estados as e ";
        string lstrCondicion = " sc.idEstadoActual = e.idEstado AND sc.idUsuarioCreacion = @idUsuarioCreacion AND ((sc.idEstadoActual = @IdestadoActual) OR (sc.idEstadoActual = @otroEstadoActual) OR (sc.idEstadoActual = @otroAdminEstadoActual))  ";
        //string groupBy = " fechaCarga ";
        string lstrOrden = string.Empty;
        base.clearParameters();
        base.setParameters("@idUsuarioCreacion", SqlDbType.Int, idUsuariocreo);
        base.setParameters("@IdestadoActual", SqlDbType.Int, EstadosSolicitud.AprobadoPorDARPLegalizar);
        base.setParameters("@otroEstadoActual", SqlDbType.Int, EstadosSolicitud.LegRechazadaCoordinador);
        base.setParameters("@otroAdminEstadoActual", SqlDbType.Int, EstadosSolicitud.LegRechazadaAdministrador);

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    /// <summary>
    /// ☻☻
    /// </summary>
    /// <param name="dataset"></param>
    /// <param name="modo"></param>
    /// <param name="idSolicitud"></param>
    /// <returns></returns>
    public Boolean fntConsultarSolicitudEstado(string dataset, string modo, Int64 idSolicitud)
    {
        string lstrCampos = " sc.* , e.nombre as nombreestado ";
        string lstrTabla = " WFSol_SolicitudComision as sc, WFSol_Estados as e ";
        string lstrCondicion = " sc.idEstadoActual = e.idEstado ";
        //string groupBy = " fechaCarga ";
        string lstrOrden = " sc.fechaModificacion DESC ";
        if (modo == "id")
        {
            lstrCondicion = lstrCondicion + " AND sc.idSolicitud = @idSolicitud  and (sc.idEstadoActual = @AprobadoPorDARPLegalizar   or sc.idEstadoActual = @LegalizacionCompletaCerrado)";
            base.clearParameters();
            base.setParameters("@idSolicitud", SqlDbType.NVarChar, idSolicitud);
            base.setParameters("@AprobadoPorDARPLegalizar", SqlDbType.Int, EstadosSolicitud.PorAprobarAdministrador);
            base.setParameters("@LegalizacionCompletaCerrado", SqlDbType.Int, EstadosSolicitud.AprobadoPorDARPLegalizar);


        }

        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }



    //☻☻ metodo para consultar las solicitudes con para permitir edicion 
    public Boolean fntConsultarSolicitudEstadoPermitir(string dataset)
    {
        string lstrCampos = " sc.* , e.nombre as nombreestado ";
        string lstrTabla = " WFSol_SolicitudComision as sc, WFSol_Estados as e ";
        string lstrCondicion = " sc.idEstadoActual = e.idEstado ";
        //string groupBy = " fechaCarga ";
        string lstrOrden = " sc.fechaModificacion DESC ";

        lstrCondicion = lstrCondicion + " and (sc.idEstadoActual = @AprobadoPorDARPLegalizar   or sc.idEstadoActual = @LegalizacionCompletaCerrado)";
        base.clearParameters();
        base.setParameters("@AprobadoPorDARPLegalizar", SqlDbType.Int, EstadosSolicitud.PorAprobarAdministrador);
        base.setParameters("@LegalizacionCompletaCerrado", SqlDbType.Int, EstadosSolicitud.AprobadoPorDARPLegalizar);


        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }








    public Boolean fntConsultarSolicitudEspecial(string dataset, string IdentificadorSolicitud)
    {
        string lstrCampos = " sc.idSolicitud ";
        string lstrTabla = " WFSol_SolicitudComision as sc ";
        string lstrCondicion = " sc.IdentificadorSolicitud = @identificadorSolicitudStr  ";
        string lstrOrden = " sc.fechaModificacion DESC ";
        base.clearParameters();
        base.setParameters("@identificadorSolicitudStr", SqlDbType.NVarChar, IdentificadorSolicitud);
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// Ingresar Solicitud permite agregar una nueva solicitud en estado incompleta hasta que no se completen plan de accion, 
    /// Sitios intervencion y Soportes respectivos
    /// </summary>
    /// <param name="lineaIntervencion"></param>
    /// <param name="nombre"></param>
    /// <param name="objeto"></param>
    /// <param name="fechaInicio"></param>
    /// <param name="fechaFin"></param>
    /// <param name="tiquetesAereos"></param>
    /// <param name="rutaAerea"></param>
    /// <param name="porcionTerrestre"></param>
    /// <param name="rutaTerrestre"></param>
    /// <param name="viaticos"></param>
    /// <param name="valorDiario"></param>
    /// <param name="numeroDias"></param>
    /// <param name="totalViaticos"></param>
    /// <param name="idUsuarioCrea"></param>
    /// <param name="idUsuarioModificacion"></param>
    /// <param name="fechaCreaModifica"></param>
    /// <param name="idEstadoActual"></param>
    /// <param name="identificadorUnico"></param>
    /// <returns></returns>
    public bool fntRegistrarSolicitud_bol(int lineaIntervencion, string nombre, string objeto, DateTime fechaInicio, DateTime fechaFin, bool tiquetesAereos, string rutaAerea, bool porcionTerrestre, string rutaTerrestre, bool viaticos, decimal valorDiario, decimal numeroDias, decimal totalViaticos, int idUsuarioCrea, int idUsuarioModificacion, DateTime fechaCreaModifica, int idEstadoActual, string identificadorUnico, string cargoFuncionario, string lugarViaje, string monedaViatico, decimal valorRutaPorTerrestre)
    {
        string strCampos = " numeroAprobacion , idLineaIntervencion , nombreFuncionario , objetoComision , fechaInicio , numeroDias , fechaFin , tiquetesAereos , rutaAerea , porcionTerrestre , rutaPorTerrestre , requiereViaticos , valorDiarioViatico , totalViaticos , fechaCreacion , fechaModificacion , idUsuarioCreacion , idUsuarioModificacion, idEstadoActual , IdentificadorSolicitud, cargoFuncionario, lugarViaje, monedaViatico, valorRutaPorTerrestre ";
        string strTabla = " WFSol_SolicitudComision ";
        string strValores = " @numeroAprobacionStr , @idLineaIntervencionStr , @nombreFuncionarioStr , @objetoComisionStr , @fechaInicioStr , @numeroDiasStr , @fechaFinStr , @tiquetesAereosStr , @rutaAereaStr , @porcionTerrestreStr , @rutaPorTerrestreStr , @requiereViaticosStr , @valorDiarioViaticoStr , @totalViaticosStr , @fechaCreacionStr , @fechaModificacionStr , @idUsuarioCreacionStr , @idUsuarioModificacionStr , @idEstadoActualStr , @IdentificadorSolicitudStr, @cargoFuncionario, @lugarViaje, @monedaViatico, @valorRutaPorTerrestre ";
        string strCondicion = " numeroAprobacion = @numeroAprobacionStr AND  idLineaIntervencion = @idLineaIntervencionStr AND nombreFuncionario = @nombreFuncionarioStr AND  objetoComision = @objetoComisionStr AND fechaInicio = @fechaInicioStr AND numeroDias = @numeroDiasStr AND fechaFin = @fechaFinStr AND tiquetesAereos = @tiquetesAereosStr AND rutaAerea = @rutaAereaStr AND porcionTerrestre = @porcionTerrestreStr AND rutaPorTerrestre = @rutaPorTerrestreStr AND requiereViaticos = @requiereViaticosStr AND valorDiarioViatico = @valorDiarioViaticoStr AND totalViaticos = @totalViaticosStr AND fechaCreacion = @fechaCreacionStr AND fechaModificacion = @fechaModificacionStr AND idUsuarioCreacion = @idUsuarioCreacionStr AND idUsuarioModificacion = @idUsuarioModificacionStr AND idEstadoActual = @idEstadoActualStr AND  IdentificadorSolicitud = @IdentificadorSolicitudStr AND cargoFuncionario = @cargoFuncionario AND lugarViaje = @lugarViaje AND monedaViatico = @monedaViatico AND valorRutaPorTerrestre = @valorRutaPorTerrestre ";
        base.clearParameters();
        base.setParameters("@numeroAprobacionStr", System.Data.SqlDbType.Int, 0);
        base.setParameters("@idLineaIntervencionStr", System.Data.SqlDbType.Int, lineaIntervencion);
        base.setParameters("@nombreFuncionarioStr", System.Data.SqlDbType.NVarChar, nombre);
        base.setParameters("@objetoComisionStr", System.Data.SqlDbType.NVarChar, objeto);
        base.setParameters("@fechaInicioStr", System.Data.SqlDbType.DateTime, fechaInicio);
        base.setParameters("@numeroDiasStr", System.Data.SqlDbType.Decimal, numeroDias);
        base.setParameters("@fechaFinStr", System.Data.SqlDbType.DateTime, fechaFin);
        base.setParameters("@tiquetesAereosStr", System.Data.SqlDbType.Bit, tiquetesAereos);
        base.setParameters("@rutaAereaStr", System.Data.SqlDbType.NVarChar, rutaAerea);
        base.setParameters("@porcionTerrestreStr", System.Data.SqlDbType.Bit, porcionTerrestre);
        base.setParameters("@rutaPorTerrestreStr", System.Data.SqlDbType.NVarChar, rutaTerrestre);
        base.setParameters("@requiereViaticosStr", System.Data.SqlDbType.Bit, viaticos);
        base.setParameters("@valorDiarioViaticoStr", System.Data.SqlDbType.Decimal, valorDiario);
        base.setParameters("@totalViaticosStr", System.Data.SqlDbType.Decimal, totalViaticos);
        base.setParameters("@fechaCreacionStr", System.Data.SqlDbType.DateTime, fechaCreaModifica);
        base.setParameters("@fechaModificacionStr", System.Data.SqlDbType.DateTime, fechaCreaModifica);
        base.setParameters("@idUsuarioCreacionStr", System.Data.SqlDbType.Int, idUsuarioCrea);
        base.setParameters("@idUsuarioModificacionStr", System.Data.SqlDbType.Int, idUsuarioModificacion);
        base.setParameters("@idEstadoActualStr", System.Data.SqlDbType.Int, idEstadoActual);
        base.setParameters("@IdentificadorSolicitudStr", System.Data.SqlDbType.NVarChar, identificadorUnico);
        base.setParameters("@cargoFuncionario", System.Data.SqlDbType.NVarChar, cargoFuncionario);
        base.setParameters("@lugarViaje", System.Data.SqlDbType.NVarChar, lugarViaje);
        base.setParameters("@monedaViatico", System.Data.SqlDbType.NVarChar, monedaViatico);
        base.setParameters("@valorRutaPorTerrestre", System.Data.SqlDbType.Decimal, valorRutaPorTerrestre);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        else
        {
            base.rollback();
            return false;
        }
    }




    /// <summary>
    /// Ingresar Estado, es quien actualiza el historico de estados de una solicitud
    /// </summary>
    /// <param name="idSolicitudRef"></param>
    /// <param name="idEstadoRef"></param>
    /// <param name="idUsuarioRef"></param>
    /// <param name="fechaEstado"></param>
    /// <param name="requiereNotificacion"></param>
    /// <returns></returns>
    public bool fntIngresarEstadoSolicitud_bol(Int64 idSolicitudRef, int idEstadoRef, int idUsuarioRef, DateTime fechaEstado, bool requiereNotificacion)
    {
        string strCampos = " idSolicitudRef, idEstadoRef, idUsuarioRef, fechaEstado, requiereNotificacion ";
        string strTabla = " WFSol_EstadosSolicitud ";
        string strValores = " @idSolicitudRefS, @idEstadoRefS, @idUsuarioRefS, @fechaEstadoS, @requiereNotificacionS ";
        string strCondicion = " idSolicitudRef = @idSolicitudRefS AND idEstadoRef = @idEstadoRefS AND idUsuarioRef = @idUsuarioRefS AND fechaEstado = @fechaEstadoS AND requiereNotificacion = @requiereNotificacionS ";
        base.clearParameters();
        base.setParameters("@idSolicitudRefS", System.Data.SqlDbType.BigInt, idSolicitudRef);
        base.setParameters("@idEstadoRefS", System.Data.SqlDbType.Int, idEstadoRef);
        base.setParameters("@idUsuarioRefS", System.Data.SqlDbType.Int, idUsuarioRef);
        base.setParameters("@fechaEstadoS", System.Data.SqlDbType.DateTime, fechaEstado);
        base.setParameters("@requiereNotificacionS", System.Data.SqlDbType.Bit, requiereNotificacion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }




    public bool fntRegistrarSolicitudHistorico_bol(long idSolicitud, int lineaIntervencion, string nombre, string objeto, DateTime fechaInicio, DateTime fechaFin, bool tiquetesAereos, string rutaAerea, bool porcionTerrestre, string rutaTerrestre, bool viaticos, decimal valorDiario, decimal numeroDias, decimal totalViaticos, int idUsuarioCrea, int idUsuarioModificacion, DateTime fechaCreaModifica, int idEstadoActual, string identificadorUnico)
    {
        string strCampos = "idSolicitud, numeroAprobacion, idLineaIntervencion, nombreFuncionario, objetoComision, fechaInicio, numeroDias, fechaFin, tiquetesAereos, rutaAerea, porcionTerrestre, rutaPorTerrestre, requiereViaticos, valorDiarioViatico, totalViaticos, fechaCreacion, fechaModificacion, idUsuarioCreacion, idUsuarioModificacion, idEstadoActual, IdentificadorSolicitud ";
        string strTabla = " WFSol_SolicitudHistorico ";
        string strValores = " @idSolicitudStr ,@numeroAprobacionStr , @idLineaIntervencionStr , @nombreFuncionarioStr , @objetoComisionStr , @fechaInicioStr , @numeroDiasStr , @fechaFinStr , @tiquetesAereosStr , @rutaAereaStr , @porcionTerrestreStr , @rutaPorTerrestreStr , @requiereViaticosStr , @valorDiarioViaticoStr , @totalViaticosStr , @fechaCreacionStr , @fechaModificacionStr , @idUsuarioCreacionStr , @idUsuarioModificacionStr , @idEstadoActualStr , @IdentificadorSolicitudStr ";
        string strCondicion = "idSolicitud = @idSolicitudStr  AND numeroAprobacion = @numeroAprobacionStr AND  idLineaIntervencion = @idLineaIntervencionStr AND nombreFuncionario = @nombreFuncionarioStr AND  objetoComision = @objetoComisionStr AND fechaInicio = @fechaInicioStr AND numeroDias = @numeroDiasStr AND fechaFin = @fechaFinStr AND tiquetesAereos = @tiquetesAereosStr AND rutaAerea = @rutaAereaStr AND porcionTerrestre = @porcionTerrestreStr AND rutaPorTerrestre = @rutaPorTerrestreStr AND requiereViaticos = @requiereViaticosStr AND valorDiarioViatico = @valorDiarioViaticoStr AND totalViaticos = @totalViaticosStr AND fechaCreacion = @fechaCreacionStr AND fechaModificacion = @fechaModificacionStr AND idUsuarioCreacion = @idUsuarioCreacionStr AND idUsuarioModificacion = @idUsuarioModificacionStr AND idEstadoActual = @idEstadoActualStr AND  IdentificadorSolicitud = @IdentificadorSolicitudStr  ";
        base.clearParameters();
        base.setParameters("@idSolicitudStr", System.Data.SqlDbType.BigInt, idSolicitud);
        base.setParameters("@numeroAprobacionStr", System.Data.SqlDbType.Int, 0);
        base.setParameters("@idLineaIntervencionStr", System.Data.SqlDbType.Int, lineaIntervencion);
        base.setParameters("@nombreFuncionarioStr", System.Data.SqlDbType.NVarChar, nombre);
        base.setParameters("@objetoComisionStr", System.Data.SqlDbType.NVarChar, objeto);
        base.setParameters("@fechaInicioStr", System.Data.SqlDbType.DateTime, fechaInicio);
        base.setParameters("@numeroDiasStr", System.Data.SqlDbType.Decimal, numeroDias);
        base.setParameters("@fechaFinStr", System.Data.SqlDbType.DateTime, fechaFin);
        base.setParameters("@tiquetesAereosStr", System.Data.SqlDbType.Bit, tiquetesAereos);
        base.setParameters("@rutaAereaStr", System.Data.SqlDbType.NVarChar, rutaAerea);
        base.setParameters("@porcionTerrestreStr", System.Data.SqlDbType.Bit, porcionTerrestre);
        base.setParameters("@rutaPorTerrestreStr", System.Data.SqlDbType.NVarChar, rutaTerrestre);
        base.setParameters("@requiereViaticosStr", System.Data.SqlDbType.Bit, viaticos);
        base.setParameters("@valorDiarioViaticoStr", System.Data.SqlDbType.Decimal, valorDiario);
        base.setParameters("@totalViaticosStr", System.Data.SqlDbType.Decimal, totalViaticos);
        base.setParameters("@fechaCreacionStr", System.Data.SqlDbType.DateTime, fechaCreaModifica);
        base.setParameters("@fechaModificacionStr", System.Data.SqlDbType.DateTime, fechaCreaModifica);
        base.setParameters("@idUsuarioCreacionStr", System.Data.SqlDbType.Int, idUsuarioCrea);
        base.setParameters("@idUsuarioModificacionStr", System.Data.SqlDbType.Int, idUsuarioModificacion);
        base.setParameters("@idEstadoActualStr", System.Data.SqlDbType.Int, idEstadoActual);
        base.setParameters("@IdentificadorSolicitudStr", System.Data.SqlDbType.NVarChar, identificadorUnico);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        else
        {
            base.rollback();
            return false;
        }
    }








    /// <summary>
    /// Ingresar Plan de Accion es la segunda apre de la solicitud
    /// </summary>
    /// <param name="idSolicitudRef"></param>
    /// <param name="idEstrategia"></param>
    /// <param name="idActividad"></param>
    /// <param name="idIndicador"></param>
    /// <param name="idUsuarioCrea"></param>
    /// <param name="idUsuarioModificacion"></param>
    /// <param name="fechaCreaModifica"></param>
    /// <returns></returns>
    public bool fntIngresarPlanAccion_bol(Int64 idSolicitudRef, int idEstrategia, int idActividad, int idIndicador, int idUsuarioCrea, int idUsuarioModificacion, DateTime fechaCreaModifica)
    {
        string strCampos = " idSolicitudRef, idEstrategia, idActividad, idIndicador, fechaCreacion, fechaModificacion, idUsuarioCreacion, idUsuarioModificacion ";
        string strTabla = " WFSol_PlanAccion ";
        string strValores = " @idSolicitudRef, @idEstrategia, @idActividad, @idIndicador, @fechaCreacion, @fechaModificacion, @idUsuarioCreacion, @idUsuarioModificacion ";
        string strCondicion = " idSolicitudRef = @idSolicitudRef AND idEstrategia = @idEstrategia AND idActividad = @idActividad AND idIndicador = @idIndicador AND fechaCreacion = @fechaCreacion AND fechaModificacion = @fechaModificacion AND idUsuarioCreacion = @idUsuarioCreacion AND idUsuarioModificacion = @idUsuarioModificacion  ";
        base.clearParameters();
        base.setParameters("@idSolicitudRef", System.Data.SqlDbType.BigInt, idSolicitudRef);
        base.setParameters("@idEstrategia", System.Data.SqlDbType.Int, idEstrategia);
        base.setParameters("@idActividad", System.Data.SqlDbType.Int, idActividad);
        base.setParameters("@idIndicador", System.Data.SqlDbType.Int, idIndicador);
        base.setParameters("@fechaCreacion", System.Data.SqlDbType.DateTime, fechaCreaModifica);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, fechaCreaModifica);
        base.setParameters("@idUsuarioCreacion", System.Data.SqlDbType.Int, idUsuarioCrea);
        base.setParameters("@idUsuarioModificacion", System.Data.SqlDbType.Int, idUsuarioModificacion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }

    /// <summary>
    /// Inserta los sitios de intervencion de cada solicitud
    /// </summary>
    /// <param name="idSolicitudRef"></param>
    /// <param name="idDepartamento"></param>
    /// <param name="idMunicipio"></param>
    /// <param name="Ubicacion"></param>
    /// <param name="idUsuarioCrea"></param>
    /// <param name="idUsuarioModificacion"></param>
    /// <param name="fechaCreaModifica"></param>
    /// <returns></returns>
    public bool fntSitiosIntervencion_bol(Int64 idSolicitudRef, string idDepartamento, string idMunicipio, string Ubicacion, int idUsuarioCrea, int idUsuarioModificacion, DateTime fechaCreaModifica)
    {
        string strCampos = " idSolicitudRef, idDepartamento, idMunicipio, ubicacionEspecifica, fechaCreacion, fechaModificacion, idUsuarioCreacion, idUsuarioModificacion ";
        string strTabla = " WFSol_SitiosSol ";
        string strValores = " @idSolicitudRef, @idDepartamento, @idMunicipio, @ubicacionEspecifica, @fechaCreacion, @fechaModificacion, @idUsuarioCreacion, @idUsuarioModificacion ";
        string strCondicion = " idSolicitudRef = @idSolicitudRef AND idDepartamento = @idDepartamento AND idMunicipio = @idMunicipio AND ubicacionEspecifica = @ubicacionEspecifica AND fechaCreacion = @fechaCreacion AND fechaModificacion = @fechaModificacion AND idUsuarioCreacion = @idUsuarioCreacion AND idUsuarioModificacion = @idUsuarioModificacion  ";
        base.clearParameters();
        base.setParameters("@idSolicitudRef", System.Data.SqlDbType.BigInt, idSolicitudRef);
        base.setParameters("@idDepartamento", System.Data.SqlDbType.NVarChar, idDepartamento);
        base.setParameters("@idMunicipio", System.Data.SqlDbType.NVarChar, idMunicipio);
        base.setParameters("@ubicacionEspecifica", System.Data.SqlDbType.NVarChar, Ubicacion);
        base.setParameters("@fechaCreacion", System.Data.SqlDbType.DateTime, fechaCreaModifica);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, fechaCreaModifica);
        base.setParameters("@idUsuarioCreacion", System.Data.SqlDbType.Int, idUsuarioCrea);
        base.setParameters("@idUsuarioModificacion", System.Data.SqlDbType.Int, idUsuarioModificacion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }

    /// <summary>
    /// Ingresa los soportes de cada solicitud
    /// </summary>
    /// <param name="idSolicitudRef"></param>
    /// <param name="rutaArchivo"></param>
    /// <param name="archivoBinario"></param>
    /// <param name="nombreArchivo"></param>
    /// <param name="descripcionArchivo"></param>
    /// <param name="fechaCreacion"></param>
    /// <param name="fechaModificacion"></param>
    /// <param name="idUsuarioCreacion"></param>
    /// <param name="idUsuarioModificacion"></param>
    /// <param name="tipoArchivo"></param>
    /// <returns></returns>
    public bool fntInsertarSoporteSol_bol(Int64 idSolicitudRef, string rutaArchivo, byte[] archivoBinario, string nombreArchivo, string descripcionArchivo, DateTime fechaCreacion, DateTime fechaModificacion, int idUsuarioCreacion, int idUsuarioModificacion, string tipoArchivo)
    {
        string strCampos = " idSolicitudRef, rutaArchivo, archivoBinario, nombreArchivo, descripcionArchivo, fechaCreacion, fechaModificacion, idUsuarioCreacion, idUsuarioModificacion, tipoArchivo ";
        string strTabla = " WFSol_SoporteSol";
        string strValores = " @idSolicitudRef, @rutaArchivo, @archivoBinario, @nombreArchivo, @descripcionArchivo, @fechaCreacion, @fechaModificacion, @idUsuarioCreacion, @idUsuarioModificacion, @tipoArchivo ";
        string strCondicion = " idSolicitudRef = @idSolicitudRef AND rutaArchivo = @rutaArchivo AND archivoBinario = @archivoBinario AND nombreArchivo = @nombreArchivo AND descripcionArchivo = @descripcionArchivo AND fechaCreacion = @fechaCreacion AND fechaModificacion = @fechaModificacion AND idUsuarioCreacion = @idUsuarioCreacion AND idUsuarioModificacion = @idUsuarioModificacion AND tipoArchivo = @tipoArchivo  ";
        base.clearParameters();
        base.setParameters("@idSolicitudRef", System.Data.SqlDbType.BigInt, idSolicitudRef);
        base.setParameters("@rutaArchivo", System.Data.SqlDbType.NVarChar, rutaArchivo);
        base.setParameters("@archivoBinario", System.Data.SqlDbType.VarBinary, archivoBinario);
        base.setParameters("@nombreArchivo", System.Data.SqlDbType.NVarChar, nombreArchivo);
        base.setParameters("@descripcionArchivo", System.Data.SqlDbType.NVarChar, descripcionArchivo);
        base.setParameters("@fechaCreacion", System.Data.SqlDbType.DateTime, fechaCreacion);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, fechaModificacion);
        base.setParameters("@idUsuarioCreacion", System.Data.SqlDbType.Int, idUsuarioCreacion);
        base.setParameters("@idUsuarioModificacion", System.Data.SqlDbType.Int, idUsuarioModificacion);
        base.setParameters("@tipoArchivo", System.Data.SqlDbType.NVarChar, tipoArchivo);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }

    /// <summary>
    /// ☻☻ metodo para guardar el soporte de legalizacion
    /// </summary>
    /// <param name="idSolicitudRef"></param>
    /// <param name="rutaArchivo"></param>
    /// <param name="archivoBinario"></param>
    /// <param name="nombreArchivo"></param>
    /// <param name="descripcionArchivo"></param>
    /// <param name="fechaCreacion"></param>
    /// <param name="fechaModificacion"></param>
    /// <param name="idUsuarioCreacion"></param>
    /// <param name="idUsuarioModificacion"></param>
    /// <param name="tipoArchivo"></param>
    /// <returns></returns>
    public bool fntInsertarSoporteSolLegalizacion_bol(Int64 idSolicitudRef, string rutaArchivo, byte[] archivoBinario, string nombreArchivo, string descripcionArchivo, DateTime fechaCreacion, DateTime fechaModificacion, int idUsuarioCreacion, int idUsuarioModificacion, string tipoArchivo)
    {
        string strCampos = " idSolicitudRef, rutaArchivo, archivoBinario, nombreArchivo, descripcionArchivo, fechaCreacion, fechaModificacion, idUsuarioCreacion, idUsuarioModificacion, tipoArchivo ";
        string strTabla = " WFSol_SoporteSolLegalizacion";
        string strValores = " @idSolicitudRef, @rutaArchivo, @archivoBinario, @nombreArchivo, @descripcionArchivo, @fechaCreacion, @fechaModificacion, @idUsuarioCreacion, @idUsuarioModificacion, @tipoArchivo ";
        string strCondicion = " idSolicitudRef = @idSolicitudRef AND rutaArchivo = @rutaArchivo AND archivoBinario = @archivoBinario AND nombreArchivo = @nombreArchivo AND descripcionArchivo = @descripcionArchivo AND fechaCreacion = @fechaCreacion AND fechaModificacion = @fechaModificacion AND idUsuarioCreacion = @idUsuarioCreacion AND idUsuarioModificacion = @idUsuarioModificacion AND tipoArchivo = @tipoArchivo  ";
        base.clearParameters();
        base.setParameters("@idSolicitudRef", System.Data.SqlDbType.BigInt, idSolicitudRef);
        base.setParameters("@rutaArchivo", System.Data.SqlDbType.NVarChar, rutaArchivo);
        base.setParameters("@archivoBinario", System.Data.SqlDbType.VarBinary, archivoBinario);
        base.setParameters("@nombreArchivo", System.Data.SqlDbType.NVarChar, nombreArchivo);
        base.setParameters("@descripcionArchivo", System.Data.SqlDbType.NVarChar, descripcionArchivo);
        base.setParameters("@fechaCreacion", System.Data.SqlDbType.DateTime, fechaCreacion);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, fechaModificacion);
        base.setParameters("@idUsuarioCreacion", System.Data.SqlDbType.Int, idUsuarioCreacion);
        base.setParameters("@idUsuarioModificacion", System.Data.SqlDbType.Int, idUsuarioModificacion);
        base.setParameters("@tipoArchivo", System.Data.SqlDbType.NVarChar, tipoArchivo);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }








    /// <summary>
    /// ☻☻ metodo  para  actualizar el soporte de solicitud 
    /// </summary>
    /// <param name="idSoporte"></param>
    /// <param name="idSolicitudRef"></param>
    /// <param name="rutaArchivo"></param>
    /// <param name="archivoBinario"></param>
    /// <param name="nombreArchivo"></param>
    /// <param name="descripcionArchivo"></param>
    /// <param name="fechaCreacion"></param>
    /// <param name="fechaModificacion"></param>
    /// <param name="idUsuarioCreacion"></param>
    /// <param name="idUsuarioModificacion"></param>
    /// <param name="tipoArchivo"></param>
    /// <returns></returns>
    public bool fntActualizarSoporteSol_bol(string idSoporte, Int64 idSolicitudRef, string rutaArchivo, byte[] archivoBinario, string nombreArchivo, string descripcionArchivo, DateTime fechaCreacion, DateTime fechaModificacion, int idUsuarioCreacion, int idUsuarioModificacion, string tipoArchivo)
    {
        string strTabla = " WFSol_SoporteSol ";
        string strCamposValores = " idSolicitudRef = @idSolicitudRef , rutaArchivo = @rutaArchivo , archivoBinario = @archivoBinario , nombreArchivo = @nombreArchivo , descripcionArchivo = @descripcionArchivo , fechaCreacion = @fechaCreacion , fechaModificacion = @fechaModificacion ,idUsuarioCreacion =@idUsuarioCreacion ,idUsuarioModificacion = @idUsuarioModificacion ,tipoArchivo = @tipoArchivo";
        string strCondicion = " idSoporteSol = @idSoporteSol ";
        base.clearParameters();
        base.setParameters("@idSoporteSol", System.Data.SqlDbType.BigInt, idSoporte);
        base.setParameters("@idSolicitudRef", System.Data.SqlDbType.BigInt, idSolicitudRef);
        base.setParameters("@rutaArchivo", System.Data.SqlDbType.NVarChar, rutaArchivo);
        base.setParameters("@archivoBinario", System.Data.SqlDbType.VarBinary, archivoBinario);
        base.setParameters("@nombreArchivo", System.Data.SqlDbType.NVarChar, nombreArchivo);
        base.setParameters("@descripcionArchivo", System.Data.SqlDbType.NVarChar, descripcionArchivo);
        base.setParameters("@fechaCreacion", System.Data.SqlDbType.DateTime, fechaCreacion);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, fechaModificacion);
        base.setParameters("@idUsuarioCreacion", System.Data.SqlDbType.Int, idUsuarioCreacion);
        base.setParameters("@idUsuarioModificacion", System.Data.SqlDbType.Int, idUsuarioModificacion);
        base.setParameters("@tipoArchivo", System.Data.SqlDbType.NVarChar, tipoArchivo);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }


    public bool fntActualizarSoporteSolLegalizacion_bol(string idSoporte, Int64 idSolicitudRef, string rutaArchivo, byte[] archivoBinario, string nombreArchivo, string descripcionArchivo, DateTime fechaCreacion, DateTime fechaModificacion, int idUsuarioCreacion, int idUsuarioModificacion, string tipoArchivo)
    {
        string strTabla = " WFSol_SoporteSolLegalizacion ";
        string strCamposValores = " idSolicitudRef = @idSolicitudRef , rutaArchivo = @rutaArchivo , archivoBinario = @archivoBinario , nombreArchivo = @nombreArchivo , descripcionArchivo = @descripcionArchivo , fechaCreacion = @fechaCreacion , fechaModificacion = @fechaModificacion ,idUsuarioCreacion =@idUsuarioCreacion ,idUsuarioModificacion = @idUsuarioModificacion ,tipoArchivo = @tipoArchivo";
        string strCondicion = " idSoporteSol = @idSoporteSol ";
        base.clearParameters();
        base.setParameters("@idSoporteSol", System.Data.SqlDbType.BigInt, idSoporte);
        base.setParameters("@idSolicitudRef", System.Data.SqlDbType.BigInt, idSolicitudRef);
        base.setParameters("@rutaArchivo", System.Data.SqlDbType.NVarChar, rutaArchivo);
        base.setParameters("@archivoBinario", System.Data.SqlDbType.VarBinary, archivoBinario);
        base.setParameters("@nombreArchivo", System.Data.SqlDbType.NVarChar, nombreArchivo);
        base.setParameters("@descripcionArchivo", System.Data.SqlDbType.NVarChar, descripcionArchivo);
        base.setParameters("@fechaCreacion", System.Data.SqlDbType.DateTime, fechaCreacion);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, fechaModificacion);
        base.setParameters("@idUsuarioCreacion", System.Data.SqlDbType.Int, idUsuarioCreacion);
        base.setParameters("@idUsuarioModificacion", System.Data.SqlDbType.Int, idUsuarioModificacion);
        base.setParameters("@tipoArchivo", System.Data.SqlDbType.NVarChar, tipoArchivo);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }





    //☻☻ metodo para actualizar el  soporte sin cambiar el archivo 
    public bool fntActualizarSoporteSolSinArchivo_bol(string idSoporte, Int64 idSolicitudRef, string nombreArchivo, string descripcionArchivo, DateTime fechaCreacion, DateTime fechaModificacion, int idUsuarioCreacion, int idUsuarioModificacion)
    {
        string strTabla = " WFSol_SoporteSol ";
        string strCamposValores = " idSolicitudRef = @idSolicitudRef , nombreArchivo = @nombreArchivo , descripcionArchivo = @descripcionArchivo , fechaCreacion = @fechaCreacion , fechaModificacion = @fechaModificacion ,idUsuarioCreacion =@idUsuarioCreacion ,idUsuarioModificacion = @idUsuarioModificacion";
        string strCondicion = " idSoporteSol = @idSoporteSol ";
        base.clearParameters();
        base.setParameters("@idSoporteSol", System.Data.SqlDbType.BigInt, idSoporte);
        base.setParameters("@idSolicitudRef", System.Data.SqlDbType.BigInt, idSolicitudRef);
        base.setParameters("@nombreArchivo", System.Data.SqlDbType.NVarChar, nombreArchivo);
        base.setParameters("@descripcionArchivo", System.Data.SqlDbType.NVarChar, descripcionArchivo);
        base.setParameters("@fechaCreacion", System.Data.SqlDbType.DateTime, fechaCreacion);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, fechaModificacion);
        base.setParameters("@idUsuarioCreacion", System.Data.SqlDbType.Int, idUsuarioCreacion);
        base.setParameters("@idUsuarioModificacion", System.Data.SqlDbType.Int, idUsuarioModificacion);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }

    //☻☻ metodo para actualizar el  soporte sin cambiar el archivo 
    public bool fntActualizarSoporteSolSinArchivoLegalizacion_bol(string idSoporte, Int64 idSolicitudRef, string nombreArchivo, string descripcionArchivo, DateTime fechaCreacion, DateTime fechaModificacion, int idUsuarioCreacion, int idUsuarioModificacion)
    {
        string strTabla = " WFSol_SoporteSolLegalizacion ";
        string strCamposValores = " idSolicitudRef = @idSolicitudRef , nombreArchivo = @nombreArchivo , descripcionArchivo = @descripcionArchivo , fechaCreacion = @fechaCreacion , fechaModificacion = @fechaModificacion ,idUsuarioCreacion =@idUsuarioCreacion ,idUsuarioModificacion = @idUsuarioModificacion";
        string strCondicion = " idSoporteSol = @idSoporteSol ";
        base.clearParameters();
        base.setParameters("@idSoporteSol", System.Data.SqlDbType.BigInt, idSoporte);
        base.setParameters("@idSolicitudRef", System.Data.SqlDbType.BigInt, idSolicitudRef);
        base.setParameters("@nombreArchivo", System.Data.SqlDbType.NVarChar, nombreArchivo);
        base.setParameters("@descripcionArchivo", System.Data.SqlDbType.NVarChar, descripcionArchivo);
        base.setParameters("@fechaCreacion", System.Data.SqlDbType.DateTime, fechaCreacion);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, fechaModificacion);
        base.setParameters("@idUsuarioCreacion", System.Data.SqlDbType.Int, idUsuarioCreacion);
        base.setParameters("@idUsuarioModificacion", System.Data.SqlDbType.Int, idUsuarioModificacion);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }








    public bool fntModificarExistingPlan(Int64 idPlan, Int64 idSolicitud, int idEstrategia, int idActividad, int idIndicador, DateTime fechaModificacion, int usuarioModificacion)
    {
        string strTabla = " WFSol_PlanAccion ";
        string strCamposValores = " idEstrategia = @idEstrategia, idActividad = @idActividad, idIndicador = @idIndicador, fechaModificacion = @fechaModificacion, idUsuarioModificacion = @idUsuarioModificacion ";
        string strCondicion = " idPlanAccion = @idPlanAccion AND idSolicitudRef = @idSolicitudRef ";
        base.clearParameters();
        base.setParameters("@idEstrategia", System.Data.SqlDbType.Int, idEstrategia);
        base.setParameters("@idActividad", System.Data.SqlDbType.Int, idActividad);
        base.setParameters("@idIndicador", System.Data.SqlDbType.Int, idIndicador);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, fechaModificacion);
        base.setParameters("@idUsuarioModificacion", System.Data.SqlDbType.Int, usuarioModificacion);
        base.setParameters("@idPlanAccion", System.Data.SqlDbType.BigInt, idPlan);
        base.setParameters("@idSolicitudRef", System.Data.SqlDbType.BigInt, idSolicitud);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }



    public bool fntModificarExistingSite(Int64 idSite, Int64 idSolicitud, string idDepartamento, string idMunicipio, string ubicacion, DateTime fechaModificacion, int usuarioModificacion)
    {
        string strTabla = " WFSol_SitiosSol ";
        string strCamposValores = " idDepartamento = @idDepartamento, idMunicipio = @idMunicipio, ubicacionEspecifica = @ubicacionEspecifica, fechaModificacion = @fechaModificacion, idUsuarioModificacion = @idUsuarioModificacion ";
        string strCondicion = " idSitiosSol = @idSitiosSol AND idSolicitudRef = @idSolicitudRef ";
        base.clearParameters();
        base.setParameters("@idDepartamento", System.Data.SqlDbType.NVarChar, idDepartamento);
        base.setParameters("@idMunicipio", System.Data.SqlDbType.NVarChar, idMunicipio);
        base.setParameters("@ubicacionEspecifica", System.Data.SqlDbType.NVarChar, ubicacion);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, fechaModificacion);
        base.setParameters("@idUsuarioModificacion", System.Data.SqlDbType.Int, usuarioModificacion);
        base.setParameters("@idSitiosSol", System.Data.SqlDbType.BigInt, idSite);
        base.setParameters("@idSolicitudRef", System.Data.SqlDbType.BigInt, idSolicitud);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }

    public bool fntIngresarLegalizacion_bol(Int64 idSolicitud, DateTime fechaLegalizacion, decimal cumplimiento, int idEstadoActual, DateTime fechaCreaModifica, string idUnico)
    {
        string strCampos = " idSolicitudRef, fechaLegalizacion, cumplimientoPorcentaje, idEstadoActual, fechaModificacion, idUnico ";
        string strTabla = " WFSol_Legalizacion ";
        string strValores = " @idSolicitudRef, @fechaLegalizacion, @cumplimientoPorcentaje, @idEstadoActual, @fechaModificacion,  @idUnico ";
        string strCondicion = " idSolicitudRef = @idSolicitudRef AND fechaLegalizacion = @fechaLegalizacion AND cumplimientoPorcentaje = @cumplimientoPorcentaje AND idEstadoActual = @idEstadoActual AND fechaModificacion = @fechaModificacion AND idUnico = @idUnico  ";
        base.clearParameters();
        base.setParameters("@idSolicitudRef", System.Data.SqlDbType.BigInt, idSolicitud);
        base.setParameters("@fechaLegalizacion", System.Data.SqlDbType.DateTime, fechaLegalizacion);
        base.setParameters("@cumplimientoPorcentaje", System.Data.SqlDbType.Decimal, cumplimiento);
        base.setParameters("@idEstadoActual", System.Data.SqlDbType.Int, idEstadoActual);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, fechaCreaModifica);
        base.setParameters("@idUnico", System.Data.SqlDbType.NVarChar, idUnico);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }


    public bool fntIngresarCompromisos_bol(Int64 idLegalizacionRef, string descripcion, DateTime fechaCompromiso, DateTime fechaCreacion, string responsable)
    {
        string strCampos = " descripcion, fechaCompromiso, fechaCreacion, idLegalizacionRef ,responsable ";
        string strTabla = " WFSol_Compromisos ";
        string strValores = " @descripcion, @fechaCompromiso, @fechaCreacion, @idLegalizacionRef , @responsable ";
        string strCondicion = " descripcion = @descripcion AND fechaCompromiso = @fechaCompromiso AND fechaCreacion = @fechaCreacion AND idLegalizacionRef = @idLegalizacionRef  AND responsable = @responsable";
        base.clearParameters();
        base.setParameters("@descripcion", System.Data.SqlDbType.NVarChar, descripcion);
        base.setParameters("@fechaCompromiso", System.Data.SqlDbType.DateTime, fechaCompromiso);
        base.setParameters("@fechaCreacion", System.Data.SqlDbType.DateTime, fechaCreacion);
        base.setParameters("@idLegalizacionRef", System.Data.SqlDbType.BigInt, idLegalizacionRef);
        base.setParameters("@responsable", System.Data.SqlDbType.NVarChar, responsable);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }


    /// <summary>
    /// ☻☻ metodo para actualizar los Compromisos 
    /// </summary>
    /// <param name="idLegalizacionRef"></param>
    /// <param name="descripcion"></param>
    /// <param name="fechaCompromiso"></param>
    /// <param name="responsable"></param>
    /// <returns></returns>
    public bool fntActualizarCompromisos_bol(Int64 idCompromiso, string descripcion, DateTime fechaCompromiso, string responsable)
    {
        string strTabla = " WFSol_Compromisos ";
        string strValores = "descripcion = @descripcion, fechaCompromiso = @fechaCompromiso , responsable =  @responsable ";
        string strCondicion = " idCompromiso = @idCompromiso";
        base.clearParameters();
        base.setParameters("@descripcion", System.Data.SqlDbType.NVarChar, descripcion);
        base.setParameters("@fechaCompromiso", System.Data.SqlDbType.DateTime, fechaCompromiso);
        base.setParameters("@responsable", System.Data.SqlDbType.NVarChar, responsable);
        base.setParameters("@idCompromiso", System.Data.SqlDbType.BigInt, idCompromiso);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }


    /// <summary>
    /// ☻☻ metodo para actualizar el beneficio 
    /// </summary>
    /// <param name="idBeneficio"></param>
    /// <param name="cantBeneficiada"></param>
    /// <param name="comentario"></param>
    /// <param name="idBeneficioLega"></param>
    /// <returns></returns>
    public bool fntActualizarBeneficiario_bol(int idBeneficio, int cantBeneficiada, string comentario, Int64 idBeneficioLega)
    {
        string strTabla = " WFSol_BeneficioLegalizacion ";
        string strValores = "idBeneficio = @idBeneficio, cantBeneficiada = @cantBeneficiada , comentario =  @comentario ";
        string strCondicion = "idBeneficioLega = @idBeneficioLega";
        base.clearParameters();
        base.setParameters("@idBeneficio", System.Data.SqlDbType.Int, idBeneficio);
        base.setParameters("@cantBeneficiada", System.Data.SqlDbType.Int, cantBeneficiada);
        base.setParameters("@comentario", System.Data.SqlDbType.NVarChar, comentario);
        base.setParameters("@idBeneficioLega", System.Data.SqlDbType.BigInt, idBeneficioLega);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }

    /// <summary>
    /// ☻☻ metodo para guardar los beneficiarios 
    /// </summary>
    /// <param name="idLegalizacionRef"></param>
    /// <param name="idBeneficio"></param>
    /// <param name="cantBeneficiada"></param>
    /// <param name="comentario"></param>
    /// <param name="fechaCreacion"></param>
    /// <returns></returns>
    public bool fntIngresarBeneficiarios_bol(Int64 idLegalizacionRef, int idBeneficio, int cantBeneficiada, string comentario, DateTime fechaCreacion)
    {
        string strCampos = " idLegalizacion, idBeneficio, cantBeneficiada, comentario, fechaCreacion ";
        string strTabla = " WFSol_BeneficioLegalizacion ";
        string strValores = " @idLegalizacion, @idBeneficio, @cantBeneficiada, @comentario, @fechaCreacion ";
        string strCondicion = " idLegalizacion = @idLegalizacion AND idBeneficio = @idBeneficio AND cantBeneficiada = @cantBeneficiada AND comentario = @comentario AND fechaCreacion = @fechaCreacion  ";
        base.clearParameters();
        base.setParameters("@idLegalizacion", System.Data.SqlDbType.BigInt, idLegalizacionRef);
        base.setParameters("@idBeneficio", System.Data.SqlDbType.Int, idBeneficio);
        base.setParameters("@cantBeneficiada", System.Data.SqlDbType.Int, cantBeneficiada);
        base.setParameters("@comentario", System.Data.SqlDbType.NVarChar, comentario);
        base.setParameters("@fechaCreacion", System.Data.SqlDbType.DateTime, fechaCreacion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }


    public bool fntActualizarEstado_bol(long idActualizar, string nombreCampoId, int idEstadoActualizar, string nombreTabla, string nombreCampo)
    {
        string strTabla = " " + nombreTabla + " ";
        string strCamposValores = " " + nombreCampo + " = @" + nombreCampo + " ";
        string strCondicion = " " + nombreCampoId + " = @" + nombreCampoId + " ";
        base.clearParameters();
        base.setParameters("@" + nombreCampo + "", System.Data.SqlDbType.Int, idEstadoActualizar);
        base.setParameters("@" + nombreCampoId + "", System.Data.SqlDbType.BigInt, idActualizar);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }

    public Boolean fntEliminarElementoBigInt(string nombreTabla, string nombreCampo, Int64 valorCampo)
    {
        string lstrTabla = " " + nombreTabla + " ";
        string lstrCondicion = " " + nombreCampo + " = @campo ";

        base.clearParameters();
        setParameters("@campo", SqlDbType.BigInt, valorCampo);

        if (Eliminar(lstrTabla, lstrCondicion, false))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Boolean fntEliminarElementoInt(string nombreTabla, string nombreCampo, int valorCampo)
    {
        string lstrTabla = " " + nombreTabla + " ";
        string lstrCondicion = " " + nombreCampo + " = @campo ";

        base.clearParameters();
        base.setParameters("@campo", SqlDbType.Int, valorCampo);

        if (Eliminar(lstrTabla, lstrCondicion, false))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Boolean fntEliminarUsuariosCoordinacion(string idCoordinacion, bool coordinador)
    {
        string lstrTabla = "  WFSol_UsuarioCoordinacion ";
        string lstrCondicion = " idCoordinacionRef = @idCoordinacionRef AND esCoordinador = @valorEsCoordinador ";
        base.clearParameters();
        base.setParameters("@idCoordinacionRef", SqlDbType.NVarChar, idCoordinacion);
        base.setParameters("@valorEsCoordinador", SqlDbType.Bit, coordinador);
        if (Eliminar(lstrTabla, lstrCondicion, false))
        {
            return true;
        }
        else
        {
            return false;
        }
    }



    //public bool fntUsuariosRegularCoordinacion_bol(string dataset, string idCoordinacion)
    //{
    //    string strCampos = " UC.idUsuarioRef, U.segusu_login ";
    //    string strTabla = " WFSol_UsuarioCoordinacion UC, Seg_Usuario U ";
    //    string strCondicion = " U.segusu_Id = UP.idUsuarioRef AND UC.idCoordinacionRef = @idCoordinacion ";
    //    string strOrden = " U.segusu_login ";
    //    base.clearParameters();
    //    base.setParameters("@idCoordinacion", System.Data.SqlDbType.NVarChar, idCoordinacion);
    //    return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    //}

    public bool fntUsuariosCoordinacion_bol(string dataset, string idCoordinacion, bool esCoordinador)
    {
        string strCampos = " UC.idUsuarioRef, U.segusu_login ";
        string strTabla = " WFSol_UsuarioCoordinacion UC, Seg_Usuario U ";
        string strCondicion = " U.segusu_Id = UC.idUsuarioRef AND UC.idCoordinacionRef = @idCoordinacion AND UC.esCoordinador = @valorBool ";
        string strOrden = " U.segusu_login ";
        base.clearParameters();
        base.setParameters("@idCoordinacion", System.Data.SqlDbType.NVarChar, idCoordinacion);
        base.setParameters("@valorBool", System.Data.SqlDbType.Bit, esCoordinador);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }


    public bool fntConsultarUsuarioEnCoordinacion_bol(string dataset, bool esCoordinador, int idUsuario)
    {
        string strCampos = " UC.idUsuarioRef, U.segusu_login ";
        string strTabla = " WFSol_UsuarioCoordinacion UC, Seg_Usuario U ";
        string strCondicion = " U.segusu_Id = UC.idUsuarioRef AND UC.idUsuarioRef = @idUsuario AND UC.esCoordinador = @valorBool ";
        string strOrden = " U.segusu_login ";
        base.clearParameters();
        base.setParameters("@idUsuario", System.Data.SqlDbType.Int, idUsuario);
        base.setParameters("@valorBool", System.Data.SqlDbType.Bit, esCoordinador);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }

    public bool fntConsultarCoordinacion_bol(string dataset, string modo, string idCoordinacion)
    {
        string strCampos = " C.nombre,  C.descripcion, C.fechaCreacion, C.fechaModificacion, C.usuarioCreacion, C.usuarioModificacion, C.idUnicoCoordinacion ";
        string strTabla = " WFSol_Coordinacion C ";
        string strCondicion = string.Empty;
        string strOrden = " C.nombre ";
        if (modo == "id")
        {
            strCondicion = " 1 = 1 ";
            base.clearParameters();
            base.setParameters("@idCoordinacion", System.Data.SqlDbType.NVarChar, idCoordinacion);
        }
        else
        {
            strCondicion = " 1 = 1 ";
        }
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }


    public bool fntInsertarUsuariosCoordinacion_bol(int idUsuario, string coordinacion, bool esCoordinador)
    {
        string strCampos = " idUsuarioRef , idCoordinacionRef, estatus, esCoordinador ";
        string strTabla = " WFSol_UsuarioCoordinacion ";
        string strValores = " @idUsuarioRef , @idCoordinacionRef , @estatus , @esCoordinador ";
        string strCondicion = " idUsuarioRef = @idUsuarioRef AND idCoordinacionRef = @idCoordinacionRef AND estatus = @estatus AND  esCoordinador = @esCoordinador ";
        base.clearParameters();
        base.setParameters("@idUsuarioRef", System.Data.SqlDbType.Int, idUsuario);
        base.setParameters("@idCoordinacionRef", System.Data.SqlDbType.NVarChar, coordinacion);
        base.setParameters("@estatus", System.Data.SqlDbType.NVarChar, "ACT");
        base.setParameters("@esCoordinador", System.Data.SqlDbType.Bit, esCoordinador);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }



    public bool fntInsertarCoordinacion_bol(string nombre, string descripcion, int idUsuario, string idCoordinacion)
    {
        string strCampos = " nombre , descripcion, fechaCreacion, fechaModificacion, usuarioCreacion, usuarioModificacion, idUnicoCoordinacion ";
        string strTabla = " WFSol_Coordinacion ";
        string strValores = " @nombre , @descripcion , @fechaCreacion , @fechaModificacion, @usuarioCreacion, @usuarioModificacion, @idUnicoCoordinacion ";
        string strCondicion = " nombre = @nombre AND descripcion = @descripcion AND fechaCreacion = @fechaCreacion AND  fechaModificacion = @fechaModificacion AND usuarioCreacion = @usuarioCreacion AND usuarioModificacion = @usuarioModificacion AND idUnicoCoordinacion = @idUnicoCoordinacion ";
        base.clearParameters();
        base.setParameters("@nombre", System.Data.SqlDbType.NVarChar, nombre);
        base.setParameters("@descripcion", System.Data.SqlDbType.NVarChar, descripcion);
        base.setParameters("@fechaCreacion", System.Data.SqlDbType.DateTime, DateTime.Now);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, DateTime.Now);
        base.setParameters("@usuarioCreacion", System.Data.SqlDbType.Int, idUsuario);
        base.setParameters("@usuarioModificacion", System.Data.SqlDbType.Int, idUsuario);
        base.setParameters("@idUnicoCoordinacion", System.Data.SqlDbType.NVarChar, idCoordinacion);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }


    public bool fntActualizarCoordinacion_bol(string nombre, string descripcion, int idUsuario, string idCoordinacion)
    {
        string strTabla = " WFSol_Coordinacion ";
        string strCamposValores = " nombre = @nombre, descripcion = @descripcion, fechaModificacion = @fechaModificacion, usuarioModificacion = @usuarioModificacion ";
        string strCondicion = " idUnicoCoordinacion = @idUnicoCoordinacion ";
        base.clearParameters();
        base.setParameters("@nombre", System.Data.SqlDbType.NVarChar, nombre);
        base.setParameters("@descripcion", System.Data.SqlDbType.NVarChar, descripcion);
        base.setParameters("@fechaModificacion", System.Data.SqlDbType.DateTime, DateTime.Now);
        base.setParameters("@usuarioModificacion", System.Data.SqlDbType.Int, idUsuario);
        base.setParameters("@idUnicoCoordinacion", System.Data.SqlDbType.NVarChar, idCoordinacion);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }


    public bool fntEliminarAdministrador_bol()
    {
        string strTabla = " WFSol_UsuarioAdministrador ";
        string strCondicion = " 1 = 1 ";
        base.clearParameters();
        return base.Eliminar(strTabla, strCondicion, false);
    }

    public bool fntInsertarUsuarioAdmin_bol(int idUsuario, string id)
    {
        string strCampos = "  idUsuario , estatus  ";
        string strTabla = " WFSol_UsuarioAdministrador ";
        string strValores = " @idUsuario , @estatus ";
        string strCondicion = " idUsuario = @idUsuario AND estatus = @estatus  ";
        base.clearParameters();
        base.setParameters("@idUsuario", System.Data.SqlDbType.Int, idUsuario);
        base.setParameters("@estatus", System.Data.SqlDbType.NVarChar, "ACT");
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }


    public bool fntUsuariosCoordinacionTodos_bol(string dataset)
    {
        string strCampos = " UC.idUsuarioRef, U.segusu_login ";
        string strTabla = " WFSol_UsuarioCoordinacion UC, Seg_Usuario U ";
        string strCondicion = " UC.idUsuarioRef = U.segusu_Id ";
        string strOrden = " U.segusu_login ";
        base.clearParameters();
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }


    public Boolean fntConsultarCoordinacionXUsuario(string dataset, int idUsuario)
    {
        string lstrCampos = " uc.idUsuarioCoor, uc.idUsuarioRef, uc.idCoordinacionRef, uc.estatus, uc.esCoordinador ";
        string lstrTabla = " WFSol_UsuarioCoordinacion as uc ";
        string lstrCondicion = " uc.idUsuarioRef = @idUsuarioRef  ";
        string lstrOrden = " uc.idUsuarioCoor DESC ";
        base.clearParameters();
        base.setParameters("@idUsuarioRef", SqlDbType.Int, idUsuario);
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }


    public Boolean fntConsultarSiCoordinador(string dataset, int idUsuario, string idCoordinacion)
    {
        string lstrCampos = " uc.idUsuarioCoor, uc.idUsuarioRef, uc.idCoordinacionRef, uc.estatus, uc.esCoordinador ";
        string lstrTabla = " WFSol_UsuarioCoordinacion as uc ";
        string lstrCondicion = " uc.idUsuarioRef = @idUsuarioRef AND  idCoordinacionRef = @idCoordinacionRef AND esCoordinador = @esCoordinador ";
        string lstrOrden = " uc.idUsuarioCoor DESC ";
        base.clearParameters();
        base.setParameters("@idUsuarioRef", SqlDbType.Int, idUsuario);
        base.setParameters("@idCoordinacionRef", SqlDbType.NVarChar, idCoordinacion);
        base.setParameters("@esCoordinador", SqlDbType.Bit, true);
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntConsultarSiAdministrador(string dataset, int idUsuario)
    {
        string lstrCampos = " idUsuarioAdmin, idUsuario, estatus ";
        string lstrTabla = " WFSol_UsuarioAdministrador ";
        string lstrCondicion = " idUsuario = @idUsuario ";
        string lstrOrden = " idUsuarioAdmin DESC ";
        base.clearParameters();
        base.setParameters("@idUsuario", SqlDbType.Int, idUsuario);
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// buscar todos los administradores 
    /// </summary>
    /// <param name="dataset"></param>
    /// <param name="idUsuario"></param>
    /// <returns></returns>
    public Boolean fntConsultarAdministradores(string dataset)
    {
        string lstrCampos = " UA.idUsuario, U.segusu_login ";
        string lstrTabla = " WFSol_UsuarioAdministrador UA, Seg_Usuario U  ";
        string lstrCondicion = " UA.idUsuario = segusu_Id AND UA.estatus = 'ACT' ";
        string lstrOrden = " segusu_login DESC ";
        base.clearParameters();
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    public Boolean fntConsultarLegalizacion(string dataset, string modo, Int64 idSolicitud, Int64 idLegalizacion, string idUnico)
    {
        string lstrCampos = " idLegalizacion, idSolicitudRef, fechaLegalizacion, cumplimientoPorcentaje, idEstadoActual, fechaModificacion, idUnico ";
        string lstrTabla = " WFSol_Legalizacion ";
        string lstrCondicion = string.Empty;
        string lstrOrden = " idLegalizacion DESC ";
        base.clearParameters();
        if (modo == "solicitud")
        {
            lstrCondicion = " idSolicitudRef = @idSolicitudRef ";
            base.setParameters("@idSolicitudRef", SqlDbType.BigInt, idSolicitud);
        }
        if (modo == "legalizacion")
        {
            lstrCondicion = " idLegalizacion = @idLegalizacion ";
            base.setParameters("@idLegalizacion", SqlDbType.BigInt, idLegalizacion);
        }
        if (modo == "unico")
        {
            lstrCondicion = " idUnico = @idUnico ";
            base.setParameters("@idUnico", SqlDbType.NVarChar, idUnico);
        }
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }



    public Boolean fntConsultarPlanValidado(string dataset, string modo, long idPlan, long idLegalizacion)
    {
        string lstrCampos = " pe.*,  pa.* ";
        string lstrTabla = " WFSol_PlanEjecutado as pe, WFSol_PlanAccion as pa";
        string lstrCondicion = " pe.idLegalizacionRef = @idLegalizacionRef AND pe.idPlanAccionRef = pa.idPlanAccion ";
        string lstrOrden = " pe.fechaCreacion DESC ";
        base.clearParameters();
        base.setParameters("@idLegalizacionRef", System.Data.SqlDbType.BigInt, idLegalizacion);
        if (modo == "id")
        {
            lstrCondicion = lstrCondicion + " AND pa.idPlanAccion = @idPlan ";
            base.setParameters("@idPlan", System.Data.SqlDbType.BigInt, idPlan);
        }
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }



    public bool fntInsertarPlanValidado_bol(long idLegalizacionRef, long idPlanAccionRef, string planEjecutado, decimal ejecucionPorcentaje)
    {
        string strCampos = " idLegalizacionRef , idPlanAccionRef,  planEjecutado,  ejecucionPorcentaje, fechaCreacion ";
        string strTabla = " WFSol_PlanEjecutado ";
        string strValores = " @idLegalizacionRef , @idPlanAccionRef, @planEjecutado, @ejecucionPorcentaje, @fechaCreacion";
        string strCondicion = " idLegalizacionRef = @idLegalizacionRef AND idPlanAccionRef = @idPlanAccionRef AND  planEjecutado = @planEjecutado AND ejecucionPorcentaje = @ejecucionPorcentaje AND fechaCreacion = @fechaCreacion ";
        base.clearParameters();
        base.setParameters("@idLegalizacionRef", System.Data.SqlDbType.BigInt, idLegalizacionRef);
        base.setParameters("@idPlanAccionRef", System.Data.SqlDbType.BigInt, idPlanAccionRef);
        base.setParameters("@planEjecutado", System.Data.SqlDbType.NVarChar, planEjecutado);
        base.setParameters("@ejecucionPorcentaje", System.Data.SqlDbType.Decimal, ejecucionPorcentaje);
        base.setParameters("@fechaCreacion", System.Data.SqlDbType.DateTime, DateTime.Now);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }


    /// <summary>
    /// ☻☻ metodo para insertar en  la tabla permite modificar solicitud 
    /// </summary>
    /// <param name="idSolicitudRef"></param>
    /// <param name="idUsuarioAdmin"></param>
    /// <param name="idUsuarioEditar"></param>
    /// <param name="justificacion"></param>
    /// <returns></returns>
    public bool fntInsertarPermiteEditarSolicitud_bol(Int64 idSolicitudRef, Int32 idUsuarioAdmin, int idUsuarioEditar, string justificacion)
    {
        string strCampos = " idSolicitudRef , idUsuarioAdmin,  idUsuarioEditar,  justificacion , fechaHabilita";
        string strTabla = " WFSol_PermiteEditar ";
        string strValores = " @idSolicitudRef , @idUsuarioAdmin, @idUsuarioEditar, @justificacion, @fechaHabilita";
        string strCondicion = " idSolicitudRef = @idSolicitudRef AND idUsuarioAdmin = @idUsuarioAdmin AND  idUsuarioEditar = @idUsuarioEditar AND justificacion = @justificacion AND fechaHabilita = @fechaHabilita ";
        base.clearParameters();
        base.setParameters("@idSolicitudRef", System.Data.SqlDbType.BigInt, idSolicitudRef);
        base.setParameters("@idUsuarioAdmin", System.Data.SqlDbType.Int, idUsuarioAdmin);
        base.setParameters("@idUsuarioEditar", System.Data.SqlDbType.Int, idUsuarioEditar);
        base.setParameters("@justificacion", System.Data.SqlDbType.VarChar, justificacion);
        base.setParameters("@fechaHabilita", System.Data.SqlDbType.DateTime, DateTime.Now);
        base.iniciarTransaccion();
        if (base.Insertar(strTabla, strCampos, strValores, strCondicion, true, true))
        {
            base.commit();
            return true;
        }
        base.rollback();
        return false;
    }






    public bool fntActualizarPlanValidado_bol(long idLegalizacionRef, long idPlanAccionRef, string planEjecutado, decimal ejecucionPorcentaje)
    {
        string strTabla = " WFSol_PlanEjecutado ";
        string strCamposValores = " idLegalizacionRef = @idLegalizacionRef, idPlanAccionRef = @idPlanAccionRef, planEjecutado = @planEjecutado, ejecucionPorcentaje = @ejecucionPorcentaje, fechaCreacion = @fechaCreacion ";
        string strCondicion = " idLegalizacionRef = @idLegalizacionRef AND idPlanAccionRef = @idPlanAccionRef ";
        base.clearParameters();
        base.setParameters("@idLegalizacionRef", System.Data.SqlDbType.BigInt, idLegalizacionRef);
        base.setParameters("@idPlanAccionRef", System.Data.SqlDbType.BigInt, idPlanAccionRef);
        base.setParameters("@planEjecutado", System.Data.SqlDbType.NVarChar, planEjecutado);
        base.setParameters("@ejecucionPorcentaje", System.Data.SqlDbType.Decimal, ejecucionPorcentaje);
        base.setParameters("@fechaCreacion", System.Data.SqlDbType.DateTime, DateTime.Now);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }


    public bool fntActualizarCumplimientoLegalizacion_bol(long idLegalizacionRef, decimal ejecucionPorcentaje)
    {
        string strTabla = " WFSol_Legalizacion ";
        string strCamposValores = " cumplimientoPorcentaje = @cumplimientoPorcentaje ";
        string strCondicion = " idLegalizacion = @idLegalizacionRef ";
        base.clearParameters();
        base.setParameters("@idLegalizacionRef", System.Data.SqlDbType.BigInt, idLegalizacionRef);
        base.setParameters("@cumplimientoPorcentaje", System.Data.SqlDbType.Decimal, ejecucionPorcentaje);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }

    public bool fntActualizarObservacion_bol(long idSolicitud, string observacion)
    {
        string strTabla = " WFSol_SolicitudComision ";
        string strCamposValores = " observacion = @observacion ";
        string strCondicion = " idSolicitud = @idSolicitud ";
        base.clearParameters();
        base.setParameters("@idSolicitud", System.Data.SqlDbType.BigInt, idSolicitud);
        base.setParameters("@observacion", System.Data.SqlDbType.NVarChar, observacion);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }



    /// <summary>
    /// ☻☻ metodo para ingresar el numero de aprobacion 
    /// </summary>
    /// <param name="idSolicitud"></param>
    /// <param name="numeroAprobacion"></param>
    /// <returns></returns>
    public bool fntActualizaNumeroAprobacion_bol(long idSolicitud, int numeroAprobacion)
    {
        string strTabla = " WFSol_SolicitudComision ";
        string strCamposValores = " numeroAprobacion = @numeroAprobacion ";
        string strCondicion = " idSolicitud = @idSolicitud ";
        base.clearParameters();
        base.setParameters("@idSolicitud", System.Data.SqlDbType.BigInt, idSolicitud);
        base.setParameters("@numeroAprobacion", System.Data.SqlDbType.Int, numeroAprobacion);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }

    public bool fntConsultarSolicitudPorId_bol(string dataset, long idSolicitud)
    {

        string strCampos = " * ";
        string strTabla = " WFSol_SolicitudComision d ";
        string strCondicion = "  idSolicitud = @idSolicitud";
        string strOrden = " d.idSolicitud ";
        base.clearParameters();
        base.setParameters("@idSolicitud", System.Data.SqlDbType.BigInt, idSolicitud);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }



    /// <summary>
    /// ☻☻ actualizar estado coordinador 
    /// </summary>
    /// <param name="idSolicitud"></param>
    /// <param name="observacion"></param>
    /// <returns></returns>
    public bool fntActualizarObservacionCordinador_bol(long idSolicitud, string observacion, string campodeObservacion)
    {
        string strTabla = " WFSol_SolicitudComision ";
        string strCamposValores = " " + campodeObservacion + "= @observacion ";
        string strCondicion = " idSolicitud = @idSolicitud ";
        base.clearParameters();
        base.setParameters("@idSolicitud", System.Data.SqlDbType.BigInt, idSolicitud);
        base.setParameters("@observacion", System.Data.SqlDbType.NVarChar, observacion);
        base.setParameters("@campodeObservacion", System.Data.SqlDbType.NVarChar, campodeObservacion);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }


    public bool fntActualizarObservacionAdministrador_bol(long idSolicitud, string observacion, string campodeObservacion)
    {
        string strTabla = " WFSol_SolicitudComision ";
        string strCamposValores = " " + campodeObservacion + "= @observacion ";
        string strCondicion = " idSolicitud = @idSolicitud ";
        base.clearParameters();
        base.setParameters("@idSolicitud", System.Data.SqlDbType.BigInt, idSolicitud);
        base.setParameters("@observacion", System.Data.SqlDbType.NVarChar, observacion);
        base.setParameters("@campodeObservacion", System.Data.SqlDbType.NVarChar, campodeObservacion);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        //observacionAdministracion
        base.commit();
        return true;
    }





    /// <summary>
    /// ☻☻ metodo   para actualizar la solicitud  
    /// </summary>
    /// <param name="idSolicitud"></param>
    /// <param name="lineaIntervencion"></param>
    /// <param name="nombre"></param>
    /// <param name="objeto"></param>
    /// <param name="fechaInicio"></param>
    /// <param name="fechaFin"></param>
    /// <param name="tiquetesAereos"></param>
    /// <param name="rutaAerea"></param>
    /// <param name="porcionTerrestre"></param>
    /// <param name="rutaTerrestre"></param>
    /// <param name="viaticos"></param>
    /// <param name="valorDiario"></param>
    /// <param name="numeroDias"></param>
    /// <param name="totalViaticos"></param>
    /// <param name="idUsuarioCrea"></param>
    /// <param name="idUsuarioModificacion"></param>
    /// <param name="fechaCreaModifica"></param>
    /// <param name="idEstadoActual"></param>
    /// <param name="identificadorUnico"></param>
    /// <returns></returns>
    public bool fntActualizarSolicitud_bol(long idSolicitud, int lineaIntervencion, string nombre, string objeto, DateTime fechaInicio, DateTime fechaFin, bool tiquetesAereos, string rutaAerea, bool porcionTerrestre, string rutaTerrestre, bool viaticos, decimal valorDiario, decimal numeroDias, decimal totalViaticos, int idUsuarioCrea, int idUsuarioModificacion, DateTime fechaCreaModifica, int idEstadoActual, string identificadorUnico, string cargoFuncionario, string lugarViaje, string monedaViatico, decimal valorRutaPorTerrestre)
    {
        //string strCampos = " numeroAprobacion , idLineaIntervencion , nombreFuncionario , objetoComision , fechaInicio , numeroDias , fechaFin , tiquetesAereos , rutaAerea , porcionTerrestre , rutaPorTerrestre , requiereViaticos , valorDiarioViatico , totalViaticos , fechaCreacion , fechaModificacion , idUsuarioCreacion , idUsuarioModificacion, idEstadoActual , IdentificadorSolicitud ";
        string strTabla = " WFSol_SolicitudComision ";
        string strValores = " numeroAprobacion = @numeroAprobacionStr , idLineaIntervencion = @idLineaIntervencionStr , nombreFuncionario =  @nombreFuncionarioStr ,objetoComision =  @objetoComisionStr ,fechaInicio = @fechaInicioStr ,numeroDias =  @numeroDiasStr ,fechaFin =  @fechaFinStr ,tiquetesAereos = @tiquetesAereosStr ,rutaAerea = @rutaAereaStr ,porcionTerrestre =  @porcionTerrestreStr , rutaPorTerrestre = @rutaPorTerrestreStr ,requiereViaticos = @requiereViaticosStr ,valorDiarioViatico =  @valorDiarioViaticoStr ,totalViaticos = @totalViaticosStr ,fechaCreacion = @fechaCreacionStr , fechaModificacion = @fechaModificacionStr ,idUsuarioCreacion =  @idUsuarioCreacionStr , idUsuarioModificacion =  @idUsuarioModificacionStr  ,IdentificadorSolicitud =  @IdentificadorSolicitudStr, cargoFuncionario = @cargoFuncionario, lugarViaje = @lugarViaje,  monedaViatico = @monedaViatico,  idEstadoActual = @idEstadoActual, valorRutaPorTerrestre = @valorRutaPorTerrestre ";
        string strCondicion = "  idSolicitud = @idSolicitud ";
        base.clearParameters();


        base.setParameters("@idSolicitud", System.Data.SqlDbType.BigInt, idSolicitud);
        base.setParameters("@numeroAprobacionStr", System.Data.SqlDbType.Int, 0);
        base.setParameters("@idLineaIntervencionStr", System.Data.SqlDbType.Int, lineaIntervencion);
        base.setParameters("@nombreFuncionarioStr", System.Data.SqlDbType.NVarChar, nombre);
        base.setParameters("@objetoComisionStr", System.Data.SqlDbType.NVarChar, objeto);
        base.setParameters("@fechaInicioStr", System.Data.SqlDbType.DateTime, fechaInicio);
        base.setParameters("@numeroDiasStr", System.Data.SqlDbType.Decimal, numeroDias);
        base.setParameters("@fechaFinStr", System.Data.SqlDbType.DateTime, fechaFin);
        base.setParameters("@tiquetesAereosStr", System.Data.SqlDbType.Bit, tiquetesAereos);
        base.setParameters("@rutaAereaStr", System.Data.SqlDbType.NVarChar, rutaAerea);
        base.setParameters("@porcionTerrestreStr", System.Data.SqlDbType.Bit, porcionTerrestre);
        base.setParameters("@rutaPorTerrestreStr", System.Data.SqlDbType.NVarChar, rutaTerrestre);
        base.setParameters("@requiereViaticosStr", System.Data.SqlDbType.Bit, viaticos);
        base.setParameters("@valorDiarioViaticoStr", System.Data.SqlDbType.Decimal, valorDiario);
        base.setParameters("@totalViaticosStr", System.Data.SqlDbType.Decimal, totalViaticos);
        base.setParameters("@fechaCreacionStr", System.Data.SqlDbType.DateTime, fechaCreaModifica);
        base.setParameters("@fechaModificacionStr", System.Data.SqlDbType.DateTime, fechaCreaModifica);
        base.setParameters("@idUsuarioCreacionStr", System.Data.SqlDbType.Int, idUsuarioCrea);
        base.setParameters("@idUsuarioModificacionStr", System.Data.SqlDbType.Int, idUsuarioModificacion);
        base.setParameters("@IdentificadorSolicitudStr", System.Data.SqlDbType.NVarChar, identificadorUnico);
        base.setParameters("@cargoFuncionario", System.Data.SqlDbType.NVarChar, cargoFuncionario);
        base.setParameters("@lugarViaje", System.Data.SqlDbType.NVarChar, lugarViaje);
        base.setParameters("@monedaViatico", System.Data.SqlDbType.NVarChar, monedaViatico);
        base.setParameters("@idEstadoActual", System.Data.SqlDbType.NVarChar, idEstadoActual);
        base.setParameters("@valorRutaPorTerrestre", System.Data.SqlDbType.Decimal, valorRutaPorTerrestre);
        base.iniciarTransaccion();

        if (!base.Modificar(strTabla, strValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;


    }


    //☻☻ metodo para actualizar los datos de la solicitud 
    public bool fntActualizarSolicitudPermiteEdicion_bol(long idSolicitud, int lineaIntervencion, string nombre, string objeto, DateTime fechaInicio, DateTime fechaFin, bool tiquetesAereos, string rutaAerea, bool porcionTerrestre, string rutaTerrestre, bool viaticos, decimal valorDiario, decimal numeroDias, decimal totalViaticos, int idUsuarioCrea, int idUsuarioModificacion, DateTime fechaCreaModifica, int idEstadoActual, string identificadorUnico, bool permiteEditar)
    {
        //string strCampos = " numeroAprobacion , idLineaIntervencion , nombreFuncionario , objetoComision , fechaInicio , numeroDias , fechaFin , tiquetesAereos , rutaAerea , porcionTerrestre , rutaPorTerrestre , requiereViaticos , valorDiarioViatico , totalViaticos , fechaCreacion , fechaModificacion , idUsuarioCreacion , idUsuarioModificacion, idEstadoActual , IdentificadorSolicitud ";
        string strTabla = " WFSol_SolicitudComision ";
        string strValores = " numeroAprobacion = @numeroAprobacionStr , idLineaIntervencion = @idLineaIntervencionStr , nombreFuncionario =  @nombreFuncionarioStr ,objetoComision =  @objetoComisionStr ,fechaInicio = @fechaInicioStr ,numeroDias =  @numeroDiasStr ,fechaFin =  @fechaFinStr ,tiquetesAereos = @tiquetesAereosStr ,rutaAerea = @rutaAereaStr ,porcionTerrestre =  @porcionTerrestreStr , rutaPorTerrestre = @rutaPorTerrestreStr ,requiereViaticos = @requiereViaticosStr ,valorDiarioViatico =  @valorDiarioViaticoStr ,totalViaticos = @totalViaticosStr ,fechaCreacion = @fechaCreacionStr , fechaModificacion = @fechaModificacionStr ,idUsuarioCreacion =  @idUsuarioCreacionStr , idUsuarioModificacion =  @idUsuarioModificacionStr  ,IdentificadorSolicitud =  @IdentificadorSolicitudStr , permiteEditar = @permiteEditar ";
        string strCondicion = "  idSolicitud = @idSolicitud ";
        base.clearParameters();


        base.setParameters("@idSolicitud", System.Data.SqlDbType.BigInt, idSolicitud);
        base.setParameters("@numeroAprobacionStr", System.Data.SqlDbType.Int, 0);
        base.setParameters("@idLineaIntervencionStr", System.Data.SqlDbType.Int, lineaIntervencion);
        base.setParameters("@nombreFuncionarioStr", System.Data.SqlDbType.NVarChar, nombre);
        base.setParameters("@objetoComisionStr", System.Data.SqlDbType.NVarChar, objeto);
        base.setParameters("@fechaInicioStr", System.Data.SqlDbType.DateTime, fechaInicio);
        base.setParameters("@numeroDiasStr", System.Data.SqlDbType.Decimal, numeroDias);
        base.setParameters("@fechaFinStr", System.Data.SqlDbType.DateTime, fechaFin);
        base.setParameters("@tiquetesAereosStr", System.Data.SqlDbType.Bit, tiquetesAereos);
        base.setParameters("@rutaAereaStr", System.Data.SqlDbType.NVarChar, rutaAerea);
        base.setParameters("@porcionTerrestreStr", System.Data.SqlDbType.Bit, porcionTerrestre);
        base.setParameters("@rutaPorTerrestreStr", System.Data.SqlDbType.NVarChar, rutaTerrestre);
        base.setParameters("@requiereViaticosStr", System.Data.SqlDbType.Bit, viaticos);
        base.setParameters("@valorDiarioViaticoStr", System.Data.SqlDbType.Decimal, valorDiario);
        base.setParameters("@totalViaticosStr", System.Data.SqlDbType.Decimal, totalViaticos);
        base.setParameters("@fechaCreacionStr", System.Data.SqlDbType.DateTime, fechaCreaModifica);
        base.setParameters("@fechaModificacionStr", System.Data.SqlDbType.DateTime, fechaCreaModifica);
        base.setParameters("@idUsuarioCreacionStr", System.Data.SqlDbType.Int, idUsuarioCrea);
        base.setParameters("@idUsuarioModificacionStr", System.Data.SqlDbType.Int, idUsuarioModificacion);
        base.setParameters("@IdentificadorSolicitudStr", System.Data.SqlDbType.NVarChar, identificadorUnico);
        base.setParameters("@permiteEditar", System.Data.SqlDbType.Bit, permiteEditar);
        base.iniciarTransaccion();

        if (!base.Modificar(strTabla, strValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;


    }





    /// <summary>
    /// ☻☻   metodo para consultar los indicadores por categoria 
    /// </summary>
    /// <param name="dataset"></param>
    /// <param name="idCategoria"></param>
    /// <returns></returns>
    public Boolean fntConsultarIndicadorCategoria(string dataset, int idCategoria)
    {
        string lstrCampos = " idIndicador , nombreIndicador";
        string lstrTabla = " WFSol_Indicador";
        string lstrCondicion = " idEstrategiaRef = @idCategoria ";
        string lstrOrden = " nombreIndicador DESC ";
        base.clearParameters();
        base.setParameters("@idCategoria", SqlDbType.Int, idCategoria);
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// ☻☻ metodo para consultar las actividades por el indicador
    /// </summary>
    /// <param name="dataset"></param>
    /// <param name="idIndicador"></param>
    /// <returns></returns>

    public Boolean fntConsultarActividadIndicador(string dataset, int idIndicador)
    {
        string lstrCampos = " idActividad , nombreActividad";
        string lstrTabla = " WFSol_Actividad";
        string lstrCondicion = " idIndicadorRef = @idIndicador ";
        string lstrOrden = " nombreActividad DESC ";
        base.clearParameters();
        base.setParameters("@idIndicador", SqlDbType.Int, idIndicador);
        if (!Consultar(lstrCampos, lstrTabla, lstrCondicion, lstrOrden, ref dataset, false, ""))
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// ☻☻ metodo para  buscar los cordinadores 
    /// </summary>
    /// <param name="dataset"></param>
    /// <returns></returns>
    public bool fntUsuariosCoordinacion_bol(string dataset)
    {
        string strCampos = " UC.idUsuarioRef, U.segusu_login , U.segusu_Correo  ";
        string strTabla = " WFSol_UsuarioCoordinacion UC, Seg_Usuario U ";
        string strCondicion = " UC.idUsuarioRef = U.segusu_Id  and UC.esCoordinador = 1  ";
        string strOrden = " U.segusu_login ";
        base.clearParameters();
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }

    public bool fntUsuariosPorId(string dataset, int segusu_Id)
    {

        string strCampos = " * ";
        string strTabla = " Seg_Usuario U";
        string strCondicion = " U.segusu_Id = @idUsuario";
        string strOrden = " U.segusu_Id ";
        base.clearParameters();
        base.setParameters("@idUsuario", System.Data.SqlDbType.Int, segusu_Id);
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }



    /// <summary>
    /// ☻☻ metodo para  buscar los correos de los  administratores 
    /// </summary>
    /// <param name="dataset"></param>
    /// <returns></returns>
    public bool fntUsuariosAdministradorCorreo_bol(string dataset)
    {
        string strCampos = " UA.idUsuario, U.segusu_login , U.segusu_Correo  ";
        string strTabla = " WFSol_UsuarioAdministrador UA, Seg_Usuario U ";
        string strCondicion = " UA.idUsuario = U.segusu_Id  and UA.estatus = 'ACT'  ";
        string strOrden = " U.segusu_login ";
        base.clearParameters();
        return base.Consultar(strCampos, strTabla, strCondicion, strOrden, ref dataset, false, "");
    }


    public bool fntActualizarObservacionAnulacion_bol(long idSolicitud, string observacionAnulacion)
    {
        string strTabla = " WFSol_SolicitudComision ";
        string strCamposValores = " observacionAnulacion = @observacionAnulacion ";
        string strCondicion = " idSolicitud = @idSolicitud ";
        base.clearParameters();
        base.setParameters("@idSolicitud", System.Data.SqlDbType.BigInt, idSolicitud);
        base.setParameters("@observacionAnulacion", System.Data.SqlDbType.NVarChar, observacionAnulacion);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }

    /// <summary>
    /// ☻☻ metodo para cambiar el valor del campo permiteEditar de la tabla solicitudComision
    /// </summary>
    /// <param name="idSolicitud"></param>
    /// <param name="permiteEditar"></param>
    /// <returns></returns>
    public bool fntActualizarPermiteEditarSolicitud_bol(long idSolicitud, bool permiteEditar)
    {
        string strTabla = " WFSol_SolicitudComision ";
        string strCamposValores = " permiteEditar = @permiteEditar ";
        string strCondicion = " idSolicitud = @idSolicitud ";
        base.clearParameters();
        base.setParameters("@idSolicitud", System.Data.SqlDbType.BigInt, idSolicitud);
        base.setParameters("@permiteEditar", System.Data.SqlDbType.Bit, permiteEditar);
        base.iniciarTransaccion();
        if (!base.Modificar(strTabla, strCamposValores, strCondicion, true))
        {
            base.rollback();
            return false;
        }
        base.commit();
        return true;
    }






    #endregion
}