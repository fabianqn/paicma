﻿//using ASP;
using System;
using System.Configuration;
using System.Web;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class mpDefault : System.Web.UI.MasterPage
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    private void fntCargarUsuario()
    {
        //if (ctrUsuario.Autenticado) {
        //    string strEstado = string.Empty;
        //    string strTipoConexion = string.Empty;
        //    //string strClaveAux = string.Empty;
        //    //string strContrasenaEncriptada;
        //    if (blPaicma.inicializar(blPaicma.conexionSeguridad)) {
        //        if (blPaicma.fntConsultaUsuario_bol("strDsUsuario", ctrUsuario.Usuario)) {
        //            if (blPaicma.myDataSet.Tables[0].Rows.Count > 0) {
        //                strEstado = blPaicma.myDataSet.Tables[0].Rows[0]["est_Descripcion"].ToString();
        //                strTipoConexion = blPaicma.myDataSet.Tables[0].Rows[0]["segCon_Descripcion"].ToString();
        //                Session["strUsuario"] = ctrUsuario.Nombre;
        //                // strUsuario = blPaicma.myDataSet.Tables[0].Rows[0]["segusu_login"].ToString();
        //                if (strEstado == "ACTIVO") {
        //                    Session["IdUsuario"] = blPaicma.myDataSet.Tables[0].Rows[0]["segusu_Id"].ToString();
        //                    // Response.Redirect("../Default.aspx");
        //                }
        //            }
        //        }
        //        blPaicma.Termina();
        //    }
        //}
    }

    protected void Page_Load(object sender,
                             EventArgs e)
    {
        fntCargarUsuario();
        fntArmarMenu();
    }

    public void fntArmarMenu()
    {
        string text = string.Empty;
        string text2 = string.Empty;
        string text3 = string.Empty;
        string text4 = string.Empty;
        string text5 = string.Empty;
        //string text6 = ConfigurationManager.AppSettings["NomAplication"];
        string text6 = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;
        blSisPAICMA blSisPAICMA = new blSisPAICMA();
        if (blSisPAICMA.inicializar(blSisPAICMA.conexionSeguridad)) {
            if (blSisPAICMA.fntConsultaMenu_bol("strDsMenu", Convert.ToInt32(base.Session["IdUsuario"].ToString()), 1) && blSisPAICMA.myDataSet.Tables[0].Rows.Count > 0) {
                int count = blSisPAICMA.myDataSet.Tables[0].Rows.Count;
                for (int i = 0; i < count; i++) {
                    text3 = blSisPAICMA.myDataSet.Tables[0].Rows[i]["segfor_Nombre"].ToString();
                    text2 = blSisPAICMA.myDataSet.Tables[0].Rows[i]["segfor_DescripcionMenu"].ToString();
                    text4 = blSisPAICMA.myDataSet.Tables[0].Rows[i]["segfor_Id"].ToString();
                    text5 = blSisPAICMA.myDataSet.Tables[0].Rows[i]["segfor_origen"].ToString();
                    text = string.Concat(new string[] { text,
                                                        "<li ><a class=parent title=",
                                                        text2,
                                                        " rel=",
                                                        text6,
                                                        "/",
                                                        text5,
                                                        "/",
                                                        text3,
                                                        "?op=",
                                                        text4,
                                                        " href=",
                                                        text6,
                                                        "/",
                                                        text5,
                                                        "/",
                                                        text3,
                                                        ">",
                                                        text2,
                                                        "</a></li>" });
                }
                
                this.lblMenu.Text = text;
                // this.lblUser.Text = "Usuario: " + base.Session["strUsuario"].ToString();
            }
            blSisPAICMA.Termina();
        }
    }
}
