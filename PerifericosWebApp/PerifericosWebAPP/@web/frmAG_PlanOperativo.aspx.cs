﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

public partial class _web_FRMag_PlanOperativo : System.Web.UI.Page
{
    //private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        fntCargarArchivo();
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        //blPaicma = null;
    }

    public void fntCargarArchivo()
    {
        string strNomSP = "SP_AG3";
        string strTablaTemporal = string.Empty;
        int intCantidad = 0;
        string strRutaServidor = string.Empty;
        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            strTablaTemporal = "tmp_AG3";
            blPaicma.fntEliminarTablaTemporal_bol(strTablaTemporal);

            strRutaServidor = System.Configuration.ConfigurationManager.AppSettings["RutaServerDB"];
            strRutaServidor = strRutaServidor + Session["strRutaCompleta"].ToString();

            if (fntInsertarArchivo(strRutaServidor))
            {
                Session["ESTRUCTURA"] = null;
                if (blPaicma.fntValidarSeguridadAG3("strDsAG3"))
                {
                    intCantidad = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                    if (intCantidad > 0)
                    {
                        ///el archivo no es seguro, tiene palabras reservadas del motor;
                        Session["Resultado"] = "NO";
                        Session["PELIGRO"] = "OK";
                    }
                    else
                    {
                        ///el archivo es seguro, no tiene palabras reservadas del motor;
                        Session["PELIGRO"] = null;
                        blPaicma.CargarArchivo(strNomSP);
                        fntCargarSeguimientoPO_bol();
                    }
                }
            }
            else
            {
                Session["Resultado"] = "NO";
                Session["ESTRUCTURA"] = "NO";
                Session["PELIGRO"] = "NA";
            }
            blPaicma.Termina();
        }
        blPaicma = null;
        System.IO.File.Delete(Session["strRutaCompleta"].ToString());
        Response.Redirect("frmCApoyoGestion.aspx");
    }

    public void fntCargarSeguimientoPO_bol()
    {
        string[] strSeguimientoPO = new string[9];
        string[] strCompromisoPO = new string[4];
        int intContador = 0;
        int intIdSeguimientoPO = 0;
        int intIdActividadPO = 0;
        string strFechaInicio = "";
        int intSw = 0;
        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaApoyoGestionPlanOperativoTmp_bol("strDsPlanOperativo"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    intContador = blPaicma.myDataSet.Tables[0].Rows.Count;
                    for (int i = 0; i < intContador; i++)
                    {
                       for (int j = 1; j < 10; j++)
                        {
                            strSeguimientoPO[(j-1)] = blPaicma.myDataSet.Tables[0].Rows[i][j].ToString();                       
                        }
                       intIdActividadPO = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[i][10].ToString());
                       strFechaInicio = blPaicma.myDataSet.Tables[0].Rows[i][4].ToString();

                        ///INSERTAR LOS REGISTROS
                       intIdSeguimientoPO = InsertaSeguimientoPO(strSeguimientoPO);

                       ///INSERTAR LOS REGISTROS ACTIVIDAD SEGUIMIENTO PO
                       if (fntInsertarSeguimientoPOvsActividad_bol(intIdActividadPO, intIdSeguimientoPO))
                       {
                           ////insertar los compromisos
                           for (int k = 13; k < 17; k++)
                           {
                               strCompromisoPO[(k - 13)] = blPaicma.myDataSet.Tables[0].Rows[i][k].ToString();
                               if (strCompromisoPO[(k - 13)].ToString() == string.Empty)
                               {
                                   k = 16;
                                   intSw = 0;
                               }
                               else
                               {
                                   intSw = 1;
                               }
                           }
                           if (intSw == 1)
                           {
                               if (blPaicma.fntIngresarCompromisoPO_bol(strCompromisoPO[(0)].ToString(), Convert.ToInt32(strCompromisoPO[(1)].ToString()), "", strFechaInicio.ToString(), strCompromisoPO[(2)].ToString(), Convert.ToInt32(strCompromisoPO[(3)].ToString()), intIdSeguimientoPO))
                               {
                                   intSw = 0;
                               }
                           }

                           ////insertar los compromisos Otros
                           for (int l = 17; l < 21; l++)
                           {
                               strCompromisoPO[(l - 17)] = blPaicma.myDataSet.Tables[0].Rows[i][l].ToString();
                               if (strCompromisoPO[(l - 17)].ToString() == string.Empty)
                               {
                                   l = 21;
                                   intSw = 0;
                               }
                               else
                               {
                                   intSw = 1;
                               }
                           }
                           if (intSw == 1)
                           {
                               if (blPaicma.fntIngresarCompromisoPO_bol(strCompromisoPO[(0)].ToString(), Convert.ToInt32(strCompromisoPO[(1)].ToString()), "", strFechaInicio.ToString(), strCompromisoPO[(2)].ToString(), Convert.ToInt32(strCompromisoPO[(3)].ToString()), intIdSeguimientoPO))
                               {
                                   intSw = 0;
                               }
                           }
                           Session["Resultado"] = "OK";
                       }
                       else
                       {
                           Session["Resultado"] = "NO";
                       }
                    }
                }
            }

            blPaicma.Termina();
        }
        blPaicma = null;
    }

    public int InsertaSeguimientoPO(string[] strSeguimientoPO)
    {
        int intIdSeguimientoPO = 0;
        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarSeguimientoPO_bol(Convert.ToInt32(strSeguimientoPO[0].ToString()), Convert.ToInt32(strSeguimientoPO[1].ToString()), strSeguimientoPO[2].ToString(), strSeguimientoPO[3].ToString(), strSeguimientoPO[4].ToString(), strSeguimientoPO[5].ToString(), strSeguimientoPO[6].ToString(), strSeguimientoPO[7].ToString(), Convert.ToInt32(strSeguimientoPO[8].ToString())))
            {
                if (blPaicma.fntConsultaSeguimientoPO_bol("strDsSeguimientoPO", Convert.ToInt32(strSeguimientoPO[0].ToString()), Convert.ToInt32(strSeguimientoPO[1].ToString()), strSeguimientoPO[2].ToString(), strSeguimientoPO[3].ToString(), strSeguimientoPO[4].ToString(), strSeguimientoPO[5].ToString(), strSeguimientoPO[6].ToString(), strSeguimientoPO[7].ToString(), Convert.ToInt32(strSeguimientoPO[8].ToString())))
                { 
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        intIdSeguimientoPO =Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                    }
                }
            }
            blPaicma.Termina();
        }
        blPaicma = null;
        return intIdSeguimientoPO;
    }

    public Boolean fntInsertarSeguimientoPOvsActividad_bol(int intIdActividadPO, int intIdSeguimientoPO)
    {
        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarSeguimientoPOVsActividad_bol(intIdActividadPO, intIdSeguimientoPO))
            { 

            }
            blPaicma.Termina();
        }
        blPaicma = null;
        return true;
    }


    public Boolean fntInsertarArchivo(string strRutaServidor)
    {
        Boolean bolValor = false;
        string strTablaTemporal = string.Empty;

        ///inicio 
        int intCantidadCampos = 0;
        string strCampos = string.Empty;
        int intCantidadXls = 0;
        string strCamposExcel = string.Empty;
        int intMaxId = 0;
        string strNomSPReiniciar = string.Empty;
        ///fin 

        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            strTablaTemporal = "tmp_AG3";
            try
            {
                if (blPaicma.fntConsultaCampoTablas("strDsCamposTablas", strTablaTemporal))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        intCantidadCampos = blPaicma.myDataSet.Tables[0].Rows.Count;
                        for (int i = 0; i < intCantidadCampos; i++)
                        {
                            if (i != 0)
                            {
                                if ((i + 1) != intCantidadCampos)
                                {
                                    strCampos = strCampos + blPaicma.myDataSet.Tables[0].Rows[i][1].ToString() + " ,";
                                }
                                else
                                {
                                    strCampos = strCampos + blPaicma.myDataSet.Tables[0].Rows[i][1].ToString();
                                }

                            }
                        }
                    }
                }

                intCantidadCampos = intCantidadCampos - 1;
                string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strRutaServidor + ";Extended Properties=Excel 12.0;";
                OleDbConnection conn = new OleDbConnection(strConn);
                conn.Open();
                string strExcel = "";
                OleDbDataAdapter myCommand = null;
                DataSet ds = null;
                strExcel = "select * from [Hoja1$]";
                myCommand = new OleDbDataAdapter(strExcel, strConn);
                ds = new DataSet();
                myCommand.Fill(ds, "table1");
                intCantidadXls = ds.Tables[0].Rows.Count;
                conn.Close();

                for (int i = 0; i < intCantidadXls; i++)
                {
                    strCamposExcel = string.Empty;
                    for (int j = 0; j < intCantidadCampos; j++)
                    {
                        if ((j + 1) != intCantidadCampos)
                        {
                            strCamposExcel = strCamposExcel + "'" + ds.Tables[0].Rows[i][j].ToString() + "' ,";
                        }
                        else
                        {
                            strCamposExcel = strCamposExcel + "'" + ds.Tables[0].Rows[i][j].ToString() + "'"; ;
                        }

                    }

                    if (blPaicma.fntConsultaMaxIdTablaDinamica_bol("strDsId", strTablaTemporal))
                    {
                        intMaxId = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                    }
                    if (intMaxId == 1)
                    {
                        strNomSPReiniciar = "SP_ReiniciarIndice ";
                        blPaicma.ReiniciaIndice(strNomSPReiniciar, strTablaTemporal);


                        // lanzo sp_reiniciarIndice
                    }
                    string sw = string.Empty;
                    if (blPaicma.fntIngresarTablaDinamica2_bol(strCampos, strTablaTemporal, strCamposExcel, intMaxId))
                    {
                        sw = "OK";
                    }
                    else
                    {
                        bolValor = false;
                        break;
                    }

                }

            }
            catch (Exception ex)
            {
                string strError = ex.ToString();
                Session["Resultado"] = "NO";
                Session["ESTRUCTURA"] = "NO";
                Session["PELIGRO"] = "NA";
                blPaicma.Termina();
            }
            bolValor = true;
            blPaicma.Termina();
        }
        blPaicma = null;
        return bolValor;
    }


}