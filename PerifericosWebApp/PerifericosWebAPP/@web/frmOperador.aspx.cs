﻿using AjaxControlToolkit;
//using ASP;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _web_frmOperador : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }
            this.ftnValidarPermisos();
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                this.fntCargarGrillaGestionOperador(3, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                return;
            }
            base.Response.Redirect("@dmin/frmLogin.aspx");
        }
    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        this.blPaicma = null;
    }
    public void ftnValidarPermisos()
    {
        string a = string.Empty;
        string a2 = string.Empty;
        string a3 = string.Empty;
        string a4 = string.Empty;
        string[] array = HttpContext.Current.Request.RawUrl.Split(new char[]
		{
			'/'
		});
        string text = array[array.GetUpperBound(0)];
        array = text.Split(new char[]
		{
			'?'
		});
        text = array[array.GetLowerBound(0)];
        if (text != string.Empty)
        {
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (this.blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(this.Session["IdUsuario"].ToString()), text) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    a = this.blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                    a2 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                    a3 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                    a4 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();
                    if (a == "False")
                    {
                        this.imgbBuscar.Visible = false;
                    }
                    if (a2 == "False")
                    {
                        this.imgbNuevo.Visible = false;
                    }
                    if (a3 == "False")
                    {
                        this.imgbEditar.Visible = false;
                        this.imgbGravar.Visible = false;
                    }
                    if (a4 == "False")
                    {
                        this.imgbEliminar.Visible = false;
                    }
                }
                this.blPaicma.Termina();
                return;
            }
        }
        else
        {
            base.Response.Redirect("@dmin/frmLogin.aspx");
        }
    }

    protected void gvOperador_PreRender(object sender, EventArgs e)
    {
        if (gvOperador.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvOperador.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvOperador.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvOperador.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }


    protected void gvActividadesOperador_PreRender(object sender, EventArgs e)
    {
        if (gvActividadesOperador.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvActividadesOperador.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvActividadesOperador.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvActividadesOperador.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }

    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlAccionesOperador.Visible = true;
        this.imgbEncontrar.Visible = false;
        this.imgbEliminar.Visible = false;
        this.imgbGravar.Visible = true;
        this.imgbCancelar.Visible = true;
        this.pnlOperadorNuevo.Visible = true;
        this.pnlMenuOperador.Visible = false;
        this.PnlOperadorGrilla.Visible = false;
        this.Session["Operacion"] = "Crear";
        this.fntValidarGrupos();
    }
    public int fntValidarGrupos()
    {
        int num = 0;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaGruposDisponiblesUsuario_bol("dsGrupos", Convert.ToInt32(this.Session["IdUsuario"].ToString())))
            {
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    num = this.blPaicma.myDataSet.Tables[0].Rows.Count;
                    this.lblGrupo.Visible = true;
                    this.ddlGrupo.Visible = true;
                    this.ddlGrupo.DataMember = "dsGrupos";
                    this.ddlGrupo.DataSource = this.blPaicma.myDataSet;
                    this.ddlGrupo.DataValueField = "gruOpe_Id";
                    this.ddlGrupo.DataTextField = "gruOpe_Nombre";
                    this.ddlGrupo.DataBind();
                    if (num > 1)
                    {
                        this.ddlGrupo.Items.Insert(this.ddlGrupo.Attributes.Count, "Seleccione...");
                    }
                }
                else
                {
                    this.fntInactivarPaneles();
                    this.lblRespuesta.Text = "No tiene ningún grupo asignado, comuníquese con el administrador para que le asignen un grupo.";
                }
            }
            this.blPaicma.Termina();
        }
        return num;
    }
    public int fntValidarGruposBusqueda()
    {
        int num = 0;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaGruposDisponiblesUsuario_bol("dsGrupos", Convert.ToInt32(this.Session["IdUsuario"].ToString())))
            {
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    num = this.blPaicma.myDataSet.Tables[0].Rows.Count;
                    this.lblGrupo.Visible = true;
                    this.ddlNombreGrupo.Visible = true;
                    this.ddlNombreGrupo.DataMember = "dsGrupos";
                    this.ddlNombreGrupo.DataSource = this.blPaicma.myDataSet;
                    this.ddlNombreGrupo.DataValueField = "gruOpe_Id";
                    this.ddlNombreGrupo.DataTextField = "gruOpe_Nombre";
                    this.ddlNombreGrupo.DataBind();
                    if (num > 1)
                    {
                        this.ddlNombreGrupo.Items.Insert(this.ddlNombreGrupo.Attributes.Count, "Seleccione...");
                    }
                }
                else
                {
                    this.fntInactivarPaneles();
                    this.lblRespuesta.Text = "No tiene ningún grupo asignado, comuníquese con el administrador para que le asignen un grupo.";
                }
            }
            this.blPaicma.Termina();
        }
        return num;
    }
    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        string url = "frmOperador.aspx";
        base.Response.Redirect(url);
    }
    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        string text = string.Empty;
        string strExtDocumento = string.Empty;
        string str = string.Empty;
        string text2 = string.Empty;
        string strGrupoOperador = string.Empty;
        string nombreSinExtension = string.Empty;
        string unique = string.Empty;
        try
        {
            if (this.ftnValidarCampos_bol())
            {
                //text = this.fuDocumento.FileName;
                strExtDocumento = Path.GetExtension(this.fuDocumento.PostedFile.FileName);
                nombreSinExtension = Path.GetFileNameWithoutExtension(this.fuDocumento.PostedFile.FileName);
                str = base.Server.MapPath("~\\@web\\Operador\\");
                unique = Guid.NewGuid().ToString();
                text = unique + strExtDocumento;
                text2 = str + text;
                this.fuDocumento.PostedFile.SaveAs(text2);
                strGrupoOperador = this.ddlGrupo.SelectedItem.ToString();
                if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
                {
                    if (this.blPaicma.fntIngresarGestionOperador_bol(text2, nombreSinExtension + strExtDocumento, strExtDocumento, Convert.ToInt32(this.Session["IdUsuario"].ToString()), this.txtObservacion.Text.Trim(), Convert.ToInt32(this.ddlGrupo.SelectedValue.ToString())))
                    {
                        this.fntTraerGrupoFuncionarioOperadores(Convert.ToInt32(this.ddlGrupo.SelectedValue.ToString()), nombreSinExtension + strExtDocumento, strGrupoOperador);
                        this.fntInactivarPaneles();
                        this.lblRespuesta.Text = this.lblRespuesta.Text+" Archivo cargado satisfactoriamente";
                    }
                    else
                    {
                        this.fntInactivarPaneles();
                        this.lblRespuesta.Text = this.lblRespuesta.Text+" Problema al cargar el archivo, por favor comuníquese con el administrador.";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            base.Response.Write(ex.ToString() + "<br />");
        }
    }
    public bool ftnValidarCampos_bol()
    {
        string a = string.Empty;
        string a2 = string.Empty;
        a = this.fuDocumento.FileName;
        a2 = Path.GetExtension(this.fuDocumento.PostedFile.FileName);
        if (a == string.Empty)
        {
            this.lblError.Text = "Debe seleccionar un documento.";
            return false;
        }
        if (!(a2 == ".txt") && !(a2 == ".pdf") && !(a2 == ".doc") && !(a2 == ".docx") && !(a2 == ".xls") && !(a2 == ".xlsx") && !(a2 == ".rar") && !(a2 == ".zip") && !(a2 == ".jpg") && !(a2 == ".gif") && !(a2 == ".png") && !(a2 == ".tif") && !(a2 == ".mp3") && !(a2 == ".mp4") && !(a2 == ".wma") && !(a2 == ".midi") && !(a2 == ".pptx") && !(a2 == ".ppt") && !(a2 == ".pps"))
        {
            this.lblError.Text = "Este tipo de archivo no se puede subir. Por favor verificar.";
            return false;
        }
        this.lblError.Text = "formato correcto.";
        if (this.txtObservacion.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe indicar un comentario u observación.";
            return false;
        }
        if (this.ddlGrupo.SelectedValue == "Seleccione...")
        {
            this.lblError.Text = "Debe seleccionar un grupo.";
            return false;
        }
        return true;
    }
    public void fntInactivarPaneles()
    {
        this.pnlRespuesta.Visible = true;
        this.pnlAccionesOperador.Visible = false;
        this.pnlOperadorNuevo.Visible = false;
        this.pnlMenuOperador.Visible = false;
        this.pnlEliminacion.Visible = false;
        this.pnlOperadorGestionNuevo.Visible = false;
        this.pnlGestionRealizada.Visible = false;
        this.pnlBuscar.Visible = false;
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        string url = "frmOperador.aspx";
        base.Response.Redirect(url);
    }
    public void fntCargarGrillaGestionOperador(int intOP, string strNombreArchivo, string strObservacionArchivo, string strNombreGrupo, string strEstado, string strFechaDesde, string strFechaHasta)
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaGestionOperador("strDsGrillaOperador", intOP, Convert.ToInt32(this.Session["IdUsuario"].ToString()), 0, strNombreArchivo, strObservacionArchivo, 0, string.Empty, strNombreGrupo, strEstado, strFechaDesde, strFechaHasta))
            {
                this.gvOperador.Columns[0].Visible = true;
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gvOperador.DataMember = "strDsGrillaOperador";
                    this.gvOperador.DataSource = this.blPaicma.myDataSet;
                    this.gvOperador.DataBind();
                    this.lblErrorGv.Text = string.Empty;
                }
                else
                {
                    this.gvOperador.DataMember = "strDsGrillaOperador";
                    this.gvOperador.DataSource = this.blPaicma.myDataSet;
                    this.gvOperador.DataBind();
                    this.lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                this.gvOperador.Columns[0].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    protected void gvOperador_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            this.gvOperador.Columns[0].Visible = true;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow = this.gvOperador.Rows[index];
            TableCell tableCell = gridViewRow.Cells[0];
            int num = Convert.ToInt32(tableCell.Text);
            this.Session["intIdGestionOperador"] = num;
            this.imgbEditar.Visible = true;
            this.gvOperador.Columns[0].Visible = false;
            string a = string.Empty;
            string str = string.Empty;
            string text = string.Empty;
            this.pnlEliminacion.Visible = false;
            this.pnlRespuesta.Visible = false;
            this.pnlMenuOperador.Visible = false;
            this.pnlAccionesOperador.Visible = true;
            this.imgbEncontrar.Visible = false;
            this.imgbGravar.Visible = false;
            this.imgbCancelar.Visible = true;
            this.pnlOperadorGestionNuevo.Visible = true;
            this.PnlOperadorGrilla.Visible = false;
            this.ftnValidarPermisos();
            this.Session["Operacion"] = "Actualizar";
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (this.blPaicma.fntConsultaGestionOperador("strDsGrillaOperadorGestion", 2, 0, Convert.ToInt32(this.Session["intIdGestionOperador"].ToString()), string.Empty, string.Empty, 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.txtDocumento.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                    this.txtFechaCarga.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                    this.lblRuta.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                    this.txtObservacionRevision.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][7].ToString();
                    a = this.blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                    if (a == ".pdf")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/pdf.png";
                    }
                    if (a == ".doc" || a == ".docx")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/word.png";
                    }
                    if (a == ".xls" || a == ".xlsx")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/excel.png";
                    }
                    if (a == ".rar")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/zip.png";
                    }
                    if (a == ".zip")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/zip.png";
                    }
                    if (a == ".jpg" || a == ".gif" || a == ".png" || a == ".tif")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/imagen.png";
                    }
                    if (a == ".mp3" || a == ".mp4" || a == ".wma" || a == ".midi")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/musica.png";
                    }
                    if (a == ".pptx" || a == ".ppt" || a == ".pps")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/ppt.png";
                    }
                    if (a == ".txt")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/txt.png";
                    }
                    if (a == ".bak")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/baseDatos.png";
                    }
                    text = ConfigurationManager.AppSettings["RutaServer"];
                    str = "Operador/";
                    text = text + str + this.txtDocumento.Text.Trim();
                
                    this.pnlOperadorGestionNuevo.Visible = true;
                    this.pnlGestionRealizada.Visible = true;
                    //this.gvActividadesOperador.Columns[0].Visible = true;
                    this.gvActividadesOperador.DataMember = "strDsGrillaOperadorGestion";
                    this.gvActividadesOperador.DataSource = this.blPaicma.myDataSet;
                    this.gvActividadesOperador.DataBind();
                    //this.gvActividadesOperador.Columns[0].Visible = false;
                    this.lblErrorGvActividades.Text = string.Empty;
                }
                this.blPaicma.Termina();
            }
        }
    }
    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        string a = string.Empty;
        string str = string.Empty;
        string text = string.Empty;
        this.pnlEliminacion.Visible = false;
        this.pnlRespuesta.Visible = false;
        this.pnlMenuOperador.Visible = false;
        this.pnlAccionesOperador.Visible = true;
        this.imgbEncontrar.Visible = false;
        this.imgbGravar.Visible = true;
        this.imgbCancelar.Visible = true;
        this.pnlOperadorGestionNuevo.Visible = true;
        this.PnlOperadorGrilla.Visible = false;
        this.ftnValidarPermisos();
        this.Session["Operacion"] = "Actualizar";
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaGestionOperador("strDsGrillaOperadorGestion", 2, 0, Convert.ToInt32(this.Session["intIdGestionOperador"].ToString()), string.Empty, string.Empty, 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.txtDocumento.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                this.txtFechaCarga.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                this.lblRuta.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                this.txtObservacionRevision.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][7].ToString();
                a = this.blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                if (a == ".pdf")
                {
                    this.imbDocumento.ImageUrl = "~/Images/pdf.png";
                }
                if (a == ".doc" || a == ".docx")
                {
                    this.imbDocumento.ImageUrl = "~/Images/word.png";
                }
                if (a == ".xls" || a == ".xlsx")
                {
                    this.imbDocumento.ImageUrl = "~/Images/excel.png";
                }
                if (a == ".rar")
                {
                    this.imbDocumento.ImageUrl = "~/Images/zip.png";
                }
                if (a == ".zip")
                {
                    this.imbDocumento.ImageUrl = "~/Images/zip.png";
                }
                if (a == ".jpg" || a == ".gif" || a == ".png" || a == ".tif")
                {
                    this.imbDocumento.ImageUrl = "~/Images/imagen.png";
                }
                if (a == ".mp3" || a == ".mp4" || a == ".wma" || a == ".midi")
                {
                    this.imbDocumento.ImageUrl = "~/Images/musica.png";
                }
                if (a == ".pptx" || a == ".ppt" || a == ".pps")
                {
                    this.imbDocumento.ImageUrl = "~/Images/ppt.png";
                }
                if (a == ".txt")
                {
                    this.imbDocumento.ImageUrl = "~/Images/txt.png";
                }
                if (a == ".bak")
                {
                    this.imbDocumento.ImageUrl = "~/Images/baseDatos.png";
                }
                text = ConfigurationManager.AppSettings["RutaServer"];
                str = "Operador/";
                text = text + str + this.txtDocumento.Text.Trim();
             
                this.pnlOperadorGestionNuevo.Visible = true;
                this.pnlGestionRealizada.Visible = true;
                //this.gvActividadesOperador.Columns[0].Visible = true;
                this.gvActividadesOperador.DataMember = "strDsGrillaOperadorGestion";
                this.gvActividadesOperador.DataSource = this.blPaicma.myDataSet;
                this.gvActividadesOperador.DataBind();
                //this.gvActividadesOperador.Columns[0].Visible = false;
                this.lblErrorGvActividades.Text = string.Empty;
            }
            this.blPaicma.Termina();
        }
    }
    protected void imbDocumento_Click(object sender, ImageClickEventArgs e)
    {
        string nombredelDocumentos = Path.GetFileName(this.lblRuta.Text.Trim());
        string text = ConfigurationManager.AppSettings["RutaServer"];
        string str = "Operador/";
        text = text + str + nombredelDocumentos;

        this.Session["strRutaDocumento"] = text;
        base.Response.Write("<script language='JavaScript'>window.open('" + this.Session["strRutaDocumento"].ToString() + "')</script>");
    }
    protected void gvActividadesOperador_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string text = string.Empty;
        string str = string.Empty;
        string arg_11_0 = string.Empty;
        string text2 = string.Empty;
        if (e.CommandName == "Ver")
        {
            //this.gvActividadesOperador.Columns[0].Visible = true;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow = this.gvActividadesOperador.Rows[index];
            TableCell tableCell = gridViewRow.Cells[0];
            string rutaBuscada = string.Empty;
            string nombreRealDocumento = string.Empty;
            try
            {
                int IdABuscar = Convert.ToInt32(tableCell.Text);
                if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
                {
                    if (this.blPaicma.fntConsultarSeguimientoEspecifico("rutadeDocumentoaBuscar", IdABuscar))
                    {
                        rutaBuscada = this.blPaicma.myDataSet.Tables[0].Rows[0][0].ToString();
                        nombreRealDocumento = Path.GetFileName(rutaBuscada);

                    }
                }    
                //Convert.ToInt32(tableCell.Text);
                //TableCell tableCell2 = gridViewRow.Cells[3];
                //string text3 = tableCell2.Text.Trim();
                //text2 = text3;
                //text2 = text2.Replace("&nbsp;", string.Empty);
                if (nombreRealDocumento.Trim() != string.Empty)
                {
                    text = ConfigurationManager.AppSettings["RutaServer"];
                    str = "Operador/Revision/";
                    text = text + str + nombreRealDocumento;
                    base.Response.Write("<script language='JavaScript'>window.open('" + text + "')</script>");
                    this.lblErrorGvActividades.Text = string.Empty;
                }
                else
                {
                    this.lblErrorGvActividades.Text = "No hay un documento asociado en el registro de seguimiento.";
                }
            }
            catch
            {
                this.lblErrorGvActividades.Text = "No hay un documento asociado en el registro de seguimiento.";
            }
            //this.gvActividadesOperador.Columns[0].Visible = false;
        }
    }
    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlAccionesOperador.Visible = true;
        this.pnlMenuOperador.Visible = false;
        this.imgbEncontrar.Visible = true;
        this.imgbEliminar.Visible = true;
        this.imgbGravar.Visible = false;
        this.imgbCancelar.Visible = true;
        this.pnlOperadorNuevo.Visible = false;
        this.PnlOperadorGrilla.Visible = false;
        this.pnlBuscar.Visible = true;
        this.fntCargarEstado();
        this.fntValidarGruposBusqueda();
        this.ftnLimpiarControles();
    }
    public void fntCargarEstado()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaEstadoSeguimientoOperador_bol("strDsEstado") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlEstado.DataMember = "strDsEstado";
                this.ddlEstado.DataSource = this.blPaicma.myDataSet;
                this.ddlEstado.DataValueField = "est_Id";
                this.ddlEstado.DataTextField = "est_Descripcion";
                this.ddlEstado.DataBind();
                this.ddlEstado.Items.Insert(this.ddlEstado.Attributes.Count, "Por revisar");
                this.ddlEstado.Items.Insert(this.ddlEstado.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    protected void imgbEncontrar_Click(object sender, ImageClickEventArgs e)
    {
        string text = string.Empty;
        string text2 = string.Empty;
        text = this.ddlEstado.SelectedItem.ToString();
        if (text == "Seleccione...")
        {
            text = string.Empty;
        }
        text2 = this.ddlNombreGrupo.SelectedItem.ToString();
        if (text2 == "Seleccione...")
        {
            text2 = string.Empty;
        }
        this.fntCargarGrillaGestionOperador(4, this.txtNombreDocumento.Text.Trim(), this.txtObservacionDocumento.Text.Trim(), text2, text, this.txtDesde.Text.Trim(), this.txtHasta.Text.Trim());
        this.pnlAccionesOperador.Visible = false;
        this.pnlMenuOperador.Visible = true;
        this.pnlBuscar.Visible = false;
        this.PnlOperadorGrilla.Visible = true;
    }
    public void ftnLimpiarControles()
    {
        this.txtNombreDocumento.Text = string.Empty;
        this.txtObservacionDocumento.Text = string.Empty;
        this.txtDesde.Text = string.Empty;
        this.txtHasta.Text = string.Empty;
    }
    protected void imgbEliminar_Click(object sender, ImageClickEventArgs e)
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaCantidadSeguimientoOperador("strDsCantidadSeguimientoOperador", Convert.ToInt32(this.Session["intIdGestionOperador"].ToString())))
            {
                int num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                if (num > 0)
                {
                    this.fntInactivarPaneles();
                    this.lblRespuesta.Text = "No se puede eliminar el documento porque ya tiene gestión relacionada.";
                }
                else
                {
                    this.fntInactivarPaneles();
                    this.pnlRespuesta.Visible = false;
                    this.pnlEliminacion.Visible = true;
                    this.lblEliminacion.Text = "¿Está seguro de eliminar el registro y su archivo adjunto?";
                }
            }
            this.blPaicma.Termina();
        }
    }
    protected void btnNo_Click(object sender, EventArgs e)
    {
        this.fntInactivarPaneles();
        this.pnlRespuesta.Visible = false;
        this.pnlAccionesOperador.Visible = true;
        this.pnlOperadorGestionNuevo.Visible = true;
        this.pnlGestionRealizada.Visible = true;
    }
    protected void btnSi_Click(object sender, EventArgs e)
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntEliminarGestionOperador(Convert.ToInt32(this.Session["intIdGestionOperador"].ToString())))
            {
                this.lblRespuesta.Text = "Registro eliminado satisfactoriamente.";
            }
            else
            {
                this.lblRespuesta.Text = "Problemas al eliminar el registro, por favor contáctese con el administrador.";
            }
            this.fntInactivarPaneles();
            this.blPaicma.Termina();
        }
    }
    protected void gvOperador_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvOperador.Columns[0].Visible = true;
        this.gvOperador.PageIndex = e.NewPageIndex;
        this.fntCargarGrillaGestionOperador(3, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
        this.gvOperador.Columns[0].Visible = false;
    }
    //protected void gvActividadesOperador_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    this.gvActividadesOperador.Columns[0].Visible = true;
    //    this.gvActividadesOperador.PageIndex = e.NewPageIndex;
    //    if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
    //    {
    //        if (this.blPaicma.fntConsultaGestionOperador("strDsGrillaOperadorGestion", 2, 0, Convert.ToInt32(this.Session["intIdGestionOperador"].ToString()), string.Empty, string.Empty, 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
    //        {
    //            this.gvActividadesOperador.DataMember = "strDsGrillaOperadorGestion";
    //            this.gvActividadesOperador.DataSource = this.blPaicma.myDataSet;
    //            this.gvActividadesOperador.DataBind();
    //        }
    //        this.blPaicma.Termina();
    //    }
    //    this.gvActividadesOperador.Columns[0].Visible = false;
    //}
    public bool fntTraerGrupoFuncionarioOperadores(int intIdGrupoOperador, string strNombreDocumento, string strGrupoOperador)
    {
        string strCorreoTo = string.Empty;
        string strTitulo = string.Empty;
        string strTextoMensaje = string.Empty;
        bool envioCorreo = false;
        string fallidos = string.Empty;
        string Destinatarios = string.Empty;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaGrupoFuncionariosOperadores("strDsGrupoFuncionario", intIdGrupoOperador))
            {
                int count = this.blPaicma.myDataSet.Tables[0].Rows.Count;
                if (count > 0)
                {
                    string[] array = new string[count];
                    for (int i = 0; i < count; i++)
                    {
                        array[i] = this.blPaicma.myDataSet.Tables[0].Rows[i][5].ToString();
                    }

                    //strTitulo = " Esto es una Prueba ";
                    //strTextoMensaje = " Esto es una Prueba ";
                    var nombre = Session["strUsuario"].ToString();
                    strTitulo = " Grupo " + strGrupoOperador + " , documento " + strNombreDocumento;
                    strTextoMensaje = "El Usuario: " + nombre;
                    strTextoMensaje = strTextoMensaje + " ha cargado el documento " + strNombreDocumento + " en el Grupo " + strGrupoOperador;
                    strTextoMensaje = strTextoMensaje + " adjuntando la siguiente observacion:  " + txtObservacion.Text;
                    for (int j = 0; j < count; j++)
                    {
                        strCorreoTo = array[j].ToString();
                        Destinatarios = Destinatarios + " - " + strCorreoTo;
                        envioCorreo = this.ftnEnviarCorreo(strCorreoTo, strTitulo, strTextoMensaje);
                        if (!envioCorreo)
                        {
                            fallidos = fallidos + " - " + strCorreoTo;
                        }

                    }
                }
            }
            this.blPaicma.Termina();
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (this.blPaicma.fntInsertarLogEmail_bol("DAICMA", Destinatarios, strTitulo, strTextoMensaje, "Operador", DateTime.Now, envioCorreo, fallidos))
                {
                    if (!envioCorreo)
                    {
                        this.lblRespuesta.Text = "Problemas para enviar a algunos correos: " + fallidos + " sin embargo ";
                    }
                }
            }
        }
        return true;
    }
    public bool ftnEnviarCorreo(string strCorreoTo, string strTitulo, string strTextoMensaje)
    {
        bool result = false;
        string text = string.Empty;
        string displayName = string.Empty;
        string host = string.Empty;
        string password = string.Empty;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaCorreo("strDsCorreo", 0) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                text = this.blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                displayName = this.blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                host = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                int port = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][2].ToString());
                password = this.blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                int num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][6].ToString());
                if (num == 1)
                {
                    MailMessage mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress(text, displayName);
                    mailMessage.To.Add(strCorreoTo);
                    mailMessage.Subject = strTitulo;
                    mailMessage.Body = strTextoMensaje;
                    mailMessage.IsBodyHtml = true;
                    SmtpClient smtpClient = new SmtpClient();
                    smtpClient.Host = host;
                    smtpClient.Port = port;
                    smtpClient.EnableSsl = false;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new NetworkCredential(text, password);
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    try
                    {
                        smtpClient.Send(mailMessage);
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
            }
            this.blPaicma.Termina();
        }
        return result;
    }
}