﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _web_frmMatrizVictimas : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
           
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }


    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaMatrizVictimas_bol("strDsMatrizVictimas"))
            {
                DataTable dt = blPaicma.myDataSet.Tables[0];//your datatable
                string attachment = "attachment; filename=MatrizVictimas.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/vnd.ms-excel";
                string tab = "";
                foreach (DataColumn dc in dt.Columns)
                {
                    Response.Write(tab + dc.ColumnName);
                    tab = "\t";
                }
                Response.Write("\n");
                int i;
                foreach (DataRow dr in dt.Rows)
                {
                    tab = "";
                    for (i = 0; i < dt.Columns.Count; i++)
                    {
                        Response.Write(tab + dr[i].ToString());
                        tab = "\t";
                    }
                    Response.Write("\n");
                }
                Response.End();

                
            }
            blPaicma.Termina();
        }

    }

    protected void btnImport_Click(object sender, EventArgs e)
    {
        try
        {
            //Upload and save the file
            string path = string.Concat(Server.MapPath("~/Archivos/" + FileUpload1.FileName)); 
            FileUpload1.SaveAs(path);
 
            // Connection String to Excel Workbook
            string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = excelConnectionString;
            OleDbCommand command = new OleDbCommand("select * from [MatrizVictimas$]", connection);
            connection.Open();
            // Create DbDataReader to Data Worksheet
            DbDataReader dr = command.ExecuteReader();
 
            // SQL Server Connection String
            string sqlConnectionString = ConfigurationManager.ConnectionStrings["PAICMA"].ConnectionString;
 
            // Bulk Copy to SQL Server 
            SqlBulkCopy bulkInsert = new SqlBulkCopy(sqlConnectionString);
            bulkInsert.DestinationTableName = "Victima";
            bulkInsert.WriteToServer(dr);
            lblError.Text = "Archivo importado correctamente.";

        }catch(Exception ex)
        {
            lblError.Text = ex.Message;
            Console.Write(ex.Message);
        }


    }

}