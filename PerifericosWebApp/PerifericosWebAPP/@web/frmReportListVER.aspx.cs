﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmReportListVER : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }

            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                this.fntCargarGrillaReportesPermitidos();
                return;
            }
            base.Response.Redirect("@dmin/frmLogin.aspx");
        }
    }


    public void fntCargarGrillaReportesPermitidos()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {

            // CONSULTAR SOLO LOS PERMITIDOS POR EL USUARIO
            if (this.blPaicma.fntConsultaReporteXUsuario("strReportesListadosPermitidos", 0, 0, 0, Convert.ToInt32(Session["idUsuario"])))
            {

                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gridReportesVer.DataMember = "strReportesListadosPermitidos";
                    this.gridReportesVer.DataSource = this.blPaicma.myDataSet;
                    this.gridReportesVer.DataBind();
                    //  this.lblErrorGv.Text = string.Empty;
                }
                else
                {
                    this.gridReportesVer.DataMember = "strReportesListadosPermitidos";
                    this.gridReportesVer.DataSource = this.blPaicma.myDataSet;
                    this.gridReportesVer.DataBind();
                    //this.lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }

            }
            this.blPaicma.Termina();
        }
    }
    protected void gridReportesVer_PreRender(object sender, EventArgs e)
    {
        if (gridReportesVer.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gridReportesVer.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gridReportesVer.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gridReportesVer.FooterRow.TableSection = TableRowSection.TableFooter;
        }

    }
    protected void gridReportesVer_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "verReporte")
        {
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gridReportesVer.Rows[intIndex];
            TableCell Item = selectedRow.Cells[0];
            TableCell Item1 = selectedRow.Cells[1];
            int intIdTabla = Convert.ToInt32(Item.Text);
            string NombreReporte = Item1.Text.Trim();
            Session["idReporteBuscar"] = Convert.ToInt32(intIdTabla);
            Session["NombreReporteSeleccionado"] = NombreReporte;
            string strRuta = "/@web/GenericReport.aspx";
            Response.Redirect(strRuta);
        }
    }
}