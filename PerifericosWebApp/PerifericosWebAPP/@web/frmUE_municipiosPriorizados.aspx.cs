﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

public partial class _web_frmUE_municipiosPriorizados : System.Web.UI.Page
{
    ///private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        fntCargarArchivo();
        Response.Redirect("frmCUnionEuropea.aspx");
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
       // blPaicma = null;
    }

    public void fntCargarArchivo()
    {
        string strNomSP = "SP_ue1";
        string strIdDatosPriorizados = string.Empty;
        string strTablaTemporal = string.Empty;
        int intCantidad = 0;
        string strRutaServidor = string.Empty;

        int intCantidadCampos = 0;
        string strCampos = string.Empty;
        int intCantidadXls = 0;
        string strCamposExcel = string.Empty;
        int intMaxId = 0;
        string strNomSPReiniciar = string.Empty;


        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            strTablaTemporal = "tmp_UE1";
            blPaicma.fntEliminarTablaTemporal_bol(strTablaTemporal);

            strRutaServidor = System.Configuration.ConfigurationManager.AppSettings["RutaServerDB"];
            strRutaServidor = strRutaServidor + Session["strRutaCompleta"].ToString();

            try
            {
                if (blPaicma.fntConsultaCampoTablas("strDsCamposTablas", strTablaTemporal))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        intCantidadCampos = blPaicma.myDataSet.Tables[0].Rows.Count;
                        for (int i = 0; i < intCantidadCampos; i++)
                        {
                            if (i != 0)
                            {
                                if ((i + 1) != intCantidadCampos)
                                {
                                    strCampos = strCampos + blPaicma.myDataSet.Tables[0].Rows[i][1].ToString() + " ,";
                                }
                                else
                                {
                                    strCampos = strCampos + blPaicma.myDataSet.Tables[0].Rows[i][1].ToString();
                                }

                            }
                        }
                    }
                }

                  intCantidadCampos = intCantidadCampos - 1;
                string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strRutaServidor + ";Extended Properties=Excel 12.0;";
                OleDbConnection conn = new OleDbConnection(strConn);
                conn.Open();
                string strExcel = "";
                OleDbDataAdapter myCommand = null;
                DataSet ds = null;
                strExcel = "select * from [Hoja1$]";
                myCommand = new OleDbDataAdapter(strExcel, strConn);
                ds = new DataSet();
                myCommand.Fill(ds, "table1");
                intCantidadXls = ds.Tables[0].Rows.Count;
                conn.Close();

                for (int i = 0; i < intCantidadXls; i++)
                {
                    strCamposExcel = string.Empty;
                    for (int j = 0; j < intCantidadCampos; j++)
                    {
                        if ((j + 1) != intCantidadCampos)
                        {
                            strCamposExcel = strCamposExcel + "'" + ds.Tables[0].Rows[i][j].ToString() + "' ,";
                        }
                        else
                        {
                            strCamposExcel = strCamposExcel + "'" + ds.Tables[0].Rows[i][j].ToString() + "'"; ;
                        }

                    }

                    if (blPaicma.fntConsultaMaxIdTablaDinamica_bol("strDsId", strTablaTemporal))
                    {
                        intMaxId = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                    }
                    if (intMaxId == 1)
                    {
                        strNomSPReiniciar = "SP_ReiniciarIndice ";
                        blPaicma.ReiniciaIndice(strNomSPReiniciar, strTablaTemporal);


                        // lanzo sp_reiniciarIndice
                    }
                    string sw = string.Empty;
                    if (blPaicma.fntIngresarTablaDinamica2_bol(strCampos, strTablaTemporal, strCamposExcel, intMaxId))
                    {
                        sw = "OK";
                    }
                    else
                    {
                        Session["Resultado"] = "NO";
                        Session["ESTRUCTURA"] = "NO";
                        Session["PELIGRO"] = "NA";
                        break;
                    }

                }


                Session["ESTRUCTURA"] = null;
                if (blPaicma.fntValidarSeguridadUE1("strDsUE1"))
                {
                    intCantidad = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                    if (intCantidad > 0)
                    {
                        ///el archivo no es seguro, tiene palabras reservadas del motor;
                        Session["Resultado"] = "NO";
                        Session["PELIGRO"] = "OK";
                    }
                    else
                    {
                        ///el archivo es seguro, no tiene palabras reservadas del motor;
                        Session["PELIGRO"] = null;
                        strIdDatosPriorizados = blPaicma.CargarArchivo(strNomSP);
                        if (strIdDatosPriorizados != "")
                        {
                            fntCargarMunicipiosPriorizados(strIdDatosPriorizados);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strError = ex.ToString();
                Session["Resultado"] = "NO";
                Session["ESTRUCTURA"] = "NO";
                Session["PELIGRO"] = "NA";
                blPaicma.Termina();
            }

            blPaicma.Termina();
        }
        blPaicma = null;
        System.IO.File.Delete(Session["strRutaCompleta"].ToString());
    }

    public void fntCargarMunicipiosPriorizados(string strIdDatosPriorizados)
    {
        string strCriterio = "";
        int intSw = 0;
        int intIndice = 0;
        int intI = 0;
        int intCantidadRegistros = 0;
        int intContador = 0;
        int intLongitud = 0;
        string strCodigoMunicipio = "";

        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTmpMunicipiosPriorizados("strDsMunicipiosPriorizados"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    while (intSw == 0)
                    {
                        strCriterio = blPaicma.myDataSet.Tables[0].Rows[intI]["ue2"].ToString();
                        if (strCriterio == "MUNICIPIO")
                        {
                            intSw = 1;
                            intIndice = intI + 1;
                            intCantidadRegistros = blPaicma.myDataSet.Tables[0].Rows.Count;
                            intLongitud = ((intCantidadRegistros - intIndice));
                            string[] strMunicipios = new string[intLongitud];
                            for (int i = intIndice; i < intCantidadRegistros; i++)
                            {
                                strMunicipios[intContador] = blPaicma.myDataSet.Tables[0].Rows[i][3].ToString();
                                intContador = intContador + 1;
                            }

                            for (int j = 0; j < intLongitud; j++)
                            {
                                if (blPaicma.fntConsultaMunicipios("strDsMunicipio", 0,strMunicipios[j].ToString()))
                                {
                                    strCodigoMunicipio = blPaicma.myDataSet.Tables[0].Rows[0][0].ToString();
                                    if (blPaicma.fntIngresarMunicipioPriorizado_bol(strCodigoMunicipio, Convert.ToInt32(strIdDatosPriorizados)))
                                    {
                                     
                                        Session["Resultado"] = "OK";
                                    }
                                }   
                            }
                        }
                        else
                        {
                            intI = intI + 1;
                        }
                    }
                }
                else
                {
                    Session["Resultado"] = "NO";
                }

            }
            blPaicma.Termina();
        }
        blPaicma = null;
    }



}