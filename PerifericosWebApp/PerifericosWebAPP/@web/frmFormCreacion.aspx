﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_web_frmFormCreacion" Codebehind="frmFormCreacion.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">

    <asp:Panel ID="pnlTitulo" runat="server" Width="90%">
      
    </asp:Panel>

    <%-- <asp:Panel ID="pnlEliminacion" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblEliminacion" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSi" runat="server"  Text="SI" onclick="btnSi_Click" />
                        <asp:Button ID="btnNo" runat="server"  Text="NO" onclick="btnNo_Click" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>--%>

    <asp:Panel ID="pnlAccionesCargaDinamica" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbNuevo" runat="server" ImageUrl="~/Images/Nuevo.png"
                ToolTip="Nuevo" OnClick="imgbNuevo_Click" Style="height: 32px" />
          <%--  <asp:ImageButton ID="imgbGuardar" runat="server" ImageUrl="~/Images/guardar.png" OnClick="imgbGuardar_Click" />--%>
            <asp:ImageButton ID="imgbCancelar" runat="server" ImageUrl="~/Images/cancelar.png" OnClick="imgbCancelar_Click" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlNuevaCarga" runat="server">
        <div class="formularioint2 contacto">
            <table class="" style="width:95%;">
                 <tr>
                    <td colspan="4">
                        <h1>Formularios Dinamicos: Carga</h1>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Filtro Organizacional"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlOpcionesFiltroOrg" runat="server" Width="450px" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlOpcionesFiltroOrg_SelectedIndexChanged" Enabled="true">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="Filtro por Departamento"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlOpcionesFiltroDep" runat="server" Width="450px" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlOpcionesFiltroDep_SelectedIndexChanged" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="Label5" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblOpcion" runat="server" Text="Seleccione la tabla a cargar:"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlOpciones" runat="server" Width="450px" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlOpciones_SelectedIndexChanged" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="lblMensaje" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>

                
                <tr>

                    <td colspan="2">
                      

                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>

            

  <%--<table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
                <th>Salary</th>
            </tr>
        </thead>
 
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
                <th>Salary</th>
            </tr>
        </tfoot>
 
        <tbody>
        
            <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
                <td>$320,800</td>
            </tr>
            <tr>
                <td>Garrett Winters</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>63</td>
                <td>2011/07/25</td>
                <td>$170,750</td>
            </tr>
          </tbody>
         </table>--%>
        </div>
        <div class="contacto2" runat="server" id="DivTable" visible="false" style="width:88%">
            <h2 style="text-align:left">Datos en la tabla</h2>
            <table style="width:100%; margin-left:0px" class="contacto">
                <tr>
                    <td colspan="5">Filtrar por fecha de creacion</td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label6" runat="server" Text="Fecha Desde:"></asp:Label></td>
                    <td> <asp:TextBox ID="txtFechaDesde" runat="server" MaxLength="50"></asp:TextBox></td>
                    <td><asp:Label ID="Label7" runat="server" Text="Fecha Hasta:"></asp:Label></td>
                    <td> <asp:TextBox ID="txtFechaHasta" runat="server" MaxLength="50"></asp:TextBox></td>
                    <td>  <asp:ImageButton ID="imgbBuscar" runat="server"
                ImageUrl="~/Images/BuscarAccion.png" ToolTip="Buscar"
                OnClick="imgbBuscar_Click" /></td>
                </tr>
            </table>
            <hr />
          <div style="width: 100%; height: 400px; overflow: scroll; max-width:700px" >
                            <asp:GridView ID="gvDatos" runat="server"  ForeColor="Black" PageSize="3" onrowcommand="gvDatos_RowCommand" CssClass=""
                            AlternatingRowStyle-CssClass="alt"  OnPreRender="gvDatos_PreRender" AllowPaging="False" >
                                <Columns>
                                    <asp:ButtonField ButtonType="Button" CommandName="DetalleFormulario" Text="Editar" />
                                </Columns>
                                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                            <asp:Panel ID="PanelFormulario" runat="server">
                                <asp:Table ID="CustomUITable" runat="server">
                                </asp:Table>
                            </asp:Panel>
                        </div>
            </div>
    </asp:Panel>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
        $('#' + '<%= gvDatos.ClientID %>').DataTable();   
        $('#' + '<%= txtFechaDesde.ClientID %>').datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
        $('#' + '<%= txtFechaHasta.ClientID %>').datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
    } );
   </script>
</asp:Content>
