﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraCharts.GLGraphics;
using DevExpress.XtraCharts.Commands;
using DevExpress.XtraCharts.Design;
using DevExpress.XtraCharts.Localization;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts.Printing;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Web;

//using DevExpress.DataAccess.ConnectionParameters;
//using DevExpress.DataAccess.Sql;


public partial class _web_PruebaReporteDEV141 : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }
            else
            {

            }

        }
    }

    // Create a report from a stream. 
    private XtraReport CreateReportFromStream(MemoryStream stream)
    {
        XtraReport report = XtraReport.FromStream(stream, true);
        return report;
    }


    private void MyDataSourceDemandedHandler(object sender, EventArgs e)
    {
        var CadenaDeConexion = System.Configuration.ConfigurationManager.ConnectionStrings["PAICMA"].ConnectionString;

        var sqlConnection = new SqlConnection(CadenaDeConexion);
        var sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlConnection.ConnectionString);
        var dataConnectionParametersBase = new DevExpress.DataAccess.ConnectionParameters.MsSqlConnectionParameters(sqlConnectionStringBuilder.DataSource, sqlConnectionStringBuilder.InitialCatalog, sqlConnectionStringBuilder.UserID, sqlConnectionStringBuilder.Password, sqlConnectionStringBuilder.IntegratedSecurity ?
                    DevExpress.DataAccess.ConnectionParameters.MsSqlAuthorizationType.Windows : DevExpress.DataAccess.ConnectionParameters.MsSqlAuthorizationType.SqlServer);
        var dataSource = (DevExpress.DataAccess.Sql.SqlDataSource)((XtraReport)sender).DataSource;
        if (dataSource != null)
        {
            dataSource.Connection.ConnectionString = CadenaDeConexion;
            dataSource.ConnectionParameters = dataConnectionParametersBase;
            ((XtraReport)sender).DataSource = dataSource;
        }
    }

    protected void ASPxDocumentViewer1_Load(object sender, EventArgs e)
    {

        int idReporteCargar = Convert.ToInt32(Session["idReporteBuscar"]);
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaReporte("strReportesListados", 1, idReporteCargar, 0))
            {
              try{
                byte[] recuperar = (byte[])this.blPaicma.myDataSet.Tables[0].Rows[0]["reporteBinario"];
                MemoryStream stream = new MemoryStream(recuperar);
                XtraReport report = new XtraReport();
                report = CreateReportFromStream(stream);
                var CadenaDeConexion = System.Configuration.ConfigurationManager.ConnectionStrings["PAICMA"].ConnectionString;

                var sqlConnection = new SqlConnection(CadenaDeConexion);
                var sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlConnection.ConnectionString);
                var dataConnectionParametersBase = new DevExpress.DataAccess.ConnectionParameters.MsSqlConnectionParameters(sqlConnectionStringBuilder.DataSource, sqlConnectionStringBuilder.InitialCatalog, sqlConnectionStringBuilder.UserID, sqlConnectionStringBuilder.Password, sqlConnectionStringBuilder.IntegratedSecurity ?
                            DevExpress.DataAccess.ConnectionParameters.MsSqlAuthorizationType.Windows : DevExpress.DataAccess.ConnectionParameters.MsSqlAuthorizationType.SqlServer);
                // var dataSource = (DevExpress.DataAccess.Sql.SqlDataSource)((XtraReport)sender).DataSource;
                var dataSource = (DevExpress.DataAccess.Sql.SqlDataSource)report.DataSource;
                if (dataSource != null)
                {
                    dataSource.Connection.ConnectionString = CadenaDeConexion;
                    dataSource.ConnectionParameters = dataConnectionParametersBase;
                    report.DataSource = dataSource;
                    //((XtraReport)sender).DataSource = dataSource;
                }
                ASPxDocumentViewer1.Report = report;
                ASPxDocumentViewer1.DataBind();
              }
              catch (Exception ex) {
                  lblError.Text = "El reporte no se ha podido cargar, intente nuevamente - " + ex.Message;
                  lblError.Style.Add("background-color", "#FBEE57");
                  lblError.Visible = true;
              }
            }
            else
            {
                lblError.Text = "El reporte no se ha podido cargar, intente nuevamente.";
                lblError.Style.Add("background-color", "#FBEE57");
                lblError.Visible = true;
            }

        }
        else
        {
            lblError.Text = "Problemas para acceder a la base de datos, comuniquese con el administrador";
            lblError.Style.Add("background-color", "#FBEE57");
            lblError.Visible = true;
        }
        blPaicma.Termina();


    }

}