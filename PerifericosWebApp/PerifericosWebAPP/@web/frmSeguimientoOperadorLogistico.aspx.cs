﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _web_frmSeguimientoOperadorLogistico : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                ftnValidarPermisos();
                fntCargarGrillaEventoTaller(0,string.Empty, string.Empty, 0, string.Empty, string.Empty, 0, string.Empty);
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }



    public void ftnValidarPermisos()
    {
        //int intIdFormulario = 0;
        string strBuscar = string.Empty;
        string strNuevo = string.Empty;
        string strEditar = string.Empty;
        string strEliminar = string.Empty;

        //obtiene el nombre de la pagina actual
        string[] strRutaPagina = HttpContext.Current.Request.RawUrl.Split('/');
        string strNombrePagina = strRutaPagina[strRutaPagina.GetUpperBound(0)];
        strRutaPagina = strNombrePagina.Split('?');
        strNombrePagina = strRutaPagina[strRutaPagina.GetLowerBound(0)];
        //fin obtiene el nombre de la pagina actual


        if (strNombrePagina != string.Empty)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {

                //intIdFormulario = Convert.ToInt32(Request.QueryString["op"].ToString());
                //Session["intIdFormulario"] = intIdFormulario;
                if (blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(Session["IdUsuario"].ToString()), strNombrePagina))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strBuscar = blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                        strNuevo = blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                        strEditar = blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                        strEliminar = blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();

                        if (strBuscar == "False")
                        {
                            imgbBuscar.Visible = false;
                        }
                        if (strNuevo == "False")
                        {
                            imgbNuevo.Visible = false;
                        }
                        if (strEditar == "False")
                        {
                            imgbEditar.Visible = false;
                            imgbGravar.Visible = false;
                        }
                        if (strEliminar == "False")
                        {
                            imgbEliminar.Visible = false;
                        }

                    }
                }

                blPaicma.Termina();
            }
        }
        else
        {
            Response.Redirect("@dmin/frmLogin.aspx");
        }
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        string strRuta = "frmSeguimientoOperadorLogistico.aspx";
        Response.Redirect(strRuta);
    }

    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        pnlAccionesSegOperadorLogistico.Visible = true;
        imgbEncontrar.Visible = false;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        pnlSegOperadorLogistico.Visible = true;
        pnlMenuSegOperadorLogistico.Visible = false;
        fntCargarObjetos();
        PnlEventoTaller.Visible = false;
        fntOcultarObjetos(true);
        Session["Operacion"] = "Crear";
    }

    public void fntCargarObjetos()
    {
        fntCargarDepartamento();
        fntCargarResponsable();
        fntCargarTaller();
    }

    public void fntCargarDepartamento()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaDepartamento_bol("strDsDepartamento", 1, ""))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlDepartamento.DataMember = "strDsDepartamento";
                    ddlDepartamento.DataSource = blPaicma.myDataSet;
                    ddlDepartamento.DataValueField = "codDepartamentoAlf2";
                    ddlDepartamento.DataTextField = "nomDepartamento";
                    ddlDepartamento.DataBind();
                    ddlDepartamento.Items.Insert(ddlDepartamento.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        fntCargarMunicipio();
    }

    public void fntCargarMunicipio()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaMunicipios("strDsMunicipio", 1, ddlDepartamento.SelectedValue))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlMunicipio.DataMember = "strDsMunicipio";
                    ddlMunicipio.DataSource = blPaicma.myDataSet;
                    ddlMunicipio.DataValueField = "codMunicipioAlf5";
                    ddlMunicipio.DataTextField = "nomMunicipio";
                    ddlMunicipio.DataBind();
                    ddlMunicipio.Items.Insert(ddlMunicipio.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarResponsable()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaResponsables("strDsResponsable"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlResponsable.DataMember = "strDsResponsable";
                    ddlResponsable.DataSource = blPaicma.myDataSet;
                    ddlResponsable.DataValueField = "res_Id";
                    ddlResponsable.DataTextField = "nombrePersona";
                    ddlResponsable.DataBind();
                    ddlResponsable.Items.Insert(ddlResponsable.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarTaller()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTaller("strDsTaller"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlTaller.DataMember = "strDsTaller";
                    ddlTaller.DataSource = blPaicma.myDataSet;
                    ddlTaller.DataValueField = "tal_Id";
                    ddlTaller.DataTextField = "tal_Nombre";
                    ddlTaller.DataBind();
                    ddlTaller.Items.Insert(ddlTaller.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        string strRuta = "frmSeguimientoOperadorLogistico.aspx";
        Response.Redirect(strRuta);
    }

    public Boolean ftnValidarCampos_bol()
    {
        if (ddlDepartamento.SelectedValue == "Seleccione...")
        {
            lblError.Text = "Debe seleccionar un departamento";
            return false;
        }

        if (ddlMunicipio.SelectedValue == "Seleccione...")
        {
            lblError.Text = "Debe seleccionar un municipio";
            return false;
        }

        if (ddlTaller.SelectedValue == "Seleccione...")
        {
            lblError.Text = "Debe seleccionar un taller";
            return false;
        }
        
        if (txtFechaTaller.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe seleccionar la fecha en la que se realizó el taller.";
            return false;
        }

        if (txtObjeto.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar el objeto del taller.";
            return false;
        }

        if (ddlResponsable.SelectedValue == "Seleccione...")
        {
            lblError.Text = "Debe seleccionar el responsable";
            return false;
        }

        if (txtOperadorLogistico.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar el nombre del operador logístico.";
            return false;
        }

        if (txtValorAntesIva.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar el valor de la factura antes de IVA.";
            return false;
        }

        if (txtValorIva.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar el valor del IVA.";
            return false;
        }

        if (txtValorComision.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar el valor de la comisión.";
            return false;
        }

        if (txtValorTotal.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar el valor total de la factura.";
            return false;
        }

        if (txtValorSaldo.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar el saldo.";
            return false;
        }

        if (txtFactura.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar el Número de la factura.";
            return false;
        }

        if (txtFechaPago.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe seleccionar la fecha de pago.";
            return false;
        }

        if (txtOrdenPagoSIIF.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar el número de ordern de pago de SIIF.";
            return false;
        }

        if (txtActa.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar el acta.";
            return false;
        }

        if (txtFechaActa.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe seleccionar la fecha del acta.";
            return false;
        }

        return true;
    }

    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["Operacion"].ToString() == "Crear")
        {
            if (ftnValidarCampos_bol())
            {
                int intIdEventoTaller = 0;
                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    if (blPaicma.fntIngresarEventoTaller_bol(ddlMunicipio.SelectedValue.ToString(), Convert.ToInt32(ddlTaller.SelectedValue.ToString()), txtFechaTaller.Text.Trim(), txtObjeto.Text.Trim(), Convert.ToInt32(ddlResponsable.SelectedValue.ToString()), txtOperadorLogistico.Text.Trim()))
                    {
                        if (blPaicma.fntConsultaEventoTaller("strDsEventoTaller", ddlMunicipio.SelectedValue.ToString(), Convert.ToInt32(ddlTaller.SelectedValue.ToString()), txtFechaTaller.Text.Trim(), txtObjeto.Text.Trim(), Convert.ToInt32(ddlResponsable.SelectedValue.ToString())))
                        {
                            if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                            {
                                intIdEventoTaller = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                                if (blPaicma.fntIngresarValorTaller_bol(intIdEventoTaller, txtValorAntesIva.Text.Trim(), txtValorIva.Text.Trim(), txtValorComision.Text.Trim(), txtValorTotal.Text.Trim(), txtValorSaldo.Text.Trim(), txtFactura.Text.Trim(), txtFechaPago.Text.Trim(), txtOrdenPagoSIIF.Text.Trim(), txtActa.Text.Trim(), txtFechaActa.Text.Trim()))
                                {
                                    fntInactivarPaneles();
                                    lblRespuesta.Text = "Registro creado satisfactoriamente";
                                }
                            }
                        }
                    }
                    else
                    {
                        fntInactivarPaneles();
                        lblRespuesta.Text = "Problema al ingresar el registro, por favor comuníquese con el administrador.";
                    }                
                    blPaicma.Termina();
                }
            }

        }
        else if (Session["Operacion"].ToString() == "Actualizar")
        {
            if (fntActualizarValorTaller_bol())
            {
                fntInactivarPaneles();
                lblRespuesta.Text = "Registro actualizado satisfactoriamente";
            }
            else
            {
                fntInactivarPaneles();
                lblRespuesta.Text = "Problema al actualizado el registro, por favor comuníquese con el administrador.";

            }
        }
    }

    public void fntInactivarPaneles()
    {
        pnlRespuesta.Visible = true;
        pnlAccionesSegOperadorLogistico.Visible = false;
        pnlMenuSegOperadorLogistico.Visible = false;
        pnlSegOperadorLogistico.Visible = false;
    }

    public void fntCargarGrillaEventoTaller(int intOP, string strIdDepartamento, string strIdMunicipio, int intIdTaller, string strFechaTaller, string strObjeto, int intIdResponsable, string strOperadorLogistico)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaEventoTalleGrilla("strDsGrilleEventoTaller", intOP, strIdDepartamento, strIdMunicipio, intIdTaller, strFechaTaller, strObjeto, intIdResponsable, strOperadorLogistico,0))
            {
                gvEventoTaller.Columns[0].Visible = true;
                gvEventoTaller.Columns[1].Visible = true;

                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvEventoTaller.DataMember = "strDsGrilleEventoTaller";
                    gvEventoTaller.DataSource = blPaicma.myDataSet;
                    gvEventoTaller.DataBind();
                }
                gvEventoTaller.Columns[0].Visible = false;
                gvEventoTaller.Columns[1].Visible = false;
            }
            blPaicma.Termina();
        }
    }

    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        pnlAccionesSegOperadorLogistico.Visible = true;
        pnlMenuSegOperadorLogistico.Visible = false;
        imgbEncontrar.Visible = true;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = false;
        imgbCancelar.Visible = true;
        pnlSegOperadorLogistico.Visible = true;
        PnlEventoTaller.Visible = false;
        fntCargarObjetos();
        ddlMunicipio.ClearSelection();
        fntLimpiarObjetos();
        fntOcultarObjetos(false);
    }

    public void fntLimpiarObjetos()
    {
        txtFechaTaller.Text = string.Empty;
        txtObjeto.Text = string.Empty;
        txtOperadorLogistico.Text = string.Empty;
        txtValorAntesIva.Text = string.Empty;
        txtValorIva.Text = string.Empty;
        txtValorComision.Text = string.Empty;
        txtValorTotal.Text = string.Empty;
        txtValorSaldo.Text = string.Empty;
        txtFactura.Text = string.Empty;
        txtFechaPago.Text = string.Empty;
        txtOrdenPagoSIIF.Text = string.Empty;
        txtActa.Text = string.Empty;
        txtFechaActa.Text = string.Empty;
    }

    public void fntOcultarObjetos(Boolean bolValor)
    {
        lblValorFacturaSinIva.Visible = bolValor;
        txtValorAntesIva.Visible = bolValor;
        lblIva.Visible = bolValor;
        txtValorIva.Visible = bolValor;
        lblValorComision.Visible = bolValor;
        txtValorComision.Visible = bolValor;
        lblValorTotal.Visible = bolValor;
        txtValorTotal.Visible = bolValor;
        lblValorSaldo.Visible = bolValor;
        txtValorSaldo.Visible = bolValor;
    }

    protected void imgbEncontrar_Click(object sender, ImageClickEventArgs e)
    {
        string strIdDepartamento = string.Empty;
        string strIdMunicipio = string.Empty;
        int intIdTaller = 0;
        string strFechaTaller = string.Empty;
        string strObjeto = string.Empty;
        int intIdResponsable = 0;
        string strOperadorLogistico = string.Empty;

        if (ddlDepartamento.SelectedValue != "Seleccione...")
        {
            strIdDepartamento = ddlDepartamento.SelectedValue.ToString();
        }

        if (ddlMunicipio.SelectedValue != "Seleccione...")
        {
            strIdMunicipio = ddlMunicipio.SelectedValue.ToString();
        }

        if (ddlTaller.SelectedValue != "Seleccione...")
        {
            intIdTaller = Convert.ToInt32(ddlTaller.SelectedValue.ToString());
        }

        if (txtFechaTaller.Text.Trim() != string.Empty)
        {
            strFechaTaller = txtFechaTaller.Text.Trim();
        }

        if (txtObjeto.Text.Trim() != string.Empty)
        {
            strObjeto = txtObjeto.Text.Trim();
        }

        if (ddlResponsable.SelectedValue != "Seleccione...")
        {
            intIdResponsable = Convert.ToInt32(ddlResponsable.SelectedValue.ToString());
        }

        if (txtOperadorLogistico.Text.Trim() != string.Empty)
        {
            strOperadorLogistico = txtOperadorLogistico.Text.Trim();
        }

      

        //if (txtFactura.Text.Trim() == string.Empty)
        //{
        //    lblError.Text = "Debe indicar el Número de la factura.";
        //    return false;
        //}

        //if (txtFechaPago.Text.Trim() == string.Empty)
        //{
        //    lblError.Text = "Debe seleccionar la fecha de pago.";
        //    return false;
        //}

        //if (txtOrdenPagoSIIF.Text.Trim() == string.Empty)
        //{
        //    lblError.Text = "Debe indicar el número de ordern de pago de SIIF.";
        //    return false;
        //}

        //if (txtActa.Text.Trim() == string.Empty)
        //{
        //    lblError.Text = "Debe indicar el acta.";
        //    return false;
        //}

        //if (txtFechaActa.Text.Trim() == string.Empty)
        //{
        //    lblError.Text = "Debe seleccionar la fecha del acta.";
        //    return false;
        //}

        fntCargarGrillaEventoTaller(1, strIdDepartamento, strIdMunicipio, intIdTaller, strFechaTaller, strObjeto, intIdResponsable, strOperadorLogistico);
        pnlAccionesSegOperadorLogistico.Visible = false;
        pnlMenuSegOperadorLogistico.Visible = true;
        pnlSegOperadorLogistico.Visible = false;
        PnlEventoTaller.Visible = true;

        
    }

    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        pnlAccionesSegOperadorLogistico.Visible = true;
        imgbEncontrar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        pnlMenuSegOperadorLogistico.Visible = false;
        pnlSegOperadorLogistico.Visible = true;
        PnlEventoTaller.Visible = false;
        fntCargarObjetos();
        fntOcultarObjetos(true);
        Session["Operacion"] = "Actualizar";

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {

            if (blPaicma.fntConsultaEventoTalleGrilla("strDsGrillaDirectorio",2,string.Empty, string.Empty,0,string.Empty,string.Empty,0,string.Empty, Convert.ToInt32(Session["intIdEventoTaller"].ToString())))
            {
                string strIdMunicipio = string.Empty;
                string strFechaAux = string.Empty;
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlDepartamento.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                    strIdMunicipio = blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                    ddlTaller.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][6].ToString();
                    strFechaAux = blPaicma.myDataSet.Tables[0].Rows[0][8].ToString();
                    txtFechaTaller.Text = strFechaAux.Substring(0, 10).ToString();
                    txtObjeto.Text = blPaicma.myDataSet.Tables[0].Rows[0][9].ToString();
                    ddlResponsable.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][10].ToString();
                    txtOperadorLogistico.Text = blPaicma.myDataSet.Tables[0].Rows[0][22].ToString();
                    txtValorAntesIva.Text = blPaicma.myDataSet.Tables[0].Rows[0][12].ToString();
                    txtValorAntesIva.Text = txtValorAntesIva.Text.Replace(",", ".");
                    txtValorIva.Text = blPaicma.myDataSet.Tables[0].Rows[0][13].ToString();
                    txtValorIva.Text = txtValorIva.Text.Replace(",", ".");
                    txtValorComision.Text = blPaicma.myDataSet.Tables[0].Rows[0][14].ToString();
                    txtValorComision.Text = txtValorComision.Text.Replace(",", ".");
                    txtValorTotal.Text = blPaicma.myDataSet.Tables[0].Rows[0][15].ToString();
                    txtValorTotal.Text = txtValorTotal.Text.Replace(",", ".");
                    txtValorSaldo.Text = blPaicma.myDataSet.Tables[0].Rows[0][16].ToString();
                    txtValorSaldo.Text = txtValorSaldo.Text.Replace(",", ".");
                    txtFactura.Text = blPaicma.myDataSet.Tables[0].Rows[0][17].ToString();
                    strFechaAux = blPaicma.myDataSet.Tables[0].Rows[0][18].ToString();
                    txtFechaPago.Text = strFechaAux.Substring(0, 10).ToString();
                    txtOrdenPagoSIIF.Text = blPaicma.myDataSet.Tables[0].Rows[0][19].ToString();
                    txtActa.Text = blPaicma.myDataSet.Tables[0].Rows[0][20].ToString();
                    strFechaAux = blPaicma.myDataSet.Tables[0].Rows[0][21].ToString();
                    txtFechaActa.Text = strFechaAux.Substring(0, 10).ToString();
                    fntCargarMunicipio();
                    ddlMunicipio.SelectedValue = strIdMunicipio;

                }
            }
            blPaicma.Termina();
        }
    }

    protected void gvEventoTaller_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {

            gvEventoTaller.Columns[0].Visible = true;
            gvEventoTaller.Columns[1].Visible = true;

            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvEventoTaller.Rows[intIndex];
            TableCell Item = selectedRow.Cells[0];
            int intIdEventoTaller = Convert.ToInt32(Item.Text);
            Session["intIdEventoTaller"] = intIdEventoTaller;
            imgbEditar.Visible = true;

            gvEventoTaller.Columns[0].Visible = false;
            gvEventoTaller.Columns[1].Visible = false;

        }
    }

    public Boolean fntActualizarValorTaller_bol()
    { 
        Boolean bolResultadoAccion = false;
         if (blPaicma.inicializar(blPaicma.conexionSeguridad))
         {
             if (blPaicma.fntModificarEventoTaller_bol(Convert.ToInt32(Session["intIdEventoTaller"].ToString()), ddlMunicipio.SelectedValue.ToString(), Convert.ToInt32(ddlTaller.SelectedValue.ToString()), txtFechaTaller.Text.Trim(), txtObjeto.Text.Trim(), Convert.ToInt32(ddlResponsable.SelectedValue.ToString()), txtOperadorLogistico.Text.Trim()))
             {
                 if (blPaicma.fntModificarValorTaller_bol(Convert.ToInt32(Session["intIdEventoTaller"].ToString()), txtValorAntesIva.Text.Trim(), txtValorIva.Text.Trim(), txtValorComision.Text.Trim(), txtValorTotal.Text.Trim(), txtValorSaldo.Text.Trim(), txtFactura.Text.Trim(), txtFechaPago.Text.Trim(), txtOrdenPagoSIIF.Text.Trim(), txtActa.Text.Trim(), txtFechaActa.Text.Trim()))
                 {
                     bolResultadoAccion = true;
                 }
             }
             else
             {
                 bolResultadoAccion = false;
             }

             blPaicma.Termina();
         }

         return bolResultadoAccion;
    }

    protected void gvEventoTaller_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEventoTaller.Columns[0].Visible = true;
        gvEventoTaller.Columns[1].Visible = true;

        gvEventoTaller.PageIndex = e.NewPageIndex;
        fntCargarGrillaEventoTaller(0, string.Empty, string.Empty, 0, string.Empty, string.Empty, 0, string.Empty);

        gvEventoTaller.Columns[0].Visible = false;
        gvEventoTaller.Columns[1].Visible = false;

    }
    protected void imgbEliminar_Click(object sender, ImageClickEventArgs e)
    {
        int IdTallerEliminar = 0;
            IdTallerEliminar = Convert.ToInt32(Session["intIdEventoTaller"]);
            if (IdTallerEliminar > 0)
            {
                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    if (blPaicma.fntEliminarSegimientoOperador_bol(IdTallerEliminar))
                    {
                        fntInactivarPaneles();
                        lblRespuesta.Text = "Registro eliminado exitosamente.";
                    }
                    else
                    {
                        fntInactivarPaneles();
                        lblRespuesta.Text = "No se ha podido eliminar el registro, consulte a su administrador.";
                        //no se pudo eliminar consulte a su administrador.
                    }
                }
                else
                {
                    fntInactivarPaneles();
                    lblRespuesta.Text = "Problemas al conectar con la base de datos.";
                    //Problemas con la base de datos.
                }
            }
            else
            {
                fntInactivarPaneles();
                lblRespuesta.Text = "Se ha perdido el elemento a eliminar, intente de nuevo.";
            }
            blPaicma.Termina();

    }
}