﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _web_frmBuscaCaracterizacion : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {

            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                ftnValidarPermisos();                
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }


    public void ftnValidarPermisos()
    {
        //int intIdFormulario = 0;
        string strBuscar = string.Empty;
        string strNuevo = string.Empty;
        string strEditar = string.Empty;
        string strEliminar = string.Empty;

        //obtiene el nombre de la pagina actual
        string[] strRutaPagina = HttpContext.Current.Request.RawUrl.Split('/');
        string strNombrePagina = strRutaPagina[strRutaPagina.GetUpperBound(0)];
        strRutaPagina = strNombrePagina.Split('?');
        strNombrePagina = strRutaPagina[strRutaPagina.GetLowerBound(0)];
        //fin obtiene el nombre de la pagina actual


        if (strNombrePagina != string.Empty)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {

                //intIdFormulario = Convert.ToInt32(Request.QueryString["op"].ToString());
                //Session["intIdFormulario"] = intIdFormulario;
                if (blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(Session["IdUsuario"].ToString()), strNombrePagina))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strBuscar = blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                        strNuevo = blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                        strEditar = blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                        strEliminar = blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();

                        if (strBuscar == "False")
                        {
                            imgbBuscar.Visible = false;
                        }
                        if (strNuevo == "False")
                        {
                            imgbNuevo.Visible = false;
                        }
                        if (strEditar == "False")
                        {
                            imgbEditar.Visible = false;
                            imgbGravar.Visible = false;
                        }
                        if (strEliminar == "False")
                        {
                            imgbEliminar.Visible = false;
                        }

                    }
                }
                blPaicma.Termina();
            }
        }
        else
        {
            Response.Redirect("@dmin/frmLogin.aspx");
        }

    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        string strRuta = "frmBuscaCaracterizacion.aspx";
        Response.Redirect(strRuta);
    }

    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        pnlAccionesDir.Visible = true;
        imgbEncontrar.Visible = false;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        pnlDirNuevo.Visible = true;
        pnlMenuDir.Visible = false;
        PnlCaracterizacionGrilla.Visible = false;

        Session["Operacion"] = "Crear";
    }

    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        string strRuta = "frmBuscaCaracterizacion.aspx";
        Response.Redirect(strRuta);
    }


    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["Operacion"].ToString() == "Crear")
        {
            if (ftnValidarCampos_bol())
            {
                lblError.Visible = false;
                lblError.Text = string.Empty;
                if (ftnIngresarPersona())
                {
                    fntInactivarPaneles();
                    lblRespuesta.Text = "Registro creado satisfactoriamente";
                }
                else
                {
                    fntInactivarPaneles();
                    lblRespuesta.Text = "Problema al ingresar el registro, por favor comuníquese con el administrador.";
                }
            }
            else
            {
                lblError.Visible = true;
            }
        }
        else
        {
            if (Session["Operacion"].ToString() == "Actualizar")
            {
                if (ftnValidarCampos_bol())
                {
                    if (fntActualizarPersona_bol())
                    {
                        fntInactivarPaneles();
                        lblRespuesta.Text = "Registro actualizado satisfactoriamente";
                    }
                    else
                    {
                        fntInactivarPaneles();
                        lblRespuesta.Text = "Problema al actualizar el registro, por favor comuníquese con el administrador.";
                    }
                }
            }
        }
    }

    public Boolean ftnValidarCampos_bol()
    {
        if (txtNombres.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe digitar el o los nombres.";
            return false;
        }
        else
        {
            if (ftnValidarSeguridadCampos_bol(txtNombres.Text.Trim()) == false)
            {
                lblError.Text = "El campo de NOMBRE tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
                return false;
            }
        }


        if (txtApellido.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe digitar el o los apellidos.";
            return false;
        }
        else
        {
            if (ftnValidarSeguridadCampos_bol(txtApellido.Text.Trim()) == false)
            {
                lblError.Text = "El campo de APELLIDO tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
                return false;
            }
        }

        return true;
    }

    public Boolean ftnIngresarPersona()
    {
        int intIdPersona = 0;
        int intTipoIdentificacion = 0;
        int intNoIdentificacion = 0;

        Boolean bolResultadoAccion = false;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            //if (blPaicma.fntIngresarPersona_bol(Convert.ToInt32(ddlVocativo.SelectedValue.ToString()), txtNombres.Text.Trim(), txtApellido.Text.Trim(), Convert.ToInt32(ddlCargo.SelectedValue.ToString()), Convert.ToInt32(ddlCategoria.SelectedValue.ToString()), txtDireccion.Text.Trim(), Convert.ToInt32(ddlTipoDireccion.SelectedValue.ToString()), ddlMunicipio.SelectedValue.ToString(), Convert.ToInt32(ddlInstitucion.SelectedValue.ToString()), intTipoIdentificacion, intNoIdentificacion))
            //{
            //    if (blPaicma.fntConsultaPersona("strDsPersona", Convert.ToInt32(ddlVocativo.SelectedValue.ToString()), txtNombres.Text.Trim(), txtApellido.Text.Trim(), Convert.ToInt32(ddlCargo.SelectedValue.ToString()), Convert.ToInt32(ddlCategoria.SelectedValue.ToString()), txtDireccion.Text.Trim(), Convert.ToInt32(ddlTipoDireccion.SelectedValue.ToString()), ddlMunicipio.SelectedValue.ToString(), Convert.ToInt32(ddlInstitucion.SelectedValue.ToString()), 0, 0, string.Empty))
            //    {
            //        if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            //        {
            //            intIdPersona = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());

            //        }
            //        bolResultadoAccion = true;
            //    }
            //}
            //else
            //{
            //    bolResultadoAccion = false;
            //}
            blPaicma.Termina();
        }
        return bolResultadoAccion;
    }

    public void fntInactivarPaneles()
    {
        pnlRespuesta.Visible = true;
        pnlAccionesDir.Visible = false;
        pnlDirNuevo.Visible = false;
        pnlMenuDir.Visible = false;
        pnlEliminacion.Visible = false;
    }

    public void fntCargarGrillaCaracterizacion(string strIdVictima, string strNoIdentificacion, string strNombre,  string strApellido)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaCaracterizacionGrilla("strDsGrillaCaracterizacion", strIdVictima, strNoIdentificacion, strNombre, strApellido))
            {
                gvCaracterizacion.Columns[0].Visible = true;
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvCaracterizacion.DataMember = "strDsGrillaCaracterizacion";
                    gvCaracterizacion.DataSource = blPaicma.myDataSet;
                    gvCaracterizacion.DataBind();
                    lblErrorGv.Text = string.Empty;
                }
                else
                {
                    gvCaracterizacion.DataMember = "strDsGrillaCaracterizacion";
                    gvCaracterizacion.DataSource = blPaicma.myDataSet;
                    gvCaracterizacion.DataBind();
                    lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                gvCaracterizacion.Columns[0].Visible = false;
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarGrillaDetalleVictima(string strIdVictima)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaDetalleVictimaGrilla("strDsGrillaDetalleVictima", strIdVictima))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvDetalleVictima.DataMember = "strDsGrillaDetalleVictima";
                    gvDetalleVictima.DataSource = blPaicma.myDataSet;
                    gvDetalleVictima.DataBind();
                    lblDetalleVictima.Text = string.Empty;
                }
                else
                {
                    gvDetalleVictima.DataMember = "strDsGrillaDetalleVictima";
                    gvDetalleVictima.DataSource = blPaicma.myDataSet;
                    gvDetalleVictima.DataBind();
                    lblDetalleVictima.Text = "No hay registros que coincidan con esos criterios.";
                }
            }
            blPaicma.Termina();
        }
    }
    public void fntCargarDetalleCaracterizacion(int idEncuesta)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaEncuestaGrilla("strDsDetalleCaracterizacion", idEncuesta))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvDetalleCaracterizacion.DataMember = "strDsDetalleCaracterizacion";
                    gvDetalleCaracterizacion.DataSource = blPaicma.myDataSet;
                    gvDetalleCaracterizacion.DataBind();
                    lblDetalleCaracterizacion.Text = string.Empty;
                }
                else
                {
                    gvDetalleCaracterizacion.DataMember = "strDsDetalleCaracterizacion";
                    gvDetalleCaracterizacion.DataSource = blPaicma.myDataSet;
                    gvDetalleCaracterizacion.DataBind();
                    lblDetalleCaracterizacion.Text = "No hay registros que coincidan con esos criterios.";
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarDetalleEncuestaCaracterizacion(int idEncuesta)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaDetalleEncuestaGrilla("strDsDetalleEncuestaCaracterizacion", idEncuesta))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvDetalleEncuestaCaracterizacion.DataMember = "strDsDetalleEncuestaCaracterizacion";
                    gvDetalleEncuestaCaracterizacion.DataSource = blPaicma.myDataSet;
                    gvDetalleEncuestaCaracterizacion.DataBind();
                    lblDetalleEncuestaCaracterizacion.Text = string.Empty;
                }
                else
                {
                    gvDetalleEncuestaCaracterizacion.DataMember = "strDsDetalleEncuestaCaracterizacion";
                    gvDetalleEncuestaCaracterizacion.DataSource = blPaicma.myDataSet;
                    gvDetalleEncuestaCaracterizacion.DataBind();
                    lblDetalleEncuestaCaracterizacion.Text = "No hay registros que coincidan con esos criterios.";
                }
            }
            blPaicma.Termina();
        }
    }
    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        pnlAccionesDir.Visible = true;
        pnlMenuDir.Visible = false;
        imgbEncontrar.Visible = true;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = false;
        imgbCancelar.Visible = true;
        pnlDirNuevo.Visible = false;
        pnlMenuDir.Visible = false;
        PnlCaracterizacionGrilla.Visible = false;
        pnlDirNuevo.Visible = true;
        fntLimpiarObjetos();

    }

    protected void imgbEncontrar_Click(object sender, ImageClickEventArgs e)
    {
        string strIdVictima = string.Empty;
        string strNoIdentificacion = string.Empty;
        string strNombre = string.Empty;
        string strApellido = string.Empty;

        if (txtIdVictima.Text.Trim() != string.Empty)
        {
            strIdVictima = txtIdVictima.Text.Trim();
        }

        if (txtNoIdentificacion.Text.Trim() != string.Empty)
        {
            strNoIdentificacion = txtNoIdentificacion.Text.Trim();
        }

        if (txtNombres.Text.Trim() != string.Empty)
        {
            strNombre = txtNombres.Text.Trim();
        }

        if (txtApellido.Text.Trim() != string.Empty)
        {
            strApellido = txtApellido.Text.Trim();
        }

        gvDetalleCaracterizacion.DataSource = null;
        gvDetalleEncuestaCaracterizacion.DataSource = null;
        gvDetalleCaracterizacion.DataBind();
        gvDetalleEncuestaCaracterizacion.DataBind();

        fntCargarGrillaCaracterizacion(strIdVictima,strNoIdentificacion, strNombre, strApellido);
        pnlAccionesDir.Visible = false;
        pnlMenuDir.Visible = true;
        pnlDirNuevo.Visible = false;
        PnlCaracterizacionGrilla.Visible = true;

    }

    public void fntLimpiarObjetos()
    {
        txtNombres.Text = string.Empty;
        txtApellido.Text = string.Empty;
    }

    protected void gvCaracterizacion_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvCaracterizacion.Rows[intIndex];
            
            //Consulta Detalle Victima
            TableCell ItemV = selectedRow.Cells[1];
            fntCargarGrillaDetalleVictima(ItemV.Text);

            //Consulta carcterizacion
            TableCell Item = selectedRow.Cells[0];
            if(Item.Text != "&nbsp;")
            {
                int intIdencuesta = Convert.ToInt32(Item.Text);
                fntCargarDetalleCaracterizacion(intIdencuesta);
                fntCargarDetalleEncuestaCaracterizacion(intIdencuesta);

            }

        }
    }

    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        pnlAccionesDir.Visible = true;
        imgbEncontrar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        pnlDirNuevo.Visible = true;
        pnlMenuDir.Visible = false;
        PnlCaracterizacionGrilla.Visible = false;

        string strTipoIdentificacion = string.Empty;
        Session["Operacion"] = "Actualizar";

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaPersonaGrilla("strDsGrillaCaracteriza", 2, 0, 0, 0, string.Empty, string.Empty, 0, string.Empty, string.Empty, Convert.ToInt32(Session["intIdPersona"].ToString())))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    string strIdMunicipio = string.Empty;

                    txtNombres.Text = blPaicma.myDataSet.Tables[0].Rows[0][7].ToString();
                    txtApellido.Text = blPaicma.myDataSet.Tables[0].Rows[0][8].ToString();

                    strIdMunicipio = blPaicma.myDataSet.Tables[0].Rows[0][13].ToString();

                    txtNoIdentificacion.Text = blPaicma.myDataSet.Tables[0].Rows[0][18].ToString();
                    strTipoIdentificacion = blPaicma.myDataSet.Tables[0].Rows[0][20].ToString();

                    blPaicma.Termina();

                    //if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                    //{
                    //    if (blPaicma.fntConsultaTelefono("strDsTelefono", Convert.ToInt32(Session["intIdPersona"].ToString())))
                    //    {
                    //        if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    //        {
                    //            int intContador = 0;
                    //            intContador = blPaicma.myDataSet.Tables[0].Rows.Count;
                    //            for (int i = 0; i < intContador; i++)
                    //            {
                    //                //if (blPaicma.myDataSet.Tables[0].Rows[i][5].ToString() == "Telefono")
                    //                //{
                    //                //    txttelefono.Text = blPaicma.myDataSet.Tables[0].Rows[i][1].ToString();
                    //                //    ddlTipoTelefono.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[i][2].ToString();
                    //                //}
                    //                //else
                    //                //{
                    //                //    txtCelular.Text = blPaicma.myDataSet.Tables[0].Rows[i][1].ToString();
                    //                //    ddlTipoCelular.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[i][2].ToString();
                    //                //}
                    //            }
                    //        }
                    //    }
                    //    blPaicma.Termina();
                    //}

                    //if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                    //{
                    //    if (blPaicma.fntConsultaCorreoElectronico("strDsCorreoElectronico", Convert.ToInt32(Session["intIdPersona"].ToString())))
                    //    {
                    //        if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    //        {
                    //        //    txtEmail.Text = blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                    //        //    ddlTipoEmail.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                    //        }
                    //    }
                    //    blPaicma.Termina();
                    //}

                }
            }
        }
        ftnValidarPermisos();
    }

    public Boolean fntActualizarPersona_bol()
    {
        Boolean bolResultadoAccion = false;
        //if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        //{
        //    if (blPaicma.fntModificarPersona_bol(Convert.ToInt32(Session["intIdPersona"].ToString()), Convert.ToInt32(ddlCategoria.SelectedValue.ToString()), Convert.ToInt32(ddlVocativo.SelectedValue.ToString()), Convert.ToInt32(ddlCargo.SelectedValue.ToString()), txtNombres.Text.Trim(), txtApellido.Text.Trim(), Convert.ToInt32(ddlInstitucion.SelectedValue.ToString()), ddlMunicipio.SelectedValue.ToString(), txtDireccion.Text.Trim(), Convert.ToInt32(ddlTipoDireccion.SelectedValue.ToString())))
        //    {

        //        bolResultadoAccion = true;
        //    }
        //    else
        //    {
        //        bolResultadoAccion = false;
        //    }

        //    blPaicma.Termina();
        //}
        return bolResultadoAccion;
    }

    protected void imgbEliminar_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles();
        pnlRespuesta.Visible = false;
        pnlEliminacion.Visible = true;
        lblEliminacion.Text = "Está seguro de eliminar el registro?";

    }

    protected void btnNo_Click(object sender, System.EventArgs e)
    {
        pnlEliminacion.Visible = false;
        pnlAccionesDir.Visible = true;
        pnlDirNuevo.Visible = true;

    }

    protected void btnSi_Click(object sender, System.EventArgs e)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntEliminarPersona_bol(Convert.ToInt32(Session["intIdPersona"].ToString())))
            {
                fntInactivarPaneles();
                lblRespuesta.Text = "Registro eliminado satisfactoriamente.";
            }
            else
            {
                fntInactivarPaneles();
                lblRespuesta.Text = "No se puede eliminar el registro ya que tiene información asociada.";
            }
            blPaicma.Termina();
        }
    }

    protected void gvCaracterizacion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCaracterizacion.Columns[0].Visible = true;
        gvCaracterizacion.PageIndex = e.NewPageIndex;
        fntCargarGrillaCaracterizacion(string.Empty, string.Empty, string.Empty, string.Empty);
        gvCaracterizacion.Columns[0].Visible = false;
    }

    public Boolean ftnValidarSeguridadCampos_bol(string strTextoCampo)
    {
        Boolean bolValor = false;
        int intCantidadSeguridad = 0;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntEliminarTablaExpresionCampo_bol())
            {
                if (blPaicma.fntIngresarExpresionCampo_bol(strTextoCampo))
                {
                    if (blPaicma.fntConsultaExpresionSeguridad("strDsSeguridadCampos", strTextoCampo))
                    {
                        intCantidadSeguridad = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                        if (intCantidadSeguridad > 0)
                        {
                            bolValor = false;
                        }
                        else
                        {
                            bolValor = true;
                        }
                    }
                }
            }
            blPaicma.Termina();
        }
        return bolValor;
    }

    protected void gvCaracterizacion_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DataRowView drv = (DataRowView)e.Row.DataItem;
        if (drv != null)
        {
            string cellValue = Convert.ToString(drv["tienecaract"]);// By column name
            if(string.IsNullOrWhiteSpace(cellValue))
            {
                e.Row.Cells[7].Text = "NO";
                //e.Row.Cells[7].Visible = false;
            }
            else
            {
                e.Row.Cells[7].Text = "SI";
                //e.Row.Cells[7].Visible = true;
            }
                
        }

    }
}