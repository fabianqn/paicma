﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

public partial class _dmin_PickaVictim : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        string Filtro = "";
        string Parametro = "";

        string caseSwitch = ddlOpcionesFiltro.SelectedValue;

        if (caseSwitch != "0") {
            if (txtBuscar.Text.Trim() != string.Empty)
            {
                switch (caseSwitch)
                {
                  
                    case "1":
                        Filtro = "Nombres";
                        break;
                    case "2":
                        Filtro = "Apellidos";
                        break;
                    case "3":
                        Filtro = "ID_IMSMAVictima";
                        break;
                    case "4":
                        Filtro = "NumeroIdentificacion";
                        break;
                    default:
                        Filtro = "NumeroIdentificacion";
                       
                        break;
                }
                Parametro = txtBuscar.Text.Trim();
            }
            else
            {
                LblError.Text = "Escriba el el valor a buscar";
                LblError.Visible = true;

            }
        }
        else
        {
            LblError.Text = "Para Buscar Seleccione un Filtro";
            LblError.Visible = true;

        }


        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultaTodoPorLike("dataRespuesta", "Victima", Filtro, Parametro))
                {

                    DataTable Listado = new DataTable();
                    Listado = blPaicma.myDataSet.Tables["dataRespuesta"];
                    gvVictimas.DataSource = Listado;
                    gvVictimas.DataBind();

                }
        }
    }
    protected void gvVictimas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "seleccion")
        {
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvVictimas.Rows[intIndex];
            string strRuta = "";
            TableCell Item1 = selectedRow.Cells[0];
            TableCell Item2 = selectedRow.Cells[1];
            TableCell Item3 = selectedRow.Cells[2];
            TableCell Item4 = selectedRow.Cells[3];
            TableCell Item5 = selectedRow.Cells[4];
            TableCell Item6 = selectedRow.Cells[5];
            TableCell Item7 = selectedRow.Cells[6];
           
            Session["IMSMA"] = Item1.Text;
            string intIdVic = Item2.Text;
            Session["VictimaID"] = intIdVic;
            Session["VictimaNombre"] = CleanInput(Item3.Text);
            Session["VictimaApellido"] = CleanInput(Item4.Text);
            Session["VictimaEstado"] = CleanInput(Item5.Text);
            Session["VictimaDepartamento"] = CleanInput(Item6.Text);
            Session["VictimaMunicipio"] = CleanInput(Item7.Text);
            strRuta = "frmFillNewForm.aspx";
            //if (Convert.ToBoolean(Session["RelacionadoConVictima"]))
            //{
                string tablaBuscar = Session["strTablaSeleccionada"].ToString();
                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {

                    if (blPaicma.fntConsultaVictimaPorId("resultadoVictimaEnTabla", tablaBuscar, Session["CampoVictimaBuscar"].ToString(), Session["IMSMA"].ToString().Trim(), Session["CampoVictimaBuscar"].ToString()))
                    {
                        DataTable dtTable;
                        dtTable = blPaicma.myDataSet.Tables["resultadoVictimaEnTabla"];

                        if (dtTable.Rows.Count > 0)
                        {
                            Session["indice"] = Convert.ToInt32(dtTable.Rows[0].ItemArray[0]);
                            strRuta = "frmModifyForm.aspx";
                        }
                        else
                        {
                            strRuta = "frmFillNewForm.aspx";
                        }

                    }
                    else
                    {
                        strRuta = "frmFormCreacion.aspx";
                    }

                }
                else
                {
                    strRuta = "frmFormCreacion.aspx";
                }
                //Session["intTablaSeleccionada"] = intTablaTemporal;
                //Session["strTablaSeleccionada"] = NombreTabla;

            //}
            //else
            //{
              

            //}

           
            Response.Redirect(strRuta);
        }
    }

    public string CleanInput(string strIn)
    {
        // Replace invalid characters with empty strings. 

        return Regex.Replace(strIn, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);

        //return Regex.Replace(strIn, @"[^\w\.@-]", "", RegexOptions.None, TimeSpan.FromSeconds(1.5));

    }
    protected void ddlOpcionesFiltro_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlOpcionesFiltro.SelectedValue == "0")
        {
            txtBuscar.Text = string.Empty;
        }
    }
}