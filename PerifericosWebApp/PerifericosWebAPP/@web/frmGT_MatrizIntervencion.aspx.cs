﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

public partial class _web_frmGT_MatrizIntervencion : System.Web.UI.Page
{
    ///private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        fntCargarArchivo();
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
       // blPaicma = null;
    }

    public void fntCargarArchivo()
    {
        string strTablaTemporal = string.Empty;
        int intCantidad = 0;
        string strNomSP = "SP_GT1";
        string strRutaServidor = string.Empty;


        int intCantidadCampos = 0;
        string strCampos = string.Empty;
        int intCantidadXls = 0;
        string strCamposExcel = string.Empty;
        int intMaxId = 0;
        string strNomSPReiniciar = string.Empty;


        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            Session["ESTRUCTURA"] = null;
            strTablaTemporal = "tmp_GT1";
            blPaicma.fntEliminarTablaTemporal_bol(strTablaTemporal);

            strRutaServidor = System.Configuration.ConfigurationManager.AppSettings["RutaServerDB"];
            strRutaServidor = strRutaServidor + Session["strRutaCompleta"].ToString();

            //if (blPaicma.fntIngresarGT1_bol(strRutaServidor))
            //{
            try
            {
                if (blPaicma.fntConsultaCampoTablas("strDsCamposTablas", strTablaTemporal))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        intCantidadCampos = blPaicma.myDataSet.Tables[0].Rows.Count;
                        for (int i = 0; i < intCantidadCampos; i++)
                        {
                            if (i != 0)
                            {
                                if ((i + 1) != intCantidadCampos)
                                {
                                    strCampos = strCampos + blPaicma.myDataSet.Tables[0].Rows[i][1].ToString() + " ,";
                                }
                                else
                                {
                                    strCampos = strCampos + blPaicma.myDataSet.Tables[0].Rows[i][1].ToString();
                                }

                            }
                        }
                    }
                }

                intCantidadCampos = intCantidadCampos - 1;
                string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strRutaServidor + ";Extended Properties=Excel 12.0;";
                OleDbConnection conn = new OleDbConnection(strConn);
                conn.Open();
                string strExcel = "";
                OleDbDataAdapter myCommand = null;
                DataSet ds = null;
                strExcel = "select * from [Hoja1$]";
                myCommand = new OleDbDataAdapter(strExcel, strConn);
                ds = new DataSet();
                myCommand.Fill(ds, "table1");
                intCantidadXls = ds.Tables[0].Rows.Count;
                conn.Close();

                for (int i = 0; i < intCantidadXls; i++)
                {
                    strCamposExcel = string.Empty;
                    for (int j = 0; j < intCantidadCampos; j++)
                    {
                        if ((j + 1) != intCantidadCampos)
                        {
                            strCamposExcel = strCamposExcel + "'" + ds.Tables[0].Rows[i][j].ToString() + "' ,";
                        }
                        else
                        {
                            strCamposExcel = strCamposExcel + "'" + ds.Tables[0].Rows[i][j].ToString() + "'"; ;
                        }

                    }

                    if (blPaicma.fntConsultaMaxIdTablaDinamica_bol("strDsId", strTablaTemporal))
                    {
                        intMaxId = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                    }
                    if (intMaxId == 1)
                    {
                        strNomSPReiniciar = "SP_ReiniciarIndice ";
                        blPaicma.ReiniciaIndice(strNomSPReiniciar, strTablaTemporal);


                        // lanzo sp_reiniciarIndice
                    }
                    string sw = string.Empty;
                    if (blPaicma.fntIngresarTablaDinamica2_bol(strCampos, strTablaTemporal, strCamposExcel, intMaxId))
                    {
                        sw = "OK";
                    }
                    else
                    {
                        Session["Resultado"] = "NO";
                        Session["ESTRUCTURA"] = "NO";
                        Session["PELIGRO"] = "NA";
                        break;
                    }

                }
                Session["ESTRUCTURA"] = null;

                        if (blPaicma.fntValidarSeguridadGT1("strDsGT1"))
                        {
                            intCantidad = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                            if (intCantidad > 0)
                            {
                                ///el archivo no es seguro, tiene palabras reservadas del motor;
                                Session["Resultado"] = "NO";
                                Session["PELIGRO"] = "OK";
                            }
                            else
                            {
                                ///el archivo es seguro, no tiene palabras reservadas del motor;
                                Session["PELIGRO"] = null;
                                blPaicma.CargarArchivo(strNomSP);
                                fntCargarDatosGeneratesTmp_bol();
                            }
                        }

            }
            catch (Exception ex)
            {
                string strError = ex.ToString();
                Session["Resultado"] = "NO";
                Session["ESTRUCTURA"] = "NO";
                Session["PELIGRO"] = "NA";
                blPaicma.Termina();
            }
            //}
            //else
            //{
            //    Session["Resultado"] = "NO";
            //    Session["ESTRUCTURA"] = "NO";
            //    Session["PELIGRO"] = "NA";
            //}

            blPaicma.Termina();
        }
        blPaicma = null;
        System.IO.File.Delete(Session["strRutaCompleta"].ToString());
        Response.Redirect("frmCGestionTerrirotial.aspx");
    }

    public int fntConsultarMaximoMatrixIntervencion_bol()
    {
        int intidCodigoInforme = 0;
        string strCodigoAuxiliar = "";
        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaMaxMatrizIntervencion_bol("strDsMaxMatrizIntervencion"))
            {
                strCodigoAuxiliar = blPaicma.myDataSet.Tables[0].Rows[0][0].ToString();
                if (strCodigoAuxiliar.ToString() != string.Empty)
                {
                    intidCodigoInforme = (Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString()) + 1);
                }
                else
                {
                    intidCodigoInforme = 1;
                }
            }
            blPaicma.Termina();
        }
        blPaicma = null;
        return intidCodigoInforme;
    }

    public void fntCargarDatosGeneratesTmp_bol()
    {
        string[] strDatosGenerales = new string[3];
        string[] strMatrizIntervencion = new string[11];
        int intContador = 0;
        string strMunicipio = "";
        string strComunidad = "";
        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaDatosGeneralesTmp_bol("strDsDatosGenerales"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    strDatosGenerales[0] = blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                    strDatosGenerales[1] = blPaicma.myDataSet.Tables[0].Rows[1][1].ToString();
                    strDatosGenerales[2] = fntConsultarMaximoMatrixIntervencion_bol().ToString();
                    if (blPaicma.fntEliminarTitulosTmpGT1_bol())
                    {
                        fntCrearActividades_bol();
                        fntCrearCompromisos_bol();

                        if (blPaicma.fntConsultaMatrizIntervencionTmpGT1_bol("dsMatrizIntervencion"))
                        {
                            if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                            {
                                intContador = blPaicma.myDataSet.Tables[0].Rows.Count;
                                for (int i = 0; i < intContador; i++)
                                {
                                    for (int j = 0; j < 11; j++)
                                    {
                                        strMatrizIntervencion[j] = blPaicma.myDataSet.Tables[0].Rows[i][j].ToString();
                                    }
                                    strMunicipio = blPaicma.myDataSet.Tables[0].Rows[i][11].ToString();
                                    strComunidad = blPaicma.myDataSet.Tables[0].Rows[i][12].ToString();
                                    fntCrearMatrizIntervencion(strDatosGenerales, strMatrizIntervencion, strMunicipio, strComunidad);
                                    Session["Resultado"] = "OK";
                                }
                            }
                        }

               
                    }
                }
            }   
            blPaicma.Termina();
        }
        blPaicma = null;
    }

    public void fntCrearActividades_bol()
    {
        int intContador = 0;
        string strIdActividad = "";
        string[] strActividad = new string[3];
        int intSw = 0;
        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaActividadesTmpGT1_bol("strDsActividadesTmpGT1"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    intContador = blPaicma.myDataSet.Tables[0].Rows.Count;
                    for (int i = 0; i < intContador; i++)
                    {
                        strIdActividad = blPaicma.myDataSet.Tables[0].Rows[i][3].ToString();
                        if (strIdActividad == string.Empty)
                        {
                            for (int j = 0; j < 3; j++)
                            {
                                strActividad[j] = blPaicma.myDataSet.Tables[0].Rows[i][j].ToString();
                            }
                            if (blPaicma.fntIngresarActividad_bol(strActividad[0].ToString(), strActividad[1].ToString(), strActividad[2].ToString()))
                            {
                                intSw = 1;
                            }
                        }
                    }

                }
            }
            blPaicma.Termina();
        }
        blPaicma = null;
    }

    public void fntCrearCompromisos_bol()
    {
        int intContador = 0;
        string IdstrCompromiso = "";
        string[] strCompromiso = new string[5];
        int intSw = 0;
        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaCompromisosTmpGT1_bol("strDsCompromisosTmp"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    intContador = blPaicma.myDataSet.Tables[0].Rows.Count;
                    for (int i = 0; i < intContador; i++)
                    {
                        IdstrCompromiso = blPaicma.myDataSet.Tables[0].Rows[i][6].ToString();
                        if (IdstrCompromiso == string.Empty)
                        {
                            for (int j = 0; j < 5; j++)
                            {
                                strCompromiso[j] = blPaicma.myDataSet.Tables[0].Rows[i][j].ToString();
                            }
                            if (blPaicma.fntIngresarCompromisoGT_bol(strCompromiso[0].ToString(), Convert.ToInt32(strCompromiso[1].ToString()), strCompromiso[2].ToString(), Convert.ToInt32(strCompromiso[4].ToString())))
                            {
                                intSw = 1;
                            }
                        }
                    }
                }

            }
            blPaicma.Termina();
        }
        blPaicma = null;
    }

    public void fntCrearMatrizIntervencion(string[] strDatosGenerales, string[] strMatrizIntervencion, string strMunicipio, string strComunidad)
    {
        int intSw = 0;
        int intIdMatrizIntervencion = 0;
        string strIdMunicipio = "";
        int intIdComunidad = 0;
        int intContador = 0;
        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarMatrizIntervencionR_bol(Convert.ToInt32(strDatosGenerales[2].ToString()), strDatosGenerales[0].ToString(), strDatosGenerales[1].ToString(), strMatrizIntervencion[1].ToString(), strMatrizIntervencion[2].ToString(), strMatrizIntervencion[3].ToString(), strMatrizIntervencion[4].ToString(), Convert.ToInt32(strMatrizIntervencion[5].ToString()), Convert.ToInt32(strMatrizIntervencion[6].ToString()), Convert.ToInt32(strMatrizIntervencion[7].ToString()), Convert.ToInt32(strMatrizIntervencion[8].ToString()), strMatrizIntervencion[9].ToString(), strMatrizIntervencion[10].ToString()))
            {
                intSw = 1;
                if (blPaicma.fntConsultaMatrizIntervencion_bol("strDsMatrizIntervencion", Convert.ToInt32(strDatosGenerales[2].ToString()), strDatosGenerales[0].ToString(), strDatosGenerales[1].ToString(), strMatrizIntervencion[1].ToString(), strMatrizIntervencion[2].ToString(), strMatrizIntervencion[3].ToString(), strMatrizIntervencion[4].ToString(), Convert.ToInt32(strMatrizIntervencion[5].ToString()), Convert.ToInt32(strMatrizIntervencion[6].ToString()), Convert.ToInt32(strMatrizIntervencion[7].ToString()), Convert.ToInt32(strMatrizIntervencion[8].ToString()), strMatrizIntervencion[9].ToString(), strMatrizIntervencion[10].ToString()))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        intIdMatrizIntervencion = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());

                        //char[] delimiterChars = { ' ', ',', '.', ':', '\t' };
                        string[] StrArrMunicipos = strMunicipio.Split(',');
                        intContador = StrArrMunicipos.Count();
                        for (int i = 0; i < intContador; i++)
                        {
                            strIdMunicipio = fntConsultarMunicipio_int(StrArrMunicipos[i].ToString());
                            if (blPaicma.fntIngresarMunicipiosImpactados_bol(strIdMunicipio, intIdMatrizIntervencion))
                            {
                                intSw = 1;
                            }
                            blPaicma.myDataSet.Clear();
                        }

                        string[] StrArrComunidad = strComunidad.Split(',');
                        intContador = StrArrComunidad.Count();
                        for (int j = 0; j < intContador; j++)
                        {
                            if (blPaicma.fntConsultaComunidad_bol("strDsComunidad", StrArrComunidad[j].ToString()))
                            {
                                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                                {
                                    intIdComunidad = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                                    if (blPaicma.fntIngresarComuidadImpactados_bol(intIdComunidad, intIdMatrizIntervencion))
                                    {
                                        intSw = 1;
                                    }
                                }
                            }
                            blPaicma.myDataSet.Clear();
                        }

                    }
                }
            }
            blPaicma.Termina();
        }
        blPaicma = null;
    }

    public string fntConsultarMunicipio_int(string strMunicipio)
    {
        string strIdMunicipio = "";
        blSisPAICMA blPaicma = new blSisPAICMA();
         if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaMunicipios("strDsMunicipio", 0,strMunicipio.ToString()))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    strIdMunicipio = blPaicma.myDataSet.Tables[0].Rows[0][0].ToString();
                }
            }

            blPaicma.Termina();
        }
         blPaicma = null;
         return strIdMunicipio;
    }
    



}