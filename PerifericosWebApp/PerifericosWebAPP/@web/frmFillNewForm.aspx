﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_web_FillNewForm" Codebehind="frmFillNewForm.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">
    <div class="Botones">
        <asp:ImageButton ID="UpdateButton" runat="server"
            ImageUrl="~/Images/guardar.png" ToolTip="Buscar"
            OnClick="UpdateButton_Click" />
        <a href="/@web/frmFormCreacion.aspx"> 
            <img src="../Images/cancelar.png" alt=""  />
        </a>
        <%--<input type="image" name="ctl00$contenido$imgbCancelar" id="contenido_imgbCancelar" src="../Images/cancelar.png" />--%>
        <%--<asp:ImageButton ID="imgbCancelar" runat="server" ImageUrl="~/Images/cancelar.png" OnClick="imgbCancelar_Click" />--%>
    </div>


 <div class="contacto2" style="width: 88%; float:left" runat="server" id="mensajes" visible="false">
<asp:Label ID="LabelMsgCorrecto" runat="server" Text="Registro Exitoso" Visible="false"></asp:Label>
    <asp:Label ID="LabelMsgError" runat="server" Text="El registro a fallado" Visible="false"></asp:Label>
    <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>  
     </div>
    <asp:Panel ID="PanelFormulario" runat="server" CssClass="contacto">
        <%-- <form id="form1" runat="server">--%>
        <h1>Formulario <%= Session["strTablaSeleccionada"].ToString() %></h1>



        <asp:Table ID="CustomUITable" runat="server" CssClass="">
        </asp:Table>

        <%--  <asp:Button runat="server" ID="enviar" OnClick="enviar_Click" />
        </form>--%>
    </asp:Panel>
    
    <p>
        <%-- <asp:Button ID="UpdateButton" runat="server" Text="Update" OnClick="UpdateButton_Click"/>--%>
        <%--    &nbsp;&nbsp;&nbsp;
        <asp:Button ID="CancelButton" runat="server" Text="Cancel" CausesValidation="false" />--%>
    </p>
</asp:Content>

<asp:Content ContentPlaceHolderID="scripts" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".ddDepartamento").prepend("<option value='0'></option>").val('0');
            $(".ddMunicipio").prepend("<option value='0'></option>").val('0');
            $('#' + '<%= UpdateButton.ClientID %>').click(function () {
                if ($(".campo").length) {
                    if ($(".campo").val() == "") {
                        alert("rellene o seleccione todos los campos");
                        return false;

                    };
                };

                if ($(".entero").length) {
                    if ($(".entero").val() == "") {
                        alert("rellene o seleccione todos los campos");
                        return false;

                    };
                };

                if ($(".entero").length) {
                    if ($(".decimal").val() == "") {
                        alert("rellene o seleccione todos los campos");
                        return false;

                    };
                };

                if ($(".ddMunicipio").length) {
                    if ($(".ddMunicipio").val() == "0" || $(".ddMunicipio").val() == "") {
                        alert("rellene o seleccione todos los campos");
                        return false;
                    }
                };

                if ($(".ddDepartamento").length) {
                    if ($(".ddDepartamento").val() == "0" || $(".ddDepartamento").val() == "") {
                        alert("rellene todos los campos");
                        return false;
                    }
                };

                if ($(".decimal").length) {

                    if (validateDecimal($(".decimal").val())) {

                    } else {
                        alert("Algunos Campos declarados decimales, no cumplen con la condicion establecida");
                        return false;
                    }
                }

            });

            function validateDecimal(dec) {
                var regx = /^\d+\.?\d{0,2}$/g;
                return regx.test(dec);
            }

            
            $(".entero").keydown(function (event) {
                // Prevent shift key since its not needed
                if (event.shiftKey == true) {
                    event.preventDefault();
                }
                // Allow Only: keyboard 0-9, numpad 0-9, backspace, tab, left arrow, right arrow, delete
                if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) {
                    // Allow normal operation
                } else {
                    // Prevent the rest
                    event.preventDefault();
                }
            });

            $(".pluginFecha").datepicker({dateFormat: 'yy-mm-dd',changeMonth: true, changeYear: true});

         

            //$("").val("0");
            //$(".ddMunicipio").val("0");
            //$(".ddDepartamento").prepend("<option value='0' selected='selected'></option>");
            //$(".ddMunicipio").prepend("<option value='0' selected='selected'></option>");

            $(".ddMunicipio option").each(function (index, val) {
                if ($(this).is('option') && (!$(this).parent().is('span')))
                    $(this).wrap((navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') ? '<span>' : null).hide();
            });

          <%--  $("#contenido_imgbCancelar").click(function () {
                alert("hola");
              <%  Response.Redirect("~/@web/frmFormCreacion.aspx"); %>
                //window.location = "@web/frmFormCreacion.aspx";
            })--%>


            $(".ddDepartamento").change(function () {

                var valorDep = $(this).val();
                var ordenDep = $(this).data("orden");
                var id = $(this).attr("id");
                //alert(id)
                var cadenaAnterior = parseInt(id.replace(/[^0-9\.]/g, ''), 10);
                
                //alert(cadenaPrevia);
                var numero = parseInt(cadenaAnterior, 10) + 1;
                //alert(numero);

                var nombreMunicipioSustituir = id.replace('comboDepartamento', 'comboMunicipio');
                var nombreMunicipioCortar = nombreMunicipioSustituir.replace(cadenaAnterior, '');
                //alert(numero);
                var idMun = nombreMunicipioCortar + numero;
                var optDep;


                if (valorDep != "0") {
                    $("#" + idMun + " option").each(function (index, val) {
                        var $el = $(this);
                        if ($el.val() == "0") {
                            $el.attr("selected", true);
                        }
                        optDep = $el.data("departamento")
                        if (optDep != valorDep) {
                            if ($(this).is('option') && (!$(this).parent().is('span')))
                                $(this).wrap((navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') ? '<span>' : null).hide();
                        } else {
                            if (navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') {
                                if (this.nodeName.toUpperCase() === 'OPTION') {
                                    var span = $(this).parent();
                                    var opt = this;
                                    if ($(this).parent().is('span')) {
                                        $(opt).show();
                                        $(span).replaceWith(opt);
                                    }
                                }
                            } else {
                                $(this).show(); //all other browsers use standard .show()
                            }
                        }
                    });
                } else {
                    $("#" + idMun).val("0");
                    $("#" + idMun + " option").each(function () {
                        if ($(this).is('option') && (!$(this).parent().is('span')))
                            $(this).wrap((navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') ? '<span>' : null).hide();
                    });
                }
            });
        });

    </script>

</asp:Content>

