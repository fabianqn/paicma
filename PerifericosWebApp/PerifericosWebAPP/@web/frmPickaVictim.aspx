﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_PickaVictim" Codebehind="frmPickaVictim.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">
    <div class ="contacto">
    <table  runat="server" id="Activiades">
        <thead>
            <tr>

                            <td colspan="3"><h1>Seleccione como Desea Buscar y Encuentre una Victima</h1></td>
                            <td></td>
                            <td></td>
                            <td></td>
                             <td></td>

            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTabla" runat="server" Text="Filtro de Busqueda"></asp:Label>
                </td>
                <td style="text-align: left">

                     <asp:DropDownList ID="ddlOpcionesFiltro" runat="server" AutoPostBack="True" Enabled="true" OnSelectedIndexChanged="ddlOpcionesFiltro_SelectedIndexChanged">
                         <asp:ListItem Value="0">----</asp:ListItem>
                         <asp:ListItem Value="4">Documento Identidad</asp:ListItem>
                         <asp:ListItem Value="3">Id Victima</asp:ListItem>
                         <asp:ListItem Value="1">Nombre</asp:ListItem>
                         <asp:ListItem Value="2">Apellido</asp:ListItem>
                     </asp:DropDownList>
                   <%-- <asp:TextBox ID="txtNombreVictima" runat="server" MaxLength="50"></asp:TextBox>--%>
                </td>


                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblAbreviatura" runat="server" Text="Valor a Buscar"></asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtBuscar" runat="server" MaxLength="20"></asp:TextBox>
                </td>
                <td></td>
                <td></td>
                <td></td>
                

            </tr>
            <tr>
                <td>
                     <asp:ImageButton ID="imgbBuscar" runat="server"
                ImageUrl="~/Images/BuscarAccion.png" ToolTip="Buscar"
                OnClick="imgbBuscar_Click" />
                </td>
                <td>
           
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>

            </tr>
        </thead>
    </table>

        <asp:GridView ID="gvVictimas" runat="server" onrowcommand="gvVictimas_RowCommand" CssClass="mGrid" PagerStyle-CssClass="pgr"
                            AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" >
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>

            <Columns>
                <asp:BoundField DataField="ID_IMSMAVictima" HeaderText="ID IMSMA" />
                <asp:BoundField DataField="NumeroIdentificacion" HeaderText="Documento ID" />
                <asp:BoundField DataField="Nombres" HeaderText="Nombre" />
                <asp:BoundField DataField="Apellidos" HeaderText="Apellido" />
                <asp:BoundField DataField="Estado" HeaderText="Estado" />
                <asp:BoundField DataField="Departamento" HeaderText="Departamento" />
                <asp:BoundField DataField="Municipio" HeaderText="Municipio" />
                <asp:ButtonField ButtonType="Image" CommandName="seleccion" ImageUrl="~/Images/seleccionar.png" Text="Button" />
            </Columns>
            
<PagerStyle CssClass="pgr"></PagerStyle>
        </asp:GridView>
         <asp:Label ID="LblError" runat="server" Text="" Visible="false"></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="Server">
</asp:Content>

