﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Data;

public partial class _dmin_frmFormulario : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {

            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                ftnValidarPermisos();
                fntCargarEncuestasPendientes();
                fntCargarFormulario();
            }
        }
    }
    
    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }

    public void ftnValidarPermisos()
    {
        //int intIdFormulario = 0;
        string strBuscar = string.Empty;
        string strNuevo = string.Empty;
        string strEditar = string.Empty;
        string strEliminar = string.Empty;

        //obtiene el nombre de la pagina actual
        string[] strRutaPagina = HttpContext.Current.Request.RawUrl.Split('/');
        string strNombrePagina = strRutaPagina[strRutaPagina.GetUpperBound(0)];
        strRutaPagina = strNombrePagina.Split('?');
        strNombrePagina = strRutaPagina[strRutaPagina.GetLowerBound(0)];
        //fin obtiene el nombre de la pagina actual


        if (strNombrePagina != string.Empty)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                //intIdFormulario = Convert.ToInt32(Request.QueryString["op"].ToString());
                //Session["intIdFormulario"] = intIdFormulario;
                if (blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(Session["IdUsuario"].ToString()), strNombrePagina))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strBuscar = blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                        strNuevo = blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                        strEditar = blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                        strEliminar = blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();

                        if (strBuscar == "False")
                        {
                            //imgbBuscar.Visible = false;
                        }
                        if (strNuevo == "False")
                        {
                            //imgbNuevo.Visible = false;
                        }
                        if (strEditar == "False")
                        {
                            //imgbEditar.Visible = false;
                            //imgbGravar.Visible = false;
                        }
                        if (strEliminar == "False")
                        {
                            //imgbEliminar.Visible = false;
                        }

                    }
                }
                blPaicma.Termina();
            }
        }
        else
        {
            Response.Redirect("@dmin/frmLogin.aspx");
        }
    }

    public void fntInactivarPaneles(Boolean bolValor)
    {
        //pnlAccionesVariable.Visible = bolValor;
        pnlMenuVariable.Visible = bolValor;       
    }

    public void fntCargarEncuestasPendientes()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaEncuestasPendientes("strDsEncuestasPendientes"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlidEncuesta.DataSource = blPaicma.myDataSet.Tables[0];
                    ddlidEncuesta.DataTextField = "cedula";
                    ddlidEncuesta.DataValueField = "idEncuesta";
                    ddlidEncuesta.DataBind();

                    ddlidEncuesta.Items.Insert(0, "--Seleccione--");
                }
            }
        }
    }

    public void fntCargarFormulario()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaVariablesAdmin("strDsVariables", string.Empty))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    DataSet variables = blPaicma.myDataSet;

                    foreach (DataRow row in variables.Tables[0].Rows)
                    {
                        if( int.Parse(row["idTipoPregunta"].ToString()) == 0)
                        {
                            continue;
                        }

                        TableRow tRow = new TableRow();
                        tRow.CssClass = "rowbottom";

                        //Celda para el label
                        Label myLabel = new Label();
                        myLabel.Text = row["preguntaFormulario"].ToString();
                        TableCell labelCell = new TableCell();
                        labelCell.Controls.Add(myLabel);
                        labelCell.CssClass = "left";
                        //labelCell.Width = Unit.Pixel(200);

                        //Celda para el control
                        TableCell controlCell = new TableCell();
                        controlCell.CssClass = "right";

                        //controlCell.Width = Unit.Pixel(200);

                        int tipopregunta = int.Parse(row["idTipoPregunta"].ToString());
                        if (tipopregunta == 0) // Custom
                        {

                        }
                        if (tipopregunta == 1) // Abierta
                        {
                            
                            TextBox newtextbox = new TextBox();
                            newtextbox.ID = row["nombreVariable"].ToString();

                            if (row["preguntaFormulario"].ToString().ToLower().Contains("observa"))
                            {
                                newtextbox.TextMode = TextBoxMode.MultiLine;
                            }
                            controlCell.Controls.Add(newtextbox);

                        }
                        if (tipopregunta == 2) // Seleccion Unica
                        {
                            if (blPaicma.fntConsultaOpcionesVariablesAdmin("strDsValorXVariable", row["nombreVariable"].ToString()))
                            {
                                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                                {
                                    DataSet valoresxvariable = blPaicma.myDataSet;

                                    DropDownList newdrop = new DropDownList();
                                    newdrop.ID = row["nombreVariable"].ToString();
                                    newdrop.DataSource = valoresxvariable.Tables[0];
                                    newdrop.DataTextField = "textoValor";
                                    newdrop.DataValueField = "idValorXVariable";
                                    newdrop.DataBind();

                                    newdrop.Items.Insert(0, "--Seleccione--");

                                    controlCell.Controls.Add(newdrop);
                                }
                            }
                        }
                        if (tipopregunta == 3) // Seleccion Multiple
                        {
                            CheckBox newcheck = new CheckBox();
                            newcheck.ID = row["nombreVariable"].ToString();
                            newcheck.ValidationGroup = row["nombreGrupo"].ToString();
                            //newcheck.Text = row["preguntaFormulario"].ToString();
                            controlCell.Controls.Add(newcheck);

                        }

                        tRow.Cells.Add(labelCell);
                        tRow.Cells.Add(controlCell);

                        Table1.Rows.Add(tRow);
                    }


                }
            }
            blPaicma.Termina();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            // recorre los controles
            foreach (Control control in Table1.Controls)
            {
                int idencuesta = Convert.ToInt32(ddlidEncuesta.SelectedValue);
                string respuesta = "";
                int? idvalorxvariable = null;
                int idvariable = 0;
                string nombrevariable = "";
                

                if (control is TextBox)
                {
                    respuesta = ((TextBox)control).Text;
                    nombrevariable = ((TextBox)control).ID;
                }

                if (control is DropDownList)
                {
                    respuesta = ((DropDownList)control).SelectedItem.Text;
                    nombrevariable = ((DropDownList)control).ID;
                    idvalorxvariable = Convert.ToInt32(((DropDownList)control).SelectedItem.Value);
                }

                if (control is CheckBox)
                {
                    nombrevariable = ((CheckBox)control).ID;
                }

                if (blPaicma.fntConsultaVariablesAdmin("strDsVariables", nombrevariable))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                       idvariable = Convert.ToInt32( blPaicma.myDataSet.Tables[0].Rows[0]["idVariable"].ToString());
                    }
                }

                blPaicma.fntIngresarRespuestaNew_bol(idencuesta, respuesta, idvalorxvariable, idvariable);

            
            }

            blPaicma.Termina();
        }
    }
}