﻿//using ASP;
using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _web_frmBiblioteca : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }
            this.ftnValidarPermisos();
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                this.fntCargarGrillaBibliotecasDisponibles();
                return;
            }
            base.Response.Redirect("@dmin/frmLogin.aspx");
        }
    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        this.blPaicma = null;
    }


    protected void gvDocumentosBiblioteca_PreRender(object sender, EventArgs e)
    {
        if (gvDocumentosBiblioteca.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvDocumentosBiblioteca.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvDocumentosBiblioteca.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvDocumentosBiblioteca.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }

    protected void gvBibliotecasAsignadas_PreRender(object sender, EventArgs e)
    {
        if (gvBibliotecasAsignadas.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvBibliotecasAsignadas.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvBibliotecasAsignadas.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvBibliotecasAsignadas.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
    public void ftnValidarPermisos()
    {
        string a = string.Empty;
        string a2 = string.Empty;
        string a3 = string.Empty;
        string a4 = string.Empty;
        string[] array = HttpContext.Current.Request.RawUrl.Split(new char[]
		{
			'/'
		});
        string text = array[array.GetUpperBound(0)];
        array = text.Split(new char[]
		{
			'?'
		});
        text = array[array.GetLowerBound(0)];
        if (text != string.Empty)
        {
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (this.blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(this.Session["IdUsuario"].ToString()), text) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    a = this.blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                    a2 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                    a3 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                    a4 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();
                    if (a == "False")
                    {
                        //this.imgbBuscar.Visible = false;
                    }
                    //a2 == "False";
                    if (a3 == "False")
                    {
                        this.imgbEditar.Visible = false;
                        this.imgbGravar.Visible = false;
                    }
                    if (a4 == "False")
                    {
                        this.imgbEliminar.Visible = false;
                    }
                }
                this.blPaicma.Termina();
                return;
            }
        }
        else
        {
            base.Response.Redirect("@dmin/frmLogin.aspx");
        }
    }
    public void fntCargarGrillaBibliotecasDisponibles()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaBibliotecasAsignadas_bol("strDsGrillaBiblioteca", Convert.ToInt32(this.Session["IdUsuario"].ToString())))
            {
                this.gvBibliotecasAsignadas.Columns[0].Visible = true;
                this.gvBibliotecasAsignadas.Columns[1].Visible = true;
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gvBibliotecasAsignadas.DataMember = "strDsGrillaBiblioteca";
                    this.gvBibliotecasAsignadas.DataSource = this.blPaicma.myDataSet;
                    this.gvBibliotecasAsignadas.DataBind();
                    this.lblErrorGv.Text = string.Empty;
                }
                else
                {
                    this.gvBibliotecasAsignadas.DataMember = "strDsGrillaBiblioteca";
                    this.gvBibliotecasAsignadas.DataSource = this.blPaicma.myDataSet;
                    this.gvBibliotecasAsignadas.DataBind();
                    this.lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                this.gvBibliotecasAsignadas.Columns[0].Visible = false;
                this.gvBibliotecasAsignadas.Columns[1].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    protected void gvBibliotecasAsignadas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            this.gvBibliotecasAsignadas.Columns[0].Visible = true;
            this.gvBibliotecasAsignadas.Columns[1].Visible = true;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow = this.gvBibliotecasAsignadas.Rows[index];
            TableCell tableCell = gridViewRow.Cells[1];
            int num = Convert.ToInt32(tableCell.Text);
            this.Session["intIdBiblioteca"] = num;
            TableCell tableCell2 = gridViewRow.Cells[2];
            string text = tableCell2.Text;
            this.Session["strNombreBiblioteca"] = text;
            this.imgbEditar.Visible = true;
            this.gvBibliotecasAsignadas.Columns[0].Visible = false;
            this.gvBibliotecasAsignadas.Columns[1].Visible = false;
            this.ftnLimpiarGrillaColorBibliotecas();
            this.gvBibliotecasAsignadas.Rows[index].BackColor = Color.Brown;
        }
    }
    public void ftnLimpiarGrillaColorBibliotecas()
    {
        int count = this.gvBibliotecasAsignadas.Rows.Count;
        for (int i = 0; i < count; i++)
        {
            this.gvBibliotecasAsignadas.Rows[i].BackColor = Color.Empty;
        }
    }
    public void fntInactivarPaneles(bool bolValor)
    {
        this.pnlAccionesBiblioteca.Visible = bolValor;
        this.pnlBibliotecaNuevo.Visible = bolValor;
        this.PnlBibliotecasDisponiblesGrilla.Visible = bolValor;
        this.pnlEliminacion.Visible = bolValor;
        this.pnlMenuBiblioteca.Visible = bolValor;
        this.pnlRespuesta.Visible = bolValor;
        this.pnlDocumentos.Visible = bolValor;
        this.pnlDocumentosBiblioteca.Visible = bolValor;
    }
    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlAccionesBiblioteca.Visible = true;
        this.imgbEncontrar.Visible = false;
        this.imgbGravar.Visible = false;
        this.imgbCancelar.Visible = true;
        this.imgbNuevoDocumento.Visible = true;
        this.imgbEliminar.Visible = false;
        this.pnlDocumentosBiblioteca.Visible = true;
        this.fntCargarGrillaDocumentosBiblioteca(Convert.ToInt32(this.Session["intIdBiblioteca"].ToString()), "ACTIVO", 0);
        this.Session["Operacion"] = "EditarBiblioteca";
    }
    public void fntCargarGrillaDocumentosBiblioteca(int intIdBiblioteca, string strEstado, int intIdCodigoDocumento)
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaDocumentosBiblioteca_bol("strDsGrillaDocumentosBiblioteca", intIdBiblioteca, strEstado, intIdCodigoDocumento))
            {
                this.gvDocumentosBiblioteca.Columns[0].Visible = true;
                this.gvDocumentosBiblioteca.Columns[1].Visible = true;
                this.gvDocumentosBiblioteca.Columns[2].Visible = true;
                this.gvDocumentosBiblioteca.Columns[10].Visible = true;
                this.gvDocumentosBiblioteca.Columns[12].Visible = true;
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gvDocumentosBiblioteca.DataMember = "strDsGrillaDocumentosBiblioteca";
                    this.gvDocumentosBiblioteca.DataSource = this.blPaicma.myDataSet;
                    this.gvDocumentosBiblioteca.DataBind();
                    this.lblErrorDocumentos.Text = string.Empty;
                }
                else
                {
                    this.gvDocumentosBiblioteca.DataMember = "strDsGrillaDocumentosBiblioteca";
                    this.gvDocumentosBiblioteca.DataSource = this.blPaicma.myDataSet;
                    this.gvDocumentosBiblioteca.DataBind();
                    this.lblErrorDocumentos.Text = "No hay documentos cargados a esta biblioteca";
                }
                this.gvDocumentosBiblioteca.Columns[0].Visible = false;
                this.gvDocumentosBiblioteca.Columns[1].Visible = false;
                this.gvDocumentosBiblioteca.Columns[2].Visible = false;
                this.gvDocumentosBiblioteca.Columns[10].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarEstado()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaEstadoBiblioteca("strDsEstado", string.Empty) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlEstado.DataMember = "strDsEstado";
                this.ddlEstado.DataSource = this.blPaicma.myDataSet;
                this.ddlEstado.DataValueField = "est_Id";
                this.ddlEstado.DataTextField = "est_Descripcion";
                this.ddlEstado.DataBind();
                this.ddlEstado.Items.Insert(this.ddlEstado.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    public int fntBuscarEstadoEspecifico(string strEstado)
    {
        int result = 0;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaEstadoBiblioteca("strDsEstado", strEstado) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                result = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
            }
            this.blPaicma.Termina();
        }
        return result;
    }
    protected void imgbNuevoDocumento_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlAccionesBiblioteca.Visible = true;
        this.pnlBibliotecaNuevo.Visible = true;
        this.imgbEncontrar.Visible = false;
        this.imgbGravar.Visible = true;
        this.imgbCancelar.Visible = true;
        this.imgbNuevoDocumento.Visible = false;
        this.Session["Operacion"] = "NuevoDocumento";
        this.ddlEstado.Visible = false;
        this.fntCargarCarpetas();
    }
    public void fntCargarCarpetas()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            int intIdGrupoBiblioteca = Convert.ToInt32(this.Session["intIdBiblioteca"].ToString());
            if (this.blPaicma.fntConsultaBibliotecaCarpeta_bol("strDsBibliotecaCarpeta", intIdGrupoBiblioteca) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlCarpeta.DataMember = "strDsBibliotecaCarpeta";
                this.ddlCarpeta.DataSource = this.blPaicma.myDataSet;
                this.ddlCarpeta.DataValueField = "bibCar_Id";
                this.ddlCarpeta.DataTextField = "bibCar_Descripcion";
                this.ddlCarpeta.DataBind();
                this.ddlCarpeta.Items.Insert(this.ddlCarpeta.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        if (this.Session["Operacion"] == null || this.Session["Operacion"].ToString() == "EditarBiblioteca")
        {
            string url = "frmBiblioteca.aspx";
            base.Response.Redirect(url);
            return;
        }
        if (this.Session["Operacion"].ToString() == "NuevoDocumento")
        {
            this.fntInactivarPaneles(false);
            this.pnlAccionesBiblioteca.Visible = true;
            this.imgbEncontrar.Visible = false;
            this.imgbGravar.Visible = true;
            this.imgbCancelar.Visible = true;
            this.imgbNuevoDocumento.Visible = true;
            this.Session["Operacion"] = "EditarBiblioteca";
            this.imgbGravar.Visible = false;
            return;
        }
        if (this.Session["Operacion"].ToString() == "NuevaVersion")
        {
            this.fntInactivarPaneles(false);
            this.pnlAccionesBiblioteca.Visible = true;
            this.imgbEncontrar.Visible = false;
            this.imgbGravar.Visible = true;
            this.imgbCancelar.Visible = true;
            this.imgbNuevoDocumento.Visible = true;
            this.Session["Operacion"] = "EditarBiblioteca";
            this.imgbGravar.Visible = false;
            this.pnlDocumentosBiblioteca.Visible = true;
            this.fntCargarGrillaDocumentosBiblioteca(Convert.ToInt32(this.Session["intIdBiblioteca"].ToString()), "ACTIVO", 0);
        }
        txtObservacion.Text = "";
    }
    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        string text = string.Empty;
        string text2 = string.Empty;
        string str = string.Empty;
        string filename = string.Empty;

        if (this.Session["Operacion"] == null || this.Session["Operacion"].ToString() == "EditarBiblioteca")
        {
            string url = "frmBiblioteca.aspx";
            base.Response.Redirect(url);
            return;
        }
        if (this.Session["Operacion"].ToString() == "NuevoDocumento")
        {
            try
            {
                if (this.ftnValidarCampos_bol())
                {
                    text = this.fuDocumento.FileName;
                    string[] array = text.Split(new char[]
					{
						'.'
					});
                    text = array[0];
                    string text3 = string.Empty;
                    if (this.Session["idVersion"] == null)
                    {
                        text3 = "_V1";
                        this.Session["intVersion"] = "1";
                    }
                    int intIdCodigoBiblioteca = this.fntTraerMaximoCodigoDocumento();
                    text2 = Path.GetExtension(this.fuDocumento.PostedFile.FileName);
                    text = string.Concat(new string[]
					{
						text,
						text3,
						"_",
						intIdCodigoBiblioteca.ToString(),
						text2
					});
                    str = base.Server.MapPath("~\\@web\\Operador\\");
                    filename = str + text;
                    this.fuDocumento.PostedFile.SaveAs(filename);
                    int intIdEstado = this.fntBuscarEstadoEspecifico("ACTIVO");
                    int intIdCarpeta = Convert.ToInt32(this.ddlCarpeta.SelectedValue.ToString());
                    if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
                    {
                        if (this.blPaicma.fntIngresarDocumentoBiblioteca_bol(Convert.ToInt32(this.Session["intIdBiblioteca"].ToString()), Convert.ToInt32(this.Session["IdUsuario"].ToString()), text, text2, this.txtObservacion.Text.Trim(), intIdEstado, intIdCodigoBiblioteca, Convert.ToInt32(this.Session["intVersion"].ToString()), intIdCarpeta))
                        {
                            this.fntTraerGrupoUsuariosBiblioteca(Convert.ToInt32(this.Session["intIdBiblioteca"].ToString()), text, this.Session["strNombreBiblioteca"].ToString());
                            this.lblRespuesta.Text = this.lblRespuesta.Text + "Archivo cargado satisfactoriamente";
                        }
                        else
                        {
                            this.lblRespuesta.Text = this.lblRespuesta.Text + "Problema al cargar el archivo, por favor comuníquese con el administrador.";
                        }
                        this.fntInactivarPaneles(false);
                        this.pnlRespuesta.Visible = true;
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                base.Response.Write(ex.ToString() + "<br />");
                return;
            }
        }
        if (this.Session["Operacion"].ToString() == "NuevaVersion")
        {
            text = this.fuDocumento.FileName;
            string[] array2 = text.Split(new char[]
			{
				'.'
			});
            text = array2[0];
            string text4 = string.Empty;
            if (this.Session["intVersion"] != null)
            {
                text4 = "_V" + this.Session["intVersion"].ToString();
            }
            int intIdCodigoBiblioteca2 = Convert.ToInt32(this.Session["intCodigoDocumento"].ToString());
            text2 = Path.GetExtension(this.fuDocumento.PostedFile.FileName);
            text = string.Concat(new string[]
			{
				text,
				text4,
				"_",
				intIdCodigoBiblioteca2.ToString(),
				text2
			});
            str = base.Server.MapPath("~\\@web\\Operador\\");
            filename = str + text;
            this.fuDocumento.PostedFile.SaveAs(filename);
            int intIdEstado2 = this.fntBuscarEstadoEspecifico("ACTIVO");
            int intIdCarpeta2 = Convert.ToInt32(this.ddlCarpeta.SelectedValue.ToString());
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (this.blPaicma.fntIngresarDocumentoBiblioteca_bol(Convert.ToInt32(this.Session["intIdBiblioteca"].ToString()), Convert.ToInt32(this.Session["IdUsuario"].ToString()), text, text2, this.txtObservacion.Text.Trim(), intIdEstado2, intIdCodigoBiblioteca2, Convert.ToInt32(this.Session["intVersion"].ToString()), intIdCarpeta2))
                {
                    this.lblRespuesta.Text = "Archivo cargado satisfactoriamente";
                }
                else
                {
                    this.lblRespuesta.Text = "Problema al cargar el archivo, por favor comuníquese con el administrador.";
                }
                this.fntInactivarPaneles(false);
                this.pnlRespuesta.Visible = true;
            }
            if (this.Session["OperacionAdd"].ToString() == "Actualizar")
            {
                this.fntModificarVersionBiblioteca(Convert.ToInt32(this.Session["intIdBibliotecaActualizar"]), Convert.ToInt32(this.Session["intEstado"]));
            }
        }

        txtObservacion.Text = "";
    }
    public bool ftnValidarCampos_bol()
    {
        string a = string.Empty;
        string a2 = string.Empty;
        a = this.fuDocumento.FileName;
        a2 = Path.GetExtension(this.fuDocumento.PostedFile.FileName);
        if (a == string.Empty)
        {
            this.lblError.Text = "Debe seleccionar un documento.";
            return false;
        }
        if (!(a2 == ".txt") && !(a2 == ".pdf") && !(a2 == ".doc") && !(a2 == ".docx") && !(a2 == ".xls") && !(a2 == ".xlsx") && !(a2 == ".rar") && !(a2 == ".zip") && !(a2 == ".jpg") && !(a2 == ".gif") && !(a2 == ".png") && !(a2 == ".tif") && !(a2 == ".mp3") && !(a2 == ".mp4") && !(a2 == ".wma") && !(a2 == ".midi") && !(a2 == ".pptx") && !(a2 == ".ppt") && !(a2 == ".pps"))
        {
            this.lblError.Text = "Este tipo de archivo no se puede subir. Por favor verificar.";
            return false;
        }
        this.lblError.Text = "formato correcto.";
        if (this.txtObservacion.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe indicar un comentario u observación.";
            return false;
        }
        if (this.ddlEstado.SelectedValue == "Seleccione...")
        {
            this.lblError.Text = "Debe seleccionar un estado.";
            return false;
        }
        if (this.ddlCarpeta.SelectedValue == "Seleccione...")
        {
            this.lblError.Text = "Debe seleccionar una carpeta.";
            return false;
        }
        return true;
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        if (this.Session["Operacion"] == null)
        {
            string url = "frmBiblioteca.aspx";
            base.Response.Redirect(url);
            return;
        }
        if (this.Session["Operacion"].ToString() == "NuevoDocumento" || this.Session["Operacion"].ToString() == "NuevaVersion")
        {
            this.fntInactivarPaneles(false);
            this.pnlAccionesBiblioteca.Visible = true;
            this.Session["Operacion"] = "EditarBiblioteca";
            this.imgbGravar.Visible = false;
            this.imgbNuevoDocumento.Visible = true;
            this.pnlDocumentosBiblioteca.Visible = true;
            this.fntCargarGrillaDocumentosBiblioteca(Convert.ToInt32(this.Session["intIdBiblioteca"].ToString()), "ACTIVO", 0);
        }
    }
    protected void gvDocumentosBiblioteca_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string text = string.Empty;
        string str = string.Empty;
        string arg_11_0 = string.Empty;
        string text2 = string.Empty;
        if (e.CommandName == "Ver")
        {
            this.gvDocumentosBiblioteca.Columns[0].Visible = true;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow = this.gvDocumentosBiblioteca.Rows[index];
            TableCell tableCell = gridViewRow.Cells[0];
            try
            {
                Convert.ToInt32(tableCell.Text);
                TableCell tableCell2 = gridViewRow.Cells[3];
                string text3 = tableCell2.Text.Trim();
                text2 = text3;
                text2 = text2.Replace("&nbsp;", string.Empty);
                if (text2.Trim() != string.Empty)
                {
                    text = ConfigurationManager.AppSettings["RutaServer"];
                    str = "Operador/";
                    text = text + str + text3;
                    base.Response.Write("<script language='JavaScript'>window.open('" + text + "')</script>");
                    this.lblErrorDocumentos.Text = string.Empty;
                }
                else
                {
                    this.lblErrorDocumentos.Text = "No hay un documento asociado en el registro de seguimiento.";
                }
            }
            catch
            {
                this.lblErrorDocumentos.Text = "No hay un documento asociado en el registro de seguimiento.";
            }
            this.gvDocumentosBiblioteca.Columns[0].Visible = false;
            return;
        }
        if (e.CommandName == "Editar")
        {
            this.fntCargarCarpetas();
            this.gvDocumentosBiblioteca.Columns[0].Visible = true;
            int index2 = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow2 = this.gvDocumentosBiblioteca.Rows[index2];
            TableCell tableCell3 = gridViewRow2.Cells[0];
            this.Session["intIdBibliotecaActualizar"] = Convert.ToInt32(tableCell3.Text);
            TableCell tableCell4 = gridViewRow2.Cells[9];
            int num = Convert.ToInt32(tableCell4.Text.Trim());
            num++;
            this.Session["intVersion"] = num;
            this.Session["intEstado"] = 31;
            this.gvDocumentosBiblioteca.Columns[10].Visible = true;
            TableCell tableCell5 = gridViewRow2.Cells[10];
            int num2 = Convert.ToInt32(tableCell5.Text.Trim());
            this.Session["intCodigoDocumento"] = num2;
            this.fntInactivarPaneles(false);
            this.pnlAccionesBiblioteca.Visible = true;
            this.pnlBibliotecaNuevo.Visible = true;
            this.imgbEncontrar.Visible = false;
            this.imgbGravar.Visible = true;
            this.imgbCancelar.Visible = true;
            this.imgbNuevoDocumento.Visible = false;
            this.Session["Operacion"] = "NuevaVersion";
            this.Session["OperacionAdd"] = "Actualizar";
            this.ddlEstado.Visible = false;
            this.pnlDocumentosBiblioteca.Visible = true;
            this.fntCargarGrillaDocumentosBiblioteca(Convert.ToInt32(this.Session["intIdBiblioteca"].ToString()), string.Empty, Convert.ToInt32(this.Session["intCodigoDocumento"].ToString()));
            this.gvDocumentosBiblioteca.Columns[12].Visible = false;

      
        }
    }
    public bool fntModificarVersionBiblioteca(int intIdBiblioteca, int intIdEstado)
    {
        bool result = false;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntModificarVersionBiblioteca_bol(intIdBiblioteca, intIdEstado))
            {
                result = true;
            }
            this.blPaicma.Termina();
        }
        return result;
    }
    protected void gvBibliotecasAsignadas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvBibliotecasAsignadas.Columns[0].Visible = true;
        this.gvBibliotecasAsignadas.PageIndex = e.NewPageIndex;
        this.fntCargarGrillaBibliotecasDisponibles();
        this.gvBibliotecasAsignadas.Columns[0].Visible = false;
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.GridView1.Columns[0].Visible = true;
        this.GridView1.PageIndex = e.NewPageIndex;
        this.fntCargarGrillaBibliotecasDisponibles();
        this.GridView1.Columns[0].Visible = false;
    }
    protected void gvDocumentosBiblioteca_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvDocumentosBiblioteca.Columns[0].Visible = true;
        this.gvDocumentosBiblioteca.PageIndex = e.NewPageIndex;
        this.fntCargarGrillaDocumentosBiblioteca(Convert.ToInt32(this.Session["intIdBiblioteca"].ToString()), "ACTIVO", 0);
        this.gvDocumentosBiblioteca.Columns[0].Visible = false;
    }
    public int fntTraerMaximoCodigoDocumento()
    {
        int num = 0;
        string a = string.Empty;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultarMaximoCodigoDocumento("dsBibliotecaCodigo") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                a = this.blPaicma.myDataSet.Tables[0].Rows[0][0].ToString();
                if (a == string.Empty)
                {
                    num = 1;
                }
                else
                {
                    num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                    num++;
                }
            }
            this.blPaicma.Termina();
        }
        return num;
    }
    public bool fntTraerGrupoUsuariosBiblioteca(int intIdGrupoBiblioteca, string strNombreDocumento, string strGrupoBiblioteca)
    {
        string strCorreoTo = string.Empty;
        string strTitulo = string.Empty;
        string strTextoMensaje = string.Empty;

        bool envioCorreo = false;
        string fallidos = string.Empty;
        string Destinatarios = string.Empty;

        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaGrupoFuncionariosBiblioteca("strDsGrupoBiblioteca", intIdGrupoBiblioteca))
            {
                int count = this.blPaicma.myDataSet.Tables[0].Rows.Count;
                if (count > 0)
                {
                    string[] array = new string[count];
                    for (int i = 0; i < count; i++)
                    {
                        array[i] = this.blPaicma.myDataSet.Tables[0].Rows[i][5].ToString();
                    }
                    //strTitulo = " Esto es una Prueba ";
                    //strTextoMensaje = " Esto es una Prueba ";
                    var nombre = Session["strUsuario"].ToString();
                    strTitulo = " Biblioteca " + strGrupoBiblioteca + " , documento " + strNombreDocumento;
                    strTextoMensaje = "El Usuario: " + nombre;
                    strTextoMensaje = strTextoMensaje + " ha cargado un nuevo documento con el nombre  " + strNombreDocumento + " en la biblioteca " + strGrupoBiblioteca;
                    strTextoMensaje = strTextoMensaje + " adjuntando la siguiente observacion:  " + txtObservacion.Text;
                    for (int j = 0; j < count; j++)
                    {
                        strCorreoTo = array[j].ToString();
                        Destinatarios = Destinatarios + " - " + strCorreoTo;
                        envioCorreo = this.ftnEnviarCorreo(strCorreoTo, strTitulo, strTextoMensaje);
                        if (!envioCorreo)
                        {
                            fallidos = fallidos + " - " + strCorreoTo;
                        }
                    }
                }
            }
            this.blPaicma.Termina();
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntInsertarLogEmail_bol("DAICMA", Destinatarios, strTitulo, strTextoMensaje, "Bibliotecas", DateTime.Now, envioCorreo, fallidos))
                {
                    if (!envioCorreo)
                    {
                        this.lblRespuesta.Text = "Problemas para enviar a algunos correos: " + fallidos + " sin embargo ";
                    }

                }
            }
        }
        return true;
    }
    public bool ftnEnviarCorreo(string strCorreoTo, string strTitulo, string strTextoMensaje)
    {
        bool result = false;
        string text = string.Empty;
        string displayName = string.Empty;
        string host = string.Empty;
        string password = string.Empty;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaCorreo("strDsCorreo", 0) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                text = this.blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                displayName = this.blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                host = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                int port = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][2].ToString());
                password = this.blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                int num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][6].ToString());
                if (num == 1)
                {
                    MailMessage mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress(text, displayName);
                    mailMessage.To.Add(strCorreoTo);
                    mailMessage.Subject = strTitulo;
                    mailMessage.Body = strTextoMensaje;
                    mailMessage.IsBodyHtml = true;
                    SmtpClient smtpClient = new SmtpClient();
                    smtpClient.Host = host;
                    smtpClient.Port = port;
                    smtpClient.EnableSsl = false;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new NetworkCredential(text, password);
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    try
                    {
                        smtpClient.Send(mailMessage);
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
            }
            this.blPaicma.Termina();
        }
        return result;
    }
}