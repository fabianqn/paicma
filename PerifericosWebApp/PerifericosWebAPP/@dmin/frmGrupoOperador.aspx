﻿<%@ page title="" language="C#" masterpagefile="~/Plantillas/sisPAICMA.master" autoeventwireup="true" inherits="_dmin_frmGrupoOperador" Codebehind="frmGrupoOperador.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <asp:Panel ID="pnlEliminacion" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
                <tr>
                    <td>
                        <asp:Label ID="lblEliminacion" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSi" runat="server"  Text="SI" OnClick="btnSi_Click" />
                        <asp:Button ID="btnNo" runat="server"  Text="NO" OnClick="btnNo_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
                <tr>
                    <td>
                        <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" Text="Aceptar" OnClick="btnOk_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlMenuGrupoOperador" runat="server">
        <div class="Botones">
            <asp:ImageButton ID="imgbBuscar" runat="server" ImageUrl="~/Images/BuscarAccion.png" ToolTip="Buscar" OnClick="imgbBuscar_Click" />
            <asp:ImageButton ID="imgbNuevo" runat="server"  ImageUrl="~/Images/Nuevo.png" ToolTip="Nuevo" OnClick="imgbNuevo_Click" />
            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png" Visible="False" ToolTip="Editar" OnClick="imgbEditar_Click" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAccionesGrupoOperador" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server" ImageUrl="~/Images/buscar.png" ToolTip="Ejecutar Busqueda" OnClick="imgbEncontrar_Click" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png" ToolTip="Guardar" OnClick="imgbGravar_Click" />
            <asp:ImageButton ID="imgbCancelar" runat="server" ImageUrl="~/Images/cancelar.png" ToolTip="Cancelar" OnClick="imgbCancelar_Click" />
            <asp:ImageButton ID="imgbEliminar" runat="server" ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar" OnClick="imgbEliminar_Click" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlGrupoOperadorNuevo" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblNombreGrupo" runat="server" Text="Grupo"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNombreGrupo" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDescripcion" runat="server" Text="Descripción"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDescripcion" runat="server" MaxLength="250" TextMode="MultiLine" Height="50px" Width="90%" placeholder="Ingrese descripción, Máximo 250 caracteres"></asp:TextBox>
                      <asp:RegularExpressionValidator runat="server" ID="valInput"
    ControlToValidate="txtDescripcion"
    ValidationExpression="^[\s\S]{0,250}$"
    ErrorMessage="Ingrese Máximo 250 Caracteres" ForeColor="Red"
    Display="Dynamic">*Ingrese Máximo 250 Caracteres</asp:RegularExpressionValidator>  
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlEstado" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="PnlGrupoOperadorGrilla" runat="server">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
                <td align="center">
                    <asp:GridView ID="gvGrupoOperador" runat="server" AutoGenerateColumns="False" width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" AllowPaging="True" OnRowCommand="gvGrupoOperador_RowCommand" OnPageIndexChanging="gvGrupoOperador_PageIndexChanging" PageSize="15">
                        <AlternatingRowStyle CssClass="alt" />
                            <Columns>
                                <asp:BoundField DataField="gruOpe_Id" HeaderText="gruOpe_Id" />
                                <asp:BoundField DataField="gruOpe_Nombre" HeaderText="Grupo" />
                                <asp:BoundField DataField="gruOpe_Descripcion" HeaderText="Descripción" />
                                <asp:BoundField DataField="gruOpe_FechaCreacion" HeaderText="Fecha de Creación" />
                                <asp:BoundField DataField="est_Descripcion" HeaderText="Estado" />
                                <asp:ButtonField ButtonType="Image" CommandName="Seleccion" ImageUrl="~/Images/seleccionar.png" />
                            </Columns>
                        <PagerStyle CssClass="pgr" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblErrorGv" runat="server"></asp:Label>
                </td>
            </tr>
            </table>
        </div>
    </asp:Panel>




</asp:Content>

