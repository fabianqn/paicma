﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmWFNumeroAutorizacion : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                LblIdSolicitud.Text = Session["IdSolicitudRef"].ToString();
            }

        }
    }


    /// <summary>
    /// ☻☻ evento para guardar el numero de  aprobacion   de la solicitud 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardarNumero_Click(object sender, EventArgs e)
    {
        string asdd = string.Empty;

        long idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        int EstadoActualSolicitud = Convert.ToInt32(Session["EstadoActualSolicitud"]);
        int ProximoEstado = 0;
        if (EstadoActualSolicitud == EstadosSolicitud.faltaNumero)
        {
            ProximoEstado = EstadosSolicitud.AprobadoPorDARPLegalizar;
        }
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, ProximoEstado, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
            {
                if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", ProximoEstado, "WFSol_SolicitudComision", "idEstadoActual"))
                {
                    if (blPaicma.fntActualizaNumeroAprobacion_bol(idSolicitud, Convert.ToInt32(TxtNumero.Text)))
                    {
                        lblRespuesta.Text = " Se Asigno el Numero de Aprobacion ";
                        pnlRespuesta.Visible = true;
                        principal.Visible = false;
                       // Session["OperacionSolicitud"] = "GuardarContinuar";

                    }
                    else
                    {
                        lblRespuesta.Text = "Ocurrio un problema al guardar  el numero  de aprobacion ";
                        pnlRespuesta.Visible = true;
                        principal.Visible = false;
                       // Session["OperacionSolicitud"] = "ErrorSalir";
                        //Fallo actualizar estado en la comision
                    }

                }
                else
                {
                    lblRespuesta.Text = "Fallo al actualizar el estado en la Solicitud";
                    pnlRespuesta.Visible = true;
                    principal.Visible = false;
                   // Session["OperacionSolicitud"] = "ErrorSalir";
                    //Fallo actualizar estado en la comision
                }
            }
            else
            {
                lblRespuesta.Text = "Fallo al registrar el estado en la Solicitud";
                pnlRespuesta.Visible = true;
                principal.Visible = false;
              //  Session["OperacionSolicitud"] = "ErrorSalir";
                //fallo actualizar estado Solicitud
            }
        }
        blPaicma.Termina();



    }


    //☻☻  evento para  redireccionar usuario
    protected void btnOk_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/@dmin/frmWFSolicitudComision.aspx");
    }

    //☻☻ cancela la operacion 
    protected void btnGuardarSalir_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/@dmin/frmWFSolicitudComision.aspx");
    }
}