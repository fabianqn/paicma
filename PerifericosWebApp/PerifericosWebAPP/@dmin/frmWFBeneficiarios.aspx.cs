﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class _dmin_frmWFBeneficiarios : System.Web.UI.Page
{

    private blSisPAICMA blPaicma = new blSisPAICMA();
    //public TextBox[] arregloTextBoxs;
    //public DropDownList[] arregloCombos;
    public DropDownList[] ArregloBeneficiarios;
    public TextBox[] ArregloCantBeneficiarios;
    public TextBox[] ArregloComentarios;
    //public TextBox[] arregloTextBoxsLongitud;
    //public TextBox[] arregloTextBoxsOrden;
    //public TextBox[] arregloTextBoxsNombre;
    //public CheckBox[] arregloCheckBox;
    public int contadorControles;
    public int cont;
    public Label[] ListLabelBeneficiarios
    {
        get { return (Label[])Session["ListLabelBeneficiarios"]; }
        set { Session["ListLabelBeneficiarios"] = value; }
    }
    public Label[] ListLabelCantBeneficiarios
    {
        get { return (Label[])Session["ListLabelCantBeneficiarios"]; }
        set { Session["ListLabelCantBeneficiarios"] = value; }
    }
    public Label[] ListLabelComentarios
    {
        get { return (Label[])Session["ListLabelComentarios"]; }
        set { Session["ListLabelComentarios"] = value; }
    }
    public DropDownList[] ListDropDownBeneficarios
    {
        get { return (DropDownList[])Session["ListDropDownBeneficarios"]; }
        set { Session["ListDropDownBeneficarios"] = value; }
    }
    public TextBox[] ListTxtCantBeneficiarios
    {
        get { return (TextBox[])Session["ListTxtCantBeneficiarios"]; }
        set { Session["ListTxtCantBeneficiarios"] = value; }
    }
    public TextBox[] ListTxtComentarios
    {
        get { return (TextBox[])Session["ListTxtComentarios"]; }
        set { Session["ListTxtComentarios"] = value; }
    }

    protected int IntControles
    {
        get { return (int)Session["IntControles"]; }
        set { Session["IntControles"] = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                this.FillBeneficiarios();
            }



            ListLabelBeneficiarios = new Label[200];
            ListLabelCantBeneficiarios = new Label[200];
            ListLabelComentarios = new Label[200];

            ListDropDownBeneficarios = new DropDownList[200];
            ListTxtCantBeneficiarios = new TextBox[200];
            ListTxtComentarios = new TextBox[200];
            lblError.Text = string.Empty;
            this.IntControles = 0;
            contadorControles = 0;
        }
        cont = this.IntControles;

        try
        {
            cont = this.IntControles;
            ArregloBeneficiarios = new DropDownList[200];

            Array.Copy(ListDropDownBeneficarios, ArregloBeneficiarios, 200);


            ArregloCantBeneficiarios = new TextBox[200];
            Array.Copy(ListTxtCantBeneficiarios, ArregloCantBeneficiarios, 200);

            ArregloComentarios = new TextBox[200];
            Array.Copy(ListTxtComentarios, ArregloComentarios, 200);

            for (int i = 0; i < cont; i++)
            {
                //AgregarControles(TextBox txtnombreCampo, DropDownList ddlTipo, Label lblNombre, Label lblTipo, Label lblLongitud, TextBox txtLongitud, Label lblTitulo, Label lblOrden, TextBox txtOrden, TextBox txtLabel)
                AgregarControles(ListLabelBeneficiarios[i], ListLabelCantBeneficiarios[i], ListLabelComentarios[i], ArregloBeneficiarios[i], ArregloCantBeneficiarios[i], ArregloComentarios[i]);
            }
            Array.Copy(ArregloBeneficiarios, ListDropDownBeneficarios, 200);
            Array.Copy(ArregloCantBeneficiarios, ListTxtCantBeneficiarios, 200);
            Array.Copy(ArregloComentarios, ListTxtComentarios, 200);
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToBoolean(Session["soloVerSolicitud"]) == true)
        {
            PlanesDinamicos.Visible = false;
            panelBotones.Visible = false;
            panelSoloVer.Visible = true;
        }
    }


    protected void AgregarControles(Label lblEstrategia, Label lblActividad, Label lblIndicador, DropDownList DdlEstrategia, TextBox DdlActividad, TextBox DdlIndicador)
    {
        try
        {
            HtmlTableRow fila = new HtmlTableRow();
            HtmlTableRow fila2 = new HtmlTableRow();
            HtmlTableRow fila3 = new HtmlTableRow();
            HtmlTableRow filaSeparadora = new HtmlTableRow();
            HtmlTableCell celdaSeparadora = new HtmlTableCell();

            HtmlTableCell celdaA1 = new HtmlTableCell();
            HtmlTableCell celdaA2 = new HtmlTableCell();
            HtmlTableCell celdaB1 = new HtmlTableCell();
            HtmlTableCell celdaB2 = new HtmlTableCell();
            HtmlTableCell celdaC1 = new HtmlTableCell();
            HtmlTableCell celdaC2 = new HtmlTableCell();

            celdaSeparadora.ColSpan = 3;
            celdaSeparadora.Controls.Add(new LiteralControl("<hr/>"));
            filaSeparadora.Cells.Add(celdaSeparadora);


            celdaA1.Controls.Add(lblEstrategia);
            celdaA2.Controls.Add(DdlEstrategia);
            celdaA2.ColSpan = 2;

            celdaB1.Controls.Add(lblActividad);
            celdaB2.Controls.Add(DdlActividad);
            celdaB2.ColSpan = 2;

            celdaC1.Controls.Add(lblIndicador);
            celdaC2.Controls.Add(DdlIndicador);
            celdaC2.ColSpan = 2;

            fila.Cells.Add(celdaA1);
            fila.Cells.Add(celdaA2);

            fila2.Cells.Add(celdaB1);
            fila2.Cells.Add(celdaB2);

            fila3.Cells.Add(celdaC1);
            fila3.Cells.Add(celdaC2);

            PlanesDinamicos.Rows.Add(fila);
            PlanesDinamicos.Rows.Add(fila2);
            PlanesDinamicos.Rows.Add(fila3);
            PlanesDinamicos.Rows.Add(filaSeparadora);



            //PlanesDinamicos.Rows.Remove()
            //    PlanesDinamicos

        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }

    }
    protected void imgbNuevoPlan_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int numeroRegistro = this.IntControles;
            ArregloBeneficiarios = new DropDownList[200];
            Array.Copy(ListDropDownBeneficarios, ArregloBeneficiarios, 200);

            ArregloCantBeneficiarios = new TextBox[200];
            Array.Copy(ListTxtCantBeneficiarios, ArregloCantBeneficiarios, 200);

            ArregloComentarios = new TextBox[200];
            Array.Copy(ListTxtComentarios, ArregloComentarios, 200);

            //Cargamos los La data para asignarla despues
            DataTable dataBeneficiarios = new DataTable();

            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarPorEstado("datosBeneficiarios", "WFSol_Beneficio", "estatus", "ACT", "nombre"))
                {
                    dataBeneficiarios = blPaicma.myDataSet.Tables["datosBeneficiarios"];
                    //if (blPaicma.fntConsultarPorEstado("datosActividad", "WFSol_Actividad", "estadoAct", "ACT", "nombreActividad"))
                    //{
                    //    dataActividad = blPaicma.myDataSet.Tables["datosActividad"];
                    //    if (blPaicma.fntConsultarPorEstado("datosIndicadores", "WFSol_Indicador", "estadoInd", "ACT", "nombreIndicador"))
                    //    {
                    //        dataIndicador = blPaicma.myDataSet.Tables["datosIndicadores"];
                    //    }
                    //    else
                    //    {
                    //        // no pudo cargar Listado de Indicador
                    //    }
                    //}
                    //else
                    //{
                    //    //No Pudo cargar Listado de actividad
                    //}
                }
                else
                {
                    //No se pudieron cargar los dropdown
                }
            }
            else
            {
                //No se ha podido conectar con la base de datos
            }
            blPaicma.Termina();

            //creamos los dropDownList
            DropDownList _ddlBeneficiarios = new DropDownList();
            _ddlBeneficiarios.ID = "ddlBeneficiarios" + numeroRegistro.ToString();
            _ddlBeneficiarios.DataMember = "datosBeneficiarios";
            _ddlBeneficiarios.DataSource = dataBeneficiarios;
            _ddlBeneficiarios.DataValueField = "idBeneficio";
            _ddlBeneficiarios.DataTextField = "nombre";
            _ddlBeneficiarios.DataBind();
            _ddlBeneficiarios.Items.Insert(_ddlBeneficiarios.Attributes.Count, "Seleccione...");
            ArregloBeneficiarios[numeroRegistro] = _ddlBeneficiarios;
            Array.Copy(ArregloBeneficiarios, ListDropDownBeneficarios, 200);

            TextBox TxtCantBeneficiarios = new TextBox();
            TxtCantBeneficiarios.ID = "txtCantBeneficiarios" + numeroRegistro.ToString();
            ArregloCantBeneficiarios[numeroRegistro] = TxtCantBeneficiarios;
            Array.Copy(ArregloCantBeneficiarios, ListTxtCantBeneficiarios, 200);

            TextBox TxtComentarios = new TextBox();
            TxtComentarios.ID = "txtComentarios" + numeroRegistro.ToString();
            ArregloComentarios[numeroRegistro] = TxtComentarios;
            Array.Copy(ArregloComentarios, ListTxtComentarios, 200);

            Label labelBeneficiarios = new Label();
            labelBeneficiarios.ID = "lblBeneficiarios" + numeroRegistro.ToString();
            labelBeneficiarios.Text = "Beneficiarios ";
            ListLabelBeneficiarios[numeroRegistro] = labelBeneficiarios;

            Label LabelCantBeneficiarios = new Label();
            LabelCantBeneficiarios.ID = "lblCantidadBeneficiarios" + numeroRegistro.ToString();

            LabelCantBeneficiarios.Text = "Cantidad ";
            ListLabelCantBeneficiarios[numeroRegistro] = LabelCantBeneficiarios;

            Label labelComentarioAdi = new Label();
            labelComentarioAdi.ID = "lblIndicador" + numeroRegistro.ToString();
            labelComentarioAdi.Text = "Comentario Adicional ";
            ListLabelComentarios[numeroRegistro] = labelComentarioAdi;

            ///agregamos los controles al panel y tabla
            ///NombreCampo, Combo, Label nuevo, label Combo, label Longitud, longitud, 
            AgregarControles(labelBeneficiarios, LabelCantBeneficiarios, labelComentarioAdi, _ddlBeneficiarios, TxtCantBeneficiarios, TxtComentarios);
            this.IntControles++;

            lblError.Text = string.Empty;
            imgbEliminarPlan.Enabled = true;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }

    protected void btnGuardarCont_Click(object sender, EventArgs e)
    {
        Session["OperacionSolicitud"] = "GuardarContinuar";
        btnOk.Text = "Continuar";
        this.guardarBeneficiarios();

    }
    protected void btnGuardarSalir_Click(object sender, EventArgs e)
    {
        Session["OperacionSolicitud"] = "GuardarSalir";
        btnOk.Text = "Terminar";
        this.guardarBeneficiarios();
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmWFSolicitudComision.aspx");
    }

    public void guardarBeneficiarios()
    {

        bool guardoCorrecto = true;
        bool primeraVes = true;
        int intCantidadControles = this.IntControles;
        ArregloBeneficiarios = new DropDownList[200];
        Array.Copy(ListDropDownBeneficarios, ArregloBeneficiarios, 200);

        ArregloCantBeneficiarios = new TextBox[200];
        Array.Copy(ListTxtCantBeneficiarios, ArregloCantBeneficiarios, 200);

        ArregloComentarios = new TextBox[200];
        Array.Copy(ListTxtComentarios, ArregloComentarios, 200);
        Int64 idLegalizacion = Convert.ToInt64(Session["IdLegalizacionRef"]);

        Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        for (int i = 0; i < intCantidadControles; i++)
        {
            
            if(guardoCorrecto){
                

                DropDownList BeneficiariosGuardar = new DropDownList();
                TextBox CantBeneficiariosGuardar = new TextBox();
                TextBox Comentarios = new TextBox();

                BeneficiariosGuardar = ArregloBeneficiarios[i];
                CantBeneficiariosGuardar = ArregloCantBeneficiarios[i];
                Comentarios = ArregloComentarios[i];

                int idUsuarioLogin = Convert.ToInt32(Session["IdUsuario"]);
                DateTime creacion = DateTime.Now;

                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    if (blPaicma.fntIngresarBeneficiarios_bol(idLegalizacion, Convert.ToInt32(BeneficiariosGuardar.SelectedValue), Convert.ToInt32(CantBeneficiariosGuardar.Text), Comentarios.Text,creacion))
                    {
                        if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, EstadosSolicitud.LegIncompletoSoporte, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
                        {
                            if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", EstadosSolicitud.LegIncompletoSoporte, "WFSol_SolicitudComision", "idEstadoActual"))
                            {
                                guardoCorrecto = true;
                                primeraVes = false;
                            }
                            else
                            {
                                //Fallo actualizar estado en la comision
                            }

                        }
                        else
                        {
                            //fallo actualizar estado Solicitud
                        }
                    }
                    else
                    {
                         guardoCorrecto = false;
                         primeraVes = false;
                    }
                }
                else
                {
                     guardoCorrecto = false;
                     primeraVes = false;
                }
                this.blPaicma.Termina();
            }
        }



        int cantidaditems = Convert.ToInt32(gvBeneficiario.Rows.Count);


        if (guardoCorrecto && !primeraVes || cantidaditems > 0)
        {

            //Agregado para que siempre actualice
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, EstadosSolicitud.LegIncompletoSoporte, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
                {
                    if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", EstadosSolicitud.LegIncompletoSoporte, "WFSol_SolicitudComision", "idEstadoActual"))
                    {
                        lblRespuesta.Text = "Beneficiarios agregados con éxito";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        Session["IdLegalizacionRef"] = idLegalizacion;
                    }
                    else
                    {
                        //Fallo actualizar estado en la comision
                    }

                }
                else
                {
                    //fallo actualizar estado Solicitud
                }
            }
            else
            {
                //No se pudo establecer conexion.
            }
            this.blPaicma.Termina();
        }
        else
        {

            if (guardoCorrecto && primeraVes)
            {
                lblRespuesta.Text = "Se presento un problema al conectar con la base de datos, Intente Nuevamente.";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";
            }
            if (!guardoCorrecto && !primeraVes)
            {
                lblRespuesta.Text = "Se presento un problema al intentar guardar al menos uno de los beneficiarios, Intente Nuevamente.";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";
            }

        } 
    }
   
    //  metodo para cargan oos beneficiarios 
    public void FillBeneficiarios()
    {
        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idLegalizacion = Convert.ToInt64(Session["IdLegalizacionRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarBeneficiarios("datosBenificiario", "0", idLegalizacion, 0))
                {
                    if (blPaicma.myDataSet.Tables["datosBenificiario"].Rows.Count > 0)
                    {
                        this.gvBeneficiario.DataMember = "datosBenificiario";
                        this.gvBeneficiario.DataSource = this.blPaicma.myDataSet;
                        this.gvBeneficiario.DataBind();
                    }
                }
                else
                {
                    //Imposible Consultar lista de planes
                    lblRespuesta.Text = "Se ha presentado un problema al consultar lista de Beneficiarios, intente de nuevo";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    Session["OperacionSolicitud"] = "ErrorSalir";
                }
            }
            else
            {
                //No pudo conectar a la base de datos
                lblRespuesta.Text = "Se ha presentado un problema al consultar lista de Beneficiarios, no se ha podido conectar a la base de datos";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";

            }
            blPaicma.Termina();
        }
        else
        {
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
    }

    //☻☻
    protected void imgbEliminarPlan_Click(object sender, ImageClickEventArgs e)
    {

        if (this.IntControles == 0)
        {
            imgbEliminarPlan.Enabled = false;
        }
        else
        {
            this.IntControles--;
            PlanesDinamicos.Rows.RemoveAt(PlanesDinamicos.Rows.Count - 1);
            PlanesDinamicos.Rows.RemoveAt(PlanesDinamicos.Rows.Count - 1);
            PlanesDinamicos.Rows.RemoveAt(PlanesDinamicos.Rows.Count - 1);
            PlanesDinamicos.Rows.RemoveAt(PlanesDinamicos.Rows.Count - 1);
            if (this.IntControles == 0)
            {
                imgbEliminarPlan.Enabled = false;
            }
        }
    }


    //☻☻ metodo para vcalidar los estados de la session **************************************************************************************
    protected void btnOk_Click(object sender, EventArgs e)
    {
        string operacionActual = Session["OperacionSolicitud"].ToString();
        if (operacionActual == "GuardarContinuar")
        {
            /// validar si cambia el estado  de la operacion 
            Session["OperacionSolicitud"] = "Inicio";
            Session["soloVerSolicitud"] = false;

            Response.Redirect("SoporteLegalizacion.aspx");
        }
        if (operacionActual == "GuardarSalir")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "ErrorSalir")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "Inicio")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
    }
    protected void gvBeneficios_PreRender(object sender, EventArgs e)
    {
        if (gvBeneficiario.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvBeneficiario.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvBeneficiario.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvBeneficiario.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }


    protected void gvBeneficiarios_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        int intIndex = Convert.ToInt32(e.CommandArgument);
        GridViewRow selectedRow = gvBeneficiario.Rows[intIndex];
        Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        HiddenField hdnField = (HiddenField)selectedRow.FindControl("HiddenFieldID");
        Int64 IdBeneficiario = Convert.ToInt64(hdnField.Value);

     //   Session["intIdBeneficiarioEditar"] = IdBeneficiario;

        if (e.CommandName == "EditarBeneficiario")
        {
            Session["IdBeneficiario"] = IdBeneficiario;
            Response.Redirect("frmWFEditExistingBeneficiario.aspx");
        }
        if (e.CommandName == "EliminarBeneficiario")
        {
            EliminarBeneficiario(IdBeneficiario);
        }


    }

    /// <summary>
    /// ☻☻  metodo para eliminar los benificiarios 
    /// </summary>
    /// <param name="idBeneficio"></param>
    private void EliminarBeneficiario(Int64 idBeneficio)
    {

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntEliminarBeneficiario(idBeneficio))
            {
                this.FillBeneficiarios();
            }
            else
            {
                //Imposible Consultar lista de planes
                lblRespuesta.Text = "Se ha presentado un problema al eliminar el beneficiario ";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";
            }
        }
        else
        {
            //No pudo conectar a la base de datos
            lblRespuesta.Text = "Se ha presentado un problema en el servicio";
            principal.Visible = false;
            pnlRespuesta.Visible = true;
            Session["OperacionSolicitud"] = "ErrorSalir";

        }
        blPaicma.Termina();
    }



}