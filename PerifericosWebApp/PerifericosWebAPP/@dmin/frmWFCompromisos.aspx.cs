﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class _dmin_frmWFCompromisos : System.Web.UI.Page
{

    private blSisPAICMA blPaicma = new blSisPAICMA();
    //public TextBox[] arregloTextBoxs;
    //public TextBox[] arregloCombos;
    public TextBox[] ArregloCompromiso;
    public TextBox[] ArregloFechaCompromiso;
    public TextBox[] ArregloResponsable;




    //public TextBox[] arregloTextBoxsLongitud;
    //public TextBox[] arregloTextBoxsOrden;
    //public TextBox[] arregloTextBoxsNombre;
    //public CheckBox[] arregloCheckBox;
    public int contadorControles;
    public int cont;
    public Label[] ListLabelCompromiso
    {
        get { return (Label[])Session["ListLabelCompromiso"]; }
        set { Session["ListLabelCompromiso"] = value; }
    }
    public Label[] ListLabelFechaC
    {
        get { return (Label[])Session["ListLabelFechaC"]; }
        set { Session["ListLabelFechaC"] = value; }
    }
    public Label[] ListLabelResponsable
    {
        get { return (Label[])Session["ListLabelResponsable"]; }
        set { Session["ListLabelResponsable"] = value; }
    }
    public TextBox[] ListTxtCompromiso
    {
        get { return (TextBox[])Session["ListDropDownEstrategia"]; }
        set { Session["ListDropDownEstrategia"] = value; }
    }
    public TextBox[] ListTxtFechaC
    {
        get { return (TextBox[])Session["ListDropDownActividad"]; }
        set { Session["ListDropDownActividad"] = value; }
    }

    public TextBox[] ListTxtResponsable
    {
        get { return (TextBox[])Session["ListtxtResponsable"]; }
        set { Session["ListtxtResponsable"] = value; }
    }
    protected int NumeroControles
    {
        get { return (int)Session["NumeroControles"]; }
        set { Session["NumeroControles"] = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                this.FillCompromisos();
            }
            ListLabelCompromiso = new Label[200];
            ListLabelFechaC = new Label[200];
            ListLabelResponsable = new Label[200];



            ListTxtCompromiso = new TextBox[200];
            ListTxtFechaC = new TextBox[200];
            ListTxtResponsable = new TextBox[200];
            lblError.Text = string.Empty;
            this.NumeroControles = 0;
            contadorControles = 0;
        }
        cont = this.NumeroControles;

        try
        {
            cont = this.NumeroControles;
            ArregloCompromiso = new TextBox[200];
            Array.Copy(ListTxtCompromiso, ArregloCompromiso, 200);

            ArregloFechaCompromiso = new TextBox[200];
            Array.Copy(ListTxtFechaC, ArregloFechaCompromiso, 200);

            ArregloResponsable = new TextBox[200];
            Array.Copy(ListTxtResponsable, ArregloResponsable, 200);


            for (int i = 0; i < cont; i++)
            {
                //AgregarControles(TextBox txtnombreCampo, DropDownList ddlTipo, Label lblNombre, Label lblTipo, Label lblLongitud, TextBox txtLongitud, Label lblTitulo, Label lblOrden, TextBox txtOrden, TextBox txtLabel)
                AgregarControles(ListLabelCompromiso[i], ListLabelFechaC[i], ListLabelResponsable[i], ArregloCompromiso[i], ArregloFechaCompromiso[i], ArregloResponsable[i]);
            }
            Array.Copy(ArregloCompromiso, ListTxtCompromiso, 200);
            Array.Copy(ArregloFechaCompromiso, ListTxtFechaC, 200);
            Array.Copy(ArregloResponsable, ListTxtResponsable, 200);
          
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToBoolean(Session["soloVerSolicitud"]) == true)
        {
            PlanesDinamicos.Visible = false;
            panelBotones.Visible = false;
            panelSoloVer.Visible = true;
        }
    }


    protected void AgregarControles(Label lblEstrategia, Label lblActividad, Label lblResponsable, TextBox DdlEstrategia, TextBox DdlActividad, TextBox DllResponsable)
    {
        try
        {
            HtmlTableRow fila = new HtmlTableRow();
            HtmlTableRow fila2 = new HtmlTableRow();
            HtmlTableRow fila3 = new HtmlTableRow();

            HtmlTableRow filaSeparadora = new HtmlTableRow();
            HtmlTableCell celdaSeparadora = new HtmlTableCell();

            HtmlTableCell celdaA1 = new HtmlTableCell();
            HtmlTableCell celdaA2 = new HtmlTableCell();
            HtmlTableCell celdaB1 = new HtmlTableCell();
            HtmlTableCell celdaB2 = new HtmlTableCell();
            HtmlTableCell celdaC1 = new HtmlTableCell();
            HtmlTableCell celdaC2 = new HtmlTableCell();


            celdaSeparadora.ColSpan = 3;
            celdaSeparadora.Controls.Add(new LiteralControl("<hr/>"));
            filaSeparadora.Cells.Add(celdaSeparadora);


            celdaA1.Controls.Add(lblEstrategia);
            celdaA2.Controls.Add(DdlEstrategia);
            celdaA2.ColSpan = 2;

            celdaB1.Controls.Add(lblActividad);
            celdaB2.Controls.Add(DdlActividad);
            celdaB2.ColSpan = 2;

            celdaC1.Controls.Add(lblResponsable);
            celdaC2.Controls.Add(DllResponsable);
            celdaC2.ColSpan = 2;



            fila.Cells.Add(celdaA1);
            fila.Cells.Add(celdaA2);

            fila2.Cells.Add(celdaB1);
            fila2.Cells.Add(celdaB2);

            fila3.Cells.Add(celdaC1);
            fila3.Cells.Add(celdaC2);

            PlanesDinamicos.Rows.Add(fila);
            PlanesDinamicos.Rows.Add(fila2);
            PlanesDinamicos.Rows.Add(fila3);
            PlanesDinamicos.Rows.Add(filaSeparadora);



            //PlanesDinamicos.Rows.Remove()
            //    PlanesDinamicos

        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }

    }
    protected void imgbNuevoPlan_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int numeroRegistro = this.NumeroControles;
            ArregloCompromiso = new TextBox[200];
            Array.Copy(ListTxtCompromiso, ArregloCompromiso, 200);

            ArregloFechaCompromiso = new TextBox[200];
            Array.Copy(ListTxtFechaC, ArregloFechaCompromiso, 200);

            ArregloResponsable = new TextBox[200];
            Array.Copy(ListTxtResponsable, ArregloResponsable, 200);



            TextBox txtCompromiso = new TextBox();
            txtCompromiso.ID = "txtCompromiso" + numeroRegistro.ToString();
            ArregloCompromiso[numeroRegistro] = txtCompromiso;
             txtCompromiso.Attributes.CssStyle.Add("width","95%");
        
            Array.Copy(ArregloCompromiso, ListTxtCompromiso, 200);


            TextBox txtFechaCompromiso = new TextBox();
            txtFechaCompromiso.ID = "txtFechaCompromiso" + numeroRegistro.ToString();
            txtFechaCompromiso.CssClass = "pluginFecha";
            ArregloFechaCompromiso[numeroRegistro] = txtFechaCompromiso;
            Array.Copy(ArregloFechaCompromiso, ListTxtFechaC, 200);



            TextBox txtResponsable = new TextBox();
            txtResponsable.ID = "txtResponsable" + numeroRegistro.ToString();
            ArregloResponsable[numeroRegistro] = txtResponsable;
            txtResponsable.Attributes.CssStyle.Add("width", "95%");
            Array.Copy(ArregloResponsable, ListTxtResponsable, 200);




            Label LabelEstrategia = new Label();
            LabelEstrategia.ID = "lblEstrategia" + numeroRegistro.ToString();
            LabelEstrategia.Text = "Compromiso ";
            ListLabelCompromiso[numeroRegistro] = LabelEstrategia;

            Label LabelActividad = new Label();
            LabelActividad.ID = "lblActividad" + numeroRegistro.ToString();
            LabelActividad.Text = "Fecha ";
            ListLabelFechaC[numeroRegistro] = LabelActividad;


            Label LabelResponsable= new Label();
            LabelResponsable.ID = "lblResponsable" + numeroRegistro.ToString();
            LabelResponsable.Text = "Responsable ";
            ListLabelResponsable[numeroRegistro] = LabelResponsable;




            //Label LabelIndicador = new Label();
            //LabelIndicador.ID = "lblIndicador" + numeroRegistro.ToString();
            //LabelIndicador.Text = "Indicador ";
            //ListLabelIndicador[numeroRegistro] = LabelIndicador;

            ///agregamos los controles al panel y tabla
            ///NombreCampo, Combo, Label nuevo, label Combo, label Longitud, longitud, 
            AgregarControles(LabelEstrategia, LabelActividad,LabelResponsable,   txtCompromiso, txtFechaCompromiso , txtResponsable);
            this.NumeroControles++;

            lblError.Text = string.Empty;
            imgbEliminarPlan.Enabled = true;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }

    protected void btnGuardarCont_Click(object sender, EventArgs e)
    {
        Session["OperacionSolicitud"] = "GuardarContinuar";
        btnOk.Text = "Continuar";
        this.guardarCompromisos();

    }
   
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmWFSolicitudComision.aspx");
    }

    public void guardarCompromisos()
    {

        
        bool guardoCorrecto = true;
        bool primeraVes = true;
        int intCantidadControles = this.NumeroControles;
        ArregloCompromiso = new TextBox[200];
        Array.Copy(ListTxtCompromiso, ArregloCompromiso, 200);

        ArregloFechaCompromiso = new TextBox[200];
        Array.Copy(ListTxtFechaC, ArregloFechaCompromiso, 200);


        ArregloResponsable = new TextBox[200];
        Array.Copy(ListTxtResponsable, ArregloResponsable, 200);



        Int64 idLegalizacion = Convert.ToInt64(Session["IdLegalizacionRef"]);
        Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);

        for (int i = 0; i < intCantidadControles; i++)
        {
            
            if(guardoCorrecto){
                TextBox TxtCompromisoGuardar = new TextBox();
                TextBox TxtFechaGuardar = new TextBox();
                TextBox TxtResponsable = new TextBox();


                TxtCompromisoGuardar = ArregloCompromiso[i];
                TxtFechaGuardar = ArregloFechaCompromiso[i];
                TxtResponsable = ArregloResponsable[i];


                DateTime creacion = DateTime.Now;

                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    if (blPaicma.fntIngresarCompromisos_bol(idLegalizacion,TxtCompromisoGuardar.Text, Convert.ToDateTime(TxtFechaGuardar.Text),creacion ,TxtResponsable.Text))
                    {
                        if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, EstadosSolicitud.LegIncompletaBeneficiarios, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
                        {
                            if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", EstadosSolicitud.LegIncompletaBeneficiarios, "WFSol_SolicitudComision", "idEstadoActual"))
                            {
                                guardoCorrecto = true;
                                primeraVes = false;
                            }
                            else
                            {
                                //Fallo actualizar estado en la comision
                            }

                        }
                        else
                        {
                            //fallo actualizar estado Solicitud
                        }
                    }
                    else
                    {
                        guardoCorrecto = false;
                        primeraVes = false;
                    }
                }
                else
                {
                     guardoCorrecto = false;
                     primeraVes = false;
                }
                this.blPaicma.Termina();
            }
        }



        int cantidaditems = Convert.ToInt32(gvCompromiso.Rows.Count);


        if (guardoCorrecto && !primeraVes || cantidaditems > 0)
        {

            //Agregado para que siempre actualice
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, EstadosSolicitud.LegIncompletaBeneficiarios, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
                {
                    if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", EstadosSolicitud.LegIncompletaBeneficiarios, "WFSol_SolicitudComision", "idEstadoActual"))
                    {
                        lblRespuesta.Text = "Compromisos agregados con éxito";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        Session["IdLegalizacionRef"] = idLegalizacion;
                    }
                    else
                    {
                        //Fallo actualizar estado en la comision
                    }

                }
                else
                {
                    //fallo actualizar estado Solicitud
                }
            }
            else
            {
                //No se pudo establecer conexion.
            }
            this.blPaicma.Termina();
        }
        else
        {
            if (guardoCorrecto && primeraVes)
            {
                lblRespuesta.Text = "Se presento un problema al conectar con la base de datos, Intente Nuevamente.";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";
            }
            if (!guardoCorrecto && !primeraVes)
            {
                lblRespuesta.Text = "Se presento un problema al intentar guardar al menos uno de los compromisos, Intente Nuevamente.";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";
            }
        }

    }

    protected void gvCompromiso_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        int intIndex = Convert.ToInt32(e.CommandArgument);
        GridViewRow selectedRow = gvCompromiso.Rows[intIndex];
        Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        HiddenField hdnField = (HiddenField)selectedRow.FindControl("HiddenFieldID");
        Int64 IdCompromiso =Convert.ToInt64(hdnField.Value);

        Session["intIdCompromisoEditar"] = IdCompromiso;

        if (e.CommandName == "EditarCompromiso")
        {
            Session["IdCompromiso"] = IdCompromiso;
            Response.Redirect("frmWFExistenteCompromisos.aspx");
        }
        if (e.CommandName == "EliminarCompromiso")
        {
            EliminarCompromiso(IdCompromiso);
        }

    }

    public void FillCompromisos()
    {
        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idLegalizacion = Convert.ToInt64(Session["IdLegalizacionRef"]);
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarCompromisos("datosCompromiso", "0", idLegalizacion, 0))
                {
                    if (blPaicma.myDataSet.Tables["datosCompromiso"].Rows.Count > 0)
                    {
                        this.gvCompromiso.DataMember = "datosCompromiso";
                        this.gvCompromiso.DataSource = this.blPaicma.myDataSet;
                        this.gvCompromiso.DataBind();
                    }
                }
                else
                {
                    //Imposible Consultar lista de planes
                    lblRespuesta.Text = "Se ha presentado un problema al consultar lista de Compromisos, intente de nuevo";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    Session["OperacionSolicitud"] = "ErrorSalir";
                }
            }
            else
            {
                //No pudo conectar a la base de datos
                lblRespuesta.Text = "Se ha presentado un problema al consultar lista de Compromisos, no se ha podido conectar a la base de datos";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";

            }
            blPaicma.Termina();
        }
        else
        {
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
    }





    protected void imgbEliminarPlan_Click(object sender, ImageClickEventArgs e)
    {

        if (this.NumeroControles == 0)
        {
            imgbEliminarPlan.Enabled = false;
        }
        else
        {
            this.NumeroControles--;
            PlanesDinamicos.Rows.RemoveAt(PlanesDinamicos.Rows.Count - 1);
            PlanesDinamicos.Rows.RemoveAt(PlanesDinamicos.Rows.Count - 1);
            PlanesDinamicos.Rows.RemoveAt(PlanesDinamicos.Rows.Count - 1);
            if (this.NumeroControles == 0)
            {
                imgbEliminarPlan.Enabled = false;
            }
        }
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        string operacionActual = Session["OperacionSolicitud"].ToString();
        if (operacionActual == "GuardarContinuar")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Session["soloVerSolicitud"] = false;
            Response.Redirect("frmWFBeneficiarios.aspx");
        }
        if (operacionActual == "GuardarSalir")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "ErrorSalir")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "Inicio")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("../Default.aspx");
        }
    }
    protected void gvCompromiso_PreRender(object sender, EventArgs e)
    {
        if (gvCompromiso.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvCompromiso.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvCompromiso.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvCompromiso.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
    protected void btnGuardarVolver_Click(object sender, EventArgs e)
    {
        Session["OperacionSolicitud"] = "GuardarSalir";
        btnOk.Text = "Terminar";
        this.guardarCompromisos();
    }


    /// <summary>
    /// ☻☻ metodo para eliminar los  compromisos 
    /// </summary>
    /// <param name="idCompromiso"></param>
    private void EliminarCompromiso(Int64 idCompromiso)
    {
 
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntEliminarCompromiso(idCompromiso))
                {
                    this.FillCompromisos();
                }
                else
                {
                    //Imposible Consultar lista de planes
                    lblRespuesta.Text = "Se ha presentado un problema al eliminar el compromiso ";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                     Session["OperacionSolicitud"] = "ErrorSalir";
                }
            }
            else
            {
                //No pudo conectar a la base de datos
                lblRespuesta.Text = "Se ha presentado un problema en el servicio";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";

            }
            blPaicma.Termina();
    }


}