﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DevExpress.XtraReports.UI;
using System.IO;

public partial class _dmin_frmUploadReport : System.Web.UI.Page
{
    //protected System.Web.UI.HtmlControls.HtmlInputFile File1;
    //protected System.Web.UI.HtmlControls.HtmlInputButton Submit1;
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!this.Page.IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }

        }
    }

    override protected void OnInit(EventArgs e)
    {
        // 
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        // 
        InitializeComponent();
        base.OnInit(e);
    }

    private void InitializeComponent()
    {
        this.btnCargar.ServerClick += new System.EventHandler(this.btnCargar_ServerClick);
        this.Load += new System.EventHandler(this.Page_Load);

    }


    #region Funciones de Reportes
    private string StoreReportToFile(XtraReport report, string ruta)
    {
        string path = ruta;
        report.SaveLayout(path);
        return path;
    }

    private MemoryStream StoreReportToStream(XtraReport report)
    {
        MemoryStream stream = new MemoryStream();
        report.SaveLayout(stream);
        return stream;
    }

    //// Load a report from a file. 
    //private void LoadReportFromFile(XtraReport report, string filePath)
    //{
    //    if (File.Exists(filePath))
    //    {
    //        report.LoadLayout(filePath);
    //    }
    //    else
    //    {
    //        Console.WriteLine("The source file does not exist.");
    //    }
    //}

    //// Load a report from a stream. 
    //private void LoadReportFromStream(XtraReport report, MemoryStream stream)
    //{
    //    report.LoadLayout(stream);
    //}

    // Create a report from a file. 
    private XtraReport CreateReportFromFile(string filePath)
    {
        XtraReport report = XtraReport.FromFile(filePath, true);
        return report;
    }

    // Create a report from a stream. 
    private XtraReport CreateReportFromStream(MemoryStream stream)
    {
        XtraReport report = XtraReport.FromStream(stream, true);
        return report;
    }
    #endregion

    private void btnCargar_ServerClick(object sender, System.EventArgs e)
    {
        if ((archivoReporte.PostedFile != null) && (archivoReporte.PostedFile.ContentLength > 0) && (txtNombreFormulario.Text.Trim() != string.Empty) && (txtObservacion.InnerText.Trim() != string.Empty))
        {
            //string fn = System.IO.Path.GetFileName(archivoReporte.PostedFile.FileName);
            string strExtDocumento = Path.GetExtension(archivoReporte.PostedFile.FileName);
            string unique = Guid.NewGuid().ToString();
            //fn = fn + DateTime.Now.ToString("YYYYMMDDHHMMSS");
            string SaveLocation = Server.MapPath("~") + "@reportes\\" + unique + strExtDocumento;
            try
            {
                archivoReporte.PostedFile.SaveAs(SaveLocation);
                // var cadena = StoreReportToFile(reporte, SaveLocation);
                MemoryStream aguardar = new MemoryStream(File.ReadAllBytes(SaveLocation));


                aguardar = StoreReportToStream(CreateReportFromFile(SaveLocation));
                byte[] reporteGuardarBD = aguardar.ToArray();
                if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
                {
                    if (this.blPaicma.fntInsertarFormReportes_bol(txtNombreFormulario.Text.Trim(), txtObservacion.InnerText.Trim(), true, 1, reporteGuardarBD, SaveLocation, 7, 7))
                    {
                        lblError.Text = "Reporte Cargado Exitosamente";

                    }
                    else
                    {
                        lblError.Text = "No se ha cargado, intente mas tarde";
                        //no guardo en base de datos
                    }
                }
                else
                {
                    lblError.Text = "No fue posible conectar con la base de datos";
                    //No se pudo inicializar

                }
                this.blPaicma.Termina();

            }
            catch (Exception ex)
            {
                lblError.Text = "Ocurrio un error al cargar el archivo, intente de nuevo - " + "Error: " + ex.Message;
                //Note: Exception.Message returns a detailed message that describes the current exception. 
                //For security reasons, we do not recommend that you return Exception.Message to end users in 
                //production environments. It would be better to return a generic error message. 
            }
            this.blPaicma.Termina();
            lblError.Style.Add("background-color", "#FBEE57");
            lblError.Visible = true;
            HtmlMeta meta = new HtmlMeta();
            meta.HttpEquiv = "Refresh";
            meta.Content = "3;url=frmReportList.aspx";
            this.Page.Controls.Add(meta);
            lblRedirigir.Text = "Sera redirigido en 3 segundos";
            lblRedirigir.Visible = true;
        }
        else
        {
            lblError.Text = "Faltan Datos por completar o seleccionar el archivo a cargar";
            lblError.Style.Add("background-color", "#FBEE57");
            lblError.Visible = true;
        }

    }

}