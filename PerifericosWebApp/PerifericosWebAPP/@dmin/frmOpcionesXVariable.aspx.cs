﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class _dmin_frmOpcionesXVariable : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {

            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                ftnValidarPermisos();

            }
        }
        fntCargarGrillaVariables(string.Empty);
    }
    
    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }

    public void ftnValidarPermisos()
    {
        //int intIdFormulario = 0;
        string strBuscar = string.Empty;
        string strNuevo = string.Empty;
        string strEditar = string.Empty;
        string strEliminar = string.Empty;

        //obtiene el nombre de la pagina actual
        string[] strRutaPagina = HttpContext.Current.Request.RawUrl.Split('/');
        string strNombrePagina = strRutaPagina[strRutaPagina.GetUpperBound(0)];
        strRutaPagina = strNombrePagina.Split('?');
        strNombrePagina = strRutaPagina[strRutaPagina.GetLowerBound(0)];
        //fin obtiene el nombre de la pagina actual


        if (strNombrePagina != string.Empty)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                //intIdFormulario = Convert.ToInt32(Request.QueryString["op"].ToString());
                //Session["intIdFormulario"] = intIdFormulario;
                if (blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(Session["IdUsuario"].ToString()), strNombrePagina))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strBuscar = blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                        strNuevo = blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                        strEditar = blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                        strEliminar = blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();

                        if (strBuscar == "False")
                        {
                            imgbBuscar.Visible = false;
                        }
                        if (strNuevo == "False")
                        {
                            imgbNuevo.Visible = false;
                        }
                        if (strEditar == "False")
                        {
                            imgbEditar.Visible = false;
                            imgbGravar.Visible = false;
                        }
                        if (strEliminar == "False")
                        {
                            imgbEliminar.Visible = false;
                        }

                    }
                }
                blPaicma.Termina();
            }
        }
        else
        {
            Response.Redirect("@dmin/frmLogin.aspx");
        }
    }

    public void fntInactivarPaneles(Boolean bolValor)
    {
        pnlAccionesVariable.Visible = bolValor;
        pnlEliminacion.Visible = bolValor;
        pnlMenuVariable.Visible = bolValor;
        pnlRespuesta.Visible = bolValor;
        PnlVariableGrilla.Visible = bolValor;
        pnlVariableNuevo.Visible = bolValor;
    }

    public void fntCargarVariables()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaVariablesAdmin("strDsVariables", string.Empty))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlidVariable.DataMember = "strDsVariables";
                    ddlidVariable.DataSource = blPaicma.myDataSet;
                    ddlidVariable.DataValueField = "idVariable";
                    ddlidVariable.DataTextField = "nombreVariable";
                    ddlidVariable.DataBind();
                    ddlidVariable.Items.Insert(ddlidVariable.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarObjetos()
    {
        fntCargarVariables();
    }

    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles(false);
        pnlAccionesVariable.Visible = true;
        imgbEncontrar.Visible = false;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        pnlVariableNuevo.Visible = true;
        fntCargarObjetos();
        Session["Operacion"] = "Crear";
    }

    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        string strRuta = "frmOpcionesXVariable.aspx";
        Response.Redirect(strRuta);
    }

    public void fntCargarGrillaVariables(string strNombreVariable)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaOpcionesVariablesAdmin("strDsGrillaVariable", strNombreVariable))
            {
                gvVariable.Columns[0].Visible = true;
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvVariable.DataMember = "strDsGrillaVariable";
                    gvVariable.DataSource = blPaicma.myDataSet;
                    gvVariable.DataBind();
                    lblErrorGv.Text = string.Empty;
                }
                else
                {
                    gvVariable.DataMember = "strDsGrillaVariable";
                    gvVariable.DataSource = blPaicma.myDataSet;
                    gvVariable.DataBind();
                    lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                gvVariable.Columns[0].Visible = false;
            }
            blPaicma.Termina();
        }
    }


    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["Operacion"].ToString() == "Crear")
        {
            if (ftnValidarCampos_bol())
            {
                if (fntValidarExistenciaVariable())
                {
                    if (ftnIngresarVariable())
                    {
                        lblError.Visible = false;
                        lblError.Text = string.Empty;
                        fntInactivarPaneles(false);
                        pnlRespuesta.Visible = true;
                        lblRespuesta.Text = "Variable creado satisfactoriamente";
                    }
                    else
                    {
                        lblError.Visible = true;
                    }
                }
                else
                {
                    lblError.Visible = true;
                }
            }
            else
            {
                //fntInactivarPaneles(false);
                //pnlRespuesta.Visible = true;
                //lblRespuesta.Text = "Problema al ingresar el registro, por favor comuníquese con el administrador.";
                lblError.Visible = true;
            }
        }
        else
        {
            if (Session["Operacion"].ToString() == "Actualizar")
            {
                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    if (blPaicma.fntModificarOpcionesVariables_bol(Convert.ToInt32(hndidValorXVariable.Value),
                        Convert.ToInt32(ddlidVariable.SelectedValue), Convert.ToInt32(txtidentificadorValor.Text), txttextoValor.Text))
                    {
                        fntInactivarPaneles(false);
                        pnlRespuesta.Visible = true;
                        lblRespuesta.Text = "Registro actualizado satisfactoriamente";
                    }
                    else
                    {
                        fntInactivarPaneles(false);
                        pnlRespuesta.Visible = true;
                        lblRespuesta.Text = "Problema al actualizar el registro, por favor comuníquese con el administrador.";
                    }
                    blPaicma.Termina();
                }
            }
        }
    }


    public Boolean ftnValidarCampos_bol()
    {

        if (txtidentificadorValor.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe digitar el Identificador de la Opción.";
            return false;
        }
        if (txttextoValor.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe digitar el Valor de la Opción.";
            return false;
        }

        return true;
    }

    public Boolean ftnValidarSeguridadCampos_bol(string strTextoCampo)
    {
        Boolean bolValor = false;
        int intCantidadSeguridad = 0;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntEliminarTablaExpresionCampo_bol())
            {
                if (blPaicma.fntIngresarExpresionCampo_bol(strTextoCampo))
                {
                    if (blPaicma.fntConsultaExpresionSeguridad("strDsSeguridadCampos", strTextoCampo))
                    {
                        intCantidadSeguridad = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                        if (intCantidadSeguridad > 0)
                        {
                            bolValor = false;
                        }
                        else
                        {
                            bolValor = true;
                        }
                    }
                }
            }
            blPaicma.Termina();
        }
        return bolValor;
    }

    public Boolean fntValidarExistenciaVariable()
    { 
        Boolean bolValor = false;
        int intCantidad = 0;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaSoloOpcionesVariablesCantidad_bol("strDsVariable", Convert.ToInt32(txtidentificadorValor.Text), txttextoValor.Text))
            {
                intCantidad = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                if (intCantidad > 0)
                {
                    bolValor = false;
                    lblError.Text = "Ya existe una Opcion Variable registrado, por favor verificar.";
                }
                else
                {
                    bolValor = true;
                }
            }
            blPaicma.Termina();
        }
        return bolValor;
    }

    public Boolean ftnIngresarVariable()
    {
        Boolean bolResultadoAccion = false;
        string strPassword = string.Empty;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarOpcionesVariablesNew_bol( Convert.ToInt32(ddlidVariable.SelectedValue), Convert.ToInt32(txtidentificadorValor.Text), txttextoValor.Text))
            {
                bolResultadoAccion = true;
            }
            else
            {
                bolResultadoAccion = false;
                lblError.Text = "Problema al ingresar el registro, por favor comuníquese con el administrador.";
            }

            blPaicma.Termina();
        }
        return bolResultadoAccion;
    }


    protected void btnOk_Click(object sender, EventArgs e)
    {
        string strRuta = "frmOpcionesXVariable.aspx";
        Response.Redirect(strRuta);
    }

    protected void gvVariable_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            gvVariable.Columns[0].Visible = true;
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvVariable.Rows[intIndex];
            TableCell Item = selectedRow.Cells[0];
            int intIdVariable = Convert.ToInt32(Item.Text);
            Session["intidValorXVariableNew"] = intIdVariable;
            imgbEditar.Visible = true;
            gvVariable.Columns[0].Visible = false;
        }
    }

    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles(false);
        pnlAccionesVariable.Visible = true;
        imgbEncontrar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        pnlVariableNuevo.Visible = true;
        int intidValorXVariableNew = 0;
        fntCargarObjetos();
        Session["Operacion"] = "Actualizar";
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            intidValorXVariableNew = Convert.ToInt32(Session["intidValorXVariableNew"].ToString());
            if (blPaicma.fntConsultaValorXVariable_bol("strDsVariable", intidValorXVariableNew))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {

                    hndidValorXVariable.Value = blPaicma.myDataSet.Tables[0].Rows[0][0].ToString();
                    ddlidVariable.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                    txtidentificadorValor.Text = blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                    txttextoValor.Text = blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();

                    ftnValidarPermisos();
                }
            }
            blPaicma.Termina();
        }
    }

    protected void imgbEliminar_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles(false);
        pnlEliminacion.Visible = true;
        lblEliminacion.Text = "¿Está seguro de eliminar la Opcion Variable?";
    }

    protected void btnNo_Click(object sender, EventArgs e)
    {
        pnlEliminacion.Visible = false;
        pnlAccionesVariable.Visible = true;
        pnlVariableNuevo.Visible = true;
    }

    protected void btnSi_Click(object sender, EventArgs e)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntEliminarOpcionesVariables_bol(Convert.ToInt32(Session["intidValorXVariableNew"].ToString())))
            {
                lblRespuesta.Text = "Registro eliminado satisfactoriamente.";
            }
            else
            {
                lblRespuesta.Text = "No se puede eliminar el registro ya que tiene información asociada.";
            }
            fntInactivarPaneles(false);
            pnlRespuesta.Visible = true;

            blPaicma.Termina();
        }
    }

    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles(false);
        pnlAccionesVariable.Visible = true;
        imgbEncontrar.Visible = true;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = false;
        imgbCancelar.Visible = true;
        pnlVariableNuevo.Visible = true;
        fntCargarObjetos();
        fntLimpiarObjetos();

    }

    public void fntLimpiarObjetos()
    {
        hndidValorXVariable.Value = string.Empty;
        //ddlidVariable.SelectedValue = string.Empty;
        txtidentificadorValor.Text = string.Empty;
        txttextoValor.Text = string.Empty;
    }

    protected void imgbEncontrar_Click(object sender, ImageClickEventArgs e)
    {
        fntCargarGrillaVariables(ddlidVariable.SelectedItem.Text);
        fntInactivarPaneles(false);
        pnlMenuVariable.Visible = true;
        PnlVariableGrilla.Visible = true;
    }


    protected void gvVariable_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvVariable.PageIndex = e.NewPageIndex;
        gvVariable.DataBind();
    }
}