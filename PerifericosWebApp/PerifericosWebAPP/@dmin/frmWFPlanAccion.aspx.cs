﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class _dmin_frmWFPlanAccion : System.Web.UI.Page
{

    private blSisPAICMA blPaicma = new blSisPAICMA();
    //public TextBox[] arregloTextBoxs;
    //public DropDownList[] arregloCombos;
    public DropDownList[] ArregloEstrategia;
    public DropDownList[] ArregloActividad;
    public DropDownList[] ArregloIndicador;
    //public TextBox[] arregloTextBoxsLongitud;
    //public TextBox[] arregloTextBoxsOrden;
    //public TextBox[] arregloTextBoxsNombre;
    //public CheckBox[] arregloCheckBox;
    public int contadorControles;
    public int cont;
    public Label[] ListLabelEstrategia
    {
        get { return (Label[])Session["ListLabelEstrategia"]; }
        set { Session["ListLabelEstrategia"] = value; }
    }
    public Label[] ListLabelActividad
    {
        get { return (Label[])Session["ListLabelActividad"]; }
        set { Session["ListLabelActividad"] = value; }
    }
    public Label[] ListLabelIndicador
    {
        get { return (Label[])Session["ListLabelIndicador"]; }
        set { Session["ListLabelIndicador"] = value; }
    }
    public DropDownList[] ListDropDownEstrategia
    {
        get { return (DropDownList[])Session["ListDropDownEstrategia"]; }
        set { Session["ListDropDownEstrategia"] = value; }
    }
    public DropDownList[] ListDropDownActividad
    {
        get { return (DropDownList[])Session["ListDropDownActividad"]; }
        set { Session["ListDropDownActividad"] = value; }
    }
    public DropDownList[] ListDropDownIndicador
    {
        get { return (DropDownList[])Session["ListDropDownIndicador"]; }
        set { Session["ListDropDownIndicador"] = value; }
    }

    protected int NumberOfControls
    {
        get { return (int)Session["NumControls"]; }
        set { Session["NumControls"] = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                this.FillPlanesAccion();
            }
            ListLabelEstrategia = new Label[200];
            ListLabelIndicador = new Label[200];
            ListLabelActividad = new Label[200];
      

            ListDropDownEstrategia = new DropDownList[200];
            ListDropDownIndicador = new DropDownList[200];
            ListDropDownActividad = new DropDownList[200];

            lblError.Text = string.Empty;
            this.NumberOfControls = 0;
            contadorControles = 0;
        }
        cont = this.NumberOfControls;

        try
        {
            cont = this.NumberOfControls;
            ArregloEstrategia = new DropDownList[200];
            Array.Copy(ListDropDownEstrategia, ArregloEstrategia, 200);

            ArregloIndicador = new DropDownList[200];
            Array.Copy(ListDropDownIndicador, ArregloIndicador, 200);

            ArregloActividad = new DropDownList[200];
            Array.Copy(ListDropDownActividad, ArregloActividad, 200);

           

            for (int i = 0; i < cont ; i++)
            {
                //AgregarControles(TextBox txtnombreCampo, DropDownList ddlTipo, Label lblNombre, Label lblTipo, Label lblLongitud, TextBox txtLongitud, Label lblTitulo, Label lblOrden, TextBox txtOrden, TextBox txtLabel)
                AgregarControles(ListLabelEstrategia[i], ListLabelIndicador[i], ListLabelActividad[i], ArregloEstrategia[i], ArregloIndicador[i], ArregloActividad[i]);
            }
            Array.Copy(ArregloEstrategia, ListDropDownEstrategia, 200);
            Array.Copy(ArregloIndicador, ListDropDownIndicador, 200);
            Array.Copy(ArregloActividad, ListDropDownActividad, 200);
       
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToBoolean(Session["soloVerSolicitud"]) == true)
        {
            PlanesDinamicos.Visible = false;
            panelBotones.Visible = false;
            panelSoloVer.Visible = true;
        }
    }


    protected void AgregarControles(Label lblEstrategia, Label lblActividad, Label lblIndicador, DropDownList DdlEstrategia, DropDownList DdlActividad, DropDownList DdlIndicador)
    {
        try
        {
            HtmlTableRow fila = new HtmlTableRow();
            HtmlTableRow fila2 = new HtmlTableRow();
            HtmlTableRow fila3 = new HtmlTableRow();
            HtmlTableRow filaSeparadora = new HtmlTableRow();
            HtmlTableCell celdaSeparadora = new HtmlTableCell();

            HtmlTableCell celdaA1 = new HtmlTableCell();
            HtmlTableCell celdaA2 = new HtmlTableCell();
            HtmlTableCell celdaB1 = new HtmlTableCell();
            HtmlTableCell celdaB2 = new HtmlTableCell();
            HtmlTableCell celdaC1 = new HtmlTableCell();
            HtmlTableCell celdaC2 = new HtmlTableCell();

            celdaSeparadora.ColSpan = 3;
            celdaSeparadora.Controls.Add(new LiteralControl("<hr/>"));
            filaSeparadora.Cells.Add(celdaSeparadora);


            celdaA1.Controls.Add(lblEstrategia);
            celdaA2.Controls.Add(DdlEstrategia);
            celdaA2.ColSpan = 2;

            celdaC1.Controls.Add(lblIndicador);
            celdaC2.Controls.Add(DdlIndicador);
            celdaC2.ColSpan = 2;

            celdaB1.Controls.Add(lblActividad);
            celdaB2.Controls.Add(DdlActividad);
            celdaB2.ColSpan = 2;

            

            fila.Cells.Add(celdaA1);
            fila.Cells.Add(celdaA2);

            fila2.Cells.Add(celdaB1);
            fila2.Cells.Add(celdaB2);

            fila3.Cells.Add(celdaC1);
            fila3.Cells.Add(celdaC2);

            PlanesDinamicos.Rows.Add(fila);
            PlanesDinamicos.Rows.Add(fila2);
            PlanesDinamicos.Rows.Add(fila3);
            PlanesDinamicos.Rows.Add(filaSeparadora);



            //PlanesDinamicos.Rows.Remove()
            //    PlanesDinamicos

        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }

    }


    protected void imgbNuevoPlan_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int numeroRegistro = this.NumberOfControls;
            ArregloEstrategia = new DropDownList[200];
            Array.Copy(ListDropDownEstrategia, ArregloEstrategia, 200);

            ArregloActividad = new DropDownList[200];
            Array.Copy(ListDropDownActividad, ArregloActividad, 200);

            ArregloIndicador = new DropDownList[200];
            Array.Copy(ListDropDownIndicador, ArregloIndicador, 200);

            //Cargamos los La data para asignarla despues
            DataTable dataEstrategia = new DataTable();
            DataTable dataActividad = new DataTable();
            DataTable dataIndicador = new DataTable();
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarPorEstado("datosEstrategia", "WFSol_Estrategia", "estadoEst", "ACT", "nombreEstrategia"))
                {
                    dataEstrategia = blPaicma.myDataSet.Tables["datosEstrategia"];
                    if (blPaicma.fntConsultarPorEstado("datosActividad", "WFSol_Actividad", "estadoAct", "ACT", "nombreActividad"))
                    {
                        dataActividad = blPaicma.myDataSet.Tables["datosActividad"];
                        if (blPaicma.fntConsultarPorEstado("datosIndicadores", "WFSol_Indicador", "estadoInd", "ACT", "nombreIndicador"))
                        {
                            dataIndicador = blPaicma.myDataSet.Tables["datosIndicadores"];
                        }
                        else
                        {
                            // no pudo cargar Listado de Indicador
                        }
                    }
                    else
                    {
                        //No Pudo cargar Listado de actividad
                    }
                }
                else
                {
                    //No se pudieron cargar los dropdown
                }
            }
            else
            {
                //No se ha podido conectar con la base de datos
            }
            blPaicma.Termina();

            //creamos los dropDownList
            DropDownList _ddlestrategia = new DropDownList();
            _ddlestrategia.ID = "ddlEstrategia" + numeroRegistro.ToString();
            _ddlestrategia.CssClass = "Estrategia";
            _ddlestrategia.DataMember = "datosEstrategia";
            _ddlestrategia.DataSource = dataEstrategia;
            _ddlestrategia.DataValueField = "idEstrategia";
            _ddlestrategia.DataTextField = "nombreEstrategia";
           
            //_ddlestrategia.Attributes.Add("size", "3");
            //_ddlestrategia.Attributes.Add("style", "overflow-x:scroll; width:600px; overflow: -moz-scrollbars-horizontal;");
            _ddlestrategia.DataBind();
            _ddlestrategia.Items.Insert(_ddlestrategia.Attributes.Count, "Seleccione...");

            ArregloEstrategia[numeroRegistro] = _ddlestrategia;
            Array.Copy(ArregloEstrategia, ListDropDownEstrategia, 200);

            // ********************************************************************

            // validacion para los indicadores 
            DropDownList _ddlindicador = new DropDownList();
            _ddlindicador.ID = "ddlindicador" + numeroRegistro.ToString();
            _ddlindicador.CssClass = "Indicador";
            // _ddlindicador.DataMember = "datosindicador";
            foreach (ListItem var in fillIndicador(dataIndicador))
            {
                _ddlindicador.Items.Add(var);
            }
            //   _ddlindicador.DataSource = fillIndicador(dataIndicador);
            _ddlindicador.DataValueField = "Value";
            _ddlindicador.DataTextField = "Text";
            //_ddlindicador.Attributes.Add("size", "3");
            //_ddlindicador.Attributes.Add("style", "overflow-x:scroll; width:600px; overflow: -moz-scrollbars-horizontal;");           
            _ddlindicador.DataBind();
            _ddlindicador.Enabled = false;
            _ddlindicador.Items.Insert(_ddlindicador.Attributes.Count, "Seleccione...");
            ArregloIndicador[numeroRegistro] = _ddlindicador;
            Array.Copy(ArregloIndicador, ListDropDownIndicador, 200);




            DropDownList _ddlactividad = new DropDownList();
            _ddlactividad.ID = "ddlactividad" + numeroRegistro.ToString();
            _ddlactividad.CssClass = "Actividad";
            //_ddlactividad.DataMember = "datosActividad";
            ///_ddlactividad.DataSource = fillActividad(dataActividad);
            foreach (ListItem var in fillActividad(dataActividad))
            {
                _ddlactividad.Items.Add(var);
            }
            _ddlactividad.DataValueField = "Value";
            _ddlactividad.DataTextField = "Text";
           _ddlactividad.Enabled = false;
           //_ddlactividad.Attributes.Add("size", "3");
           //_ddlactividad.Attributes.Add("style", "overflow-x:scroll; width:600px; overflow: -moz-scrollbars-horizontal;");
            _ddlactividad.DataBind();
            _ddlactividad.Items.Insert(_ddlactividad.Attributes.Count, "Seleccione...");
            ArregloActividad[numeroRegistro] = _ddlactividad;
            Array.Copy(ArregloActividad, ListDropDownActividad, 200);
            Label LabelEstrategia = new Label();
            LabelEstrategia.ID = "lblEstrategia" + numeroRegistro.ToString();
            LabelEstrategia.Text = "Estrategia ";
            ListLabelEstrategia[numeroRegistro] = LabelEstrategia;

            Label LabelIndicador = new Label();
            LabelIndicador.ID = "lblIndicador" + numeroRegistro.ToString();
            LabelIndicador.Text = "Indicador ";
            ListLabelIndicador[numeroRegistro] = LabelIndicador;

            Label LabelActividad = new Label();
            LabelActividad.ID = "lblActividad" + numeroRegistro.ToString();

            LabelActividad.Text = "Actividad ";
            ListLabelActividad[numeroRegistro] = LabelActividad;

            ///agregamos los controles al panel y tabla
            ///NombreCampo, Combo, Label nuevo, label Combo, label Longitud, longitud, 
            AgregarControles(LabelEstrategia, LabelIndicador, LabelActividad, _ddlestrategia, _ddlindicador, _ddlactividad);
            this.NumberOfControls++;

            lblError.Text = string.Empty;
            imgbEliminarPlan.Enabled = true;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }

    protected void btnGuardarCont_Click(object sender, EventArgs e)
    {
        Session["OperacionSolicitud"] = "GuardarContinuar";
        btnOk.Text = "Continuar";
        this.guardarPlan();

    }
    protected void btnGuardarSalir_Click(object sender, EventArgs e)
    {
        Session["OperacionSolicitud"] = "GuardarSalir";
        btnOk.Text = "Terminar";
        this.guardarPlan();
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmWFSolicitudComision.aspx");
    }

    public void guardarPlan()
    {

        bool guardoCorrecto = true;
        bool primeraVes = true;
        int intCantidadControles = this.NumberOfControls;
        ArregloEstrategia = new DropDownList[200];
        Array.Copy(ListDropDownEstrategia, ArregloEstrategia, 200);

        ArregloActividad = new DropDownList[200];
        Array.Copy(ListDropDownActividad, ArregloActividad, 200);

        ArregloIndicador = new DropDownList[200];
        Array.Copy(ListDropDownIndicador, ArregloIndicador, 200);
        Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        for (int i = 0; i < intCantidadControles; i++)
        {
            
            if(guardoCorrecto){
                

                DropDownList EstrategiaGuardar = new DropDownList();
                DropDownList ActividadGuardar = new DropDownList();
                DropDownList IndicadorGuardar = new DropDownList();

                EstrategiaGuardar = ArregloEstrategia[i];
                ActividadGuardar = ArregloActividad[i];
                IndicadorGuardar = ArregloIndicador[i];

                int idUsuarioLogin = Convert.ToInt32(Session["IdUsuario"]);
                DateTime creacion = DateTime.Now;

                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    if (blPaicma.fntIngresarPlanAccion_bol(idSolicitud, Convert.ToInt32(EstrategiaGuardar.SelectedValue), Convert.ToInt32(ActividadGuardar.SelectedValue), Convert.ToInt32(IndicadorGuardar.SelectedValue), idUsuarioLogin, idUsuarioLogin, creacion))
                    {
                        if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, EstadosSolicitud.IncompletaSitios, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
                        {
                            if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", EstadosSolicitud.IncompletaSitios, "WFSol_SolicitudComision", "idEstadoActual"))
                            {
                                guardoCorrecto = true;
                                primeraVes = false;
                            }
                            else
                            {
                                //Fallo actualizar estado en la comision
                            }

                        }
                        else
                        {
                            //fallo actualizar estado Solicitud
                        }
                       
                    }
                    else
                    {
                         guardoCorrecto = false;
                         primeraVes = false;
                    }
                }
                else
                {
                     guardoCorrecto = false;
                     primeraVes = false;
                }
                this.blPaicma.Termina();
            }
        }

       int cantidaditems = Convert.ToInt32( gvPlanAccion.Rows.Count);


       if (guardoCorrecto && !primeraVes || cantidaditems>0)
       {

           //Agregado para que siempre actualice
           if (blPaicma.inicializar(blPaicma.conexionSeguridad))
           {
                   if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, EstadosSolicitud.IncompletaSitios, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
                   {
                       if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", EstadosSolicitud.IncompletaSitios, "WFSol_SolicitudComision", "idEstadoActual"))
                       {
                           lblRespuesta.Text = "Estrategias Agregadas con éxito";
                           principal.Visible = false;
                           pnlRespuesta.Visible = true;
                           Session["IdSolicitudRef"] = idSolicitud;
                       }
                       else
                       {
                           //Fallo actualizar estado en la comision
                       }

                   }
                   else
                   {
                       //fallo actualizar estado Solicitud
                   }
           }
           else
           {
                    //No se pudo establecer conexion.
           }
           this.blPaicma.Termina();
        }
       else
       {

           if (guardoCorrecto && primeraVes)
           {
               lblRespuesta.Text = "Se presento un problema al conectar con la base de datos, Intente Nuevamente.";
               principal.Visible = false;
               pnlRespuesta.Visible = true;
               Session["OperacionSolicitud"] = "ErrorSalir";
           }
           if (!guardoCorrecto && !primeraVes)
           {
               lblRespuesta.Text = "Se presento un problema al intentar guardar al menos uno de los planes, Intente Nuevamente.";
               principal.Visible = false;
               pnlRespuesta.Visible = true;
               Session["OperacionSolicitud"] = "ErrorSalir";
           }

       }


    }

    protected void gvPlanAccion_RowCommand(object sender, GridViewCommandEventArgs e)
    {        
        
        if (e.CommandName == "EditarPlan")
        {
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvPlanAccion.Rows[intIndex];
            TableCell Item = selectedRow.Cells[0];
            Int64 intIdPlan = Convert.ToInt64(Item.Text);
            Session["intIdPlanEditar"] = Convert.ToInt64(intIdPlan);
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
            Session["IdSolicitudRef"] = idSolicitud;
            Response.Redirect("frmWFEditExistingPlan.aspx");
        }
        if (e.CommandName == "EliminarPlan")
        {
            
            GridViewRow selectedRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);            
            TableCell Item = selectedRow.Cells[0];
            Int64 intIdPlan = Convert.ToInt64(Item.Text);
            Session["intIdPlanEditar"] = Convert.ToInt64(intIdPlan);
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);

            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntEliminarElementoBigInt("WFSol_PlanAccion", "idPlanAccion", intIdPlan))
                {
                    lblRespuestaInterna.Text = "Registro Eliminado Correctamente.";
                    lblRespuestaInterna.Style.Add("background-color", "#FBEE57");
                    lblRespuestaInterna.Visible = true;
                }
                else
                {

                    lblRespuestaInterna.Text = "No se pudo eliminar el registro.";
                    lblRespuestaInterna.Style.Add("background-color", "#FBEE57");
                    lblRespuestaInterna.Visible = true;
                    //No pudo eliminar
                }
            }
            else
            {
                //No pudo conectar a la base de datos.
            }
            blPaicma.Termina();
        }

    }

    public void FillPlanesAccion()
    {
        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarPlanes("datosPlanesAccion", idSolicitud, "", 0))
                {
                    if (blPaicma.myDataSet.Tables["datosPlanesAccion"].Rows.Count > 0)
                    {
                        this.gvPlanAccion.DataMember = "datosPlanesAccion";
                        this.gvPlanAccion.DataSource = this.blPaicma.myDataSet;
                        this.gvPlanAccion.DataBind();
                    }
                }
                else
                {
                    //Imposible Consultar lista de planes
                    lblRespuesta.Text = "Se ha presentado un problema al consultar lista de planes, intente de nuevo";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    Session["OperacionSolicitud"] = "ErrorSalir";
                }
            }
            else
            {
                //No pudo conectar a la base de datos
                lblRespuesta.Text = "Se ha presentado un problema al consultar lista de planes, no se ha podido conectar a la base de datos";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";

            }
            blPaicma.Termina();
        }
        else
        {
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
    }
    protected void imgbEliminarPlan_Click(object sender, ImageClickEventArgs e)
    {

        if (this.NumberOfControls == 0)
        {
            imgbEliminarPlan.Enabled = false;
        }
        else
        {
            this.NumberOfControls--;
            PlanesDinamicos.Rows.RemoveAt(PlanesDinamicos.Rows.Count - 1);
            PlanesDinamicos.Rows.RemoveAt(PlanesDinamicos.Rows.Count - 1);
            PlanesDinamicos.Rows.RemoveAt(PlanesDinamicos.Rows.Count - 1);
            PlanesDinamicos.Rows.RemoveAt(PlanesDinamicos.Rows.Count - 1);
            if (this.NumberOfControls == 0)
            {
                imgbEliminarPlan.Enabled = false;
            }
        }
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        string operacionActual = Session["OperacionSolicitud"].ToString();
        if (operacionActual == "GuardarContinuar")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Session["soloVerSolicitud"] = false;
            Response.Redirect("frmWFSitiosIntervencion.aspx");
        }
        if (operacionActual == "GuardarSalir")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "ErrorSalir")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "Inicio")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if(operacionActual == "Anular")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }



    }
    protected void gvPlanAccion_PreRender(object sender, EventArgs e)
    {
        if (gvPlanAccion.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvPlanAccion.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvPlanAccion.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvPlanAccion.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }


    /// <summary>
    /// ☻☻  metodo que retorna la lista  de valor y atributos para el dropdownlist Indicador
    /// </summary>
    /// <returns></returns>
    protected List<ListItem> fillIndicador(DataTable datatable )
    {
        List<ListItem> resultado = new List<ListItem>();
        foreach (DataRow row in datatable.Rows)
                {
                    string valor = row[0].ToString();
                    string texto = row[1].ToString();
                    string estrategia = row[5].ToString();

                    ListItem valorCombo2 = new ListItem();
                    valorCombo2.Value = valor;
                    valorCombo2.Text = texto;
                    valorCombo2.Attributes["data-estrategia"] = estrategia;
                    resultado.Add(valorCombo2);
                }
        return resultado;
    }


    /// <summary>
    /// ☻☻  metodo que retorna la lista  de valor y atributos para el dropdownlist actividad
    /// </summary>
    /// <returns></returns>
    protected List<ListItem> fillActividad(DataTable datatable)
    {
        List<ListItem> resultado = new List<ListItem>();
        foreach (DataRow row in datatable.Rows)
        {
            string valor = row[0].ToString();
            string texto = row[1].ToString();
            string estrategia = row[4].ToString();

            ListItem valorCombo2 = new ListItem();
            valorCombo2.Value = valor;
            valorCombo2.Text = texto;
            valorCombo2.Attributes["data-Indicador"] = estrategia;
            resultado.Add(valorCombo2);
        }
        return resultado;
    }



    


}