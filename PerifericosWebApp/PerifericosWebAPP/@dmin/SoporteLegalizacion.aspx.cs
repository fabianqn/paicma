﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class _dmin_SoporteLegalizacion : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                if (Convert.ToBoolean(Session["soloVerSolicitud"]) == true)
                {
                    TablaNuevo.Visible = false;
                    panelSoloVer.Visible = true;
                }
                this.FillSoportes();
            }
        }
    }

    override protected void OnInit(EventArgs e)
    {
        // 
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        // 
        InitializeComponent();
        base.OnInit(e);
    }

    private void InitializeComponent()
    {
        //this.btnCargar.ServerClick += new System.EventHandler(this.btnCargar_ServerClick);
        this.Load += new System.EventHandler(this.Page_Load);
    }

    //private void btnCargar_ServerClick(object sender, System.EventArgs e)
    //{


    //}

    private byte[] GetBinaryFile(string filename)
    {
        byte[] bytes;
        using (FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Read))
        {
            bytes = new byte[file.Length];
            file.Read(bytes, 0, (int)file.Length);
        }
        return bytes;
    }



    public void subirArchivos()
    {
        Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);

        if ((archivoSoporte.PostedFile != null) && (archivoSoporte.PostedFile.ContentLength > 0) && (txtNombre.Text.Trim() != string.Empty) && (txtDescripcion.Text.Trim() != string.Empty))
        {

            //string fn = System.IO.Path.GetFileName(archivoReporte.PostedFile.FileName);
            string strExtDocumento = Path.GetExtension(archivoSoporte.PostedFile.FileName);
            if (strExtDocumento.ToUpper() == ".DOC" || strExtDocumento.ToUpper() == ".DOCX" || strExtDocumento.ToUpper() == ".XLS" || strExtDocumento.ToUpper() == ".XLSX" || strExtDocumento.ToUpper() == ".PDF" || strExtDocumento.ToUpper() == ".JPG" || strExtDocumento.ToUpper() == ".PNG" || strExtDocumento.ToUpper() == ".PPT" || strExtDocumento.ToUpper() == ".PPTX" || strExtDocumento.ToUpper() == ".ZIP" || strExtDocumento.ToUpper() == ".RAR" || strExtDocumento.ToUpper() == ".Outlook")
            {

                string unique = Guid.NewGuid().ToString();

                int idUsuarioLogin = Convert.ToInt32(Session["IdUsuario"]);
                DateTime creacion = DateTime.Now;
                //fn = fn + DateTime.Now.ToString("YYYYMMDDHHMMSS");
                string SaveLocation = Server.MapPath("~") + "Archivos\\" + unique + strExtDocumento;
                try
                {
                    archivoSoporte.PostedFile.SaveAs(SaveLocation);

                    byte[] reporteGuardarBD = GetBinaryFile(SaveLocation);

                    if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
                    {
                        if (this.blPaicma.fntInsertarSoporteSolLegalizacion_bol(idSolicitud, SaveLocation, reporteGuardarBD, txtNombre.Text, txtDescripcion.Text, creacion, creacion, idUsuarioLogin, idUsuarioLogin, strExtDocumento))
                        {
                            if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, EstadosSolicitud.LegPorAprobarCoordinador, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
                            {
                                if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", EstadosSolicitud.LegPorAprobarCoordinador, "WFSol_SolicitudComision", "idEstadoActual"))
                                {
                                    lblRespuesta.Text = "Soporte Agregado con éxito";
                                    principal.Visible = false;
                                    pnlRespuesta.Visible = true;
                                    Session["IdSolicitudRef"] = idSolicitud;
                                }
                                else
                                {
                                    //Fallo actualizar estado en la comision
                                }
                            }
                            else
                            {
                                //fallo actualizar estado Solicitud
                            }

                        }
                        else
                        {
                            lblRespuesta.Text = "Se presento un problema al conectar con la base de datos, Intente Nuevamente.";
                            principal.Visible = false;
                            pnlRespuesta.Visible = true;
                            Session["IdSolicitudRef"] = idSolicitud;
                        }
                    }
                    else
                    {
                        lblRespuesta.Text = "Se presento un problema al guardar el soporte, no pudo conectar con la base de datos.";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        Session["OperacionSolicitud"] = "ErrorSalir";
                    }
                    this.blPaicma.Termina();

                }
                catch (Exception ex)
                {
                    lblRespuesta.Text = "Se presento un problema al guardar el soporte," + "Error: " + ex.Message;
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    Session["OperacionSolicitud"] = "ErrorSalir";
                }


            }
            else
            {
                // tipo de documento 
                lblError.Text = "tipo de archivo no valido ";
                lblError.Style.Add("background-color", "#FBEE57");
                lblError.Visible = true;
            }

        }
        else
        {
            lblError.Text = "Faltan Datos por completar o seleccionar el archivo a cargar";
            lblError.Style.Add("background-color", "#FBEE57");
            lblError.Visible = true;
            Session["IdSolicitudRef"] = idSolicitud;
        }
    }


    /// <summary>
    /// ☻☻ metodo para buscar los soporte de legalizacion 
    /// </summary>
    public void FillSoportes()
    {
        DataTable datasoporte = new DataTable();

        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarSoporteLegalizacion("datosSoporteComision", idSolicitud, "", 0))
                {
                    datasoporte = blPaicma.myDataSet.Tables["datosSoporteComision"];
                    if (blPaicma.myDataSet.Tables["datosSoporteComision"].Rows.Count > 0)
                    {


                        this.gvSoporteSolicitud.DataMember = "datosSoporteComision";
                        //this.gvSoporteSolicitud.DataSource = datasoporte;
                        //this.gvSoporteSolicitud.DataBind();
                    }
                    this.gvSoporteSolicitud.DataSource = datasoporte;
                    this.gvSoporteSolicitud.DataBind();
                }
                else
                {
                    //Imposible Consultar lista de planes

                }
            }
            else
            {
                //No pudo conectar a la base de datos
            }
            blPaicma.Termina();
        }

    }

    protected void gvSoporteSolicitud_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int intIndex = Convert.ToInt32(e.CommandArgument);
        GridViewRow selectedRow = gvSoporteSolicitud.Rows[intIndex];
        TableCell Item = selectedRow.Cells[0];
        Int64 intIdSoporte = Convert.ToInt64(Item.Text);
        Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        Session["intIdSoporteLegalizacion"] = Convert.ToInt64(intIdSoporte);
        Session["IdSolicitudRef"] = Convert.ToInt64(idSolicitud);
        if (e.CommandName == "VerSoporte")
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {

                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    if (blPaicma.fntConsultarSoporteXIdLegalizacion("datosSoporteComisionXid", "", intIdSoporte))
                    {

                        if (blPaicma.myDataSet.Tables["datosSoporteComisionXid"].Rows.Count > 0)
                        {
                            txtNombre.Text = blPaicma.myDataSet.Tables["datosSoporteComisionXid"].Rows[0]["nombreArchivo"].ToString();
                            txtDescripcion.Text = blPaicma.myDataSet.Tables["datosSoporteComisionXid"].Rows[0]["descripcionArchivo"].ToString();


                        }

                        string Archivonombre = blPaicma.myDataSet.Tables["datosSoporteComisionXid"].Rows[0]["rutaArchivo"].ToString();
                        string simbolo = Archivonombre.Replace(@"\", "/");

                        string nombredeArchivo = simbolo.Split('/').Last();

                        //   Download(blPaicma.myDataSet.Tables["datosSoporteComisionXid"].Rows[0]["rutaArchivo"].ToString());
                        Download(nombredeArchivo, @"~\Archivos\");
                    }
                }



            }

            // File.WriteAllBytes("Foo.txt", arrBytes);
        }
        if (e.CommandName == "EditarSoporte")
        {
            Response.Redirect("frmWFEditExistingSuportLegalizacion.aspx");
        }
        if (e.CommandName == "EliminarSoporte")
        {
            //No pudo eliminar
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntEliminarElementoBigInt("WFSol_SoporteSolLegalizacion", "idSoporteSol", intIdSoporte))
                {
                    lblRespuestaInterna.Text = "Registro Eliminado Correctamente.";
                    lblRespuestaInterna.Style.Add("background-color", "#FBEE57");
                    lblRespuestaInterna.Visible = true;
                    FillSoportes();

                }
                else
                {
                    //No pudo eliminar
                    lblRespuestaInterna.Text = "No se pudo eliminar el registro.";
                    lblRespuestaInterna.Style.Add("background-color", "#FBEE57");
                    lblRespuestaInterna.Visible = true;
                }
            }
            else
            {
                //No pudo conectar a la base de datos.
            }
            blPaicma.Termina();
        }
    }
    protected void imgNuevoSoporte_Click(object sender, ImageClickEventArgs e)
    {
        soportesTabla.Visible = true;
        panelSoloVer.Visible = false;
    }
    protected void btnGuardarCompletar_Click(object sender, EventArgs e)
    {
        Session["OperacionSolicitud"] = "GuardarSalir";
        btnOk.Text = "Terminar";
        this.subirArchivos();

        //Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        //☻☻ mensaje para enviar 
        //StringBuilder mailBody = new StringBuilder();
        //mailBody.AppendFormat(@"<html> <head>");
        //mailBody.AppendFormat(@"</head> <body>");
        //mailBody.AppendFormat(@"<div style=' width: 98%;'>");
        //mailBody.AppendFormat("<h1 style='text-align: center;color: #880404;'>Nueva Solicitud </h1> </br>");
        //mailBody.AppendFormat("<h1 style='text-align: center;' > --------------Detalle-------------- </h1>");
        //mailBody.AppendFormat(" idSolicitud: {0}", idSolicitud);

        //mailBody.AppendFormat("<br />");
        //mailBody.AppendFormat("<p>  se  genero una solicitud </p>");
        //mailBody.AppendFormat(@"</div>");
        //mailBody.AppendFormat(@"</body> </html>");

        //StringBuilder correosConcatenar = new StringBuilder();
        //DataTable correos = new DataTable();
        //if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        //{
        //    if (blPaicma.fntUsuariosCoordinacion_bol("CorreosCoodinadores"))
        //    {

        //        correos = blPaicma.myDataSet.Tables["CorreosCoodinadores"];


        //        for (int i = 0; i < correos.Rows.Count; i++)
        //        {
        //            correosConcatenar.Append(correos.Rows[i]["segusu_Correo"].ToString() + ",");
        //        }



        //        string correosfinal = correosConcatenar.ToString().TrimEnd(',');
        //        EnviarMail("Nueva Solicitud ", mailBody.ToString(), correosfinal);
        //    }
        //    else
        //    {
        //        //No consulta coordinador 
        //    }
        //}
        //else
        //{
        //    //No pudo conectar a la base de datos.
        //}
        //blPaicma.Termina();

        // EnviarMail();
        // metodo para enviar correo 
    }

    protected void btnGuardarAgregar_Click(object sender, EventArgs e)
    {
        Session["OperacionSolicitud"] = "GuardarContinuar";
        btnOk.Text = "Continuar";
        this.subirArchivos();
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        int cantidaditems = Convert.ToInt32(gvSoporteSolicitud.Rows.Count);
        Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        if (cantidaditems > 0)
        {
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, EstadosSolicitud.LegPorAprobarCoordinador, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
                {
                    if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", EstadosSolicitud.LegPorAprobarCoordinador, "WFSol_SolicitudComision", "idEstadoActual"))
                    {
                        lblRespuesta.Text = "Soporte Agregado con éxito";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        Session["IdSolicitudRef"] = idSolicitud;
                    }
                    else
                    {
                        //Fallo actualizar estado en la comision
                    }
                }
                else
                {
                    //fallo actualizar estado Solicitud
                }

            }
        }
        else
        {
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
       
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        string operacionActual = Session["OperacionSolicitud"].ToString();
        if (operacionActual == "GuardarContinuar")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Session["soloVerSolicitud"] = false;
            Response.Redirect("SoporteLegalizacion.aspx");
        }
        if (operacionActual == "GuardarSalir")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "ErrorSalir")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("SoporteLegalizacion.aspx");
        }
        if (operacionActual == "Inicio")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
    }
    protected void gvSoporteSolicitud_PreRender(object sender, EventArgs e)
    {
        if (gvSoporteSolicitud.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvSoporteSolicitud.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvSoporteSolicitud.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvSoporteSolicitud.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }


    /// <summary>
    /// ☻☻ metodo  para descargar el archivo  cuado se desconoce el repositorio 
    /// </summary>
    /// <param name="sFileName"></param>
    /// <param name="sFilePath"></param>
    public static void Download(string sFileName, string sFilePath)
    {
        HttpContext.Current.Response.ContentType = "APPLICATION/OCTET-STREAM";
        String Header = "Attachment; Filename=" + sFileName;
        HttpContext.Current.Response.AppendHeader("Content-Disposition", Header);
        System.IO.FileInfo Dfile = new System.IO.FileInfo(HttpContext.Current.Server.MapPath(sFilePath));
        HttpContext.Current.Response.WriteFile(Dfile.FullName + sFileName);
        HttpContext.Current.Response.End();
    }


    ///// <summary>
    ///// descargar el archivo con la ruta que se guarda en el servidor 
    ///// </summary>
    ///// <param name="sFilePath"></param>
    //public static void Download(string sFilePath)
    //{
    //    HttpContext.Current.Response.ContentType = "APPLICATION/OCTET-STREAM";
    //    String Header = "Attachment; Filename=";
    //    HttpContext.Current.Response.AppendHeader("Content-Disposition", Header);
    //    //  System.IO.FileInfo Dfile = new System.IO.FileInfo(HttpContext.Current.Server.MapPath(sFilePath));
    //    HttpContext.Current.Response.WriteFile(@"C:\Users\user\Documents\Citpax\Development\PAICMAPerifericos\PAICMAPerifericosV2\Archivos\eecfbba6-5797-4ba4-b346-ec715a991dd4.jpg");
    //    HttpContext.Current.Response.End();
    //}


    //☻☻ metodo para enviar email 
    public void EnviarMail(string strTitulo, string strTextoMensaje, string strCorreoTo)
    {
        string text = string.Empty;
        string displayName = string.Empty;
        string host = string.Empty;
        string password = string.Empty;
        //Guardar en Log
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaCorreo("strDsCorreo", 0) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                text = this.blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                displayName = this.blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                host = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                int port = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][2].ToString());
                password = this.blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                int num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][6].ToString());
                if (num == 1)
                {
                    System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
                    mailMessage.From = new System.Net.Mail.MailAddress(text, displayName);
                    mailMessage.To.Add(strCorreoTo);
                    mailMessage.Subject = strTitulo;
                    mailMessage.Body = strTextoMensaje;
                    mailMessage.IsBodyHtml = true;
                    System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();
                    smtpClient.Host = host;
                    smtpClient.Port = port;
                    smtpClient.EnableSsl = false;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new System.Net.NetworkCredential(text, password);
                    smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                    try
                    {
                        smtpClient.Send(mailMessage);
                        string envioCorreo = "Mensaje enviado satisfactoriamente";

                    }
                    catch (Exception ex)
                    {

                        string envioCorreoEror = "ERROR: " + ex.Message;
                    }
                }
            }
            this.blPaicma.Termina();
        }
    }
    //public void EnviarMail(string asunto, string mensaje, string correosenviar)
    //{
    //    System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage();
    //    correo.From = new System.Net.Mail.MailAddress("soportesimiltech@gmail.com");
    //    string correos = correosenviar;
    //    correo.To.Add(correos);
    //    correo.Subject = asunto;
    //    correo.Body = mensaje;

    //    correo.IsBodyHtml = true;
    //    correo.Priority = System.Net.Mail.MailPriority.Normal;
    //    //
    //    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
    //    //
    //    //---------------------------------------------
    //    // Estos datos debes rellanarlos correctamente
    //    //---------------------------------------------
    //    smtp.Host = "smtp.gmail.com";
    //    smtp.Port = 587;
    //    smtp.Credentials = new System.Net.NetworkCredential("soportesimiltech@gmail.com", "$asdf12345");
    //    smtp.EnableSsl = true;
    //    try
    //    {
    //        smtp.Send(correo);
    //        string envioCorreo = "Mensaje enviado satisfactoriamente";
    //    }
    //    catch (Exception ex)
    //    {
    //        string envioCorreoEror = "ERROR: " + ex.Message;
    //    }

    //}
}