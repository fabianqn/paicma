﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmWFExistenteCompromisos" Codebehind="frmWFExistenteCompromisos.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">
    
    <h1>Editando  Compromiso  </h1>
        <hr />
        <table style="width:50%">
            <tbody>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Compromiso"></asp:Label>
                    </td>
                    <td  colspan="2">
                        <asp:TextBox ID="txtCompromiso" runat="server" Width="95%"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                             ControlToValidate="txtCompromiso"
                            ErrorMessage="Este campo es requerido." Enabled="true"
                            ForeColor="Red" Width="100%">  </asp:RequiredFieldValidator>


                    </td>
                </tr>
                 <tr>
                    <td>
                         <asp:Label ID="Label2" runat="server" Text="Fecha Compromiso"></asp:Label>
                    </td>
                    <td  colspan="2">
                        <asp:TextBox ID="txtFechaCompromiso" runat="server" CssClass="pluginFecha"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                             ControlToValidate="txtFechaCompromiso"
                            ErrorMessage="Este campo es requerido." Enabled="true"
                            ForeColor="Red" Width="100%">  </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                         <asp:Label ID="Label3" runat="server" Text="Reponsable"></asp:Label>
                    </td>
                    <td  colspan="2">
                        <asp:TextBox ID="txtResponsable" runat="server" Width="95%"></asp:TextBox>
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                             ControlToValidate="txtResponsable"
                            ErrorMessage="Este campo es requerido." Enabled="true"
                            ForeColor="Red" Width="100%">  </asp:RequiredFieldValidator>
                    </td>
                </tr>
                  <tr>
                <td colspan="2">
                    <hr />
                    <div class="Botones">

                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click"  />
                                    <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
                    </div>
                </td>

            </tr>


            </tbody>           
        </table>




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">

         <script type="text/javascript">
             function newDoc() {
                 window.location.assign("frmWFSolicitudComision.aspx")
             }
             $(document).ready(function () {
                 $(".pluginFecha").datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
             });
   </script>


</asp:Content>

