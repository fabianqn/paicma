﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
public partial class _dmin_frmWFNuevaSolicitud : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    private string _OperacionSolicitud = string.Empty;


   


    protected void Page_Load(object sender, EventArgs e)
    {
        this.MaintainScrollPositionOnPostBack = true;
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                this.txtValorRutaT.Attributes.Add("onblur", "javascript:funBlurValor(this);");
                this.txtValorDiario.Attributes.Add("onblur", "javascript:funBlurValor(this);");
                //this.txtTotalViaticos.Attributes.Add("onblur", "javascript:funBlurValor(this);");

                Guid identificadorSolicitud = new Guid();
                identificadorSolicitud = Guid.NewGuid();
                txtRadicacion.Text = identificadorSolicitud.ToString();
                txtRadicacion.Enabled = false;
                txtNumeroAprobacion.Text = "A la Espera";
                txtNumeroAprobacion.Enabled = false;
                txtTotalViaticos.Text = "0";
                txtValorDiario.Text = "0";
                txtNumeroDias.Text = "0";
                txtValorRutaT.Text = "0";
                txtTotalViaticos.Enabled = false;
                
                this.cargarLineasIntervencion();

                if (Session["operacionSolicitud"].ToString() == "modificar") {
                    this.fillSolicitudes();
                }


            }
        }
    }

    protected void btnGuardarCont_Click(object sender, EventArgs e)
    {
        ViewState["_OperacionSolicitud"] = "GuardarContinuar";
        btnOk.Text = "Continuar";
        this.guardarSolicitud();
    }
    protected void btnGuardarSalir_Click(object sender, EventArgs e)
    {

        ViewState["_OperacionSolicitud"] = "GuardarSalir";
        btnOk.Text = "Terminar";
        this.guardarSolicitud();
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmWFSolicitudComision.aspx");
    }

    public void guardarSolicitud()
    {
        int idUsuarioLogin = Convert.ToInt32(Session["IdUsuario"]);
        DateTime creacion = DateTime.Now;

        if (this.blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            /// ☻☻ validar el estado de la solicitud para  modificarla o crearla segun el caso 
            if (Session["operacionSolicitud"].ToString() == "modificar")
            {
                /// el valor de la solicitud seleccionada 
                /// 
                string pruebavalor = Session["IdSolicitudRef"].ToString();
                if (Session["IdSolicitudRef"].ToString() != "0")
                {
                    
                    // modificar los datos de la solicitud 
                    //Convert.ToDecimal(txtNumeroDias.Text.Replace(".", ","))
                    if (this.blPaicma.fntActualizarSolicitud_bol(Convert.ToInt64(Session["IdSolicitudRef"]), ddlLineaIntervencion.SelectedIndex, txtNombreFuncionario.Text.Trim().ToUpper(), txtObjeto.Text.Trim(), Convert.ToDateTime(txtFechaInicio.Text), Convert.ToDateTime(txtFechaFin.Text), chkTiqueteAereo.Checked, txtRutaAerea.Text.Trim(), chkPorcionTerrestre.Checked, txtRutaTerrestre.Text, chkViaticos.Checked, Convert.ToDecimal(txtValorDiario.Text), Convert.ToDecimal(txtNumeroDias.Text.Replace(".", ",")), Convert.ToDecimal(txtTotalViaticos.Text), idUsuarioLogin, idUsuarioLogin, creacion, EstadosSolicitud.IncompletaPlanes, txtRadicacion.Text, txtCargo.Text.Trim(), txtLugarViaje.Text.Trim(), ddlMonedaValor.SelectedValue, Convert.ToDecimal(txtValorRutaT.Text)))
                        
                     {
                         lblRespuesta.Text = "Solicitud Actualizada con éxito";
                         principal.Visible = false;
                         pnlRespuesta.Visible = true;
                     }
                    else
                    {
                        lblRespuesta.Text = "Se ha presentado un problema al crear la solicitud, actualizar estado incompleto";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        // Session["OperacionSolicitud"] = "ErrorSalir";

                       ViewState["_OperacionSolicitud"] = "ErrorSalir";
                    }
                }
            }
            else
            {
                // asi decimal Convert.ToDecimal(txtNumeroDias.Text.Replace(".", ","))
                if (this.blPaicma.fntRegistrarSolicitud_bol(ddlLineaIntervencion.SelectedIndex, txtNombreFuncionario.Text.ToUpper(), txtObjeto.Text, Convert.ToDateTime(txtFechaInicio.Text), Convert.ToDateTime(txtFechaFin.Text), chkTiqueteAereo.Checked, txtRutaAerea.Text, chkPorcionTerrestre.Checked, txtRutaTerrestre.Text, chkViaticos.Checked, Convert.ToDecimal(txtValorDiario.Text), Convert.ToDecimal(txtNumeroDias.Text.Replace(".", ",")), Convert.ToDecimal(txtTotalViaticos.Text), idUsuarioLogin, idUsuarioLogin, creacion, EstadosSolicitud.IncompletaPlanes, txtRadicacion.Text, txtCargo.Text.Trim(), txtLugarViaje.Text.Trim(), ddlMonedaValor.SelectedValue, Convert.ToDecimal(txtValorRutaT.Text)))
                {
                    if (this.blPaicma.fntConsultarSolicitudEspecial("dataConsultaSolicitudEspecial", txtRadicacion.Text))
                    {
                        if (this.blPaicma.myDataSet.Tables["dataConsultaSolicitudEspecial"].Rows.Count > 0)
                        {
                            int idSolicitudCreada = Convert.ToInt16(blPaicma.myDataSet.Tables["dataConsultaSolicitudEspecial"].Rows[0]["idSolicitud"]);
                            if (this.blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitudCreada, EstadosSolicitud.IncompletaPlanes, idUsuarioLogin, creacion, false))
                            {
                                lblRespuesta.Text = "Solicitud Creada con éxito";
                                principal.Visible = false;
                                pnlRespuesta.Visible = true;
                                Session["IdSolicitudRef"] = idSolicitudCreada;
                            }
                            else
                            {
                                lblRespuesta.Text = "Se ha presentado un problema al crear la solicitud, actualizar estado incompleto";
                                principal.Visible = false;
                                pnlRespuesta.Visible = true;
                               // Session["OperacionSolicitud"] = "ErrorSalir";

                                ViewState["_OperacionSolicitud"] = "ErrorSalir";
                                ///No se puedo ingresar el estado
                            }
                        }
                        else
                        {
                            lblRespuesta.Text = "Se ha presentado un problema al crear la solicitud, solicitu no encontrada";
                            principal.Visible = false;
                            pnlRespuesta.Visible = true;
                            ViewState["_OperacionSolicitud"] = "ErrorSalir";
                            // no existe solicitud
                        }

                    }
                    else
                    {
                        lblRespuesta.Text = "Se ha presentado un problema al crear la solicitud, no se puede consultar solicitud creada";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        ViewState["_OperacionSolicitud"] = "ErrorSalir";
                        //no se puedo consultar la solicitud recien creada
                    }

                }
                else
                {
                    lblRespuesta.Text = "Se ha presentado un problema al crear la solicitud, no se ha podido registrar";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    ViewState["_OperacionSolicitud"] = "ErrorSalir";
                    //No se pudo registrar la solicitud
                }
            }

        }
        else
        {
            lblRespuesta.Text = "Se ha presentado un problema al crear la solicitud, imposible conectar con la base de datos";
            principal.Visible = false;
            pnlRespuesta.Visible = true;
            ViewState["_OperacionSolicitud"] = "ErrorSalir";
            // no se ha podido conectar con base de datos
        }
        this.blPaicma.Termina();
    }


    public void cargarLineasIntervencion()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarPorEstado("datosLineasIntevencion", "WFSol_LineaIntervencion", "estatusLinea", "ACT", "nombreLinea"))
            {

                ddlLineaIntervencion.DataMember = "datosLineasIntevencion";
                ddlLineaIntervencion.DataSource = blPaicma.myDataSet.Tables["datosLineasIntevencion"];
                ddlLineaIntervencion.DataValueField = "idLineaIntervencion";
                ddlLineaIntervencion.DataTextField = "nombreLinea";
                ddlLineaIntervencion.DataBind();
                ddlLineaIntervencion.Items.Insert(ddlLineaIntervencion.Attributes.Count, new ListItem("Seleccione...", ""));
            }
            else
            {
                ddlLineaIntervencion.Items.Insert(ddlLineaIntervencion.Attributes.Count, new ListItem("Seleccione...", ""));
                ///
            }

        }
        else
        {
            //no se ha podido conectar con la base de datos.
        }
        blPaicma.Termina();

    }

    public void fillSolicitudes()
    {
        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarSolicitud("datosSolicitudSeleccionada", "id", idSolicitud))
                {
                    if (blPaicma.myDataSet.Tables["datosSolicitudSeleccionada"].Rows.Count > 0)
                    {
                        txtNumeroAprobacion.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["numeroAprobacion"].ToString();
                        txtRadicacion.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["IdentificadorSolicitud"].ToString();
                        ddlLineaIntervencion.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0]["idLineaIntervencion"].ToString();
                        txtNombreFuncionario.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["nombreFuncionario"].ToString();
                        txtObjeto.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["objetoComision"].ToString();
                        txtFechaInicio.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["fechaInicio"].ToString();
                        txtFechaFin.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["fechaFin"].ToString();
                        txtNumeroDias.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["numeroDias"].ToString().Replace(",", "."); ;

                        chkTiqueteAereo.Checked = Convert.ToBoolean(this.blPaicma.myDataSet.Tables[0].Rows[0]["tiquetesAereos"]);
                        txtRutaAerea.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["rutaAerea"].ToString();

                        chkPorcionTerrestre.Checked = Convert.ToBoolean(this.blPaicma.myDataSet.Tables[0].Rows[0]["porcionTerrestre"]);
                        txtRutaTerrestre.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["rutaPorTerrestre"].ToString();
                        txtValorRutaT.Text = agregarDecimales(this.blPaicma.myDataSet.Tables[0].Rows[0]["valorRutaPorTerrestre"].ToString());

                        chkViaticos.Checked = Convert.ToBoolean(this.blPaicma.myDataSet.Tables[0].Rows[0]["requiereViaticos"]);
                        txtValorDiario.Text = agregarDecimales(this.blPaicma.myDataSet.Tables[0].Rows[0]["valorDiarioViatico"].ToString());


                        txtTotalViaticos.Text = agregarDecimales(this.blPaicma.myDataSet.Tables[0].Rows[0]["totalViaticos"].ToString());
                        txtValorDiario.Text = agregarDecimales(this.blPaicma.myDataSet.Tables[0].Rows[0]["valorDiarioViatico"].ToString());

                        txtCargo.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["cargoFuncionario"].ToString();
                        txtLugarViaje.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["lugarViaje"].ToString();

                        ddlMonedaValor.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0]["monedaViatico"].ToString();
                        ddlValorTotal.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0]["monedaViatico"].ToString();
                        ddlMonedaValorRutaT.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0]["monedaViatico"].ToString();




                        Session["EstadoActualSolicitud"] = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0]["idEstadoActual"]);
                        if (Convert.ToBoolean(this.blPaicma.myDataSet.Tables[0].Rows[0]["tiquetesAereos"]) == true)
                        {
                            Label10.Visible = true;
                            txtRutaAerea.Visible = true;
                        }

                        if (Convert.ToBoolean(this.blPaicma.myDataSet.Tables[0].Rows[0]["porcionTerrestre"]) == true)
                        {
                            Label11.Visible = true;
                            txtRutaTerrestre.Visible = true;
                            txtValorRutaT.Visible = true;
                            lblValorRutaT.Visible = true;
                            lblValorRutaT.Visible = true;
                            ddlMonedaValorRutaT.Visible = true;
                        }
                        if (Convert.ToBoolean(this.blPaicma.myDataSet.Tables[0].Rows[0]["requiereViaticos"]))
                        {
                            lblValorDiario.Visible = true;
                            txtValorDiario.Visible = true;
                            ddlMonedaValor.Visible = true;
                        }
                    }
                    else
                    {
                        lblRespuesta.Text = "Se ha presentado un problema al consultar datos de la solicitud, intente de nuevo";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        ViewState["_OperacionSolicitud"]  = "ErrorSalir";
                    }
                }
                else
                {
                    lblRespuesta.Text = "Se ha presentado un problema al consultar datos de la solicitud, intente de nuevo";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    ViewState["_OperacionSolicitud"] = "ErrorSalir";
                }
            }
            else
            {
                lblRespuesta.Text = "Se ha presentado un problema al consultar datos de la solicitud, intente de nuevo";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                ViewState["_OperacionSolicitud"] = "ErrorSalir";
            }
        }
        else
        {
            lblRespuesta.Text = "Se ha presentado un problema al consultar datos de la solicitud, intente de nuevo";
            principal.Visible = false;
            pnlRespuesta.Visible = true;
            ViewState["_OperacionSolicitud"] = "ErrorSalir";
        }
        this.blPaicma.Termina();

        // muestra o no muestra los botones 
        if(Session["permiteEditarSoloFormulario"] != null)
        {
            bool permiteEditarSolicitud = Convert.ToBoolean(Session["permiteEditarSoloFormulario"]);
            if (permiteEditarSolicitud == true)
            {
                btnGuardarCont.Visible = false;
                //btnGuardarSalir.Visible = false;
                btnGuardarEditar.Visible = true;
                ddlLineaIntervencion.Enabled = false;
                txtNombreFuncionario.Enabled = false;
                txtObjeto.Enabled = false;
            }
            else
            {
                btnGuardarEditar.Visible = false;
            }
        }

        //Verificar si es por rechazo
        if (Convert.ToInt32(Session["EstadoActualSolicitud"]) == EstadosSolicitud.RechazadaCoordinador || Convert.ToInt32(Session["EstadoActualSolicitud"]) == EstadosSolicitud.RechazadaAdministrador)
        {
            //btnGuardarSalir.Visible = false;
        }
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        string operacionActual = ViewState["_OperacionSolicitud"].ToString();
        if (operacionActual == "GuardarContinuar")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Session["soloVerSolicitud"] = false;
            Response.Redirect("frmWFPlanAccion.aspx");
            
        }

        if (operacionActual == "GuardarSalir")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
            
        }

        if (operacionActual == "ErrorSalir")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
            
        }
        if (operacionActual == "Inicio")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
            
        }
    }
    protected void chkViaticos_CheckedChanged(object sender, EventArgs e)
    {
        if (chkViaticos.Checked)
        {
            lblValorDiario.Visible = true;
            txtValorDiario.Visible = true;
            ddlMonedaValor.Visible = true;
        }
        else
        {
            lblValorDiario.Visible = false;
            txtValorDiario.Visible = false;
            ddlMonedaValor.Visible = false;
        }
    }
    protected void chkPorcionTerrestre_CheckedChanged(object sender, EventArgs e)
    {
        if (chkPorcionTerrestre.Checked){
            lblRutaTerrestre.Visible = true;
            txtRutaTerrestre.Visible = true;
            lblValorRutaT.Visible = true;
            txtValorRutaT.Visible = true;
            ddlMonedaValorRutaT.Visible = true;
        }else{
            lblRutaTerrestre.Visible = false;
            txtRutaTerrestre.Visible = false;
            lblValorRutaT.Visible = false;
            txtValorRutaT.Visible = false;
            ddlMonedaValorRutaT.Visible = false;
            
        }
        
    }
    protected void chkTiqueteAereo_CheckedChanged(object sender, EventArgs e)
    {
        if (chkTiqueteAereo.Checked)
        {
            Label10.Visible = true;
            txtRutaAerea.Visible = true;
        }
        else
        {
            Label10.Visible = false;
            txtRutaAerea.Visible = false;
        }
    }
    protected void txtValorDiario_TextChanged(object sender, EventArgs e)
    {
        if (txtValorDiario.Text == string.Empty)
        {
            txtValorDiario.Text = "0";
        }
        else
        {
           decimal valorDiario;
           decimal cantDias;
           decimal valorTerrestre;
           if (Decimal.TryParse(txtValorDiario.Text, out valorDiario))
           {
               if (Decimal.TryParse(txtNumeroDias.Text.Replace(".", ","), out cantDias))
               {
                   
                   if (Decimal.TryParse(txtValorRutaT.Text, out valorTerrestre))
                   {
                       string valorMostrar = String.Format("{0:N0}", Convert.ToDouble(((valorDiario * cantDias) + valorTerrestre)));
                       txtTotalViaticos.Text = valorMostrar;
                   }
                   else
                   {
                       txtValorRutaT.Text = "0";
                       txtTotalViaticos.Text = "0";
                   }

               }
               else
               {
                   txtNumeroDias.Text = "0";
                   txtTotalViaticos.Text = "0";
               }
           }
           else
           {
               txtValorDiario.Text = "0";
               txtTotalViaticos.Text = "0";
           }
        }
    }
    protected void txtNumeroDias_TextChanged(object sender, EventArgs e)
    {
        if (txtNumeroDias.Text == string.Empty)
        {
            txtValorDiario.Text = "0";
        }
        decimal valorDiario;
        decimal cantDias;
        decimal valorTerrestre;
        if (Decimal.TryParse(txtValorDiario.Text, out valorDiario))
        {
            if (Decimal.TryParse(txtNumeroDias.Text, out cantDias))
            {
                if (Decimal.TryParse(txtValorRutaT.Text, out valorTerrestre))
                {
                    txtTotalViaticos.Text = String.Format("{0:N0}", Convert.ToDouble(((valorDiario * cantDias) + valorTerrestre)));
                }
                else
                {
                    txtValorRutaT.Text = "0";
                    txtTotalViaticos.Text = "0";
                }
            }
            else
            {
                txtNumeroDias.Text = "0";
                txtTotalViaticos.Text = "0";
            }
        }
        else
        {
            txtValorDiario.Text = "0";
            txtTotalViaticos.Text = "0";
        }
    }

    //☻☻ validar  cuando cambie la fecha y calcular el nuevo valor de dias 
    protected void txtFechaFin_TextChanged(object sender, EventArgs e)
    {
        if(txtFechaInicio.Text != string.Empty && txtFechaFin.Text != string.Empty)
        {

            DateTime fechaF = Convert.ToDateTime(txtFechaFin.Text).Date;
            DateTime FechAc = Convert.ToDateTime(txtFechaInicio.Text).Date;
            double totalDias = ((FechAc - fechaF).TotalDays) * 1;

            if (totalDias < 0)
                totalDias = totalDias * -1;
            
            int valordias = (fechaF.Day - FechAc.Day) + 1;
            txtNumeroDias.Text = (totalDias - 0.5).ToString().Replace(",", ".");

            if (txtValorDiario.Text == string.Empty)
            {
                txtValorDiario.Text = "0";
            }
            else
            {
                decimal valorDiario;
                decimal cantDias;
                decimal valorTerrestre;
                if (Decimal.TryParse(txtValorDiario.Text, out valorDiario))
                {
                    if (Decimal.TryParse(txtNumeroDias.Text.Replace(".", ","), out cantDias))
                    {
                        if (Decimal.TryParse(txtValorRutaT.Text, out valorTerrestre))
                        {
                            string valorMostrar = String.Format("{0:N0}", Convert.ToDouble(((valorDiario * cantDias) + valorTerrestre)));
                            txtTotalViaticos.Text = valorMostrar;
                        }
                        else
                        {
                            txtValorRutaT.Text = "0";
                            txtTotalViaticos.Text = "0";
                        }
                    }
                    else
                    {
                        txtNumeroDias.Text = "0";
                        txtTotalViaticos.Text = "0";
                    }
                }
                else
                {
                    txtValorDiario.Text = "0";
                    txtTotalViaticos.Text = "0";
                }
            }
        }
    }


    public string agregarDecimales(string numero)
    {
        return String.Format("{0:N0}", Convert.ToDouble(numero));
    }

    // evento para validarla cantidad de dias 
    protected void txtFechaInicio_TextChanged(object sender, EventArgs e)
    {
        if (txtFechaInicio.Text != string.Empty && txtFechaFin.Text != string.Empty)
        {
            DateTime fechaF = Convert.ToDateTime(txtFechaFin.Text).Date;
            DateTime FechAc = Convert.ToDateTime(txtFechaInicio.Text).Date;
            int valordias = (fechaF.Day - FechAc.Day);

            txtNumeroDias.Text = (valordias - 0.5).ToString().Replace(",", ".");

            if (txtValorDiario.Text == string.Empty)
            {
                txtValorDiario.Text = "0";
            }
            else
            {
                decimal valorDiario;
                decimal cantDias;
                decimal valorTerrestre;
                if (Decimal.TryParse(txtValorDiario.Text, out valorDiario))
                {
                    if (Decimal.TryParse(txtNumeroDias.Text.Replace(".", ","), out cantDias))
                    {
                        if (Decimal.TryParse(txtValorRutaT.Text.Replace(".", ","), out valorTerrestre))
                        {
                            string valorMostrar = String.Format("{0:N0}", Convert.ToDouble(((valorDiario * cantDias) + valorTerrestre)));
                            txtTotalViaticos.Text = valorMostrar;
                        }
                        else
                        {
                            txtValorRutaT.Text = "0";
                            txtTotalViaticos.Text = "0";
                            //no pudo convertir
                        }
                        
                    }
                    else
                    {
                        txtNumeroDias.Text = "0";
                        txtTotalViaticos.Text = "0";
                    }
                }
                else
                {
                    txtValorDiario.Text = "0";
                    txtTotalViaticos.Text = "0";
                }
            }

        }
    }

    //☻☻ evento  para guardar la solictud editada 
    protected void btnGuardarEditar_Click(object sender, EventArgs e)
    {
        int idUsuarioLogin = Convert.ToInt32(Session["IdUsuario"]);
        DateTime creacion = DateTime.Now;

        if (this.blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            /// ☻☻ validar el estado de la solicitud para  modificarla o crearla segun el caso 
            if (Session["operacionSolicitud"].ToString() == "modificar")
            {
                /// el valor de la solicitud seleccionada 
                /// 
                string pruebavalor = Session["IdSolicitudRef"].ToString();
                if (Session["IdSolicitudRef"].ToString() != "0")
                {
                    // modificar los datos de la solicitud 
                    //Convert.ToDecimal(txtNumeroDias.Text.Replace(".", ","))
                    if (this.blPaicma.fntActualizarSolicitudPermiteEdicion_bol(Convert.ToInt64(Session["IdSolicitudRef"]), ddlLineaIntervencion.SelectedIndex, txtNombreFuncionario.Text.ToUpper(), txtObjeto.Text, Convert.ToDateTime(txtFechaInicio.Text), Convert.ToDateTime(txtFechaFin.Text), chkTiqueteAereo.Checked, txtRutaAerea.Text, chkPorcionTerrestre.Checked, txtRutaTerrestre.Text, chkViaticos.Checked, Convert.ToDecimal(txtValorDiario.Text), Convert.ToDecimal(txtNumeroDias.Text.Replace(".", ",")), Convert.ToDecimal(txtTotalViaticos.Text), idUsuarioLogin, idUsuarioLogin, creacion, EstadosSolicitud.IncompletaPlanes, txtRadicacion.Text,false))
                    {

                        if (this.blPaicma.fntRegistrarSolicitudHistorico_bol(Convert.ToInt64(Session["IdSolicitudRef"]), ddlLineaIntervencion.SelectedIndex, txtNombreFuncionario.Text.ToUpper(), txtObjeto.Text, Convert.ToDateTime(txtFechaInicio.Text), Convert.ToDateTime(txtFechaFin.Text), chkTiqueteAereo.Checked, txtRutaAerea.Text, chkPorcionTerrestre.Checked, txtRutaTerrestre.Text, chkViaticos.Checked, Convert.ToDecimal(txtValorDiario.Text), Convert.ToDecimal(txtNumeroDias.Text.Replace(".", ",")), Convert.ToDecimal(txtTotalViaticos.Text), idUsuarioLogin, idUsuarioLogin, creacion, EstadosSolicitud.IncompletaPlanes, txtRadicacion.Text))
                        {
                            lblRespuesta.Text = "Solicitud Actualizada  con éxito";
                            principal.Visible = false;
                            pnlRespuesta.Visible = true;
                            ViewState["_OperacionSolicitud"] = "ErrorSalir";
                        }
                        else
                        {
                            lblRespuesta.Text = "Se ha presentado un problema al crear la solicitud, actualizar estado incompleto";
                            principal.Visible = false;
                            pnlRespuesta.Visible = true;
                            // Session["OperacionSolicitud"] = "ErrorSalir";
                            ViewState["_OperacionSolicitud"] = "ErrorSalir";
                        }
                    }
                    else
                    {
                        lblRespuesta.Text = "Se ha presentado un problema al crear la solicitud, actualizar estado incompleto";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        ViewState["_OperacionSolicitud"] = "ErrorSalir";
                    }
                }
            }
        }
        else
        {
            lblRespuesta.Text = "Se ha presentado un problema al crear la solicitud, imposible conectar con la base de datos";
            principal.Visible = false;
            pnlRespuesta.Visible = true;
            ViewState["_OperacionSolicitud"] = "ErrorSalir";
            // no se ha podido conectar con base de datos
        }
        this.blPaicma.Termina();
    }
    protected void ddlMonedaValor_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlValorTotal.SelectedValue = ddlMonedaValor.SelectedValue;
        ddlMonedaValorRutaT.SelectedValue = ddlMonedaValor.SelectedValue;
    }
    protected void ddlMonedaValor_TextChanged(object sender, EventArgs e)
    {

    }
    protected void ddlValorTotal_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void txtValorRutaT_TextChanged(object sender, EventArgs e)
    {
        decimal valorDiario;
        decimal cantDias;
        decimal valorTerrestre;
        if (Decimal.TryParse(txtValorDiario.Text, out valorDiario))
        {
            if (Decimal.TryParse(txtNumeroDias.Text.Replace(".", ","), out cantDias))
            {
                if (Decimal.TryParse(txtValorRutaT.Text, out valorTerrestre))
                {
                    string valorMostrar = String.Format("{0:N0}", Convert.ToDouble(((valorDiario * cantDias) + valorTerrestre))); 
                    txtTotalViaticos.Text = valorMostrar;
                }
                else
                {
                    txtValorRutaT.Text = "0";
                    txtTotalViaticos.Text = "0";
                }
            }
            else
            {
                txtNumeroDias.Text = "0";
                txtTotalViaticos.Text = "0";
            }
        }
        else
        {
            txtValorDiario.Text = "0";
            txtTotalViaticos.Text = "0";
        }
    }
}