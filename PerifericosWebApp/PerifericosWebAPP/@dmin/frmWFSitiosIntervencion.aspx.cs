﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class _dmin_frmWFSitiosIntervencion : System.Web.UI.Page
{

    private blSisPAICMA blPaicma = new blSisPAICMA();
    //public TextBox[] arregloTextBoxs;
    //public DropDownList[] arregloCombos;
    public DropDownList[] ArregloDepartamento;
    public DropDownList[] ArregloMunicipio;
    public TextBox[] ArregloUbicacion;
    //public TextBox[] arregloTextBoxsLongitud;
    //public TextBox[] arregloTextBoxsOrden;
    //public TextBox[] arregloTextBoxsNombre;
    //public CheckBox[] arregloCheckBox;
    public int contadorControles;
    public int cont;
    public Label[] ListLabelDepartamento
    {
        get { return (Label[])Session["ListLabelDepartamento"]; }
        set { Session["ListLabelDepartamento"] = value; }
    }
    public Label[] ListLabelMunicipio
    {
        get { return (Label[])Session["ListLabelMunicipio"]; }
        set { Session["ListLabelMunicipio"] = value; }
    }
    public Label[] ListLabelUbicacion
    {
        get { return (Label[])Session["ListLabelUbicacion"]; }
        set { Session["ListLabelUbicacion"] = value; }
    }
    public DropDownList[] ListDropDownDepartamento
    {
        get { return (DropDownList[])Session["ListDropDownDepartamento"]; }
        set { Session["ListDropDownDepartamento"] = value; }
    }
    public DropDownList[] ListDropDownMunicipio
    {
        get { return (DropDownList[])Session["ListDropDownMunicipio"]; }
        set { Session["ListDropDownMunicipio"] = value; }
    }
    public TextBox[] ListTextBoxUbicacion
    {
        get { return (TextBox[])Session["ListTextBoxUbicacion"]; }
        set { Session["ListTextBoxUbicacion"] = value; }
    }
    protected int NumberOfControles
    {
        get { return (int)Session["NumControles"]; }
        set { Session["NumControles"] = value; }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                this.FillSitiosIntervencion();
            }
            ListLabelDepartamento = new Label[200];
            ListLabelMunicipio = new Label[200];
            ListLabelUbicacion = new Label[200];

            ListDropDownDepartamento = new DropDownList[200];
            ListDropDownMunicipio = new DropDownList[200];
            ListTextBoxUbicacion = new TextBox[200];
            lblError.Text = string.Empty;
            this.NumberOfControles = 0;
            contadorControles = 0;
        }
        cont = this.NumberOfControles;
        try
        {
            cont = this.NumberOfControles;
            ArregloDepartamento = new DropDownList[200];
            Array.Copy(ListDropDownDepartamento, ArregloDepartamento, 200);

            ArregloMunicipio = new DropDownList[200];
            Array.Copy(ListDropDownMunicipio, ArregloMunicipio, 200);

            ArregloUbicacion = new TextBox[200];
            Array.Copy(ListTextBoxUbicacion, ArregloUbicacion, 200);

            for (int i = 0; i < cont; i++)
            {
                //AgregarControles(TextBox txtnombreCampo, DropDownList ddlTipo, Label lblNombre, Label lblTipo, Label lblLongitud, TextBox txtLongitud, Label lblTitulo, Label lblOrden, TextBox txtOrden, TextBox txtLabel)
                AgregarControles(ListLabelDepartamento[i], ListLabelMunicipio[i], ListLabelUbicacion[i], ArregloDepartamento[i], ArregloMunicipio[i], ArregloUbicacion[i]);
            }
            Array.Copy(ArregloDepartamento, ListDropDownDepartamento, 200);
            Array.Copy(ArregloMunicipio, ListDropDownMunicipio, 200);
            Array.Copy(ArregloUbicacion, ListTextBoxUbicacion, 200);
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToBoolean(Session["soloVerSolicitud"]) == true)
        {
            SitiosDinamicos.Visible = false;
            panelBotones.Visible = false;
            panelSoloVer.Visible = true;
        }
    }


    protected void AgregarControles(Label lblDepartamento, Label lblMunicipio, Label lblUbicacion, DropDownList DdlDepartamento,DropDownList DdlMunicipio,TextBox txtUbicacion )
    {
        try
        {
            HtmlTableRow fila = new HtmlTableRow();
            HtmlTableRow fila2 = new HtmlTableRow();
            HtmlTableRow fila3 = new HtmlTableRow();
            HtmlTableRow filaSeparadora = new HtmlTableRow();
            HtmlTableCell celdaSeparadora = new HtmlTableCell();

            HtmlTableCell celdaA1 = new HtmlTableCell();
            HtmlTableCell celdaA2 = new HtmlTableCell();
            HtmlTableCell celdaB1 = new HtmlTableCell();
            HtmlTableCell celdaB2 = new HtmlTableCell();
            HtmlTableCell celdaC1 = new HtmlTableCell();
            HtmlTableCell celdaC2 = new HtmlTableCell();
           
            celdaSeparadora.ColSpan = 2;
            celdaSeparadora.Controls.Add(new LiteralControl("<hr/>"));
            filaSeparadora.Cells.Add(celdaSeparadora);


            celdaA1.Controls.Add(lblDepartamento);
            celdaA2.Controls.Add(DdlDepartamento);

            celdaB1.Controls.Add(lblMunicipio);
            celdaB2.Controls.Add(DdlMunicipio);

            celdaC1.Controls.Add(lblUbicacion);
            celdaC2.Controls.Add(txtUbicacion);

            fila.Cells.Add(celdaA1);
            fila.Cells.Add(celdaA2);

            fila2.Cells.Add(celdaB1);
            fila2.Cells.Add(celdaB2);

            fila3.Cells.Add(celdaC1);
            fila3.Cells.Add(celdaC2);

            SitiosDinamicos.Rows.Add(fila);
            SitiosDinamicos.Rows.Add(fila2);
            SitiosDinamicos.Rows.Add(fila3);
            SitiosDinamicos.Rows.Add(filaSeparadora);

        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }

    }
    protected void imgbNuevoSitio_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int numeroRegistro = this.NumberOfControles;
            ArregloDepartamento = new DropDownList[200];
            Array.Copy(ListDropDownDepartamento, ArregloDepartamento, 200);

            ArregloMunicipio = new DropDownList[200];
            Array.Copy(ListDropDownMunicipio, ArregloMunicipio, 200);

            ArregloUbicacion = new TextBox[200];
            Array.Copy(ListTextBoxUbicacion, ArregloUbicacion, 200);


            //Cargamos los La data para asignarla despues
            DataTable dataDepartamento = new DataTable();
            DataTable dataMunicipio = new DataTable();

            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarPorEstado("datosDepartamento", "tbl_Departamento", "1","1","nomDepartamento"))
                {
                    dataDepartamento = blPaicma.myDataSet.Tables["datosDepartamento"];
                    if (blPaicma.fntConsultarPorEstado("datosMunicipio", "tbl_Municipio", "1", "1", "nomMunicipio"))
                     {
                         dataMunicipio = blPaicma.myDataSet.Tables["datosMunicipio"];
                     }
                     else
                     {

                          //No Pudo cargar Listado de actividad
                     }
                }
                else
                {
                    //No se pudieron cargar los dropdown
                }
            }
            else
            {
                //No se ha podido conectar con la base de datos
            }
            blPaicma.Termina();


            //creamos los dropDownList
            DropDownList _ddlDepartamento = new DropDownList();
            _ddlDepartamento.ID = "ddlDepartamento" + numeroRegistro.ToString();
            _ddlDepartamento.CssClass = "Departamento";
            _ddlDepartamento.DataMember = "datosDepartamento";
            _ddlDepartamento.DataSource = dataDepartamento;
            _ddlDepartamento.DataValueField = "codDepartamentoAlf2";
            _ddlDepartamento.DataTextField = "nomDepartamento";
            _ddlDepartamento.DataBind();
            _ddlDepartamento.Items.Insert(_ddlDepartamento.Attributes.Count, "Seleccione...");
            ArregloDepartamento[numeroRegistro] = _ddlDepartamento;
            Array.Copy(ArregloDepartamento, ListDropDownDepartamento, 200);

            DropDownList _ddlMunicipio = new DropDownList();
            _ddlMunicipio.ID = "ddlMunicipio" + numeroRegistro.ToString();
            _ddlMunicipio.DataMember = "datosMunicipio";
            foreach (ListItem var in fillMunicipio(dataMunicipio))
            {
                _ddlMunicipio.Items.Add(var);
            }
            _ddlMunicipio.DataValueField = "Value";
            _ddlMunicipio.DataTextField = "Text";
            _ddlMunicipio.DataBind();
            _ddlMunicipio.Enabled = false;



            _ddlMunicipio.Items.Insert(_ddlMunicipio.Attributes.Count, "Seleccione...");
            ArregloMunicipio[numeroRegistro] = _ddlMunicipio;
            Array.Copy(ArregloMunicipio, ListDropDownMunicipio, 200);

            TextBox _txtUbicacion = new TextBox();
            _txtUbicacion.ID = "txtUbicacion" + numeroRegistro.ToString();
            _txtUbicacion.TextMode = TextBoxMode.MultiLine;
            ArregloUbicacion[numeroRegistro] = _txtUbicacion;
            Array.Copy(ArregloUbicacion, ListTextBoxUbicacion, 200);
            _txtUbicacion.Attributes.CssStyle.Add("width","69%");
                
            Label LabelDepartamento = new Label();
            LabelDepartamento.ID = "lblDepartamento" + numeroRegistro.ToString();
            LabelDepartamento.Text = "Departamento ";
            ListLabelDepartamento[numeroRegistro] = LabelDepartamento;

            Label LabelMunicipio = new Label();
            LabelMunicipio.ID = "lblMunicipio" + numeroRegistro.ToString();
            LabelMunicipio.Text = "Municipio ";
            ListLabelMunicipio[numeroRegistro] = LabelMunicipio;

            Label LabelUbicacion = new Label();
            LabelUbicacion.ID = "lblUbicacion" + numeroRegistro.ToString();
            LabelUbicacion.Text = "Ubicación ";
            ListLabelUbicacion[numeroRegistro] = LabelUbicacion;

            ///agregamos los controles al panel y tabla
            ///NombreCampo, Combo, Label nuevo, label Combo, label Longitud, longitud, 
            AgregarControles(LabelDepartamento, LabelMunicipio, LabelUbicacion, _ddlDepartamento, _ddlMunicipio, _txtUbicacion);
            this.NumberOfControles++;

            lblError.Text = string.Empty;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }

    protected void btnGuardarCont_Click(object sender, EventArgs e)
    {
        Session["OperacionSolicitud"] = "GuardarContinuar";
        btnOk.Text = "Continuar";
        this.guardarSitios();
    }
    protected void btnGuardarSalir_Click(object sender, EventArgs e)
    {
        Session["OperacionSolicitud"] = "GuardarSalir";
        btnOk.Text = "Terminar";
        this.guardarSitios();
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmWFSolicitudComision.aspx");
    }
    protected void gvSitiosIntervencion_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int intIndex = Convert.ToInt32(e.CommandArgument);
        GridViewRow selectedRow = gvSitiosIntervencion.Rows[intIndex];
        TableCell Item = selectedRow.Cells[0];
        Int64 intIDSitio = Convert.ToInt64(Item.Text);
        Session["intIdSitio"] = Convert.ToInt64(intIDSitio);
        Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        if (e.CommandName == "EditarSitio")
        {
            Session["IdSolicitudRef"] = idSolicitud;
            base.Response.Redirect("frmWFEditExistingSite.aspx");

        }
        if (e.CommandName == "EliminarSitio")
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntEliminarElementoBigInt("WFSol_SitiosSol", "idSitiosSol", intIDSitio))
                {
                    lblRespuestaInterna.Text = "Registro Eliminado Correctamente.";
                    lblRespuestaInterna.Style.Add("background-color", "#FBEE57");
                    lblRespuestaInterna.Visible = true;
                }
                else
                {
                    //No pudo eliminar
                    lblRespuestaInterna.Text = "No se pudo eliminar el registro.";
                    lblRespuestaInterna.Style.Add("background-color", "#FBEE57");
                    lblRespuestaInterna.Visible = true;
                }
            }
            else
            {
                //No pudo conectar a la base de datos.
            }
            blPaicma.Termina();


        }
    }

    public void guardarSitios()
    {


        bool guardoCorrecto = true;
        bool primeraVes = true;
        int intCantidadControles = this.NumberOfControles;
        ArregloDepartamento = new DropDownList[200];
        Array.Copy(ListDropDownDepartamento, ArregloDepartamento, 200);

        ArregloMunicipio = new DropDownList[200];
        Array.Copy(ListDropDownMunicipio, ArregloMunicipio, 200);

        ArregloUbicacion = new TextBox[200];
        Array.Copy(ListTextBoxUbicacion, ArregloUbicacion, 200);
        Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        for (int i = 0; i < intCantidadControles; i++)
        {
            if (guardoCorrecto)
            {

                DropDownList DepartamentoGuardar = new DropDownList();
                DropDownList MunicipioGuardar = new DropDownList();
                TextBox UbicacionGuardar = new TextBox();

                DepartamentoGuardar = ArregloDepartamento[i];
                MunicipioGuardar = ArregloMunicipio[i];
                UbicacionGuardar = ArregloUbicacion[i];

                int idUsuarioLogin = Convert.ToInt32(Session["IdUsuario"]);
                DateTime creacion = DateTime.Now;

                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    
                    if (blPaicma.fntSitiosIntervencion_bol(idSolicitud, DepartamentoGuardar.SelectedValue, MunicipioGuardar.SelectedValue, UbicacionGuardar.Text, idUsuarioLogin, idUsuarioLogin, creacion))
                    {
                        
                        if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, EstadosSolicitud.IncompletaSoporte, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
                        {
                            if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", EstadosSolicitud.IncompletaSoporte, "WFSol_SolicitudComision", "idEstadoActual"))
                            {
                                guardoCorrecto = true;
                                primeraVes = false;
                            }
                            else
                            {
                                //Fallo actualizar estado en la comision
                            }

                        }
                        else
                        {
                            //fallo actualizar estado Solicitud
                        }
                    }
                    else
                    {
                        guardoCorrecto = false;
                        primeraVes = false;
                    }
                }
                else
                {
                    guardoCorrecto = false;
                    primeraVes = false;
                }
                this.blPaicma.Termina();
            }
        }

       int valor  = gvSitiosIntervencion.Rows.Count;

       if (guardoCorrecto && !primeraVes || valor >0)
        {
           //Agregado para actualizar siempre
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, EstadosSolicitud.IncompletaSitios, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
                {
                    if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", EstadosSolicitud.IncompletaSitios, "WFSol_SolicitudComision", "idEstadoActual"))
                    {
                        lblRespuesta.Text = "Sitios Agregados con éxito";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        Session["IdSolicitudRef"] = idSolicitud;
                    }
                    else
                    {
                        //Fallo actualizar estado en la comision
                    }

                }
                else
                {
                    //fallo actualizar estado Solicitud
                }
            }
            else
            {
                //No se pudo establecer conexion.
            }
            this.blPaicma.Termina();
        }
       else
       {
           if (guardoCorrecto && primeraVes)
           {
               lblRespuesta.Text = "Se presento un problema al conectar con la base de datos, Intente Nuevamente.";
               principal.Visible = false;
               pnlRespuesta.Visible = true;
               Session["OperacionSolicitud"] = "ErrorSalir";
           }
           if (!guardoCorrecto && !primeraVes)
           {
               lblRespuesta.Text = "Se presento un problema al intentar guardar al menos uno de los planes, Intente Nuevamente.";
               principal.Visible = false;
               pnlRespuesta.Visible = true;
               Session["OperacionSolicitud"] = "ErrorSalir";
           }
       }
        
    }

    public void FillSitiosIntervencion()
    {
        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarSitios("datosSitiosInter", idSolicitud, "", 0))
                {
                    if (blPaicma.myDataSet.Tables["datosSitiosInter"].Rows.Count > 0)
                    {
                        this.gvSitiosIntervencion.DataMember = "datosSitiosInter";
                        this.gvSitiosIntervencion.DataSource = this.blPaicma.myDataSet;
                        this.gvSitiosIntervencion.DataBind();
                    }
                }
                else
                {
                    //Imposible Consultar lista de planes
                }
            }
            else
            {
                //No pudo conectar a la base de datos
            }
            blPaicma.Termina();

        }
    }
    protected void imgbEliminarSitio_Click(object sender, ImageClickEventArgs e)
    {
        if (this.NumberOfControles == 0)
        {
            imgbEliminarSitio.Enabled = false;
        }
        else
        {
            this.NumberOfControles--;
            SitiosDinamicos.Rows.RemoveAt(SitiosDinamicos.Rows.Count - 1);
            SitiosDinamicos.Rows.RemoveAt(SitiosDinamicos.Rows.Count - 1);
            SitiosDinamicos.Rows.RemoveAt(SitiosDinamicos.Rows.Count - 1);
            SitiosDinamicos.Rows.RemoveAt(SitiosDinamicos.Rows.Count - 1);
            if (this.NumberOfControles == 0)
            {
                imgbEliminarSitio.Enabled = false;
            }
        }
    }

    protected void gvSitiosIntervencion_PreRender(object sender, EventArgs e)
    {
        if (gvSitiosIntervencion.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvSitiosIntervencion.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvSitiosIntervencion.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvSitiosIntervencion.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        string operacionActual = Session["OperacionSolicitud"].ToString();
        if (operacionActual == "GuardarContinuar")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Session["soloVerSolicitud"] = false;
            Response.Redirect("frmWFSoporteSolicitud.aspx");
        }
        if (operacionActual == "GuardarSalir")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "ErrorSalir")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "Inicio")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "Anular")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
    }

    /// <summary>
    /// ☻☻  metodo que retorna la lista  de valor y atributos para el dropdownlist actividad
    /// </summary>
    /// <returns></returns>
    protected List<ListItem> fillMunicipio(DataTable datatable)
    {
        List<ListItem> resultado = new List<ListItem>();
        foreach (DataRow row in datatable.Rows)
        {
            string valor = row[0].ToString();
            string texto = row[1].ToString();
            string id = row[2].ToString();

            ListItem valorCombo2 = new ListItem();
            valorCombo2.Value = valor;
            valorCombo2.Text = texto;
            valorCombo2.Attributes["data-Indicador"] = id;
            resultado.Add(valorCombo2);
        }
        return resultado;
    }






}