﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmPermisos : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {

            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                ftnValidarPermisos();
                fntCargarGrillaDirectorio(0, 0, 0, string.Empty, 0);
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }

    public void ftnValidarPermisos()
    {
        //int intIdFormulario = 0;
        string strBuscar = string.Empty;
        string strNuevo = string.Empty;
        string strEditar = string.Empty;
        string strEliminar = string.Empty;

        //obtiene el nombre de la pagina actual
        string[] strRutaPagina = HttpContext.Current.Request.RawUrl.Split('/');
        string strNombrePagina = strRutaPagina[strRutaPagina.GetUpperBound(0)];
        strRutaPagina = strNombrePagina.Split('?');
        strNombrePagina = strRutaPagina[strRutaPagina.GetLowerBound(0)];
        //fin obtiene el nombre de la pagina actual

        if (strNombrePagina != string.Empty)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                //intIdFormulario = Convert.ToInt32(Request.QueryString["op"].ToString());
                //Session["intIdFormulario"] = intIdFormulario;
                if (blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(Session["IdUsuario"].ToString()), strNombrePagina))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strBuscar = blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                        strNuevo = blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                        strEditar = blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                        strEliminar = blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();

                        if (strBuscar == "False")
                        {
                            imgbBuscar.Visible = false;
                        }
                        if (strNuevo == "False")
                        {
                            imgbNuevo.Visible = false;
                        }
                        if (strEditar == "False")
                        {
                            imgbEditar.Visible = false;
                            imgbGravar.Visible = false;
                        }
                        if (strEliminar == "False")
                        {
                            imgbEliminar.Visible = false;
                        }

                    }
                }
                blPaicma.Termina();
            }
        }
        else
        {
            Response.Redirect("@dmin/frmLogin.aspx");
        }
    }

    public void fntInactivarPaneles(Boolean bolValor)
    {
        pnlAccionesPermiso.Visible = bolValor;
        pnlEliminacion.Visible = bolValor;
        pnlMenuPermiso.Visible = bolValor;
        pnlPermisoNuevo.Visible = bolValor;
        pnlRespuesta.Visible = bolValor;
        PnlUsuarioGrilla.Visible = bolValor;
        pnlEditarPermisos.Visible = bolValor;
    }

    public void fntCargarGrillaDirectorio(int intTipo, int intIdUsuario, int IntTipoConexion, string strUsuario, int intIdEstado)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaUsuarioAdmin("strDsGrillaUsuario", intTipo, intIdUsuario, IntTipoConexion, strUsuario, intIdEstado, string.Empty))
            {
                gvUsuario.Columns[0].Visible = true;
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvUsuario.DataMember = "strDsGrillaUsuario";
                    gvUsuario.DataSource = blPaicma.myDataSet;
                    gvUsuario.DataBind();
                    lblErrorGv.Text = string.Empty;
                }
                else
                {
                    gvUsuario.DataMember = "strDsGrillaUsuario";
                    gvUsuario.DataSource = blPaicma.myDataSet;
                    gvUsuario.DataBind();
                    lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                gvUsuario.Columns[0].Visible = false;
            }
            blPaicma.Termina();
        }
    }

    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles(false);
        pnlAccionesPermiso.Visible = true;
        imgbEncontrar.Visible = false;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        pnlPermisoNuevo.Visible = true;
        fntCargarObjetos();
        Session["Operacion"] = "Crear";
    }

    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["EditarPermiso"] == null)
        {
            string strRuta = "frmPermisos.aspx";
            Response.Redirect(strRuta);
        }
        else
        {
            if (Session["EditarPermiso"].ToString() == "SI")
            {
                pnlEditarPermisos.Visible = false;
                pnlPermisoNuevo.Visible = true;
                Session["intIdPermisoFormulario"] = null;
                Session["EditarPermiso"] = null;
                Session["strIdFormulario"] = null;
            }
        }
        
    }

    public void fntCargarTipoConexion()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTipoConexion("strDsTipoConexion"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlTipoConexion.DataMember = "strDsTipoConexion";
                    ddlTipoConexion.DataSource = blPaicma.myDataSet;
                    ddlTipoConexion.DataValueField = "segcon_Id";
                    ddlTipoConexion.DataTextField = "segCon_Descripcion";
                    ddlTipoConexion.DataBind();
                    ddlTipoConexion.Items.Insert(ddlTipoConexion.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarEstado()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaEstadoTipoUsuario("strDsEstado"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlEstado.DataMember = "strDsEstado";
                    ddlEstado.DataSource = blPaicma.myDataSet;
                    ddlEstado.DataValueField = "est_Id";
                    ddlEstado.DataTextField = "est_Descripcion";
                    ddlEstado.DataBind();
                    ddlEstado.Items.Insert(ddlEstado.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarFormulariosDisponibles()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaFormularioNoAsignados_bol("strDsFormulario", Convert.ToInt32(Session["intIdUsuarioNew"].ToString())))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlFormulario.DataMember = "strDsFormulario";
                    ddlFormulario.DataSource = blPaicma.myDataSet;
                    ddlFormulario.DataValueField = "segfor_Id";
                    ddlFormulario.DataTextField = "segfor_DescripcionMenu";
                    ddlFormulario.DataBind();
                    ddlFormulario.Items.Insert(ddlFormulario.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarObjetos()
    {
        fntCargarTipoConexion();
        fntCargarEstado();
    }

    protected void gvUsuario_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            gvUsuario.Columns[0].Visible = true;
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvUsuario.Rows[intIndex];
            TableCell Item = selectedRow.Cells[0];
            int intIdUsuario = Convert.ToInt32(Item.Text);
            Session["intIdUsuarioNew"] = intIdUsuario;
            imgbEditar.Visible = true;
            gvUsuario.Columns[0].Visible = false;
        }
    }

    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles(false);
        pnlAccionesPermiso.Visible = true;
        imgbEncontrar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        pnlPermisoNuevo.Visible = true;
        int intIdUsuarioNew = 0;
        fntCargarObjetos();
        Session["Operacion"] = "Actualizar";
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            intIdUsuarioNew = Convert.ToInt32(Session["intIdUsuarioNew"].ToString());
            if (blPaicma.fntConsultaUsuarioAdmin("strDsUsuario", 1, intIdUsuarioNew, 0, string.Empty, 0, string.Empty))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlTipoConexion.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                    txtUsuario.Text = blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                    ddlEstado.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                    fntCargarGrillaUsuarioPermisos();
                    ftnInactivarCampos(false);
                    ftnValidarPermisos();
                }
            }
            blPaicma.Termina();
        }

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaFormularioNoAsignados_bol("strDsFormulario", Convert.ToInt32(Session["intIdUsuarioNew"].ToString())))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    int intCantidadFormulario = blPaicma.myDataSet.Tables[0].Rows.Count;
                    imgbNuevoPermiso.Visible = true;
                }
                else
                {
                    imgbNuevoPermiso.Visible = false;
                }
            }
            blPaicma.Termina();
        }

        
    }

    public void ftnInactivarCampos(Boolean bolValor)
    {
        ddlTipoConexion.Enabled = bolValor;
        txtUsuario.Enabled = bolValor;
        ddlEstado.Enabled = bolValor;
    }

    public void fntCargarGrillaUsuarioPermisos()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaPermisoUsuarioFormulario_bol("strDsGrillaUsuarioFormulario", Convert.ToInt32(Session["intIdUsuarioNew"].ToString())))
            {
                gvFormularioPermisos.Columns[0].Visible = true;
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvFormularioPermisos.DataMember = "strDsGrillaUsuarioFormulario";
                    gvFormularioPermisos.DataSource = blPaicma.myDataSet;
                    gvFormularioPermisos.DataBind();
                    lblErrorGv.Text = string.Empty;
                }
                else
                {
                    gvFormularioPermisos.DataMember = "strDsGrillaUsuarioFormulario";
                    gvFormularioPermisos.DataSource = blPaicma.myDataSet;
                    gvFormularioPermisos.DataBind();
                    lblError.Text = "No hay formularios asignados a este usuario.";
                }
                gvFormularioPermisos.Columns[0].Visible = false;
            }
            blPaicma.Termina();
        }
    }

    protected void gvFormularioPermisos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            Session["EditarPermiso"] = "SI";
            gvFormularioPermisos.Columns[0].Visible = true;
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvFormularioPermisos.Rows[intIndex];
            TableCell Item = selectedRow.Cells[0];
            int intIdPermisoFormulario = Convert.ToInt32(Item.Text);
            Session["intIdPermisoFormulario"] = intIdPermisoFormulario;

            TableCell Item2 = selectedRow.Cells[1];
            string strFormulario = Item2.Text;
            txtFormulario.Text = strFormulario;
           

            TableCell Item3 = selectedRow.Cells[2];
            string strConsultar = Item3.Text;

            TableCell Item4 = selectedRow.Cells[3];
            string strCrear = Item4.Text;

            TableCell Item5 = selectedRow.Cells[4];
            string strModificar = Item5.Text;

            TableCell Item6 = selectedRow.Cells[5];
            string strEliminar = Item6.Text;

            TableCell Item7 = selectedRow.Cells[6];
            string strImprimir = Item7.Text;
           
            fntInactivarPaneles(false);

            pnlEditarPermisos.Visible = true;
            pnlAccionesPermiso.Visible = true;
            ftnLlenarOpvionesPermisos(strConsultar, strCrear, strModificar, strEliminar, strImprimir);
            gvFormularioPermisos.Columns[0].Visible = false;

        }
    }

    public void ftnLlenarOpvionesPermisos(string strConsultar, string strCrear, string strModificar, string strEliminar, string strImprimir)
    {
        ddlConsultar.SelectedValue = strConsultar;
        ddlCrear.SelectedValue = strCrear;
        ddlModificar.SelectedValue = strModificar;
        ddlEliminar.SelectedValue = strEliminar;
        ddlImprimir.SelectedValue = strImprimir;
    }


    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["EditarPermiso"] == null)
        {
            if (Session["strIdFormulario"] == null)
            {
                ///
            }
            else 
            {
                if (Session["strIdFormulario"].ToString() == "SI")
                {
                    fntInactivarPaneles(false);
                    pnlRespuesta.Visible = true;
                    if (ftnGuardarPermisosFormulario())
                    {
                        lblRespuesta.Text = "Registro insertado satisfactoriamente";
                    }
                    else
                    {
                        lblRespuesta.Text = "Problema al insertar el registro, por favor comuníquese con el administrador.";
                    }
                }
            }
        }
        else
        {
            if (Session["EditarPermiso"].ToString() == "SI")
            {
                fntInactivarPaneles(false);
                pnlRespuesta.Visible = true;
                if (ftnActualizarPermisosFormulario())
                {
                    lblRespuesta.Text = "Registro actualizado satisfactoriamente";
                }
                else
                {
                    lblRespuesta.Text = "Problema al actualizar el registro, por favor comuníquese con el administrador.";    
                }
            }
        }
        imgbNuevoPermiso.Visible = true;
        imgbEliminar.Visible = true;
    }

    public Boolean ftnActualizarPermisosFormulario()
    {
        Boolean bolValor = false;
        int intConsultar = 0;
        int intCrear = 0;
        int intModificar = 0;
        int intEliminar = 0;
        int intImprimir = 0;
        int intIdPermidoUsuarioFormulario = 0;
        
        intIdPermidoUsuarioFormulario = Convert.ToInt32(Session["intIdPermisoFormulario"].ToString());

        
        if (ddlConsultar.SelectedValue.ToString() == "SI")
        {
            intConsultar = 1;
        }
        if (ddlCrear.SelectedValue.ToString() == "SI")
        {
            intCrear  = 1;
        }
        if (ddlModificar.SelectedValue.ToString() == "SI")
        {
            intModificar = 1;
        }
        if (ddlEliminar.SelectedValue.ToString() == "SI")
        {
            intEliminar = 1;
        }
        if (ddlImprimir.SelectedValue.ToString() == "SI")
        {
            intImprimir = 1;
        }

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntModificarPermisoFormulario_bol(intIdPermidoUsuarioFormulario, intConsultar, intCrear, intModificar, intEliminar, intImprimir))
            {
                bolValor = true;
            }
            else
            {
                bolValor = false;
                lblError.Text = "Problema al actualizar el registro, por favor comuníquese con el administrador.";
            }

            blPaicma.Termina();
        }

        return bolValor;
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        if (Session["EditarPermiso"] == null)
        {
            if (Session["strIdFormulario"].ToString() == "SI")
            {
                fntInactivarPaneles(false);
                pnlAccionesPermiso.Visible = true;
                imgbEncontrar.Visible = false;
                imgbGravar.Visible = true;
                imgbCancelar.Visible = true;
                pnlPermisoNuevo.Visible = true;
                //fntCargarObjetos();
                Session["Operacion"] = "Actualizar";
                fntCargarGrillaUsuarioPermisos();
                ftnInactivarCampos(false);
                ftnValidarPermisos();
                Session["intIdPermisoFormulario"] = null;
                Session["EditarPermiso"] = null;
                Session["strIdFormulario"] = null;
            }
            else
            {
                fntInactivarPaneles(false);
                pnlAccionesPermiso.Visible = true;
                imgbEncontrar.Visible = false;
                imgbGravar.Visible = true;
                imgbCancelar.Visible = true;
                pnlPermisoNuevo.Visible = true;
                fntCargarObjetos();
                Session["Operacion"] = "Actualizar";
                fntCargarGrillaUsuarioPermisos();
                ftnInactivarCampos(false);
                ftnValidarPermisos();
                Session["intIdPermisoFormulario"] = null;
                Session["EditarPermiso"] = null;
                Session["strIdFormulario"] = null;
            }
        }
        else
        {
            if (Session["EditarPermiso"].ToString() == "SI")
            {
                fntInactivarPaneles(false);
                pnlAccionesPermiso.Visible = true;
                imgbEncontrar.Visible = false;
                imgbGravar.Visible = true;
                imgbCancelar.Visible = true;
                pnlPermisoNuevo.Visible = true;
                fntCargarObjetos();
                Session["Operacion"] = "Actualizar";
                fntCargarGrillaUsuarioPermisos();
                ftnInactivarCampos(false);
                ftnValidarPermisos();
                Session["intIdPermisoFormulario"] = null;
                Session["EditarPermiso"] = null;
                Session["strIdFormulario"] = null;
            }
        }
    }

    protected void gvFormularioPermisos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvFormularioPermisos.Columns[0].Visible = true;
        gvFormularioPermisos.PageIndex = e.NewPageIndex;
        fntCargarGrillaUsuarioPermisos();
        gvFormularioPermisos.Columns[0].Visible = false;
    }

    protected void gvUsuario_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUsuario.Columns[0].Visible = true;
        gvUsuario.PageIndex = e.NewPageIndex;
        fntCargarGrillaDirectorio(0, 0, 0, string.Empty, 0);
        gvUsuario.Columns[0].Visible = false;
    }

    protected void imgbNuevoPermiso_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles(false);
        pnlAccionesPermiso.Visible = true;
        imgbNuevoPermiso.Visible = false;
        imgbEliminar.Visible = false;
        pnlEditarPermisos.Visible = true;
        txtFormulario.Visible = false;
        ddlFormulario.Visible = true;
        fntCargarFormulariosDisponibles();
        Session["strIdFormulario"] = "SI";
    }

    public Boolean ftnGuardarPermisosFormulario()
    {
        Boolean bolValor = false;
        int intConsultar = 0;
        int intCrear = 0;
        int intModificar = 0;
        int intEliminar = 0;
        int intImprimir = 0;
        int intIdFormulario = 0;

        if (ddlConsultar.SelectedValue.ToString() == "SI")
        {
            intConsultar = 1;
        }
        if (ddlCrear.SelectedValue.ToString() == "SI")
        {
            intCrear = 1;
        }
        if (ddlModificar.SelectedValue.ToString() == "SI")
        {
            intModificar = 1;
        }
        if (ddlEliminar.SelectedValue.ToString() == "SI")
        {
            intEliminar = 1;
        }
        if (ddlImprimir.SelectedValue.ToString() == "SI")
        {
            intImprimir = 1;
        }

        if (ddlFormulario.SelectedValue != "Seleccione...")
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                intIdFormulario = Convert.ToInt32(ddlFormulario.SelectedValue.ToString());
                lblErrorNuevo.Text = string.Empty;
                ////inssertar
                if (blPaicma.fntIngresarNewPermiso_bol(Convert.ToInt32(Session["intIdUsuarioNew"].ToString()), intIdFormulario, intConsultar, intCrear, intModificar, intEliminar, intImprimir, Convert.ToInt32(Session["IdUsuario"].ToString())))
                {
                    bolValor = true;
                }
                else
                {
                    bolValor = false;
                    lblError.Text = "Problema al insertar el registro, por favor comuníquese con el administrador.";
                }
                blPaicma.Termina();
            }
        }
        else
        {
            lblErrorNuevo.Text = "Debe seleccionar un formulario";
            bolValor = false;
        }
        return bolValor;
    }

    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles(false);
        pnlAccionesPermiso.Visible = true;
        imgbEncontrar.Visible = true;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = false;
        imgbCancelar.Visible = true;
        pnlPermisoNuevo.Visible = true;
        fntCargarObjetos();
        fntLimpiarObjetos();  
    }

    public void fntLimpiarObjetos()
    {
        txtUsuario.Text = string.Empty;
    }


    protected void imgbEncontrar_Click(object sender, ImageClickEventArgs e)
    {
        int intIdTipoConexion = 0;
        string strUsuario = string.Empty;
        int intIdEstado = 0;

        if (ddlTipoConexion.SelectedValue != "Seleccione...")
        {
            intIdTipoConexion = Convert.ToInt32(ddlTipoConexion.SelectedValue.ToString());
        }

        if (txtUsuario.Text.Trim() != string.Empty)
        {
            strUsuario = txtUsuario.Text.Trim();
        }

        if (ddlEstado.SelectedValue != "Seleccione...")
        {
            intIdEstado = Convert.ToInt32(ddlEstado.SelectedValue.ToString());
        }

        fntCargarGrillaDirectorio(2, 0, intIdTipoConexion, strUsuario, intIdEstado);
        fntInactivarPaneles(false);
        pnlMenuPermiso.Visible = true;
        PnlUsuarioGrilla.Visible = true;
    }

    protected void imgbEliminar_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles(false);
        pnlEliminacion.Visible = true;
        lblEliminacion.Text = "¿Está seguro de eliminar el registro?";
    }


    public Boolean ftnEliminarPermisosFormulario()
    {
        Boolean bolValor = false;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntEliminarPermisoFormulario_bol(1, Convert.ToInt32(Session["intIdUsuarioNew"].ToString()), Convert.ToInt32(Session["intIdPermisoFormulario"].ToString())))
            {
                bolValor = true;
            }
            else
            {
                bolValor = false;
                lblError.Text = "Problema al insertar el registro, por favor comuníquese con el administrador.";
            }
            blPaicma.Termina();
        }

        return bolValor;
    }

    public Boolean ftnEliminarPermisosUsuario()
    {
        Boolean bolValor = false;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntEliminarPermisoFormulario_bol(2, Convert.ToInt32(Session["intIdUsuarioNew"].ToString()), 0))
            {
                bolValor = true;
            }
            else
            {
                bolValor = false;
                lblError.Text = "Problema al insertar el registro, por favor comuníquese con el administrador.";
            }
            blPaicma.Termina();
        }

        return bolValor;
    }

    protected void btnSi_Click(object sender, EventArgs e)
    {
        if (Session["EditarPermiso"] == null)
        {
            if (Session["strIdFormulario"] == null)
            {
                fntInactivarPaneles(false);
                pnlRespuesta.Visible = true;
                if (ftnEliminarPermisosUsuario())
                {
                    lblRespuesta.Text = "Registro eliminado satisfactoriamente";
                }
                else
                {
                    lblRespuesta.Text = "Problema al eliminar el registro, por favor comuníquese con el administrador.";
                }
            }

        }
        else
        {
            if (Session["EditarPermiso"].ToString() == "SI")
            {
                fntInactivarPaneles(false);
                pnlRespuesta.Visible = true;
                if (ftnEliminarPermisosFormulario())
                {
                    lblRespuesta.Text = "Registro eliminado satisfactoriamente";
                }
                else
                {
                    lblRespuesta.Text = "Problema al eliminar el registro, por favor comuníquese con el administrador.";
                }
            }
        }
    }
    protected void btnNo_Click(object sender, EventArgs e)
    {
        fntInactivarPaneles(false);
        pnlAccionesPermiso.Visible = true;
        if (Session["EditarPermiso"] == null)
        {
            pnlPermisoNuevo.Visible = true;
        }
        else
        {
            pnlEditarPermisos.Visible = true;
        }
    }
}