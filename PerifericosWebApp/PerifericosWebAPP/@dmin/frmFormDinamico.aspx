﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmFormDinamico" Codebehind="frmFormDinamico.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <asp:Panel ID="pnlEliminacion" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto">
                <tr>
                    <td>
                        <asp:Label ID="lblEliminacion" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
<%--                        <asp:Button ID="btnSi" runat="server" Text="SI" OnClick="btnSi_Click" />--%>
               <%--         <asp:Button ID="btnNo" runat="server" Text="NO" OnClick="btnNo_Click" />--%>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto">
                <tr>
                    <td>
                        <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" Text="Aceptar" OnClick="btnOk_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlMenuDinamico" runat="server">
        <div class="Botones">
            <asp:ImageButton ID="imgbBuscar" runat="server"
                ImageUrl="~/Images/BuscarAccion.png" ToolTip="Buscar"
                OnClick="imgbBuscar_Click" />
            <asp:ImageButton ID="imgbNuevo" runat="server" ImageUrl="~/Images/Nuevo.png"
                ToolTip="Nuevo" OnClick="imgbNuevo_Click" Style="height: 32px" />
            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png"
                Visible="False" ToolTip="Editar" OnClick="imgbEditar_Click" />
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlAccionesDinamico" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server"
                ImageUrl="~/Images/buscar.png" ToolTip="Ejecutar Busqueda"
                OnClick="imgbEncontrar_Click" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png"
                ToolTip="Guardar" OnClick="imgbGravar_Click" />
            <asp:ImageButton ID="imgbCancelar" runat="server" ImageUrl="~/Images/cancelar.png" ToolTip="Cancelar" OnClick="imgbCancelar_Click" />

            <asp:ImageButton ID="imgbEliminar" runat="server"
                ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar"
                OnClick="imgbEliminar_Click" />
        </div>


    </asp:Panel>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlCamposTabla" runat="server" Visible="False">
                <div class="contacto">
                <table  runat="server" id="Activiades">
                    <thead>
                        <tr>
                            <td colspan="4">
                                <h1>Formularios Dinamicos: Creacion</h1>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTabla" runat="server" Text="Nombre de la tabla"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtTabla" runat="server" MaxLength="50"></asp:TextBox>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblAbreviatura" runat="server" Text="Abreviatura"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtAbreviatura" runat="server" MaxLength="10" ></asp:TextBox>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblObservacion" runat="server" Text="Observación"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine"
                                    MaxLength="250" Width="100%" placeholder="Ingrese Observacion, Máximo 250 caracteres"></asp:TextBox>
                                <asp:RegularExpressionValidator runat="server" ID="valInput"
                                    ControlToValidate="txtObservacion"
                                    ValidationExpression="^[\s\S]{0,250}$"
                                    ErrorMessage="Ingrese Máximo 250 Caracteres" ForeColor="Red"
                                    Display="Dynamic">*Ingrese Máximo 250 Caracteres</asp:RegularExpressionValidator>  
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="¿Esta relacionado con una Victima"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:CheckBox ID="checkRelVictima" runat="server" />
                                <%--    <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine"
                                    MaxLength="200" Width="100%"></asp:TextBox>--%>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Filtro Organizacional"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlOpcionesFiltroOrg" runat="server" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlOpcionesFiltroOrg_SelectedIndexChanged" Enabled="true">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>

                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label4" runat="server" Text="Filtro por Departamento"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlOpcionesFiltroDep" runat="server" AutoPostBack="True" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Label ID="Label5" runat="server" Visible="false"></asp:Label>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                      </thead>
                </table>
                 <table  runat="server" id="camposdinamicos">
                    <thead>
                        <tr>
                            <td colspan="3">
                                <h1 id="mensajeCrear" runat="server">Crear Campos</h1>
                                <asp:Button runat="server" ID="btnConfigPermisos" Text="Configurar Permisos de Formulario" Visible="false" OnClick="btnConfigPermisos_Click" type="button"/>
                            </td>
                            <td>
                                <asp:ImageButton ID="imgbNuevoCampo" runat="server"
                                    ImageUrl="~/Images/NuevoCampo.png" ToolTip="Nuevo Campo"
                                    OnClick="imgbNuevoCampo_Click" Visible="False" /></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Label ID="lblError" runat="server"></asp:Label>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                </table>
                </div>

            </asp:Panel>
            <br />

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel ID="PnlTablasGrilla" runat="server">
        <div class="formulario2">
            <table class="contacto2">
                <tr>
                    <td colspan="4">
                        <h1>Formularios Dinamicos: Creacion</h1>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:GridView ID="gvTablas" runat="server" AutoGenerateColumns="False"
                            Width="100%" CssClass="mGrid" PagerStyle-CssClass="pgr"
                            AlternatingRowStyle-CssClass="alt" AllowPaging="True"
                            OnRowCommand="gvTablas_RowCommand"
                            OnPageIndexChanging="gvTablas_PageIndexChanging">
                            <AlternatingRowStyle CssClass="alt" />
                            <Columns>
                                <asp:BoundField DataField="admTab_Id" HeaderText="admTab_Id" />
                                <asp:BoundField DataField="admTab_NombreTabla"
                                    HeaderText="Tabla" />
                                <asp:BoundField DataField="admTab_AbreviaturaTabla"
                                    HeaderText="Abreviatura" />
                                <asp:BoundField DataField="admTab_NumeroCampos"
                                    HeaderText="Campos" />
                                <asp:BoundField DataField="admTab_fecha"
                                    HeaderText="Fecha" />
                                <asp:BoundField DataField="admTab_Observacion" HeaderText="Observación" />
                                <asp:BoundField DataField="segusu_login" HeaderText="Usuario" />
                                <asp:ButtonField ButtonType="Image" CommandName="Seleccion"
                                    ImageUrl="~/Images/seleccionar.png" />
                            </Columns>
                            <PagerStyle CssClass="pgr" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorGv" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlBusqueda" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" runat="server" id="tblBusqueda">

                <tr>
                    <td>
                        <asp:Label ID="lblNombreBusqueda" runat="server" Text="Nombre de la tabla"></asp:Label>
                    </td>
                    <td style="text-align: left">
                        <asp:TextBox ID="txtTablaBusqueda" runat="server" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                    </td>
                    <td style="text-align: left">
                        <asp:DropDownList ID="ddlEstadoBusqueda" runat="server">
                            <asp:ListItem Value="3">Seleccione...</asp:ListItem>
                            <asp:ListItem Value="0">Inactivo</asp:ListItem>
                            <asp:ListItem Value="1">Activo</asp:ListItem>
                        </asp:DropDownList>
                    </td>

                </tr>


                <tr>
                    <td>&nbsp;</td>
                    <td style="text-align: left">
                        <asp:Label ID="lblErrorBusqueda" runat="server"></asp:Label>
                    </td>
                </tr>


            </table>
        </div>
    </asp:Panel>
    <%--   <div class="contacto2">
        <asp:Button runat="server" ID="btnConfigPermisos" Text="Configurar Permisos de Formulario"/>

    </div>--%>

    <asp:Panel ID="pnlCampos" runat="server" Visible="False">
        <div class="formulario2">

            <table class="contacto2">
                <tr>
                    <td align="center">
                        <asp:GridView ID="gvCampos" runat="server" AutoGenerateColumns="False"
                            Width="100%" CssClass="mGrid" PagerStyle-CssClass="pgr"
                            AlternatingRowStyle-CssClass="alt" AllowPaging="True"
                            PageSize="10" OnPageIndexChanging="gvCampos_PageIndexChanging" OnRowCommand="gvCampos_RowCommand">
                            <AlternatingRowStyle CssClass="alt" />
                            <Columns>
                                <asp:BoundField DataField="camForm_IdCampo" HeaderText="Id" />
                                <asp:BoundField DataField="camForm_NombreCampo" HeaderText="Campo" />
                                <asp:BoundField DataField="tipCon_DescControl"
                                    HeaderText="Tipo de Dato" />
                                <asp:BoundField DataField="camForm_LongitudMax"
                                    HeaderText="Longitud" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button runat="server" ButtonType="Image" CommandName="Seleccion" CommandArgument='<%# Container.DataItemIndex %>' ImageUrl="~/Images/seleccionar.png" Text="Opciones"
                                            Visible='<%# IsLista(Eval("tipCon_DescControl").ToString()) %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="pgr" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblGVcampos" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>


</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="Server">
    <script type="text/javascript">
        //$(document).ready(function () {
        //    $('input[data-type='number]').keypress(function (e) { var regex = new RegExp("^[0-9]$");var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);if (regex.test(str)) {return true;} e.preventDefault();return false;});
        //    $('input[data-type='number']').blur(function () { var regex = new RegExp("^$|^[0-9]{1,3}$");var str = $(this).val();if (regex.test(str)) {return true;} else {$(this).val("");$(this).focus();return false;}});
        //    //$('#contenido_gvOperador').DataTable();
        //    //$('#contenido_gvActividadesOperador').DataTable();
        //    //$('#contenido_txtDesde').datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
        //    //$('#contenido_txtHasta').datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
        //});
    </script>
</asp:Content>
