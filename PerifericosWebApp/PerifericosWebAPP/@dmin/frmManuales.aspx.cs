﻿//using ASP;
using System;
using System.Configuration;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmManuales : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }
            this.ftnValidarPermisos();
            this.fntCargarGrillaManuales();
        }
    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        this.blPaicma = null;
    }
    public void ftnValidarPermisos()
    {
        string arg_05_0 = string.Empty;
        string arg_0B_0 = string.Empty;
        string arg_11_0 = string.Empty;
        string arg_17_0 = string.Empty;
        string[] array = HttpContext.Current.Request.RawUrl.Split(new char[]
		{
			'/'
		});
        string text = array[array.GetUpperBound(0)];
        array = text.Split(new char[]
		{
			'?'
		});
        text = array[array.GetLowerBound(0)];
        if (text != string.Empty)
        {
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (this.blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(this.Session["IdUsuario"].ToString()), text) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                    this.blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                    this.blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                    this.blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();
                }
                this.blPaicma.Termina();
                return;
            }
        }
        else
        {
            base.Response.Redirect("@dmin/frmLogin.aspx");
        }
    }
    public void fntCargarGrillaManuales()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaManuales("strDsManuales"))
            {
                this.gvManuales.Columns[0].Visible = true;
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gvManuales.DataMember = "strDsManuales";
                    this.gvManuales.DataSource = this.blPaicma.myDataSet;
                    this.gvManuales.DataBind();
                    this.lblErrorGv.Text = string.Empty;
                }
                else
                {
                    this.gvManuales.DataMember = "strDsManuales";
                    this.gvManuales.DataSource = this.blPaicma.myDataSet;
                    this.gvManuales.DataBind();
                    this.lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                this.gvManuales.Columns[0].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    protected void gvManuales_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string text = string.Empty;
        string str = string.Empty;
        string arg_11_0 = string.Empty;
        string text2 = string.Empty;
        this.gvManuales.Columns[0].Visible = true;
        if (e.CommandName == "Ver")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow = this.gvManuales.Rows[index];
            TableCell tableCell = gridViewRow.Cells[2];
            try
            {
                text2 = tableCell.Text;
                if (text2.Trim() != string.Empty)
                {
                    str = "FormatosEstandar/";
                    text = ConfigurationManager.AppSettings["RutaServerDinamico"];
                    text = text + str + text2;
                    base.Response.Write("<script language='JavaScript'>window.open('" + text + "')</script>");
                    this.lblErrorGv.Text = string.Empty;
                }
                else
                {
                    this.lblErrorGv.Text = "No hay un documento asociado en el registro de seguimiento.";
                }
            }
            catch
            {
                this.lblErrorGv.Text = "Error al asociar al documento, por favor ponerse en contacto con el administrador.";
            }
        }
        this.gvManuales.Columns[0].Visible = false;
    }
}