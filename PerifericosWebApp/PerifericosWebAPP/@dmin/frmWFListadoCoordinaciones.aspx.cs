﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmWFListadoCoordinaciones : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                this.FillCoordinaciones();
                return;
            }
            base.Response.Redirect("@dmin/frmLogin.aspx");
        }
    }

    public void FillCoordinaciones()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarCoordinacion_bol("strCoordinacionEspecifica", "", ""))
            {
                if (blPaicma.myDataSet.Tables["strCoordinacionEspecifica"].Rows.Count > 0)
                {
                    this.gvCoordinaciones.DataMember = "strCoordinacionEspecifica";
                    this.gvCoordinaciones.DataSource = this.blPaicma.myDataSet;
                    this.gvCoordinaciones.DataBind();
                }
            }
            else
            {
                //no puede consultar
            }
        }
        else
        {
            //no pudo inicializar
        }
        blPaicma.Termina();
    }

    protected void gvCoordinaciones_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditarCoordinacion")
        {
            Session["ModoCoordinacion"] = "Modificar";
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvCoordinaciones.Rows[intIndex];
            TableCell Item1 = selectedRow.Cells[2];
            string idCoordinacion = Item1.Text;
            Session["idCoordinacionRef"] = idCoordinacion;
            base.Response.Redirect("frmWFCoordinacion.aspx");

        }
    }
    protected void gvCoordinaciones_PreRender(object sender, EventArgs e)
    {
        if (gvCoordinaciones.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvCoordinaciones.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvCoordinaciones.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvCoordinaciones.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
    protected void btnNuevaCoordinacion_Click(object sender, EventArgs e)
    {
        Session["ModoCoordinacion"] = "crear";
        Session["idCoordinacionRef"] = "0";
        base.Response.Redirect("frmWFCoordinacion.aspx");
    }
}