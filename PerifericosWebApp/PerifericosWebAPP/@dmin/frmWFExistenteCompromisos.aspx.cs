﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmWFExistenteCompromisos : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                if (Session["IdCompromiso"] != null)
                {
                    FullCompromiso();
                }
                else
                {
                    Response.Redirect("~/@dmin/frmLogin.aspx");
                }

            }

        }
    }

    //☻☻ metodo para cargar los  valores del compromiso para editar 
    private void FullCompromiso()
    {
        Int64 idLegalizacion = Convert.ToInt64(Session["IdLegalizacionRef"]);
        Int64 idCompromiso = Convert.ToInt64(Session["IdCompromiso"]);
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarCompromisos("datosCompromiso", "id", idLegalizacion, idCompromiso))
            {
                if (blPaicma.myDataSet.Tables["datosCompromiso"].Rows.Count > 0)
                {
                    txtCompromiso.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["descripcion"].ToString();
                    txtFechaCompromiso.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["fechaCompromiso"].ToString();
                    txtFechaCompromiso.CssClass = "pluginFecha";

                    txtResponsable.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["responsable"].ToString();
                }
            }
            else
            {
                //Imposible Consultar lista de planes
                //lblRespuesta.Text = "Se ha presentado un problema al consultar lista de Compromisos, intente de nuevo";
                //principal.Visible = false;
                //pnlRespuesta.Visible = true;
                //Session["OperacionSolicitud"] = "ErrorSalir";
            }
        }
        else
        {
            //No pudo conectar a la base de datos
            //lblRespuesta.Text = "Se ha presentado un problema al consultar lista de Compromisos, no se ha podido conectar a la base de datos";
            //principal.Visible = false;
            //pnlRespuesta.Visible = true;
            //Session["OperacionSolicitud"] = "ErrorSalir";

        }
        blPaicma.Termina();

    }

    //☻☻ evento para guardar los cambios de compromiso
    protected void btnGuardar_Click(object sender, EventArgs e)
    {

           Int64 idLegalizacion = Convert.ToInt64(Session["IdLegalizacionRef"]);
           Int64 idCompromiso = Convert.ToInt64(Session["IdCompromiso"]);
        
             if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    if (blPaicma.fntActualizarCompromisos_bol(idCompromiso, txtCompromiso.Text, Convert.ToDateTime(txtFechaCompromiso.Text), txtResponsable.Text))
                    {
                        Response.Redirect("frmWFCompromisos.aspx");
                    }
                    else
                    {
                      
                    }
                }
                this.blPaicma.Termina();
    }




}