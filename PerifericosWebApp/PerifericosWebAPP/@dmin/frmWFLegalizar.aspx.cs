﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class frmWFLegalizar : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    private EstadosSolicitud Estados = new EstadosSolicitud();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                this.FillPorLegalizar();
            }

           
        }
    }

    public void FillPorLegalizar()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            int usuarioCreo = Convert.ToInt32(Session["IdUsuario"]);

            if (blPaicma.fntConsultarPorLegalizar("dataRespuesta", usuarioCreo))
            {
                DataTable Listado = new DataTable();
                Listado = blPaicma.myDataSet.Tables["dataRespuesta"];
                gvSolicitud.DataSource = Listado;
                gvSolicitud.DataBind();
            }
            else
            {
                //no pudo consultar solicitud
            }
        }
        else
        {
            //no pudo inciar base de datos
        }
        this.blPaicma.Termina();
    }

    //protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    //{
    //    string Filtro = "";
    //    string Parametro = "";

    //    string caseSwitch = ddlOpcionesFiltro.SelectedValue;

    //    if (caseSwitch != "0")
    //    {
    //        if (txtBuscar.Text.Trim() != string.Empty)
    //        {
    //            switch (caseSwitch)
    //            {
    //                case "1":
    //                    Filtro = "idSolicitud";
    //                    break;
    //                case "2":
    //                    Filtro = "IdentificadorSolicitud";
    //                    break;
    //                default:
    //                    Filtro = "idSolicitud";
    //                    break;
    //            }
    //            Parametro = txtBuscar.Text.Trim();
    //        }
    //        else
    //        {
    //            LblError.Text = "Escriba el el valor a buscar";
    //            LblError.Visible = true;
    //        }
    //    }
    //    else
    //    {
    //        LblError.Text = "Para Buscar Seleccione un Filtro"; 
    //        LblError.Visible = true;
    //    }
    //    if (blPaicma.inicializar(blPaicma.conexionSeguridad))
    //    {
    //        int usuarioCreo = Convert.ToInt32( Session["IdUsuario"]);

    //        if (blPaicma.fntConsultaSolicitudPorLike("dataRespuesta", Filtro, Parametro, usuarioCreo))
    //        {
    //            DataTable Listado = new DataTable();
    //            Listado = blPaicma.myDataSet.Tables["dataRespuesta"];
    //            gvSolicitud.DataSource = Listado;
    //            gvSolicitud.DataBind();
    //        }
    //        else
    //        {
    //            //no pudo consultar solicitud
    //        }
    //    }
    //    else
    //    {
    //       //no pudo inciar base de datos
    //    }
    //    this.blPaicma.Termina();
    //}
    protected void gvSolicitud_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "legalizar")
        {
            int intIndex = Convert.ToInt32(e.CommandArgument);

            Guid IdUnico = Guid.NewGuid();

            GridViewRow selectedRow = gvSolicitud.Rows[intIndex];
            TableCell Item1 = selectedRow.Cells[0];
            long intIdSolicitud = Convert.ToInt64(Item1.Text);
            Session["IdSolicitudRef"] = intIdSolicitud;
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntIngresarLegalizacion_bol(intIdSolicitud, DateTime.Now, 0, EstadosSolicitud.LegIncompletaPlanes, DateTime.Now, IdUnico.ToString()))
                {
                    if (blPaicma.fntIngresarEstadoSolicitud_bol(intIdSolicitud, EstadosSolicitud.LegIncompletaPlanes, Convert.ToInt32(Session["IdUsuario"]),DateTime.Now, false))
                    {
                        if (blPaicma.fntActualizarEstado_bol(intIdSolicitud, "idSolicitud", EstadosSolicitud.LegIncompletaPlanes, "WFSol_SolicitudComision", "idEstadoActual"))
                        {
                            Session["vieneDeLegalizar"] = "verdad";
                            Response.Redirect("frmWFDetalleSolicitud.aspx");
                            //Actgualizado Correcto
                        }
                        else
                        {
                            //Fallo actualizar estado en la comision
                        }

                    }else{
                        //fallo actualizar estado Solicitud
                    }
                }
                else
                {
                    //Fallo al registrar Legalizacion
                }
            }
           
        
        }
    }
    protected void gvSolicitud_PreRender(object sender, EventArgs e)
    {
        if (gvSolicitud.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvSolicitud.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvSolicitud.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvSolicitud.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
}