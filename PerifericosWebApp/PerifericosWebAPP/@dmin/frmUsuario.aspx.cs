﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class _dmin_frmUsuario : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }
            this.ftnValidarPermisos();
            this.fntCargarGrillaDirectorio(0, 0, 0, string.Empty, 0, string.Empty);
        }
    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        this.blPaicma = null;
    }
    public void ftnValidarPermisos()
    {
        string a = string.Empty;
        string a2 = string.Empty;
        string a3 = string.Empty;
        string a4 = string.Empty;
        string[] array = HttpContext.Current.Request.RawUrl.Split(new char[]
		{
			'/'
		});
        string text = array[array.GetUpperBound(0)];
        array = text.Split(new char[]
		{
			'?'
		});
        text = array[array.GetLowerBound(0)];
        if (text != string.Empty)
        {
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (this.blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(this.Session["IdUsuario"].ToString()), text) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    a = this.blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                    a2 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                    a3 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                    a4 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();
                    if (a == "False")
                    {
                        this.imgbBuscar.Visible = false;
                    }
                    if (a2 == "False")
                    {
                        this.imgbNuevo.Visible = false;
                    }
                    if (a3 == "False")
                    {
                        this.imgbEditar.Visible = false;
                        this.imgbGravar.Visible = false;
                    }
                    if (a4 == "False")
                    {
                        this.imgbEliminar.Visible = false;
                    }
                }
                this.blPaicma.Termina();
                return;
            }
        }
        else
        {
            base.Response.Redirect("@dmin/frmLogin.aspx");
        }
    }
    public void fntInactivarPaneles(bool bolValor)
    {
        this.pnlAccionesUsuario.Visible = bolValor;
        this.pnlEliminacion.Visible = bolValor;
        this.pnlMenuUsuario.Visible = bolValor;
        this.pnlRespuesta.Visible = bolValor;
        this.PnlUsuarioGrilla.Visible = bolValor;
        this.pnlUsuarioNuevo.Visible = bolValor;
    }
    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlAccionesUsuario.Visible = true;
        this.imgbEncontrar.Visible = false;
        this.imgbEliminar.Visible = false;
        this.imgbGravar.Visible = true;
        this.imgbCancelar.Visible = true;
        this.pnlUsuarioNuevo.Visible = true;
        this.fntCargarObjetos();
        this.Session["Operacion"] = "Crear";
    }
    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        string url = "frmUsuario.aspx";
        base.Response.Redirect(url);
    }
    public void fntCargarGrillaDirectorio(int intTipo, int intIdUsuario, int IntTipoConexion, string strUsuario, int intIdEstado, string correoUsuario)
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaUsuarioAdmin("strDsGrillaUsuario", intTipo, intIdUsuario, IntTipoConexion, strUsuario, intIdEstado, correoUsuario))
            {
                this.gvUsuario.Columns[0].Visible = true;
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gvUsuario.DataMember = "strDsGrillaUsuario";
                    this.gvUsuario.DataSource = this.blPaicma.myDataSet;
                    this.gvUsuario.DataBind();
                    this.lblErrorGv.Text = string.Empty;
                }
                else
                {
                    this.gvUsuario.DataMember = "strDsGrillaUsuario";
                    this.gvUsuario.DataSource = this.blPaicma.myDataSet;
                    this.gvUsuario.DataBind();
                    this.lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                this.gvUsuario.Columns[0].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarTipoConexion()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaTipoConexion("strDsTipoConexion") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlTipoConexion.DataMember = "strDsTipoConexion";
                this.ddlTipoConexion.DataSource = this.blPaicma.myDataSet;
                this.ddlTipoConexion.DataValueField = "segcon_Id";
                this.ddlTipoConexion.DataTextField = "segCon_Descripcion";
                this.ddlTipoConexion.DataBind();
                this.ddlTipoConexion.Items.Insert(this.ddlTipoConexion.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarEstado()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaEstadoTipoUsuario("strDsEstado") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlEstado.DataMember = "strDsEstado";
                this.ddlEstado.DataSource = this.blPaicma.myDataSet;
                this.ddlEstado.DataValueField = "est_Id";
                this.ddlEstado.DataTextField = "est_Descripcion";
                this.ddlEstado.DataBind();
                this.ddlEstado.Items.Insert(this.ddlEstado.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    protected void ddlTipoConexion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.ddlTipoConexion.SelectedValue == "Seleccione...")
        {
            this.ftnOcultarCampos(false);
        }
        if (this.ddlTipoConexion.SelectedItem.ToString() == "Local")
        {
            this.ftnOcultarCampos(true);
        }
        if (this.ddlTipoConexion.SelectedItem.ToString() == "Directorio Activo")
        {
            this.ftnOcultarCampos(false);
        }
    }
    public void ftnOcultarCampos(bool bolValor)
    {
        this.lblContraseña.Visible = bolValor;
        this.txtPassword.Visible = bolValor;
    }
    public void ftnInactivarCampos(bool bolValor)
    {
        this.ddlTipoConexion.Enabled = bolValor;
        this.txtUsuario.Enabled = bolValor;
    }
    public void fntCargarObjetos()
    {
        this.fntCargarTipoConexion();
        this.fntCargarEstado();
    }
    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        if (this.Session["Operacion"].ToString() != "Crear")
        {
            if (this.Session["Operacion"].ToString() == "Actualizar")
            {
                bool flag = true;
                string arg_B7_0 = string.Empty;
                string strPassword = string.Empty;
                if (this.txtPassword.Text.Trim() != string.Empty)
                {
                    this.txtPassword.Text.Trim();
                    strPassword = this.fntEncriptaPassword();
                   // flag = this.fntValidadComplejidadPassword(this.txtPassword.Text.Trim());
                }
                if (!flag)
                {
                    this.lblError.Text = "La contraseña no cumple con las políticas de seguridad. Por favor verificar.";
                    return;
                }
                if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
                {
                    if (this.blPaicma.fntModificarUsuario_bol(strPassword, Convert.ToInt32(this.ddlEstado.SelectedValue.ToString()), Convert.ToInt32(this.Session["intIdUsuarioNew"].ToString()), this.txtCorreo.Text.Trim()))
                    {
                        this.fntInactivarPaneles(false);
                        this.pnlRespuesta.Visible = true;
                        this.lblRespuesta.Text = "Registro actualizado satisfactoriamente";
                    }
                    else
                    {
                        this.fntInactivarPaneles(false);
                        this.pnlRespuesta.Visible = true;
                        this.lblRespuesta.Text = "Problema al actualizar el registro, por favor comuníquese con el administrador.";
                    }
                    this.blPaicma.Termina();
                }
            }
            return;
        }
        //if (!this.ftnValidarCampos_bol())
        //{
        //    this.lblError.Visible = true;
        //    return;
        //}
        if (!this.fntValidarExistenciaUsuario())
        {
            this.lblError.Visible = true;
            return;
        }
        if (this.ftnIngresarUsuario())
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.fntInactivarPaneles(false);
            this.pnlRespuesta.Visible = true;
            this.lblRespuesta.Text = "Usuario creado satisfactoriamente";
            return;
        }
        this.lblError.Visible = true;
    }
    public bool ftnValidarCampos_bol()
    {
        if (this.ddlTipoConexion.SelectedValue == "Seleccione...")
        {
            this.lblError.Text = "Debe seleccionar el tipo de conexión.";
            return false;
        }
        if (this.ddlTipoConexion.SelectedItem.ToString() == "Local")
        {
            if (this.txtPassword.Text.Trim() == string.Empty)
            {
                this.lblError.Text = "Debe digitar la contraseña";
                return false;
            }
            if (!this.ftnValidarSeguridadCampos_bol(this.txtPassword.Text.Trim()))
            {
                this.lblError.Text = "El campo de CONTRASEÑA tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
                return false;
            }
            //if (!this.fntValidadComplejidadPassword(this.txtPassword.Text.Trim()))
            //{
            //    this.lblError.Text = "La contraseña no cumple con las políticas de seguridad. Por favor verificar.";
            //    return false;
            //}
        }
        if (this.txtUsuario.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar el usuario con el cual desea ingresar al sistema.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtUsuario.Text.Trim()))
        {
            this.lblError.Text = "El campo de USUARIO tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
            return false;
        }
        if (this.ddlEstado.SelectedValue == "Seleccione...")
        {
            this.lblError.Text = "Debe seleccionar el estado del usuario.";
            return false;
        }
        if (this.txtCorreo.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar el Correo del usuario.";
            return false;
        }
        return true;
    }
    public bool ftnValidarSeguridadCampos_bol(string strTextoCampo)
    {
        bool result = false;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntEliminarTablaExpresionCampo_bol() && this.blPaicma.fntIngresarExpresionCampo_bol(strTextoCampo) && this.blPaicma.fntConsultaExpresionSeguridad("strDsSeguridadCampos", strTextoCampo))
            {
                int num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                result = (num <= 0);
            }
            this.blPaicma.Termina();
        }
        return result;
    }
    public bool fntValidarExistenciaUsuario()
    {
        bool result = false;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaSoloUsuarioCantidad_bol("strDsUsuario", this.txtUsuario.Text.Trim()))
            {
                int num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                if (num > 0)
                {
                    result = false;
                    this.lblError.Text = "Ya existe un usuario registrado, por favor verificar.";
                }
                else
                {
                    result = true;
                }
            }
            this.blPaicma.Termina();
        }
        return result;
    }
    public bool ftnIngresarUsuario()
    {
        bool result = false;
        string strPassword = string.Empty;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            strPassword = this.fntEncriptaPassword();
            if (this.blPaicma.fnt_ConsultaAplicativo_bol("dsAplicativo") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                int intIdAplicativo = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0]["segApl_id"].ToString());
                if (this.blPaicma.fntIngresarUsuarioNew_bol(Convert.ToInt32(this.ddlTipoConexion.SelectedValue.ToString()), this.txtUsuario.Text.Trim(), strPassword, Convert.ToInt32(this.ddlEstado.SelectedValue.ToString()), Convert.ToInt32(this.Session["IdUsuario"].ToString()), intIdAplicativo, this.txtCorreo.Text.Trim()))
                {
                    result = true;
                }
                else
                {
                    result = false;
                    this.lblError.Text = "Problema al ingresar el registro, por favor comuníquese con el administrador.";
                }
            }
            this.blPaicma.Termina();
        }
        return result;
    }
    private string fntEncriptaPassword()
    {
        string text = string.Empty;
        string text2 = string.Empty;
        text2 = this.txtPassword.Text.Trim();
        if (text2.Length > 0)
        {
            int num = Convert.ToInt32(text2.Length);
            for (int i = 0; i < num; i++)
            {
                char c = text2[i];
                int value = (int)c;
                char c2 = text2[i];
                int num2 = (int)c2;
                num2 += i * 2;
                int value2 = Convert.ToInt32(value) + Convert.ToInt32(num2);
                char c3 = Convert.ToChar(value2);
                text += c3;
            }
        }
        return text;
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        string url = "frmUsuario.aspx";
        base.Response.Redirect(url);
    }
    protected void gvUsuario_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            this.gvUsuario.Columns[0].Visible = true;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow = this.gvUsuario.Rows[index];
            TableCell tableCell = gridViewRow.Cells[0];
            int num = Convert.ToInt32(tableCell.Text);
            this.Session["intIdUsuarioNew"] = num;
            this.imgbEditar.Visible = true;
            this.gvUsuario.Columns[0].Visible = false;
        }
    }
    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlAccionesUsuario.Visible = true;
        this.imgbEncontrar.Visible = false;
        this.imgbGravar.Visible = true;
        this.imgbCancelar.Visible = true;
        this.pnlUsuarioNuevo.Visible = true;
        this.fntCargarObjetos();
        this.Session["Operacion"] = "Actualizar";
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            int intIdUsuario = Convert.ToInt32(this.Session["intIdUsuarioNew"].ToString());
            if (this.blPaicma.fntConsultaUsuarioAdmin("strDsUsuario", 1, intIdUsuario, 0, string.Empty, 0, string.Empty) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlTipoConexion.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                this.txtUsuario.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                this.ddlEstado.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                this.txtCorreo.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][10].ToString();
                if (this.ddlTipoConexion.SelectedItem.ToString() == "Local")
                {
                    this.ftnOcultarCampos(true);
                }
                if (this.ddlTipoConexion.SelectedItem.ToString() == "Directorio Activo")
                {
                    this.ftnOcultarCampos(false);
                }
                this.ftnInactivarCampos(false);
                this.ftnValidarPermisos();
            }
            this.blPaicma.Termina();
        }
    }
    protected void imgbEliminar_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlEliminacion.Visible = true;
        this.lblEliminacion.Text = "¿Está seguro de eliminar el usuario?";
    }
    protected void btnNo_Click(object sender, EventArgs e)
    {
        this.pnlEliminacion.Visible = false;
        this.pnlAccionesUsuario.Visible = true;
        this.pnlUsuarioNuevo.Visible = true;
    }
    protected void btnSi_Click(object sender, EventArgs e)
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntEliminarUsuario_bol(Convert.ToInt32(this.Session["intIdUsuarioNew"].ToString())))
            {
                this.lblRespuesta.Text = "Registro eliminado satisfactoriamente.";
            }
            else
            {
                this.lblRespuesta.Text = "No se puede eliminar el registro ya que tiene información asociada.";
            }
            this.fntInactivarPaneles(false);
            this.pnlRespuesta.Visible = true;
            this.blPaicma.Termina();
        }
    }
    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlAccionesUsuario.Visible = true;
        this.imgbEncontrar.Visible = true;
        this.imgbEliminar.Visible = false;
        this.imgbGravar.Visible = false;
        this.imgbCancelar.Visible = true;
        this.pnlUsuarioNuevo.Visible = true;
        this.fntCargarObjetos();
        this.fntLimpiarObjetos();
        this.ftnOcultarCampos(false);
    }
    public void fntLimpiarObjetos()
    {
        this.txtUsuario.Text = string.Empty;
    }
    protected void imgbEncontrar_Click(object sender, ImageClickEventArgs e)
    {
        int intTipoConexion = 0;
        string strUsuario = string.Empty;
        string strCorreo = string.Empty;

        int intIdEstado = 0;
        if (this.ddlTipoConexion.SelectedValue != "Seleccione...")
        {
            intTipoConexion = Convert.ToInt32(this.ddlTipoConexion.SelectedValue.ToString());
        }
        if (this.txtUsuario.Text.Trim() != string.Empty)
        {
            strUsuario = this.txtUsuario.Text.Trim();
        }
        if (this.ddlEstado.SelectedValue != "Seleccione...")
        {
            intIdEstado = Convert.ToInt32(this.ddlEstado.SelectedValue.ToString());
        }
        if (this.txtCorreo.Text.Trim() != string.Empty)
        {
            strCorreo = this.txtCorreo.Text.Trim();
        }
        this.fntCargarGrillaDirectorio(2, 0, intTipoConexion, strUsuario, intIdEstado,strCorreo);
        this.fntInactivarPaneles(false);
        this.pnlMenuUsuario.Visible = true;
        this.PnlUsuarioGrilla.Visible = true;
    }
    public bool fntValidadComplejidadPassword(string strPassword)
    {
        int num = 0;
        int num2 = 0;
        num2 = strPassword.Length;
        Regex regex = new Regex("[a-z]");
        int num3 = 0;
        Regex regex2 = new Regex("[A-Z]");
        int num4 = 0;
        Regex regex3 = new Regex("[0-9]");
        int num5 = 0;
        Regex regex4 = new Regex("[$,%,&,#,!]");
        MatchCollection matchCollection = regex.Matches(strPassword);
        foreach (Match arg_68_0 in matchCollection)
        {
            num++;
        }
        num3 = matchCollection.Count;
        MatchCollection matchCollection2 = regex2.Matches(strPassword);
        num = 0;
        foreach (Match arg_B9_0 in matchCollection2)
        {
            num++;
        }
        num4 = matchCollection2.Count;
        MatchCollection matchCollection3 = regex3.Matches(strPassword);
        num = 0;
        foreach (Match arg_10A_0 in matchCollection3)
        {
            num++;
        }
        num5 = matchCollection3.Count;
        MatchCollection matchCollection4 = regex4.Matches(strPassword);
        num = 0;
        foreach (Match arg_15B_0 in matchCollection4)
        {
            num++;
        }
        int count = matchCollection4.Count;
        if (num2 >= 8)
        {
            bool flag = true;
            return num3 != 0 && num4 != 0 && num5 != 0 && count != 0 && flag;
        }
        return false;
    }
    protected void gvUsuario_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvUsuario.Columns[0].Visible = true;
        this.gvUsuario.PageIndex = e.NewPageIndex;
        this.fntCargarGrillaDirectorio(0, 0, 0, string.Empty, 0, string.Empty);
        this.gvUsuario.Columns[0].Visible = false;
    }

}