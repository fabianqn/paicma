﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmWFNuevaSolicitud" Codebehind="frmWFNuevaSolicitud.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function funBlurValor(sender) {
            var num = sender.value.replace(/\./g, '');
            if (!isNaN(num)) {
                num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
                num = num.split('').reverse().join('').replace(/^[\.]/, '');
                document.getElementById(sender.id).value = num;
            }

            else {
                alert('Solo se permiten numeros');
                document.getElementById(sender.id).value = "";

            }
        }
      </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">
      <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Aceptar" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>


    <div class="contacto2" id="principal" runat="server">
        <h1>Crear Nueva Solicitud</h1>
        <div style="width: 100%">
            <table style="width: 100%">
                <tbody>
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="Número Aprobación"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtNumeroAprobacion" runat="server"></asp:TextBox>

                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="No. Radicación"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtRadicacion" runat="server" TextMode="MultiLine" Rows="2" Columns="30"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text="Línea Intervención" Width="100%"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlLineaIntervencion" runat="server"></asp:DropDownList>
                             <asp:RequiredFieldValidator id="RequiredFieldValidator6" runat="server"
                              ControlToValidate="ddlLineaIntervencion"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red" Width="100%">
                            </asp:RequiredFieldValidator>
                           <%-- <asp:TextBox ID="txtIntervencion" runat="server"></asp:TextBox>--%>

                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Nombre Funcionario"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtNombreFuncionario" runat="server" Width="100%"></asp:TextBox>
                            <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server"
                              ControlToValidate="txtNombreFuncionario"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red" Width="100%">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1">
                            <asp:Label ID="Label5" runat="server" Text="Cargo"></asp:Label>
                        </td>
                        <td colspan="1">
                            <asp:TextBox ID="txtCargo" runat="server" TextMode="MultiLine" Rows="5" Width="90%"></asp:TextBox>
                             <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server"
                              ControlToValidate="txtCargo"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red" Width="100%">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td></td>
                        <td colspan="1">
                            <asp:Label ID="Label12" runat="server" Text="Objeto"></asp:Label>
                        </td>
                         <td colspan="1">
                            <asp:TextBox ID="txtObjeto" runat="server" TextMode="MultiLine" Rows="5" Width="100%"></asp:TextBox>
                             <asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server"
                              ControlToValidate="txtObjeto"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red" Width="100%">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="1">
                            <asp:Label ID="Label14" runat="server" Text="Lugar del Viaje"></asp:Label>
                        </td>
                        <td colspan="4">
                            <asp:TextBox ID="txtLugarViaje" runat="server" TextMode="MultiLine" Rows="2" Width="100%"></asp:TextBox>
                             <asp:RequiredFieldValidator id="RequiredFieldValidator7" runat="server"
                              ControlToValidate="txtLugarViaje"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red" Width="100%">
                            </asp:RequiredFieldValidator>
                        </td>
                       
                    </tr>


                    <tr>
                        <td>
                            <asp:Label ID="Label6" runat="server" Text="Fecha Inicio"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaInicio" runat="server" Width="70%" OnTextChanged="txtFechaInicio_TextChanged" AutoPostBack="true"></asp:TextBox>
                            <asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server"
                              ControlToValidate="txtFechaInicio"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red" Width="100%">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="Label9" runat="server" Text="Fecha Fin"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaFin" runat="server" Width="100%" OnTextChanged="txtFechaFin_TextChanged" AutoPostBack="true"></asp:TextBox>
                            <asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" Width="100%"
                              ControlToValidate="txtFechaFin"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label8" runat="server" Text="Tickets Aéreos"></asp:Label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkTiqueteAereo" runat="server" OnCheckedChanged="chkTiqueteAereo_CheckedChanged" AutoPostBack="true"/>
                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="Label10" runat="server" Text="Ruta Aérea" Visible="false"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtRutaAerea" runat="server" TextMode="MultiLine" Width="100%" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label11" runat="server" Text="Porción Terrestre"></asp:Label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkPorcionTerrestre" runat="server" OnCheckedChanged="chkPorcionTerrestre_CheckedChanged"  AutoPostBack="true"/>
                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="lblRutaTerrestre" runat="server" Text="Ruta Terrestre" Visible="false"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtRutaTerrestre" runat="server" TextMode="MultiLine" Width="100%" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                     <tr>
                        <td>
                           
                        </td>
                        <td>
                           
                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="lblValorRutaT" runat="server" Text="Valor Ruta Terreste" Visible="false"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtValorRutaT" runat="server"  OnTextChanged="txtValorRutaT_TextChanged" ClientIDMode="Static" Visible="false" AutoPostBack="true"></asp:TextBox>
                            <asp:DropDownList ID="ddlMonedaValorRutaT" Enabled="false" Visible="false" runat="server">
                                <asp:ListItem Value="COP">COP</asp:ListItem>
                                <asp:ListItem Value="USD">USD</asp:ListItem>
                            </asp:DropDownList>
                              <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                ControlToValidate="txtValorRutaT" 
                                ErrorMessage="Solo numeros decimales separados por punto (.) o enteros" 
                                ForeColor="Red"
                                ValidationExpression="[0-9]*\.?[0-9]*" 
                                Width="100%">
                            </asp:RegularExpressionValidator>
                            <%--ValidationExpression="^(?:|\d+(\.\d{1,2})?)$" --%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label13" runat="server" Text="Viaticos"></asp:Label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkViaticos" runat="server" OnCheckedChanged="chkViaticos_CheckedChanged" AutoPostBack="true" />
                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="lblValorDiario" runat="server" Text="Valor Diario" Visible="false"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtValorDiario" runat="server" Visible="false" OnTextChanged="txtValorDiario_TextChanged" ClientIDMode="Static" AutoPostBack="true"></asp:TextBox>
                            <asp:DropDownList ID="ddlMonedaValor" runat="server" Visible="false" OnSelectedIndexChanged="ddlMonedaValor_SelectedIndexChanged" AutoPostBack="True">
                                <asp:ListItem Value="COP">COP</asp:ListItem>
                                <asp:ListItem Value="USD">USD</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                ControlToValidate="txtValorDiario" 
                                ErrorMessage="Solo numeros decimales separados por punto (.) o enteros" 
                                ForeColor="Red"
                                ValidationExpression="[0-9]*\.?[0-9]*" Width="100%">
                            </asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1">
                            <asp:Label ID="Label7" runat="server" Text="Número de Días"></asp:Label>
                        </td>
                        <td colspan="1">
                            <asp:TextBox ID="txtNumeroDias" runat="server" OnTextChanged="txtNumeroDias_TextChanged" AutoPostBack="true" Enabled="false"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                ControlToValidate="txtNumeroDias" 
                                ErrorMessage="Solo Numeros decimales separados por punto (.) o enteros" 
                                    ForeColor="Red"
                                ValidationExpression="^(?:|\d+(\.\d{1,2})?)$" Width="100%">
                            </asp:RegularExpressionValidator>
                        </td>
                        <td></td>
                        <td colspan="1">
                            <asp:Label ID="Label15" runat="server" Text="Total viaticos"></asp:Label>
                        </td>
                        <td colspan="1">
                            <asp:TextBox ID="txtTotalViaticos" runat="server"></asp:TextBox>
                            <asp:DropDownList ID="ddlValorTotal" Enabled="false" runat="server" OnSelectedIndexChanged="ddlValorTotal_SelectedIndexChanged">
                                <asp:ListItem Value="COP">COP</asp:ListItem>
                                <asp:ListItem Value="USD">USD</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                    <tr>
                        <td colspan="1"></td>
                        <td colspan="1">
                            <asp:Button ID="btnGuardarCont" runat="server" Text="Guardar y Continuar" OnClick="btnGuardarCont_Click" />
                        </td>
                        <td colspan="1"></td>
                        <td colspan="1">
                            <%--<asp:Button ID="btnGuardarSalir" runat="server" Text="Guardar y Salir" OnClick="btnGuardarSalir_Click" />--%>
                        </td>
                         <td colspan="1">
                             <asp:Button ID="btnGuardarEditar" runat="server" Text="guardar Cambios" Visible="false"  OnClick="btnGuardarEditar_Click" />
                          </td>

                        <td>
                          <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
                          <%--  <asp:Button ID="btnCancelar" UseSubmitBehavior="false" runat="server" Text="Cancelar"  OnClick="btnCancelar_Click"/>--%>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="Server">
     <script type="text/javascript">
         function newDoc() {
             window.location.assign("frmWFSolicitudComision.aspx")
         }

        $(document).ready(function () { 
            $('#' + '<%= txtFechaInicio.ClientID %>').datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
            $('#' + '<%= txtFechaFin.ClientID %>').datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
        });
    </script>

</asp:Content>

