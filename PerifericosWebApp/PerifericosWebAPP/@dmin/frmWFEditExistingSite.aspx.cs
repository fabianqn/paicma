﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmWFEditExistingSite : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                Int64 idSitio = Convert.ToInt64(Session["intIdSitio"]);
                Int64 idSolcitud = Convert.ToInt64(Session["IdSolicitudRef"]);
                ddlistDepartamento.DataSource = fillDepartamentos();
                ddlistDepartamento.DataTextField = "Text";
                ddlistDepartamento.DataValueField = "Value";
                ddlistDepartamento.DataBind();

                ddlistMunicipio.DataSource = fillMunicipios();
                ddlistMunicipio.DataTextField = "Text";
                ddlistMunicipio.DataValueField = "Value";
                ddlistMunicipio.DataBind();
                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    if (blPaicma.fntConsultarSitios("datosSitiosSolicitud", idSolcitud, "id", idSitio))
                    {

                        if (blPaicma.myDataSet.Tables["datosSitiosSolicitud"].Rows.Count > 0)
                        {

                            string valorDepartamento = blPaicma.myDataSet.Tables["datosSitiosSolicitud"].Rows[0]["idDepartamento"].ToString();
                           // ddlistDepartamento.SelectedValue = blPaicma.myDataSet.Tables["datosSitiosSolicitud"].Rows[0]["idDepartamento"].ToString();
                           
                            string valorMunicipio = blPaicma.myDataSet.Tables["datosSitiosSolicitud"].Rows[0]["idMunicipio"].ToString();
                            // ddlistMunicipio.SelectedValue = blPaicma.myDataSet.Tables["datosSitiosSolicitud"].Rows[0]["idMunicipio"].ToString();

                          
                          //  ddlistMunicipio.SelectedValue = blPaicma.myDataSet.Tables["datosSitiosSolicitud"].Rows[0]["idMunicipio"].ToString();
                            txtUbicacion.Text = blPaicma.myDataSet.Tables["datosSitiosSolicitud"].Rows[0]["ubicacion"].ToString();
                            //ddlistIndicador.SelectedIndex = Convert.ToInt32(blPaicma.myDataSet.Tables["datosPlanesAccion"].Rows[0]["idIndicador"]);
                            LLenarDropMunicipio(valorDepartamento, valorMunicipio);
                        }
                        else
                        {
                            //no hay registros
                        }
                    }
                    else
                    {
                        //No pudo consultar los sitios
                    }
                }
                else
                {
                    //No pudo conectar con la base de datos.
                }
                this.blPaicma.Termina();
            }
        }

    }

    protected List<ListItem> fillDepartamentos()
    {
        List<ListItem> resultado = new List<ListItem>();

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarDepartamentos("strDepartamentosConsultar"))
            {
                DataTable dataSourceToDropDown = new DataTable();
                dataSourceToDropDown = blPaicma.myDataSet.Tables["strDepartamentosConsultar"];

                foreach (DataRow row in dataSourceToDropDown.Rows)
                {
                    string valor = row[0].ToString();
                    string texto = row[1].ToString();
                    ListItem valorCombo1 = new ListItem();
                    valorCombo1.Value = valor;
                    valorCombo1.Text = texto;
                    resultado.Add(valorCombo1);
                }


            }
        }
        this.blPaicma.Termina();
        return resultado;
    }
    protected List<ListItem> fillMunicipios()
    {
        List<ListItem> resultado = new List<ListItem>();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarMunicipios("strMunicipiosConsultar"))
            {
                DataTable dataSourceToDropDown = new DataTable();
                dataSourceToDropDown = blPaicma.myDataSet.Tables["strMunicipiosConsultar"];

                foreach (DataRow row in dataSourceToDropDown.Rows)
                {
                    string valor = row[0].ToString();
                    string texto = row[1].ToString();
                    string departamento = row[2].ToString();
                    ListItem valorCombo2 = new ListItem();
                    valorCombo2.Value = valor;
                    valorCombo2.Text = texto;
                    valorCombo2.Attributes["data-departamento"] = departamento;
                    resultado.Add(valorCombo2);
                }
            }
        }
        this.blPaicma.Termina();
        return resultado;
    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntModificarExistingSite(Convert.ToInt64(Session["intIdSitio"]), Convert.ToInt64(Session["IdSolicitudRef"]), ddlistDepartamento.SelectedValue.ToString(), ddlistMunicipio.SelectedValue.ToString(), txtUbicacion.Text, DateTime.Now, Convert.ToInt32(Session["IdUsuario"])))
            {
                Response.Redirect("frmWFSitiosIntervencion.aspx");
                
            }
            else
            {
                //No pudo actualizar
            }
        }
        else
        {
            //no pudo conectar con la base de datos.
        }
        this.blPaicma.Termina();
    }


    private void LLenarDropMunicipio(string idDepartamento , string municipioselect)
    {
        DataTable mmunicipio = new DataTable();

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {

            if (blPaicma.fntConsultarMunicipiosporDepartamento("strMunicipiosConsultar", idDepartamento))
            {
                mmunicipio = blPaicma.myDataSet.Tables["strMunicipiosConsultar"];
                ddlistMunicipio.DataSource = mmunicipio;
                ddlistMunicipio.DataTextField = "nomMunicipio";
                ddlistMunicipio.DataValueField = "codMunicipioAlf5";
                ddlistMunicipio.DataBind();

            }
            else
            {
                //No pudo actualizar
            }
        }
        else
        {
            //no pudo conectar con la base de datos.
        }
        this.blPaicma.Termina();
  

        if (municipioselect == "0")
        {
            ddlistMunicipio.Items[0].Selected = true;
           // ddlistMunicipio.SelectedValue = ddlistMunicipio.Items[0].ToString();
        }
        else
        {
            ddlistMunicipio.SelectedValue = municipioselect;
        }
        ddlistDepartamento.SelectedValue = idDepartamento;
    }


    /// <summary>
    /// ☻☻ evento para cambiar los datos del departamento 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlistDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlistDepartamento.SelectedValue !=string.Empty)
        {

            LLenarDropMunicipio(ddlistDepartamento.SelectedValue, "0");
        }
        else
        {
            ddlistDepartamento.Items.Clear();
            ddlistDepartamento.Items.Insert(ddlistDepartamento.Attributes.Count, new ListItem("Seleccione...", "0"));
        }

    }
}
