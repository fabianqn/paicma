﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmReportList : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }

            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                this.fntCargarGrillaReportes();
                return;
            }
            base.Response.Redirect("@dmin/frmLogin.aspx");
        }
    }


    public void fntCargarGrillaReportes()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaReporte("strReportesListados", 0, 0, 0))
            {

                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gridReportes.DataMember = "strReportesListados";
                    this.gridReportes.DataSource = this.blPaicma.myDataSet;
                    this.gridReportes.DataBind();
                    //  this.lblErrorGv.Text = string.Empty;
                }
                else
                {
                    this.gridReportes.DataMember = "strReportesListados";
                    this.gridReportes.DataSource = this.blPaicma.myDataSet;
                    this.gridReportes.DataBind();
                    //this.lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                //this.gvBibliotecasAsignadas.Columns[0].Visible = false;
                //this.gvBibliotecasAsignadas.Columns[1].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    protected void gridReportes_PreRender(object sender, EventArgs e)
    {
        if (gridReportes.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gridReportes.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gridReportes.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gridReportes.FooterRow.TableSection = TableRowSection.TableFooter;
        }

    }
    protected void gridReportes_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int intIndex = Convert.ToInt32(e.CommandArgument);
        GridViewRow selectedRow = gridReportes.Rows[intIndex];
        TableCell Item = selectedRow.Cells[0];
        TableCell Item1 = selectedRow.Cells[1];
        int intIdTabla = Convert.ToInt32(Item.Text);
        string NombreReporte = Item1.Text.Trim();
        Session["idReporteBuscar"] = Convert.ToInt32(intIdTabla);
        Session["NombreReporteSeleccionado"] = NombreReporte;

        if (e.CommandName == "verReporte")
        {
            string strRuta = "/@web/GenericReport.aspx";
            Response.Redirect(strRuta);
        }
        if (e.CommandName == "editarReporte")
        {
            string strRuta = "frmModifyReport.aspx";
            Response.Redirect(strRuta);

        }
        if (e.CommandName == "permisosReporte")
        {
            string strRuta = "frmPermisosUsuarioReportes.aspx";
            Response.Redirect(strRuta);

        }


    }
    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        string strRuta = "frmUploadReport.aspx";
        Response.Redirect(strRuta);
    }
}