﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmLogin : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender,
                             EventArgs e)
    {
        if (!(Page.IsPostBack)) {
            fntLimpiarControles();
            Session.Clear();
        }
    }

    protected void Page_Unload(object sender,
                               EventArgs e)
    {
        blPaicma = null;
    }

    public void fntLimpiarControles()
    {
        //txtClave.Text = string.Empty;
        //txtUsuario.Text = string.Empty;
    }

    public void fntValidarUsuario(string strUsuario,
                                  string strClave)
    {
        string strEstado = string.Empty;
        string strTipoConexion = string.Empty;
        string strClaveAux = string.Empty;
        string strContrasenaEncriptada;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad)) {
            if (blPaicma.fntConsultaUsuario_bol("strDsUsuario", strUsuario)) {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0) {
                    strEstado = blPaicma.myDataSet.Tables[0].Rows[0]["est_Descripcion"].ToString();
                    strTipoConexion = blPaicma.myDataSet.Tables[0].Rows[0]["segCon_Descripcion"].ToString();
                    strUsuario = blPaicma.myDataSet.Tables[0].Rows[0]["segusu_login"].ToString();
                    if (strEstado == "ACTIVO") {
                        Session["IdUsuario"] = blPaicma.myDataSet.Tables[0].Rows[0]["segusu_Id"].ToString();
                        if (strTipoConexion == "Local") {
                            strClaveAux = blPaicma.myDataSet.Tables[0].Rows[0]["segusu_Password"].ToString();
                            ////proceso de encriptar clave
                            strContrasenaEncriptada = fntEncriptaPassword(strClave);
                            ////Fin proceso de encriptar clave
                            if (strClaveAux == strContrasenaEncriptada) {
                                ///usuario autenticado correctamente
                                //Session["IdUsuario"] = blPaicma.myDataSet.Tables[0].Rows[0]["segusu_Id"].ToString();
                                Session["strUsuario"] = strUsuario;
                                Response.Redirect("../Default.aspx");
                            }
                            else {
                                ////contraseñas no son iguales
                                //lblError.Text = "Contraseña incorrecta, por favor verificar";
                                Session["IdUsuario"] = null;
                                // lblError.Text = "Datos incorrectos, por favor verificarlos.";
                            }
                        }
                        else {
                            ////usuario existente y se conecta por directorio activo
                            if (blPaicma.fntConsultaDominioConexion_bol("dsConexion")) {
                                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0) {
                                    string strDominio = blPaicma.myDataSet.Tables[0].Rows[0]["cnxDominio"].ToString();
                                    ;
                                    if (fntValidarUsuarioDirectorioActivo(strDominio)) {
                                        Session["strUsuario"] = strUsuario;
                                        Response.Redirect("../Default.aspx");
                                    }
                                    else {
                                        //  lblError.Text = "Datos incorrectos, por favor verificarlos.";
                                        Session["strUsuario"] = null;
                                        Session["IdUsuario"] = null;
                                    }
                                }
                            }
                        }
                    }
                    else {
                        ////usuario existente pero inactivo estado = INACTIVO
                        //lblError.Text = "Usuario inactivo, por favor comunicarse con el administrador";
                        //  lblError.Text = "Datos incorrectos, por favor verificarlos.";
                    }
                }
                else {
                    ////usuario no existe;
                    //lblError.Text = "Usuario no existe, por favor verificar los datos";
                    // lblError.Text = "Datos incorrectos, por favor verificarlos.";
                }
            }
            blPaicma.Termina();
        }
    }

    private string fntEncriptaPassword(string strPassword)
    {
        string strPasswordEncritpado = "";
        char caracter;
        int m;
        char aux1;
        int intAux1;
        char aux2;
        int intAux2;
        int numero;
        if (strPassword.Length <= 0) {
            //MsgBox("No hay datos para encriptar")
        }
        else {
            m = Convert.ToInt32(strPassword.Length);
            for (int i = 0; i < m; i++) {
                aux1 = strPassword[i];
                intAux1 = (int)aux1;
                aux2 = strPassword[i];
                intAux2 = (int)aux2;
                intAux2 = intAux2 + (i * 2);
                numero = Convert.ToInt32(intAux1) + Convert.ToInt32(intAux2);
                caracter = Convert.ToChar(numero);
                strPasswordEncritpado = strPasswordEncritpado + caracter;
            }
        }
        return strPasswordEncritpado;
    }

    public Boolean fntValidarUsuarioDirectorioActivo(string strDominio)
    {
        //Boolean bolValor = false;
        //string strLogin = txtUsuario.Text.Trim();
        //string strPassword = txtClave.Text.Trim();
        //if (blPaicma.PerteneceDominio(strDominio, strLogin, strPassword))
        //{
        //    bolValor = true;
        //}
        //else
        //{
        //    bolValor = false;
        //}
        return true;
    }

    protected void btnIngresar_Click(object sender,
                                     EventArgs e)
    {
        string strNombre = string.Empty;
        string strClave = string.Empty;
        string strSW = string.Empty;
        strNombre = txtUsuario.Text.Trim();
        strClave = txtClave.Text.Trim();
        if (fntValidarCampos(strNombre, strClave))
        {
            strSW = "OK";
            fntValidarUsuario(strNombre, strClave);
        }
        else
        {
            strSW = "NO";
        }
    }

    public Boolean fntValidarCampos(string strUsuario,
                                    string strClave)
    {
        if (strUsuario == string.Empty)
        {
            lblError.Text = "Debe digitar el usuario";
            return false;
        }
        else
        {
            if (strClave == string.Empty)
            {
                lblError.Text = "Debe digitar la clave";
                return false;
            }
        }
        lblError.Text = string.Empty;
        return true;
    }

    protected void CtrLogin1_IniciarSesion(object sender,
                                           EventArgs e)
    {
        //string strEstado = string.Empty;
        //string strTipoConexion = string.Empty;
        ////string strClaveAux = string.Empty;
        ////string strContrasenaEncriptada;
        //if (blPaicma.inicializar(blPaicma.conexionSeguridad)) {
        //    if (blPaicma.fntConsultaUsuario_bol("strDsUsuario", CtrLogin1.Usuario)) {
        //        if (blPaicma.myDataSet.Tables[0].Rows.Count > 0) {
        //            strEstado = blPaicma.myDataSet.Tables[0].Rows[0]["est_Descripcion"].ToString();
        //            strTipoConexion = blPaicma.myDataSet.Tables[0].Rows[0]["segCon_Descripcion"].ToString();
        //            // strUsuario = blPaicma.myDataSet.Tables[0].Rows[0]["segusu_login"].ToString();
        //            if (strEstado == "ACTIVO") {
        //                Session["IdUsuario"] = blPaicma.myDataSet.Tables[0].Rows[0]["segusu_Id"].ToString();
        //                Session["strUsuario"] = CtrLogin1.Nombre;
        //                Response.Redirect("../Default.aspx", false);
        //                //if (strTipoConexion == "Local")
        //                //{
        //                //    strClaveAux = blPaicma.myDataSet.Tables[0].Rows[0]["segusu_Password"].ToString();
        //                //    ////proceso de encriptar clave
        //                //    strContrasenaEncriptada = fntEncriptaPassword(strClave);
        //                //    ////Fin proceso de encriptar clave
        //                //    if (strClaveAux == strContrasenaEncriptada)
        //                //    {
        //                //        ///usuario autenticado correctamente
        //                //        //Session["IdUsuario"] = blPaicma.myDataSet.Tables[0].Rows[0]["segusu_Id"].ToString();
        //                //        Session["strUsuario"] = strUsuario;
        //                //        Response.Redirect("../Default.aspx");
        //                //    }
        //                //    else
        //                //    {
        //                //        ////contraseñas no son iguales
        //                //        //lblError.Text = "Contraseña incorrecta, por favor verificar";
        //                //        Session["IdUsuario"] = null;
        //                //        // lblError.Text = "Datos incorrectos, por favor verificarlos.";
        //                //    }
        //                //}
        //                //else
        //                //{
        //                //    ////usuario existente y se conecta por directorio activo
        //                //    if (blPaicma.fntConsultaDominioConexion_bol("dsConexion"))
        //                //    {
        //                //        if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
        //                //        {
        //                //            string strDominio = blPaicma.myDataSet.Tables[0].Rows[0]["cnxDominio"].ToString(); ;
        //                //            if (fntValidarUsuarioDirectorioActivo(strDominio))
        //                //            {
        //                //                Session["strUsuario"] = strUsuario;
        //                //                Response.Redirect("../Default.aspx");
        //                //            }
        //                //            else
        //                //            {
        //                //                //  lblError.Text = "Datos incorrectos, por favor verificarlos.";
        //                //                Session["strUsuario"] = null;
        //                //                Session["IdUsuario"] = null;
        //                //            }
        //                //        }
        //                //    }
        //                //}
        //            }
        //            else {
        //                ////usuario existente pero inactivo estado = INACTIVO
        //                //lblError.Text = "Usuario inactivo, por favor comunicarse con el administrador";
        //                //  lblError.Text = "Datos incorrectos, por favor verificarlos.";
        //            }
        //        }
        //        else {
        //            ////usuario no existe;
        //            //lblError.Text = "Usuario no existe, por favor verificar los datos";
        //            // lblError.Text = "Datos incorrectos, por favor verificarlos.";
        //        }
        //    }
        //    blPaicma.Termina();
        //}
    }
}