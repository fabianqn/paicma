﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class _dmin_frmWFPermitirEditarSolicitud : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    private EstadosSolicitud Estados = new EstadosSolicitud();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                this.fullSolicitud();
            }


        }
    }
    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        string Filtro = "";
        string Parametro = "";

        string caseSwitch = ddlOpcionesFiltro.SelectedValue;

        if (caseSwitch != "0")
        {
            if (txtBuscar.Text.Trim() != string.Empty)
            {
                switch (caseSwitch)
                {
                    case "1":
                        Filtro = "idSolicitud";
                        break;
                    case "2":
                        Filtro = "IdentificadorSolicitud";
                        break;
                    default:
                        Filtro = "idSolicitud";
                        break;
                }
                Parametro = txtBuscar.Text.Trim();
            }
            else
            {
                LblError.Text = "Escriba el el valor a buscar";
                LblError.Visible = true;
            }
        }
        else
        {
            LblError.Text = "Para Buscar Seleccione un Filtro";
            LblError.Visible = true;
        }
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            int usuarioCreo = Convert.ToInt32(Session["IdUsuario"]);

            if (blPaicma.fntConsultarSolicitudEstado("dataRespuesta", "id", Convert.ToInt64(txtBuscar.Text.Trim())))
            {
                DataTable Listado = new DataTable();
                Listado = blPaicma.myDataSet.Tables["dataRespuesta"];
                gvSolicitud.DataSource = Listado;
                gvSolicitud.DataBind();
            }
            else
            {
                //no pudo consultar solicitud
            }
        }
        else
        {
            //no pudo inciar base de datos
        }
        this.blPaicma.Termina();
    }
    protected void gvSolicitud_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "permiteModificar")
        {
            int intIndex = Convert.ToInt32(e.CommandArgument);

            Guid IdUnico = Guid.NewGuid();

            GridViewRow selectedRow = gvSolicitud.Rows[intIndex];
            TableCell Item1 = selectedRow.Cells[0];
            long intIdSolicitud = Convert.ToInt64(Item1.Text);
            ViewState["IdSolicitudEfditar"] = intIdSolicitud;

            lblIdSolicitudPermiteEdit.Text = intIdSolicitud.ToString();

            pnlPermiteEditarSolicitud.Visible = true;

            ///// buscar los datos de la solicitusd  para guardarlos en la otra tabla 
            //if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            //{
            //    int usuarioCreo = Convert.ToInt32(Session["IdUsuario"]);

            //    if (blPaicma.fntConsultarSolicitudEstado("solicitudporid", "id", intIdSolicitud))
            //    {
            //        DataTable Listado = new DataTable();
            //        Listado = blPaicma.myDataSet.Tables["solicitudporid"];

            //        /// ☻☻ llamar el metodo que guarda los valores cuando se habilita  una solicitud 

          
            //    }
            //    else
            //    {
            //        //no pudo consultar solicitud
            //    }
            //}
            //else
            //{
            //    //no pudo inciar base de datos
            //}
            //this.blPaicma.Termina(); 





            //if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            //{
            //    if (blPaicma.fntIngresarLegalizacion_bol(intIdSolicitud, DateTime.Now, 0, EstadosSolicitud.LegIncompletaPlanes, DateTime.Now, IdUnico.ToString()))
            //    {
            //        if (blPaicma.fntIngresarEstadoSolicitud_bol(intIdSolicitud, EstadosSolicitud.LegIncompletaPlanes, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
            //        {
            //            if (blPaicma.fntActualizarEstado_bol(intIdSolicitud, "idSolicitud", EstadosSolicitud.LegIncompletaPlanes, "WFSol_SolicitudComision", "idEstadoActual"))
            //            {
            //                Session["vieneDeLegalizar"] = "verdad";
            //                Response.Redirect("frmWFDetalleSolicitud.aspx");
            //                //Actgualizado Correcto
            //            }
            //            else
            //            {
            //                //Fallo actualizar estado en la comision
            //            }

            //        }
            //        else
            //        {
            //            //fallo actualizar estado Solicitud
            //        }
            //    }
            //    else
            //    {
            //        //Fallo al registrar Legalizacion
            //    }
            //}


        }
    }


    //☻☻ metodo  para cargar 
    private void fullSolicitud()
    {
        
     if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarSolicitudEstadoPermitir("dataRespuesta"))
            {
                DataTable Listado = new DataTable();
                Listado = blPaicma.myDataSet.Tables["dataRespuesta"];
                gvSolicitud.DataSource = Listado;
                gvSolicitud.DataBind();
            }
            else
            {
                //no pudo consultar solicitud
            }
        }
        else
        {
            //no pudo inciar base de datos
        }
        this.blPaicma.Termina();
    }

    //☻☻ evento para guardar los cambios  soplicitud permitida 
    protected void btnGuardar_Click(object sender, EventArgs e)
    {

        long intIdSolicitud = Convert.ToInt64(ViewState["IdSolicitudEfditar"]);

        /// buscar los datos de la solicitusd  para guardarlos en la otra tabla 
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            int usuarioAdmin = Convert.ToInt32(Session["IdUsuario"]);

            if (blPaicma.fntConsultarSolicitudEstado("solicitudporid", "id", intIdSolicitud))
            {
                DataTable Listado = new DataTable();
                Listado = blPaicma.myDataSet.Tables["solicitudporid"];

                //☻☻ llamar el metodo que guarda los valores cuando se habilita  una solicitud 
                int idUsuarioEditar =Convert.ToInt32(Listado.Rows[0]["idUsuarioCreacion"]);

                 //☻☻ metodo quew guarda en  la nueva tabla cuando la solicitud se puede editar 
                if (blPaicma.fntInsertarPermiteEditarSolicitud_bol(intIdSolicitud, usuarioAdmin, idUsuarioEditar ,txtJustificacion.Text ))
                {

                    //☻☻  metodo  para editar el estado de la solicitud 
                    if (blPaicma.fntActualizarPermiteEditarSolicitud_bol(intIdSolicitud, true))
                        {
                    
                        // validar si  el historico de la solicitud se guarda ak
                            LimpiarControles();
                        }
                    else
                        {

                        }
                }
                else
                {

                }

            }
            else
            {
                //no pudo consultar solicitud
            }
        }
        else
        {
            //no pudo inciar base de datos
        }
        this.blPaicma.Termina(); 

    }


    //☻☻ evento para ocultar el panel y limpiar los controles 
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        LimpiarControles();
    }
    //☻☻ metodo para  limpiar los controles 
    private void LimpiarControles()
    {
        lblTextoEditar.Text = string.Empty;
        txtJustificacion.Text = string.Empty;
        pnlPermiteEditarSolicitud.Visible = false;
    }



}