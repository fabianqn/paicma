﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/mpLogin.master" AutoEventWireup="true" Inherits="_dmin_frmLogin" Codebehind="frmLogin.aspx.cs" %>

<%@ Register Assembly="Presidencia.Controles" Namespace="Presidencia.Controles" TagPrefix="pre" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">
    <%--<pre:CtrLogin ID="CtrLogin1" runat="server" OnIniciarSesion="CtrLogin1_IniciarSesion" Dominio="pubpresiden" UsarValidacionCAPTCHA="true" ></pre:CtrLogin>--%>

        <asp:Panel ID="pnlLogin" runat="server">
        <div class="formularioint3">        
            <table class="contactoLogin">
                <tr>
                    <td colspan="2">
                        INICIO DE SESIÓN</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblUsuario" runat="server" Text="Usuario"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtUsuario" runat="server" MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblClave" runat="server" Text="Clave"></asp:Label>
                    </td>
                    <td style="text-align: left">
                        <asp:TextBox ID="txtClave" runat="server" TextMode="Password" MaxLength="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" 
                            onclick="btnIngresar_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <p>
        <br />
    </p>
</asp:Content>

