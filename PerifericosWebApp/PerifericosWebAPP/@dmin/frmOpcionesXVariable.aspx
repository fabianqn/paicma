﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" Inherits="_dmin_frmOpcionesXVariable" Codebehind="frmOpcionesXVariable.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .contacto input[type='text'], .contacto textarea, .contacto select{
            text-transform: none !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <asp:Panel ID="pnlTitulo" runat="server" Width="90%" >
        <div align="left">
            <asp:Label ID="lblTitulo" runat="server" Text="ADMINISTRACIÓN DE OPCIONES X VARIABLE"></asp:Label>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlEliminacion" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblEliminacion" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSi" runat="server"  Text="SI" onclick="btnSi_Click" />
                        <asp:Button ID="btnNo" runat="server"  Text="NO" onclick="btnNo_Click" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" Text="Aceptar" onclick="btnOk_Click" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlMenuVariable" runat="server">
        <div class="Botones">
            <asp:ImageButton ID="imgbBuscar" runat="server" 
                ImageUrl="~/Images/BuscarAccion.png" ToolTip="Buscar" 
                onclick="imgbBuscar_Click" />
            <asp:ImageButton ID="imgbNuevo" runat="server"  ImageUrl="~/Images/Nuevo.png" 
                ToolTip="Nuevo" onclick="imgbNuevo_Click" />
            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png" 
                Visible="False" ToolTip="Editar" onclick="imgbEditar_Click" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAccionesVariable" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server" 
                ImageUrl="~/Images/buscar.png" ToolTip="Ejecutar Busqueda" 
                onclick="imgbEncontrar_Click" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png" 
                ToolTip="Guardar" onclick="imgbGravar_Click" />
            <asp:ImageButton ID="imgbCancelar" runat="server" 
                ImageUrl="~/Images/cancelar.png" ToolTip="Cancelar" 
                onclick="imgbCancelar_Click" />
            <asp:ImageButton ID="imgbEliminar" runat="server" 
                ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar" 
                onclick="imgbEliminar_Click" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlVariableNuevo" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto" >

            <tr>
                <td>
                    <asp:Label ID="lblidVariable" runat="server" Text="Variable"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlidVariable" runat="server" AutoPostBack="False">
                    </asp:DropDownList>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblnombreVariable" runat="server" Text="Identificador Valor"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtidentificadorValor" runat="server" MaxLength="20"></asp:TextBox>
                     <asp:RegularExpressionValidator ID="regexFortxtNoIdentificacion" runat="server"
                            ControlToValidate="txtidentificadorValor" ErrorMessage="*Ingrese Valores Numericos"
                            ForeColor="Red"
                            ValidationExpression="^[0-9]*">
                    </asp:RegularExpressionValidator>
                    <asp:HiddenField ID="hndidValorXVariable" runat="server" ></asp:HiddenField>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbltextoValor" runat="server" Text="Texto Valor"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txttextoValor" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="PnlVariableGrilla" runat="server">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvVariable" runat="server" AutoGenerateColumns="False" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
                    onrowcommand="gvVariable_RowCommand" PageSize="30" OnPageIndexChanging="gvVariable_PageIndexChanging">
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="idValorXVariable" HeaderText="Id ValorXVariable" />
                        <asp:BoundField DataField="nombreVariable" HeaderText="Variable" />
                        <asp:BoundField DataField="identificadorValor" HeaderText="identificadorValor" />
                        <asp:BoundField DataField="textoValor" HeaderText="textoValor" />
                        <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                            ImageUrl="~/Images/seleccionar.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorGv" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>


</asp:Content>

