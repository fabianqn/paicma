﻿//using ASP;
using System;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmCorreo : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Unload(object sender, EventArgs e)
    {
        this.blPaicma = null;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }
            this.ftnValidarPermisos();
            this.fntCargarGrillaCorreo();
        }
    }
    public void ftnValidarPermisos()
    {
        string arg_05_0 = string.Empty;
        string a = string.Empty;
        string a2 = string.Empty;
        string arg_17_0 = string.Empty;
        string[] array = HttpContext.Current.Request.RawUrl.Split(new char[]
		{
			'/'
		});
        string text = array[array.GetUpperBound(0)];
        array = text.Split(new char[]
		{
			'?'
		});
        text = array[array.GetLowerBound(0)];
        if (text != string.Empty)
        {
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (this.blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(this.Session["IdUsuario"].ToString()), text) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                    a = this.blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                    a2 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                    this.blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();
                    if (a == "False")
                    {
                        this.imgbNuevo.Visible = false;
                    }
                    if (a2 == "False")
                    {
                        this.imgbEditar.Visible = false;
                        this.imgbGravar.Visible = false;
                    }
                }
                this.blPaicma.Termina();
                return;
            }
        }
        else
        {
            base.Response.Redirect("@dmin/frmCorreo.aspx");
        }
    }
    public void fntInactivarPaneles(bool bolValor)
    {
        this.pnlAccionesCorreo.Visible = bolValor;
        this.pnlEliminacion.Visible = bolValor;
        this.PnlCorreoGrilla.Visible = bolValor;
        this.pnlCorreoNuevo.Visible = bolValor;
        this.pnlMenuCorreo.Visible = bolValor;
        this.pnlRespuesta.Visible = bolValor;
    }
    public void fntCargarGrillaCorreo()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaCorreo("strDsGrillaCorreo", 0))
            {
                this.gvCorreo.Columns[0].Visible = true;
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gvCorreo.DataMember = "strDsGrillaCorreo";
                    this.gvCorreo.DataSource = this.blPaicma.myDataSet;
                    this.gvCorreo.DataBind();
                    this.lblErrorGv.Text = string.Empty;
                }
                else
                {
                    this.gvCorreo.DataMember = "strDsGrillaCorreo";
                    this.gvCorreo.DataSource = this.blPaicma.myDataSet;
                    this.gvCorreo.DataBind();
                    this.lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                this.gvCorreo.Columns[0].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlAccionesCorreo.Visible = true;
        this.imgbGravar.Visible = true;
        this.imgbCancelar.Visible = true;
        this.pnlCorreoNuevo.Visible = true;
        this.Session["Operacion"] = "Crear";
    }
    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        string url = "frmCorreo.aspx";
        base.Response.Redirect(url);
    }
    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        if (this.Session["Operacion"].ToString() != "Crear")
        {
            if (this.Session["Operacion"].ToString() == "Actualizar" && this.ftnValidarCampos_bol() && this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                int intIdEstado;
                if (this.ddlEstado.SelectedValue == "ACTIVO")
                {
                    intIdEstado = 1;
                }
                else
                {
                    intIdEstado = 0;
                }
                if (this.blPaicma.fntModificarCorreo_bol(Convert.ToInt32(this.Session["intIdCorreo"].ToString()), this.txtServidor.Text.Trim(), Convert.ToInt32(this.txtPuerto.Text.Trim()), this.txtUsuario.Text.Trim(), this.txtContraseña.Text.Trim(), this.txtNombre.Text.Trim(), intIdEstado))
                {
                    this.fntInactivarPaneles(false);
                    this.pnlRespuesta.Visible = true;
                    this.lblRespuesta.Text = "Registro actualizado satisfactoriamente";
                }
                else
                {
                    this.fntInactivarPaneles(false);
                    this.pnlRespuesta.Visible = true;
                    this.lblRespuesta.Text = "Problema al actualizar el registro, por favor comuníquese con el administrador.";
                }
                this.blPaicma.Termina();
            }
            return;
        }
        if (!this.ftnValidarCampos_bol())
        {
            this.lblError.Visible = true;
            return;
        }
        if (this.ftnIngresarCorreo())
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.fntInactivarPaneles(false);
            this.pnlRespuesta.Visible = true;
            this.lblRespuesta.Text = "Correo creado satisfactoriamente";
            return;
        }
        this.lblError.Visible = true;
    }
    public bool ftnValidarCampos_bol()
    {
        if (this.txtServidor.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar el nombre o IP del Servidor de Correo Electronico.";
            return false;
        }
        if (this.txtPuerto.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar el puerto del servico SMTP.";
            return false;
        }
        if (this.txtUsuario.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar la cuenta de correo electronico que enviara los correos.";
            return false;
        }
        if (this.txtContraseña.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar la contraseña del correo electronico.";
            return false;
        }
        if (this.txtNombre.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar el nombre con el cual quiere se se vea el correo";
            return false;
        }
        if (this.ddlEstado.SelectedValue.ToString() == "Seleccione...")
        {
            this.lblError.Text = "Debe Seleccionar un estado";
            return false;
        }
        return true;
    }
    public bool ftnIngresarCorreo()
    {
        bool result = false;
        int intEstado;
        if (this.ddlEstado.SelectedValue == "ACTIVO")
        {
            intEstado = 1;
        }
        else
        {
            intEstado = 0;
        }
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntIngresarCorreo_bol(this.txtServidor.Text.Trim(), Convert.ToInt32(this.txtPuerto.Text.Trim()), this.txtUsuario.Text.Trim(), this.txtContraseña.Text.Trim(), this.txtNombre.Text.Trim(), intEstado))
            {
                result = true;
            }
            else
            {
                result = false;
                this.lblError.Text = "Problema al ingresar el registro, por favor comuníquese con el administrador.";
            }
            this.blPaicma.Termina();
        }
        return result;
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        string url = "frmCorreo.aspx";
        base.Response.Redirect(url);
    }
    protected void gvCorreo_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            this.gvCorreo.Columns[0].Visible = true;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow = this.gvCorreo.Rows[index];
            TableCell tableCell = gridViewRow.Cells[0];
            int num = Convert.ToInt32(tableCell.Text);
            this.Session["intIdCorreo"] = num;
            this.imgbEditar.Visible = true;
            this.gvCorreo.Columns[0].Visible = false;
        }
    }
    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlAccionesCorreo.Visible = true;
        this.imgbGravar.Visible = true;
        this.imgbCancelar.Visible = true;
        this.pnlCorreoNuevo.Visible = true;
        this.Session["Operacion"] = "Actualizar";
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            Convert.ToInt32(this.Session["intIdCorreo"].ToString());
            if (this.blPaicma.fntConsultaCorreo("strDsCorreo", Convert.ToInt32(this.Session["intIdCorreo"].ToString())) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.txtServidor.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                this.txtPuerto.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                this.txtUsuario.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                this.txtContraseña.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                this.txtNombre.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                int num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][6].ToString());
                if (num == 1)
                {
                    this.ddlEstado.SelectedValue = "ACTIVO";
                }
                else
                {
                    this.ddlEstado.SelectedValue = "INACTIVO";
                }
                this.ftnValidarPermisos();
            }
            this.blPaicma.Termina();
        }
    }
    protected void btnTest_Click(object sender, EventArgs e)
    {
        string strTitulo = "Test Envio de Correo";
        string strTextoMensaje = "Prueba de correo de sincronización";
        if (this.ftnEnviarCorreo(strTitulo, strTextoMensaje))
        {
            this.lblError.Text = "Se realizó la configuración del correo correctamente, revisar la cuenta de correo para visualizar el correo de prueba.";
            return;
        }
        this.lblError.Text = "Problemas al enviar el correo, por favor verificar los parámetros de configuración.";
    }
    public bool ftnEnviarCorreo(string strTitulo, string strTextoMensaje)
    {
        bool result = false;
        string text = string.Empty;
        string displayName = string.Empty;
        string host = string.Empty;
        string password = string.Empty;
        string addresses = string.Empty;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaCorreo("strDsCorreo", 0) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                text = this.blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                addresses = text;
                displayName = this.blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                host = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                int port = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][2].ToString());
                password = this.blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                int num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][6].ToString());
                if (num == 1)
                {
                    MailMessage mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress(text, displayName);
                    mailMessage.To.Add(addresses);
                    mailMessage.Subject = strTitulo;
                    mailMessage.Body = strTextoMensaje;
                    mailMessage.IsBodyHtml = true;
                    SmtpClient smtpClient = new SmtpClient();
                    smtpClient.Host = host;
                    smtpClient.Port = port;
                    smtpClient.EnableSsl = false;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new NetworkCredential(text, password);
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    try
                    {
                        smtpClient.Send(mailMessage);
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
            }
            this.blPaicma.Termina();
        }
        return result;
    }
}