﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// 
/// </summary>
public class EstadosSolicitud
{

    public const int IncompletaPlanes = 1;
    public const int IncompletaSitios = 2;
    public const int IncompletaSoporte = 3;
    public const int PorAprobarCoordinador = 4;
    public const int PorAprobarAdministrador = 5;
    public const int RechazadaCoordinador = 7;
    public const int RechazadaAdministrador = 8;
    public const int AprobadoPorDARPLegalizar = 9;
    public const int LegIncompletaPlanes = 10;
    public const int LegIncompletaCompromisos = 12;
    public const int LegIncompletaBeneficiarios = 13;
    public const int LegIncompletoSoporte = 14;
    public const int LegPorAprobarCoordinador = 15;
    public const int LegPorAprobarAdministrador = 17;
    public const int LegRechazadaCoordinador = 18;
    public const int LegRechazadaAdministrador = 19;
    public const int LegalizacionCompletaCerrado = 20;
    public const int faltaNumero = 21;
    public const int Anulada = 27;

}



