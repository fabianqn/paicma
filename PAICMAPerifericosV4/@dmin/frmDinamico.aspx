﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmDinamico.aspx.cs" Inherits="_dmin_frmDinamico" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

    <asp:Panel ID="pnlEliminacion" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblEliminacion" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSi" runat="server"  Text="SI" onclick="btnSi_Click" />
                        <asp:Button ID="btnNo" runat="server"  Text="NO" onclick="btnNo_Click" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" Text="Aceptar" onclick="btnOk_Click" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlMenuDinamico" runat="server">
        <div class="Botones">
            <asp:ImageButton ID="imgbBuscar" runat="server" 
                ImageUrl="~/Images/BuscarAccion.png" ToolTip="Buscar" 
                onclick="imgbBuscar_Click" />
            <asp:ImageButton ID="imgbNuevo" runat="server" ImageUrl="~/Images/Nuevo.png" 
                ToolTip="Nuevo" onclick="imgbNuevo_Click" style="height: 32px" />
            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png"  
                Visible="False" ToolTip="Editar" onclick="imgbEditar_Click" />
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlAccionesDinamico" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server" 
                ImageUrl="~/Images/buscar.png" ToolTip="Ejecutar Busqueda" 
                onclick="imgbEncontrar_Click" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png" 
                ToolTip="Guardar" onclick="imgbGravar_Click"  />
            <asp:ImageButton ID="imgbCancelar" runat="server" ImageUrl="~/Images/cancelar.png" ToolTip="Cancelar" onclick="imgbCancelar_Click" />
            <asp:ImageButton ID="imgbNuevoCampo" runat="server" 
                ImageUrl="~/Images/NuevoCampo.png" ToolTip="Nuevo Campo" 
                onclick="imgbNuevoCampo_Click" Visible="False"  />
            <asp:ImageButton ID="imgbEliminar" runat="server" 
                ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar" 
                onclick="imgbEliminar_Click" />
        </div>
    </asp:Panel>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlCamposTabla" runat="server" Visible="False">

                <table class="contacto3" runat="server" ID="Activiades">
                <thead>
                    <tr>
                        <td>
                            <asp:Label ID="lblTabla" runat="server" Text="Nombre de la tabla"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtTabla" runat="server" MaxLength="50"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblAbreviatura" runat="server" Text="Abreviatura"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtAbreviatura" runat="server" MaxLength="10"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblObservacion" runat="server" Text="Observación"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine" 
                                MaxLength="200" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    </thead>
        </table>
            <asp:Label ID="lblError" runat="server"></asp:Label>
            
            </asp:Panel>
            <br />

        </ContentTemplate>
    </asp:UpdatePanel>

     <asp:Panel ID="PnlTablasGrilla" runat="server">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvTablas" runat="server" AutoGenerateColumns="False" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
                    onrowcommand="gvTablas_RowCommand" 
                    onpageindexchanging="gvTablas_PageIndexChanging">
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="admTab_Id" HeaderText="admTab_Id" />
                        <asp:BoundField DataField="admTab_NombreTabla" 
                            HeaderText="Tabla" />
                        <asp:BoundField DataField="admTab_AbreviaturaTabla" 
                            HeaderText="Abreviatura" />
                        <asp:BoundField DataField="admTab_NumeroCampos" 
                            HeaderText="Campos" />
                        <asp:BoundField DataField="admTab_fecha" 
                            HeaderText="Fecha" />
                        <asp:BoundField DataField="admTab_Observacion" HeaderText="Observación" />
                        <asp:BoundField DataField="segusu_login" HeaderText="Usuario" />
                        <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                            ImageUrl="~/Images/seleccionar.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorGv" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlBusqueda" runat="server" Visible="False">
        <div class="formularioint2">
                <table class="contacto" runat="server" ID="tblBusqueda">

                    <tr>
                        <td>
                            <asp:Label ID="lblNombreBusqueda" runat="server" Text="Nombre de la tabla"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtTablaBusqueda" runat="server" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="ddlEstadoBusqueda" runat="server">
                                <asp:ListItem Value="3">Seleccione...</asp:ListItem>
                                <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                <asp:ListItem Value="1">Activo</asp:ListItem>
                            </asp:DropDownList>
                        </td>

                    </tr>
                  

                    <tr>
                        <td>
                            &nbsp;</td>
                        <td style="text-align: left">
                            <asp:Label ID="lblErrorBusqueda" runat="server"></asp:Label>
                        </td>
                    </tr>
                  

            </table>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlCampos" runat="server" Visible="False">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvCampos" runat="server" AutoGenerateColumns="False" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
                    PageSize="20" onpageindexchanging="gvCampos_PageIndexChanging">
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="Campos" HeaderText="Campo" />
                        <asp:BoundField DataField="TipoDato" 
                            HeaderText="Tipo de Dato" />
                        <asp:BoundField DataField="max_length" 
                            HeaderText="Longitud" />
                        <asp:BoundField DataField="is_nullable" 
                            HeaderText="Acepta Nullos" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblGVcampos" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>


</asp:Content>

