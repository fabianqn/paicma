﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmWFCompromisos.aspx.cs" Inherits="_dmin_frmWFCompromisos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">

     <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Aceptar" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <div class="contacto2" id="principal" runat="server">
        <h1>Compromisos Adquiridos</h1>
       <hr />
        <asp:GridView ID="gvCompromiso" CssClass="mGrid" runat="server" AutoGenerateColumns="False" OnRowCommand="gvCompromiso_RowCommand" OnPreRender="gvCompromiso_PreRender">
            <Columns>
                <asp:BoundField DataField="descripcion" HeaderText="Compromiso" />
                <asp:BoundField DataField="fechaCompromiso" HeaderText="fecha de Compromiso" />
                <asp:BoundField DataField="responsable" HeaderText="responsable" />
                 <asp:ButtonField ButtonType="Button" Text="Editar" CommandName="EditarCompromiso" />
                       <asp:ButtonField ButtonType="Button" Text="Eliminar" CommandName="EliminarCompromiso" />
                <asp:TemplateField ShowHeader="true">
                    <ItemTemplate>
                        <asp:HiddenField runat="server" ID="HiddenFieldID" Value='<%# Eval("idCompromiso") %>'   > </asp:HiddenField>          
                    </ItemTemplate>
                </asp:TemplateField>  
            </Columns>
            </asp:GridView>
        <div style="width: 100%">
            <hr />
            <table style="width: 100%" id="PlanesDinamicos" runat="server">
                <tr>
                    <td>
                        <h1 id="mensajeCrear" runat="server">Agregar Compromisos</h1>
                    </td>
                    <td>
                        <asp:ImageButton ID="imgbNuevoPlan" runat="server"
                            ImageUrl="~/Images/NuevoCampo.png" ToolTip="Nuevo Campo"
                            OnClick="imgbNuevoPlan_Click" Visible="true" />
                    </td>
                     <td>
                        <asp:ImageButton ID="imgbEliminarPlan" runat="server"
                            ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar Ultimmo Campo"
                            OnClick="imgbEliminarPlan_Click" Visible="true" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                     <hr />

                    </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
            </table>
        </div>

        <asp:Panel ID="panelBotones" runat="server">
        <div class="Botones">
            <asp:Button ID="btnGuardarCont" runat="server" Text="Guardar y Continuar" OnClick="btnGuardarCont_Click" />
            <asp:Button ID="btnGuardarVolver" runat="server" Text="Guardar y Volver" OnClick="btnGuardarVolver_Click" Visible ="false"/>
            <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
        </div>
    </asp:Panel>
        <asp:Panel ID="panelSoloVer" runat="server" Visible="false">
        <div class="Botones">
            <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
       <%--     <asp:Button ID="btnCancelarSoloVer" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" />--%>
        </div>
    </asp:Panel>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="Server">
        <script type="text/javascript">
            function newDoc() {
                window.location.assign("frmWFSolicitudComision.aspx")
            }
        $(document).ready(function () {
            $('#' + '<%= gvCompromiso.ClientID %>').DataTable();
            $(".pluginFecha").datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
        });
   </script>
</asp:Content>

