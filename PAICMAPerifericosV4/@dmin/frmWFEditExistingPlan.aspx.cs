﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmWFEditExistingPlan : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
             
                Int64 idPlan = Convert.ToInt64(Session["intIdPlanEditar"]);
                Int64 idSolcitud = Convert.ToInt64(Session["IdSolicitudRef"]);
                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    if (blPaicma.fntConsultarPlanes("datosPlanesAccion", idSolcitud, "id", idPlan))
                    {
                        if (blPaicma.myDataSet.Tables["datosPlanesAccion"].Rows.Count > 0)
                        {

                            string idCategoria = blPaicma.myDataSet.Tables["datosPlanesAccion"].Rows[0]["idEstrategia"].ToString();
                            string idIndicador  = blPaicma.myDataSet.Tables["datosPlanesAccion"].Rows[0]["idIndicador"].ToString();
                            string idActividad = blPaicma.myDataSet.Tables["datosPlanesAccion"].Rows[0]["idActividad"].ToString();
                            this.cargarCombos(idCategoria, idIndicador, idActividad);
                        }
                        else
                        {
                            //no hay registros
                        }

                        
                    }
                    else
                    {
                        //no consulto planes
                    }
                }
                else
                {
                    //no puedo conectar con base de datos
                }
                this.blPaicma.Termina();
               

            }
        }

    }

    public void cargarCombos(string idCategoria,string indicador , string actividad)
    {

            //Cargamos los La data para asignarla despues
            DataTable dataEstrategia = new DataTable();
            DataTable dataActividad = new DataTable();
            DataTable dataIndicador = new DataTable();
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarPorEstado("datosEstrategia", "WFSol_Estrategia", "estadoEst","ACT","nombreEstrategia"))
                {
                     dataEstrategia = blPaicma.myDataSet.Tables["datosEstrategia"];
                }
                else
                {
                    //No se pudieron cargar los dropdown
                }
            }
            else
            {
                //No se ha podido conectar con la base de datos
            }
            blPaicma.Termina();

            ddlistEstrategia.DataMember = "datosEstrategia";
            ddlistEstrategia.DataSource = dataEstrategia;
            ddlistEstrategia.DataValueField = "idEstrategia";
            ddlistEstrategia.DataTextField = "nombreEstrategia";
            ddlistEstrategia.DataBind();
            ddlistEstrategia.Items.Insert(ddlistEstrategia.Attributes.Count, new ListItem("Seleccione...", ""));

            LLenarDropIndicador(Convert.ToInt32(idCategoria));
            LLenarDropActividad(Convert.ToInt32(indicador));

            ddlistEstrategia.SelectedValue = idCategoria;
            ddlistIndicador.SelectedValue = indicador;
            ddlistActividad.SelectedValue = actividad;


        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntModificarExistingPlan(Convert.ToInt64(Session["intIdPlanEditar"]), Convert.ToInt64(Session["IdSolicitudRef"]), Convert.ToInt32(ddlistEstrategia.SelectedValue), Convert.ToInt32(ddlistActividad.SelectedValue), Convert.ToInt32(ddlistIndicador.SelectedValue), DateTime.Now, Convert.ToInt32(Session["IdUsuario"])))
                {
                    Response.Redirect("frmWFSolicitudComision.aspx");
                }
                else
                {
                    Response.Redirect("frmWFSolicitudComision.aspx");
                    //No pudo actualizar
                }
            }
            else
            {
                //no pudo conectar con la base de datos.
            }
            this.blPaicma.Termina();
        }

     

      /// <summary>
        /// ☻☻ metodo para llenar la lista de categorias
        /// </summary>
       /// <param name="categoria"></param>
      private void LLenarDropIndicador(int categoria)
      {
          DataTable dataIndicador = new DataTable();
          if (blPaicma.inicializar(blPaicma.conexionSeguridad))
          {
              if (blPaicma.fntConsultarIndicadorCategoria("datosIndicadoresSelect",categoria))
              {
                  dataIndicador = blPaicma.myDataSet.Tables["datosIndicadoresSelect"];


                  ddlistIndicador.DataMember = "datosIndicadoresSelect";
                  ddlistIndicador.DataSource = dataIndicador;
                  ddlistIndicador.DataValueField = "idIndicador";
                  ddlistIndicador.DataTextField = "nombreIndicador";
                  ddlistIndicador.DataBind();
              }
              else
              {
                  //No pudo actualizar
              }
          }
          else
          {
              //no pudo conectar con la base de datos.
          }
          this.blPaicma.Termina();
      }

      /// <summary>
    /// ☻☻ metodo para llenar la lista de actividades
    /// </summary>
    /// <param name="indicador"></param>
      private void LLenarDropActividad(int indicador)
      {
          DataTable dataActividad = new DataTable();

          if (blPaicma.inicializar(blPaicma.conexionSeguridad))
          {

              if (blPaicma.fntConsultarActividadIndicador("datosActividadSelect", indicador))
                  {
                      dataActividad = blPaicma.myDataSet.Tables["datosActividadSelect"];
                      ddlistActividad.DataMember = "datosActividadSelect";
                      ddlistActividad.DataSource = dataActividad;
                      ddlistActividad.DataValueField = "idActividad";
                      ddlistActividad.DataTextField = "nombreActividad";
                      ddlistActividad.DataBind();
                }
              else
              {
                  //No pudo actualizar
              }
          }
          else
          {
              //no pudo conectar con la base de datos.
          }
          this.blPaicma.Termina();

      }

    /// <summary>
    /// ☻☻ metodo para cambiar  la estrategia 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
      protected void ddlistEstrategia_SelectedIndexChanged1(object sender, EventArgs e)
      {
          // ☻☻ consultar la drop 

          if (ddlistEstrategia.SelectedValue == string.Empty)
          {

              ddlistIndicador.Items.Clear();
              ddlistActividad.Items.Clear();
              ddlistIndicador.Items.Insert(ddlistIndicador.Attributes.Count, new ListItem("Seleccione...", ""));
              ddlistActividad.Items.Insert(ddlistActividad.Attributes.Count, new ListItem("Seleccione...", ""));
          }
          else
          {
              int idCategoria = Convert.ToInt32(ddlistEstrategia.SelectedValue);
              LLenarDropIndicador(idCategoria);
              ddlistActividad.Items.Clear();
              ddlistIndicador.Items.Insert(ddlistIndicador.Attributes.Count, new ListItem("Seleccione...", ""));
              ddlistActividad.Items.Insert(ddlistActividad.Attributes.Count, new ListItem("Seleccione...", ""));
          }
      }

    /// <summary>
    /// ☻☻  evento  para cambiar las actividades 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
      protected void ddlistIndicador_SelectedIndexChanged(object sender, EventArgs e)
      {
          // ☻☻ consultar la drop 
          int idIndicador = Convert.ToInt32(ddlistIndicador.SelectedValue);
          LLenarDropActividad(idIndicador);
      }
}
