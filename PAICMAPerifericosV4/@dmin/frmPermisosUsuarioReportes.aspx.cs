﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class _dmin_prueba : System.Web.UI.Page
{

    private blSisPAICMA blPaicma = new blSisPAICMA();
    private void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }

            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                this.fntCargarListaUsuarios();
                return;
            }
            base.Response.Redirect("@dmin/frmLogin.aspx");
            
        }
    }
    public void fntCargarListaUsuarios()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarUsuarios_bol("strUsuarios"))
            {
                

                ListBoxUsuarios.DataSource = blPaicma.myDataSet.Tables["strUsuarios"];
                ListBoxUsuarios.DataValueField = "segusu_Id";
                ListBoxUsuarios.DataTextField = "segusu_login";
                ListBoxUsuarios.DataBind();
                if (blPaicma.fntConsultarReportePermitidos_bol("strReportePermitidos", Convert.ToInt32(Session["idReporteBuscar"])))
                    {
                        
                        ListBoxUsuariosSelecciondos.DataSource = blPaicma.myDataSet.Tables["strReportePermitidos"];
                        ListBoxUsuariosSelecciondos.DataValueField = "usuper_segusu_Id";
                        ListBoxUsuariosSelecciondos.DataTextField = "segusu_login";
                        ListBoxUsuariosSelecciondos.DataBind();
                        Session["cantReg"] = ListBoxUsuariosSelecciondos.Items.Count;

                       for (int i = ListBoxUsuariosSelecciondos.Items.Count - 1; i >= 0; i--){
                               for (int j = ListBoxUsuarios.Items.Count - 1; j >= 0; j--){
                                   if (ListBoxUsuarios.Items[j].Value == ListBoxUsuariosSelecciondos.Items[i].Value){
                                        ListItem li = ListBoxUsuarios.Items[j];
                                        ListBoxUsuarios.Items.Remove(li);
                                   }        
                               }
                       }
                    }else{
                   }
                }else{
            }
        }
        blPaicma.Termina();
    }

    

    protected void btnMoveRight_Click(object sender, System.EventArgs e)
    {
        for (int i = ListBoxUsuarios.Items.Count - 1; i >= 0; i--)
        {
            if (ListBoxUsuarios.Items[i].Selected == true)
            {
                ListBoxUsuariosSelecciondos.Items.Add(ListBoxUsuarios.Items[i]);
                ListItem li = ListBoxUsuarios.Items[i];
                ListBoxUsuarios.Items.Remove(li);
            }
        }
    }



    protected void btnMoveLeft_Click(object sender, System.EventArgs e)
    {
        for (int i = ListBoxUsuariosSelecciondos.Items.Count - 1; i >= 0; i--)
        {
            if (ListBoxUsuariosSelecciondos.Items[i].Selected == true)
            {
                ListBoxUsuarios.Items.Add(ListBoxUsuariosSelecciondos.Items[i]);
                ListItem li = ListBoxUsuariosSelecciondos.Items[i];
                ListBoxUsuariosSelecciondos.Items.Remove(li);
            }
        }
    }


    public bool fntInsertarUsuarios_bol(ListBox ListaIdUsuarios, int idreporte)
    {

        bool respuesta = false;
        for (int i = ListaIdUsuarios.Items.Count - 1; i >= 0; i--)
        {

            if (blPaicma.fntInsertarPermisosReporte_bol(Convert.ToInt32(ListaIdUsuarios.Items[i].Value), idreporte))
            {
                respuesta = true;
            }
            else
            {
                respuesta = false;
                break;
            }
            
        }
        return respuesta;
    }


    protected void imgbCancelar_Click(object sender, System.EventArgs e)
    {
        string strRuta = "frmReportList.aspx";
        Response.Redirect(strRuta);
    }

    protected void imgbGravar_Click(object sender, System.EventArgs e)
    {
        imgbGravar.Enabled = false;
        if (ListBoxUsuariosSelecciondos.Items.Count > 0)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (Convert.ToInt32(Session["cantReg"]) > 0)
                {
                    if (blPaicma.fntEliminarPermisosReporte_bol(Convert.ToInt32(Session["idReporteBuscar"])))
                    {
                        if (fntInsertarUsuarios_bol(ListBoxUsuariosSelecciondos, Convert.ToInt32(Session["idReporteBuscar"])))
                        {
                            LabelMsgUsuarios.Visible = false;
                            mensajes.Visible = true;
                            LabelMsgError.Visible = false;
                            HtmlMeta meta = new HtmlMeta();
                            meta.HttpEquiv = "Refresh";
                            meta.Content = "5;url=frmReportList.aspx";
                            this.Page.Controls.Add(meta);
                            LabelMsgCorrecto.Visible = true;
                            Label1.Text = "Sera redirigido en 5 segundos";
                            Label1.Visible = true;
                        }
                        else
                        {
                            LabelMsgUsuarios.Visible = false;
                            HtmlMeta meta = new HtmlMeta();
                            meta.HttpEquiv = "Refresh";
                            meta.Content = "5;url=frmReportList.aspx";
                            this.Page.Controls.Add(meta);
                            mensajes.Visible = true;
                            LabelMsgError.Visible = true;
                            Label1.Text = "Sera redirigido en 5 segundos";
                            Label1.Visible = true;
                        }
                    }
                    else
                    {
                        LabelMsgUsuarios.Visible = false;
                        HtmlMeta meta = new HtmlMeta();
                        meta.HttpEquiv = "Refresh";
                        meta.Content = "5;url=frmReportList.aspx";
                        this.Page.Controls.Add(meta);
                        mensajes.Visible = true;
                        LabelMsgError.Visible = true;
                        Label1.Text = "Sera redirigido en 5 segundos";
                        Label1.Visible = true;
                    }
                }
                else
                {
                    if (fntInsertarUsuarios_bol(ListBoxUsuariosSelecciondos, Convert.ToInt32(Session["idReporteBuscar"])))
                    {
                        LabelMsgUsuarios.Visible = false;
                        LabelMsgError.Visible = false;
                        HtmlMeta meta = new HtmlMeta();
                        meta.HttpEquiv = "Refresh";
                        meta.Content = "5;url=frmReportList.aspx";
                        this.Page.Controls.Add(meta);
                        mensajes.Visible = true;
                        LabelMsgCorrecto.Visible = true;
                        Label1.Text = "Sera redirigido en 5 segundos";
                        Label1.Visible = true;
                    }
                    else
                    {
                        LabelMsgUsuarios.Visible = false;
                        HtmlMeta meta = new HtmlMeta();
                        meta.HttpEquiv = "Refresh";
                        meta.Content = "5;url=frmReportList.aspx";
                        this.Page.Controls.Add(meta);
                        mensajes.Visible = true;
                        LabelMsgError.Visible = true;
                        Label1.Text = "Sera redirigido en 5 segundos";
                        Label1.Visible = true;
                    }
                }
            }
            else
            {
                        LabelMsgUsuarios.Visible = false;
                        LabelMsgError.Visible = false;
                        HtmlMeta meta = new HtmlMeta();
                        meta.HttpEquiv = "Refresh";
                        meta.Content = "5;url=frmReportList.aspx";
                        this.Page.Controls.Add(meta);
                        mensajes.Visible = true;
                        LabelMsgBaseDatos.Visible = true;
                        Label1.Text = "Sera redirigido en 5 segundos";
                        Label1.Visible = true;
              
            }
            blPaicma.Termina();
        }
        else
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntEliminarPermisosReporte_bol(Convert.ToInt32(Session["idReporteBuscar"])))
                {
                    LabelMsgUsuarios.Visible = false;
                    mensajes.Visible = true;
                    LabelMsgError.Visible = false;
                    HtmlMeta meta = new HtmlMeta();
                    meta.HttpEquiv = "Refresh";
                    meta.Content = "5;url=frmReportList.aspx";
                    this.Page.Controls.Add(meta);
                    LabelMsgCorrecto.Visible = true;
                    Label1.Text = "Sera redirigido en 5 segundos";
                    Label1.Visible = true;
                }
            }
            this.blPaicma.Termina();
        }
    }
 }


