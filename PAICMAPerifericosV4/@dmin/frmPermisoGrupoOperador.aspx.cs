﻿using ASP;
using System;
using System.Drawing;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmPermisoGrupoOperador : System.Web.UI.Page
{

    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }
            this.ftnValidarPermisos();
            this.fntCargarGrillaGrupoOperador(0, 0, string.Empty, string.Empty, 0);
        }
    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        this.blPaicma = null;
    }
    public void ftnValidarPermisos()
    {
        string a = string.Empty;
        string a2 = string.Empty;
        string a3 = string.Empty;
        string a4 = string.Empty;
        string[] array = HttpContext.Current.Request.RawUrl.Split(new char[]
		{
			'/'
		});
        string text = array[array.GetUpperBound(0)];
        array = text.Split(new char[]
		{
			'?'
		});
        text = array[array.GetLowerBound(0)];
        if (text != string.Empty)
        {
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (this.blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(this.Session["IdUsuario"].ToString()), text) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    a = this.blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                    a2 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                    a3 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                    a4 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();
                    if (a == "False")
                    {
                        this.imgbBuscar.Visible = false;
                    }
                    a2 = "False";
                    if (a3 == "False")
                    {
                        this.imgbEditar.Visible = false;
                        this.imgbGravar.Visible = false;
                    }
                    if (a4 == "False")
                    {
                        this.imgbEliminar.Visible = false;
                    }
                }
                this.blPaicma.Termina();
                return;
            }
        }
        else
        {
            base.Response.Redirect("@dmin/frmGrupoOperador.aspx");
        }
    }
    public void fntInactivarPaneles(bool bolValor)
    {
        this.pnlAccionesPermisosGrupoOperador.Visible = bolValor;
        this.pnlEliminacion.Visible = bolValor;
        this.PnlPermisosGrupoOperadorGrilla.Visible = bolValor;
        this.pnlPermisosGrupoOperadorNuevo.Visible = bolValor;
        this.pnlMenuPermisosGrupoOperador.Visible = bolValor;
        this.pnlRespuesta.Visible = bolValor;
        this.pnlUsuariosDisponibles.Visible = bolValor;
    }
    public void fntCargarGrillaGrupoOperador(int intOp, int intIdGrupoOperador, string strNombreGrupo, string strDescripcionGrupo, int intIdEstado)
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaGrupoOperador_bol("strDsGrillaGrupoOperador", intOp, intIdGrupoOperador, strNombreGrupo, strDescripcionGrupo, intIdEstado))
            {
                this.gvGrupoOperador.Columns[0].Visible = true;
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gvGrupoOperador.DataMember = "strDsGrillaGrupoOperador";
                    this.gvGrupoOperador.DataSource = this.blPaicma.myDataSet;
                    this.gvGrupoOperador.DataBind();
                    this.lblErrorGv.Text = string.Empty;
                }
                else
                {
                    this.gvGrupoOperador.DataMember = "strDsGrillaGrupoOperador";
                    this.gvGrupoOperador.DataSource = this.blPaicma.myDataSet;
                    this.gvGrupoOperador.DataBind();
                    this.lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                this.gvGrupoOperador.Columns[0].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    protected void gvGrupoOperador_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            this.gvGrupoOperador.Columns[0].Visible = true;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow = this.gvGrupoOperador.Rows[index];
            TableCell tableCell = gridViewRow.Cells[0];
            int num = Convert.ToInt32(tableCell.Text);
            this.Session["intIdGrupoOperador"] = num;
            this.imgbEditar.Visible = true;
            this.gvGrupoOperador.Columns[0].Visible = false;
            int count = this.gvGrupoOperador.Rows.Count;
            for (int i = 0; i < count; i++)
            {
                this.gvGrupoOperador.Rows[i].BackColor = Color.Empty;
            }
            this.gvGrupoOperador.Rows[index].BackColor = Color.Brown;
        }
    }
    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlAccionesPermisosGrupoOperador.Visible = true;
        this.imgbEncontrar.Visible = false;
        this.imgbGravar.Visible = true;
        this.imgbCancelar.Visible = true;
        this.pnlPermisosGrupoOperadorNuevo.Visible = true;
        this.fntCargarObjetos();
        this.Session["Operacion"] = "EditarPermisoGrupo";
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            int intIdGrupoOperador = Convert.ToInt32(this.Session["intIdGrupoOperador"].ToString());
            if (this.blPaicma.fntConsultaGrupoOperador_bol("strDsGrillaGrupoOperador", 1, intIdGrupoOperador, string.Empty, string.Empty, 0) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlEstado.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                this.txtNombreGrupo.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                this.txtDescripcion.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                this.ftnValidarPermisos();
                this.fntCargarGrillaUsuariosGrupoOperador(intIdGrupoOperador);
                this.imgbNuevoPermiso.Visible = true;
                this.ftn_InactivarControles();
                this.imgbGravar.Visible = false;
                if (this.fntVerificarUsuariosDisponibles_int(intIdGrupoOperador) == 0)
                {
                    this.imgbNuevoPermiso.Visible = false;
                }
                else
                {
                    this.imgbNuevoPermiso.Visible = true;
                }
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarObjetos()
    {
        this.fntCargarEstado();
    }
    public int fntVerificarUsuariosDisponibles_int(int intIdGrupoOperador)
    {
        int result = 0;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaUsuariosDisponiblesGrupoOperador_bol("strDsGrillaUsuariosDisponiblesGrupoOperador", intIdGrupoOperador))
            {
                result = this.blPaicma.myDataSet.Tables[0].Rows.Count;
            }
            this.blPaicma.Termina();
        }
        return result;
    }
    public void fntCargarEstado()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaEstadoTipoUsuario("strDsEstado") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlEstado.DataMember = "strDsEstado";
                this.ddlEstado.DataSource = this.blPaicma.myDataSet;
                this.ddlEstado.DataValueField = "est_Id";
                this.ddlEstado.DataTextField = "est_Descripcion";
                this.ddlEstado.DataBind();
                this.ddlEstado.Items.Insert(this.ddlEstado.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        if (this.Session["Operacion"] == null)
        {
            string url = "frmPermisoGrupoOperador.aspx";
            base.Response.Redirect(url);
        }
        if (this.Session["Operacion"].ToString() == "EditarPermisoGrupo" || this.Session["Operacion"].ToString() == "EliminarUsuario")
        {
            string url2 = "frmPermisoGrupoOperador.aspx";
            this.Session["Operacion"] = null;
            this.Session["intIdGrupoUsuario"] = null;
            base.Response.Redirect(url2);
        }
        if (this.Session["Operacion"].ToString() == "NuevoUsuario")
        {
            this.fntInactivarPaneles(false);
            this.pnlPermisosGrupoOperadorNuevo.Visible = true;
            this.pnlAccionesPermisosGrupoOperador.Visible = true;
            this.imgbNuevoPermiso.Visible = true;
            this.imgbEliminar.Visible = true;
            this.Session["Operacion"] = "EditarPermisoGrupo";
            this.imgbGravar.Visible = false;
        }
    }
    public void fntCargarGrillaUsuariosGrupoOperador(int intIdGrupoOperador)
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaUsuarioGrupoOperador_bol("strDsGrillaUsuariosGrupoOperador", intIdGrupoOperador))
            {
                this.gvUsuariosPermisos.Columns[0].Visible = true;
                this.gvUsuariosPermisos.Columns[1].Visible = true;
                this.gvUsuariosPermisos.Columns[2].Visible = true;
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gvUsuariosPermisos.DataMember = "strDsGrillaUsuariosGrupoOperador";
                    this.gvUsuariosPermisos.DataSource = this.blPaicma.myDataSet;
                    this.gvUsuariosPermisos.DataBind();
                    this.lblErrorGv.Text = string.Empty;
                }
                else
                {
                    this.gvUsuariosPermisos.DataMember = "strDsGrillaUsuariosGrupoOperador";
                    this.gvUsuariosPermisos.DataSource = this.blPaicma.myDataSet;
                    this.gvUsuariosPermisos.DataBind();
                    this.lblGrillaUsuario.Text = "No hay usuarios asignados a este grupo.";
                }
                this.gvUsuariosPermisos.Columns[0].Visible = false;
                this.gvUsuariosPermisos.Columns[1].Visible = false;
                this.gvUsuariosPermisos.Columns[2].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    public void ftn_InactivarControles()
    {
        this.txtDescripcion.Enabled = false;
        this.txtNombreGrupo.Enabled = false;
        this.ddlEstado.Enabled = false;
    }
    public void fntCargarGrillaUsuariosDisponiblesGrupoOperador(int intIdGrupoOperador)
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaUsuariosDisponiblesGrupoOperador_bol("strDsGrillaUsuariosDisponiblesGrupoOperador", intIdGrupoOperador))
            {
                this.gvUsuariosDisponibles.Columns[0].Visible = true;
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gvUsuariosDisponibles.DataMember = "strDsGrillaUsuariosDisponiblesGrupoOperador";
                    this.gvUsuariosDisponibles.DataSource = this.blPaicma.myDataSet;
                    this.gvUsuariosDisponibles.DataBind();
                    this.lblErrorGv.Text = string.Empty;
                }
                else
                {
                    this.gvUsuariosDisponibles.DataMember = "strDsGrillaUsuariosDisponiblesGrupoOperador";
                    this.gvUsuariosDisponibles.DataSource = this.blPaicma.myDataSet;
                    this.gvUsuariosDisponibles.DataBind();
                    this.lblGrillaUsuario.Text = "No hay usuarios disponibles para este grupo.";
                }
                this.gvUsuariosDisponibles.Columns[0].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    protected void imgbNuevoPermiso_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlAccionesPermisosGrupoOperador.Visible = true;
        this.imgbNuevoPermiso.Visible = false;
        this.imgbEliminar.Visible = false;
        this.pnlUsuariosDisponibles.Visible = true;
        this.fntCargarGrillaUsuariosDisponiblesGrupoOperador(Convert.ToInt32(this.Session["intIdGrupoOperador"].ToString()));
        this.Session["Operacion"] = "NuevoUsuario";
        this.imgbGravar.Visible = true;
    }
    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        int intSupervisar = 0;
        if (this.Session["Operacion"].ToString() == "NuevoUsuario")
        {
            int count = this.gvUsuariosDisponibles.Rows.Count;
            for (int i = 0; i < count; i++)
            {
                CheckBox checkBox = (CheckBox)this.gvUsuariosDisponibles.Rows[i].Cells[2].FindControl("chkUsuario");
                if (checkBox.Checked)
                {
                    CheckBox checkBox2 = (CheckBox)this.gvUsuariosDisponibles.Rows[i].Cells[3].FindControl("chkSupervisar");
                    if (checkBox2.Checked)
                    {
                        intSupervisar = 1;
                    }
                    this.gvUsuariosDisponibles.Columns[0].Visible = true;
                    int intIdUsuario = Convert.ToInt32(this.gvUsuariosDisponibles.Rows[i].Cells[0].Text);
                    this.gvUsuariosDisponibles.Columns[0].Visible = false;
                    if (this.ftnGuardarPermisosUsuarios(Convert.ToInt32(this.Session["intIdGrupoOperador"].ToString()), intIdUsuario, Convert.ToInt32(this.Session["IdUsuario"].ToString()), intSupervisar))
                    {
                        this.lblRespuesta.Text = "Registro insertado satisfactoriamente";
                        this.lblError.Text = string.Empty;
                        this.lblGrillaUsuario.Text = string.Empty;
                    }
                    else
                    {
                        this.lblRespuesta.Text = "Problema al insertar el registro, por favor comuníquese con el administrador.";
                    }
                }
            }
            this.fntInactivarPaneles(false);
            this.pnlRespuesta.Visible = true;
        }
    }
    public bool ftnGuardarPermisosUsuarios(int intIdGrupoOperador, int intIdUsuario, int intIdUsuarioCreacion, int intSupervisar)
    {
        bool result = false;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntIngresarUsuarioGrupoOperador_bol(intIdGrupoOperador, intIdUsuario, intIdUsuarioCreacion, intSupervisar))
            {
                result = true;
            }
            else
            {
                result = false;
                this.lblError.Text = "Problema al insertar el registro, por favor comuníquese con el administrador.";
            }
            this.blPaicma.Termina();
        }
        return result;
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        if (this.Session["Operacion"] == null)
        {
            return;
        }
        if (this.Session["Operacion"].ToString() == "NuevoUsuario" || this.Session["Operacion"].ToString() == "EliminarUsuario" || this.Session["Operacion"].ToString() == "EditarPermisoGrupo")
        {
            this.fntInactivarPaneles(false);
            this.pnlAccionesPermisosGrupoOperador.Visible = true;
            this.pnlPermisosGrupoOperadorNuevo.Visible = true;
            this.fntCargarGrillaUsuariosGrupoOperador(Convert.ToInt32(this.Session["intIdGrupoOperador"].ToString()));
            this.imgbNuevoPermiso.Visible = true;
            this.imgbEliminar.Visible = true;
            if (this.fntVerificarUsuariosDisponibles_int(Convert.ToInt32(this.Session["intIdGrupoOperador"].ToString())) == 0)
            {
                this.imgbNuevoPermiso.Visible = false;
                return;
            }
            this.imgbNuevoPermiso.Visible = true;
        }
    }
    protected void gvUsuariosPermisos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            this.gvUsuariosPermisos.Columns[0].Visible = true;
            this.gvUsuariosPermisos.Columns[1].Visible = true;
            this.gvUsuariosPermisos.Columns[2].Visible = true;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow = this.gvUsuariosPermisos.Rows[index];
            TableCell tableCell = gridViewRow.Cells[0];
            int num = Convert.ToInt32(tableCell.Text);
            this.Session["intIdGrupoUsuario"] = num;
            TableCell tableCell2 = gridViewRow.Cells[3];
            string text = tableCell2.Text;
            this.Session["strUsuarioGrupoR"] = text;
            this.gvUsuariosPermisos.Columns[0].Visible = false;
            this.gvUsuariosPermisos.Columns[1].Visible = false;
            this.gvUsuariosPermisos.Columns[2].Visible = false;
            this.ftnLimpiarGrillaColor_gvUsuariosPermisos();
            this.gvUsuariosPermisos.Rows[index].BackColor = Color.Brown;
            this.Session["Operacion"] = "EliminarUsuario";
        }
    }
    protected void imgbEliminar_Click(object sender, ImageClickEventArgs e)
    {
        if (this.Session["Operacion"].ToString() == "EliminarUsuario")
        {
            this.fntInactivarPaneles(false);
            this.pnlEliminacion.Visible = true;
            this.lblEliminacion.Text = string.Concat(new string[]
			{
				"¿Está seguro de eliminar el usuario ",
				this.Session["strUsuarioGrupoR"].ToString(),
				"  del grupo ",
				this.txtNombreGrupo.Text.Trim(),
				" ?"
			});
            return;
        }
        if (this.Session["Operacion"].ToString() == "EditarPermisoGrupo")
        {
            this.fntInactivarPaneles(false);
            this.pnlEliminacion.Visible = true;
            this.lblEliminacion.Text = "¿Está seguro de eliminar todos los usuarios del grupo " + this.txtNombreGrupo.Text.Trim() + " ?";
        }
    }
    protected void btnNo_Click(object sender, EventArgs e)
    {
        if (this.Session["Operacion"].ToString() == "EliminarUsuario")
        {
            this.fntInactivarPaneles(false);
            this.Session["strUsuarioGrupoR"] = null;
            this.Session["intIdGrupoUsuario"] = null;
            this.Session["Operacion"] = "EditarPermisoGrupo";
            this.pnlAccionesPermisosGrupoOperador.Visible = true;
            this.pnlPermisosGrupoOperadorNuevo.Visible = true;
            this.ftnLimpiarGrillaColor_gvUsuariosPermisos();
        }
    }
    public void ftnLimpiarGrillaColor_gvUsuariosPermisos()
    {
        int count = this.gvUsuariosPermisos.Rows.Count;
        for (int i = 0; i < count; i++)
        {
            this.gvUsuariosPermisos.Rows[i].BackColor = Color.Empty;
        }
    }
    protected void btnSi_Click(object sender, EventArgs e)
    {
        if (this.Session["Operacion"].ToString() == "EliminarUsuario")
        {
            if (this.fntEliminaUsuarioGrupo(Convert.ToInt32(this.Session["intIdGrupoUsuario"].ToString())))
            {
                this.lblRespuesta.Text = "Usuario eliminado satisfactoriamente.";
            }
            else
            {
                this.lblRespuesta.Text = "Problemas al eliminar el usuario, contactar al administrador.";
            }
            this.fntInactivarPaneles(false);
            this.pnlRespuesta.Visible = true;
            return;
        }
        if (this.Session["Operacion"].ToString() == "EditarPermisoGrupo")
        {
            if (this.fntEliminaTodosLosUsuariosGrupo(Convert.ToInt32(this.Session["intIdGrupoOperador"].ToString())))
            {
                this.lblRespuesta.Text = "Usuarios eliminados satisfactoriamente.";
            }
            else
            {
                this.lblRespuesta.Text = "Problemas al eliminar los usuarios del grupo, contactar al administrador.";
            }
            this.fntInactivarPaneles(false);
            this.pnlRespuesta.Visible = true;
        }
    }
    public bool fntEliminaUsuarioGrupo(int intIdGrupoUsuario)
    {
        bool result = false;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            result = this.blPaicma.fntEliminarGrupoUsuario_bol(intIdGrupoUsuario);
            this.blPaicma.Termina();
        }
        return result;
    }
    public bool fntEliminaTodosLosUsuariosGrupo(int intIdGrupoOperador)
    {
        bool result = false;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            result = this.blPaicma.fntEliminarTodosUsuariosGrupo_bol(intIdGrupoOperador);
            this.blPaicma.Termina();
        }
        return result;
    }
    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlAccionesPermisosGrupoOperador.Visible = true;
        this.pnlPermisosGrupoOperadorNuevo.Visible = true;
        this.imgbEncontrar.Visible = true;
        this.imgbEliminar.Visible = false;
        this.imgbGravar.Visible = false;
        this.imgbCancelar.Visible = true;
        this.fntCargarObjetos();
        this.fntLimpiarObjetos();
    }
    public void fntLimpiarObjetos()
    {
        this.txtDescripcion.Text = string.Empty;
        this.txtNombreGrupo.Text = string.Empty;
    }
    protected void imgbEncontrar_Click(object sender, ImageClickEventArgs e)
    {
        string strNombreGrupo = string.Empty;
        string strDescripcionGrupo = string.Empty;
        int intIdEstado = 0;
        if (this.txtNombreGrupo.Text.Trim() != string.Empty)
        {
            strNombreGrupo = this.txtNombreGrupo.Text.Trim();
        }
        if (this.txtDescripcion.Text.Trim() != string.Empty)
        {
            strDescripcionGrupo = this.txtDescripcion.Text.Trim();
        }
        if (this.ddlEstado.SelectedValue != "Seleccione...")
        {
            intIdEstado = Convert.ToInt32(this.ddlEstado.SelectedValue.ToString());
        }
        this.fntCargarGrillaGrupoOperador(2, 0, strNombreGrupo, strDescripcionGrupo, intIdEstado);
        this.fntInactivarPaneles(false);
        this.pnlMenuPermisosGrupoOperador.Visible = true;
        this.PnlPermisosGrupoOperadorGrilla.Visible = true;
    }
    protected void gvUsuariosPermisos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
    }
    protected void gvUsuariosDisponibles_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvUsuariosDisponibles.Columns[0].Visible = true;
        this.gvUsuariosDisponibles.PageIndex = e.NewPageIndex;
        this.fntCargarGrillaUsuariosDisponiblesGrupoOperador(Convert.ToInt32(this.Session["intIdGrupoOperador"].ToString()));
        this.gvUsuariosDisponibles.Columns[0].Visible = false;
    }
    protected void gvGrupoOperador_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvGrupoOperador.Columns[0].Visible = true;
        this.gvGrupoOperador.PageIndex = e.NewPageIndex;
        this.fntCargarGrillaGrupoOperador(0, 0, string.Empty, string.Empty, 0);
        this.gvGrupoOperador.Columns[0].Visible = false;
    }
}