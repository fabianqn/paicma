﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Text.RegularExpressions;


public partial class _dmin_frmFormDinamico : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    public TextBox[] arregloTextBoxs;
    public DropDownList[] arregloCombos;
    public TextBox[] arregloTextBoxsLongitud;
    public TextBox[] arregloTextBoxsOrden;
    public TextBox[] arregloTextBoxsNombre;
    public CheckBox[] arregloCheckBox;
    public int contadorControles;
    public int cont;
    public Label[] arregloLables
    {
        get { return (Label[])Session["arrLables"]; }
        set { Session["arrLables"] = value; }
    }
    public Label[] arregloLables2
    {
        get { return (Label[])Session["arrLables2"]; }
        set { Session["arrLables2"] = value; }
    }
    public Label[] arregloLables3
    {
        get { return (Label[])Session["arrLables3"]; }
        set { Session["arrLables3"] = value; }
    }
    public Label[] arregloLables4
    {
        get { return (Label[])Session["arrLables4"]; }
        set { Session["arrLables4"] = value; }
    }
    public Label[] arregloLables5
    {
        get { return (Label[])Session["arrLables5"]; }
        set { Session["arrLables5"] = value; }
    }

    public Label[] arregloLablesCheck
    {
        get { return (Label[])Session["arrLablesCheck"]; }
        set { Session["arrLablesCheck"] = value; }
    }


    public TextBox[] arrTextboxs
    {
        get { return (TextBox[])Session["arreglotextbox"]; }
        set { Session["arreglotextbox"] = value; }

    }
    public DropDownList[] arrCombos
    {
        get { return (DropDownList[])Session["arregloCombos"]; }
        set { Session["arregloCombos"] = value; }
    }
    public TextBox[] arrLongitud
    {
        get { return (TextBox[])Session["arregloLongitud"]; }
        set { Session["arregloLongitud"] = value; }
    }
    public TextBox[] arrOrden
    {
        get { return (TextBox[])Session["arregloOrden"]; }
        set { Session["arregloOrden"] = value; }
    }

    public TextBox[] arrNombre
    {
        get { return (TextBox[])Session["arrNombre"]; }
        set { Session["arrNombre"] = value; }

    }

    public CheckBox[] arrcheck
    {
        get { return (CheckBox[])Session["arregloCheck"]; }
        set { Session["arregloCheck"] = value; }

    }


    protected int NumberOfControls
    {
        get { return (int)Session["NumControls"]; }
        set { Session["NumControls"] = value; }
    }


    protected void Page_Init(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                //ftnValidarPermisos();
                fntCargarGrillaTablas(1, 0, string.Empty);
                fntCargaPrimerFiltro();

            }

            lblError.Text = string.Empty;
            arregloTextBoxs = new TextBox[200];
            arregloCombos = new DropDownList[200];
            arregloLables = new Label[200];
            arregloLables2 = new Label[200];
            arregloTextBoxsLongitud = new TextBox[200];
            arregloLables3 = new Label[200];
            arregloLables4 = new Label[200];
            arregloLables5 = new Label[200];

            arregloLablesCheck = new Label[200]; 

            arregloTextBoxsOrden = new TextBox[200];
            arregloTextBoxsNombre = new TextBox[200];

            arregloCheckBox = new CheckBox[200];
            this.NumberOfControls = 0;
            contadorControles = 0;
            arrTextboxs = new TextBox[200];
            arrNombre = new TextBox[200];
            arrLongitud = new TextBox[200];
            arrCombos = new DropDownList[200];
            arrOrden = new TextBox[200];

            arrcheck = new CheckBox[200];
            
        }
        cont = this.NumberOfControls;

        try
        {
            cont = this.NumberOfControls;
            arregloTextBoxs = new TextBox[200];
            Array.Copy(arrTextboxs, arregloTextBoxs, 200);

            arregloCombos = new DropDownList[200];
            Array.Copy(arrCombos, arregloCombos, 200);

            arregloTextBoxsLongitud = new TextBox[200];
            Array.Copy(arrLongitud, arregloTextBoxsLongitud, 200);

            arregloTextBoxsOrden = new TextBox[200];
            Array.Copy(arrOrden, arregloTextBoxsOrden, 200);

            arregloTextBoxsNombre = new TextBox[200];
            Array.Copy(arrNombre, arregloTextBoxsNombre, 200);

            arregloCheckBox = new CheckBox[200];
            Array.Copy(arrcheck, arregloCheckBox, 200);

            for (int i = 0; i < cont; i++)
            {
                //AgregarControles(TextBox txtnombreCampo, DropDownList ddlTipo, Label lblNombre, Label lblTipo, Label lblLongitud, TextBox txtLongitud, Label lblTitulo, Label lblOrden, TextBox txtOrden, TextBox txtLabel)
                AgregarControles(arregloTextBoxs[i], arregloCombos[i], arregloLables[i], arregloLables2[i], arregloLables3[i], arregloTextBoxsLongitud[i], arregloLables4[i], arregloLables5[i], arregloTextBoxsOrden[i], arregloTextBoxsNombre[i], arregloLablesCheck[i], arregloCheckBox[i]);
            }

            Array.Copy(arregloTextBoxs, arrTextboxs, 200);
            Array.Copy(arregloCombos, arrCombos, 200);
            Array.Copy(arregloTextBoxsLongitud, arrLongitud, 200);
            Array.Copy(arregloTextBoxsOrden, arrOrden, 200);
            Array.Copy(arregloTextBoxsNombre, arrNombre, 200);
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }

        

    }


    
    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        const string number = "\"number\"";
        const string complemento = "]').keypress(function (e) { var regex = new RegExp('^[0-9]$');var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);if (regex.test(str)) {return true;} e.preventDefault();return false;}); $('input[data-type=";
        var script2 = " $(':input').keypress(function (e) {var regex = new RegExp('^[a-zA-Z0-9@. ]+$');var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);if (regex.test(str)) {return true;} e.preventDefault();return false;});$(':input').blur(function () {var regex = new RegExp('^$|^[a-zA-Z0-9@. ]+$');var str = $(this).val();if (regex.test(str)) {return true;} else { $(this).val('');$(this).focus();return false;}}); $('input[data-type=" + number + complemento + number + "]').blur(function () { var regex = new RegExp('^$|^[0-9]{1,3}$');var str = $(this).val();if (regex.test(str)) {return true;} else {$(this).val('');$(this).focus();return false;}});";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowE", script2, true);
    }
    

    public void fntCargarGrillaTablas(int intIdEstadoActivacion, int intOp, string strNombreTabla)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaAdmTab_bol("strDsGrillaTablas", intIdEstadoActivacion, intOp, strNombreTabla, 0))
            {
                gvTablas.Columns[0].Visible = true;
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvTablas.DataMember = "strDsGrillaTablas";
                    gvTablas.DataSource = blPaicma.myDataSet;
                    gvTablas.DataBind();
                    lblErrorGv.Text = string.Empty;
                }
                else
                {
                    gvTablas.DataMember = "strDsGrillaTablas";
                    gvTablas.DataSource = blPaicma.myDataSet;
                    gvTablas.DataBind();
                    lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                gvTablas.Columns[0].Visible = false;
            }
            blPaicma.Termina();
        }
    }
    //(TxtNombreCampo, nuevoCmb, labelNombre, labelTipo, LabelLogitud, nuevoTxtLongitud, LabelTituloCampo, LabelOrden, nuevoTxtOrden, txtMostrarLabel);
    protected void AgregarControles(TextBox txtnombreCampo, DropDownList ddlTipo, Label lblNombre, Label lblTipo, Label lblLongitud, TextBox txtLongitud, Label lblTitulo, Label lblOrden, TextBox txtOrden, TextBox txtLabel, Label lblCheckBox, CheckBox checkShow)
    {
        try
        {
            HtmlTableRow fila = new HtmlTableRow();
            HtmlTableRow fila2 = new HtmlTableRow();
            HtmlTableRow filaSeparadora = new HtmlTableRow();
            HtmlTableCell celdaSeparadora = new HtmlTableCell();
          
            HtmlTableCell celda = new HtmlTableCell();
            HtmlTableCell celda2 = new HtmlTableCell();
            HtmlTableCell celda3 = new HtmlTableCell();
            HtmlTableCell celda4 = new HtmlTableCell();
            HtmlTableCell celda5 = new HtmlTableCell();
            HtmlTableCell celda6 = new HtmlTableCell();
            HtmlTableCell celda7 = new HtmlTableCell();
            HtmlTableCell celda8 = new HtmlTableCell();
            HtmlTableCell celda9 = new HtmlTableCell();
            HtmlTableCell celda10 = new HtmlTableCell();
            HtmlTableCell celda11 = new HtmlTableCell();
            HtmlTableCell celda12 = new HtmlTableCell();

            celdaSeparadora.ColSpan = 6;
            celdaSeparadora.Controls.Add(new LiteralControl("<hr/>"));
            filaSeparadora.Cells.Add(celdaSeparadora);

           //Agrego Nombre del Campo
            celda.Controls.Add(lblNombre);
            celda2.Controls.Add(txtnombreCampo);
            //Agrego Texto que llevara el label
            celda3.Controls.Add(lblTitulo);
            celda4.Controls.Add(txtLabel);
            //Agrego Combo Box de tipo
            celda5.Controls.Add(lblTipo);
            celda6.Controls.Add(ddlTipo);
            //Agrego texto de orden
            celda7.Controls.Add(lblOrden);
            celda8.Controls.Add(txtOrden);
            // agrego la caja de logitud
            celda9.Controls.Add(lblLongitud);
            celda10.Controls.Add(txtLongitud);

            //Check que indica si se va a mostrar en la tabla o no
            celda11.Controls.Add(lblCheckBox);
            celda12.Controls.Add(checkShow);


            fila.Cells.Add(celda);
            fila.Cells.Add(celda2);

            fila.Cells.Add(celda3);
            fila.Cells.Add(celda4);
       
            fila.Cells.Add(celda5);
            fila.Cells.Add(celda6);

            fila2.Cells.Add(celda7);
            fila2.Cells.Add(celda8);

            fila2.Cells.Add(celda9);
            fila2.Cells.Add(celda10);

            fila2.Cells.Add(celda11);
            fila2.Cells.Add(celda12);

            camposdinamicos.Rows.Add(fila);
            camposdinamicos.Rows.Add(fila2);
            camposdinamicos.Rows.Add(filaSeparadora);

        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }

    }

    public void ftnValidarPermisos()
    {
        //int intIdFormulario = 0;
        string strBuscar = string.Empty;
        string strNuevo = string.Empty;
        string strEditar = string.Empty;
        string strEliminar = string.Empty;

        //obtiene el nombre de la pagina actual
        string[] strRutaPagina = HttpContext.Current.Request.RawUrl.Split('/');
        string strNombrePagina = strRutaPagina[strRutaPagina.GetUpperBound(0)];
        strRutaPagina = strNombrePagina.Split('?');
        strNombrePagina = strRutaPagina[strRutaPagina.GetLowerBound(0)];
        //fin obtiene el nombre de la pagina actual

        if (strNombrePagina != string.Empty)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {

                //intIdFormulario = Convert.ToInt32(Request.QueryString["op"].ToString());
                //Session["intIdFormulario"] = intIdFormulario;
                if (blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(Session["IdUsuario"].ToString()), strNombrePagina))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strBuscar = blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                        strNuevo = blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                        strEditar = blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                        strEliminar = blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();

                        if (strBuscar == "False")
                        {
                            imgbBuscar.Visible = false;
                        }
                        if (strNuevo == "False")
                        {
                            imgbNuevo.Visible = false;
                            imgbNuevoCampo.Visible = false;
                        }
                        if (strEditar == "False")
                        {
                            imgbEditar.Visible = false;
                            imgbGravar.Visible = false;
                            imgbNuevoCampo.Visible = false;
                        }
                        if (strEliminar == "False")
                        {
                            imgbEliminar.Visible = false;
                        }


                    }
                }

                blPaicma.Termina();
            }
        }
        else
        {
            Response.Redirect("@dmin/frmLogin.aspx");
        }

    }

    public void ftnInactivarControles(Boolean bolValor)
    {
        pnlAccionesDinamico.Visible = bolValor;
        pnlCamposTabla.Visible = bolValor;
        pnlEliminacion.Visible = bolValor;
        pnlMenuDinamico.Visible = bolValor;
        pnlRespuesta.Visible = bolValor;
        PnlTablasGrilla.Visible = bolValor;
        pnlBusqueda.Visible = bolValor;
        pnlCampos.Visible = bolValor;
    }

    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        ftnInactivarControles(false);
        pnlAccionesDinamico.Visible = true;
        pnlCamposTabla.Visible = true;

        imgbEncontrar.Visible = false;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        imgbNuevoCampo.Visible = true;
        Session["Operacion"] = "Crear";
    }

    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        string strRuta = "frmFormDinamico.aspx";
        Response.Redirect(strRuta);
    }

    protected void imgbNuevoCampo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int numeroRegistro = this.NumberOfControls;

            //creamos nuestros textbox
            TextBox TxtNombreCampo = new TextBox();
            TxtNombreCampo.ID = "txt" + numeroRegistro.ToString();
            TxtNombreCampo.Attributes["maxlength"] = "50";
            arregloTextBoxs = new TextBox[200];
            Array.Copy(arrTextboxs, arregloTextBoxs, 200);
            arregloTextBoxs[numeroRegistro] = TxtNombreCampo;
            Array.Copy(arregloTextBoxs, arrTextboxs, 200);

            //creamos los dropDownList
            DropDownList nuevoCmb = new DropDownList();
            nuevoCmb.ID = "ddl" + numeroRegistro.ToString();

            ListItem valorCombo0 = new ListItem();
            valorCombo0.Value="0";
            valorCombo0.Text="Tipo de Dato";
            nuevoCmb.Items.Add(valorCombo0);

       
            ListItem valorCombo1 = new ListItem();
            valorCombo1.Value = "1";
            valorCombo1.Text = "Entero";
            nuevoCmb.Items.Add(valorCombo1);

            ListItem valorCombo2 = new ListItem();
            valorCombo2.Value = "2";
            valorCombo2.Text = "Alfanumérico";
            nuevoCmb.Items.Add(valorCombo2);

            ListItem valorCombo3 = new ListItem();
            valorCombo3.Value = "3";
            valorCombo3.Text = "Fecha";
            nuevoCmb.Items.Add(valorCombo3);

            ListItem valorCombo4 = new ListItem();
            valorCombo4.Value = "4";
            valorCombo4.Text = "Verdadero/Falso";
            nuevoCmb.Items.Add(valorCombo4);

            ListItem valorCombo5 = new ListItem();
            valorCombo5.Value = "5";
            valorCombo5.Text = "Lista de Seleccion";
            nuevoCmb.Items.Add(valorCombo5);

            ListItem valorCombo6 = new ListItem();
            valorCombo6.Value = "6";
            valorCombo6.Text = "Decimal";
            nuevoCmb.Items.Add(valorCombo6);

            ListItem valorCombo7 = new ListItem();
            valorCombo7.Value = "7";
            valorCombo7.Text = "Municipio";
            nuevoCmb.Items.Add(valorCombo7);

            //nuevoCmb.Items.Add("Decimal");
            //nuevoCmb.Items.Add("Alfanumérico");
            //nuevoCmb.Items.Add("Fecha");
            //nuevoCmb.Items.Add("Verdadero/Falso");
            //nuevoCmb.Items.Add("Lista de Seleccion");
            nuevoCmb.SelectedIndex = 0;


            arregloCombos = new DropDownList[200];
            Array.Copy(arrCombos, arregloCombos, 200);
            arregloCombos[numeroRegistro] = nuevoCmb;
            Array.Copy(arregloCombos, arrCombos, 200);

            Label labelNombre = new Label();
            labelNombre.ID = "lblNombreC" + numeroRegistro.ToString();
            labelNombre.Text = "Nombre del Campo ";
            arregloLables[numeroRegistro] = labelNombre;

            Label labelTipo = new Label();
            labelTipo.ID = "lblTipoC" + numeroRegistro.ToString();
            labelTipo.Text = " Tipo ";
            arregloLables2[numeroRegistro] = labelTipo;

            //creamos nuestros textbox
            TextBox nuevoTxtLongitud = new TextBox();
            nuevoTxtLongitud.ID = "txtLongitud" + numeroRegistro.ToString();
            nuevoTxtLongitud.Attributes["data-type"] = "number";
            //nuevoTxtLongitud.Attributes["maxlength"] = "3";
            //nuevoTxtLongitud.Attributes["min"] = "1";
            //nuevoTxtLongitud.Attributes["max"] = "199";

            arregloTextBoxsLongitud = new TextBox[200];
            Array.Copy(arrLongitud, arregloTextBoxsLongitud, 200);
            arregloTextBoxsLongitud[numeroRegistro] = nuevoTxtLongitud;
            Array.Copy(arregloTextBoxsLongitud, arrLongitud, 200);
            TextBox nuevoTxtOrden = new TextBox();
            nuevoTxtOrden.ID = "txtOrden" + numeroRegistro.ToString();
            nuevoTxtOrden.Attributes["data-type"] = "number";
            //nuevoTxtOrden.Attributes["maxlength"] = "3";
            //nuevoTxtOrden.Attributes["min"] = "1";
            //nuevoTxtOrden.Attributes["max"] = "199";
            arregloTextBoxsOrden = new TextBox[200];
            Array.Copy(arrOrden, arregloTextBoxsOrden, 200);
            arregloTextBoxsOrden[numeroRegistro] = nuevoTxtOrden;
            Array.Copy(arregloTextBoxsOrden, arrOrden, 200);


            TextBox txtMostrarLabel = new TextBox();
            txtMostrarLabel.ID = "txtMostrarLabel" + numeroRegistro.ToString();
            txtMostrarLabel.Attributes["maxlength"] = "250";
            arregloTextBoxsNombre = new TextBox[200];
            Array.Copy(arrNombre, arregloTextBoxsNombre, 200);
            arregloTextBoxsNombre[numeroRegistro] = txtMostrarLabel;
            Array.Copy(arregloTextBoxsNombre, arrNombre, 200);

            CheckBox CheckShowTable = new CheckBox();
            CheckShowTable.ID = "checkShowTable" + numeroRegistro.ToString();
            CheckShowTable.Text = " ¿Filtrar por este Campo? ";

            arregloCheckBox = new CheckBox[200];
            Array.Copy(arrcheck, arregloCheckBox, 200);
            arregloCheckBox[numeroRegistro] = CheckShowTable;
            Array.Copy(arregloCheckBox, arrcheck, 200);



            Label LabelLogitud = new Label();
            LabelLogitud.ID = "lbllongitudC" + numeroRegistro.ToString();
            LabelLogitud.Text = " Longitud ";
            arregloLables3[numeroRegistro] = LabelLogitud;

            Label LabelTituloCampo = new Label();
            LabelTituloCampo.ID = "lblTituloC" + numeroRegistro.ToString();
            LabelTituloCampo.Text = " Titulo a Mostrar ";
            arregloLables4[numeroRegistro] = LabelTituloCampo;

            Label LabelOrden = new Label();
            LabelOrden.ID = "lblOrdenC" + numeroRegistro.ToString();
            LabelOrden.Text = " Orden ";
            arregloLables5[numeroRegistro] = LabelOrden;


            Label labelCheck = new Label();
            labelCheck.ID = "lblShowTable" + numeroRegistro.ToString();
            labelCheck.Text = " ¿Filtrar por este Campo? ";
            arregloLablesCheck[numeroRegistro] = labelCheck;


           

            ///agregamos los controles al panel y tabla
            ///NombreCampo, Combo, Label nuevo, label Combo, label Longitud, longitud, 
            AgregarControles(TxtNombreCampo, nuevoCmb, labelNombre, labelTipo, LabelLogitud, nuevoTxtLongitud, LabelTituloCampo, LabelOrden, nuevoTxtOrden, txtMostrarLabel, labelCheck, CheckShowTable);
            this.NumberOfControls++;

            lblError.Text = string.Empty;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }



    }

    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        if (ftnValidarCampos_bol())
        {
            //ftnInactivarControles(false);
            //pnlEliminacion.Visible = true;
            Session["Operacion"] = "Crear";
            if (Session["Operacion"] != null)
            {
                if (Session["Operacion"].ToString() == "Crear")
                {

                    int intCantidadControles = this.NumberOfControls;
                    List<Controles> ListaCampos = new List<Controles>();
                    string strCamposACrear = string.Empty;
                    string strTipoDeDato = string.Empty;
                    string strTipoDeDatoAux = string.Empty;
                    string strLongitud = string.Empty;
                    string strTabla = string.Empty;
                    strTabla = "tblU_" + txtTabla.Text.Trim();
                    strTabla.Replace(" ", string.Empty);


                    arregloTextBoxs = new TextBox[200];
                    Array.Copy(arrTextboxs, arregloTextBoxs, 200);

                    arregloCombos = new DropDownList[200];
                    Array.Copy(arrCombos, arregloCombos, 200);

                    arregloTextBoxsLongitud = new TextBox[200];
                    Array.Copy(arrLongitud, arregloTextBoxsLongitud, 200);

                    arregloTextBoxsOrden = new TextBox[200];
                    Array.Copy(arrOrden, arregloTextBoxsOrden, 200);

                    arregloTextBoxsNombre = new TextBox[200];
                    Array.Copy(arrNombre, arregloTextBoxsNombre, 200);

                    arregloCheckBox = new CheckBox[200];
                    Array.Copy(arrcheck, arregloCheckBox, 200);

                    if (intCantidadControles > 0)
                    {
                        for (int i = 0; i < intCantidadControles; i++)
                        {
                            Controles _control = new Controles();
                            strTipoDeDatoAux = arregloCombos[i].Text;
                            strLongitud = arregloTextBoxsLongitud[i].Text.Trim();
                            _control.NombreCampo = arregloTextBoxs[i].Text.Trim();
                            _control.Label = arregloTextBoxsNombre[i].Text;
                            if (arregloTextBoxsLongitud[i].Text.Trim() == string.Empty)
                            {
                                _control.Orden = 199;
                            }
                            else
                            {
                                _control.Orden = Convert.ToInt16(arregloTextBoxsOrden[i].Text.Trim());
                            }

                            if (arregloTextBoxsLongitud[i].Text.Trim() == string.Empty)
                            {
                                _control.LongitudMax = 200;
                            }
                            else
                            {
                                _control.LongitudMax = Convert.ToInt32(arregloTextBoxsLongitud[i].Text.Trim());
                            }

                            _control.TipoControl = Convert.ToInt16(arregloCombos[i].SelectedValue);
                            _control.idTabla = 0;
                            _control.checkShowTable = arregloCheckBox[i].Checked;
                            if (strTipoDeDatoAux == "0" || strTipoDeDatoAux == "2" || strTipoDeDatoAux == "5")
                            {
                                strTipoDeDato = "[nvarchar]";


                                if (strLongitud != string.Empty)
                                {
                                    strTipoDeDato = strTipoDeDato + " (" + strLongitud + ") ";
                                }
                                else
                                {
                                    strTipoDeDato = strTipoDeDato + " (1000) ";
                                }

                            }

                            if (strTipoDeDatoAux == "1")
                            {

                                strTipoDeDato = "[int]";
                            }
                            if (strTipoDeDatoAux == "6")
                            {

                                strTipoDeDato = "[decimal](18, 2)";
                            }
                            if (strTipoDeDatoAux == "3")
                            {

                                strTipoDeDato = "[datetime]";
                            }
                            if (strTipoDeDatoAux == "4")
                            {

                                strTipoDeDato = "[bit]";
                            }

                            if (strTipoDeDatoAux == "7") //TIPO MUNICIPIO
                            {

                                string nombreCampoDepartamento = "idDepartamentoRelacion";
                                string tipoCampoDepartamento = "[nvarchar](100)";
                                strTipoDeDato = "[nvarchar](100)";
                                strCamposACrear = strCamposACrear + txtAbreviatura.Text.Trim() + "_" + nombreCampoDepartamento + " " + tipoCampoDepartamento + " NULL, ";
                                Controles controlAdicional = new Controles();
                                controlAdicional.idTabla = 0;
                                controlAdicional.Label = "Departamento";
                                controlAdicional.LongitudMax = 100;
                                controlAdicional.NombreCampo = "idDepartamentoRelacion";
                                controlAdicional.Orden = Convert.ToInt16(arregloTextBoxsOrden[i].Text.Trim());
                                controlAdicional.TipoControl = 8; //TIPO DEPARTAMENTO

                                ListaCampos.Add(controlAdicional);
                            }

                            strCamposACrear = strCamposACrear + txtAbreviatura.Text.Trim() + "_" + arregloTextBoxs[i].Text.Trim() + " " + strTipoDeDato + " NULL, ";
                            ListaCampos.Add(_control);
                        }

                        if (checkRelVictima.Checked)
                        {
                            strCamposACrear = strCamposACrear + txtAbreviatura.Text.Trim() + "_IdVictimaRel " + "[nvarchar] (30) " + " NULL, ";

                        }

                        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                        {
                            if (blPaicma.fntCrearTablaNueva_bol(strTabla.Replace(" ", string.Empty), strCamposACrear))
                            {
                                if (blPaicma.fntInsertarFormAdmTab_bol(strTabla, txtAbreviatura.Text.Trim(), this.NumberOfControls, Convert.ToInt32(Session["IdUsuario"].ToString()), txtObservacion.Text.Trim(), checkRelVictima.Checked, Convert.ToInt32(ddlOpcionesFiltroDep.SelectedValue)))
                                {
                                    if (blPaicma.fntConsultaIdTablaCreada("strDsIdTablaCreada", strTabla))
                                    {
                                        if (blPaicma.myDataSet.Tables["strDsIdTablaCreada"].Rows.Count > 0)
                                        {
                                            string IdTbl = blPaicma.myDataSet.Tables["strDsIdTablaCreada"].Rows[0]["admTab_Id"].ToString();
                                            if (fntInsertarFormCampos_bol(ListaCampos, Convert.ToInt32(IdTbl)))
                                            {
                                                lblRespuesta.Text = "Formulario creado satisfactoriamente.";
                                                //}
                                            }
                                            else {
                                                lblRespuesta.Text = "La tabla fisica del formulario fue creada, pero ocurrió un problema al insertar en el maestro de campos.";
                                            };
                                        }
                                        else {
                                            lblRespuesta.Text = "La tabla fisica del formulario fue creada, pero ocurrió un problema al insertar en el maestro de tablas.";
                                        };
                                    }
                                    else {
                                        lblRespuesta.Text = "La tabla fisica del formulario fue creada, pero ocurrió un problema al consultar su indexado en el maestro tablas.";
                                    }
                                }
                                else {
                                    lblRespuesta.Text = "La tabla fisica del formulario fue creada, pero ocurrió un problema al insertar en el maestro de tablas.";
                                };
                            }
                            else
                            {
                                lblRespuesta.Text = "Ocurrió un problema al crear la tabla del formulario fisicamente, revise que no exista todavia de lo contrario por favor contactar al administrador, Gracias.";
                            }
                            pnlRespuesta.Visible = true;
                            blPaicma.Termina();
                        }
                    }
                }
            }
            else
            {
                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    if (blPaicma.fntEliminarTablaNueva_bol(txtTabla.Text.Trim()))
                    {


                        if (blPaicma.fntEliminarFilaAdmTab_bol(Convert.ToInt32(Session["intIdTabla"].ToString())))
                        {
                            lblRespuesta.Text = "Tabla Eliminada satisfactoriamente.";
                        }
                    }
                    else
                    {
                        lblRespuesta.Text = "Problemas al eliminar la tabla, por favor contáctese con el administrador.";
                    }
                    //ftnInactivarControles(false);
                    pnlRespuesta.Visible = true;
                    blPaicma.Termina();
                }
            }
            lblEliminacion.Text = "¿Está seguro de crear la tabla " + txtTabla.Text.Trim() + "?";
        }
        else
        {
            //no paso la validacion
        }

    }


  

    //protected void btnNo_Click(object sender, EventArgs e)
    //{
    //    if (Session["Operacion"] != null)
    //    {
    //        if (Session["Operacion"].ToString() == "Crear")
    //        {
    //            ftnInactivarControles(false);
    //            pnlAccionesDinamico.Visible = true;
    //            pnlCamposTabla.Visible = true;
    //        }
    //    }
    //    else
    //    {
    //        ftnInactivarControles(false);
    //        pnlAccionesDinamico.Visible = true;
    //        pnlCamposTabla.Visible = true;
    //        pnlCampos.Visible = true;
    //    }
    //}

    //protected void btnSi_Click(object sender, EventArgs e)
    //{
    //    if (Session["Operacion"] != null)
    //    {
    //        if (Session["Operacion"].ToString() == "Crear")
    //        {

    //            int intCantidadControles = contadorControles;
    //            List<Controles> ListaCampos = new List<Controles>();
    //            string strCamposACrear = string.Empty;
    //            string strTipoDeDato = string.Empty;
    //            int idTipoDato = 0;
    //            string strTipoDeDatoAux = string.Empty;
    //            string strLongitud = string.Empty;
    //            string strSw;
    //            string strTabla = string.Empty;
    //            strTabla = "tblU_" + txtTabla.Text.Trim();
    //            strTabla.Replace(" ", string.Empty);
    //            if (intCantidadControles > 0)
    //            {
    //                for (int i = 0; i < intCantidadControles; i++)
    //                {
    //                    Controles _control = new Controles();
    //                    strTipoDeDatoAux = arregloCombos[i].Text;
    //                    strLongitud = arregloTextBoxsLongitud[i].Text.Trim();
    //                    _control.NombreCampo = arregloTextBoxs[i].Text.Trim();
    //                    _control.Label = arregloTextBoxsNombre[i].Text;
    //                    if (arregloTextBoxsLongitud[i].Text.Trim() == string.Empty)
    //                    {
    //                        _control.Orden = 199;
    //                    }
    //                    else
    //                    {
    //                        _control.Orden = Convert.ToInt16(arregloTextBoxsOrden[i].Text.Trim());
    //                    }
                       
    //                    if (arregloTextBoxsLongitud[i].Text.Trim() == string.Empty) {
    //                        _control.LongitudMax = 200;
    //                    }
    //                    else
    //                    {
    //                        _control.LongitudMax = Convert.ToInt32(arregloTextBoxsLongitud[i].Text.Trim());
    //                    }
  
    //                    _control.TipoControl = Convert.ToInt16(arregloCombos[i].SelectedValue);
    //                    _control.idTabla = 0;
    //                    _control.checkShowTable = arregloCheckBox[i].Checked;
    //                    //if (arregloCheck[i].Checked){
    //                    //    _control.RelVictima = true;
    //                    //}else{
    //                    //    _control.RelVictima = false;
    //                    //}
                        
                      


    //                    ////AgregarControles(TextBox txtnombreCampo, DropDownList ddlTipo, Label lblNombre, Label lblTipo, Label lblLongitud, TextBox txtLongitud, Label lblTitulo, Label lblOrden, TextBox txtOrden, TextBox txtLabel)
    //                    //AgregarControles(arregloTextBoxs[i], arregloCombos[i], arregloLables[i], arregloLables2[i], arregloLables3[i], arregloTextBoxsLongitud[i], arregloLables4[i], arregloLables5[i], arregloTextBoxsOrden[i], arregloTextBoxsNombre[i]);
                       
    //                    //_control.Orden


    //                    if (strTipoDeDatoAux == "0" || strTipoDeDatoAux == "2" || strTipoDeDatoAux == "5")
    //                    {
    //                        strTipoDeDato = "[nvarchar]";
                         

    //                        if (strLongitud != string.Empty)
    //                        {
    //                            strTipoDeDato = strTipoDeDato + " (" + strLongitud + ") ";
    //                        }
    //                        else
    //                        {
    //                            strTipoDeDato = strTipoDeDato + " (1000) ";
    //                        }
    //                    }

    //                    if (strTipoDeDatoAux == "1")
    //                    {
    //                        strTipoDeDato = "[int]";
    //                    }
    //                    if (strTipoDeDatoAux == "6")
    //                    {
                         
    //                        strTipoDeDato = "[decimal](18, 2)";
    //                    }
    //                    if (strTipoDeDatoAux == "3")
    //                    {
                        
    //                        strTipoDeDato = "[datetime]";
    //                    }
    //                    if (strTipoDeDatoAux == "4")
    //                    {
                          
    //                        strTipoDeDato = "[bit]";
    //                    }
                  
    //                    strCamposACrear = strCamposACrear + txtAbreviatura.Text.Trim() + "_" + arregloTextBoxs[i].Text.Trim() + " " + strTipoDeDato + " NULL, ";
    //                    ListaCampos.Add(_control);
    //                }

    //                if (checkRelVictima.Checked)
    //                {
    //                    strCamposACrear = strCamposACrear + txtAbreviatura.Text.Trim() + "_IdVictimaRel " + "[nvarchar] (30) " + " NULL, ";

    //                }
                    
    //                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
    //                {
    //                    if (blPaicma.fntCrearTablaNueva_bol(strTabla.Replace(" ",string.Empty), strCamposACrear))
    //                    {
    //                        if (blPaicma.fntInsertarFormAdmTab_bol(strTabla, txtAbreviatura.Text.Trim(), this.NumberOfControls, Convert.ToInt32(Session["IdUsuario"].ToString()), txtObservacion.Text.Trim(), checkRelVictima.Checked, Convert.ToInt32(ddlOpcionesFiltroDep.SelectedValue)))
    //                        {
    //                            if(blPaicma.fntConsultaIdTablaCreada("strDsIdTablaCreada",strTabla)){
    //                                 if (blPaicma.myDataSet.Tables["strDsIdTablaCreada"].Rows.Count > 0)
    //                                    {
    //                                        string IdTbl  = blPaicma.myDataSet.Tables["strDsIdTablaCreada"].Rows[0]["admTab_Id"].ToString();
    //                                        if (fntInsertarFormCampos_bol(ListaCampos, Convert.ToInt32(IdTbl)))
    //                                        {
    //                                            lblRespuesta.Text = "Tabla creada satisfactoriamente.";
    //                                            //}
    //                                        }
    //                                        else { };
    //                                    }
    //                                    else { };
    //                            }else{}

                                
    //                         }else{};
    //                    }
    //                    else
    //                    {
    //                            lblRespuesta.Text = "Problemas al crear la Tabla, por favor contactar al administrador, Gracias.";
    //                    }
    //                    //ftnInactivarControles(false);
    //                    pnlRespuesta.Visible = true;
    //                    blPaicma.Termina();
    //                   }
    //             }
    //        }
    //    }
    //    else
    //    {
    //        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
    //        {
    //            if (blPaicma.fntEliminarTablaNueva_bol(txtTabla.Text.Trim()))
    //            {

                    
    //                if (blPaicma.fntEliminarFilaAdmTab_bol(Convert.ToInt32(Session["intIdTabla"].ToString())))
    //                {
    //                    lblRespuesta.Text = "Tabla Eliminada satisfactoriamente.";
    //                }
    //            }
    //            else
    //            {
    //                lblRespuesta.Text = "Problemas al eliminar la tabla, por favor contáctese con el administrador.";
    //            }
    //            ftnInactivarControles(false);
    //            pnlRespuesta.Visible = true;
    //            blPaicma.Termina();
    //        }
    //    }
    //}


    
    public bool fntInsertarFormCampos_bol(List<Controles> FormControles, int idTabla)
    {

        bool respuesta=false;
        foreach (Controles element in FormControles)
        {
            if (blPaicma.fntInsertarFormCampos_bol(element, idTabla)) {
                respuesta = true;
            }
            else
            {
                respuesta = false;
                break;
            }
        }
        return respuesta;
    }


    public Boolean ftnValidarCampos_bol()
    {

        if (txtTabla.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar el nombre de la tabla.";
            return false;
        }

        if (txtAbreviatura.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar la abreviatura de la tabla.";
            return false;
        }

        if (txtObservacion.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar una observación sobre la tabla.";
            return false;
        }

        if (ddlOpcionesFiltroDep.SelectedValue.ToString() == "Seleccione...")
        {
            lblError.Text = "Debe indicar un filtro por departamento.";
            return false;
        }
     

        //int intCantidadControles = contadorControles;
        int intCantidadControles = this.NumberOfControls;
        int intContadorInterno = 0;
        string strCamposACrear = string.Empty;

        if (intCantidadControles > 0)
        {
            for (int i = 0; i < intCantidadControles; i++)
            {
                strCamposACrear = arregloTextBoxs[i].Text.Trim();
                if (strCamposACrear != string.Empty)
                {
                    intContadorInterno++;
                }
                else
                {
                    i = intCantidadControles;
                }
            }
            //contadorControles = intContadorInterno;
        }

        if (intContadorInterno < 3)
        {
            lblError.Text = "Mínimo Debe crear Tres(3) campos en la tabla.";
            return false;
        }


        lblError.Text = string.Empty;
        return true;
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        string strRuta = "frmFormDinamico.aspx";
        Response.Redirect(strRuta);
    }

    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        ftnInactivarControles(false);
        pnlAccionesDinamico.Visible = true;
        pnlBusqueda.Visible = true;
        imgbEncontrar.Visible = true;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = false;
        imgbCancelar.Visible = true;
        ftnLimpiarControles();
    }

    public void ftnLimpiarControles()
    {
        txtTablaBusqueda.Text = string.Empty;
        ddlEstadoBusqueda.SelectedIndex = 0;
    }

    protected void imgbEncontrar_Click(object sender, ImageClickEventArgs e)
    {
        int intIdEstadoActivacion = 0;
        intIdEstadoActivacion = Convert.ToInt32(ddlEstadoBusqueda.SelectedValue.ToString());
        fntCargarGrillaTablas(intIdEstadoActivacion, 1, txtTablaBusqueda.Text.Trim());
        ftnInactivarControles(false);
        pnlMenuDinamico.Visible = true;
        PnlTablasGrilla.Visible = true;
    }

    protected void gvTablas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        gvTablas.Columns[0].Visible = true;
        if (e.CommandName == "Seleccion")
        {
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvTablas.Rows[intIndex];
            TableCell Item = selectedRow.Cells[0];

            TableCell Item1 = selectedRow.Cells[1];
            TableCell Item2 = selectedRow.Cells[2];
            TableCell Item3 = selectedRow.Cells[5];
            int intIdTabla = Convert.ToInt32(Item.Text);
            string NombreTabla = "tblU_" + Item1.Text;
            string Abreviatura = Item2.Text;
            string Observacion = Item3.Text;
            Session["intIdTabla"] = Convert.ToInt32(intIdTabla);
            Session["NombreTablaCAM"] = NombreTabla;
            Session["AbreviaturaCAM"] = CleanInput(Abreviatura);
            Session["ObservacionCAM"] = CleanInput(Observacion);
            imgbEditar.Visible = true;
        }
        gvTablas.Columns[0].Visible = false;
    }


    protected void gvCampos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        gvCampos.Columns[0].Visible = true;
        if (e.CommandName == "Seleccion")
        {
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow;
            foreach (GridViewRow campo in gvCampos.Rows)
            {
                if (campo.DataItemIndex == intIndex)
                {
                    selectedRow = campo;
                    TableCell Item = selectedRow.Cells[0];
                    int intNomTabCampos = Convert.ToInt32(Item.Text);
                    Session["intNomTabCampos"] = intNomTabCampos;
                }

            }
            
            imgbEditar.Visible = true;
            string strRuta = "frmDropdownCreate.aspx";
            Response.Redirect(strRuta);
        }
        gvTablas.Columns[0].Visible = false;
    }

    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            //if (blPaicma.fntConsultaAdmTab_bol("strDsGrillaCampos", 0, 2, string.Empty, Convert.ToInt32(Session["intIdTabla"].ToString())))
            //{
            if (blPaicma.fntConsultaListadoControles("strDsGrillaCampos", Convert.ToInt32(Session["intIdTabla"].ToString())))
             {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ftnInactivarControles(false);
                    pnlAccionesDinamico.Visible = true;
                    pnlCamposTabla.Visible = true;
                    pnlCampos.Visible = true;
                    imgbEncontrar.Visible = false;
                    imgbGravar.Visible = false;
                    gvCampos.DataMember = "strDsGrillaCampos";
                    gvCampos.DataSource = blPaicma.myDataSet;
                    gvCampos.DataBind();
                    lblGVcampos.Text = string.Empty;
                    txtTabla.Text = Session["NombreTablaCAM"].ToString();
                    txtAbreviatura.Text = Session["AbreviaturaCAM"].ToString();
                    txtObservacion.Text = Session["ObservacionCAM"].ToString();
                    txtTabla.Enabled = false;
                    txtAbreviatura.Enabled = false;
                    txtObservacion.Enabled = false;
                    Label1.Visible = false;
                    checkRelVictima.Visible = false;
                    Label2.Visible = false;
                    Label4.Visible = false;
                    ddlOpcionesFiltroDep.Visible = false;
                    ddlOpcionesFiltroOrg.Visible = false;
                    mensajeCrear.Visible = false;
                    btnConfigPermisos.Visible = true;
                }
                else
                {
                    gvCampos.DataMember = "strDsGrillaCampos";
                    gvCampos.DataSource = blPaicma.myDataSet;
                    gvCampos.DataBind();
                    lblGVcampos.Text = "No hay registros que coincidan con esos criterios.";
                }
               // ftnValidarPermisos();
            }
            blPaicma.Termina();
        }
    }

    protected void imgbEliminar_Click(object sender, ImageClickEventArgs e)
    {
        Session["Operacion"] = null;
        int intCantidadRegistrosTablaDinamica = 0;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaCantidadRegistrosTablaDinamica("strDsCantidadRegistrosTablaDinamica", txtTabla.Text.Trim()))
            {
                intCantidadRegistrosTablaDinamica = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                if (intCantidadRegistrosTablaDinamica > 0)
                {
                    ftnInactivarControles(false);
                    pnlRespuesta.Visible = true;
                    lblRespuesta.Text = "No se puede eliminar la tabla porque ya tiene registros almacenados.";
                }
                else
                {
                    ftnInactivarControles(false);
                    pnlEliminacion.Visible = true;
                    lblEliminacion.Text = "¿Está seguro de eliminar el registro y su archivo adjunto?";
                }

            }
            blPaicma.Termina();
        }
    }

    public string CleanInput(string strIn)
    {
        // Replace invalid characters with empty strings. 
       
            return Regex.Replace(strIn, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);

            //return Regex.Replace(strIn, @"[^\w\.@-]", "", RegexOptions.None, TimeSpan.FromSeconds(1.5));
       
    }

    protected void gvTablas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTablas.PageIndex = e.NewPageIndex;
        fntCargarGrillaTablas(1, 0, string.Empty);
    }

    protected void gvCampos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCampos.PageIndex = e.NewPageIndex;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            //if (blPaicma.fntConsultaAdmTab_bol("strDsGrillaCampos", 0, 2, string.Empty, Convert.ToInt32(Session["intIdTabla"].ToString())))
            if (blPaicma.fntConsultaListadoControles("strDsGrillaCampos", Convert.ToInt32(Session["intIdTabla"].ToString())))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvCampos.DataMember = "strDsGrillaCampos";
                    gvCampos.DataSource = blPaicma.myDataSet;
                    gvCampos.DataBind();
                    lblGVcampos.Text = string.Empty;
                }
            }
            blPaicma.Termina();
        }
    }
    protected Boolean IsLista(string cadena)
    {
        if (cadena == "Lista")
        {
            return true;
        }else{
            return false;
        }
        
    }

    public void fntCargaPrimerFiltro()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaFiltroPrimer("opcionesFiltroPrimario"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlOpcionesFiltroOrg.DataMember = "opcionesFiltroPrimario";
                    ddlOpcionesFiltroOrg.DataSource = blPaicma.myDataSet;
                    ddlOpcionesFiltroOrg.DataValueField = "id";
                    ddlOpcionesFiltroOrg.DataTextField = "primerFil_Descripcion";
                    ddlOpcionesFiltroOrg.DataBind();
                    ddlOpcionesFiltroOrg.Items.Insert(ddlOpcionesFiltroOrg.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargaSegundoFiltro(string primerFiltro)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaSegundoFiltro("opcionesFiltroSecundario", primerFiltro))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlOpcionesFiltroDep.DataMember = "opcionesFiltroSecundario";
                    ddlOpcionesFiltroDep.DataSource = blPaicma.myDataSet;
                    ddlOpcionesFiltroDep.DataValueField = "id";
                    ddlOpcionesFiltroDep.DataTextField = "segundoFil_Descripcion";
                    ddlOpcionesFiltroDep.DataBind();
                    ddlOpcionesFiltroDep.Items.Insert(ddlOpcionesFiltroDep.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    protected void ddlOpcionesFiltroOrg_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlOpcionesFiltroDep.SelectedValue != "Seleccione...") { 
        fntCargaSegundoFiltro(ddlOpcionesFiltroOrg.SelectedValue);
        ddlOpcionesFiltroDep.Enabled = true;
        }
        else
        {
            ddlOpcionesFiltroDep.Items.Clear();

        }
    }

    protected void btnConfigPermisos_Click(object sender, EventArgs e)
    {
        string strRuta = "frmPermisosUsuarioFormulario.aspx";
        Response.Redirect(strRuta);
    }
}