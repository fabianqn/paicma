﻿using ASP;
using System;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmGrupoOperador : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }
            this.ftnValidarPermisos();
            this.fntCargarGrillaGrupoOperador(0, 0, string.Empty, string.Empty, 0);
        }
    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        this.blPaicma = null;
    }
    public void ftnValidarPermisos()
    {
        string a = string.Empty;
        string a2 = string.Empty;
        string a3 = string.Empty;
        string a4 = string.Empty;
        string[] array = HttpContext.Current.Request.RawUrl.Split(new char[]
		{
			'/'
		});
        string text = array[array.GetUpperBound(0)];
        array = text.Split(new char[]
		{
			'?'
		});
        text = array[array.GetLowerBound(0)];
        if (text != string.Empty)
        {
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (this.blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(this.Session["IdUsuario"].ToString()), text) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    a = this.blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                    a2 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                    a3 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                    a4 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();
                    if (a == "False")
                    {
                        this.imgbBuscar.Visible = false;
                    }
                    if (a2 == "False")
                    {
                        this.imgbNuevo.Visible = false;
                    }
                    if (a3 == "False")
                    {
                        this.imgbEditar.Visible = false;
                        this.imgbGravar.Visible = false;
                    }
                    if (a4 == "False")
                    {
                        this.imgbEliminar.Visible = false;
                    }
                }
                this.blPaicma.Termina();
                return;
            }
        }
        else
        {
            base.Response.Redirect("@dmin/frmGrupoOperador.aspx");
        }
    }
    public void fntInactivarPaneles(bool bolValor)
    {
        this.pnlAccionesGrupoOperador.Visible = bolValor;
        this.pnlEliminacion.Visible = bolValor;
        this.PnlGrupoOperadorGrilla.Visible = bolValor;
        this.pnlGrupoOperadorNuevo.Visible = bolValor;
        this.pnlMenuGrupoOperador.Visible = bolValor;
        this.pnlRespuesta.Visible = bolValor;
    }
    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlAccionesGrupoOperador.Visible = true;
        this.imgbEncontrar.Visible = false;
        this.imgbEliminar.Visible = false;
        this.imgbGravar.Visible = true;
        this.imgbCancelar.Visible = true;
        this.pnlGrupoOperadorNuevo.Visible = true;
        this.fntCargarObjetos();
        this.Session["Operacion"] = "Crear";
    }
    public void fntCargarObjetos()
    {
        this.fntCargarEstado();
    }
    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        string url = "frmGrupoOperador.aspx";
        base.Response.Redirect(url);
    }
    public void fntCargarGrillaGrupoOperador(int intOp, int intIdGrupoOperador, string strNombreGrupo, string strDescripcionGrupo, int intIdEstado)
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaGrupoOperador_bol("strDsGrillaGrupoOperador", intOp, intIdGrupoOperador, strNombreGrupo, strDescripcionGrupo, intIdEstado))
            {
                this.gvGrupoOperador.Columns[0].Visible = true;
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gvGrupoOperador.DataMember = "strDsGrillaGrupoOperador";
                    this.gvGrupoOperador.DataSource = this.blPaicma.myDataSet;
                    this.gvGrupoOperador.DataBind();
                    this.lblErrorGv.Text = string.Empty;
                }
                else
                {
                    this.gvGrupoOperador.DataMember = "strDsGrillaGrupoOperador";
                    this.gvGrupoOperador.DataSource = this.blPaicma.myDataSet;
                    this.gvGrupoOperador.DataBind();
                    this.lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                this.gvGrupoOperador.Columns[0].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarEstado()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaEstadoTipoUsuario("strDsEstado") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlEstado.DataMember = "strDsEstado";
                this.ddlEstado.DataSource = this.blPaicma.myDataSet;
                this.ddlEstado.DataValueField = "est_Id";
                this.ddlEstado.DataTextField = "est_Descripcion";
                this.ddlEstado.DataBind();
                this.ddlEstado.Items.Insert(this.ddlEstado.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        if (this.Session["Operacion"].ToString() != "Crear")
        {
            if (this.Session["Operacion"].ToString() == "Actualizar" && this.ftnValidarCampos_bol() && this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (this.blPaicma.fntModificarGrupoOperador_bol(Convert.ToInt32(this.Session["intIdGrupoOperador"].ToString()), this.txtNombreGrupo.Text.Trim(), this.txtDescripcion.Text.Trim(), Convert.ToInt32(this.ddlEstado.SelectedValue.ToString())))
                {
                    this.fntInactivarPaneles(false);
                    this.pnlRespuesta.Visible = true;
                    this.lblRespuesta.Text = "Registro actualizado satisfactoriamente";
                }
                else
                {
                    this.fntInactivarPaneles(false);
                    this.pnlRespuesta.Visible = true;
                    this.lblRespuesta.Text = "Problema al actualizar el registro, por favor comuníquese con el administrador.";
                }
                this.blPaicma.Termina();
            }
            return;
        }
        if (!this.ftnValidarCampos_bol())
        {
            this.lblError.Visible = true;
            return;
        }
        if (!this.fntValidarExistenciaGrupo())
        {
            this.lblError.Visible = true;
            return;
        }
        if (this.ftnIngresarGrupoOperador())
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.fntInactivarPaneles(false);
            this.pnlRespuesta.Visible = true;
            this.lblRespuesta.Text = "Grupo creado satisfactoriamente";
            return;
        }
        this.lblError.Visible = true;
    }
    public bool ftnValidarCampos_bol()
    {
        if (this.txtNombreGrupo.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar el nombre del grupo.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtNombreGrupo.Text.Trim()))
        {
            this.lblError.Text = "Nombre de grupo inválido, ingrese uno diferente, si el problema persiste comuníquese con su administrador.";
            return false;
        }
        if (this.txtDescripcion.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar la descripción del grupo.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtDescripcion.Text.Trim()))
        {
            this.lblError.Text = "Descripcion Inválida, ingrese una diferente, si el problema persiste comuníquese con su administrador.";
            return false;
        }
        if (this.ddlEstado.SelectedValue == "Seleccione...")
        {
            this.lblError.Text = "Debe seleccionar el estado del usuario.";
            return false;
        }
        return true;
    }
    public bool ftnValidarSeguridadCampos_bol(string strTextoCampo)
    {
        bool result = false;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntEliminarTablaExpresionCampo_bol() && this.blPaicma.fntIngresarExpresionCampo_bol(strTextoCampo) && this.blPaicma.fntConsultaExpresionSeguridad("strDsSeguridadCampos", strTextoCampo))
            {
                int num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                result = (num <= 0);
            }
            this.blPaicma.Termina();
        }
        return result;
    }
    public bool fntValidarExistenciaGrupo()
    {
        bool result = false;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaExistenciaGrupoOperador_bol("strDsUsuario", this.txtNombreGrupo.Text.Trim()))
            {
                int num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                if (num > 0)
                {
                    result = false;
                    this.lblError.Text = "Ya existe un grupo registrado, por favor verificar.";
                }
                else
                {
                    result = true;
                }
            }
            this.blPaicma.Termina();
        }
        return result;
    }
    public bool ftnIngresarGrupoOperador()
    {
        bool result = false;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntIngresarGrupoOperador_bol(this.txtNombreGrupo.Text.Trim(), this.txtDescripcion.Text.Trim(), Convert.ToInt32(this.Session["IdUsuario"].ToString()), Convert.ToInt32(this.ddlEstado.SelectedValue.ToString())))
            {
                result = true;
            }
            else
            {
                result = false;
                this.lblError.Text = "Problema al ingresar el registro, por favor comuníquese con el administrador.";
            }
            this.blPaicma.Termina();
        }
        return result;
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        string url = "frmGrupoOperador.aspx";
        base.Response.Redirect(url);
    }
    protected void gvGrupoOperador_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            this.gvGrupoOperador.Columns[0].Visible = true;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow = this.gvGrupoOperador.Rows[index];
            TableCell tableCell = gridViewRow.Cells[0];
            int num = Convert.ToInt32(tableCell.Text);
            this.Session["intIdGrupoOperador"] = num;
            this.imgbEditar.Visible = true;
            this.gvGrupoOperador.Columns[0].Visible = false;
        }
    }
    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlAccionesGrupoOperador.Visible = true;
        this.imgbEncontrar.Visible = false;
        this.imgbGravar.Visible = true;
        this.imgbCancelar.Visible = true;
        this.pnlGrupoOperadorNuevo.Visible = true;
        this.fntCargarObjetos();
        this.Session["Operacion"] = "Actualizar";
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            int intIdGrupoOperador = Convert.ToInt32(this.Session["intIdGrupoOperador"].ToString());
            if (this.blPaicma.fntConsultaGrupoOperador_bol("strDsGrillaGrupoOperador", 1, intIdGrupoOperador, string.Empty, string.Empty, 0) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlEstado.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                this.txtNombreGrupo.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                this.txtDescripcion.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                this.ftnValidarPermisos();
            }
            this.blPaicma.Termina();
        }
    }
    protected void imgbEliminar_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlEliminacion.Visible = true;
        this.lblEliminacion.Text = "¿Está seguro de eliminar el Grupo?";
    }
    protected void btnNo_Click(object sender, EventArgs e)
    {
        this.pnlEliminacion.Visible = false;
        this.pnlAccionesGrupoOperador.Visible = true;
        this.pnlGrupoOperadorNuevo.Visible = true;
    }
    protected void btnSi_Click(object sender, EventArgs e)
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            int num = 0;
            if (this.blPaicma.fntConsultaCantdadDocumentosGrupoOperador_bol("strDsGrupoOperador", Convert.ToInt32(this.Session["intIdGrupoOperador"].ToString())) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
            }
            if (num == 0)
            {
                if (this.blPaicma.fntEliminarGrupoOperador_bol(Convert.ToInt32(this.Session["intIdGrupoOperador"].ToString())))
                {
                    this.lblRespuesta.Text = "Registro eliminado satisfactoriamente.";
                }
                else
                {
                    this.lblRespuesta.Text = "No se puede eliminar el registro ya que tiene información asociada.";
                }
            }
            else
            {
                this.lblRespuesta.Text = "No se puede eliminar el registro ya que tiene información asociada.";
            }
            this.fntInactivarPaneles(false);
            this.pnlRespuesta.Visible = true;
            this.blPaicma.Termina();
        }
    }
    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles(false);
        this.pnlAccionesGrupoOperador.Visible = true;
        this.imgbEncontrar.Visible = true;
        this.imgbEliminar.Visible = false;
        this.imgbGravar.Visible = false;
        this.imgbCancelar.Visible = true;
        this.pnlGrupoOperadorNuevo.Visible = true;
        this.fntCargarObjetos();
        this.fntLimpiarObjetos();
    }
    public void fntLimpiarObjetos()
    {
        this.txtDescripcion.Text = string.Empty;
        this.txtNombreGrupo.Text = string.Empty;
    }
    protected void imgbEncontrar_Click(object sender, ImageClickEventArgs e)
    {
        string strNombreGrupo = string.Empty;
        string strDescripcionGrupo = string.Empty;
        int intIdEstado = 0;
        if (this.txtNombreGrupo.Text.Trim() != string.Empty)
        {
            strNombreGrupo = this.txtNombreGrupo.Text.Trim();
        }
        if (this.txtDescripcion.Text.Trim() != string.Empty)
        {
            strDescripcionGrupo = this.txtDescripcion.Text.Trim();
        }
        if (this.ddlEstado.SelectedValue != "Seleccione...")
        {
            intIdEstado = Convert.ToInt32(this.ddlEstado.SelectedValue.ToString());
        }
        this.fntCargarGrillaGrupoOperador(2, 0, strNombreGrupo, strDescripcionGrupo, intIdEstado);
        this.fntInactivarPaneles(false);
        this.pnlMenuGrupoOperador.Visible = true;
        this.PnlGrupoOperadorGrilla.Visible = true;
    }
    protected void gvGrupoOperador_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvGrupoOperador.Columns[0].Visible = true;
        this.gvGrupoOperador.PageIndex = e.NewPageIndex;
        this.fntCargarGrillaGrupoOperador(0, 0, string.Empty, string.Empty, 0);
        this.gvGrupoOperador.Columns[0].Visible = false;
    }
}