﻿<script type="text/javascript">
        $(function () {
            $(".ddDepartamento").prepend("<option value='0'></option>").val('0');
            $(".ddMunicipio").prepend("<option value='0'></option>").val('0');
            $("#contenido_UpdateButton").click(function () {
                if ($(".campo").val() == "") {
                    alert("rellene o seleccione todos los campos");
                    return false;
 
                };
                if ($(".entero").val() == "") {
                    alert("rellene o seleccione todos los campos");
                    return false;
 
                };
 
                if ($(".decimal").val() == "") {
                    alert("rellene o seleccione todos los campos");
                    return false;
 
                };
 
 
                if ($(".ddMunicipio").val() == "0" || $(".ddMunicipio").val() == "") {
                    alert("rellene o seleccione todos los campos");
                    return false;
                }
 
                if ($(".ddDepartamento").val() == "0" || $(".ddDepartamento").val() == "") {
                    alert("rellene todos los campos");
                    return false;
                }
                if ($(".decimal").length) {
 
                    if (validateDecimal($(".decimal").val())) {
 
                    } else {
                        alert("Algunos Campos declarados decimales, no cumplen con la condicion establecida");
                        return false;
                    }
                }
 
            });
 
            function validateDecimal(dec) {
                var regx = /^\d+\.?\d{0,2}$/g;
                return regx.test(dec);
            }
 
            $(".decimal").blur(function () {
                var $el = $(this);
                var regx = /^\d+\.?\d{0,2}$/g;
                return regx.test($(this).val());
            });
 
            $(".entero").keydown(function (event) {
                // Prevent shift key since its not needed
                if (event.shiftKey == true) {
                    event.preventDefault();
                }
                // Allow Only: keyboard 0-9, numpad 0-9, backspace, tab, left arrow, right arrow, delete
                if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) {
                    // Allow normal operation
                } else {
                    // Prevent the rest
                    event.preventDefault();
                }
            });
 
            $(".pluginFecha").datepicker({dateFormat: 'yy-mm-dd',changeMonth: true, changeYear: true});
 
        
 
            //$("").val("0");
            //$(".ddMunicipio").val("0");
            //$(".ddDepartamento").prepend("<option value='0' selected='selected'></option>");
            //$(".ddMunicipio").prepend("<option value='0' selected='selected'></option>");
 
            $(".ddMunicipio option").each(function (index, val) {
                if ($(this).is('option') && (!$(this).parent().is('span')))
                    $(this).wrap((navigator.appName == 'Microsoft Internet Explorer') ? '<span>' : null).hide();
            });
 
 
            $(".ddDepartamento").change(function () {
 
                var valorDep = $(this).val();
                var ordenDep = $(this).data("orden");
                var id = $(this).attr("id");
                var numero = parseInt(id[id.length - 1],10)+1;
                var idMun = "contenido_comboMunicipio" + numero;
                var optDep;
 
 
                if (valorDep != "0") {
                    $("#" + idMun + " option").each(function (index, val) {
                        var $el = $(this);
                        if ($el.val() == "0") {
                            $el.attr("selected", true);
                        }
                        optDep = $el.data("departamento")
                        if (optDep != valorDep) {
                            if ($(this).is('option') && (!$(this).parent().is('span')))
                                $(this).wrap((navigator.appName == 'Microsoft Internet Explorer') ? '<span>' : null).hide();
                        } else {
                            if (navigator.appName == 'Microsoft Internet Explorer') {
                                if (this.nodeName.toUpperCase() === 'OPTION') {
                                    var span = $(this).parent();
                                    var opt = this;
                                    if ($(this).parent().is('span')) {
                                        $(opt).show();
                                        $(span).replaceWith(opt);
                                    }
                                }
                            } else {
                                $(this).show(); //all other browsers use standard .show()
                            }
                        }
                    });
                } else {
                    $("#" + idMun).val("0");
                    $("#" + idMun + " option").each(function () {
                        if ($(this).is('option') && (!$(this).parent().is('span')))
                            $(this).wrap((navigator.appName == 'Microsoft Internet Explorer') ? '<span>' : null).hide();
                    });
                }
 
            });
        });
 
    </script>