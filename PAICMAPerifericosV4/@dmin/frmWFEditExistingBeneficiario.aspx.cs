﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class _dmin_frmWFEditExistingBeneficiario : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                if (Session["IdBeneficiario"] != null)
                {
                   
                    FullBenefio();
                    //FullBeneficiario();
                }
                else
                {
                    Response.Redirect("~/@dmin/frmLogin.aspx");
                }

            }

        }
    }



    //☻☻ metodo para cargar los  valores del compromiso para editar 
    private void FullBenificiario()
    {

        Int64 idLegalizacion = Convert.ToInt64(Session["IdLegalizacionRef"]);
        Int64 idBeneficioLega = Convert.ToInt64(Session["IdBeneficiario"]);

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarBeneficiarios("datosBenificiario", "id", idLegalizacion, idBeneficioLega))
            {
                if (blPaicma.myDataSet.Tables["datosBenificiario"].Rows.Count > 0)
                {
                    ///☻☻ cambiar el valor seleccionado 
                   ddlBeneficio.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0]["idBeneficio"].ToString();
        
                  txtCantidad.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["cantBeneficiada"].ToString();
                  txtComentario.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["comentario"].ToString();
                }
            }
            else
            {
                //Imposible Consultar lista de planes
                //lblRespuesta.Text = "Se ha presentado un problema al consultar lista de Compromisos, intente de nuevo";
            }
        }
        else
        {
            //No pudo conectar a la base de datos
            //lblRespuesta.Text = "Se ha presentado un problema al consultar lista de Compromisos, no se ha podido conectar a la base de datos";

        }
        blPaicma.Termina();
    }

    //☻☻ evento para guardar los cambios de compromiso
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Int64 idCompromiso = Convert.ToInt64(Session["IdCompromiso"]);
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntActualizarBeneficiario_bol(Convert.ToInt32(ddlBeneficio.SelectedValue), Convert.ToInt32(txtCantidad.Text), txtComentario.Text, Convert.ToInt64(Session["IdBeneficiario"])))
             {
                 Response.Redirect("frmWFBeneficiarios.aspx");
             }
            else
            {

            }
        }
        this.blPaicma.Termina();
    }


    //☻☻ metodo para gargar los valores  en el dropdownlist beneficio
    private void FullBenefio()
    {
        DataTable dataBeneficiarios = new DataTable();

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarPorEstado("datosBeneficiarios", "WFSol_Beneficio", "estatus", "ACT", "nombre"))
            {
                dataBeneficiarios = blPaicma.myDataSet.Tables["datosBeneficiarios"];

                ddlBeneficio.DataSource = dataBeneficiarios;
                ddlBeneficio.DataValueField = "idBeneficio";
                ddlBeneficio.DataTextField = "nombre";
                ddlBeneficio.DataBind();

                ///☻☻ metodo para cargar los valores para editar
                FullBenificiario();
            }
        }

    }

 





}