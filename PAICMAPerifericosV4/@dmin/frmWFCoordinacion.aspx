﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmWFCoordinacion.aspx.cs" Inherits="_dmin_frmWFCoordinacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">
     
    <div class="contacto2">
        <h1>Crear Nueva Coordinación</h1>
        <hr />
        <table style="width:100%">
           <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Nombre: "></asp:Label>
                </td>
                 <td>
                     <asp:TextBox ID="txtNombreCoordinacion" runat="server" Width="100%"></asp:TextBox>
                </td>
                <td style="width:10%"></td>
                 <td>
                      <asp:Label ID="Label1" runat="server" Text="Descripcion: "></asp:Label>
                </td>
                 <td>
                    <asp:TextBox ID="txtDescripcionCoordinacion" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                </td>
            </tr>
      
        </table>
    </div>

     <div class="contacto2" style="width: 95%; float:left" id="funcion" runat="server">
         <h1>Usuarios de la Coordinación: </h1>
       <div style="width:25%; float:left">
           <h2>Disponibles</h2>
           <asp:ListBox Runat="server" ID="ListBoxUsuarios" SelectionMode="Multiple" Height="150px"  Width="100%"></asp:ListBox>
       </div>
       <div style="width:9%; float:left; margin-right: 2px;">
           <h2> &nbsp;</h2>
            <div style="width:100%; height: 73px;">
               <asp:Button Runat="server" ID="btnMoveLeftUser" Text="<<" OnClick="btnMoveLeftUser_Click" Height="75px" Width="83%" ></asp:Button>
            </div>
            <div style="width:100%;">
			    <asp:Button Runat="server" ID="btnMoveRightUser" Text=">>" OnClick="btnMoveRightUser_Click" Height="75px" Width="80%" ></asp:Button>
            </div>
       </div>
       <div style="width:25%; float:left">
           <h2>Usuarios Regulares</h2>
           <asp:ListBox Runat="server" ID="ListBoxUsuariosSelecciondos" SelectionMode="Multiple" Height="150px" Width="100%"  ></asp:ListBox>
       </div>
       <div style="width:10%; float:left;  margin-right: 0px;">
           <h2> &nbsp;</h2>
            <div style="width:100%; height: 73px;">
               <asp:Button Runat="server" ID="btnMoveLeftCoor" Text="<<" OnClick="btnMoveLeftCoor_Click" Height="75px" Width="79%"  ></asp:Button>
            </div>
            <div style="width:100%;">
			    <asp:Button Runat="server" ID="btnMoveRightCoor" Text=">>" OnClick="btnMoveRightCoor_Click" Height="75px" Width="79%"  ></asp:Button>
            </div>
       </div>
       <div style="width:25%; float:left; margin-left: 0px;">
           <h2>Usuarios Coordinadores</h2>
           <asp:ListBox Runat="server" ID="ListBoxUsuariosCoordinadores" SelectionMode="Multiple" Height="150px" Width="100%" ></asp:ListBox>
       </div>
        <asp:Panel ID="panelBotones" runat="server">
         
        <div class="Botones">
            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click"/>
        </div>

          <div style="width:100%">
              <asp:Label ID="lblMensaje" runat="server" Text="Nombre: " Visible="false"></asp:Label>
              <asp:Label ID="lblMensajeAdicional" runat="server" Text="Nombre: " Visible="false"></asp:Label>
          </div>
    </asp:Panel>
    </div>

     

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">
</asp:Content>

