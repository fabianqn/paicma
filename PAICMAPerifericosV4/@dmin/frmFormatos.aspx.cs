﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmFormatos : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                ftnValidarPermisos();
                fntCargarGrillaFormatos();
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }

    public void ftnValidarPermisos()
    {
        //int intIdFormulario = 0;
        string strBuscar = string.Empty;
        string strNuevo = string.Empty;
        string strEditar = string.Empty;
        string strEliminar = string.Empty;

        //obtiene el nombre de la pagina actual
        string[] strRutaPagina = HttpContext.Current.Request.RawUrl.Split('/');
        string strNombrePagina = strRutaPagina[strRutaPagina.GetUpperBound(0)];
        strRutaPagina = strNombrePagina.Split('?');
        strNombrePagina = strRutaPagina[strRutaPagina.GetLowerBound(0)];
        //fin obtiene el nombre de la pagina actual


        if (strNombrePagina != string.Empty)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {

                //intIdFormulario = Convert.ToInt32(Request.QueryString["op"].ToString());
                //Session["intIdFormulario"] = intIdFormulario;
                if (blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(Session["IdUsuario"].ToString()), strNombrePagina))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strBuscar = blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                        strNuevo = blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                        strEditar = blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                        strEliminar = blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();
                    }
                }

                blPaicma.Termina();
            }
        }
        else
        {
            Response.Redirect("@dmin/frmLogin.aspx");
        }
    }

    public void fntCargarGrillaFormatos()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaFormatos("strDsFormatos"))
            {
                gvFormato.Columns[0].Visible = true;
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvFormato.DataMember = "strDsFormatos";
                    gvFormato.DataSource = blPaicma.myDataSet;
                    gvFormato.DataBind();
                    lblErrorGv.Text = string.Empty;
                }
                else
                {
                    gvFormato.DataMember = "strDsFormatos";
                    gvFormato.DataSource = blPaicma.myDataSet;
                    gvFormato.DataBind();
                    lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                gvFormato.Columns[0].Visible = false;
            }
            blPaicma.Termina();
        }
    }

    protected void gvFormato_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strRutaServidor = string.Empty;
        string strFolder = string.Empty;
        string strNombreDocumentoAuxiliar = string.Empty;
        string strNombreDocumento = string.Empty;
        gvFormato.Columns[0].Visible = true;
        if (e.CommandName == "Ver")
        {
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvFormato.Rows[intIndex];
            TableCell Item = selectedRow.Cells[3];

            try
            {
                strNombreDocumento = Item.Text;
                if (strNombreDocumento.Trim() != string.Empty)
                {
                    strFolder = "FormatosEstandar/";
                    strRutaServidor = System.Configuration.ConfigurationManager.AppSettings["RutaServerDinamico"];
                    strRutaServidor = strRutaServidor + strFolder + strNombreDocumento;
                    Response.Write("<script language='JavaScript'>window.open('" + strRutaServidor + "')</script>");
                    lblErrorGv.Text = string.Empty;
                }
                else
                {
                    lblErrorGv.Text = "No hay un documento asociado en el registro de seguimiento.";
                }
            }
            catch
            {
                lblErrorGv.Text = "Error al asociar al documento, por favor ponerse en contacto con el administrador.";
            }
        }
        gvFormato.Columns[0].Visible = false;

    }
}