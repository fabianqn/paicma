﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class _dmin_frmDinamico : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    static TextBox[] arregloTextBoxs;
    static DropDownList[] arregloCombos;
    static TextBox[] arregloTextBoxsLongitud;
    static int contadorControles;
    static Label[] arregloLables;
    static Label[] arregloLables2;
    static Label[] arregloLables3;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                ftnValidarPermisos();
                fntCargarGrillaTablas(1, 0, string.Empty);

            }

            lblError.Text = string.Empty;
            arregloTextBoxs = new TextBox[100];
            arregloCombos = new DropDownList[100];
            arregloLables = new Label[100];
            arregloLables2 = new Label[100];
            arregloTextBoxsLongitud = new TextBox[100];
            arregloLables3 = new Label[100];

            contadorControles = 0;
        }
        try
        {
            for (int i = 0; i < contadorControles; i++)
            {
                AgregarControles(arregloTextBoxs[i], arregloCombos[i], arregloLables[i], arregloLables2[i], arregloLables3[i], arregloTextBoxsLongitud[i]);
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }


        var script2 = " $(':input').keypress(function (e) {var regex = new RegExp('^[a-zA-Z0-9@. ]+$');var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);if (regex.test(str)) {return true;} e.preventDefault();return false;});$(':input').blur(function () {var regex = new RegExp('^$|^[a-zA-Z0-9@. ]+$');var str = $(this).val();if (regex.test(str)) {return true;} else { $(this).val('');$(this).focus();return false;}}); ";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowE", script2, true);

    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }



    public void fntCargarGrillaTablas(int intIdEstadoActivacion, int intOp, string strNombreTabla)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTablas("strDsGrillaTablas", intIdEstadoActivacion, intOp, strNombreTabla, 0))
            {
                gvTablas.Columns[0].Visible = true;
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvTablas.DataMember = "strDsGrillaTablas";
                    gvTablas.DataSource = blPaicma.myDataSet;
                    gvTablas.DataBind();
                    lblErrorGv.Text = string.Empty;
                }
                else
                {
                    gvTablas.DataMember = "strDsGrillaTablas";
                    gvTablas.DataSource = blPaicma.myDataSet;
                    gvTablas.DataBind();
                    lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                gvTablas.Columns[0].Visible = false;
            }
            blPaicma.Termina();
        }
    }

    protected void AgregarControles(TextBox txt, DropDownList ddl, Label lbl, Label lbl2, Label lbl3, TextBox txtLongitud)
    {
        try
        {
            HtmlTableRow fila = new HtmlTableRow();
            HtmlTableCell celda = new HtmlTableCell();
            HtmlTableCell celda2 = new HtmlTableCell();
            HtmlTableCell celda3 = new HtmlTableCell();
            HtmlTableCell celda4 = new HtmlTableCell();
            HtmlTableCell celda5 = new HtmlTableCell();
            HtmlTableCell celda6 = new HtmlTableCell();

            celda3.Controls.Add(lbl);
            celda.Controls.Add(txt);
            celda4.Controls.Add(lbl2);
            celda2.Controls.Add(ddl);
            celda5.Controls.Add(lbl3);
            celda6.Controls.Add(txtLongitud);

            fila.Cells.Add(celda3);
            fila.Cells.Add(celda);
            fila.Cells.Add(celda4);
            fila.Cells.Add(celda2);
            fila.Cells.Add(celda5);
            fila.Cells.Add(celda6);

            Activiades.Rows.Add(fila);

        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }

    }

    public void ftnValidarPermisos()
    {
        //int intIdFormulario = 0;
        string strBuscar = string.Empty;
        string strNuevo = string.Empty;
        string strEditar = string.Empty;
        string strEliminar = string.Empty;

        //obtiene el nombre de la pagina actual
        string[] strRutaPagina = HttpContext.Current.Request.RawUrl.Split('/');
        string strNombrePagina = strRutaPagina[strRutaPagina.GetUpperBound(0)];
        strRutaPagina = strNombrePagina.Split('?');
        strNombrePagina = strRutaPagina[strRutaPagina.GetLowerBound(0)];
        //fin obtiene el nombre de la pagina actual

        if (strNombrePagina != string.Empty)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {

                //intIdFormulario = Convert.ToInt32(Request.QueryString["op"].ToString());
                //Session["intIdFormulario"] = intIdFormulario;
                if (blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(Session["IdUsuario"].ToString()), strNombrePagina))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strBuscar = blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                        strNuevo = blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                        strEditar = blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                        strEliminar = blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();

                        if (strBuscar == "False")
                        {
                            imgbBuscar.Visible = false;
                        }
                        if (strNuevo == "False")
                        {
                            imgbNuevo.Visible = false;
                            imgbNuevoCampo.Visible = false;
                        }
                        if (strEditar == "False")
                        {
                            imgbEditar.Visible = false;
                            imgbGravar.Visible = false;
                            imgbNuevoCampo.Visible = false;
                        }
                        if (strEliminar == "False")
                        {
                            imgbEliminar.Visible = false;
                        }


                    }
                }

                blPaicma.Termina();
            }
        }
        else
        {
            Response.Redirect("@dmin/frmLogin.aspx");
        }

    }

    public void ftnInactivarControles(Boolean bolValor)
    {
        pnlAccionesDinamico.Visible = bolValor;
        pnlCamposTabla.Visible = bolValor;
        pnlEliminacion.Visible = bolValor;
        pnlMenuDinamico.Visible = bolValor;
        pnlRespuesta.Visible = bolValor;
        PnlTablasGrilla.Visible = bolValor;
        pnlBusqueda.Visible = bolValor;
        pnlCampos.Visible = bolValor;
    }

    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        ftnInactivarControles(false);
        pnlAccionesDinamico.Visible = true;
        pnlCamposTabla.Visible = true;

        imgbEncontrar.Visible = false;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        imgbNuevoCampo.Visible = true;
        Session["Operacion"] = "Crear";
    }

    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        string strRuta = "frmDinamico.aspx";
        Response.Redirect(strRuta);
    }

    protected void imgbNuevoCampo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int numeroRegistro = contadorControles;

            //creamos nuestros textbox
            TextBox nuevoTxt = new TextBox();
            nuevoTxt.ID = "txt" + numeroRegistro.ToString();
            arregloTextBoxs[numeroRegistro] = nuevoTxt;

            //creamos los dropDownList
            DropDownList nuevoCmb = new DropDownList();
            nuevoCmb.ID = "ddl" + numeroRegistro.ToString();
            nuevoCmb.Items.Add("Tipo de Dato");
            nuevoCmb.Items.Add("Entero");
            nuevoCmb.Items.Add("Decimal");
            nuevoCmb.Items.Add("Alfanumérico");
            nuevoCmb.Items.Add("Fecha");
            nuevoCmb.Items.Add("Falso y Verdadero");
            nuevoCmb.SelectedIndex = 0;
            arregloCombos[numeroRegistro] = nuevoCmb;


            Label nuevoLbl = new Label();
            nuevoLbl.ID = "lbl" + numeroRegistro.ToString();
            nuevoLbl.Text = "Nombre del Campo ";
            arregloLables[numeroRegistro] = nuevoLbl;

            Label nuevoLbl2 = new Label();
            nuevoLbl2.ID = "lbl2" + numeroRegistro.ToString();
            nuevoLbl2.Text = " Tipo ";
            arregloLables2[numeroRegistro] = nuevoLbl2;


            //creamos nuestros textbox
            TextBox nuevoTxtLongitud = new TextBox();
            nuevoTxtLongitud.ID = "txtLongitud" + numeroRegistro.ToString();
            nuevoTxtLongitud.Attributes["type"] = "number";
            arregloTextBoxsLongitud[numeroRegistro] = nuevoTxtLongitud;


            Label nuevoLbl3 = new Label();
            nuevoLbl3.ID = "lbl3" + numeroRegistro.ToString();
            nuevoLbl3.Text = " Longitud ";
            arregloLables3[numeroRegistro] = nuevoLbl3;


            ///agregamos los controles al panel y tabla
            AgregarControles(nuevoTxt, nuevoCmb, nuevoLbl, nuevoLbl2, nuevoLbl3, nuevoTxtLongitud);
            contadorControles++;

            lblError.Text = string.Empty;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }

    }

    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        if (ftnValidarCampos_bol())
        {
            ftnInactivarControles(false);
            pnlEliminacion.Visible = true;
            Session["Operacion"] = "Crear";
            lblEliminacion.Text = "¿Está seguro de crear la tabla " + txtTabla.Text.Trim() + "?";
        }
        else
        {
            //no paso la validacion
        }

    }

    protected void btnNo_Click(object sender, EventArgs e)
    {
        if (Session["Operacion"] != null)
        {
            if (Session["Operacion"].ToString() == "Crear")
            {
                ftnInactivarControles(false);
                pnlAccionesDinamico.Visible = true;
                pnlCamposTabla.Visible = true;
            }
        }
        else
        {
            ftnInactivarControles(false);
            pnlAccionesDinamico.Visible = true;
            pnlCamposTabla.Visible = true;
            pnlCampos.Visible = true;
        }
    }

    protected void btnSi_Click(object sender, EventArgs e)
    {
        if (Session["Operacion"] != null)
        {
            if (Session["Operacion"].ToString() == "Crear")
            {

                int intCantidadControles = contadorControles;

                string strCamposACrear = string.Empty;
                string strTipoDeDato = string.Empty;
                string strTipoDeDatoAux = string.Empty;
                string strLongitud = string.Empty;
                string strSw;

                if (intCantidadControles > 0)
                {
                    for (int i = 0; i < intCantidadControles; i++)
                    {
                        strTipoDeDatoAux = arregloCombos[i].Text;
                        strLongitud = arregloTextBoxsLongitud[i].Text.Trim();
                        if (strTipoDeDatoAux == "Tipo de Dato" || strTipoDeDatoAux == "Alfanumérico")
                        {
                            strTipoDeDato = "[nvarchar]";
                            if (strLongitud != string.Empty)
                            {
                                strTipoDeDato = strTipoDeDato + " (" + strLongitud + ") ";
                            }
                            else
                            {
                                strTipoDeDato = strTipoDeDato + " (1000) ";
                            }

                        }
                        if (strTipoDeDatoAux == "Entero")
                        {
                            strTipoDeDato = "[int]";
                        }
                        if (strTipoDeDatoAux == "Decimal")
                        {
                            strTipoDeDato = "[decimal](18, 2)";
                        }
                        if (strTipoDeDatoAux == "Fecha")
                        {
                            strTipoDeDato = "[datetime]";
                        }
                        if (strTipoDeDatoAux == "Falso y Verdadero")
                        {
                            strTipoDeDato = "[bit]";
                        }

                        strCamposACrear = strCamposACrear + txtAbreviatura.Text.Trim() + "_" + arregloTextBoxs[i].Text.Trim() + " " + strTipoDeDato + " NULL, ";

                    }

                    string strTabla = string.Empty;
                    if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                    {
                        strTabla = "tblU_" + txtTabla.Text.Trim();
                        if (blPaicma.fntCrearTablaNueva_bol(strTabla, strCamposACrear))
                        {
                            if (blPaicma.fntIngresarAdmTablas_bol(strTabla, txtAbreviatura.Text.Trim(), contadorControles, Convert.ToInt32(Session["IdUsuario"].ToString()), txtObservacion.Text.Trim()))
                            {
                                lblRespuesta.Text = "Tabla creada satisfactoriamente.";
                            }
                        }
                        else
                        {
                            lblRespuesta.Text = "Problemas al crear la Tabla, por favor contactar al administrador, Gracias.";
                        }
                        ftnInactivarControles(false);
                        pnlRespuesta.Visible = true;

                        blPaicma.Termina();
                    }
                }
            }
        }
        else
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntEliminarTablaNueva_bol(txtTabla.Text.Trim()))
                {
                    if (blPaicma.fntEliminarRegistroTabla(Convert.ToInt32(Session["intIdTabla"].ToString())))
                    {
                        lblRespuesta.Text = "Tabla Eliminada satisfactoriamente.";
                    }
                }
                else
                {
                    lblRespuesta.Text = "Problemas al eliminar la tabla, por favor contáctese con el administrador.";
                }
                ftnInactivarControles(false);
                pnlRespuesta.Visible = true;
                blPaicma.Termina();
            }
        }
    }

    public Boolean ftnValidarCampos_bol()
    {

        if (txtTabla.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar el nombre de la tabla.";
            return false;
        }

        if (txtAbreviatura.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar la abreviatura de la tabla.";
            return false;
        }

        if (txtObservacion.Text.Trim() == string.Empty)
        {
            lblError.Text = "Debe indicar una observación sobre la tabla.";
            return false;
        }


        int intCantidadControles = contadorControles;
        int intContadorInterno = 0;
        string strCamposACrear = string.Empty;

        if (intCantidadControles > 0)
        {
            for (int i = 0; i < intCantidadControles; i++)
            {
                strCamposACrear = arregloTextBoxs[i].Text.Trim();
                if (strCamposACrear != string.Empty)
                {
                    intContadorInterno++;
                }
                else
                {
                    i = intCantidadControles;
                }
            }
            contadorControles = intContadorInterno;
        }

        if (contadorControles == 0)
        {
            lblError.Text = "Mínimo Debe crear un campo para la tabla.";
            return false;
        }


        lblError.Text = string.Empty;
        return true;
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        string strRuta = "frmDinamico.aspx";
        Response.Redirect(strRuta);
    }

    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        ftnInactivarControles(false);
        pnlAccionesDinamico.Visible = true;
        pnlBusqueda.Visible = true;
        imgbEncontrar.Visible = true;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = false;
        imgbCancelar.Visible = true;
        ftnLimpiarControles();
    }

    public void ftnLimpiarControles()
    {
        txtTablaBusqueda.Text = string.Empty;
        ddlEstadoBusqueda.SelectedIndex = 0;
    }

    protected void imgbEncontrar_Click(object sender, ImageClickEventArgs e)
    {
        int intIdEstadoActivacion = 0;
        intIdEstadoActivacion = Convert.ToInt32(ddlEstadoBusqueda.SelectedValue.ToString());
        fntCargarGrillaTablas(intIdEstadoActivacion, 1, txtTablaBusqueda.Text.Trim());
        ftnInactivarControles(false);
        pnlMenuDinamico.Visible = true;
        PnlTablasGrilla.Visible = true;
    }

    protected void gvTablas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        gvTablas.Columns[0].Visible = true;
        if (e.CommandName == "Seleccion")
        {
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvTablas.Rows[intIndex];
            TableCell Item = selectedRow.Cells[0];
            int intIdTabla = Convert.ToInt32(Item.Text);
            Session["intIdTabla"] = intIdTabla;
            imgbEditar.Visible = true;
        }
        gvTablas.Columns[0].Visible = false;
    }

    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTablas("strDsGrillaCampos", 0, 2, string.Empty, Convert.ToInt32(Session["intIdTabla"].ToString())))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ftnInactivarControles(false);
                    pnlAccionesDinamico.Visible = true;
                    pnlCamposTabla.Visible = true;
                    pnlCampos.Visible = true;
                    imgbEncontrar.Visible = false;
                    imgbGravar.Visible = false;


                    gvCampos.DataMember = "strDsGrillaCampos";
                    gvCampos.DataSource = blPaicma.myDataSet;
                    gvCampos.DataBind();
                    lblGVcampos.Text = string.Empty;


                    txtTabla.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NAME"].ToString();
                    txtAbreviatura.Text = blPaicma.myDataSet.Tables[0].Rows[0]["admTab_AbreviaturaTabla"].ToString();
                    txtObservacion.Text = blPaicma.myDataSet.Tables[0].Rows[0]["admTab_Observacion"].ToString();
                    txtTabla.Enabled = false;
                    txtAbreviatura.Enabled = false;
                    txtObservacion.Enabled = false;
                }
                else
                {
                    gvCampos.DataMember = "strDsGrillaCampos";
                    gvCampos.DataSource = blPaicma.myDataSet;
                    gvCampos.DataBind();
                    lblGVcampos.Text = "No hay registros que coincidan con esos criterios.";
                }
                ftnValidarPermisos();
            }
            blPaicma.Termina();
        }
    }

    protected void imgbEliminar_Click(object sender, ImageClickEventArgs e)
    {
        Session["Operacion"] = null;
        int intCantidadRegistrosTablaDinamica = 0;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaCantidadRegistrosTablaDinamica("strDsCantidadRegistrosTablaDinamica", txtTabla.Text.Trim()))
            {
                intCantidadRegistrosTablaDinamica = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                if (intCantidadRegistrosTablaDinamica > 0)
                {
                    ftnInactivarControles(false);
                    pnlRespuesta.Visible = true;
                    lblRespuesta.Text = "No se puede eliminar la tabla porque ya tiene registros almacenados.";
                }
                else
                {
                    ftnInactivarControles(false);
                    pnlEliminacion.Visible = true;
                    lblEliminacion.Text = "¿Está seguro de eliminar el registro y su archivo adjunto?";
                }

            }
            blPaicma.Termina();
        }
    }

    protected void gvTablas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTablas.PageIndex = e.NewPageIndex;
        fntCargarGrillaTablas(1, 0, string.Empty);
    }

    protected void gvCampos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCampos.PageIndex = e.NewPageIndex;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTablas("strDsGrillaCampos", 0, 2, string.Empty, Convert.ToInt32(Session["intIdTabla"].ToString())))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvCampos.DataMember = "strDsGrillaCampos";
                    gvCampos.DataSource = blPaicma.myDataSet;
                    gvCampos.DataBind();
                    lblGVcampos.Text = string.Empty;
                }
            }
            blPaicma.Termina();
        }
    }


}