﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmWFPlanAccion.aspx.cs" Inherits="_dmin_frmWFPlanAccion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="Server">

     <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Aceptar" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <div class="contacto2" id="principal" runat="server">
        <h1>Estrategias</h1>
        <hr />
        <asp:GridView ID="gvPlanAccion" CssClass="mGrid" runat="server" AutoGenerateColumns="False" OnRowCommand="gvPlanAccion_RowCommand" OnPreRender="gvPlanAccion_PreRender">
            <Columns>
                <asp:BoundField DataField="idPlanAccion" HeaderText="ID Plan" />
                <asp:BoundField DataField="Estrategia" HeaderText="Estrategia" />
                <asp:BoundField DataField="Actividad" HeaderText="Actividad" />
                <asp:BoundField DataField="indicador" HeaderText="Indicador" />
                <asp:ButtonField ButtonType="Button" Text="Editar" CommandName="EditarPlan"  />
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="~/Images/eliminar.png"
                            CommandName="Delete" OnClientClick="return confirm('¿Estas Seguro que queires eliminar este Plan?');"
                            AlternateText="EliminarPlan" />               
                    </ItemTemplate>
                </asp:TemplateField>  
            </Columns>
        </asp:GridView>
        <asp:Label ID="lblRespuestaInterna" runat="server" Visible="false"></asp:Label>
        <div style="width: 100%">
            <hr />
            <table style="width: 100%" id="PlanesDinamicos" runat="server">
                <tr>
                    <td>
                        <h1 id="mensajeCrear" runat="server">Agregar Estrategias</h1>
                    </td>
                    <td>
                        <asp:ImageButton ID="imgbNuevoPlan" runat="server"
                            ImageUrl="~/Images/NuevoCampo.png" ToolTip="Nuevo Campo"
                            OnClick="imgbNuevoPlan_Click" Visible="true" />
                    </td>
                     <td>
                        <asp:ImageButton ID="imgbEliminarPlan" runat="server"
                            ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar Ultimmo Campo"
                            OnClick="imgbEliminarPlan_Click" Visible="true" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                     <hr />

                    </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
            </table>
        </div>

        <asp:Panel ID="panelBotones" runat="server">
        <div class="Botones">
            <asp:Button ID="btnGuardarCont" runat="server" Text="Guardar y Continuar" OnClick="btnGuardarCont_Click" />
            <%--<asp:Button ID="btnGuardarSalir" runat="server" Text="Guardar y Salir" OnClick="btnGuardarSalir_Click"/>--%>
           



            <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
        </div>
    </asp:Panel>
        <asp:Panel ID="panelSoloVer" runat="server" Visible="false">
        <div class="Botones">
            <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
       <%--     <asp:Button ID="btnCancelarSoloVer" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" />--%>
        </div>
    </asp:Panel>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="Server">
        <script type="text/javascript">
            function newDoc() {
                window.location.assign("frmWFSolicitudComision.aspx")
            }
            function setCharAt(str, index, chr) {
                if (index > str.length - 1) return str;
                return str.substr(0, index) + chr + str.substr(index + 1);
            }
            $(document).ready(function () {
            $('#'+ '<%= gvPlanAccion.ClientID %>').DataTable();
            });

                $(".Estrategia").change(function () {
                var valorDep = $(this).val();


               // var ordenDep = $(this).data("orden");
                var id = $(this).attr("id");
                //alert(id)
                var cadenaAnterior = parseInt(id.replace(/[^0-9\.]/g, ''), 10);
                
   
                var numero = parseInt(cadenaAnterior, 10);
                    //alert(numero);
                var nombreElementoSustituir = id.replace('ddlEstrategia', 'ddlindicador');
                var n = nombreElementoSustituir.lastIndexOf(cadenaAnterior);
                var nombreElementoCortado = setCharAt(nombreElementoSustituir, n, '');
               // var nombreElementoCortado = nombreElementoSustituir.replace(cadenaAnterior, '');

                var nombreElementoSustituir2 = id.replace('ddlEstrategia', 'ddlactividad');
                var n2 = nombreElementoSustituir2.lastIndexOf(cadenaAnterior);
                var nombreElementoCortado2 = setCharAt(nombreElementoSustituir2, n2, '');
               // var nombreElementoCortado2 = nombreElementoSustituir2.replace(cadenaAnterior, '');
                var idMun = nombreElementoCortado + numero;
                var idActividad = nombreElementoCortado2 + numero;
                var optDep;
                var control = document.getElementById(idMun);
                control.removeAttribute("disabled");
                if (valorDep != "Seleccione...") {


                    $("#" + idMun + " option").each(function (index, val) {

                        var $el = $(this);
                        if ($el.val() == "0") {
                            $el.attr("selected", true);
                        }
                        optDep = $el.data("estrategia")
                        if (optDep != valorDep) {
                            if ($(this).is('option') && (!$(this).parent().is('span')))
                                $(this).wrap((navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') ? '<span>' : null).hide();
                        } else {
                            if (navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') {
                                if (this.nodeName.toUpperCase() === 'OPTION') {
                                    var span = $(this).parent();
                                    var opt = this;
                                    if ($(this).parent().is('span')) {
                                        $(opt).show();
                                        $(span).replaceWith(opt);
                                    }
                                }
                            } else {
                                $(this).show(); //all other browsers use standard .show()
                            }
                        }
                    });
                } else {
         
                    $("#" + idMun).val("0");
                    $("#" + idMun + " option").each(function () {
                        if ($(this).is('option') && (!$(this).parent().is('span')))
                            $(this).wrap((navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') ? '<span>' : null).hide();
                    });
                    $("#" + idActividad).val("0");
                    $("#" + idActividad + " option").each(function () {
                        if ($(this).is('option') && (!$(this).parent().is('span')))
                            $(this).wrap((navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') ? '<span>' : null).hide();
                    });

                }

            });









         // funcion cuando seleccione  un indicador 
           $(".Indicador").change( function () {
            var valorDep = $(this).val();


            // var ordenDep = $(this).data("orden");
            var id = $(this).attr("id");
            //alert(id)
            var cadenaAnterior = parseInt(id.replace(/[^0-9\.]/g, ''), 10);
            var nombreElementoSustituir3 = id.replace('ddlindicador', 'ddlactividad');
            var n3 = nombreElementoSustituir3.lastIndexOf(cadenaAnterior);
            var nombreElementoCortado3 = setCharAt(nombreElementoSustituir3, n3, '');
           // var nombreElementoCortado3 = nombreElementoSustituir3.replace(cadenaAnterior, '');
            //alert(cadenaPrevia);
            var numero = parseInt(cadenaAnterior, 10);
            //alert(numero);
            var idMun = nombreElementoCortado3 + numero;

            var optDep;


           // $("#" + idMun).disabled = true;

            var control = document.getElementById(idMun);
            control.removeAttribute("disabled");



            if (valorDep != "0") {



                $("#" + idMun + " option").each(function (index, val) {
                    var $el = $(this);
                    if ($el.val() == "0") {
                        $el.attr("selected", true);
                    }
                    optDep = $el.data("indicador")
                    if (optDep != valorDep) {
                        if ($(this).is('option') && (!$(this).parent().is('span')))
                            $(this).wrap((navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') ? '<span>' : null).hide();
                    } else {
                        if (navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') {
                            if (this.nodeName.toUpperCase() === 'OPTION') {
                                var span = $(this).parent();
                                var opt = this;
                                if ($(this).parent().is('span')) {
                                    $(opt).show();
                                    $(span).replaceWith(opt);
                                }
                            }
                        } else {
                            $(this).show(); //all other browsers use standard .show()
                        }
                    }
                });
            } else {
                $("#" + idMun).val("0");
                $("#" + idMun + " option").each(function () {
                    if ($(this).is('option') && (!$(this).parent().is('span')))
                        $(this).wrap((navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') ? '<span>' : null).hide();
                });
            }

        });




    </script>
</asp:Content>

