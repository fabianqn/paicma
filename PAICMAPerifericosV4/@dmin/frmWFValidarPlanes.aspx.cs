﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class _dmin_frmWFValidarPlanes : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    private DataTable dtPlanesValidados = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                
                //this.fillSolicitudes();
                //this.cargarLineasIntervencion();
                this.FillPlanesAccion();
                this.FillPlanesValidados();
                //this.FillSitiosIntervencion();
                if (txtCumplimiento.Text != string.Empty)
                {
                    if (Convert.ToDecimal(txtCumplimiento.Text) > 0)
                    {
                        btnContinuar.Visible = true;
                    }
                }
                //this.FillSoportes();
                //this.CargarPermisos();
            }

        }
    }

    public void FillPlanesValidados()
    {
        if (Session["IdLegalizacionRef"].ToString() != "0")
        {
            Int64 idLegalizacion = Convert.ToInt64(Session["IdLegalizacionRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                decimal calcularCumplimiento = 0;
                if (blPaicma.fntConsultarPlanValidado("datosPlanesValidados", "",  0, idLegalizacion))
                {
                    dtPlanesValidados = blPaicma.myDataSet.Tables["datosPlanesValidados"];
                    Session["cantPlanesValidados"] = blPaicma.myDataSet.Tables["datosPlanesValidados"].Rows.Count;

                    
                        dtPlanesValidados = blPaicma.myDataSet.Tables["datosPlanesValidados"];
                        foreach (DataRow dtRow in dtPlanesValidados.Rows)
                        {
                            calcularCumplimiento = calcularCumplimiento + Convert.ToDecimal(dtRow["ejecucionPorcentaje"]);
                        }

                        if (calcularCumplimiento !=0)
                        {
                            txtCumplimiento.Text = (calcularCumplimiento / Convert.ToInt32(Session["PlanesAccionCargados"])).ToString();
                        }

                  
                }
                else
                {
                    //Imposible Consultar lista de planes
                    lblRespuesta.Text = "Se ha presentado un problema al consultar lista de planes, intente de nuevo";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    Session["OperacionValidarPlanes"] = "ErrorSalir";
                }
            }
            else
            {
                //No pudo conectar a la base de datos
                lblRespuesta.Text = "Se ha presentado un problema al consultar lista de planes, no se ha podido conectar a la base de datos";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionValidarPlanes"] = "ErrorSalir";
            }
            blPaicma.Termina();
        }
        else
        {
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
    }

    public void FillPlanesAccion()
    {
        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarPlanes("datosPlanesAccion", idSolicitud, "", 0))
                {
                    Session["PlanesAccionCargados"] = blPaicma.myDataSet.Tables["datosPlanesAccion"].Rows.Count; 


                    if (blPaicma.myDataSet.Tables["datosPlanesAccion"].Rows.Count > 0)
                    {

                        this.gvPlanAccionElegir.DataMember = "datosPlanesAccion";
                        this.gvPlanAccionElegir.DataSource = this.blPaicma.myDataSet;
                        this.gvPlanAccionElegir.DataBind();
                    }
                }
                else
                {
                    //Imposible Consultar lista de planes
                    lblRespuesta.Text = "Se ha presentado un problema al consultar lista de planes, intente de nuevo";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    Session["OperacionValidarPlanes"] = "ErrorSalir";
                }
            }
            else
            {
                //No pudo conectar a la base de datos
                lblRespuesta.Text = "Se ha presentado un problema al consultar lista de planes, no se ha podido conectar a la base de datos";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionValidarPlanes"] = "ErrorSalir";
            }
            blPaicma.Termina();
        }
        else
        {
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
    }

    protected void gvPlanAccionElegir_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int intIndex = Convert.ToInt32(e.CommandArgument);
        GridViewRow selectedRow = gvPlanAccionElegir.Rows[intIndex];
        TableCell Item = selectedRow.Cells[0];
        Int64 idPlanAccion = Convert.ToInt64(Item.Text);




          TableCell Item2 = selectedRow.Cells[1];
          string nombreEstrategia = Item2.Text;
          TableCell Item3 = selectedRow.Cells[4];
          ViewState["valorIndicador"] = Item3.Text;

      
        Session["idPlanAccionElegido"] = idPlanAccion;


        if (e.CommandName == "Validar")
        {
            lblRespuesta.Text = "Datos actualizados";
            lblRespuesta.Visible = true;
            validacionPlan.Visible = true;
            txtActividadValidar.Text = string.Empty;
            txtValorIndicador.Text = string.Empty;
            lblStrategia.Text = nombreEstrategia;
        }
    }
    protected void gvPlanAccionElegir_PreRender(object sender, EventArgs e)
    {
        if (gvPlanAccionElegir.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvPlanAccionElegir.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvPlanAccionElegir.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvPlanAccionElegir.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
    protected void btnGuardarCont_Click(object sender, EventArgs e)
    {
        long idLega = Convert.ToInt64(Session["IdLegalizacionRef"]);
        long idPlan = Convert.ToInt64(Session["idPlanAccionElegido"]);
        string planEjecutado = txtActividadValidar.Text;

        decimal indicador = decimal.TryParse(txtValorIndicador.Text, out indicador) ? Convert.ToDecimal(indicador) : 0;

        decimal ValorIndicador =Convert.ToDecimal( ViewState["valorIndicador"].ToString());

        decimal valorPorcentaje = Math.Round((indicador / ValorIndicador) * 100, 2);




        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarPlanValidado("strPlanesValidacion", "id", idPlan, idLega))
            {
                if (blPaicma.myDataSet.Tables["strPlanesValidacion"].Rows.Count > 0)
                {
                    //Edita
                    if (blPaicma.fntActualizarPlanValidado_bol(idLega, idPlan, planEjecutado, valorPorcentaje))
                    {
                        lblRespuesta.Text = "Datos actualizados";
                        lblRespuesta.Visible = true;
                        validacionPlan.Visible = false;
                        txtActividadValidar.Text = string.Empty;
                        txtValorIndicador.Text = string.Empty;
                        
                        decimal calcularCumplimiento = 0;
                        if (blPaicma.fntConsultarPlanValidado("datosPlanesValidados", "", 0, idLega))
                        {
                           dtPlanesValidados = blPaicma.myDataSet.Tables["datosPlanesValidados"];
                           foreach (DataRow dtRow in dtPlanesValidados.Rows)
                           {
                              calcularCumplimiento = calcularCumplimiento + Convert.ToDecimal(dtRow["ejecucionPorcentaje"]);
                           }
                        }
                        else
                        {
                            //Imposible Consultar lista de planes
                            lblMensajeGuardar.Text = "Se ha presentado un problema al consultar lista de planes, intente de nuevo";
                            principal.Visible = false;
                            pnlRespuesta.Visible = true;
                            Session["OperacionValidarPlanes"] = "ErrorSalir";
                        }

                        txtCumplimiento.Text = (calcularCumplimiento / Convert.ToInt32(Session["PlanesAccionCargados"])).ToString();
                        //Correcto

                        lblMensajeGuardar.Text = "Datos actualizados";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        Session["OperacionValidarPlanes"] = "Guardar";
                    }
                    else
                    {
                        lblRespuesta.Text = "Imposible Actualizar";
                        lblRespuesta.Visible = true;
                        validacionPlan.Visible = false;
                        txtActividadValidar.Text = string.Empty;
                        txtValorIndicador.Text = string.Empty;
                        //fallo
                    }
                }
                else
                {
                    //Inserta
                    if (blPaicma.fntInsertarPlanValidado_bol(idLega, idPlan, planEjecutado, valorPorcentaje))
                    {
                        lblRespuesta.Text = "Plan Ejecutado Registrado";
                        lblRespuesta.Visible = true;
                        validacionPlan.Visible = false;
                        txtActividadValidar.Text = string.Empty;
                        txtValorIndicador.Text = string.Empty;
                        lblMensajeGuardar.Text = "Plan Ejecutado Registrado";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        Session["OperacionValidarPlanes"] = "Guardar";
                        //Correcto
                    }
                    else
                    {
                        lblRespuesta.Text = "Imposible Registrar Plan";
                        lblRespuesta.Visible = true;
                        validacionPlan.Visible = false;
                        txtActividadValidar.Text = string.Empty;
                        txtValorIndicador.Text = string.Empty;
                        //fallo
                    }                
                }
            }
        }
        blPaicma.Termina();

    }
    protected void btnGuardarSalir_Click(object sender, EventArgs e)
    {
        long idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        long idLega = Convert.ToInt64(Session["IdLegalizacionRef"]);
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, EstadosSolicitud.LegIncompletaCompromisos, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
            {
                if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", EstadosSolicitud.LegIncompletaCompromisos, "WFSol_SolicitudComision", "idEstadoActual"))
                {
                    if(blPaicma.fntActualizarCumplimientoLegalizacion_bol(idLega, Convert.ToDecimal(txtCumplimiento.Text))){
                        lblRespuesta.Text = "Se ha presentado un problema al consultar lista de planes, intente de nuevo";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        Session["OperacionValidarPlanes"] = "ErrorSalir";
                    }
                }
                else
                {
                    //Fallo actualizar estado en la comision
                }
            }
            else
            {
                //fallo actualizar estado Solicitud
            }
        }
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        string operacionActual = Session["OperacionValidarPlanes"].ToString();


        if (operacionActual == "Guardar")
        {
            Session["OperacionSolicitud"] = "Inicio";
            lblMensajeGuardar.Text = "Actualizado Correctamente";
            Response.Redirect("frmWFCompromisos.aspx");
        }

        if (operacionActual == "ErrorSalir")
        {
            lblMensajeGuardar.Text = "Hubo problemas para actualizar";
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "Inicio")
        {
            lblMensajeGuardar.Text = "No se puede consultar la solicitud respectiva";
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
    }
    protected void btnContinuar_Click(object sender, EventArgs e)
    {
        long idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        long idLega = Convert.ToInt64(Session["IdLegalizacionRef"]);
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, EstadosSolicitud.LegIncompletaCompromisos, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
            {
                if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", EstadosSolicitud.LegIncompletaCompromisos, "WFSol_SolicitudComision", "idEstadoActual"))
                {
                    if (blPaicma.fntActualizarCumplimientoLegalizacion_bol(idLega, Convert.ToDecimal(txtCumplimiento.Text)))
                    {
                        lblMensajeGuardar.Text = "Datos actualizados";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        Session["OperacionValidarPlanes"] = "Guardar";
                    }
                }
                else
                {
                    lblRespuesta.Text = "Se ha presentado un problema al consultar lista de planes, intente de nuevo";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    Session["OperacionValidarPlanes"] = "ErrorSalir";
                    //Fallo actualizar estado en la comision
                }
            }
            else
            {
                lblRespuesta.Text = "Se ha presentado un problema al consultar lista de planes, intente de nuevo";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionValidarPlanes"] = "ErrorSalir";
                //fallo actualizar estado Solicitud
            }
        }
        
    }
}