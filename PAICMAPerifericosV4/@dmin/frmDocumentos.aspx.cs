﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmDocumentos : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                ftnValidarPermisos();
                fntCargarGrillaDocumentos(0,string.Empty,0);
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }

    public void ftnValidarPermisos()
    {
        //int intIdFormulario = 0;
        string strBuscar = string.Empty;
        string strNuevo = string.Empty;
        string strEditar = string.Empty;
        string strEliminar = string.Empty;

        //obtiene el nombre de la pagina actual
        string[] strRutaPagina = HttpContext.Current.Request.RawUrl.Split('/');
        string strNombrePagina = strRutaPagina[strRutaPagina.GetUpperBound(0)];
        strRutaPagina = strNombrePagina.Split('?');
        strNombrePagina = strRutaPagina[strRutaPagina.GetLowerBound(0)];
        //fin obtiene el nombre de la pagina actual


        if (strNombrePagina != string.Empty)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {

                //intIdFormulario = Convert.ToInt32(Request.QueryString["op"].ToString());
                //Session["intIdFormulario"] = intIdFormulario;
                if (blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(Session["IdUsuario"].ToString()), strNombrePagina))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strBuscar = blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                        strNuevo = blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                        strEditar = blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                        strEliminar = blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();

                        if (strBuscar == "False")
                        {
                            imgbBuscar.Visible = false;
                        }
                        if (strNuevo == "False")
                        {
                            imgbNuevo.Visible = false;
                        }
                        if (strEditar == "False")
                        {
                            imgbEditar.Visible = false;
                            imgbGravar.Visible = false;
                        }
                        if (strEliminar == "False")
                        {
                            imgbEliminar.Visible = false;
                        }

                    }
                }
                blPaicma.Termina();
            }
        }
        else
        {
            Response.Redirect("@dmin/frmLogin.aspx");
        }
    }

    public void fntCargarGrillaDocumentos( int intOP, string strDocumento, int intIdUsuario)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaDocumentosCargados("strDsDocumentos", intOP, strDocumento, intIdUsuario))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvDocumento.DataMember = "strDsDocumentos";
                    gvDocumento.DataSource = blPaicma.myDataSet;
                    gvDocumento.DataBind();
                    lblErrorGv.Text = string.Empty;
                }
                else
                {
                    gvDocumento.DataMember = "strDsDocumentos";
                    gvDocumento.DataSource = blPaicma.myDataSet;
                    gvDocumento.DataBind();
                    lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
            }
            blPaicma.Termina();
        }
    }

    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles(false);
        pnlAccionesDocumento.Visible = true;
        imgbEncontrar.Visible = true;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = false;
        imgbCancelar.Visible = true;
        pnlDocumentoBusqueda.Visible = true;
        fntCargarUsuario();
        txtNombreDocumento.Text = string.Empty;
        
    }

    public void fntInactivarPaneles(Boolean bolValor)
    {
        pnlAccionesDocumento.Visible = bolValor;
        pnlDocumentoBusqueda.Visible = bolValor;
        PnlDocumentoGrilla.Visible = bolValor;
        pnlEliminacion.Visible = bolValor;
        pnlMenuDocumento.Visible = bolValor;
        pnlRespuesta.Visible = bolValor;
    }

    public void fntCargarUsuario()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaSoloUsuario_bol("strDsUsuario"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlUsuario.DataMember = "strDsUsuario";
                    ddlUsuario.DataSource = blPaicma.myDataSet;
                    ddlUsuario.DataValueField = "segusu_Id";
                    ddlUsuario.DataTextField = "segusu_login";
                    ddlUsuario.DataBind();
                    ddlUsuario.Items.Insert(ddlUsuario.Attributes.Count, "Seleccione...");

                }
            }
            blPaicma.Termina();
        }
    }

    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        string strRuta = "frmDocumentos.aspx";
        Response.Redirect(strRuta);
    }

    protected void imgbEncontrar_Click(object sender, ImageClickEventArgs e)
    {
        int intIdUsuario = 0;
        if (ddlUsuario.SelectedValue != "Seleccione...")
        {
            intIdUsuario = Convert.ToInt32(ddlUsuario.SelectedValue.ToString());
        }
        fntCargarGrillaDocumentos(1, txtNombreDocumento.Text.Trim(), intIdUsuario);

        fntInactivarPaneles(false);
        pnlMenuDocumento.Visible = true;
        PnlDocumentoGrilla.Visible = true;
    }

    protected void gvDocumento_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strRutaServidor = string.Empty;
        string strFolder = string.Empty;
        string strNombreDocumentoAuxiliar = string.Empty;
        string strNombreDocumento = string.Empty;

        if (e.CommandName == "Ver")
        {
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvDocumento.Rows[intIndex];
            TableCell Item = selectedRow.Cells[2];

            try
            {
                strNombreDocumento = Item.Text;
                if (strNombreDocumento.Trim() != string.Empty)
                {
                    strFolder = "Archivos/";
                    strRutaServidor = System.Configuration.ConfigurationManager.AppSettings["RutaServerDinamico"];
                    strRutaServidor = strRutaServidor + strFolder + strNombreDocumento;
                    Response.Write("<script language='JavaScript'>window.open('" + strRutaServidor + "')</script>");
                    lblErrorGv.Text = string.Empty;
                }
                else
                {
                    lblErrorGv.Text = "No hay un documento asociado en el registro de seguimiento.";
                }
            }
            catch
            { 
            }
        }
    }

    protected void gvDocumento_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocumento.PageIndex = e.NewPageIndex;
        fntCargarGrillaDocumentos(0,string.Empty,0);
     
    }


    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {

    }
}