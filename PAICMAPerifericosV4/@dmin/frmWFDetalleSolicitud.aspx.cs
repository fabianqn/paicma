﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmWFDetalleSolicitud : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                lblObsAdmin.Visible = false;
                lblObsAdminLega.Visible = false;
                lblObsCoord.Visible = false;
                lblObsCoordLega.Visible = false;

                //Label12.Visible = false;
                //Label12.Text = "Observacion Obligatoria";
                this.fillSolicitudes();
                this.cargarLineasIntervencion();
                this.FillPlanesAccion();
                this.FillSitiosIntervencion();
                
                this.FillSoportes();
                this.CargarPermisos();
            }

        }

  
    }

    public void CargarPermisos()
    {
        int idUsuarioSolicitud = Convert.ToInt32(Session["IdUsuarioCreadorSolicitud"]);
        Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        int EstadoActualSolicitud = Convert.ToInt32(Session["EstadoActualSolicitud"]);
        string coordinacionUsuario = string.Empty;
        if (EstadoActualSolicitud == EstadosSolicitud.PorAprobarCoordinador || EstadoActualSolicitud == EstadosSolicitud.LegPorAprobarCoordinador)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarCoordinacionXUsuario("datosCoordinacionSegunUsuario", idUsuarioSolicitud))
                {
                    if (blPaicma.myDataSet.Tables["datosCoordinacionSegunUsuario"].Rows.Count > 0)
                    {
                        coordinacionUsuario = this.blPaicma.myDataSet.Tables[0].Rows[0]["idCoordinacionRef"].ToString();
                        if (blPaicma.fntConsultarSiCoordinador("datosSiEsCoordinador", Convert.ToInt32(Session["IdUsuario"]), coordinacionUsuario))
                        {
                            if (blPaicma.myDataSet.Tables["datosSiEsCoordinador"].Rows.Count > 0)
                            { 

                                 if (EstadoActualSolicitud == EstadosSolicitud.PorAprobarCoordinador){
                                     lblObsCoord.Visible = true;
                                     txtObservacion.Visible = true;
                                     txtObservacion.Enabled = true;
                                     validadorObservacion.Enabled = true;

                                 }else{
                                     lblObsCoordLega.Visible = true;
                                     txtObservacionCoodinacionLega.Visible = true;
                                     validatorCoordinadorLega.Enabled = true;
                                     txtObservacionCoodinacionLega.Enabled = true;
                                     btnContinuar.Visible = false;
                                 }
                                //Se puede mostrar los botones de aprobar o rechazar por coordinador
                                btnAprobarCoord.Visible = true;
                                btnRechazarCoord.Visible = true;
                                btnAsignarNumero.Visible = false;
                            }
                            else
                            {
                                //No se deben mostrar los botones de aprobar o rechazar por coordinador
                                lblObsCoord.Visible = false;
                                lblObsCoordLega.Visible = false;
                                txtObservacion.Visible = false;
                                validadorObservacion.Enabled = false;
                                txtObservacionCoodinacionLega.Visible = false;
                                validatorCoordinadorLega.Enabled = false;
                                btnAprobarCoord.Visible = false;
                                btnRechazarCoord.Visible = false;
                                btnAsignarNumero.Visible = false;
                            }
                        }
                        else
                        {
                            //no se pudo consultar si era coordinador o no
                        }

                    }
                    else
                    {
                        //No trajo ningun dato la consulta, usuario no existe como coordinador
                    }

                }
                else
                {
                    //error al consultar coordinacion por usuario
                }
            }
            else
            {
                //no se pudo conectar a la base de datos
            }
            blPaicma.Termina();
        }
        //Activar botones de Admin
        if (EstadoActualSolicitud == EstadosSolicitud.PorAprobarAdministrador || EstadoActualSolicitud == EstadosSolicitud.LegPorAprobarAdministrador)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarSiAdministrador("datosSiEsAdmin", Convert.ToInt32(Session["IdUsuario"])))
                {
                    if (blPaicma.myDataSet.Tables["datosSiEsAdmin"].Rows.Count > 0)
                    {

                         if (EstadoActualSolicitud == EstadosSolicitud.PorAprobarAdministrador) {
                                lblObsAdmin.Visible = true;
                                txtObservacionAdministrador.Visible = true;
                                txtObservacionAdministrador.Enabled = true;
                                validatorAdmin.Enabled = true;
                                txtObservacion.Text = ViewState["observacionCoordinador"].ToString();
                                lblObsCoord.Visible = true;
                                txtObservacion.Visible = true;
                                txtObservacion.Enabled = false;


                         }else{
                             lblObsAdminLega.Visible = true;
                             txtObservacionAdministradorLega.Visible = true;
                             validatorAdminLega.Enabled = true;
                             txtObservacion.Text = ViewState["observacionCoordinador"].ToString();
                             txtObservacionCoodinacionLega.Text = ViewState["observacionCoordinadorLega"].ToString();
                             txtObservacionCoodinacionLega.Visible = true;
                             txtObservacionCoodinacionLega.Enabled = false;
                             lblObsCoord.Visible = true;
                             txtObservacion.Visible = true;
                             txtObservacion.Enabled = false;
                             btnContinuar.Visible = false;
                             txtObservacionAdministradorLega.Enabled = true;
                         }  
                        btnAprobarAdmin.Visible=true;
                        btnRechazarAdmin.Visible=true;
                        btnAsignarNumero.Visible = false;

                    }else{
                        //no debe activar los botones

                        lblObsAdmin.Visible = false;
                        txtObservacionAdministrador.Visible = false;
                        validatorAdmin.Enabled = false;

                        lblObsAdminLega.Visible = false;
                        txtObservacionAdministradorLega.Visible = false;
                        validatorAdminLega.Enabled = false;

                        //Label12.Visible = false;
                        //txtObservacion.Visible = false;
                        //validadorObservacion.Enabled = false;

                        btnAprobarAdmin.Visible = false;
                        btnRechazarAdmin.Visible = false;
                        btnAsignarNumero.Visible = false;

                    }
                }else{
                    //error no pudo consultar si es admin
                }
            }
            else 
            {
                //no se puede conectar con la base de datos
            }
        }
        else if (EstadoActualSolicitud == EstadosSolicitud.faltaNumero)
        {
            // mostrar boton para ingresar 
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarSiAdministrador("datosSiEsAdmin", Convert.ToInt32(Session["IdUsuario"])))
                {
                    if (blPaicma.myDataSet.Tables["datosSiEsAdmin"].Rows.Count > 0)
                    {
                        btnAsignarNumero.Visible = true;
                    }
                    else
                    {
                        btnAsignarNumero.Visible = false;
                    }
                }
            }
        }

        blPaicma.Termina();

        if (EstadoActualSolicitud == EstadosSolicitud.LegIncompletaPlanes) { 
            if (idUsuarioSolicitud == Convert.ToInt32(Session["IdUsuario"]))
            {
                btnGuardarSalir.Visible = true;
                btnGuardarCont.Visible = true;
            }
        }

        // valida con el estado se difierente a falta numero 
        if (EstadoActualSolicitud > EstadosSolicitud.LegIncompletaPlanes && EstadoActualSolicitud != EstadosSolicitud.faltaNumero)
        {
            btnContinuar.Visible = true;
        }
        if (EstadoActualSolicitud == EstadosSolicitud.Anulada || EstadoActualSolicitud == EstadosSolicitud.LegalizacionCompletaCerrado)
        {
            btnAnular.Visible = false;
            btnAsignarNumero.Visible = false;
            lblObsAnula.Visible = true;
          
            
            txtObservacion.Visible = true;
            txtObservacion.Enabled = false;

            lblObsAdmin.Visible = true;
            lblObsAdminLega.Visible = true;
            lblObsCoord.Visible = true;
            lblObsCoordLega.Visible = true;

            txtObservacionAdministrador.Visible = true;
            txtObservacionAdministrador.Enabled = false;

            txtObservacionCoodinacionLega.Visible = true;
            txtObservacionCoodinacionLega.Enabled = false;

            txtObservacionAdministradorLega.Visible = true;
            txtObservacionAdministradorLega.Enabled = false;

            txtObservacionAnulacion.Visible = true;
            txtObservacionAnulacion.Enabled = false;

            validatorAnula.Enabled = false;
            btnAprobarAdmin.Visible = false;
            btnRechazarAdmin.Visible = false;
            btnAsignarNumero.Visible = false;
            btnContinuar.Visible = false;
        }


        if (txtObservacion.Text != string.Empty)
        {
            lblObsCoord.Visible = true;
            txtObservacion.Visible = true;
        }

        if (txtObservacionAdministrador.Text != string.Empty)
        {
            lblObsAdmin.Visible = true;
            txtObservacionAdministrador.Visible = true;
        }

        if (txtObservacionCoodinacionLega.Text != string.Empty)
        {
            lblObsCoordLega.Visible = true;
            txtObservacionCoodinacionLega.Visible = true;
        }

        if (txtObservacionAdministradorLega.Text != string.Empty)
        {
            lblObsAdminLega.Visible = true;
            txtObservacionAdministradorLega.Visible = true;
        }







    }


    public void FillPlanesAccion()
    {
        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarPlanes("datosPlanesAccion", idSolicitud, "", 0))
                {
                    if (blPaicma.myDataSet.Tables["datosPlanesAccion"].Rows.Count > 0)
                    {
                        this.gvPlanAccionDetalle.DataMember = "datosPlanesAccion";
                        this.gvPlanAccionDetalle.DataSource = this.blPaicma.myDataSet;
                        this.gvPlanAccionDetalle.DataBind();
                    }
                }
                else
                {
                    //Imposible Consultar lista de planes
                    lblRespuesta.Text = "Se ha presentado un problema al consultar lista de planes, intente de nuevo";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    Session["OperacionSolicitud"] = "ErrorSalir";
                }
            }
            else
            {
                //No pudo conectar a la base de datos
                lblRespuesta.Text = "Se ha presentado un problema al consultar lista de planes, no se ha podido conectar a la base de datos";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";
            }
            blPaicma.Termina();
        }
        else
        {
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
    }


    public void FillSitiosIntervencion()
    {
        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarSitios("datosSitiosInter", idSolicitud, "", 0))
                {
                    if (blPaicma.myDataSet.Tables["datosSitiosInter"].Rows.Count > 0)
                    {
                        this.gvSitiosIntervencion.DataMember = "datosSitiosInter";
                        this.gvSitiosIntervencion.DataSource = this.blPaicma.myDataSet;
                        this.gvSitiosIntervencion.DataBind();
                    }
                }
                else
                {
                    //Imposible Consultar lista de planes
                    lblRespuesta.Text = "Se ha presentado un problema al consultar los sitios de intervención, intente de nuevo";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    Session["OperacionSolicitud"] = "ErrorSalir";
                }
            }
            else
            {
                //No pudo conectar a la base de datos
                lblRespuesta.Text = "Se ha presentado un problema al consultar lista de planes, no se ha podido conectar a la base de datos";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";
            }
            blPaicma.Termina();

        }
    }

    public void FillSoportes()
    {
        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarSoporte("datosSoporteComision", idSolicitud, "", 0))
                {
                    if (blPaicma.myDataSet.Tables["datosSoporteComision"].Rows.Count > 0)
                    {
                        this.gvSoporteSolicitud.DataMember = "datosSoporteComision";
                        this.gvSoporteSolicitud.DataSource = this.blPaicma.myDataSet;
                        this.gvSoporteSolicitud.DataBind();
                    }
                }
                else
                {
                    //Imposible Consultar lista de planes
                }
            }
            else
            {
                //No pudo conectar a la base de datos
            }
            blPaicma.Termina();
        }

    }

    public void fillSolicitudes()
    {
        Session["IdUsuarioCreadorSolicitud"] = 0;
        Session["EstadoActualSolicitud"] = 0;
        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarSolicitud("datosSolicitudSeleccionada", "id", idSolicitud))
                {
                    if (blPaicma.myDataSet.Tables["datosSolicitudSeleccionada"].Rows.Count > 0)
                    {
                        txtNumeroAprobacion.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["numeroAprobacion"].ToString();
                        txtRadicacion.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["IdentificadorSolicitud"].ToString();
                        ddlLineaIntervencion.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0]["idLineaIntervencion"].ToString();
                        txtNombreFuncionario.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["nombreFuncionario"].ToString();
                        txtObjeto.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["objetoComision"].ToString();
                        txtFechaInicio.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["fechaInicio"].ToString();
                        txtFechaFin.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["fechaFin"].ToString();
                        ViewState["observacionCoordinador"] = this.blPaicma.myDataSet.Tables[0].Rows[0]["observacionCoordinador"].ToString();
                        ViewState["observacionAdmin"] = this.blPaicma.myDataSet.Tables[0].Rows[0]["observacionAdministracion"].ToString();
                        ViewState["observacionCoordinadorLega"] = this.blPaicma.myDataSet.Tables[0].Rows[0]["observacionCoordinadorLega"].ToString();
                        ViewState["observacionAdminLega"] = this.blPaicma.myDataSet.Tables[0].Rows[0]["observacionAdministradorLega"].ToString();
                        ViewState["observacionAnulacion"] = this.blPaicma.myDataSet.Tables[0].Rows[0]["observacionAnulacion"].ToString();

                        txtObservacion.Text = ViewState["observacionCoordinador"].ToString();
                        txtObservacionAdministrador.Text = ViewState["observacionAdmin"].ToString();
                        txtObservacionCoodinacionLega.Text = ViewState["observacionCoordinadorLega"].ToString();
                        txtObservacionAdministradorLega.Text = ViewState["observacionAdminLega"].ToString();
                        txtObservacionAnulacion.Text = ViewState["observacionAnulacion"].ToString();

                        txtNumeroDias.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["numeroDias"].ToString();
                        //Datos para los permisos
                        Session["IdUsuarioCreadorSolicitud"] = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0]["idUsuarioCreacion"]);
                        Session["EstadoActualSolicitud"] = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0]["idEstadoActual"]);
                        chkTiqueteAereo.Checked = Convert.ToBoolean(this.blPaicma.myDataSet.Tables[0].Rows[0]["tiquetesAereos"]);
                        txtRutaAerea.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["rutaAerea"].ToString();
                        
                        chkPorcionTerrestre.Checked = Convert.ToBoolean(this.blPaicma.myDataSet.Tables[0].Rows[0]["porcionTerrestre"]);
                        txtRutaTerrestre.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["rutaPorTerrestre"].ToString();
                        txtValorRutaT.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["valorRutaPorTerrestre"].ToString();

                        chkViaticos.Checked = Convert.ToBoolean(this.blPaicma.myDataSet.Tables[0].Rows[0]["requiereViaticos"]);
                        txtValorDiario.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["valorDiarioViatico"].ToString();

                        txtTotalViaticos.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["totalViaticos"].ToString();
                        txtValorDiario.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["valorDiarioViatico"].ToString();
                        ddlMonedaValor.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0]["monedaViatico"].ToString();
                        ddlValorTotal.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0]["monedaViatico"].ToString();
                        ddlMonedaValorRutaT.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0]["monedaViatico"].ToString();

                        int estadoActual =Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0]["idEstadoActual"]);
                        if (estadoActual < EstadosSolicitud.AprobadoPorDARPLegalizar)
                            btnAnular.Visible = true;
                        else
                            btnAnular.Visible = false;
                        
              
                           

                    }
                    else
                    {
                        lblRespuesta.Text = "Se ha presentado un problema al consultar datos de la solicitud, intente de nuevo";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        Session["OperacionSolicitud"] = "ErrorSalir";
                    }
                }
                else
                {
                    lblRespuesta.Text = "Se ha presentado un problema al consultar datos de la solicitud, intente de nuevo";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    Session["OperacionSolicitud"] = "ErrorSalir";
                }
            }
            else
            {
                lblRespuesta.Text = "Se ha presentado un problema al consultar datos de la solicitud, intente de nuevo";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";
            }
        }
        else
        {
            lblRespuesta.Text = "Se ha presentado un problema al consultar datos de la solicitud, intente de nuevo";
            principal.Visible = false;
            pnlRespuesta.Visible = true;
            Session["OperacionSolicitud"] = "ErrorSalir";
        }
        this.blPaicma.Termina();


        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarLegalizacion("strLegalizacion", "solicitud", Convert.ToInt64(Session["IdSolicitudRef"]),0,""))
            {
                if (blPaicma.myDataSet.Tables["strLegalizacion"].Rows.Count > 0)
                {
                    Session["IdLegalizacionRef"] = Convert.ToInt64(this.blPaicma.myDataSet.Tables[0].Rows[0]["idLegalizacion"]);
                    Session["PorcentajeAvance"] = Convert.ToDecimal(this.blPaicma.myDataSet.Tables[0].Rows[0]["cumplimientoPorcentaje"]);
                    txtCumplimiento.Text = Session["PorcentajeAvance"].ToString();
                     FillPlanesAccionLeg();
                     FillCompromisosLeg();
                     FillBeneficiariosLeg();
                     FillSoportesLeg();
                }
                else
                {
                    Session["IdLegalizacionRef"] = 0;
                    Session["PorcentajeAvance"] = 0;
                    txtCumplimiento.Text = Session["PorcentajeAvance"].ToString();
                }
            }
            else
            {
                //Session["IdLegalizacionRef"] = 0;
                //Session["PorcentajeAvance"] = 0;
                //txtCumplimiento.Text = Session["PorcentajeAvance"].ToString();
            }
        }
    }

    public void cargarLineasIntervencion()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarPorEstado("datosLineasIntevencion", "WFSol_LineaIntervencion", "estatusLinea", "ACT", "nombreLinea"))
            {
                ddlLineaIntervencion.DataMember = "datosLineasIntevencion";
                ddlLineaIntervencion.DataSource = blPaicma.myDataSet.Tables["datosLineasIntevencion"];
                ddlLineaIntervencion.DataValueField = "idLineaIntervencion";
                ddlLineaIntervencion.DataTextField = "nombreLinea";
                ddlLineaIntervencion.DataBind();
                ddlLineaIntervencion.Items.Insert(ddlLineaIntervencion.Attributes.Count, new ListItem("Seleccione...", ""));
            }
            else
            {
                ddlLineaIntervencion.Items.Insert(ddlLineaIntervencion.Attributes.Count, new ListItem("Seleccione...", ""));
            }
        }
        else
        {
            //no se ha podido conectar con la base de datos.
        }
        blPaicma.Termina();

    }

   
    protected void gvPlanAccionDetalle_PreRender(object sender, EventArgs e)
    {
        if (gvPlanAccionDetalle.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvPlanAccionDetalle.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvPlanAccionDetalle.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvPlanAccionDetalle.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
    protected void gvSitiosIntervencion_PreRender(object sender, EventArgs e)
    {
        if (gvSitiosIntervencion.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvSitiosIntervencion.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvSitiosIntervencion.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvSitiosIntervencion.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
   
    protected void gvSoporteSolicitud_PreRender(object sender, EventArgs e)
    {
        if (gvSoporteSolicitud.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvSoporteSolicitud.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvSoporteSolicitud.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvSoporteSolicitud.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }

    

    protected void gvSoporteSolicitud_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int intIndex = Convert.ToInt32(e.CommandArgument);
        GridViewRow selectedRow = gvSoporteSolicitud.Rows[intIndex];
        TableCell Item = selectedRow.Cells[0];
        Int64 idSoporte = Convert.ToInt64(Item.Text);
        if (e.CommandName == "VerSoporte")
        {
            //Session["operacionSolicitud"] = "modificar";
            //base.Response.Redirect("frmWFNuevaSolicitud.aspx");

            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarSoporteXId("datosSoporteComisionXid", "", idSoporte))
                {

                    string Archivonombre = blPaicma.myDataSet.Tables["datosSoporteComisionXid"].Rows[0]["rutaArchivo"].ToString();
                    string simbolo = Archivonombre.Replace(@"\", "/");

                    string nombredeArchivo = simbolo.Split('/').Last();

                    //   Download(blPaicma.myDataSet.Tables["datosSoporteComisionXid"].Rows[0]["rutaArchivo"].ToString());
                    Download(nombredeArchivo, @"~\Archivos\");
                }
            }






        }
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        string operacionActual = Session["OperacionSolicitud"].ToString();
        if (operacionActual == "GuardarContinuar")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Session["soloVerSolicitud"] = false;
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "GuardarSalir")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "ErrorSalir")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "Inicio")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
        if (operacionActual == "Anular")
        {
            Session["OperacionSolicitud"] = "Inicio";
            Response.Redirect("frmWFSolicitudComision.aspx");
        }




    }
    protected void btnGuardarCont_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmWFValidarPlanes.aspx");
        //Response.Redirect("frmWFCompromisos.aspx");
    }
    protected void btnGuardarSalir_Click(object sender, EventArgs e)
    {
        Session["OperacionSolicitud"] = "GuardarSalir";
    }


    protected void btnAprobarCoord_Click(object sender, EventArgs e)
    {
        long idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        int EstadoActualSolicitud = Convert.ToInt32(Session["EstadoActualSolicitud"]);
        int ProximoEstado = 0;
        string tablaGuardar = "";
        string ObservacionGuardar = string.Empty;
        if (EstadoActualSolicitud == EstadosSolicitud.PorAprobarCoordinador)
        {
            ProximoEstado = EstadosSolicitud.PorAprobarAdministrador;
            tablaGuardar = "observacionCoordinador";
            ObservacionGuardar = txtObservacion.Text;
        }

        if (EstadoActualSolicitud == EstadosSolicitud.LegPorAprobarCoordinador)
        {
            ProximoEstado = EstadosSolicitud.LegPorAprobarAdministrador;
            tablaGuardar = "observacionCoordinadorLega";
            ObservacionGuardar = txtObservacionCoodinacionLega.Text;
        }
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, ProximoEstado, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
            {
                if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", ProximoEstado, "WFSol_SolicitudComision", "idEstadoActual"))
                {
                    if (blPaicma.fntActualizarObservacionCordinador_bol(idSolicitud, ObservacionGuardar, tablaGuardar))
                    {
                        lblRespuesta.Text = "Solicitud Aceptada";
                        pnlRespuesta.Visible = true;
                        principal.Visible = false;
                        Session["OperacionSolicitud"] = "GuardarContinuar";
                        StringBuilder mailBody = new StringBuilder();
                        mailBody.AppendFormat(@"<html> <head>");
                        mailBody.AppendFormat(@"</head> <body>");
                        mailBody.AppendFormat(@"<div style=' width: 98%;'>");
                        mailBody.AppendFormat("<h1 style='text-align: center;color: #880404;'>Nueva Solicitud </h1> </br>");
                        mailBody.AppendFormat("<h1 style='text-align: center;' > --------------Detalle-------------- </h1>");
                        mailBody.AppendFormat(" idSolicitud: {0}", idSolicitud);

                        mailBody.AppendFormat("<br />");
                        mailBody.AppendFormat("<p>  se  genero una solicitud </p>");
                        mailBody.AppendFormat(@"</div>");
                        mailBody.AppendFormat(@"</body> </html>");
                        StringBuilder correosConcatenar = new StringBuilder();
                        DataTable correos = new DataTable();

                        if (blPaicma.fntUsuariosAdministradorCorreo_bol("CorreosCoodinadores"))
                        {

                            correos = blPaicma.myDataSet.Tables["CorreosCoodinadores"];


                            for (int i = 0; i < correos.Rows.Count; i++)
                            {
                                correosConcatenar.Append(correos.Rows[i]["segusu_Correo"].ToString() + ",");
                            }
                            string correosfinal = correosConcatenar.ToString().TrimEnd(',');
                            EnviarMail("Nueva Solicitud ", mailBody.ToString(), correosfinal);
                        }
                        else
                        {
                            //No consulta coordinador 
                        }

                    }
                    else
                    {
                        pnlRespuesta.Visible = true;
                        principal.Visible = false;
                        Session["OperacionSolicitud"] = "ErrorSalir";
                        //Fallo actualizar estado en la comision
                    }
                   

                }
                else
                {
                    pnlRespuesta.Visible = true;
                    principal.Visible = false;
                    Session["OperacionSolicitud"] = "ErrorSalir";
                    //Fallo actualizar estado en la comision
                }
            }
            else
            {
                pnlRespuesta.Visible = true;
                principal.Visible = false;
                Session["OperacionSolicitud"] = "ErrorSalir";
                //fallo actualizar estado Solicitud
            }
        }
        blPaicma.Termina();
    }
    protected void btnRechazarCoord_Click(object sender, EventArgs e)
    {
        long idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        int EstadoActualSolicitud = Convert.ToInt32(Session["EstadoActualSolicitud"]);
        int ProximoEstado = 0;
        string tablaGuardar = "";
        string observacionGuardar = string.Empty;
        if (EstadoActualSolicitud == EstadosSolicitud.PorAprobarCoordinador)
        {
            ProximoEstado = EstadosSolicitud.RechazadaCoordinador;
            tablaGuardar = "observacionCoordinador";
            observacionGuardar = txtObservacion.Text;
        }

        if (EstadoActualSolicitud == EstadosSolicitud.LegPorAprobarCoordinador)
        {
            ProximoEstado = EstadosSolicitud.LegRechazadaCoordinador;
            tablaGuardar = "observacionCoordinadorLega";
            observacionGuardar = txtObservacionCoodinacionLega.Text;
        }
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, ProximoEstado, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
            {
                if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", ProximoEstado, "WFSol_SolicitudComision", "idEstadoActual"))
                {
                    if (blPaicma.fntActualizarObservacionCordinador_bol(idSolicitud, observacionGuardar, tablaGuardar))
                    {
                        lblRespuesta.Text = "Solicitud rechazada";
                        pnlRespuesta.Visible = true;
                        principal.Visible = false;
                        Session["OperacionSolicitud"] = "GuardarContinuar";

                        enviarCorreoRechazaCoordinador();
                    }
                    else
                    {
                        lblRespuesta.Text = "Error en el servidor ";
                        pnlRespuesta.Visible = true;
                        principal.Visible = false;
                        Session["OperacionSolicitud"] = "ErrorSalir";
                        //Fallo actualizar estado en la comision
                    }

                }
                else
                {
                    pnlRespuesta.Visible = true;
                    principal.Visible = false;
                    Session["OperacionSolicitud"] = "ErrorSalir";
                    //Fallo actualizar estado en la comision
                }
            }
            else
            {
                pnlRespuesta.Visible = true;
                principal.Visible = false;
                Session["OperacionSolicitud"] = "ErrorSalir";
                //fallo actualizar estado Solicitud
            }
        }
        blPaicma.Termina();
        
    }
    protected void btnAprobarAdmin_Click(object sender, EventArgs e)
    {
        long idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        int EstadoActualSolicitud = Convert.ToInt32(Session["EstadoActualSolicitud"]);
        int ProximoEstado = 0;
        string tablaGuardar = "";
        string observacionGuardar = string.Empty;
        if (EstadoActualSolicitud == EstadosSolicitud.PorAprobarAdministrador)
        {
            ProximoEstado = EstadosSolicitud.faltaNumero;
            tablaGuardar = "observacionAdministracion";
            observacionGuardar = txtObservacionAdministrador.Text;

        }
        if (EstadoActualSolicitud == EstadosSolicitud.LegPorAprobarAdministrador)
        {
            ProximoEstado = EstadosSolicitud.LegalizacionCompletaCerrado;
            tablaGuardar = "observacionAdministradorLega";
            observacionGuardar = txtObservacionAdministradorLega.Text;
        }

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, ProximoEstado, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
            {
                if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", ProximoEstado, "WFSol_SolicitudComision", "idEstadoActual"))
                {
                    if (blPaicma.fntActualizarObservacionAdministrador_bol(idSolicitud, observacionGuardar, tablaGuardar))
                    {
                        lblRespuesta.Text = "Cambios registrados exitosamente";
                        pnlRespuesta.Visible = true;
                        principal.Visible = false;
                        Session["OperacionSolicitud"] = "GuardarContinuar";
                    }
                    else
                    {
                        lblRespuesta.Text = "Ocurrio un problema al guardar la observacion";
                        pnlRespuesta.Visible = true;
                        principal.Visible = false;
                        Session["OperacionSolicitud"] = "ErrorSalir";
                        //Fallo actualizar estado en la comision
                    }
                }
                else
                {
                    lblRespuesta.Text = "Fallo al actualizar el estado en la Solicitud";
                    pnlRespuesta.Visible = true;
                    principal.Visible = false;
                    Session["OperacionSolicitud"] = "ErrorSalir";
                    //Fallo actualizar estado en la comision
                }
            }
            else
            {
                lblRespuesta.Text = "Fallo al registrar el estado en la Solicitud";
                pnlRespuesta.Visible = true;
                principal.Visible = false;
                Session["OperacionSolicitud"] = "ErrorSalir";
                //fallo actualizar estado Solicitud
            }
        }
        blPaicma.Termina();
    }
    protected void btnRechazarAdmin_Click(object sender, EventArgs e)
    {
        long idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        int EstadoActualSolicitud = Convert.ToInt32(Session["EstadoActualSolicitud"]);
        int ProximoEstado = 0;
        string tablaGuardar = "";
        string observacionGuardar = string.Empty;

        if (EstadoActualSolicitud == EstadosSolicitud.PorAprobarAdministrador)
        {
            ProximoEstado = EstadosSolicitud.RechazadaAdministrador;
            tablaGuardar = "observacionAdministracion";
            observacionGuardar = txtObservacionAdministrador.Text;
        }

        if (EstadoActualSolicitud == EstadosSolicitud.LegPorAprobarAdministrador)
        {
            ProximoEstado = EstadosSolicitud.LegRechazadaAdministrador;
            tablaGuardar = "observacionAdministradorLega";
            observacionGuardar = txtObservacionAdministradorLega.Text;
        }
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, ProximoEstado, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
            {
                if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", ProximoEstado, "WFSol_SolicitudComision", "idEstadoActual"))
                {
                    if (blPaicma.fntActualizarObservacionAdministrador_bol(idSolicitud, observacionGuardar, tablaGuardar))
                    {
                        lblRespuesta.Text = "Cambios registrados exitosamente";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        Session["OperacionSolicitud"] = "GuardarContinuar";
                        enviarCorreoRechazaAdmin();
                    }
                    else
                    {
                        lblRespuesta.Text = "Ocurrio un problema al guardar la observacion";
                        pnlRespuesta.Visible = true;
                        principal.Visible = false;
                        Session["OperacionSolicitud"] = "ErrorSalir";
                        //Fallo actualizar estado en la comision
                    }
                }
                else
                {
                    lblRespuesta.Text = "Fallo al actualizar el estado en la Solicitud";
                    pnlRespuesta.Visible = true;
                    principal.Visible = false;
                    Session["OperacionSolicitud"] = "ErrorSalir";
                    //Fallo actualizar estado en la comision
                }
            }
            else
            {
                lblRespuesta.Text = "Fallo al registrar el estado en la Solicitud";
                pnlRespuesta.Visible = true;
                principal.Visible = false;
                Session["OperacionSolicitud"] = "ErrorSalir";
                //fallo actualizar estado Solicitud
            }
        }
        blPaicma.Termina();
    }

    protected Boolean Validable()
    {
        if (Convert.ToInt32(Session["IdUsuarioCreadorSolicitud"]) == Convert.ToInt32(Session["IdUsuario"]))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    protected void btnContinuar_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmWFValidarPlanes.aspx");
    }




    //☻☻ metodo para enviar email 
    //private void EnviarMail(string asunto, string mensaje, string correosenviar)
    //{
    //    System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage();
    //    correo.From = new System.Net.Mail.MailAddress("soportesimiltech@gmail.com");
    //    string correos = correosenviar;
    //    correo.To.Add(correos);
    //    correo.Subject = asunto;
    //    correo.Body = mensaje;

    //    correo.IsBodyHtml = true;
    //    correo.Priority = System.Net.Mail.MailPriority.Normal;
    //    //
    //    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
    //    //
    //    //---------------------------------------------
    //    // Estos datos debes rellanarlos correctamente
    //    //---------------------------------------------
    //    smtp.Host = "smtp.gmail.com";
    //    smtp.Port = 587;
    //    smtp.Credentials = new System.Net.NetworkCredential("soportesimiltech@gmail.com", "$asdf12345");
    //    smtp.EnableSsl = true;
    //    try
    //    {
    //        smtp.Send(correo);
    //        string envioCorreo = "Mensaje enviado satisfactoriamente";
    //    }
    //    catch (Exception ex)
    //    {
    //        string envioCorreoEror = "ERROR: " + ex.Message;
    //    }

    //}

    public void EnviarMail(string strTitulo, string strTextoMensaje, string strCorreoTo)
    {
        string text = string.Empty;
        string displayName = string.Empty;
        string host = string.Empty;
        string password = string.Empty;
        //Guardar en Log
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaCorreo("strDsCorreo", 0) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                text = this.blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                displayName = this.blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                host = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                int port = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][2].ToString());
                password = this.blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                int num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][6].ToString());
                if (num == 1)
                {
                    System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
                    mailMessage.From = new System.Net.Mail.MailAddress(text, displayName);
                    mailMessage.To.Add(strCorreoTo);
                    mailMessage.Subject = strTitulo;
                    mailMessage.Body = strTextoMensaje;
                    mailMessage.IsBodyHtml = true;
                    System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();
                    smtpClient.Host = host;
                    smtpClient.Port = port;
                    smtpClient.EnableSsl = false;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new System.Net.NetworkCredential(text, password);
                    smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                    try
                    {
                        smtpClient.Send(mailMessage);
                        string envioCorreo = "Mensaje enviado satisfactoriamente";
                      
                    }
                    catch (Exception ex)
                    {
                        
                        string envioCorreoEror = "ERROR: " + ex.Message;
                    }
                }
            }
            this.blPaicma.Termina();
        }
    }



    /// <summary>
    /// ☻☻ metodo  para descargar el archivo  cuado se desconoce el repositorio 
    /// </summary>
    /// <param name="sFileName"></param>
    /// <param name="sFilePath"></param>
    public static void Download(string sFileName, string sFilePath)
    {
        HttpContext.Current.Response.ContentType = "APPLICATION/OCTET-STREAM";
        String Header = "Attachment; Filename=" + sFileName;
        HttpContext.Current.Response.AppendHeader("Content-Disposition", Header);
        System.IO.FileInfo Dfile = new System.IO.FileInfo(HttpContext.Current.Server.MapPath(sFilePath));
        HttpContext.Current.Response.WriteFile(Dfile.FullName + sFileName);
        HttpContext.Current.Response.End();
    }


    //☻☻  evento  para  abrir formulario que pide  el numero de legalizacion   de la solicitud 
    protected void btnAsignarNumero_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/@dmin/frmWFNumeroAutorizacion.aspx");
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/@dmin/frmWFSolicitudComision.aspx");
    }


  private void enviarCorreoRechazaCoordinador()
    {
        Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
       DataTable  solicitud = new DataTable();
       int IdPersonacreoSolicitud = 0;

       if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarSolicitudPorId_bol("CorroPersonaCreo", idSolicitud))
            {

                solicitud = blPaicma.myDataSet.Tables["CorroPersonaCreo"];
                IdPersonacreoSolicitud = Convert.ToInt32(solicitud.Rows[0]["idUsuarioCreacion"]);
            }
            else
            {
                //No consulta coordinador 
            }
        }
        else
        {
            //No pudo conectar a la base de datos.
        }
        //☻☻ mensaje para enviar 
        StringBuilder mailBody = new StringBuilder();
        mailBody.AppendFormat(@"<html> <head>");
        mailBody.AppendFormat(@"</head> <body>");
        mailBody.AppendFormat(@"<div style=' width: 98%;'>");
        mailBody.AppendFormat("<h1 style='text-align: center;color: #880404;'>Solicitud Rechazada por El Coordinador  </h1> </br>");
        mailBody.AppendFormat("<h1 style='text-align: center;' > --------------Detalle-------------- </h1>");
        mailBody.AppendFormat(" idSolicitud: {0}", idSolicitud);

        mailBody.AppendFormat("<br />");
        mailBody.AppendFormat("<p>  se  genero una solicitud </p>");
        mailBody.AppendFormat(@"</div>");
        mailBody.AppendFormat(@"</body> </html>");

        StringBuilder correosConcatenar = new StringBuilder();
        DataTable correos = new DataTable();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntUsuariosPorId("CorroPersonaCreo",IdPersonacreoSolicitud))
            {
                correos = blPaicma.myDataSet.Tables["CorroPersonaCreo"];
                correosConcatenar.Append(correos.Rows[0]["segusu_Correo"].ToString());
                EnviarMail("Nueva Solicitud ", mailBody.ToString(), correosConcatenar.ToString());
            }
            else
            {
                //No consulta coordinador 
            }
        }
        else
        {
            //No pudo conectar a la base de datos.
        }
        blPaicma.Termina();
    }



  private void enviarCorreoRechazaAdmin()
  {
      Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
      DataTable solicitud = new DataTable();
      int IdPersonacreoSolicitud = 0;

      if (blPaicma.inicializar(blPaicma.conexionSeguridad))
      {
          if (blPaicma.fntConsultarSolicitudPorId_bol("CorroPersonaCreo", idSolicitud))
          {

              solicitud = blPaicma.myDataSet.Tables["CorroPersonaCreo"];
              IdPersonacreoSolicitud = Convert.ToInt32(solicitud.Rows[0]["idUsuarioCreacion"]);
          }
          else
          {
              //No consulta coordinador 
          }
      }
      else
      {
          //No pudo conectar a la base de datos.
      }
      //☻☻ mensaje para enviar 
      StringBuilder mailBody = new StringBuilder();
      mailBody.AppendFormat(@"<html> <head>");
      mailBody.AppendFormat(@"</head> <body>");
      mailBody.AppendFormat(@"<div style=' width: 98%;'>");
      mailBody.AppendFormat("<h1 style='text-align: center;color: #880404;'>Solicitud Rechazada por El Adminstrador  </h1> </br>");
      mailBody.AppendFormat("<h1 style='text-align: center;' > --------------Detalle-------------- </h1>");
      mailBody.AppendFormat(" idSolicitud: {0}", idSolicitud);

      mailBody.AppendFormat("<br />");
      mailBody.AppendFormat("<p>  se  genero una solicitud </p>");
      mailBody.AppendFormat(@"</div>");
      mailBody.AppendFormat(@"</body> </html>");

      StringBuilder correosConcatenar = new StringBuilder();
      DataTable correos = new DataTable();
      if (blPaicma.inicializar(blPaicma.conexionSeguridad))
      {
          if (blPaicma.fntUsuariosPorId("CorroPersonaCreo", IdPersonacreoSolicitud))
          {
              correos = blPaicma.myDataSet.Tables["CorroPersonaCreo"];
              correosConcatenar.Append(correos.Rows[0]["segusu_Correo"].ToString());
              EnviarMail("Nueva Solicitud ", mailBody.ToString(), correosConcatenar.ToString());
          }
          else
          {
              //No consulta coordinador 
          }
      }
      else
      {
          //No pudo conectar a la base de datos.
      }
      blPaicma.Termina();
  }


    //☻☻ evento para  anular la solicitud  ( solo  la puede anular el usuario que la creo 
    protected void btnAnular_Click(object sender, EventArgs e)
    {
        if (btnAnular.Text == "Anular Solicitud")
        {
            txtObservacionAnulacion.Visible = true;
            validatorAnula.Enabled = true;
            btnAnular.Text = "Anular";
            lblObsAnula.Text = "Observacion Anulacion";
            lblObsAnula.Visible = true;
            txtObservacionAnulacion.Enabled = true;
        }
        else if(btnAnular.Text == "Anular" )
        {
        long idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        int EstadoActualSolicitud = Convert.ToInt32(Session["EstadoActualSolicitud"]);
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarEstadoSolicitud_bol(idSolicitud, EstadosSolicitud.Anulada, Convert.ToInt32(Session["IdUsuario"]), DateTime.Now, false))
            {
                if (blPaicma.fntActualizarEstado_bol(idSolicitud, "idSolicitud", EstadosSolicitud.Anulada, "WFSol_SolicitudComision", "idEstadoActual"))
                {

                    if (blPaicma.fntActualizarObservacionAnulacion_bol(idSolicitud, txtObservacionAnulacion.Text))
                    {
                        lblRespuesta.Text = "Se  Anulo la Solicitud";
                        principal.Visible = false;
                        pnlRespuesta.Visible = true;
                        Session["OperacionSolicitud"] = "Anular";
                    }
                    else
                    {
                        lblRespuesta.Text = "Ocurrio un problema al guardar la observacion";
                        pnlRespuesta.Visible = true;
                        principal.Visible = false;
                        Session["OperacionSolicitud"] = "ErrorSalir";
                        //Fallo actualizar estado en la comision

                    }


                }
                else
                {
                    //Fallo actualizar estado en la comision
                }
            }
            else
            {
                //fallo actualizar estado Solicitud
            }
        }
        else
        {
        }
        this.blPaicma.Termina();

        }
    }

    //☻☻ metodo para validar los planes de accion 
    public void FillPlanesAccionLeg()
    {
        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarPlanes("datosPlanesAccion", idSolicitud, "", 0))
                {
                    Session["PlanesAccionCargados"] = blPaicma.myDataSet.Tables["datosPlanesAccion"].Rows.Count;


                    if (blPaicma.myDataSet.Tables["datosPlanesAccion"].Rows.Count > 0)
                    {

                        this.gvPlanAccionElegir.DataMember = "datosPlanesAccion";
                        this.gvPlanAccionElegir.DataSource = this.blPaicma.myDataSet;
                        this.gvPlanAccionElegir.DataBind();
                    }
                }
                else
                {
                    //Imposible Consultar lista de planes
                    lblRespuesta.Text = "Se ha presentado un problema al consultar lista de planes, intente de nuevo";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    Session["OperacionValidarPlanes"] = "ErrorSalir";
                }
            }
            else
            {
                //No pudo conectar a la base de datos
                lblRespuesta.Text = "Se ha presentado un problema al consultar lista de planes, no se ha podido conectar a la base de datos";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionValidarPlanes"] = "ErrorSalir";
            }
            blPaicma.Termina();
        }
        else
        {
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
    }

    //☻☻ metodo para cargar los compromisos adquiridos 
    public void FillCompromisosLeg()
    {
        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idLegalizacion = Convert.ToInt64(Session["IdLegalizacionRef"]);
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarCompromisos("datosCompromiso", "0", idLegalizacion, 0))
                {
                    if (blPaicma.myDataSet.Tables["datosCompromiso"].Rows.Count > 0)
                    {
                        this.gvCompromiso.DataMember = "datosCompromiso";
                        this.gvCompromiso.DataSource = this.blPaicma.myDataSet;
                        this.gvCompromiso.DataBind();
                    }
                }
                else
                {
                    //Imposible Consultar lista de planes
                    lblRespuesta.Text = "Se ha presentado un problema al consultar lista de Compromisos, intente de nuevo";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    Session["OperacionSolicitud"] = "ErrorSalir";
                }
            }
            else
            {
                //No pudo conectar a la base de datos
                lblRespuesta.Text = "Se ha presentado un problema al consultar lista de Compromisos, no se ha podido conectar a la base de datos";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";

            }
            blPaicma.Termina();
        }
        else
        {
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
    }

    //☻☻ metodo para cargar los beneficiarios 
    public void FillBeneficiariosLeg()
    {
        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idLegalizacion = Convert.ToInt64(Session["IdLegalizacionRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarBeneficiarios("datosBenificiario", "0", idLegalizacion, 0))
                {
                    if (blPaicma.myDataSet.Tables["datosBenificiario"].Rows.Count > 0)
                    {
                        this.gvBeneficiario.DataMember = "datosBenificiario";
                        this.gvBeneficiario.DataSource = this.blPaicma.myDataSet;
                        this.gvBeneficiario.DataBind();
                    }
                }
                else
                {
                    //Imposible Consultar lista de planes
                    lblRespuesta.Text = "Se ha presentado un problema al consultar lista de Beneficiarios, intente de nuevo";
                    principal.Visible = false;
                    pnlRespuesta.Visible = true;
                    Session["OperacionSolicitud"] = "ErrorSalir";
                }
            }
            else
            {
                //No pudo conectar a la base de datos
                lblRespuesta.Text = "Se ha presentado un problema al consultar lista de Beneficiarios, no se ha podido conectar a la base de datos";
                principal.Visible = false;
                pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";

            }
            blPaicma.Termina();
        }
        else
        {
            Response.Redirect("frmWFSolicitudComision.aspx");
        }
    }

    //☻☻ metdo para cargar los soportes de legalizacion 
    public void FillSoportesLeg()
    {
        DataTable datasoporte = new DataTable();

        if (Session["IdSolicitudRef"].ToString() != "0")
        {
            Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarSoporteLegalizacion("datosSoporteComision", idSolicitud, "", 0))
                {
                    datasoporte = blPaicma.myDataSet.Tables["datosSoporteComision"];
                    if (blPaicma.myDataSet.Tables["datosSoporteComision"].Rows.Count > 0)
                    {


                        this.gvSoporteSolicitudleg.DataMember = "datosSoporteComision";
                        //this.gvSoporteSolicitud.DataSource = datasoporte;
                        //this.gvSoporteSolicitud.DataBind();
                    }
                    this.gvSoporteSolicitudleg.DataSource = datasoporte;
                    this.gvSoporteSolicitudleg.DataBind();
                }
                else
                {
                    //Imposible Consultar lista de planes

                }
            }
            else
            {
                //No pudo conectar a la base de datos
            }
            blPaicma.Termina();
        }

    }

    protected void gvSoporteSolicitudleg_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int intIndex = Convert.ToInt32(e.CommandArgument);
        GridViewRow selectedRow = gvSoporteSolicitudleg.Rows[intIndex];
        TableCell Item = selectedRow.Cells[0];
        Int64 idSoporte = Convert.ToInt64(Item.Text);
        if (e.CommandName == "VerSoporte")
        {
            //Session["operacionSolicitud"] = "modificar";
            //base.Response.Redirect("frmWFNuevaSolicitud.aspx");

            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarSoporteXIdLegalizacion("datosSoporteComisionXid", "", idSoporte))
                {

                    string Archivonombre = blPaicma.myDataSet.Tables["datosSoporteComisionXid"].Rows[0]["rutaArchivo"].ToString();
                    string simbolo = Archivonombre.Replace(@"\", "/");

                    string nombredeArchivo = simbolo.Split('/').Last();

                    //   Download(blPaicma.myDataSet.Tables["datosSoporteComisionXid"].Rows[0]["rutaArchivo"].ToString());
                    Download(nombredeArchivo, @"~\Archivos\");
                }
            }






        }
    }

    protected void gvPlanAccionElegir_PreRender(object sender, EventArgs e)
    {
        if (gvPlanAccionElegir.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvPlanAccionElegir.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvPlanAccionElegir.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvPlanAccionElegir.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
    protected void gvCompromiso_PreRender(object sender, EventArgs e)
    {
        if (gvCompromiso.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvCompromiso.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvCompromiso.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvCompromiso.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
    protected void gvBeneficiario_PreRender(object sender, EventArgs e)
    {
        if (gvBeneficiario.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvBeneficiario.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvBeneficiario.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvBeneficiario.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
    protected void gvSoporteSolicitudleg_PreRender(object sender, EventArgs e)
    {
        if (gvSoporteSolicitudleg.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvSoporteSolicitudleg.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvSoporteSolicitudleg.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvSoporteSolicitudleg.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }
}