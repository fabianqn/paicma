﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;

public partial class _dmin_frmDropdownCreate : System.Web.UI.Page
{
    static int contadorOpciones;
    private blSisPAICMA blPaicma = new blSisPAICMA();
    static List<Control> ListaOpciones = new List<Control>();
    static List<TextBox> ListaValores = new List<TextBox>();
    static List<TextBox> ListaDescripciones = new List<TextBox>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {

            }
            lblError.Visible = false;
            contadorOpciones = 0;
            TextBox tb = new TextBox();
            tb.ID = "value" + contadorOpciones;
            ListaValores = new List<TextBox>();
            ListaValores.Add(tb);

            TextBox tb1 = new TextBox();
            tb1.ID = "descripcion" + contadorOpciones;
            ListaDescripciones = new List<TextBox>();
            ListaDescripciones.Add(tb1);


            //AgregarControles(TextBox txtnombreCampo, DropDownList ddlTipo, Label lblNombre, Label lblTipo, Label lblLongitud, TextBox txtLongitud, Label lblTitulo, Label lblOrden, TextBox txtOrden, TextBox txtLabel)
            AgregarControles(ListaValores[contadorOpciones], ListaDescripciones[contadorOpciones], contadorOpciones.ToString());
            contadorOpciones++;
            LlenarTabla();
        }
        else
        {

            try
            {
                for (int i = 0; i < contadorOpciones; i++)
                {
                    //AgregarControles(TextBox txtnombreCampo, DropDownList ddlTipo, Label lblNombre, Label lblTipo, Label lblLongitud, TextBox txtLongitud, Label lblTitulo, Label lblOrden, TextBox txtOrden, TextBox txtLabel)
                    AgregarControles(ListaValores[i], ListaDescripciones[i], i.ToString());
                }

            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }
    }

    protected void LlenarTabla()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            //if (blPaicma.fntConsultaAdmTab_bol("strDsGrillaCampos", 0, 2, string.Empty, Convert.ToInt32(Session["intIdTabla"].ToString())))
            //{
            int intTablaRelacion = (int)Session["intNomTabCampos"];
            if (blPaicma.fntConsultaTodoPorId("strOpcionesCampos", "Form_OptionsList", "IdCampoRelacion", intTablaRelacion, "camDesc"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {

                    gvOpcionesDisponibles.DataMember = "strOpcionesCampos";
                    gvOpcionesDisponibles.DataSource = blPaicma.myDataSet;
                    gvOpcionesDisponibles.DataBind();
                    gvOpcionesDisponibles.Visible = true;
                    gvOpcionesDisponibles.Enabled = false;
                }
            }
        }
    }


    protected void AgregarControles(TextBox valorOpcion, TextBox descripcionOpcion, string valorConsecutivo)
    {
        try
        {
            HtmlTableRow fila = new HtmlTableRow();
            HtmlTableCell celda = new HtmlTableCell();
            HtmlTableCell celda2 = new HtmlTableCell();
            HtmlTableCell celda3 = new HtmlTableCell();
            HtmlTableCell celda4 = new HtmlTableCell();
           

            //Agrego Nombre del Campo
            celda.InnerText = "Valor " + valorConsecutivo;
            celda2.Controls.Add(valorOpcion);
            //Agrego Texto que llevara el label
            celda3.InnerText = "Descripción " + valorConsecutivo;
            celda4.Controls.Add(descripcionOpcion);
            //Agrego Combo Box de tipo
            


            fila.Cells.Add(celda);
            fila.Cells.Add(celda2);

            fila.Cells.Add(celda3);
            fila.Cells.Add(celda4);
            Opciones.Rows.Add(fila);

        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            lblError.Visible = true;
        }

    }

    

    protected void imgbCancelar_Click(object sender, EventArgs e)
    {
        string strRuta = "frmFormDinamico.aspx";
        Response.Redirect(strRuta);
    
    }

    protected void imgbGrabar_Click(object sender, EventArgs e)
    {
        int intTablaRelacion = (int)Session["intNomTabCampos"];
        bool sw = true;
        try
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            { 
                int i = 0;
                for (i = 0; i < ListaValores.Count(); i++)
                {
                    if (ListaValores[i].Text != string.Empty)
                    {
                        if (ListaDescripciones[i].Text != string.Empty)
                        {
                            if (blPaicma.fntInsertarOpcionesLista_bol(intTablaRelacion, ListaValores[i].Text, ListaDescripciones[i].Text) == false) {
                                sw = false;
                                break;
                            }
                        }
                    }
             
                }
                if (sw)
                {
                    HtmlMeta meta = new HtmlMeta();
                    meta.HttpEquiv = "Refresh";
                    meta.Content = "3;url=frmFormDinamico.aspx";
                    this.Page.Controls.Add(meta);
                    lblError.Visible = true;
                    lblError.Text = "Opciones Agregadas de forma Correcta, Sera redirigido en 3 segundos ";
                    lblError.Visible = true;
                }
                else
                {
                    HtmlMeta meta = new HtmlMeta();
                    meta.HttpEquiv = "Refresh";
                    meta.Content = "3;url=frmFormDinamico.aspx";
                    this.Page.Controls.Add(meta);
                    lblError.Visible = true;
                    lblError.Text = "Ha ocurrido un problema intente nuevamente, Sera redirigido en 3 segundos ";
                    lblError.Visible = true;

                }
            
            }
        }
        catch
        {


        }

    }

    protected void imgbNuevoCampo_Click(object sender, EventArgs e)
    {
        
        TextBox tb = new TextBox();
        tb.ID = "value" + contadorOpciones;
        ListaValores.Add(tb);

        TextBox tb1 = new TextBox();
        tb1.ID = "descripcion" + contadorOpciones;
        ListaDescripciones.Add(tb1);

        AgregarControles(ListaValores[contadorOpciones], ListaDescripciones[contadorOpciones], contadorOpciones.ToString());
        contadorOpciones++;
    }

  

}