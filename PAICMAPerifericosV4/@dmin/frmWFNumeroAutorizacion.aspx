﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmWFNumeroAutorizacion.aspx.cs" Inherits="_dmin_frmWFNumeroAutorizacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">


    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Aceptar" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>


        <div class="contacto2" id="principal" runat="server">
        <table style="width: 100%">
                <tbody>
                <tr style="text-align:center;">
                    <td> <h1 style="font-size:150%"> Asignar  número de aprobación a la solicitud </h1> </td>
                </tr>
                </tbody>  
        </table>
        <hr />
          
        <table>
            <tbody>
                <tr>
                    <td>  <label for="male">Id Solicitud :</label></td>
                    <td>   <asp:Label ID="LblIdSolicitud" runat="server" Text="Label"></asp:Label>    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label12" runat="server" Text="Numero de Legalizacion   :"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="TxtNumero" runat="server"></asp:TextBox>
                     

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                             ControlToValidate="TxtNumero"
                            ErrorMessage="Este campo es requerido." Enabled="true"
                            ForeColor="Red" Width="100%">  </asp:RequiredFieldValidator>
                      </td>
                </tr>
                <tr> 
                    <td> 
                       
                    </td>
                           
                </tr>
            </tbody>
        </table>
            <asp:Panel ID="panelBotones" runat="server">
                        <div class="Botones">
            <asp:Button ID="btnGuardarNumero" runat="server"  Text="Guardar" OnClick="btnGuardarNumero_Click"  />
            <asp:Button ID="btnGuardarSalir" runat="server"  Text="Cancelar" OnClick="btnGuardarSalir_Click" />
          
          </div>
            </asp:Panel>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">
</asp:Content>

