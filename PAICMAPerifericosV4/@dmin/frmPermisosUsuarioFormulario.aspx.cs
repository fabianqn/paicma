﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class _dmin_prueba : System.Web.UI.Page
{

    private blSisPAICMA blPaicma = new blSisPAICMA();
    private void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fntCargarListaUsuarios();
        }
    }
    public void fntCargarListaUsuarios()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarUsuarios_bol("strTodosLosUsuarios"))
            {
                ListBoxUsuarios.DataSource = blPaicma.myDataSet.Tables["strTodosLosUsuarios"];
                ListBoxUsuarios.DataValueField = "segusu_Id";
                ListBoxUsuarios.DataTextField = "segusu_login";
                ListBoxUsuarios.DataBind();
                   if (blPaicma.fntConsultarUsuariosPermitidos_bol("strUsuariosPermitidos", Convert.ToInt32(Session["intIdTabla"])))
                    {
                        ListBoxUsuariosSelecciondos.DataSource = blPaicma.myDataSet.Tables["strUsuariosPermitidos"];
                        ListBoxUsuariosSelecciondos.DataValueField = "usuper_segusu_Id";
                        ListBoxUsuariosSelecciondos.DataTextField = "segusu_login";
                        ListBoxUsuariosSelecciondos.DataBind();
                        Session["cantReg"] = ListBoxUsuariosSelecciondos.Items.Count;

                       for (int i = ListBoxUsuariosSelecciondos.Items.Count - 1; i >= 0; i--){
                               for (int j = ListBoxUsuarios.Items.Count - 1; j >= 0; j--){
                                   if (ListBoxUsuarios.Items[j].Value == ListBoxUsuariosSelecciondos.Items[i].Value){
                                        ListItem li = ListBoxUsuarios.Items[j];
                                        ListBoxUsuarios.Items.Remove(li);
                                   }        
                               }
                       }
                    }else{
                   }
                }else{
            }
        }
    }

    

    protected void btnMoveRight_Click(object sender, System.EventArgs e)
    {
        for (int i = ListBoxUsuarios.Items.Count - 1; i >= 0; i--)
        {
            if (ListBoxUsuarios.Items[i].Selected == true)
            {
                ListBoxUsuariosSelecciondos.Items.Add(ListBoxUsuarios.Items[i]);
                ListItem li = ListBoxUsuarios.Items[i];
                ListBoxUsuarios.Items.Remove(li);
            }
        }
    }



    protected void btnMoveLeft_Click(object sender, System.EventArgs e)
    {
        for (int i = ListBoxUsuariosSelecciondos.Items.Count - 1; i >= 0; i--)
        {
            if (ListBoxUsuariosSelecciondos.Items[i].Selected == true)
            {
                ListBoxUsuarios.Items.Add(ListBoxUsuariosSelecciondos.Items[i]);
                ListItem li = ListBoxUsuariosSelecciondos.Items[i];
                ListBoxUsuariosSelecciondos.Items.Remove(li);
            }
        }
    }


    public bool fntInsertarUsuarios_bol(ListBox ListaIdUsuarios, int idTabla)
    {

        bool respuesta = false;
        for (int i = ListaIdUsuarios.Items.Count - 1; i >= 0; i--)
        {

            if (blPaicma.fntInsertarPermisosUsuario_bol(Convert.ToInt32(ListaIdUsuarios.Items[i].Value), idTabla))
            {
                respuesta = true;
            }
            else
            {
                respuesta = false;
                break;
            }
            
        }
        return respuesta;
    }


    protected void imgbCancelar_Click(object sender, System.EventArgs e)
    {
        string strRuta = "frmFormDinamico.aspx";
        Response.Redirect(strRuta);
    }

    protected void imgbGravar_Click(object sender, System.EventArgs e)
    {
        imgbGravar.Enabled = false;
        if (ListBoxUsuariosSelecciondos.Items.Count > 0)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (Convert.ToInt32(Session["cantReg"]) > 0)
                {
                    if (blPaicma.fntEliminarPermisosUsuarios_bol(Convert.ToInt32(Session["intIdTabla"])))
                    {
                        if (fntInsertarUsuarios_bol(ListBoxUsuariosSelecciondos, Convert.ToInt32(Session["intIdTabla"])))
                        {
                            LabelMsgUsuarios.Visible = false;
                            mensajes.Visible = true;
                            LabelMsgError.Visible = false;
                            HtmlMeta meta = new HtmlMeta();
                            meta.HttpEquiv = "Refresh";
                            meta.Content = "5;url=frmFormDinamico.aspx";
                            this.Page.Controls.Add(meta);
                            LabelMsgCorrecto.Visible = true;
                            Label1.Text = "Sera redirigido en 5 segundos";
                            Label1.Visible = true;
                        }
                        else
                        {
                            LabelMsgUsuarios.Visible = false;
                            HtmlMeta meta = new HtmlMeta();
                            meta.HttpEquiv = "Refresh";
                            meta.Content = "5;url=frmFormDinamico.aspx";
                            this.Page.Controls.Add(meta);
                            mensajes.Visible = true;
                            LabelMsgError.Visible = true;
                            Label1.Text = "Sera redirigido en 5 segundos";
                            Label1.Visible = true;
                        }
                    }
                    else
                    {
                        LabelMsgUsuarios.Visible = false;
                        HtmlMeta meta = new HtmlMeta();
                        meta.HttpEquiv = "Refresh";
                        meta.Content = "5;url=frmFormDinamico.aspx";
                        this.Page.Controls.Add(meta);
                        mensajes.Visible = true;
                        LabelMsgError.Visible = true;
                        Label1.Text = "Sera redirigido en 5 segundos";
                        Label1.Visible = true;
                    }
                }
                else
                {
                    if (fntInsertarUsuarios_bol(ListBoxUsuariosSelecciondos, Convert.ToInt32(Session["intIdTabla"])))
                    {
                        LabelMsgUsuarios.Visible = false;
                        LabelMsgError.Visible = false;
                        HtmlMeta meta = new HtmlMeta();
                        meta.HttpEquiv = "Refresh";
                        meta.Content = "5;url=frmFormDinamico.aspx";
                        this.Page.Controls.Add(meta);
                        mensajes.Visible = true;
                        LabelMsgCorrecto.Visible = true;
                        Label1.Text = "Sera redirigido en 5 segundos";
                        Label1.Visible = true;
                    }
                    else
                    {
                        LabelMsgUsuarios.Visible = false;
                        HtmlMeta meta = new HtmlMeta();
                        meta.HttpEquiv = "Refresh";
                        meta.Content = "5;url=frmFormDinamico.aspx";
                        this.Page.Controls.Add(meta);
                        mensajes.Visible = true;
                        LabelMsgError.Visible = true;
                        Label1.Text = "Sera redirigido en 5 segundos";
                        Label1.Visible = true;
                    }
                }
            }
            else
            {
                        LabelMsgUsuarios.Visible = false;
                        LabelMsgError.Visible = false;
                        HtmlMeta meta = new HtmlMeta();
                        meta.HttpEquiv = "Refresh";
                        meta.Content = "5;url=frmFormDinamico.aspx";
                        this.Page.Controls.Add(meta);
                        mensajes.Visible = true;
                        LabelMsgBaseDatos.Visible = true;
                        Label1.Text = "Sera redirigido en 5 segundos";
                        Label1.Visible = true;
              
            }
        }
        else
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                    if (blPaicma.fntEliminarPermisosUsuarios_bol(Convert.ToInt32(Session["intIdTabla"])))
                    {
                        LabelMsgUsuarios.Visible = false;
                        mensajes.Visible = true;
                        LabelMsgError.Visible = false;
                        HtmlMeta meta = new HtmlMeta();
                        meta.HttpEquiv = "Refresh";
                        meta.Content = "5;url=frmFormDinamico.aspx";
                        this.Page.Controls.Add(meta);
                        LabelMsgCorrecto.Visible = true;
                        Label1.Text = "Sera redirigido en 5 segundos";
                        Label1.Visible = true;
                    
                    }
             }
            //imgbGravar.Enabled = true;
            //LabelMsgError.Visible = false;
            //mensajes.Visible = true;
            //LabelMsgUsuarios.Visible = true;
        }
    }
 }


