﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Data;



public partial class _dmin_frmWFCoordinacion : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }

            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                this.fntCargarListaUsuarios();
                this.fntCargarDatosCoordinacion();
                return;
            }
            base.Response.Redirect("@dmin/frmLogin.aspx");

        }
    }


    public void fntCargarListaUsuarios()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarUsuarios_bol("strUsuarios"))
            {
                ListBoxUsuarios.DataSource = blPaicma.myDataSet.Tables["strUsuarios"];
                ListBoxUsuarios.DataValueField = "segusu_Id";
                ListBoxUsuarios.DataTextField = "segusu_login";
                ListBoxUsuarios.DataBind();

                var sesionModo = Session["ModoCoordinacion"];
                if (sesionModo != null)
                {
                    if (sesionModo.ToString() == "Modificar")
                    {
                        string idCoordinacion = Session["idCoordinacionRef"].ToString();
                        //Consulta usuarios Normales
                        if (blPaicma.fntUsuariosCoordinacion_bol("strUsuarioRegular", idCoordinacion, false))
                        {
                            ListBoxUsuariosSelecciondos.DataSource = blPaicma.myDataSet.Tables["strUsuarioRegular"];
                            ListBoxUsuariosSelecciondos.DataValueField = "idUsuarioRef";
                            ListBoxUsuariosSelecciondos.DataTextField = "segusu_login";
                            ListBoxUsuariosSelecciondos.DataBind();
                            Session["cantRegular"] = ListBoxUsuariosSelecciondos.Items.Count;

                            for (int i = ListBoxUsuariosSelecciondos.Items.Count - 1; i >= 0; i--)
                            {
                                for (int j = ListBoxUsuarios.Items.Count - 1; j >= 0; j--)
                                {
                                    if (ListBoxUsuarios.Items[j].Value == ListBoxUsuariosSelecciondos.Items[i].Value)
                                    {
                                        ListItem li = ListBoxUsuarios.Items[j];
                                        ListBoxUsuarios.Items.Remove(li);
                                    }
                                }
                            }
                            // Consulta usuarios Coordinador
                            if (blPaicma.fntUsuariosCoordinacion_bol("strUsuarioCoordinador", idCoordinacion, true))
                            {

                                ListBoxUsuariosCoordinadores.DataSource = blPaicma.myDataSet.Tables["strUsuarioCoordinador"];
                                ListBoxUsuariosCoordinadores.DataValueField = "idUsuarioRef";
                                ListBoxUsuariosCoordinadores.DataTextField = "segusu_login";
                                ListBoxUsuariosCoordinadores.DataBind();
                                Session["cantCoordinador"] = ListBoxUsuariosCoordinadores.Items.Count;

                                for (int i = ListBoxUsuariosCoordinadores.Items.Count - 1; i >= 0; i--)
                                {
                                    for (int j = ListBoxUsuariosSelecciondos.Items.Count - 1; j >= 0; j--)
                                    {
                                        if (ListBoxUsuariosSelecciondos.Items[j].Value == ListBoxUsuariosCoordinadores.Items[i].Value)
                                        {
                                            ListItem li = ListBoxUsuariosSelecciondos.Items[j];
                                            ListBoxUsuariosSelecciondos.Items.Remove(li);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //no pudo consultar coordinadores                    
                            }

                        }
                        else
                        {
                            //no pudo consultar usuarios regulares
                        }
                    }
                    else
                    {

                    }
                }
                else
                {
                    // Sesion esta en crear 
                }
            }
            else
            {
                //no pudo consultar listado de usuarios
            }

            //Verificar que no queden en la lista usuarios ya asignados a otras coordinaciones
            DataTable dtTable;
            if (blPaicma.fntUsuariosCoordinacionTodos_bol("todosLosMiembrosCoodinacion"))
            {
                dtTable = blPaicma.myDataSet.Tables["todosLosMiembrosCoodinacion"];
                if (dtTable.Rows.Count > 0)
                {
                    foreach (DataRow dtRow in dtTable.Rows)
                    {
                        int IdUsuario = Convert.ToInt32(dtRow["idUsuarioRef"]);
                        for (int j = ListBoxUsuarios.Items.Count - 1; j >= 0; j--)
                        {
                            if (Convert.ToInt32(ListBoxUsuarios.Items[j].Value) == IdUsuario)
                            {
                                ListItem li = ListBoxUsuarios.Items[j];
                                ListBoxUsuarios.Items.Remove(li);
                            }
                            else
                            {
                                //vacio
                            }
                        }
                    }
                }
                else
                {
                    //no hay ningun usuario
                }
            }
            else
            {
                //no pudo consultar
            }
        }
        else
        {
            //no pudo conectar con la base de datos
        }
        blPaicma.Termina();
    }

    public void fntCargarDatosCoordinacion()
    {

        if (Session["ModoCoordinacion"].ToString() == "Modificar")
        {
            string idCoordinacion = Session["idCoordinacionRef"].ToString();
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultarCoordinacion_bol("strCoordinacionEspecifica", "id", idCoordinacion))
                {
                    if (blPaicma.myDataSet.Tables["strCoordinacionEspecifica"].Rows.Count > 0)
                    {
                        txtNombreCoordinacion.Text = blPaicma.myDataSet.Tables["strCoordinacionEspecifica"].Rows[0]["nombre"].ToString();
                        txtDescripcionCoordinacion.Text = blPaicma.myDataSet.Tables["strCoordinacionEspecifica"].Rows[0]["descripcion"].ToString();
                    }
                }
                else
                {
                    //no puede consultar
                }
            }
            else
            {
                //no pudo inicializar
            }
            blPaicma.Termina();
        }
    }

    protected void btnMoveLeftUser_Click(object sender, EventArgs e)
    {
        for (int i = ListBoxUsuariosSelecciondos.Items.Count - 1; i >= 0; i--)
        {
            if (ListBoxUsuariosSelecciondos.Items[i].Selected == true)
            {
                ListBoxUsuarios.Items.Add(ListBoxUsuariosSelecciondos.Items[i]);
                ListItem li = ListBoxUsuariosSelecciondos.Items[i];
                ListBoxUsuariosSelecciondos.Items.Remove(li);
            }
        }
    }
    protected void btnMoveRightUser_Click(object sender, EventArgs e)
    {
        for (int i = ListBoxUsuarios.Items.Count - 1; i >= 0; i--)
        {
            if (ListBoxUsuarios.Items[i].Selected == true)
            {
                ListBoxUsuariosSelecciondos.Items.Add(ListBoxUsuarios.Items[i]);
                ListItem li = ListBoxUsuarios.Items[i];
                ListBoxUsuarios.Items.Remove(li);
            }
        }
    }
    protected void btnMoveLeftCoor_Click(object sender, EventArgs e)
    {
        for (int i = ListBoxUsuariosCoordinadores.Items.Count - 1; i >= 0; i--)
        {
            if (ListBoxUsuariosCoordinadores.Items[i].Selected == true)
            {
                ListBoxUsuariosSelecciondos.Items.Add(ListBoxUsuariosCoordinadores.Items[i]);
                ListItem li = ListBoxUsuariosCoordinadores.Items[i];
                ListBoxUsuariosCoordinadores.Items.Remove(li);
            }
        }
    }
    protected void btnMoveRightCoor_Click(object sender, EventArgs e)
    {
        for (int i = ListBoxUsuariosSelecciondos.Items.Count - 1; i >= 0; i--)
        {
            if (ListBoxUsuariosSelecciondos.Items[i].Selected == true)
            {
                ListBoxUsuariosCoordinadores.Items.Add(ListBoxUsuariosSelecciondos.Items[i]);
                ListItem li = ListBoxUsuariosSelecciondos.Items[i];
                ListBoxUsuariosSelecciondos.Items.Remove(li);
            }
        }
    }



    public bool fntInsertarUsuariosCoodinacion_bol(ListBox ListaIdUsuarios, string idCoordinacion, bool esCoordinador)
    {
        bool respuesta = false;
        for (int i = ListaIdUsuarios.Items.Count - 1; i >= 0; i--)
        {
            if (blPaicma.fntInsertarUsuariosCoordinacion_bol(Convert.ToInt32(ListaIdUsuarios.Items[i].Value), idCoordinacion, esCoordinador))
            {
                respuesta = true;
            }
            else
            {
                respuesta = false;
                break;
            }
        }
        return respuesta;
    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {

        bool continuar = false;

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {

            if (Session["ModoCoordinacion"].ToString() != "Modificar")
            {
                //Guid IdNuevaCoordinacion = new NewGuid();
                Session["idCoordinacionRef"] = Guid.NewGuid().ToString();
                //Guardar info de de coordinacion
                if (blPaicma.fntInsertarCoordinacion_bol(txtNombreCoordinacion.Text, txtDescripcionCoordinacion.Text, Convert.ToInt32(Session["IdUsuario"]), Session["idCoordinacionRef"].ToString()))
                {
                    continuar = true;

                }
                else
                {
                    continuar = false;
                }
            }
            else
            {
                if (blPaicma.fntActualizarCoordinacion_bol(txtNombreCoordinacion.Text, txtDescripcionCoordinacion.Text, Convert.ToInt32(Session["IdUsuario"]), Session["idCoordinacionRef"].ToString()))
                {
                    continuar = true;
                }
                else
                {
                    continuar = false;
                }

            }

            if (continuar)
            {
                if (ListBoxUsuariosSelecciondos.Items.Count > 0)
                {
                    if (Convert.ToInt32(Session["cantRegular"]) > 0)
                    {
                        if (blPaicma.fntEliminarUsuariosCoordinacion(Session["idCoordinacionRef"].ToString(), false))
                        {
                            if (fntInsertarUsuariosCoodinacion_bol(ListBoxUsuariosSelecciondos, Session["idCoordinacionRef"].ToString(), false))
                            {
                                //Trata de guardar usuarios coordinador
                                if (ListBoxUsuariosCoordinadores.Items.Count > 0)
                                {
                                    if (Convert.ToInt32(Session["cantCoordinador"]) > 0)
                                    {
                                        if (blPaicma.fntEliminarUsuariosCoordinacion(Session["idCoordinacionRef"].ToString(), true))
                                        {
                                            if (this.fntInsertarUsuariosCoodinacion_bol(ListBoxUsuariosCoordinadores, Session["idCoordinacionRef"].ToString(), true))
                                            {
                                                //Guardo todo correctamente
                                                lblMensaje.Visible = true;
                                                lblMensaje.Text = "Coordinacion registrada exitosamente";
                                                HtmlMeta meta = new HtmlMeta();
                                                meta.HttpEquiv = "Refresh";
                                                meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";
                                                this.Page.Controls.Add(meta);
                                                lblMensajeAdicional.Visible = true;
                                                lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";
                                            }
                                            else
                                            {
                                                //no pudo guardar usuarios coordinador
                                                lblMensaje.Visible = true;
                                                lblMensaje.Text = "No se registraron los usuarios";
                                                HtmlMeta meta = new HtmlMeta();
                                                meta.HttpEquiv = "Refresh";
                                                meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";
                                                this.Page.Controls.Add(meta);
                                                lblMensajeAdicional.Visible = true;
                                                lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";

                                            }
                                        }
                                        else
                                        {
                                            //no pudo eliminar usuarios coordinador
                                            lblMensaje.Visible = true;
                                            lblMensaje.Text = "No se actualizo lista de usuarios correctamente";
                                            HtmlMeta meta = new HtmlMeta();
                                            meta.HttpEquiv = "Refresh";
                                            meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";
                                            this.Page.Controls.Add(meta);
                                            lblMensajeAdicional.Visible = true;
                                            lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";
                                        }
                                    }
                                    else
                                    {
                                        //solo inserta
                                        if (this.fntInsertarUsuariosCoodinacion_bol(ListBoxUsuariosCoordinadores, Session["idCoordinacionRef"].ToString(), true))
                                        {
                                            //Guardo todo correctamente
                                            lblMensaje.Visible = true;
                                            lblMensaje.Text = "Coordinacion registrada exitosamente";
                                            HtmlMeta meta = new HtmlMeta();
                                            meta.HttpEquiv = "Refresh";
                                            meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";
                                            this.Page.Controls.Add(meta);
                                            lblMensajeAdicional.Visible = true;
                                            lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";
                                        }
                                        else
                                        {
                                            //No puede Solo insertar
                                            lblMensaje.Visible = true;
                                            lblMensaje.Text = "No se registraron los usuarios";
                                            HtmlMeta meta = new HtmlMeta();
                                            meta.HttpEquiv = "Refresh";
                                            meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";
                                            this.Page.Controls.Add(meta);
                                            lblMensajeAdicional.Visible = true;
                                            lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";


                                        }
                                    }
                                }
                                else
                                {
                                    //no hay usuarios coordinadores en la lista
                                    if (blPaicma.fntEliminarUsuariosCoordinacion(Session["idCoordinacionRef"].ToString(), true))
                                    {
                                        lblMensaje.Visible = true;
                                        lblMensaje.Text = "Coordinacion registrada exitosamente";
                                        HtmlMeta meta = new HtmlMeta();
                                        meta.HttpEquiv = "Refresh";
                                        meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";
                                        this.Page.Controls.Add(meta);
                                        lblMensajeAdicional.Visible = true;
                                        lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";
                                    }


                                }
                            }
                            else
                            {
                                lblMensaje.Visible = true;
                                lblMensaje.Text = "No se registraron los usuarios";
                                HtmlMeta meta = new HtmlMeta();
                                meta.HttpEquiv = "Refresh";
                                meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";
                                this.Page.Controls.Add(meta);
                                lblMensajeAdicional.Visible = true;
                                lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";
                            }
                        }
                        else
                        {
                            lblMensaje.Visible = true;
                            lblMensaje.Text = "No se actualizo lista de usuarios correctamente";
                            HtmlMeta meta = new HtmlMeta();
                            meta.HttpEquiv = "Refresh";
                            meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";
                            this.Page.Controls.Add(meta);
                            lblMensajeAdicional.Visible = true;
                            lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";
                        }
                    }
                    else
                    {
                        if (this.fntInsertarUsuariosCoodinacion_bol(ListBoxUsuariosSelecciondos, Session["idCoordinacionRef"].ToString(), false))
                        {

                            //Trata de guardar usuarios coordinador
                            if (ListBoxUsuariosCoordinadores.Items.Count > 0)
                            {
                                if (Convert.ToInt32(Session["cantCoordinador"]) > 0)
                                {
                                    if (blPaicma.fntEliminarUsuariosCoordinacion(Session["idCoordinacionRef"].ToString(), true))
                                    {
                                        if (this.fntInsertarUsuariosCoodinacion_bol(ListBoxUsuariosCoordinadores, Session["idCoordinacionRef"].ToString(), true))
                                        {
                                            //Guardo todo correctamente
                                            lblMensaje.Visible = true;
                                            lblMensaje.Text = "Coordinacion registrada exitosamente";
                                            HtmlMeta meta = new HtmlMeta();
                                            meta.HttpEquiv = "Refresh";
                                            meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";

                                            this.Page.Controls.Add(meta);
                                            lblMensajeAdicional.Visible = true;
                                            lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";
                                        }
                                        else
                                        {
                                            //no pudo guardar usuarios coordinador
                                            lblMensaje.Visible = true;
                                            lblMensaje.Text = "No se registraron los usuarios";
                                            HtmlMeta meta = new HtmlMeta();
                                            meta.HttpEquiv = "Refresh";
                                            meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";

                                            this.Page.Controls.Add(meta);
                                            lblMensajeAdicional.Visible = true;
                                            lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";

                                        }
                                    }
                                    else
                                    {
                                        //no pudo eliminar usuarios coordinador
                                        lblMensaje.Visible = true;
                                        lblMensaje.Text = "No se actualizo lista de usuarios correctamente";
                                        HtmlMeta meta = new HtmlMeta();
                                        meta.HttpEquiv = "Refresh";
                                        meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";

                                        this.Page.Controls.Add(meta);
                                        lblMensajeAdicional.Visible = true;
                                        lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";
                                    }
                                }
                                else
                                {
                                    //solo inserta
                                    if (this.fntInsertarUsuariosCoodinacion_bol(ListBoxUsuariosCoordinadores, Session["idCoordinacionRef"].ToString(), true))
                                    {
                                        //Guardo todo correctamente
                                        lblMensaje.Visible = true;
                                        lblMensaje.Text = "Coordinacion registrada exitosamente";
                                        HtmlMeta meta = new HtmlMeta();
                                        meta.HttpEquiv = "Refresh";
                                        meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";

                                        this.Page.Controls.Add(meta);
                                        lblMensajeAdicional.Visible = true;
                                        lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";
                                    }
                                    else
                                    {
                                        //No puede Solo insertar
                                        lblMensaje.Visible = true;
                                        lblMensaje.Text = "No se registraron los usuarios";
                                        HtmlMeta meta = new HtmlMeta();
                                        meta.HttpEquiv = "Refresh";
                                        meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";

                                        this.Page.Controls.Add(meta);
                                        lblMensajeAdicional.Visible = true;
                                        lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";


                                    }
                                }
                            }
                            else
                            {
                                //no hay usuarios coordinadores en la lista
                                if (blPaicma.fntEliminarUsuariosCoordinacion(Session["idCoordinacionRef"].ToString(), true))
                                {
                                    lblMensaje.Visible = true;
                                    lblMensaje.Text = "Coordinacion registrada exitosamente";
                                    HtmlMeta meta = new HtmlMeta();
                                    meta.HttpEquiv = "Refresh";
                                    meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";

                                    this.Page.Controls.Add(meta);
                                    lblMensajeAdicional.Visible = true;
                                    lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";
                                }
                            }
                        }
                        else
                        {
                            lblMensaje.Visible = true;
                            lblMensaje.Text = "No se registraron los usuarios";
                            HtmlMeta meta = new HtmlMeta();
                            meta.HttpEquiv = "Refresh";
                            meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";

                            this.Page.Controls.Add(meta);
                            lblMensajeAdicional.Visible = true;
                            lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";
                        }
                    }
                }
                else
                {
                    if (blPaicma.fntEliminarPermisosReporte_bol(Convert.ToInt32(Session["idReporteBuscar"])))
                    {
                        lblMensaje.Visible = true;
                        lblMensaje.Text = "Coordinacion registrada exitosamente";
                        HtmlMeta meta = new HtmlMeta();
                        meta.HttpEquiv = "Refresh";
                        meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";

                        this.Page.Controls.Add(meta);
                        lblMensajeAdicional.Visible = true;
                        lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";
                    }

                }
            }
            else
            {
                lblMensaje.Visible = true;
                lblMensaje.Text = "No se actualizo lista de usuarios correctamente";
                HtmlMeta meta = new HtmlMeta();
                meta.HttpEquiv = "Refresh";
                meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";

                this.Page.Controls.Add(meta);
                lblMensajeAdicional.Visible = true;
                lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";
                //No pudo continuar
            }
        }
        else
        {
            lblMensaje.Visible = true;
            lblMensaje.Text = "No se actualizo lista de usuarios correctamente";
            HtmlMeta meta = new HtmlMeta();
            meta.HttpEquiv = "Refresh";
            meta.Content = "5;url=frmWFListadoCoordinaciones.aspx";

            this.Page.Controls.Add(meta);
            lblMensajeAdicional.Visible = true;
            lblMensajeAdicional.Text = "Sera redirigido en 5 segundos";

        }
        blPaicma.Termina();
    }
}