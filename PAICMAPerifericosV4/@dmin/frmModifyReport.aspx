﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmModifyReport.aspx.cs" Inherits="_dmin_frmUploadReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        #txtObservacion {
            width: 347px;
        }
        #archivoReporte {
            width: 353px;
            margin-left: 0px;
        }
        #btnCargar {
            width: 129px;
            margin-left: 0px;
            margin-right: 0px;
        }
        .auto-style1 {
            width: 177px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">
         <div class="Botones">
       <input type="image" name="contenido_atras" id="contenido_atras" title="Atras" src="../Images/volver.png" onclick="window.history.back();" />
    </div>
   
    <div class="contacto">

        <table>
            <thead>

            </thead>
            <tbody>
                 <tr>
                    <td colspan="1" class="auto-style1" >
                      <h1 style="float:left; align-content:center">Carga de Reportes</h1> 
                    </td>
                     <td colspan="1" >
                 <%--   <asp:Button ID="btnVerReporte" runat="server" Text="Visualizar" Width="190px" OnClick="btnVerReporte_Click" />--%>
                            </td>
                    
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label1" runat="server" Text="Label">Nombre del Reporte: &nbsp;</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtNombreFormulario" runat="server" Width="346px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label3" runat="server" Text="Label">Descripción Reporte:  &nbsp;</asp:Label>
                    </td>
                    <td>
                        <textarea id="txtObservacion" rows="4" cols="50" name="txtObservacion" runat="server"></textarea>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label4" runat="server" Text="Label">Archivo a Cargar: &nbsp;</asp:Label>
                   </td>
                    <td>
                        <input type="file" id="archivoReporte" accept=".repx" name="archivoReporte" runat="server" />
                    </td>
                </tr>
                 <tr>

                   <td colspan="1" class="auto-style1">
                        &nbsp;
                   </td>
                    <td colspan="1">
                        &nbsp;
                   </td>
                   
                </tr>
                 <tr>

                   <td colspan="1" class="auto-style1">
                        <asp:CheckBox ID="ckhActivo" runat="server" Text="¿Reporte Activo?" />
                   </td>
                    <td colspan="1">
                    <%--    <asp:Button ID="btnpermisos" runat="server" Text="Asignar Permisos" OnClick="btnpermisos_Click" Width="136px" />--%>
                   </td>
                   
                </tr>
                
            </tbody>
            <tfoot>

            </tfoot>
        </table>
        <br />

     

        <table>
            <tbody>
                <tr> 
                    <td colspan="2">
                        <input type="submit" id="btnCargar" value="Actualizar" runat="server" /><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" style="margin-left: 38px; margin-right: 33px" Width="94px" />
                    </td>
                </tr>
                 <tr> 
                    <td colspan="2">
                       <asp:Label ID="lblError" runat="server" Text="" Visible ="false"></asp:Label>
                        <asp:Label ID="lblRedirigir" runat="server" Text=""  Visible ="false"></asp:Label>
                    </td>
                </tr>
                </tbody>
            </table>


    </div>
    
<%--
<br/>
--%>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">
</asp:Content>

