﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmWFEditExistingSuportLegalizacion : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                Int64 idSitio = Int64.TryParse(Session["intIdSoporteLegalizacion"].ToString(), out idSitio) ? idSitio : 0;
                Int64 idSolicitud = Int64.TryParse(Session["IdSolicitudRef"].ToString(), out idSolicitud) ? idSolicitud : 0;
                if (Session["IdSolicitudRef"].ToString() != "0")
                {
                    if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                    {
                        if (blPaicma.fntConsultarSoporteLegalizacion("datosSoporteComision", idSolicitud, "id", idSitio))
                        {
                            if (blPaicma.myDataSet.Tables["datosSoporteComision"].Rows.Count > 0)
                            {
                                txtNombre.Text = blPaicma.myDataSet.Tables["datosSoporteComision"].Rows[0]["nombreArchivo"].ToString();
                                txtDescripcion.Text = blPaicma.myDataSet.Tables["datosSoporteComision"].Rows[0]["descripcionArchivo"].ToString();

                            }
                        }
                        else
                        {
                            //Imposible Consultar lista de planes
                        }
                    }
                    else
                    {
                        //No pudo conectar a la base de datos
                    }
                    blPaicma.Termina();
                }

            }
        }
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {

    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        this.subirArchivos();

    }
    public void subirArchivos()
    {
        Int64 idSolicitud = Convert.ToInt64(Session["IdSolicitudRef"]);
        if ((archivoReporte.PostedFile != null) && (archivoReporte.PostedFile.ContentLength > 0) && (txtNombre.Text.Trim() != string.Empty) && (txtDescripcion.Text.Trim() != string.Empty))
        {
            //string fn = System.IO.Path.GetFileName(archivoReporte.PostedFile.FileName);
            string strExtDocumento = Path.GetExtension(archivoReporte.PostedFile.FileName);
            string unique = Guid.NewGuid().ToString();

            int idUsuarioLogin = Convert.ToInt32(Session["IdUsuario"]);
            DateTime creacion = DateTime.Now;
            //fn = fn + DateTime.Now.ToString("YYYYMMDDHHMMSS");
            string SaveLocation = Server.MapPath("~") + "Archivos\\" + unique + strExtDocumento;
            try
            {
                archivoReporte.PostedFile.SaveAs(SaveLocation);
                byte[] reporteGuardarBD = GetBinaryFile(SaveLocation);

                if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
                {
                    if (this.blPaicma.fntActualizarSoporteSolLegalizacion_bol(Session["intIdSoporteLegalizacion"].ToString(), idSolicitud, SaveLocation, reporteGuardarBD, txtNombre.Text, txtDescripcion.Text, creacion, creacion, idUsuarioLogin, idUsuarioLogin, strExtDocumento))
                    {

                        Response.Redirect("SoporteLegalizacion.aspx");
                    }
                    else
                    {
                        //lblRespuesta.Text = "Se presento un problema al conectar con la base de datos, Intente Nuevamente.";
                        //principal.Visible = false;
                        //pnlRespuesta.Visible = true;
                        Session["IdSolicitudRef"] = idSolicitud;
                    }
                }
                else
                {
                    //lblRespuesta.Text = "Se presento un problema al guardar el soporte, no pudo conectar con la base de datos.";
                    //principal.Visible = false;
                    //pnlRespuesta.Visible = true;
                    Session["OperacionSolicitud"] = "ErrorSalir";
                }
                this.blPaicma.Termina();

            }
            catch (Exception ex)
            {
                //lblRespuesta.Text = "Se presento un problema al guardar el soporte," + "Error: " + ex.Message;
                //principal.Visible = false;
                //pnlRespuesta.Visible = true;
                Session["OperacionSolicitud"] = "ErrorSalir";
            }
        }

        else if ((txtNombre.Text.Trim() != string.Empty) && (txtDescripcion.Text.Trim() != string.Empty))
        {

            string unique = Guid.NewGuid().ToString();
            int idUsuarioLogin = Convert.ToInt32(Session["IdUsuario"]);
            DateTime creacion = DateTime.Now;
            try
            {

                if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
                {
                    if (this.blPaicma.fntActualizarSoporteSolSinArchivoLegalizacion_bol(Session["intIdSoporteLegalizacion"].ToString(), idSolicitud, txtNombre.Text, txtDescripcion.Text, creacion, creacion, idUsuarioLogin, idUsuarioLogin))
                    {

                        Response.Redirect("SoporteLegalizacion.aspx");
                    }
                    else
                    {
                        //Session["IdSolicitudRef"] = idSolicitud;
                    }
                }
                else
                {
                    //Session["OperacionSolicitud"] = "ErrorSalir";
                }
                this.blPaicma.Termina();

            }
            catch (Exception ex)
            {
                Session["OperacionSolicitud"] = "ErrorSalir";
            }
        }
        else
        {
            //lblError.Text = "Faltan Datos por completar o seleccionar el archivo a cargar";
            //lblError.Style.Add("background-color", "#FBEE57");
            //lblError.Visible = true;
            Session["IdSolicitudRef"] = idSolicitud;
        }


    }


    private byte[] GetBinaryFile(string filename)
    {
        byte[] bytes;
        using (FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Read))
        {
            bytes = new byte[file.Length];
            file.Read(bytes, 0, (int)file.Length);
        }
        return bytes;
    }

}