﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DevExpress.XtraReports.UI;
using System.IO;

public partial class _dmin_frmUploadReport : System.Web.UI.Page
{
    //protected System.Web.UI.HtmlControls.HtmlInputFile File1;
    //protected System.Web.UI.HtmlControls.HtmlInputButton Submit1;
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!this.Page.IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }

            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                this.cargarDetalleReporte();
                return;
            }
            this.blPaicma.Termina();
            base.Response.Redirect("frmLogin.aspx");

        }
    }

    override protected void OnInit(EventArgs e)
    {
        // 
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        // 
        InitializeComponent();
        base.OnInit(e);
    }

    private void InitializeComponent()
    {
        this.btnCargar.ServerClick += new System.EventHandler(this.btnCargar_ServerClick);
        this.Load += new System.EventHandler(this.Page_Load);

    }


    private void cargarDetalleReporte()
    {
        int idReporteBuscar = Convert.ToInt32(Session["idReporteBuscar"]);
        string nombreReporte = Session["NombreReporteSeleccionado"].ToString();
        if (this.blPaicma.fntConsultaReporte("strReportesListados", 1, idReporteBuscar, 0))
        {
            txtNombreFormulario.Text = this.blPaicma.myDataSet.Tables[0].Rows[0]["nombreReporte"].ToString();
            txtObservacion.InnerText = this.blPaicma.myDataSet.Tables[0].Rows[0]["descripcionReporte"].ToString();
            ckhActivo.Checked = Convert.ToBoolean(this.blPaicma.myDataSet.Tables[0].Rows[0]["estatus"]);
            Session["ModificarUsuarioCreacion"] = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0]["usuarioCreacion"]);
            Session["ModificarReporteBinario"] = (byte[])this.blPaicma.myDataSet.Tables[0].Rows[0]["reporteBinario"];
            Session["ModificarRutaReporte"] = this.blPaicma.myDataSet.Tables[0].Rows[0]["rutaReporte"].ToString();
        }
        else
        {
            base.Response.Redirect("frmReportList.aspx");
        }

    }


    #region Funciones de Reportes
    private string StoreReportToFile(XtraReport report, string ruta)
    {
        string path = ruta;
        report.SaveLayout(path);
        return path;
    }

    private MemoryStream StoreReportToStream(XtraReport report)
    {
        MemoryStream stream = new MemoryStream();
        report.SaveLayout(stream);
        return stream;
    }

    // Create a report from a file. 
    private XtraReport CreateReportFromFile(string filePath)
    {
        XtraReport report = XtraReport.FromFile(filePath, true);
        return report;
    }

    // Create a report from a stream. 
    private XtraReport CreateReportFromStream(MemoryStream stream)
    {
        XtraReport report = XtraReport.FromStream(stream, true);
        return report;
    }
    #endregion

    private void btnCargar_ServerClick(object sender, System.EventArgs e)
    {
        if ((archivoReporte.PostedFile != null) && (archivoReporte.PostedFile.ContentLength > 0))
        {
            if ((txtNombreFormulario.Text.Trim() != string.Empty) && (txtObservacion.InnerText.Trim() != string.Empty))
            {
                string strExtDocumento = Path.GetExtension(archivoReporte.PostedFile.FileName);
                string unique = Guid.NewGuid().ToString();
                //fn = fn + DateTime.Now.ToString("YYYYMMDDHHMMSS");
                string SaveLocation = Server.MapPath("~") + "@reportes\\" + unique + strExtDocumento;
                try
                {
                    archivoReporte.PostedFile.SaveAs(SaveLocation);
                    // var cadena = StoreReportToFile(reporte, SaveLocation);
                    MemoryStream aguardar = new MemoryStream();
                    aguardar = StoreReportToStream(CreateReportFromFile(SaveLocation));
                    byte[] reporteGuardarBD = aguardar.ToArray();
                    if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
                    {
                        if (this.blPaicma.fntModificarFormReportes_bol(txtNombreFormulario.Text.Trim(), txtObservacion.InnerText.Trim(), ckhActivo.Checked, 1, reporteGuardarBD, SaveLocation, Convert.ToInt32(Session["ModificarUsuarioCreacion"]), Convert.ToInt32(Session["IdUsuario"]), Convert.ToInt32(Session["idReporteBuscar"])))
                        {
                            lblError.Text = "Reporte Modificado Exitosamente, Archivo Cargado Correctamente";
                        }
                        else
                        {
                            //no guardo en base de datos
                            lblError.Text = "No fue cargado el archivo, intente mas tarde";
                        }
                    }
                    else
                    {
                        //No se pudo inicializar
                        lblError.Text = "No fue posible conectar con la base de datos";
                    }

                }
                catch (Exception ex)
                {
                    lblError.Text = "Ocurrio un error al cargar el archivo, intente de nuevo - " + "Error: " + ex.Message;
                }
                this.blPaicma.Termina();
                lblError.Visible = true;
                HtmlMeta meta = new HtmlMeta();
                meta.HttpEquiv = "Refresh";
                meta.Content = "3;url=frmReportList.aspx";
                this.Page.Controls.Add(meta);
                lblRedirigir.Text = "Sera redirigido en 3 segundos";
                lblRedirigir.Visible = true;

            }
            else
            {
                lblError.Text = "Faltan Datos por completar";
                lblError.Style.Add("background-color", "#FBEE57");
                lblError.Visible = true;
            }
        }
        else
        {
            if ((txtNombreFormulario.Text.Trim() != string.Empty) && (txtObservacion.InnerText.Trim() != string.Empty))
            {
                try
                {
                    if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
                    {
                        if (this.blPaicma.fntModificarFormReportes_bol(txtNombreFormulario.Text.ToString().Trim(), txtObservacion.InnerText.ToString().Trim(), ckhActivo.Checked, 1, (byte[])Session["ModificarReporteBinario"], Session["ModificarRutaReporte"].ToString(), Convert.ToInt32(Session["ModificarUsuarioCreacion"]), Convert.ToInt32(Session["IdUsuario"]), Convert.ToInt32(Session["idReporteBuscar"])))
                        {
                            lblError.Text = "Reporte Modificado Exitosamente";

                        }
                        else
                        {
                            //no guardo en base de datos
                            lblError.Text = "No se ha modificado el reporte, intente mas tarde";
                        }
                    }
                    else
                    {
                        //No se pudo inicializar
                        lblError.Text = "No fue posible conectar con la base de datos";
                    }

                }
                catch (Exception ex)
                {
                    lblError.Text = "Ocurrio un error al cargar el archivo, intente de nuevo - " + "Error: " + ex.Message;
                    //Note: Exception.Message returns a detailed message that describes the current exception. 
                    //For security reasons, we do not recommend that you return Exception.Message to end users in 
                    //production environments. It would be better to return a generic error message. 
                }
                this.blPaicma.Termina();
                lblError.Style.Add("background-color", "#FBEE57");
                lblError.Visible = true;
                HtmlMeta meta = new HtmlMeta();
                meta.HttpEquiv = "Refresh";
                meta.Content = "3;url=frmReportList.aspx";
                this.Page.Controls.Add(meta);
                lblRedirigir.Text = "Sera redirigido en 3 segundos";
                lblRedirigir.Visible = true;

            }
            else
            {
                lblError.Text = "Faltan Datos por completar o seleccionar el archivo a cargar";
                lblError.Style.Add("background-color", "#FBEE57");
                lblError.Visible = true;
            }
        }

    }
    protected void btnVerReporte_Click(object sender, EventArgs e)
    {
        string strRuta = "/@web/GenericReport.aspx";
        Response.Redirect(strRuta);
    }
    protected void btnpermisos_Click(object sender, EventArgs e)
    {
        string strRuta = "frmPermisosUsuarioReportes.aspx";
        Response.Redirect(strRuta);
    }
}