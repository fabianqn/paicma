﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmWFValidarPlanes.aspx.cs" Inherits="_dmin_frmWFValidarPlanes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 121px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">
     <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblMensajeGuardar" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Aceptar" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>


     <div class="contacto2" id="principal" runat="server">
        <table style="width: 100%">
                <tbody>
                <tr>
                    <td> <h1 style="font-size:150%">Estrategias a Validar</h1> </td>
                    <td></td>
                    <td><asp:TextBox ID="txtCumplimiento" runat="server" Enabled="false" Visible="false"></asp:TextBox></td>
                </tr>
                </tbody>  
        </table>
        <hr />
        <asp:GridView ID="gvPlanAccionElegir" CssClass="mGrid" runat="server" AutoGenerateColumns="False" OnRowCommand="gvPlanAccionElegir_RowCommand" OnPreRender="gvPlanAccionElegir_PreRender">
            <Columns>
                <asp:BoundField DataField="idPlanAccion" HeaderText="ID Plan" />
                <asp:BoundField DataField="Estrategia" HeaderText="Estrategia" />
                <asp:BoundField DataField="Actividad" HeaderText="Actividad" />
                <asp:BoundField DataField="indicador" HeaderText="Indicador" />
                <asp:BoundField DataField="valorIndicador" HeaderText="Valor Indicador" FooterStyle-VerticalAlign="Middle" />
                <asp:BoundField DataField="ejecucionPorcentaje"  DataFormatString="{0:N2}%"  HeaderText  ="Porcentaje" FooterStyle-VerticalAlign="Middle" />

                 <asp:TemplateField>
                     <ItemTemplate>
                          <asp:Button runat="server" ButtonType="Button" CommandName="Validar" CommandArgument='<%# Container.DataItemIndex %>' ImageUrl="~/Images/seleccionar.png" Text="Validar"/>
                     </ItemTemplate>
                </asp:TemplateField>
            </Columns>
         </asp:GridView>
        <asp:Label ID="lblRespuesta" runat="server" Text=""></asp:Label>
        <hr />
         <div id="validacionPlan" runat="server" visible="false">
        <h1 style="font-size:100%">Datos de Validacion Obligatorios   <asp:Label ID="lblStrategia" runat="server" Text="Label"></asp:Label> </h1>
           
        <hr />
             
        <table style="width:100%">
            <tbody>
                <tr>
                   <td><asp:Label ID="Label1" runat="server" Text="Actividad Desarrollada"></asp:Label></td>
                   <td><asp:TextBox ID="txtActividadValidar" runat="server" TextMode="MultiLine" Enabled="true" Height="51px" Width="350px"></asp:TextBox></td>  
                   <td class="auto-style1"><asp:Label ID="Label2" runat="server" Text="Valor Indicador"></asp:Label></td>
                   <td><asp:TextBox ID="txtValorIndicador" runat="server" Enabled="true" ></asp:TextBox></td>  
                   <td><asp:Button ID="btnGuardarCont" runat="server" Text="Guardar" OnClick="btnGuardarCont_Click" /></td>
                </tr>
               
            </tbody>
        </table>
        </div>
        <asp:Panel ID="panelBotones" runat="server">
        <div class="Botones">
            <asp:Button ID="btnGuardarSalir" runat="server" Visible="false" Text="Salir" OnClick="btnGuardarSalir_Click"/>
            <asp:Button ID="btnContinuar" runat="server" Visible="false" Text="Continuar" OnClick="btnContinuar_Click"/>
            <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
        </div>
    </asp:Panel>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">

    <script type="text/javascript">
        function newDoc() {
            window.location.assign("frmWFDetalleSolicitud.aspx")
        }
        $(document).ready(function () {
            $('#' + '<%= gvPlanAccionElegir.ClientID %>').DataTable();
        });
    </script>
</asp:Content>

