﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmWFSolicitudComision.aspx.cs" Inherits="_dmin_frmWFSolicitudComision" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <div class="contacto2">
        <h3>Solicitud de Comisión</h3>
        <br />
       
        <asp:Button ID="btnNuevaSolicitud" runat="server" Text="Nueva Solicitud" OnClick="btnNuevaSolicitud_Click"  />
        <hr />
        <asp:GridView ID="gvSolicitud" runat="server" CssClass="mGrid" AutoGenerateColumns="False" OnRowCommand="gvSolicitud_RowCommand" OnPreRender="gvSolicitud_PreRender">
            <Columns>
                <asp:BoundField DataField="idSolicitud" HeaderText="ID Solicitud" />
                <asp:BoundField DataField="IdentificadorSolicitud" HeaderText="N°Radicado" />
                <asp:BoundField DataField="nombreFuncionario" HeaderText="Solicitante" />
                <asp:BoundField DataField="fechaCreacion" HeaderText="Fecha Solicitud" />
                <asp:BoundField DataField="idUsuarioCreacion" HeaderText="Id Usuario" />
                <asp:BoundField DataField="nombreestado" HeaderText="Estado" />
                <asp:BoundField DataField="idEstadoActual" HeaderText="idEstado" />
                <asp:BoundField DataField="permiteEditar" HeaderText="permite editar"  Visible="false" />
                <asp:TemplateField>
                     <ItemTemplate>
                          <asp:HiddenField runat="server" ID="HiddenFieldID" Value='<%# Eval("permiteEditar") %>'   > </asp:HiddenField>       
                          <asp:Button runat="server" ButtonType="Image" CommandName="Editar" CommandArgument='<%# Container.DataItemIndex %>' ImageUrl="~/Images/seleccionar.png" Text="Editar"
                                            Visible='<%# Editable(Convert.ToInt32(Eval("idUsuarioCreacion")) ,Convert.ToInt32(Eval("permiteEditar")), Convert.ToInt32(Eval("idEstadoActual")) ) %>' />
                     </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField>
                     <ItemTemplate>
                          <asp:Button runat="server" ButtonType="Image" CommandName="Ver" CommandArgument='<%# Container.DataItemIndex %>' ImageUrl="~/Images/seleccionar.png" Text="Ver"
                                            Visible='<%# sePuedeVer(Eval("idSolicitud").ToString()) %>' />
                     </ItemTemplate>
                </asp:TemplateField>

            <%--    <asp:ButtonField ButtonType="Button" Text="Solicitud" CommandName="EditarSolicitud" />
                <asp:ButtonField ButtonType="Button" Text="Planificacion" CommandName="EditarPlanificacion" />
                <asp:ButtonField ButtonType="Button" Text="Sitios" CommandName="EditarSitios" />
                <asp:ButtonField ButtonType="Button" Text="Soportes" CommandName="EditarSoportes" />--%>
            </Columns>
        </asp:GridView>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#' + '<%= gvSolicitud.ClientID %>').DataTable();
        });
   </script>
</asp:Content>

