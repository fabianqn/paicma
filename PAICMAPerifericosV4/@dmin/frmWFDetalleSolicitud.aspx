﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmWFDetalleSolicitud.aspx.cs" Inherits="_dmin_frmWFDetalleSolicitud" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">
    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Aceptar" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>
    <div class="contacto2" id="principal" runat="server">
        <table style="width: 100%">
                <tbody>
                <tr>
                    <td> <h1 style="font-size:150%">Detalle de Solicitud</h1> </td>
                    <td></td>
                    <td><asp:Label ID="Label14" runat="server" Visible="false" Text="Cumplimiento"></asp:Label></td>
                    <td><asp:TextBox ID="txtCumplimiento" runat="server" Visible="false" Enabled="false"></asp:TextBox></td>
                </tr>
                </tbody>  
        </table>
       
        <hr />
    <table style="width: 100%">
                <tbody>
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="Número Aprobación"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtNumeroAprobacion" runat="server" Enabled="false"></asp:TextBox>

                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="No. Radicación"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtRadicacion" runat="server" TextMode="MultiLine" Rows="2" Columns="30" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text="Línea Intervención" Width="100%"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlLineaIntervencion" runat="server" Enabled="false"></asp:DropDownList>
                            <%-- <asp:RequiredFieldValidator id="RequiredFieldValidator6" runat="server"
                              ControlToValidate="ddlLineaIntervencion"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red" Width="100%">
                            </asp:RequiredFieldValidator>--%>
                           <%-- <asp:TextBox ID="txtIntervencion" runat="server"></asp:TextBox>--%>

                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Nombre Funcionario"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtNombreFuncionario" runat="server" Width="100%" Enabled="false"></asp:TextBox>
                           <%-- <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server"
                              ControlToValidate="txtNombreFuncionario"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red" Width="100%">
                            </asp:RequiredFieldValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1">
                            <asp:Label ID="Label5" runat="server" Text="Objeto"></asp:Label>
                        </td>
                        <td colspan="4">
                            <asp:TextBox ID="txtObjeto" runat="server" TextMode="MultiLine" Width="100%" Enabled="false"></asp:TextBox>
                           <%--  <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server"
                              ControlToValidate="txtObjeto"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red" Width="100%">
                            </asp:RequiredFieldValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label6" runat="server" Text="Fecha Inicio"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaInicio" runat="server" Width="70%" Enabled="false"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server"
                              ControlToValidate="txtFechaInicio"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red" Width="100%">
                            </asp:RequiredFieldValidator>--%>
                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="Label9" runat="server" Text="Fecha Fin"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaFin" runat="server" Width="100%" Enabled="false"></asp:TextBox>
                           <%-- <asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" Width="100%"
                              ControlToValidate="txtFechaFin"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red">
                            </asp:RequiredFieldValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label8" runat="server" Text="Tickets Aéreos"></asp:Label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkTiqueteAereo" runat="server" Enabled="false" />
                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="Label10" runat="server" Text="Ruta Aérea"  ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtRutaAerea" runat="server" TextMode="MultiLine" Width="100%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label11" runat="server" Text="Porción Terrestre"></asp:Label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkPorcionTerrestre" runat="server" Enabled="false" />
                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="lblRutaTerrestre" runat="server" Text="Ruta Terrestre" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtRutaTerrestre" runat="server" TextMode="MultiLine" Width="100%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                     <tr>
                        <td>
                           
                        </td>
                        <td>
                           
                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="lblValorRutaT" runat="server" Text="Valor Ruta Terreste" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtValorRutaT" runat="server" Enabled="false" ></asp:TextBox>
                            <asp:DropDownList ID="ddlMonedaValorRutaT" Enabled="false" runat="server">
                                <asp:ListItem Value="COP">COP</asp:ListItem>
                                <asp:ListItem Value="USD">USD</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label13" runat="server" Text="Viáticos"></asp:Label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkViaticos" runat="server" Enabled="false" />
                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="lblValorDiario" runat="server" Text="Valor Diario" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtValorDiario" runat="server" Enabled="false" ></asp:TextBox>
                            <asp:DropDownList ID="ddlMonedaValor" runat="server" Enabled="false" >
                                <asp:ListItem Value="COP">COP</asp:ListItem>
                                <asp:ListItem Value="USD">USD</asp:ListItem>
                            </asp:DropDownList>
                           <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                ControlToValidate="txtValorDiario" 
                                ErrorMessage="Solo numeros decimales separados por punto (.) o enteros" 
                                ForeColor="Red"
                                ValidationExpression="^(?:|\d+(\.\d{1,2})?)$" Width="100%">
                            </asp:RegularExpressionValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1">
                            <asp:Label ID="Label7" runat="server" Text="Número de Días"></asp:Label>
                        </td>
                        <td colspan="1">
                            <asp:TextBox ID="txtNumeroDias" runat="server" Enabled="false" ></asp:TextBox>
                              <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                ControlToValidate="txtNumeroDias" 
                                ErrorMessage="Solo Numeros decimales separados por punto (.) o enteros" 
                                    ForeColor="Red"
                                ValidationExpression="^(?:|\d+(\.\d{1,2})?)$" Width="100%">
                            </asp:RegularExpressionValidator>--%>
                        </td>
                        <td></td>
                        <td colspan="1">
                            <asp:Label ID="Label15" runat="server" Text="Total viáticos"></asp:Label>
                        </td>
                        <td colspan="1">
                            <asp:TextBox ID="txtTotalViaticos" runat="server" Enabled="false"></asp:TextBox>
                            <asp:DropDownList ID="ddlValorTotal" Enabled="false" runat="server">
                                <asp:ListItem Value="COP">COP</asp:ListItem>
                                <asp:ListItem Value="USD">USD</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                </tbody>
            </table>
        <hr />
        <h1>Planes de Acción</h1>
        <hr />
        <asp:GridView ID="gvPlanAccionDetalle" CssClass="mGrid" runat="server" AutoGenerateColumns="False" OnPreRender="gvPlanAccionDetalle_PreRender">
            <Columns>
                <asp:BoundField DataField="idPlanAccion" HeaderText="ID Plan" />
                <asp:BoundField DataField="Estrategia" HeaderText="Estrategia" />
                <asp:BoundField DataField="Actividad" HeaderText="Actividad" />
                <asp:BoundField DataField="indicador" HeaderText="Indicador" />
                  <%--<asp:TemplateField>
                     <ItemTemplate>
                          <asp:Button runat="server" ButtonType="Button" CommandName="Validar" CommandArgument='<%# Container.DataItemIndex %>' ImageUrl="~/Images/seleccionar.png" Text="Validar"
                                            Visible='<%# Validable() %>' />
                     </ItemTemplate>
                </asp:TemplateField>--%>
            </Columns>
         </asp:GridView>
        <hr />
        <h1>Sitios de intervención</h1>
        <hr />
         <asp:GridView ID="gvSitiosIntervencion" CssClass="mGrid" runat="server" AutoGenerateColumns="False" OnPreRender ="gvSitiosIntervencion_PreRender">
            <Columns>
                <asp:BoundField DataField="idSitiosSol" HeaderText="ID Sitios" />
                <asp:BoundField DataField="Departamento" HeaderText="Departamento" />
                <asp:BoundField DataField="Municipio" HeaderText="Municipio" />
                <asp:BoundField DataField="Ubicacion" HeaderText="Ubicacion" />
            </Columns>
          </asp:GridView>
        <hr />
        <h1>Soportes de la Solicitud</h1>
        <hr />
        <asp:GridView ID="gvSoporteSolicitud" runat="server" CssClass="mGrid" AutoGenerateColumns="False" OnRowCommand="gvSoporteSolicitud_RowCommand" OnPreRender="gvSoporteSolicitud_PreRender">
            <Columns>
                <asp:BoundField DataField="idSoporteSol" HeaderText="ID Soporte" />
                <asp:BoundField DataField="nombreArchivo" HeaderText="Nombre" />
                <asp:BoundField DataField="tipoArchivo" HeaderText="Tipo Archivo" />
                <asp:ButtonField ButtonType="Button" Text="Ver" CommandName="VerSoporte" />
            </Columns>
        </asp:GridView>
  
         <h1> Validar Estrategias</h1>
        <hr />

        <asp:GridView ID="gvPlanAccionElegir" CssClass="mGrid" runat="server" AutoGenerateColumns="False" OnPreRender="gvPlanAccionElegir_PreRender">
            <Columns>
                <asp:BoundField DataField="idPlanAccion" HeaderText="ID Plan" />
                <asp:BoundField DataField="Estrategia" HeaderText="Estrategia" />
                <asp:BoundField DataField="Actividad" HeaderText="Actividad" />
                <asp:BoundField DataField="indicador" HeaderText="Indicador" />
                <asp:BoundField DataField="valorIndicador" HeaderText="Valor Indicador" FooterStyle-VerticalAlign="Middle" />
                <asp:BoundField DataField="ejecucionPorcentaje"  DataFormatString="{0:N2}%"  HeaderText  ="Porcentaje" FooterStyle-VerticalAlign="Middle" />
            </Columns>
         </asp:GridView>



       <h1>Compromisos Adquiridos</h1>
       <hr />
        <asp:GridView ID="gvCompromiso" CssClass="mGrid" runat="server" AutoGenerateColumns="False" OnPreRender="gvCompromiso_PreRender">
            <Columns>
                <asp:BoundField DataField="descripcion" HeaderText="Compromiso" />
                <asp:BoundField DataField="fechaCompromiso" HeaderText="fecha de Compromiso" />
                <asp:BoundField DataField="responsable" HeaderText="responsable" />
                <asp:TemplateField ShowHeader="true">
                    <ItemTemplate>
                        <asp:HiddenField runat="server" ID="HiddenFieldID" Value='<%# Eval("idCompromiso") %>'   > </asp:HiddenField>          
                    </ItemTemplate>
                </asp:TemplateField>  
            </Columns>
            </asp:GridView>


            <h1>Legalización de Beneficiarios</h1>
        <hr />
        <asp:GridView ID="gvBeneficiario" CssClass="mGrid" runat="server" AutoGenerateColumns="False" OnPreRender="gvBeneficiario_PreRender">
            <Columns>
                <asp:BoundField DataField="nombre" HeaderText="Beneficiario" />
                <asp:BoundField DataField="cantBeneficiada" HeaderText="Cantidad" />
                <asp:BoundField DataField="comentario" HeaderText="comentario" />
                <asp:TemplateField ShowHeader="False">
                   <ItemTemplate>
                      <asp:HiddenField runat="server" ID="HiddenFieldID" Value='<%# Eval("idBeneficioLega") %>'   > </asp:HiddenField>                      
                   </ItemTemplate>
                </asp:TemplateField>  
            </Columns>
            </asp:GridView>


          <h1>Soportes Legalizacion</h1>
        <hr />
        <asp:GridView ID="gvSoporteSolicitudleg" runat="server" CssClass="mGrid" AutoGenerateColumns="False" OnRowCommand="gvSoporteSolicitudleg_RowCommand" OnPreRender="gvSoporteSolicitudleg_PreRender">
            <Columns>
                <asp:BoundField DataField="idSoporteSol" HeaderText="ID Soporte" />
                <asp:BoundField DataField="nombreArchivo" HeaderText="Nombre" />
                <asp:BoundField DataField="tipoArchivo" HeaderText="Tipo Archivo" />
                <asp:ButtonField ButtonType="Button" Text="Ver" CommandName="VerSoporte" />
            </Columns>
        </asp:GridView>





        <table>
            <tbody>
                <tr>
                    <td>
                        <asp:Label ID="lblObsCoord" runat="server" Text="Obs. Coordinador" Visible="false"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine" Visible="false" Rows="2" Columns="30" Enabled ="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator id="validadorObservacion" Enabled="false" runat="server" Width="100%"
                              ControlToValidate="txtObservacion"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red">
                            </asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:Label ID="lblObsAdmin" runat="server" Text="Obs. Administrador" Visible="false"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtObservacionAdministrador" runat="server" TextMode="MultiLine" Visible="false" Rows="2" Columns="30" Enabled ="false"></asp:TextBox>
                        <asp:RequiredFieldValidator id="validatorAdmin" Enabled="false" runat="server" Width="100%"
                              ControlToValidate="txtObservacionAdministrador"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red">
                            </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr><td colspan ="5"></td></tr>
                <tr>
                    <td>
                        <asp:Label ID="lblObsCoordLega" runat="server" Text="Obs. Coord. Legalización " Visible="false"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtObservacionCoodinacionLega" runat="server" TextMode="MultiLine" Visible="false" Rows="2" Columns="30" Enabled ="false" ></asp:TextBox>
                       <asp:RequiredFieldValidator id="validatorCoordinadorLega" Enabled="false" runat="server" Width="100%"
                              ControlToValidate="txtObservacionCoodinacionLega"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red">
                            </asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:Label ID="lblObsAdminLega" runat="server" Text="Obs. Admin. Legalización" Visible="false"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtObservacionAdministradorLega" runat="server" TextMode="MultiLine" Visible="false" Rows="2" Columns="30" Enabled ="false"></asp:TextBox>
                      <asp:RequiredFieldValidator id="validatorAdminLega" Enabled="false" runat="server" Width="100%"
                              ControlToValidate="txtObservacionAdministradorLega"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red">
                            </asp:RequiredFieldValidator>
                    </td>
                </tr>
                 <tr><td colspan ="5"></td></tr>
                <tr>
                    <td>
                        <asp:Label ID="lblObsAnula" runat="server" Text="Observacion Anulacion" Visible="false"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtObservacionAnulacion" runat="server" TextMode="MultiLine" Visible="false" Rows="2" Columns="30" Enabled ="false"></asp:TextBox>
                       <asp:RequiredFieldValidator id="validatorAnula" Enabled="false" runat="server" Width="100%"
                              ControlToValidate="txtObservacionAnulacion"
                              ErrorMessage="Este campo es requerido."
                              ForeColor="Red">
                            </asp:RequiredFieldValidator>
                    </td>
                </tr>
            </tbody>
        </table>

        <asp:Panel ID="panelBotones" runat="server">
        <div class="Botones">
            <asp:Button ID="btnGuardarCont" runat="server" Visible="false" Text="Guardar y Continuar" OnClick="btnGuardarCont_Click" />
            <asp:Button ID="btnGuardarSalir" runat="server" Visible="false" Text="Guardar y Salir" OnClick="btnGuardarSalir_Click"/>
            <asp:Button ID="btnAprobarCoord" runat="server" Visible="false" Text="Aprobar Coordinador" OnClick="btnAprobarCoord_Click"/>
             <asp:Button ID="btnRechazarCoord" runat="server" Visible="false" Text="Rechazar Coordinador" OnClick="btnRechazarCoord_Click"/>
             <asp:Button ID="btnAprobarAdmin" runat="server" Visible="false" Text="Aprobar Admin" OnClick="btnAprobarAdmin_Click"/>
             <asp:Button ID="btnRechazarAdmin" runat="server" Visible="false" Text="Rechazar Admin" OnClick="btnRechazarAdmin_Click"/>
             <asp:Button ID="btnContinuar" runat="server" Visible="false" Text="Continuar" OnClick="btnContinuar_Click"/>
             <asp:Button ID="btnAnular" runat="server" Visible="false"  Text="Anular Solicitud"  OnClick="btnAnular_Click"/>

            <asp:Button ID="btnAsignarNumero" runat="server" Text="Asignar Numero de Legalizacion" OnClick="btnAsignarNumero_Click"  Visible="false"/>
            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" CausesValidation="False" />

           <!-- <input type ="button" class="botonEspecial" runat="server" value="Cancelar"  onclick="newDoc()" />-->
        </div>
    </asp:Panel>

    </div>
    

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">

     <script type="text/javascript">
         //function newDoc() {
         //    window.location.assign("frmWFSolicitudComision.aspx")
         //}

        $(document).ready(function () {
            $('#' + '<%= gvPlanAccionDetalle.ClientID %>').DataTable();
            $('#' + '<%= gvSitiosIntervencion.ClientID %>').DataTable();
            $('#' + '<%= gvSoporteSolicitud.ClientID %>').DataTable();
            $('#' + '<%= gvPlanAccionElegir.ClientID %>').DataTable();
            $('#' + '<%= gvCompromiso.ClientID %>').DataTable();
            $('#' + '<%= gvBeneficiario.ClientID %>').DataTable();
            $('#' + '<%= gvSoporteSolicitudleg.ClientID %>').DataTable();











        });
   </script>
</asp:Content>

