﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _dmin_frmWFSolicitudComision : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {

                this.FillSolicitudes();
                if (this.puedeCrearNueva())
                {
                    btnNuevaSolicitud.Visible = true;
                }
                else
                {
                    btnNuevaSolicitud.Visible = false;
                }
            }
        }
    }

    public void FillSolicitudes() {

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarSolicitud("datosSolicitudMaestro", "",0))
            
            {
                if (blPaicma.myDataSet.Tables["datosSolicitudMaestro"].Rows.Count > 0)
                {
                    this.gvSolicitud.DataMember = "datosSolicitudMaestro";
                    this.gvSolicitud.DataSource = this.blPaicma.myDataSet;
                    this.gvSolicitud.DataBind();
                }
                else
                {
                    //no existen registros
                }
            }
            else
            {
                //no se pudo consultar la solicitud
            }
        }
        else
        {
            //No se ha podido conectar a la base de datos
        }
        blPaicma.Termina();
    
    
    
    }
    protected void btnNuevaSolicitud_Click(object sender, EventArgs e)
    {
        Session["operacionSolicitud"] = "crear";
        Session["IdSolicitudRef"] = "0";
        base.Response.Redirect("frmWFNuevaSolicitud.aspx");
    }
    protected void gvSolicitud_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int intIndex = Convert.ToInt32(e.CommandArgument);
        GridViewRow selectedRow = gvSolicitud.Rows[intIndex];
        TableCell Item = selectedRow.Cells[0];
        Int64 idSolicitud = Convert.ToInt64(Item.Text);
                TableCell Item2 = selectedRow.Cells[6];
        int idEstadoActual = Convert.ToInt32(Item2.Text);
        Session["soloVerSolicitud"] = false;
        Session["IdSolicitudRef"] = Convert.ToInt64(idSolicitud);
        Session["EstadoActualSolicitud"] = Convert.ToInt32(idEstadoActual);

        //☻☻ crear sesion  que indique que solo puede modificar los campos de nueva solicitud 
        HiddenField hdnField = (HiddenField)selectedRow.FindControl("HiddenFieldID");

        Boolean permiteEditar = Convert.ToBoolean( hdnField.Value);
        Session["permiteEditarSoloFormulario"] = permiteEditar;


        if (e.CommandName == "Editar")
        {
            Session["operacionSolicitud"] = "modificar";
            base.Response.Redirect("frmWFNuevaSolicitud.aspx");

        }
        if (e.CommandName == "Ver")
        {
            base.Response.Redirect("frmWFDetalleSolicitud.aspx");
        }

        
    }
    
    protected void gvSolicitud_PreRender(object sender, EventArgs e)
    {
        if (gvSolicitud.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvSolicitud.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvSolicitud.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvSolicitud.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }

    protected Boolean Editable(int idUsuario , int permiteeditar, int estadoActual)
    {
        if (idUsuario == Convert.ToInt32(Session["IdUsuario"]))
        {

            if (estadoActual == EstadosSolicitud.LegalizacionCompletaCerrado)
            {
                return false;
            }

            if (estadoActual > 4)
            {
                if (estadoActual != 7 && estadoActual != 8)
                {
                    //☻☻   1 = true  0 = false  validar si permite editar 
                    if (permiteeditar == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
             
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }

    }

    protected bool puedeCrearNueva()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarUsuarioEnCoordinacion_bol("puedeCrearSolicitudes", false, Convert.ToInt32(Session["IdUsuario"])))
            {
                if (blPaicma.myDataSet.Tables["puedeCrearSolicitudes"].Rows.Count > 0)
                {
                    blPaicma.Termina();
                    return true;
                }
                else
                {
                    blPaicma.Termina();
                    return false;
                }
            }
            else
            {
                blPaicma.Termina();
                return false;
            }
        }
        else
        {
            blPaicma.Termina();
             return false;
        }
        
    }

    protected Boolean sePuedeVer(string idSolicitud)
    {
        return true;

    }
}