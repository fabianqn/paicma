﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmWFEditExistingBeneficiario.aspx.cs" Inherits="_dmin_frmWFEditExistingBeneficiario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">
      <h1>Editando  Beneficiario  </h1>
        <hr />
        <table style="width:50%">
            <tbody>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Beneficiarios"></asp:Label>
                    </td>
                    <td  colspan="2">
                        <asp:DropDownList ID="ddlBeneficio" runat="server" ></asp:DropDownList>
                         <asp:RequiredFieldValidator id="RequiredFieldValidator6" runat="server"
                              ControlToValidate="ddlBeneficio"
                              ErrorMessage="Beneficio  requerido."
                              ForeColor="Red" Width="100%">
                            </asp:RequiredFieldValidator>


                    </td>
                </tr>
                 <tr>
                    <td>
                         <asp:Label ID="Label2" runat="server" Text="Cantidad"></asp:Label>
                    </td>
                    <td  colspan="2">
                        <asp:TextBox ID="txtCantidad" runat="server" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                             ControlToValidate="txtCantidad"
                            ErrorMessage="Este campo es requerido." Enabled="true"
                            ForeColor="Red" Width="100%">  </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                         <asp:Label ID="Label3" runat="server" Text="Comentario Adicional"></asp:Label>
                    </td>
                    <td  colspan="2">
                        <asp:TextBox ID="txtComentario" runat="server" Width="95%"></asp:TextBox>
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                             ControlToValidate="txtComentario"
                            ErrorMessage="Este campo es requerido." Enabled="true"
                            ForeColor="Red" Width="100%">  </asp:RequiredFieldValidator>
                    </td>
                </tr>
                  <tr>
                <td colspan="2">
                    <hr />
                    <div class="Botones">

                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click"  />
                                    <input type ="button" class="botonEspecial" runat="server" value="Cancelar" onclick="newDoc()" />
                    </div>
                </td>

            </tr>


            </tbody>           
        </table>
</asp:Content>




<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">
         <script type="text/javascript">
             function newDoc() {
                 window.location.assign("frmWFSolicitudComision.aspx")
             }
          
   </script>


</asp:Content>

