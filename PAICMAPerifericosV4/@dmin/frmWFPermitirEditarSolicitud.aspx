﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmWFPermitirEditarSolicitud.aspx.cs" Inherits="_dmin_frmWFPermitirEditarSolicitud" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">
     <div class ="contacto">
         <h1> Habilitar  Solicitud</h1>
        <hr />

    <table  runat="server" id="Activiades">
        <thead>
            <tr>

                            <td colspan="3"><h1>Seleccione cómo desea buscar la Solcitud de Comisión y coloque el valor requerido</h1></td>
                            <td></td>
                            <td></td>
                            <td></td>
                             <td></td>

            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTabla" runat="server" Text="Filtro de Busqueda"></asp:Label>
                </td>
                <td style="text-align: left">

                     <asp:DropDownList ID="ddlOpcionesFiltro" runat="server" Enabled="true">
                         <asp:ListItem Value="0">----</asp:ListItem>
                         <asp:ListItem Value="1">ID Solicitud</asp:ListItem>
                         <asp:ListItem Value="2">Nro Radicado</asp:ListItem>
                     </asp:DropDownList>
                   <%-- <asp:TextBox ID="txtNombreVictima" runat="server" MaxLength="50"></asp:TextBox>--%>
                </td>


                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblAbreviatura" runat="server" Text="Valor a Buscar"></asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtBuscar" runat="server" MaxLength="20"></asp:TextBox>
                </td>
                <td></td>
                <td></td>
                <td></td>
                

            </tr>
            <tr>
                <td>
                     <asp:ImageButton ID="imgbBuscar" runat="server"
                ImageUrl="~/Images/BuscarAccion.png" ToolTip="Buscar"
                OnClick="imgbBuscar_Click" />
                </td>
                <td>
           
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>

            </tr>
        </thead>
    </table>

     <asp:GridView ID="gvSolicitud" runat="server" onrowcommand="gvSolicitud_RowCommand" CssClass="mGrid" PagerStyle-CssClass="pgr"
                            AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" >
           <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="idSolicitud" HeaderText="ID Solicitud" />
                <asp:BoundField DataField="IdentificadorSolicitud" HeaderText="N°Radicado" />
                <asp:BoundField DataField="nombreFuncionario" HeaderText="Solicitante" />
                <asp:BoundField DataField="fechaCreacion" HeaderText="Fecha Solicitud" />
                <asp:BoundField DataField="nombreestado" HeaderText="Estado" />
                <asp:ButtonField ButtonType="Button" CommandName="permiteModificar" Text="Permitir Edicion" />
            </Columns>
<PagerStyle CssClass="pgr"></PagerStyle>
        </asp:GridView>
         <asp:Label ID="LblError" runat="server" Text="" Visible="false"></asp:Label>
    </div>

    <asp:Panel ID="pnlPermiteEditarSolicitud" runat="server" Visible="false">

       <h1>Permitir Editar Solicitud  </h1>
        <hr />  
        <table style="width:80%">
            <tbody>
                <tr>
                    <td> <asp:Label ID="lblTextoEditar" runat="server" Text="Id solicitud  editar :"></asp:Label></td>
                    <td> <asp:Label ID="lblIdSolicitudPermiteEdit" runat="server" Text="Label"></asp:Label></td>
                </tr>

                <tr >
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Justificación "></asp:Label>
                    </td>
                    <td  colspan="2">
                        <asp:TextBox ID="txtJustificacion" runat="server" Width="95%" TextMode="MultiLine"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                             ControlToValidate="txtJustificacion"
                            ErrorMessage="Este campo es requerido." Enabled="true"
                            ForeColor="Red" Width="100%">  </asp:RequiredFieldValidator>


                    </td>
                </tr>

                  <tr>
                <td colspan="2">
                    <hr />
                    <div class="Botones">

                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click"  />
                        <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" CausesValidation="False" />
                    </div>
                </td>

            </tr>


            </tbody>           
        </table>




    </asp:Panel>








</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">
</asp:Content>

