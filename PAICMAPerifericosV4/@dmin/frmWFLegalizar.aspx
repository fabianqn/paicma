﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmWFLegalizar.aspx.cs" Inherits="frmWFLegalizar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">
     <div class ="contacto2">
         <h1>Legalizar Solicitud</h1>
        <hr />


        <asp:GridView ID="gvSolicitud" runat="server" onrowcommand="gvSolicitud_RowCommand" CssClass="mGrid" PagerStyle-CssClass="pgr"
                            AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" OnPreRender="gvSolicitud_PreRender" >
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>

            <Columns>
                <asp:BoundField DataField="idSolicitud" HeaderText="ID Solicitud" />
                <asp:BoundField DataField="IdentificadorSolicitud" HeaderText="N°Radicado" />
                <asp:BoundField DataField="nombreFuncionario" HeaderText="Solicitante" />
                <asp:BoundField DataField="fechaCreacion" HeaderText="Fecha Solicitud" />
                <asp:BoundField DataField="nombreestado" HeaderText="Estado" />
                <asp:ButtonField ButtonType="Button" CommandName="legalizar" Text="Legalizar" />
            </Columns>
            
<PagerStyle CssClass="pgr"></PagerStyle>
        </asp:GridView>
         <asp:Label ID="LblError" runat="server" Text="" Visible="false"></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" Runat="Server">
     <script type="text/javascript">
            
        $(document).ready(function () {
            $('#' + '<%= gvSolicitud.ClientID %>').DataTable();
        });
   </script>
</asp:Content>

