﻿<%@Page Title="Busqueda Caracterización" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmBuscaCaracterizacion.aspx.cs" Inherits="_web_frmBuscaCaracterizacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">
    
    <asp:Panel ID="pnlTitulo" runat="server" Width="90%" >
        <div align="left">
            <asp:Label ID="lblTitulo" runat="server" Text="BUSCAR CARACTERIZACIÓN"></asp:Label>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlEliminacion" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblEliminacion" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSi" runat="server"  Text="SI" onclick="btnSi_Click" />
                        <asp:Button ID="btnNo" runat="server"  Text="NO" onclick="btnNo_Click" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Aceptar" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlMenuDir" runat="server">
        <div class="Botones">
            <asp:ImageButton ID="imgbBuscar" runat="server" 
                ImageUrl="~/Images/BuscarAccion.png" onclick="imgbBuscar_Click" 
                ToolTip="Buscar" />
            <asp:ImageButton ID="imgbNuevo" runat="server" ImageUrl="~/Images/Nuevo.png" 
                onclick="imgbNuevo_Click" ToolTip="Nuevo" Visible="False" />
            <asp:ImageButton ID="imgbEditar" runat="server" 
                ImageUrl="~/Images/Editar.png" onclick="imgbEditar_Click" 
                Visible="False" ToolTip="Editar" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAccionesDir" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server" 
                ImageUrl="~/Images/buscar.png" onclick="imgbEncontrar_Click" 
                ToolTip="Ejecutar Busqueda" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png" 
                onclick="imgbGravar_Click" ToolTip="Guardar" />
            <asp:ImageButton ID="imgbCancelar" runat="server" 
                ImageUrl="~/Images/cancelar.png" onclick="imgbCancelar_Click" 
                ToolTip="Cancelar" />
            <asp:ImageButton ID="imgbEliminar" runat="server" 
                ImageUrl="~/Images/eliminar.png" onclick="imgbEliminar_Click" 
                ToolTip="Eliminar" />
        </div>
    </asp:Panel>

        <asp:Panel ID="pnlDirNuevo" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblIdVictima" runat="server" Text="ID Víctima"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtIdVictima" runat="server" MaxLength="20"></asp:TextBox>                    
                </td>
            </tr>            
            <tr>
                <td>
                    <asp:Label ID="lblNoIdentificacion" runat="server" Text="Número Identificacion"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtNoIdentificacion" runat="server" MaxLength="20"></asp:TextBox>
                    <asp:RangeValidator ID="rvNoIdentificacion" runat="server" 
                        ControlToValidate="txtNoIdentificacion" ErrorMessage="Solo valores numéricos" 
                        MaximumValue="99999999999999999999" MinimumValue="0"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblNombres" runat="server" Text="Nombre"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNombres" runat="server" MaxLength="50"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblApellido" runat="server" Text="Apellido"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtApellido" runat="server" MaxLength="50"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>

            <tr>
                <td colspan="3">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        </div>
    </asp:Panel>


      <asp:Panel ID="PnlCaracterizacionGrilla" runat="server">
        <div class="formulario2" style="width:800px;overflow-x:scroll">
            <h1>Resultados Búsqueda</h1>
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvCaracterizacion" runat="server" AutoGenerateColumns="False" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
                    onrowcommand="gvCaracterizacion_RowCommand" 
                    onpageindexchanging="gvCaracterizacion_PageIndexChanging" PageSize="20" EmptyDataText="No se encontraron registros con el criterio seleccionado." OnRowDataBound="gvCaracterizacion_RowDataBound">
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>   
                        <asp:BoundField DataField="tienecaract" 
                            HeaderText="-" />
                        <asp:BoundField DataField="ID_IMSMAVictima" 
                            HeaderText="Id Víctima" />
                        <asp:BoundField DataField="id" 
                            HeaderText="Identificador" />
                        <asp:BoundField DataField="nombres" 
                            HeaderText="Nombre" />
                        <asp:BoundField DataField="direccion" 
                            HeaderText="Dirección" />
                        <asp:BoundField DataField="departamento" 
                            HeaderText="Departamento" />
                        <asp:BoundField DataField="municipio" 
                            HeaderText="Municipio" />
                        <asp:BoundField DataField="tienecaract" 
                            HeaderText="Caracterizado" />
                        <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                            ImageUrl="~/Images/seleccionar.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorGv" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlDetalleVictimaGrilla" runat="server">
        <div class="formulario2" style="width:800px;overflow-x:scroll">
            <h1>Detalle Víctima</h1>
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvDetalleVictima" runat="server" AutoGenerateColumns="True" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="False" EmptyDataText="No se encontraron registros con el criterio seleccionado.">
                    <AlternatingRowStyle CssClass="alt" />
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblDetalleVictima" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlDetalleCaracterizacionGrilla" runat="server">
        <div class="formulario2" style="width:800px;overflow-x:scroll">
            <h1>Caracterización</h1>
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvDetalleCaracterizacion" runat="server" AutoGenerateColumns="True" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="False" EmptyDataText="No se encontraron registros con el criterio seleccionado.">
                    <AlternatingRowStyle CssClass="alt" />
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblDetalleCaracterizacion" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlDetalleEncuestaCaracterizacionGrilla" runat="server">
        <div class="formulario2" style="width:800px;overflow-x:scroll">
            <h1>Detalle Caracterización</h1>
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvDetalleEncuestaCaracterizacion" runat="server" AutoGenerateColumns="True" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="False" EmptyDataText="No se encontraron registros con el criterio seleccionado.">
                    <AlternatingRowStyle CssClass="alt" />
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblDetalleEncuestaCaracterizacion" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>