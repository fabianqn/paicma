﻿using ASP;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class _web_frmOperadorGestion : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }
            this.ftnValidarPermisos();
            this.fntCargarGrillaGestionOperador(3, string.Empty, string.Empty, string.Empty, string.Empty);
        }
    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        this.blPaicma = null;
    }
    public void ftnValidarPermisos()
    {
        string a = string.Empty;
        string arg_0B_0 = string.Empty;
        string a2 = string.Empty;
        string arg_17_0 = string.Empty;
        string[] array = HttpContext.Current.Request.RawUrl.Split(new char[]
		{
			'/'
		});
        string text = array[array.GetUpperBound(0)];
        array = text.Split(new char[]
		{
			'?'
		});
        text = array[array.GetLowerBound(0)];
        if (text != string.Empty)
        {
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (this.blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(this.Session["IdUsuario"].ToString()), text) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    a = this.blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                    this.blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                    a2 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                    this.blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();
                    if (a == "False")
                    {
                        this.imgbBuscar.Visible = false;
                    }
                    if (a2 == "False")
                    {
                        this.imgbEditar.Visible = false;
                        this.imgbGravar.Visible = false;
                    }
                }
                this.blPaicma.Termina();
                return;
            }
        }
        else
        {
            base.Response.Redirect("@dmin/frmLogin.aspx");
        }
    }


    protected void gvOperadorGestion_PreRender(object sender, EventArgs e)
    {
        if (gvOperadorGestion.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvOperadorGestion.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvOperadorGestion.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvOperadorGestion.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }

    protected void gvActividadesOperador_PreRender(object sender, EventArgs e)
    {
        if (gvActividadesOperador.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvActividadesOperador.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvActividadesOperador.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvActividadesOperador.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }

    public void fntCargarGrillaGestionOperador(int intIdTipo, string strObservacion, string strActividad, string strIdEstado, string strNombreGrupo)
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaGestionOperadorSeguimiento("strDsGrillaOperadorGestion", intIdTipo, Convert.ToInt32(this.Session["IdUsuario"].ToString()), 0, string.Empty, strObservacion, strActividad, strIdEstado, strNombreGrupo))
            {
                this.gvOperadorGestion.Columns[0].Visible = true;
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gvOperadorGestion.DataMember = "strDsGrillaOperadorGestion";
                    this.gvOperadorGestion.DataSource = this.blPaicma.myDataSet;
                    this.gvOperadorGestion.DataBind();
                    this.lblErrorGv.Text = string.Empty;
                }
                else
                {
                    this.gvOperadorGestion.DataMember = "strDsGrillaOperadorGestion";
                    this.gvOperadorGestion.DataSource = this.blPaicma.myDataSet;
                    this.gvOperadorGestion.DataBind();
                    this.lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                this.gvOperadorGestion.Columns[0].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        string url = "frmOperadorGestion.aspx";
        base.Response.Redirect(url);
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        string url = "frmOperadorGestion.aspx";
        base.Response.Redirect(url);
    }
    public void fntCargarObjetos()
    {
        this.fntCargarTipoActividad();
        this.fntCargarEstado();
    }
    public void fntCargarObjetosBusqueda()
    {
        this.fntCargarTipoActividadBusqueda();
        this.fntCargarEstadoBusqueda();
    }
    public void fntCargarTipoActividad()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaTipo("strDsTipo", "Telefono") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlActividad.DataMember = "strDsTipo";
                this.ddlActividad.DataSource = this.blPaicma.myDataSet;
                this.ddlActividad.DataValueField = "tip_Id";
                this.ddlActividad.DataTextField = "tip_Descripcion";
                this.ddlActividad.DataBind();
                //this.ddlActividad.Items.Insert(this.ddlActividad.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarTipoActividadBusqueda()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaTipo("strDsTipo", "Telefono") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlActividad.DataMember = "strDsTipo";
                this.ddlActividad.DataSource = this.blPaicma.myDataSet;
                this.ddlActividad.DataValueField = "tip_Id";
                this.ddlActividad.DataTextField = "tip_Descripcion";
                this.ddlActividad.DataBind();
                this.ddlActividad.Items.Insert(this.ddlActividad.Attributes.Count, "Sin Actividad");
                this.ddlActividad.Items.Insert(this.ddlActividad.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarEstado()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaEstadoSeguimientoOperador_bol("strDsEstado") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlEstado.DataMember = "strDsEstado";
                this.ddlEstado.DataSource = this.blPaicma.myDataSet;
                this.ddlEstado.DataValueField = "est_Id";
                this.ddlEstado.DataTextField = "est_Descripcion";
                this.ddlEstado.DataBind();
                this.ddlEstado.Items.Insert(this.ddlEstado.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarEstadoBusqueda()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaEstadoSeguimientoOperador_bol("strDsEstado") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlEstado.DataMember = "strDsEstado";
                this.ddlEstado.DataSource = this.blPaicma.myDataSet;
                this.ddlEstado.DataValueField = "est_Id";
                this.ddlEstado.DataTextField = "est_Descripcion";
                this.ddlEstado.DataBind();
                this.ddlEstado.Items.Insert(this.ddlEstado.Attributes.Count, "Por revisar");
                this.ddlEstado.Items.Insert(this.ddlEstado.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    protected void gvOperadorGestion_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            this.gvOperadorGestion.Columns[0].Visible = true;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow = this.gvOperadorGestion.Rows[index];
            TableCell tableCell = gridViewRow.Cells[0];
            int num = Convert.ToInt32(tableCell.Text);
            this.Session["intIdGestionOperador"] = num;
            this.imgbEditar.Visible = true;
            this.gvOperadorGestion.Columns[0].Visible = false;
            string a = string.Empty;
            string text = string.Empty;
            string str = string.Empty;
            this.pnlEliminacion.Visible = false;
            this.pnlRespuesta.Visible = false;
            this.pnlMenuOperadorGestion.Visible = false;
            this.pnlAccionesOperadorGestion.Visible = true;
            this.imgbEncontrar.Visible = false;
            this.imgbGravar.Visible = true;
            this.imgbCancelar.Visible = true;
            this.pnlOperadorGestionNuevo.Visible = true;
            this.PnlOperadorGestionGrilla.Visible = false;
            this.fntCargarObjetos();
            this.ftnInactivarControles(true);
            this.Session["Operacion"] = "Actualizar";
            this.fntInactivarControles(false);
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (this.blPaicma.fntConsultaGestionOperadorSeguimiento("strDsGrillaOperadorGestion", 2, 0, Convert.ToInt32(this.Session["intIdGestionOperador"].ToString()), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.txtDocumento.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                    this.lblRuta.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                    this.txtFechaCarga.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                    this.txtObservacion.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][7].ToString();
                    this.txtOperador.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][6].ToString();
                    a = this.blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                    if (a == ".pdf")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/pdf.png";
                    }
                    if (a == ".doc" || a == ".docx")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/word.png";
                    }
                    if (a == ".xls" || a == ".xlsx")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/excel.png";
                    }
                    if (a == ".rar")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/zip.png";
                    }
                    if (a == ".zip")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/zip.png";
                    }
                    if (a == ".jpg" || a == ".gif" || a == ".png" || a == ".tif")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/imagen.png";
                    }
                    if (a == ".mp3" || a == ".mp3" || a == ".wma" || a == ".midi")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/musica.png";
                    }
                    if (a == ".pptx" || a == ".ppt")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/ppt.png";
                    }
                    if (a == ".txt")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/txt.png";
                    }
                    if (a == ".bak")
                    {
                        this.imbDocumento.ImageUrl = "~/Images/baseDatos.png";
                    }
                    text = ConfigurationManager.AppSettings["RutaServer"];
                    str = "Operador/";
                    text = text + str + this.txtDocumento.Text.Trim();
                   // this.lblRuta.Text = text;

                    

                    //this.gvActividadesOperador.Columns[0].Visible = true;
                    this.pnlGestionRealizada.Visible = true;
                    this.gvActividadesOperador.DataMember = "strDsGrillaOperadorGestion";
                    this.gvActividadesOperador.DataSource = this.blPaicma.myDataSet;
                    this.gvActividadesOperador.DataBind();
                    this.lblErrorGvActividades.Text = string.Empty;
                    //this.gvActividadesOperador.Columns[0].Visible = false;
                }
                this.blPaicma.Termina();
            }
        }
    }
    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        string a = string.Empty;
        string text = string.Empty;
        string str = string.Empty;
        this.pnlEliminacion.Visible = false;
        this.pnlRespuesta.Visible = false;
        this.pnlMenuOperadorGestion.Visible = false;
        this.pnlAccionesOperadorGestion.Visible = true;
        this.imgbEncontrar.Visible = false;
        this.imgbGravar.Visible = true;
        this.imgbCancelar.Visible = true;
        this.pnlOperadorGestionNuevo.Visible = true;
        this.PnlOperadorGestionGrilla.Visible = false;
        this.fntCargarObjetos();
        this.ftnInactivarControles(true);
        this.Session["Operacion"] = "Actualizar";
        this.fntInactivarControles(false);
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaGestionOperadorSeguimiento("strDsGrillaOperadorGestion", 2, 0, Convert.ToInt32(this.Session["intIdGestionOperador"].ToString()), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.txtDocumento.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                this.lblRuta.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                this.txtFechaCarga.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                this.txtObservacion.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][7].ToString();
                this.txtOperador.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][6].ToString();
                a = this.blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                if (a == ".pdf")
                {
                    this.imbDocumento.ImageUrl = "~/Images/pdf.png";
                }
                if (a == ".doc" || a == ".docx")
                {
                    this.imbDocumento.ImageUrl = "~/Images/word.png";
                }
                if (a == ".xls" || a == ".xlsx")
                {
                    this.imbDocumento.ImageUrl = "~/Images/excel.png";
                }
                if (a == ".rar")
                {
                    this.imbDocumento.ImageUrl = "~/Images/zip.png";
                }
                if (a == ".zip")
                {
                    this.imbDocumento.ImageUrl = "~/Images/zip.png";
                }
                if (a == ".jpg" || a == ".gif" || a == ".png" || a == ".tif")
                {
                    this.imbDocumento.ImageUrl = "~/Images/imagen.png";
                }
                if (a == ".mp3" || a == ".mp3" || a == ".wma" || a == ".midi")
                {
                    this.imbDocumento.ImageUrl = "~/Images/musica.png";
                }
                if (a == ".pptx" || a == ".ppt")
                {
                    this.imbDocumento.ImageUrl = "~/Images/ppt.png";
                }
                if (a == ".txt")
                {
                    this.imbDocumento.ImageUrl = "~/Images/txt.png";
                }
                if (a == ".bak")
                {
                    this.imbDocumento.ImageUrl = "~/Images/baseDatos.png";
                }
                text = ConfigurationManager.AppSettings["RutaServer"];
                str = "Operador/";
                text = text + str + this.txtDocumento.Text.Trim();
                //this.lblRuta.Text = text;
                this.gvActividadesOperador.Columns[0].Visible = true;
                this.pnlGestionRealizada.Visible = true;
                this.gvActividadesOperador.DataMember = "strDsGrillaOperadorGestion";
                this.gvActividadesOperador.DataSource = this.blPaicma.myDataSet;
                this.gvActividadesOperador.DataBind();
                this.lblErrorGvActividades.Text = string.Empty;
                this.gvActividadesOperador.Columns[0].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    protected void fntInactivarControles(bool bolValor)
    {
        this.lblNombreGrupo.Visible = bolValor;
        this.ddlNombreGrupo.Visible = bolValor;
    }
    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        if (this.Session["Operacion"].ToString() == "Actualizar" && this.ftnValidarCampos_bol())
        {
            if (this.ftnIngresarPersona())
            {
                this.fntInactivarPaneles();
                this.lblRespuesta.Text = this.lblRespuesta.Text + " Registro creado satisfactoriamente";
                return;
            }
            this.fntInactivarPaneles();
            this.lblRespuesta.Text = this.lblRespuesta.Text + " Problema al ingresar el registro, por favor comuníquese con el administrador.";
        }
    }
    public bool ftnValidarCampos_bol()
    {
        if (this.ddlActividad.SelectedValue == "Seleccione...")
        {
            this.lblError.Text = "Debe seleccionar la actividad a realizar.";
            return false;
        }
        if (this.ddlEstado.SelectedValue == "Seleccione...")
        {
            this.lblError.Text = "Debe seleccionar un estado.";
            return false;
        }
        if (this.txtObservacionRevision.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar una observacion.";
            return false;
        }
        string a = string.Empty;
        string a2 = string.Empty;
        a = this.fuDocumentoSeguimiento.FileName;
        a2 = Path.GetExtension(this.fuDocumentoSeguimiento.PostedFile.FileName);
        if (a != string.Empty)
        {
            if (!(a2 == ".txt") && !(a2 == ".pdf") && !(a2 == ".doc") && !(a2 == ".docx") && !(a2 == ".xls") && !(a2 == ".xlsx") && !(a2 == ".rar") && !(a2 == ".zip") && !(a2 == ".jpg") && !(a2 == ".gif") && !(a2 == ".png") && !(a2 == ".tif") && !(a2 == ".mp3") && !(a2 == ".mp4") && !(a2 == ".wma") && !(a2 == ".midi") && !(a2 == ".pptx") && !(a2 == ".ppt") && !(a2 == ".pps"))
            {
                this.lblError.Text = "Este tipo de archivo no se puede subir. Por favor verificar.";
                return false;
            }
            this.lblError.Text = string.Empty;
        }
        return true;
    }
    public bool ftnIngresarPersona()
    {
        bool result = false;
        string text = string.Empty;
        string strExt = string.Empty;
        string text2 = string.Empty;
        string nameSinExtension = string.Empty;
        string text3 = string.Empty;
        string str = string.Empty;
        string unique = string.Empty;
        string str2 = "Operador\\Revision";
        text = this.fuDocumentoSeguimiento.FileName;
        if (text != string.Empty)
        {
            strExt = Path.GetExtension(this.fuDocumentoSeguimiento.PostedFile.FileName);
            text2 = base.Server.MapPath(this.fuDocumentoSeguimiento.FileName);
            nameSinExtension = Path.GetFileNameWithoutExtension(this.fuDocumentoSeguimiento.PostedFile.FileName);
           // text3 = str + str2 + "\\" + nameSinExtension + strExt;
            int length = text2.Length;
            int length2 = text.Length;
            int length3 = length - length2;
            str = text2.Substring(0, length3);
            unique = Guid.NewGuid().ToString();
            text = unique + strExt;
            text3 = str + str2 + "\\" + unique + strExt;
            //text3 = str + str2 + "\\" + text;
            this.fuDocumentoSeguimiento.SaveAs(text3);
        }
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntIngresarSeguimientoOperador_bol(Convert.ToInt32(this.Session["intIdGestionOperador"].ToString()), Convert.ToInt32(this.Session["IdUsuario"].ToString()), Convert.ToInt32(this.ddlActividad.SelectedValue.ToString()), this.txtObservacionRevision.Text.Trim(), Convert.ToInt32(this.ddlEstado.SelectedValue.ToString()), text3, nameSinExtension + strExt, strExt))
            {
                result = true;
                this.fntTraerGrupoFuncionarioOperadores(Convert.ToInt32(this.Session["intIdGestionOperador"].ToString()));
            }
            else
            {
                result = false;
            }
            this.blPaicma.Termina();
        }
        return result;
    }
    public void fntInactivarPaneles()
    {
        this.pnlRespuesta.Visible = true;
        this.pnlAccionesOperadorGestion.Visible = false;
        this.pnlOperadorGestionNuevo.Visible = false;
        this.pnlMenuOperadorGestion.Visible = false;
        this.pnlEliminacion.Visible = false;
        this.pnlGestionRealizada.Visible = false;
    }
    protected void imbDocumento_Click(object sender, ImageClickEventArgs e)
    {

        string nombredelDocumentos = Path.GetFileName(this.lblRuta.Text.Trim());
       string text = ConfigurationManager.AppSettings["RutaServer"];
       string str = "Operador/";
       text = text + str + nombredelDocumentos;
       this.Session["strRutaDocumento"] = text;
        base.Response.Write("<script language='JavaScript'>window.open('" + this.Session["strRutaDocumento"].ToString() + "')</script>");
    }
    protected void gvActividadesOperador_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string text = string.Empty;
        string str = string.Empty;
        string text2 = string.Empty;
        if (e.CommandName == "Ver")
        {
            //this.gvActividadesOperador.Columns[0].Visible = true;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow = this.gvActividadesOperador.Rows[index];
            TableCell tableCell = gridViewRow.Cells[0];
            string rutaBuscada = string.Empty;
            string nombreRealDocumento = string.Empty;
            try
            {
                int IdABuscar = Convert.ToInt32(tableCell.Text);
                if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
                {
                    if (this.blPaicma.fntConsultarSeguimientoEspecifico("rutadeDocumentoaBuscar", IdABuscar))
                    {
                        rutaBuscada = this.blPaicma.myDataSet.Tables[0].Rows[0][0].ToString();
                        nombreRealDocumento = Path.GetFileName(rutaBuscada);

                    }
                }          

                //TableCell tableCell2 = gridViewRow.Cells[3];
                //string text3 = tableCell2.Text;
                //text2 = text3;
                //text2 = text2.Replace("&nbsp;", string.Empty);
                if (nombreRealDocumento.Trim() != string.Empty)
                {
                    text = ConfigurationManager.AppSettings["RutaServer"];
                    str = "Operador/Revision/";
                    text = text + str + nombreRealDocumento;
                    base.Response.Write("<script language='JavaScript'>window.open('" + text + "')</script>");
                    this.lblErrorGvActividades.Text = string.Empty;
                }
                else
                {
                    this.lblErrorGvActividades.Text = "No hay un documento asociado en el registro de seguimiento.";
                }
            }
            catch
            {
                this.lblErrorGvActividades.Text = "No hay un documento asociado en el registro de seguimiento.";
            }
            //this.gvActividadesOperador.Columns[0].Visible = false;
        }
    }
    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlAccionesOperadorGestion.Visible = true;
        this.pnlMenuOperadorGestion.Visible = false;
        this.imgbEncontrar.Visible = true;
        this.imgbGravar.Visible = false;
        this.imgbCancelar.Visible = true;
        this.PnlOperadorGestionGrilla.Visible = false;
        this.pnlGestionRealizada.Visible = false;
        this.pnlOperadorGestionNuevo.Visible = true;
        this.ftnLimpiarControles();
        this.fntCargarObjetosBusqueda();
        this.ftnInactivarControles(false);
        this.fntInactivarControles(true);
        this.fntValidarGruposBusqueda();
    }
    public int fntValidarGruposBusqueda()
    {
        int num = 0;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaGruposDisponiblesUsuario_bol("dsGrupos", Convert.ToInt32(this.Session["IdUsuario"].ToString())))
            {
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    num = this.blPaicma.myDataSet.Tables[0].Rows.Count;
                    this.ddlNombreGrupo.Visible = true;
                    this.ddlNombreGrupo.DataMember = "dsGrupos";
                    this.ddlNombreGrupo.DataSource = this.blPaicma.myDataSet;
                    this.ddlNombreGrupo.DataValueField = "gruOpe_Id";
                    this.ddlNombreGrupo.DataTextField = "gruOpe_Nombre";
                    this.ddlNombreGrupo.DataBind();
                    if (num > 1)
                    {
                        this.ddlNombreGrupo.Items.Insert(this.ddlNombreGrupo.Attributes.Count, "Seleccione...");
                    }
                }
                else
                {
                    this.fntInactivarPaneles();
                    this.lblRespuesta.Text = "No tiene ningún grupo asignado, comuníquese con el administrador para que le asignen un grupo.";
                }
            }
            this.blPaicma.Termina();
        }
        return num;
    }
    public void ftnLimpiarControles()
    {
        this.txtDocumento.Text = string.Empty;
        this.txtFechaCarga.Text = string.Empty;
        this.txtObservacion.Text = string.Empty;
        this.txtOperador.Text = string.Empty;
        this.txtObservacionRevision.Text = string.Empty;
    }
    public void ftnInactivarControles(bool bolValor)
    {
        this.lblDocumento.Visible = bolValor;
        this.txtDocumento.Visible = bolValor;
        this.lblFecha.Visible = bolValor;
        this.txtFechaCarga.Visible = bolValor;
        this.lblObservaciones.Visible = bolValor;
        this.txtObservacion.Visible = bolValor;
        this.lblOperador.Visible = bolValor;
        this.txtOperador.Visible = bolValor;
        this.imbDocumento.Visible = bolValor;
        this.lblDocumentoSeguimiento.Visible = bolValor;
        this.fuDocumentoSeguimiento.Visible = bolValor;
    }
    protected void imgbEncontrar_Click(object sender, ImageClickEventArgs e)
    {
        string strActividad = string.Empty;
        string strIdEstado = string.Empty;
        string strNombreGrupo = string.Empty;
        if (this.ddlActividad.SelectedValue != "Seleccione...")
        {
            strActividad = this.ddlActividad.SelectedItem.ToString();
        }
        if (this.ddlEstado.SelectedValue != "Seleccione...")
        {
            strIdEstado = this.ddlEstado.SelectedItem.ToString();
        }
        if (this.ddlNombreGrupo.SelectedValue != "Seleccione...")
        {
            strNombreGrupo = this.ddlNombreGrupo.SelectedItem.ToString();
        }
        this.fntCargarGrillaGestionOperador(5, this.txtObservacionRevision.Text.Trim(), strActividad, strIdEstado, strNombreGrupo);
        this.pnlAccionesOperadorGestion.Visible = false;
        this.pnlMenuOperadorGestion.Visible = true;
        this.pnlOperadorGestionNuevo.Visible = false;
        this.PnlOperadorGestionGrilla.Visible = true;
    }
    //protected void gvActividadesOperador_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    this.gvActividadesOperador.Columns[0].Visible = true;
    //    this.gvActividadesOperador.PageIndex = e.NewPageIndex;
    //    if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
    //    {
    //        if (this.blPaicma.fntConsultaGestionOperadorSeguimiento("strDsGrillaOperadorGestion", 2, 0, Convert.ToInt32(this.Session["intIdGestionOperador"].ToString()), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
    //        {
    //            this.gvActividadesOperador.DataMember = "strDsGrillaOperadorGestion";
    //            this.gvActividadesOperador.DataSource = this.blPaicma.myDataSet;
    //            this.gvActividadesOperador.DataBind();
    //        }
    //        this.blPaicma.Termina();
    //    }
    //    this.gvActividadesOperador.Columns[0].Visible = false;
    //}
    protected void gvOperadorGestion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvOperadorGestion.Columns[0].Visible = true;
        this.gvOperadorGestion.PageIndex = e.NewPageIndex;
        this.fntCargarGrillaGestionOperador(3, string.Empty, string.Empty, string.Empty, string.Empty);
        this.gvOperadorGestion.Columns[0].Visible = false;
    }
    public bool fntTraerGrupoFuncionarioOperadores(int intIdGestionOperador)
    {
        string strCorreoTo = string.Empty;
        string strTitulo = string.Empty;
        string strTextoMensaje = string.Empty;
        string text = string.Empty;
        string text2 = string.Empty;
        bool envioCorreo = false;
        string fallidos = string.Empty;
        string Destinatarios = string.Empty;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaGrupoFuncionariosDocumento("strDsGrupoFuncionario", intIdGestionOperador))
            {
                int count = this.blPaicma.myDataSet.Tables[0].Rows.Count;
                text2 = this.blPaicma.myDataSet.Tables[0].Rows[0][0].ToString();
                text = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                if (count > 0)
                {
                    string[] array = new string[count];
                    for (int i = 0; i < count; i++)
                    {
                        array[i] = this.blPaicma.myDataSet.Tables[0].Rows[i][2].ToString();
                    }
                    strTitulo = " Grupo " + text + " , documento " + text2;
                    strTextoMensaje = "El Usuario: "+ txtOperador.Text.Trim() +" ha realizado una nueva gestión de seguimiento en el documento " + text2 + " del grupo " + text;
                    strTextoMensaje = strTextoMensaje + " adjuntando la siguiente observacion: " + txtObservacionRevision.Text.Trim();

                    //strTitulo = " Esto es una Prueba ";
                    //strTextoMensaje = " Esto es una Prueba ";

                    for (int j = 0; j < count; j++)
                    {
                        strCorreoTo = array[j].ToString();
                        //strCorreoTo = "anibalcool@gmail.com";
                        Destinatarios = Destinatarios + " - " + strCorreoTo;
                        envioCorreo = this.ftnEnviarCorreo(strCorreoTo, strTitulo, strTextoMensaje);
                        if (!envioCorreo)
                        {
                            fallidos = fallidos + " - " + strCorreoTo;
                        }
                    }
                    
                    
                }
            }
            this.blPaicma.Termina();
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntInsertarLogEmail_bol("DAICMA", Destinatarios, strTitulo, strTextoMensaje, "OperadorGestion", DateTime.Now, envioCorreo, fallidos))
                {
                    if (!envioCorreo)
                    {
                        this.lblRespuesta.Text = "Problemas para enviar a algunos correos: " + fallidos + " sin embargo ";
                    }
                }
            }
        }
        return true;
    }
    public bool ftnEnviarCorreo(string strCorreoTo, string strTitulo, string strTextoMensaje)
    {
        bool result = false;
        string text = string.Empty;
        string displayName = string.Empty;
        string host = string.Empty;
        string password = string.Empty;
        //Guardar en Log
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaCorreo("strDsCorreo", 0) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                text = this.blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                displayName = this.blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                host = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                int port = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][2].ToString());
                password = this.blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                int num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][6].ToString());
                if (num == 1)
                {
                    MailMessage mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress(text, displayName);
                    mailMessage.To.Add(strCorreoTo);
                    mailMessage.Subject = strTitulo;
                    mailMessage.Body = strTextoMensaje;
                    mailMessage.IsBodyHtml = true;
                    SmtpClient smtpClient = new SmtpClient();
                    smtpClient.Host = host;
                    smtpClient.Port = port;
                    smtpClient.EnableSsl = false;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new NetworkCredential(text, password);
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    //MailMessage mailMessage = new MailMessage();
                    //mailMessage.From = new MailAddress(text, displayName);
                    ////mailMessage.From = new MailAddress("anibal.morenop@gmail.com", "Anibal");
                    //mailMessage.To.Add(strCorreoTo);
                    //mailMessage.Subject = strTitulo;
                    //mailMessage.Body = strTextoMensaje;
                    //mailMessage.IsBodyHtml = true;
                    //SmtpClient smtpClient = new SmtpClient();
                    //smtpClient.Host = host;
                    ////smtpClient.Host = "smtp.gmail.com";
                    ////smtpClient.Port = 587;
                    //smtpClient.Port = port;
                    //smtpClient.EnableSsl = false;
                    //smtpClient.UseDefaultCredentials = false;
                    //smtpClient.Credentials = new NetworkCredential(text, password);
                    ////smtpClient.Credentials = new NetworkCredential("anibal.morenop@gmail.com", "73881936814199");
                    //smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    try
                    {
                        smtpClient.Send(mailMessage);
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
            }
            this.blPaicma.Termina();
        }
        return result;
    }
}