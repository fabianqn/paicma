﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Data;

public partial class _dmin_frmEncuesta : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {

            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                ftnValidarPermisos();

            }
            fntCargarDepartamento();
            CargarPaises();
        }
    }
    
    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }

    public void ftnValidarPermisos()
    {
        //int intIdFormulario = 0;
        string strBuscar = string.Empty;
        string strNuevo = string.Empty;
        string strEditar = string.Empty;
        string strEliminar = string.Empty;

        //obtiene el nombre de la pagina actual
        string[] strRutaPagina = HttpContext.Current.Request.RawUrl.Split('/');
        string strNombrePagina = strRutaPagina[strRutaPagina.GetUpperBound(0)];
        strRutaPagina = strNombrePagina.Split('?');
        strNombrePagina = strRutaPagina[strRutaPagina.GetLowerBound(0)];
        //fin obtiene el nombre de la pagina actual


        if (strNombrePagina != string.Empty)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                //intIdFormulario = Convert.ToInt32(Request.QueryString["op"].ToString());
                //Session["intIdFormulario"] = intIdFormulario;
                if (blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(Session["IdUsuario"].ToString()), strNombrePagina))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strBuscar = blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                        strNuevo = blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                        strEditar = blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                        strEliminar = blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();

                        if (strBuscar == "False")
                        {
                            //imgbBuscar.Visible = false;
                        }
                        if (strNuevo == "False")
                        {
                            imgbNuevo.Visible = false;
                        }
                        if (strEditar == "False")
                        {
                            //imgbEditar.Visible = false;
                            imgbGravar.Visible = false;
                        }
                        if (strEliminar == "False")
                        {
                            //imgbEliminar.Visible = false;
                        }

                    }
                }
                blPaicma.Termina();
            }
        }
        else
        {
            Response.Redirect("@dmin/frmLogin.aspx");
        }
    }

    public void fntInactivarPaneles(Boolean bolValor)
    {
        pnlAccionesVariable.Visible = bolValor;
        pnlMenuVariable.Visible = bolValor;
    }

    public void fntCargarVariables()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaVariablesAdmin("strDsVariables", string.Empty))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    //ddlidVariable.DataMember = "strDsVariables";
                    //ddlidVariable.DataSource = blPaicma.myDataSet;
                    //ddlidVariable.DataValueField = "idVariable";
                    //ddlidVariable.DataTextField = "nombreVariable";
                    //ddlidVariable.DataBind();
                    //ddlidVariable.Items.Insert(ddlidVariable.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarObjetos()
    {
        fntCargarVariables();
    }

    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles(false);
        pnlAccionesVariable.Visible = true;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        pnlBuscar.Visible = true;
        fntCargarObjetos();
        Session["Operacion"] = "Crear";
    }

    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        string strRuta = "frmEncuesta.aspx";
        Response.Redirect(strRuta);
    }


    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["Operacion"].ToString() == "Crear")
        {
            if (fntIngresarEncuesta())
            {
                lblError.Visible = false;
                lblError.Text = string.Empty;
                fntInactivarPaneles(false);
                pnlRespuesta.Visible = true;
                lblRespuesta.Text = "Encuesta creada satisfactoriamente";
            }
            else
            {
                lblError.Visible = true;
            }
        }
    }


    public Boolean ftnValidarCampos_bol()
    {

        //if (txtidentificadorValor.Text.Trim() == string.Empty)
        //{
        //    lblError.Text = "Debe digitar el Identificador de la Opción.";
        //    return false;
        //}
        //if (txttextoValor.Text.Trim() == string.Empty)
        //{
        //    lblError.Text = "Debe digitar el Valor de la Opción.";
        //    return false;
        //}

        return true;
    }

    

    public void CargarPaises()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarPaises_bol("strDsPaises"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlPaises.DataMember = "strDsPaises";
                    ddlPaises.DataSource = blPaicma.myDataSet;
                    ddlPaises.DataValueField = "pa_id";
                    ddlPaises.DataTextField = "nomPais";
                    ddlPaises.DataBind();
                    ddlPaises.Items.Insert(ddlPaises.Attributes.Count, "Seleccione...");

                }
            }
            blPaicma.Termina();
        }
    }



    public void fntCargarDepartamento()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaDepartamento_bol("strDsDepartamento", 1, ""))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlDepartamento.DataMember = "strDsDepartamento";
                    ddlDepartamento.DataSource = blPaicma.myDataSet;
                    ddlDepartamento.DataValueField = "codDepartamentoAlf2";
                    ddlDepartamento.DataTextField = "nomDepartamento";
                    ddlDepartamento.DataBind();
                    ddlDepartamento.Items.Insert(ddlDepartamento.Attributes.Count, "Seleccione...");

                    ddlDepartamento2.DataMember = "strDsDepartamento";
                    ddlDepartamento2.DataSource = blPaicma.myDataSet;
                    ddlDepartamento2.DataValueField = "codDepartamentoAlf2";
                    ddlDepartamento2.DataTextField = "nomDepartamento";
                    ddlDepartamento2.DataBind();
                    ddlDepartamento2.Items.Insert(ddlDepartamento.Attributes.Count, "Seleccione...");

                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarMunicipio()
    {
        if (ddlDepartamento.SelectedValue.ToString() != "Seleccione...") { 

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaMunicipios("strDsMunicipio", 1, ddlDepartamento.SelectedValue))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlMunicipio.DataMember = "strDsMunicipio";
                    ddlMunicipio.DataSource = blPaicma.myDataSet;
                    ddlMunicipio.DataValueField = "codMunicipioAlf5";
                    ddlMunicipio.DataTextField = "nomMunicipio";
                    ddlMunicipio.DataBind();
                    ddlMunicipio.Items.Insert(ddlMunicipio.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
        }
        else
        {
            ddlMunicipio.Items.Clear();
            ddlMunicipio.Items.Insert(ddlMunicipio.Attributes.Count, "Seleccione...");
                
        }
    }

    public void fntCargarMunicipio2()
    {
        if (ddlDepartamento2.SelectedValue.ToString() != "Seleccione...")
        {

            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultaMunicipios("strDsMunicipio2", 1, ddlDepartamento2.SelectedValue))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        ddlMunicipio2.DataMember = "strDsMunicipio2";
                        ddlMunicipio2.DataSource = blPaicma.myDataSet;
                        ddlMunicipio2.DataValueField = "codMunicipioAlf5";
                        ddlMunicipio2.DataTextField = "nomMunicipio";
                        ddlMunicipio2.DataBind();
                        ddlMunicipio2.Items.Insert(ddlMunicipio2.Attributes.Count, "Seleccione...");
                    }
                }
                blPaicma.Termina();
            }
        }
        else
        {
            ddlMunicipio2.Items.Clear();
            ddlMunicipio2.Items.Insert(ddlMunicipio2.Attributes.Count, "Seleccione...");

        }
    }

    protected void ddlDepartamento_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        fntCargarMunicipio();
    }


    public Boolean fntIngresarEncuesta()
    {
        Boolean bolValor = false;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarEncuesta_bol(
                Convert.ToInt32(txtroll.Text) ,  txtcedula.Text ,  txtnombre.Text ,  Convert.ToInt32(txtedad.Text) ,txtdia.Value.ToString() ,  txtmes.Value.ToString() ,  txtanio.Text ,  
                Convert.ToInt32(ddlDepartamento.SelectedValue.ToString()) ,  Convert.ToInt32(ddlMunicipio.SelectedValue.ToString()),  txtvereda.Text ,  txtcorregimiento.Text ,  txtdireccionVivienda.Text ,  
                Convert.ToInt32(txtarea.Text) ,  Convert.ToInt32(txtestrato.Text) , Convert.ToInt32( txttotal_hogares.Text) , Convert.ToInt32( txtnum_personas_hogar.Text) ,
                Convert.ToInt32(txtnum_personas_vivienda.Text), txtemail.Text, txtresultado_final.Text, Convert.ToInt32(ddlDepartamento2.SelectedValue.ToString()), Convert.ToInt32(ddlMunicipio2.SelectedValue.ToString()),
                Convert.ToInt32(ddlPaises.SelectedValue.ToString()), Convert.ToInt32(ddlSexo.Value), txtnom_enc.Text, Convert.ToInt32(txtautorizacion.Text), txtvictim_guid.Text
             ))
            {
                lblRespuesta.Text = "Registro creado satisfactoriamente.";
            }
            else
            {
                lblRespuesta.Text = "No se puede crear el registro.";
            }
            fntInactivarPaneles(false);
            pnlRespuesta.Visible = true;

            blPaicma.Termina();
        }
        return bolValor;
    }

    public Boolean ftnValidarSeguridadCampos_bol(string strTextoCampo)
    {
        Boolean bolValor = false;
        int intCantidadSeguridad = 0;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntEliminarTablaExpresionCampo_bol())
            {
                if (blPaicma.fntIngresarExpresionCampo_bol(strTextoCampo))
                {
                    if (blPaicma.fntConsultaExpresionSeguridad("strDsSeguridadCampos", strTextoCampo))
                    {
                        intCantidadSeguridad = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                        if (intCantidadSeguridad > 0)
                        {
                            bolValor = false;
                        }
                        else
                        {
                            bolValor = true;
                        }
                    }
                }
            }
            blPaicma.Termina();
        }
        return bolValor;
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        string strRuta = "frmEncuesta.aspx";
        Response.Redirect(strRuta);
    }


    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles(false);
        pnlAccionesVariable.Visible = true;
        //imgbEncontrar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        int intidValorXVariableNew = 0;
        fntCargarObjetos();
        Session["Operacion"] = "Actualizar";
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            intidValorXVariableNew = Convert.ToInt32(Session["intidValorXVariableNew"].ToString());
            if (blPaicma.fntConsultaVariable_bol("strDsVariable", intidValorXVariableNew))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {

                    //hndidValorXVariable.Value = blPaicma.myDataSet.Tables[0].Rows[0][0].ToString();
                    //txtidentificadorValor.Text = blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                    //txttextoValor.Text = blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();

                    ftnValidarPermisos();
                }
            }
            blPaicma.Termina();
        }
    }


    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles(false);
        pnlAccionesVariable.Visible = true;
 //       imgbEncontrar.Visible = true;
        //imgbEliminar.Visible = false;
        imgbGravar.Visible = false;
        imgbCancelar.Visible = true;
        fntCargarObjetos();
        fntLimpiarObjetos();

    }

    public void fntLimpiarObjetos()
    {
        //hndidValorXVariable.Value = string.Empty;
        //txtidentificadorValor.Text = string.Empty;
        //txttextoValor.Text = string.Empty;
    }

    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaDetalleVictimaGrilla("strDsDetalleVictima", txtvictim_guid.Text))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {

                    gridVictimas.DataMember = "strDsDetalleVictima";
                    gridVictimas.DataSource = blPaicma.myDataSet;
                    gridVictimas.DataBind();
                    lista.Visible = true;

                    //pnlEncuestaNuevo.Visible = true;
                    //lblError.Text = "";

                    //txtroll.Text = "1";
                    //txtcedula.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtnombre.Text = blPaicma.myDataSet.Tables[0].Rows[0]["Nombres"].ToString() + " " + blPaicma.myDataSet.Tables[0].Rows[0]["Apellidos"].ToString();
                    //txtedad.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtdia.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtmes.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtanio.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtdepartamento1.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtmunicipio1.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtvereda.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtcorregimiento.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtdireccionVivienda.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtarea.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtestrato.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txttotal_hogares.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtnum_personas_hogar.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtnum_personas_vivienda.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtemail.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtresultado_final.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtdepartamento2.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtmunicipio2.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtpais.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtsexo.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtnom_enc.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();
                    //txtautorizacion.Text = blPaicma.myDataSet.Tables[0].Rows[0]["NumeroIdentificacion"].ToString();

                    //hndidValorXVariable.Value = blPaicma.myDataSet.Tables[0].Rows[0][0].ToString();
                    //txtidentificadorValor.Text = blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                    //txttextoValor.Text = blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();


                }
                else
                {
                    pnlEncuestaNuevo.Visible = false;
                    lblError.Text = "No hay resultados con el criterio de búsqueda.";
                }
            }
            blPaicma.Termina();
        }
        
       

    }
    protected void ddlDepartamento2_SelectedIndexChanged(object sender, EventArgs e)
    {
        fntCargarMunicipio2();
    }
    protected void gridVictimas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "seleccionar")
        {
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gridVictimas.Rows[intIndex];
            TableCell Item = selectedRow.Cells[0];
            TableCell item2 = selectedRow.Cells[1];

            string IdVictima = Item.Text;
            string NumeroDocumento = item2.Text;
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultaVictimaPorId("resultadoEncuestaVictima", "car_Encuesta", "cedula", NumeroDocumento, "Nombre"))
                {
                    if (blPaicma.myDataSet.Tables["resultadoEncuestaVictima"].Rows.Count > 0)
                    {
                        DataTable dtTable = new DataTable();
                        dtTable = blPaicma.myDataSet.Tables["resultadoEncuestaVictima"];
                        
                        txtcedula.Text = NumeroDocumento;
                        txtnombre.Text = dtTable.Rows[0]["Nombre"].ToString();
                        txtedad.Text = dtTable.Rows[0]["edad"].ToString();
                        txtdia.Value = dtTable.Rows[0]["dia"].ToString();
                        txtmes.Value = dtTable.Rows[0]["mes"].ToString();
                        txtanio.Text = dtTable.Rows[0]["anio"].ToString();
                        int numDepartamento1 = Convert.ToInt32(dtTable.Rows[0]["departamento1"]);

                      
                        if (numDepartamento1 < 10)
                        {
                            try
                            {
                                ddlDepartamento.SelectedValue = "0" + numDepartamento1.ToString();
                            }
                            catch
                            {
                                ddlDepartamento.SelectedValue = "Seleccione...";
                            }
                        }
                        else
                        {
                            try
                            {
                                ddlDepartamento.SelectedValue = numDepartamento1.ToString();
                            }
                            catch
                            {
                                ddlDepartamento.SelectedValue = "Seleccione...";
                            }
                        }
                       // ddlDepartamento.SelectedValue = blPaicma.myDataSet.Tables["resultadoEncuestaVictima"].Rows[0]["departamento1"].ToString();
                        fntCargarMunicipio();
                        //bool respuesta = blPaicma.fntConsultaVictimaPorId("resultadoEncuestaVictima", "car_Encuesta", "cedula", NumeroDocumento, "Nombre");
                        int numMunicipio1 = Convert.ToInt32(dtTable.Rows[0]["municipio1"]);
                        if (numMunicipio1 < 10)
                        {

                            try
                            {
                                ddlMunicipio.SelectedValue = numDepartamento1.ToString() + "0" + numMunicipio1.ToString();
                            }
                            catch
                            {
                                ddlMunicipio.SelectedValue = "Seleccione...";
                            }

                          //  ddlMunicipio.SelectedValue = numDepartamento1.ToString() + numMunicipio1.ToString();
                        }
                        else
                        {

                            try
                            {
                                ddlMunicipio.SelectedValue = numDepartamento1.ToString() + numMunicipio1.ToString();
                            }
                            catch
                            {
                                ddlMunicipio.SelectedValue = "Seleccione...";
                            }

                            //ddlMunicipio.SelectedValue = numDepartamento1.ToString() + numMunicipio1.ToString();
                        }
                        //ddlMunicipio.SelectedValue = blPaicma.myDataSet.Tables["resultadoEncuestaVictima"].Rows[0]["municipio1"].ToString();
                        txtvereda.Text = dtTable.Rows[0]["vereda"].ToString();
                        txtcorregimiento.Text = dtTable.Rows[0]["corregimiento"].ToString();
                        txtdireccionVivienda.Text = dtTable.Rows[0]["direccionVivienda"].ToString();
                        txtarea.Text = dtTable.Rows[0]["area"].ToString();
                        txtestrato.Text = dtTable.Rows[0]["estrato"].ToString();
                        txttotal_hogares.Text = dtTable.Rows[0]["total_hogares"].ToString();
                        txtnum_personas_hogar.Text = dtTable.Rows[0]["num_personas_hogar"].ToString();
                        txtnum_personas_vivienda.Text = dtTable.Rows[0]["num_personas_vivienda"].ToString();
                        txtemail.Text = dtTable.Rows[0]["email"].ToString();
                        txtresultado_final.Text = dtTable.Rows[0]["resultado_final"].ToString();
                        int numDepartamento2 = Convert.ToInt32(dtTable.Rows[0]["departamento2"]);
                        if (numDepartamento2 < 10)
                        {

                            try
                            {
                                ddlDepartamento2.SelectedValue = "0" + numDepartamento2.ToString();
                            }
                            catch
                            {

                                ddlDepartamento2.SelectedValue = "Seleccione...";
                            }

                        }
                        else
                        {
                            try
                            {
                                ddlDepartamento2.SelectedValue = numDepartamento2.ToString();
                            }
                            catch
                            {
                                ddlDepartamento2.SelectedValue = "Seleccione...";
                            }

                            //ddlDepartamento2.SelectedValue = numDepartamento2.ToString();
                        }
                        //ddlDepartamento2.SelectedValue = dtTable.Rows[0]["departamento2"].ToString();
                        fntCargarMunicipio2();
                       // respuesta = blPaicma.fntConsultaVictimaPorId("resultadoEncuestaVictima", "car_Encuesta", "cedula", NumeroDocumento, "Nombre");
                        int numMunicipio2 = Convert.ToInt32(dtTable.Rows[0]["municipio2"]);
                        if (numMunicipio2 < 10)
                        {

                            try
                            {
                                ddlMunicipio2.SelectedValue = numDepartamento2.ToString()+ "0" + numMunicipio2.ToString();
                                //ddlDepartamento2.SelectedValue = numDepartamento2.ToString();
                            }
                            catch
                            {
                                ddlMunicipio2.SelectedValue = "Seleccione...";
                            }

                            //ddlMunicipio2.SelectedValue = "0" + numMunicipio2.ToString();
                        }
                        else
                        {
                            try
                            {
                                ddlMunicipio2.SelectedValue = numDepartamento2.ToString() + numMunicipio2.ToString();
                                //ddlMunicipio2.SelectedValue = "0" + numMunicipio2.ToString();
                                //ddlDepartamento2.SelectedValue = numDepartamento2.ToString();
                            }
                            catch
                            {
                                ddlMunicipio2.SelectedValue = "Seleccione...";
                            }
                            //ddlMunicipio2.SelectedValue = numMunicipio2.ToString();
                        }
                        //ddlMunicipio2.SelectedValue = blPaicma.myDataSet.Tables["resultadoEncuestaVictima"].Rows[0]["municipio2"].ToString();
                        ddlPaises.SelectedValue = dtTable.Rows[0]["pais"].ToString();
                        ddlSexo.Value = dtTable.Rows[0]["sexo"].ToString();
                        txtnom_enc.Text = dtTable.Rows[0]["nom_enc"].ToString();
                        txtautorizacion.Text = dtTable.Rows[0]["autorizacion"].ToString();
                        //hndidValorXVariable.Value = blPaicma.myDataSet.Tables["resultadoEncuestaVictima"].Rows[0][0].ToString();
                        //txtidentificadorValor.Text = blPaicma.myDataSet.Tables["resultadoEncuestaVictima"].Rows[0][2].ToString();
                        //txttextoValor.Text = blPaicma.myDataSet.Tables["resultadoEncuestaVictima"].Rows[0][3].ToString();

                        pnlEncuestaNuevo.Visible = true;
                        lblError.Text = "";
                        lista.Visible = false;
                    }
                    else
                    {
                        if (blPaicma.fntConsultaVictimaPorId("resultadoVictima", "Victima", "ID_IMSMAVictima", IdVictima, "Nombres"))
                        {
                            if (blPaicma.myDataSet.Tables["resultadoVictima"].Rows.Count > 0)
                            {
                                txtcedula.Text = blPaicma.myDataSet.Tables["resultadoVictima"].Rows[0]["NumeroIdentificacion"].ToString();
                                txtnombre.Text = blPaicma.myDataSet.Tables["resultadoVictima"].Rows[0]["Nombres"].ToString() + " " + blPaicma.myDataSet.Tables["resultadoVictima"].Rows[0]["Apellidos"].ToString();
                                pnlEncuestaNuevo.Visible = true;
                                lblError.Text = "";
                                lista.Visible = false;
                            }

                        }
                    }
                }
                else
                {
                    if (blPaicma.fntConsultaVictimaPorId("resultadoVictima", "Victima", "ID_IMSMAVictima", IdVictima, "Nombres"))
                    {
                        if (blPaicma.myDataSet.Tables["resultadoVictima"].Rows.Count > 0)
                        {
                            txtcedula.Text = blPaicma.myDataSet.Tables["resultadoVictima"].Rows[0]["NumeroIdentificacion"].ToString();
                            txtnombre.Text = blPaicma.myDataSet.Tables["resultadoVictima"].Rows[0]["Nombres"].ToString() + " " + blPaicma.myDataSet.Tables["resultadoVictima"].Rows[0]["Apellidos"].ToString();
                            pnlEncuestaNuevo.Visible = true;
                            lblError.Text = "";
                            lista.Visible = false;
                        }
                    }
                }
            }


        }
    }


    protected void gridVictimas_PreRender(object sender, EventArgs e)
    {
        if (gridVictimas.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gridVictimas.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gridVictimas.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gridVictimas.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }

    protected void btnVolverLista_Click(object sender, EventArgs e)
    {
        pnlEncuestaNuevo.Visible = false;
        lista.Visible = true;
    }
}