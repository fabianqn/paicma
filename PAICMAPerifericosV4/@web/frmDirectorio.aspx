﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmDirectorio.aspx.cs" Inherits="_web_frmDirectorio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <asp:Panel ID="pnlTitulo" runat="server" Width="90%" >
        <div align="left">
            <asp:Label ID="lblTitulo" runat="server" Text="DIRECTORIO DE CONTACTOS"></asp:Label>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlEliminacion" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblEliminacion" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSi" runat="server"  Text="SI" onclick="btnSi_Click" />
                        <asp:Button ID="btnNo" runat="server"  Text="NO" onclick="btnNo_Click" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Aceptar" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlMenuDir" runat="server">
        <div class="Botones">
            <asp:ImageButton ID="imgbBuscar" runat="server" 
                ImageUrl="~/Images/BuscarAccion.png" onclick="imgbBuscar_Click" 
                ToolTip="Buscar" />
            <asp:ImageButton ID="imgbNuevo" runat="server" ImageUrl="~/Images/Nuevo.png" 
                onclick="imgbNuevo_Click" ToolTip="Nuevo" />
            <asp:ImageButton ID="imgbEditar" runat="server" 
                ImageUrl="~/Images/Editar.png" onclick="imgbEditar_Click" 
                Visible="False" ToolTip="Editar" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAccionesDir" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server" 
                ImageUrl="~/Images/buscar.png" onclick="imgbEncontrar_Click" 
                ToolTip="Ejecutar Busqueda" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png" 
                onclick="imgbGravar_Click" ToolTip="Guardar" />
            <asp:ImageButton ID="imgbCancelar" runat="server" 
                ImageUrl="~/Images/cancelar.png" onclick="imgbCancelar_Click" 
                ToolTip="Cancelar" />
            <asp:ImageButton ID="imgbNuevoTelefono" runat="server" 
                ImageUrl="~/Images/NuevoCampo.png" ToolTip="Nuevo Teléfono" 
                Visible="False" onclick="imgbNuevoTelefono_Click" />
            <asp:ImageButton ID="imgbNuevoEmail" runat="server" 
                ImageUrl="~/Images/email.png" onclick="imgbNuevoEmail_Click" 
                ToolTip="Nuevo Correo Electrónico" Visible="False" />
            <asp:ImageButton ID="imgbEliminar" runat="server" 
                ImageUrl="~/Images/eliminar.png" onclick="imgbEliminar_Click" 
                ToolTip="Eliminar" />
        </div>
    </asp:Panel>

        <asp:Panel ID="pnlDirNuevo" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    
                    <asp:Label ID="lblCategoria" runat="server" Text="Categoria"></asp:Label>
                    
                </td>
                <td>
                    
                    <asp:DropDownList ID="ddlCategoria" runat="server">
                    </asp:DropDownList>
                    
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblVocativo" runat="server" Text="Vocativo"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlVocativo" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    
                    <asp:Label ID="lblCargo" runat="server" Text="Cargo"></asp:Label>
                    
                </td>
                <td>
                 
                    <asp:DropDownList ID="ddlCargo" runat="server">
                    </asp:DropDownList>
                 
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTipoIdentificacion" runat="server" Text="Tipo Identificación"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="ddlTipoIdentificacion" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblNoIdentificacion" runat="server" Text="Número Identificacion"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtNoIdentificacion" runat="server" MaxLength="20"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regexFortxtNoIdentificacion" runat="server"
                            ControlToValidate="txtNoIdentificacion" ErrorMessage="*Ingrese Valores Numericos"
                            ForeColor="Red"
                            ValidationExpression="^[0-9]*">
                    </asp:RegularExpressionValidator>


                    <%--<asp:RangeValidator ID="rvNoIdentificacion" runat="server" 
                        ControlToValidate="txtNoIdentificacion" ErrorMessage="Solo valores numéricos" 
                        MaximumValue="99999999999999999999" MinimumValue="0"></asp:RangeValidator>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblNombres" runat="server" Text="Nombre"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNombres" runat="server" MaxLength="50"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblApellido" runat="server" Text="Apellido"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtApellido" runat="server" MaxLength="50"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
                        <tr>
                <td>
                    <asp:Label ID="lblInstitucion" runat="server" Text="Institución"></asp:Label>
                            </td>
                <td colspan="2">
                    <asp:DropDownList ID="ddlInstitucion" runat="server">
                    </asp:DropDownList>
                            </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDepartamento" runat="server" Text="Departamento"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="ddlDepartamento" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlDepartamento_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblMunicipio" runat="server" Text="Municipio"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="ddlMunicipio" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDireccion" runat="server" Text="Dirección"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDireccion" runat="server" MaxLength="100"></asp:TextBox>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTipoDireccion" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTelefono" runat="server" Text="Teléfono"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txttelefono" runat="server" MaxLength="10"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regexFortxttelefono" runat="server"
                            ControlToValidate="txttelefono" ErrorMessage="*Ingrese Valores Numericos"
                            ForeColor="Red"
                            ValidationExpression="^[0-9]*">
                    </asp:RegularExpressionValidator>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTipoTelefono" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCelular" runat="server" Text="Celular"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCelular" runat="server" MaxLength="10"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regexFortxtCelular" runat="server"
                            ControlToValidate="txtCelular" ErrorMessage="*Ingrese Valores Numericos"
                            ForeColor="Red"
                            ValidationExpression="^[0-9]*">
                    </asp:RegularExpressionValidator>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTipoCelular" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblEmail" runat="server" Text="Correo Electrónico"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="50"></asp:TextBox>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTipoEmail" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:RegularExpressionValidator ID="REVEmail" runat="server" 
                        ControlToValidate="txtEmail" 
                        ErrorMessage="Formato inválido &quot;correo@com.co&quot;" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                        Width="50%"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
        </table>

        <table class="contacto" runat="server" id= "tblTelefonos" >
            <tr>
                <td>
                    <span>
                    <asp:Label ID="lblTituloTelefono" runat="server" style="text-align: left" 
                        Text="Teléfonos"></asp:Label>
                    </span>
                    </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvTelefonos" runat="server" AllowPaging="True" 
                        AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" CssClass="mGrid" 
                        onpageindexchanging="gvTelefonos_PageIndexChanging" 
                        onrowcommand="gvTelefonos_RowCommand" PagerStyle-CssClass="pgr" PageSize="5" 
                        Visible="False" width="90%">
                        <AlternatingRowStyle CssClass="alt" />
                        <Columns>
                            <asp:BoundField DataField="tel_Id" HeaderText="tel_Id" />
                            <asp:BoundField DataField="numTelefono" HeaderText="Telefono" />
                            <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                            <asp:BoundField DataField="DescProcedencia" HeaderText="Procedencia" />
                            <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                                ImageUrl="~/Images/seleccionar.png" />
                        </Columns>
                        <PagerStyle CssClass="pgr" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblErrorGvTelefonos" runat="server"></asp:Label>
                </td>
            </tr>
        </table>


        <table class="contacto" runat="server" id= "tblCorreosElectronicos" >
            <tr>
                <td>
                    <span>
                    <asp:Label ID="lblCorreoElectronico" runat="server" style="text-align: left" 
                        Text="Correo Electrónico"></asp:Label>
                    </span>
                    </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvCorreoElectronico" runat="server" AllowPaging="True" 
                        AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" CssClass="mGrid" 
                        onpageindexchanging="gvCorreoElectronico_PageIndexChanging" 
                        onrowcommand="gvCorreoElectronico_RowCommand" PagerStyle-CssClass="pgr" PageSize="5" 
                        Visible="False" width="90%">
                        <AlternatingRowStyle CssClass="alt" />
                        <Columns>
                            <asp:BoundField DataField="corele_Id" HeaderText="corele_Id" />
                            <asp:BoundField DataField="direccionCorreoElectronico" 
                                HeaderText="Correo Electrónico" />
                            <asp:BoundField DataField="tip_Descripcion" HeaderText="Procedencia" />
                            <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                                ImageUrl="~/Images/seleccionar.png" />
                        </Columns>
                        <PagerStyle CssClass="pgr" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblErrorGvCorreoElectronico" runat="server"></asp:Label>
                </td>
            </tr>
        </table>

        </div>
    </asp:Panel>


    <asp:Panel ID="pnlTelefonos" runat="server" Visible="False">
            <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblTipo" runat="server" Text="Tipo"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTipo" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LblNumeroTelefono" runat="server" Text="Número Teléfonico"></asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtNumeroTelefono" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RangeValidator ID="rvNumeroTelefono" runat="server" 
                        ControlToValidate="txtNumeroTelefono" ErrorMessage="Solo valores numéricos" 
                        MaximumValue="999999999999999" MinimumValue="0"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblProcedencia" runat="server" Text="Procedencia"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlProcedencia" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblErrorTelefono" runat="server"></asp:Label>
                </td>
            </tr>
        </table>

        </div>
    </asp:Panel>

    <asp:Panel ID="pnlCorreoElectronico" runat="server" Visible="False">
            <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblTituloEmail" runat="server" Text="Correo Electrónico"></asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtCorreoElectronico" runat="server" MaxLength="50"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="REVEmail0" runat="server" 
                        ControlToValidate="txtCorreoElectronico" 
                        ErrorMessage="Formato inválido &quot;correo@com.co&quot;" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Width="50%"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblProcedenciaEmail" runat="server" Text="Procedencia"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlProcedenciaEmail" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblErroEmail" runat="server"></asp:Label>
                </td>
            </tr>
        </table>

        </div>
    </asp:Panel>



      <asp:Panel ID="PnlDirectorioGrilla" runat="server">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvDirectorio" runat="server" AutoGenerateColumns="False" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
                    onrowcommand="gvDirectorio_RowCommand" 
                    onpageindexchanging="gvDirectorio_PageIndexChanging" PageSize="20">
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="per_Id" HeaderText="per_Id" />
                        <asp:BoundField DataField="cat_descripcion" 
                            HeaderText="Categoria" />
                        <asp:BoundField DataField="cargo" 
                            HeaderText="Cargo" />
                        <asp:BoundField DataField="voc_Descripcion" HeaderText="Vocativo" />
                        <asp:BoundField DataField="Persona" 
                            HeaderText="Contacto" />
                        <asp:BoundField DataField="ins_Nombre" 
                            HeaderText="Institución" />
                        <asp:BoundField DataField="nomDepartamento" 
                            HeaderText="Departamento" />
                        <asp:BoundField DataField="nomMunicipio" 
                            HeaderText="Municipio" />
                        <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                            ImageUrl="~/Images/seleccionar.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorGv" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>


</asp:Content>

