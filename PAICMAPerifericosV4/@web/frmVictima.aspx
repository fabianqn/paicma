﻿<%@ page title="" language="C#" masterpagefile="~/Plantillas/sisPAICMA.master" autoeventwireup="true" CodeFile="frmVictima.aspx.cs" inherits="_web_frmVictima" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <asp:Panel ID="pnlTitulo" runat="server" Width="90%" >
        <div>
            <asp:Label ID="lblTitulo" runat="server" Text="Seguimiento a Victimas"></asp:Label>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlEliminacion" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblEliminacion" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSi" runat="server"  Text="SI" />
                        <asp:Button ID="btnNo" runat="server"  Text="NO" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" Text="Aceptar" OnClick="btnOk_Click" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlMenuVictima" runat="server">
        <div class="Botones">
            <asp:ImageButton ID="imgbBuscar" runat="server"  ImageUrl="~/Images/BuscarAccion.png" ToolTip="Buscar" />
            <asp:ImageButton ID="imgbNuevo" runat="server" ImageUrl="~/Images/Nuevo.png" ToolTip="Nuevo" OnClick="imgbNuevo_Click" />
            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png" Visible="False" ToolTip="Editar" OnClick="imgbEditar_Click" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAccionesVictima" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server" ImageUrl="~/Images/buscar.png" ToolTip="Ejecutar Busqueda" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png" ToolTip="Guardar" OnClick="imgbGravar_Click" />
            <asp:ImageButton ID="imgbNuevoDatosAccidente" runat="server" ImageUrl="~/Images/NuevoCampo.png" OnClick="imgbNuevoDatosAccidente_Click" ToolTip="Nuevo Datos Accidente" Visible="False" />
            <asp:ImageButton ID="imgbCancelar" runat="server" ImageUrl="~/Images/cancelar.png" ToolTip="Cancelar" OnClick="imgbCancelar_Click" />
            <asp:ImageButton ID="imgbEliminar" runat="server" ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar" />
        </div>
    </asp:Panel>

    <asp:Panel ID="PnlVictimaGrilla" runat="server" Visible="False">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvVictimas" runat="server" AutoGenerateColumns="False" width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" AllowPaging="True" PageSize="20" OnRowCommand="gvVictimas_RowCommand">
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="vic_Id" HeaderText="vic_Id" />
                        <asp:BoundField DataField="tip_Descripcion" HeaderText="Tipo Documento" />
                        <asp:BoundField DataField="identificacionNacionalPersona" HeaderText="Número Documento " />
                        <asp:BoundField DataField="nombrePersona" HeaderText="Nombre" />
                        <asp:BoundField DataField="apellidoPersona"                             HeaderText="Apellido" />
                        <asp:ButtonField ButtonType="Image" CommandName="Seleccion" ImageUrl="~/Images/seleccionar.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorGv" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlVictimaNuevo" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblTipoIdentificacion" runat="server" Text="Tipo Identificación"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTipoIdentificacion" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblNoIdentificacion" runat="server" Text="Número Identificacion"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNoIdentificacion" runat="server" MaxLength="20"></asp:TextBox>
                    <asp:RangeValidator ID="rvNoIdentificacion" runat="server" 
                        ControlToValidate="txtNoIdentificacion" ErrorMessage="Solo valores numéricos" 
                        MaximumValue="99999999999999999999" MinimumValue="0"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblNombres" runat="server" Text="Nombre"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNombres" runat="server" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblApellido" runat="server" Text="Apellido"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtApellido" runat="server" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTelefono" runat="server" Text="Teléfono"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txttelefono" runat="server" MaxLength="10"></asp:TextBox>
                    <asp:RangeValidator ID="rvTelefono" runat="server" ControlToValidate="txttelefono" ErrorMessage="Solo valores numéricos" MaximumValue="9999999999" MinimumValue="0"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDireccion" runat="server" Text="Dirección"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDireccion" runat="server" MaxLength="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFEchaNacimiento" runat="server" Text="Fecha Nacimiento"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFechaNacimiento" runat="server" MaxLength="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblEdad" runat="server" Text="Edad"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtEdad" runat="server" MaxLength="10"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblGenero" runat="server" Text="Genero"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlGenero" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblEtnia" runat="server" Text="Etnia"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlEtnia" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblNivelEducacion" runat="server" Text="Nivel de Educación"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlNivelEducacion" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
        </table>

        <table class="contacto" runat="server" id= "tblDatosAccidente" >
            <tr>
                <td>
                    <span>
                    <asp:Label ID="lblTituloDatosAccidente" runat="server" style="text-align: left" Text="Datos Accidente"></asp:Label>
                    </span>
                    </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvDatosAccidente" runat="server" AllowPaging="True" 
                        AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" CssClass="mGrid"  PagerStyle-CssClass="pgr" PageSize="5" Visible="False" OnRowCommand="gvDatosAccidente_RowCommand" >
                        <AlternatingRowStyle CssClass="alt" />
                        <Columns>
                            <asp:BoundField DataField="monseg_Id" HeaderText="monseg_Id" />
                            <asp:BoundField DataField="vereda" HeaderText="Vereda" />
                            <asp:BoundField DataField="arepob_Nombre" HeaderText="Area Poblacional" />
                            <asp:BoundField DataField="monseg_SitioAccidente" HeaderText="Sitio Accidente" />
                            <asp:BoundField DataField="monseg_Condicion" HeaderText="Condición" />
                            <asp:BoundField DataField="est_Descripcion" HeaderText="Estado" />
                            <asp:BoundField HeaderText="Accitivad en el momento de accidente" DataField="monseg_ActividadAccidente" />
                            <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                                ImageUrl="~/Images/seleccionar.png" />
                        </Columns>
                        <PagerStyle CssClass="pgr" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblErrorGvDatosAccidente" runat="server"></asp:Label>
                </td>
            </tr>
        </table>

        </div>
    </asp:Panel>


        <asp:Panel ID="pnlDetalleDatosVictima" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="lblVereda" runat="server" Text="Vereda"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlVereda" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="lblAreaPoblacional" runat="server" Text="Área Poblacional"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlAreaPoblacional" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="lblSitioAccidente" runat="server" Text="Sitio del Accidente"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtSitioAccidente" runat="server" MaxLength="50" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="lblCondicion" runat="server" Text="Condicion"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCondicion" runat="server" MaxLength="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlEstado" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="lblActividadAccidente" runat="server" Text="Actividad Accidente"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtActividadAccidente" runat="server" MaxLength="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1" align="center">
                    <asp:CheckBoxList ID="chkVictima1" runat="server" style="text-align: right">
                        <asp:ListItem Value="0">Amputación inicial</asp:ListItem>
                        <asp:ListItem Value="1">Amputación Final</asp:ListItem>
                        <asp:ListItem Value="3">Herida</asp:ListItem>
                        <asp:ListItem Value="4">Esquirla</asp:ListItem>
                        <asp:ListItem Value="5">Quemadura</asp:ListItem>
                    </asp:CheckBoxList>
                </td>
                <td align="left">
                    <asp:CheckBoxList ID="chkVictima2" runat="server">
                        <asp:ListItem Value="5">Fractura</asp:ListItem>
                        <asp:ListItem Value="6">Infección</asp:ListItem>
                        <asp:ListItem Value="7">Afectación Visual</asp:ListItem>
                        <asp:ListItem Value="8">Afectación Auditiva</asp:ListItem>
                        <asp:ListItem Value="9">Otro</asp:ListItem>
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="lblDiagnosticoMedico" runat="server" Text="Diagnostico Medico"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDiagnosticoNuevo" runat="server" MaxLength="10" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="lblDescHechos" runat="server" Text="Descripción Hechos"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDescripcionHechos" runat="server" MaxLength="10" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="lblSeguimiento" runat="server" Text="Seguimiento"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtSeguimiento" runat="server" MaxLength="10" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label11" runat="server"></asp:Label>
                </td>
            </tr>
        </table>

        </div>
    </asp:Panel>

</asp:Content>

