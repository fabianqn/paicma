﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

public partial class _web_frmERM_AtencionSalud : System.Web.UI.Page
{
    //private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        fntCargarArchivo();
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
       // blPaicma = null;
    }

    public void fntCargarArchivo()
    {
        string strNomSP = "SP_ERM4";
        string strTablaTemporal = string.Empty;
        int intCantidad = 0;
        string strRutaServidor = string.Empty;

        ///inicio 
        int intCantidadCampos = 0;
        string strCampos = string.Empty;
        int intCantidadXls = 0;
        string strCamposExcel = string.Empty;
        int intMaxId = 0;
        string strNomSPReiniciar = string.Empty;
        ///fin 


        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            strTablaTemporal = "tmp_erm4";
            blPaicma.fntEliminarTablaTemporal_bol(strTablaTemporal);

            strRutaServidor = System.Configuration.ConfigurationManager.AppSettings["RutaServerDB"];
            strRutaServidor = strRutaServidor + Session["strRutaCompleta"].ToString();

             try
            {

                if (blPaicma.fntConsultaCampoTablas("strDsCamposTablas", strTablaTemporal))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        intCantidadCampos = blPaicma.myDataSet.Tables[0].Rows.Count;
                        for (int i = 0; i < intCantidadCampos; i++)
                        {
                            if (i != 0)
                            {
                                if ((i + 1) != intCantidadCampos)
                                {
                                    strCampos = strCampos + blPaicma.myDataSet.Tables[0].Rows[i][1].ToString() + " ,";
                                }
                                else
                                {
                                    strCampos = strCampos + blPaicma.myDataSet.Tables[0].Rows[i][1].ToString();
                                }

                            }
                        }
                    }
                }

                intCantidadCampos = intCantidadCampos - 1;
                string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strRutaServidor + ";Extended Properties=Excel 12.0;";
                OleDbConnection conn = new OleDbConnection(strConn);
                conn.Open();
                string strExcel = "";
                OleDbDataAdapter myCommand = null;
                DataSet ds = null;
                strExcel = "select * from [Hoja1$]";
                myCommand = new OleDbDataAdapter(strExcel, strConn);
                ds = new DataSet();
                myCommand.Fill(ds, "table1");
                intCantidadXls = ds.Tables[0].Rows.Count;

                conn.Close();
                for (int i = 0; i < intCantidadXls; i++)
                {
                    strCamposExcel = string.Empty;
                    for (int j = 0; j < intCantidadCampos; j++)
                    {
                        if ((j + 1) != intCantidadCampos)
                        {
                            strCamposExcel = strCamposExcel + "'" + ds.Tables[0].Rows[i][j].ToString() + "' ,";
                        }
                        else
                        {
                            strCamposExcel = strCamposExcel + "'" + ds.Tables[0].Rows[i][j].ToString() + "'"; ;
                        }

                    }

                    if (blPaicma.fntConsultaMaxIdTablaDinamica_bol("strDsId", strTablaTemporal))
                    {
                        intMaxId = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                    }
                    if (intMaxId == 1)
                    {
                        strNomSPReiniciar = "SP_ReiniciarIndice ";
                        blPaicma.ReiniciaIndice(strNomSPReiniciar, strTablaTemporal);


                        // lanzo sp_reiniciarIndice
                    }
                    string sw = string.Empty;
                    if (blPaicma.fntIngresarTablaDinamica2_bol(strCampos, strTablaTemporal, strCamposExcel, intMaxId))
                    {
                        sw = "OK";
                    }
                    else
                    {
                        Session["Resultado"] = "NO";
                        Session["ESTRUCTURA"] = "NO";
                        Session["PELIGRO"] = "NA";
                        break;
                    }

                }


                Session["ESTRUCTURA"] = null;
                if (blPaicma.fntValidarSeguridadEMR4("strDsErm4"))
                {
                    intCantidad = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                    if (intCantidad > 0)
                    {
                        ///el archivo no es seguro, tiene palabras reservadas del motor;
                        Session["Resultado"] = "NO";
                        Session["PELIGRO"] = "OK";
                    }
                    else
                    {
                        ///el archivo es seguro, no tiene palabras reservadas del motor;
                        blPaicma.CargarArchivo(strNomSP);
                        Session["Resultado"] = "OK";
                    }
                }
            
             }
             catch (Exception ex)
             {
                 string strError = ex.ToString();
                 Session["Resultado"] = "NO";
                 Session["ESTRUCTURA"] = "NO";
                 Session["PELIGRO"] = "NA";
                 blPaicma.Termina();
             }

            blPaicma.Termina();
        }
        blPaicma = null;
        System.IO.File.Delete(Session["strRutaCompleta"].ToString());
        Response.Redirect("frmCERM.aspx");
    }



}