﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

public partial class _web_ejemplo : System.Web.UI.Page
{
    //private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        fntCargarArchivo();
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        //blPaicma = null;
    }

    public void fntCargarArchivo()
    {
        string strCriterio = "CRITERIOS A EVALUAR";
        int intIndice = 0;
        int intSw = 0;
        int intI = 0;
        int intCantidadRegistros = 0;
        string strCantidadRegistrosAux = string.Empty;
        int intContador = 0;
        string strCriterios = string.Empty;
        string strCriterioAux = string.Empty;
        string strPregunta = string.Empty;
        int intIndiceCategoria = 0;
        int validador = 0;
        string strIdTabulacionEncuesta = string.Empty;
        string strObservacion = string.Empty;
        string strt = string.Empty;
        string strTablaTemporal = string.Empty;
        int intCantidad = 0;
        string strRutaServidor = string.Empty;

        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            strTablaTemporal = "tmp_AG2";
            blPaicma.fntEliminarTablaTemporal_bol(strTablaTemporal);

            strRutaServidor = System.Configuration.ConfigurationManager.AppSettings["RutaServerDB"];
            strRutaServidor = strRutaServidor + Session["strRutaCompleta"].ToString();

            if (fntInsertarArchivo(strRutaServidor))
            {


                Session["ESTRUCTURA"] = null;
                if (blPaicma.fntValidarSeguridadAG2("strDsAG2"))
                {
                    intCantidad = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                    if (intCantidad > 0)
                    {
                        ///el archivo no es seguro, tiene palabras reservadas del motor;
                        Session["Resultado"] = "NO";
                        Session["PELIGRO"] = "OK";
                    }
                    else
                    {
                        ///el archivo es seguro, no tiene palabras reservadas del motor;
                        Session["PELIGRO"] = null;
                        string strNomSP = "SP_ag2";
                        strIdTabulacionEncuesta = blPaicma.CargarArchivo(strNomSP);

                        if (blPaicma.fntConsultaCriteriosEvaluar_bol("strDsCriterios"))
                        {
                            if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                            {
                                while (intSw == 0)
                                {
                                    strCriterio = blPaicma.myDataSet.Tables[0].Rows[intI]["ag1"].ToString();
                                    if (strCriterio == "CRITERIOS A EVALUAR")
                                    {
                                        intSw = 1;
                                        intIndice = intI + 1;
                                    }
                                    else
                                    {
                                        intI = intI + 1;
                                    }
                                }

                                intSw = 0;
                                intI = 3;
                                while (intSw == 0)
                                {
                                    strCantidadRegistrosAux = blPaicma.myDataSet.Tables[0].Rows[intIndice][intI].ToString();
                                    if (strCantidadRegistrosAux == "")
                                    {
                                        intSw = 1;
                                    }
                                    else
                                    {
                                        intCantidadRegistros = Convert.ToInt32(strCantidadRegistrosAux);
                                        intI = intI + 1;
                                    }
                                }

                                intIndice = intIndice + 1;
                                intSw = 0;
                                intI = 2;

                                while (intSw == 0)
                                {
                                    strCriterioAux = blPaicma.myDataSet.Tables[0].Rows[intIndice][intI].ToString();
                                    strCriterios = strCriterioAux;
                                    intI = intI + 1;

                                    int intCantidadPreguntas = blPaicma.myDataSet.Tables[0].Rows.Count;
                                    int intI1 = 2;
                                    int intIndice1 = intIndice;
                                    for (int i = intIndice1; i < intCantidadPreguntas; i++)
                                    {
                                        int intIndice2 = intIndice1;

                                        strPregunta = blPaicma.myDataSet.Tables[0].Rows[intIndice1][intI1].ToString();

                                        if (strPregunta == "OBSERVACIONES")
                                        {
                                            break;
                                        }
                                        intIndice1 = intIndice1 + 1;


                                        int intI2 = 3;
                                        intContador = 0;
                                        string[] strCalificacion = new string[intCantidadRegistros];
                                        validador = 0;
                                        for (int j = 1; j <= (intCantidadRegistros); j++)
                                        {
                                            strCalificacion[intContador] = blPaicma.myDataSet.Tables[0].Rows[intIndice2][intI2].ToString();
                                            if (strCalificacion[intContador].ToString() == "")
                                            {
                                                ///SE CONSULTA PRIMERO SI LA CATEGORIA EXISTE
                                                if (blPaicma.fntConsultaCategoria_bol("strDsCategoria", strPregunta))
                                                {
                                                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                                                    {
                                                        intIndiceCategoria = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0]["cat_Id"].ToString());
                                                        blPaicma.fntConsultaCriteriosEvaluar_bol("strDsCriterios");
                                                        validador = 1;
                                                    }  
                                                }

                                                if (validador == 0)
                                                {
                                                    if (blPaicma.fntIngresarCriterio_bol(strPregunta))
                                                    {
                                                        j = (intCantidadRegistros + 1);
                                                        if (blPaicma.fntConsultaCategoria_bol("strDsCategoria", strPregunta))
                                                        {
                                                            intIndiceCategoria = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0]["cat_Id"].ToString());
                                                            blPaicma.fntConsultaCriteriosEvaluar_bol("strDsCriterios");
                                                            validador = 1;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                intContador = intContador + 1;
                                                intI2 = intI2 + 1;
                                                validador = 0;
                                            }
                                        }

                                        if (validador == 0)
                                        {
                                            try
                                            {
                                                for (int k = 0; k < (intCantidadRegistros); k++)
                                                {
                                                    if (blPaicma.fntIngresarCriterioEvaluar_bol(Convert.ToInt32(strIdTabulacionEncuesta), intIndiceCategoria, (k + 1), strPregunta, strCalificacion[k].ToString()))
                                                    {
                                                        strt = "ok";
                                                        Session["Resultado"] = "OK";
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Session["Resultado"] = "NO";
                                                Session["ESTRUCTURA"] = "NO";
                                                Session["PELIGRO"] = "NA";
                                            }
                                        }

                                    }

                                    intIndice1 = intIndice1 + 2;
                                    for (int i = intIndice1; i < intCantidadPreguntas; i++)
                                    {
                                        strObservacion = blPaicma.myDataSet.Tables[0].Rows[i][intI1].ToString();
                                        if (blPaicma.fntIngresarObservacionCriterio_bol(Convert.ToInt32(strIdTabulacionEncuesta), strObservacion))
                                        {
                                            strt = "ok";
                                            Session["Resultado"] = "OK";
                                        }
                                    }
                                    intSw = 1;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                Session["Resultado"] = "NO";
                Session["ESTRUCTURA"] = "NO";
                Session["PELIGRO"] = "NA";
            }
            blPaicma.Termina();
        }
        blPaicma = null;
        System.IO.File.Delete(Session["strRutaCompleta"].ToString());
        Response.Redirect("frmCApoyoGestion.aspx");
    }

    public Boolean fntInsertarArchivo(string strRutaServidor)
    {
        Boolean bolValor = false;
        string strTablaTemporal = string.Empty;

        ///inicio 
        int intCantidadCampos = 0;
        string strCampos = string.Empty;
        int intCantidadXls = 0;
        string strCamposExcel = string.Empty;
        int intMaxId = 0;
        string strNomSPReiniciar = string.Empty;
        ///fin 
        
        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            strTablaTemporal = "tmp_AG2";
            try
            {
                if (blPaicma.fntConsultaCampoTablas("strDsCamposTablas", strTablaTemporal))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        intCantidadCampos = blPaicma.myDataSet.Tables[0].Rows.Count;
                        for (int i = 0; i < intCantidadCampos; i++)
                        {
                            if (i != 0)
                            {
                                if ((i + 1) != intCantidadCampos)
                                {
                                    strCampos = strCampos + blPaicma.myDataSet.Tables[0].Rows[i][1].ToString() + " ,";
                                }
                                else
                                {
                                    strCampos = strCampos + blPaicma.myDataSet.Tables[0].Rows[i][1].ToString();
                                }

                            }
                        }
                    }
                }

                intCantidadCampos = intCantidadCampos - 1;
                string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strRutaServidor + ";Extended Properties=Excel 12.0;";
                OleDbConnection conn = new OleDbConnection(strConn);
                conn.Open();
                string strExcel = "";
                OleDbDataAdapter myCommand = null;
                DataSet ds = null;
                strExcel = "select * from [Hoja1$]";
                myCommand = new OleDbDataAdapter(strExcel, strConn);
                ds = new DataSet();
                myCommand.Fill(ds, "table1");
                intCantidadXls = ds.Tables[0].Rows.Count;
                conn.Close();


                for (int i = 0; i < intCantidadXls; i++)
                {
                    strCamposExcel = string.Empty;
                    for (int j = 0; j < intCantidadCampos; j++)
                    {
                        if ((j + 1) != intCantidadCampos)
                        {
                            strCamposExcel = strCamposExcel + "'" + ds.Tables[0].Rows[i][j].ToString() + "' ,";
                        }
                        else
                        {
                            strCamposExcel = strCamposExcel + "'" + ds.Tables[0].Rows[i][j].ToString() + "'"; ;
                        }

                    }

                    if (blPaicma.fntConsultaMaxIdTablaDinamica_bol("strDsId", strTablaTemporal))
                    {
                        intMaxId = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                    }
                    if (intMaxId == 1)
                    {
                        strNomSPReiniciar = "SP_ReiniciarIndice ";
                        blPaicma.ReiniciaIndice(strNomSPReiniciar, strTablaTemporal);


                        // lanzo sp_reiniciarIndice
                    }
                    string sw = string.Empty;
                    if (blPaicma.fntIngresarTablaDinamica2_bol(strCampos, strTablaTemporal, strCamposExcel, intMaxId))
                    {
                        sw = "OK";
                    }
                    else
                    {
                        bolValor = false;
                        break;
                    }

                }        

            }
            catch (Exception ex)
            {
                string strError = ex.ToString();
                Session["Resultado"] = "NO";
                Session["ESTRUCTURA"] = "NO";
                Session["PELIGRO"] = "NA";
                blPaicma.Termina();
            }
            bolValor = true;
            blPaicma.Termina();
        }
        blPaicma = null;
        return bolValor;
    }


}