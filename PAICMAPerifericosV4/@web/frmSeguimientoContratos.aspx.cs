﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _web_frmSeguimientoContratos : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                ftnValidarPermisos();
                Session["IdRecursosProyecto"] = string.Empty;
                fntCargarGrillaProyectos(0, 0, 0, 0, string.Empty, string.Empty, 0, string.Empty, 0, 0);
            }
        }
        else
        {
            if (Session["IdRecursosProyecto"].ToString() == string.Empty)
            {
                ftnInactivarControles(true);
            }
            else
            {
                ftnInactivarControles(false);
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }

    public void ftnValidarPermisos()
    {
        //int intIdFormulario = 0;
        string strBuscar = string.Empty;
        string strNuevo = string.Empty;
        string strEditar = string.Empty;
        string strEliminar = string.Empty;

        //obtiene el nombre de la pagina actual
        string[] strRutaPagina = HttpContext.Current.Request.RawUrl.Split('/');
        string strNombrePagina = strRutaPagina[strRutaPagina.GetUpperBound(0)];
        strRutaPagina = strNombrePagina.Split('?');
        strNombrePagina = strRutaPagina[strRutaPagina.GetLowerBound(0)];
        //fin obtiene el nombre de la pagina actual


        if (strNombrePagina != string.Empty)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {

                //intIdFormulario = Convert.ToInt32(Request.QueryString["op"].ToString());
                //Session["intIdFormulario"] = intIdFormulario;
                if (blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(Session["IdUsuario"].ToString()), strNombrePagina))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strBuscar = blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                        strNuevo = blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                        strEditar = blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                        strEliminar = blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();

                        if (strBuscar == "False")
                        {
                            imgbBuscar.Visible = false;
                        }
                        if (strNuevo == "False")
                        {
                            imgbNuevo.Visible = false;
                        }
                        if (strEditar == "False")
                        {
                            imgbEditar.Visible = false;
                            imgbGravar.Visible = false;
                        }
                        if (strEliminar == "False")
                        {
                            imgbEliminar.Visible = false;
                        }


                    }
                }

                blPaicma.Termina();
            }
        }
        else
        {
            Response.Redirect("@dmin/frmLogin.aspx");
        }

    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        if (Session["IdRecursosProyecto"].ToString() == string.Empty)
        {
            string strRuta = "frmSeguimientoContratos.aspx";
            Response.Redirect(strRuta);
            PnlProyectos.Visible = true;
        }
        else
        {
            pnlRespuesta.Visible = false;
            pnlAccionesSegContrato.Visible = true;
            pnlSegContratoNuevo.Visible = true;
            ftnInactivarControles(false);
            Activiades.Visible = true;
            fntCargarActividadRecursoProyecto();
            fntCargarEstadoActividadRecursoProyecto();
            fntCargarGrillaActividades(Convert.ToInt32(Session["IdRecursosProyecto"].ToString()));
            ftnLimpiarCamposActividades();
        }

    }

    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        pnlAccionesSegContrato.Visible = true;
        imgbEncontrar.Visible = false;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        pnlSegContratoNuevo.Visible = true;
        pnlMenuSegContrato.Visible = false;
        PnlProyectos.Visible = false;
        fntCargarObjetos();
        Activiades.Visible = false;
        fntOcultarCampos(true);
        Session["Operacion"] = "Crear";
    }

    public void fntCargarRubroPresupuestal()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaRubroPresupuestal("strDsRubroPresupuestal"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlCodigoPresupuestal.DataMember = "strDsRubroPresupuestal";
                    ddlCodigoPresupuestal.DataSource = blPaicma.myDataSet;
                    ddlCodigoPresupuestal.DataValueField = "rubpre_Id";
                    ddlCodigoPresupuestal.DataTextField = "RubroPresupuestal";
                    ddlCodigoPresupuestal.DataBind();
                    ddlCodigoPresupuestal.Items.Insert(ddlCodigoPresupuestal.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarProyecto()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaProyecto("strDsProyecto"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlProyecto.DataMember = "strDsProyecto";
                    ddlProyecto.DataSource = blPaicma.myDataSet;
                    ddlProyecto.DataValueField = "pro_Id";
                    ddlProyecto.DataTextField = "nomProyecto";
                    ddlProyecto.DataBind();
                    ddlProyecto.Items.Insert(ddlProyecto.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarTipoProyecto()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTipoProyecto("strDsTipoProyecto"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlTipoProyecto.DataMember = "strDsTipoProyecto";
                    ddlTipoProyecto.DataSource = blPaicma.myDataSet;
                    ddlTipoProyecto.DataValueField = "tip_Id";
                    ddlTipoProyecto.DataTextField = "tip_Descripcion";
                    ddlTipoProyecto.DataBind();
                    ddlTipoProyecto.Items.Insert(ddlTipoProyecto.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarActividadRecursoProyecto()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaActividadRecursosProyecto("strDsActividadRecursoProyecto"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlActividad.DataMember = "strDsActividadRecursoProyecto";
                    ddlActividad.DataSource = blPaicma.myDataSet;
                    ddlActividad.DataValueField = "acti_Id";
                    ddlActividad.DataTextField = "actividadOcupacionalPersona";
                    ddlActividad.DataBind();
                    ddlActividad.Items.Insert(ddlActividad.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarEstadoActividadRecursoProyecto()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaEstadoActividadesProyectosRecursos("strDsEstadoActividadRecursoProyecto"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlEstado.DataMember = "strDsEstadoActividadRecursoProyecto";
                    ddlEstado.DataSource = blPaicma.myDataSet;
                    ddlEstado.DataValueField = "est_Id";
                    ddlEstado.DataTextField = "est_Descripcion";
                    ddlEstado.DataBind();
                    ddlEstado.Items.Insert(ddlEstado.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarTipoIdentificacion()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTipoIdentificacion_bol("strDsTipoIdentificacion",string.Empty,1))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlTipoIdentificacion.DataMember = "strDsTipoIdentificacion";
                    ddlTipoIdentificacion.DataSource = blPaicma.myDataSet;
                    ddlTipoIdentificacion.DataValueField = "tip_Id";
                    ddlTipoIdentificacion.DataTextField = "tip_Descripcion";
                    ddlTipoIdentificacion.DataBind();
                    ddlTipoIdentificacion.Items.Insert(ddlTipoIdentificacion.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void ftnInactivarControles(Boolean bolValor)
    {
        txtValorProyecto.Enabled = bolValor;
        ddlCodigoPresupuestal.Enabled = bolValor;
        ddlProyecto.Enabled = bolValor;
        ddlTipoProyecto.Enabled = bolValor;
    }

    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        string strRuta = "frmSeguimientoContratos.aspx";
        Response.Redirect(strRuta);
    }

    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["Operacion"].ToString() == "Crear")
        {

            if (Session["IdRecursosProyecto"].ToString() == string.Empty)
            {
                if (ftnValidarCampos_bol(1))
                {
                    if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                    {

                        if (blPaicma.fntIngresarRecursosProyecto_bol(Convert.ToInt32(ddlCodigoPresupuestal.SelectedValue.ToString()), Convert.ToInt32(ddlTipoProyecto.SelectedValue.ToString()), Convert.ToInt32(ddlProyecto.SelectedValue.ToString()), txtValorProyecto.Text.Trim(), txtContrato.Text.Trim(), txtFechaContrato.Text.Trim(), Convert.ToInt32(Session["intIdContratista"].ToString())))
                        {
                            if (blPaicma.fntConsultaRecursosProyecto("strDsRecursoProyecto", Convert.ToInt32(ddlCodigoPresupuestal.SelectedValue.ToString()), Convert.ToInt32(ddlTipoProyecto.SelectedValue.ToString()), Convert.ToInt32(ddlProyecto.SelectedValue.ToString()), txtValorProyecto.Text.Trim()))
                            {
                                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                                {
                                    Session["IdRecursosProyecto"] = blPaicma.myDataSet.Tables[0].Rows[0][0].ToString();
                                    ftnInactivarControles(false);
                                    ftnInactivarPaneles(false);
                                    pnlRespuesta.Visible = true;
                                    lblRespuesta.Text = "Registro creado satisfactoriamente, ya puede crear las actividades del proyecto.";
                                }
                            }
                        }
                        else
                        {
                            ftnInactivarPaneles(false);
                            pnlRespuesta.Visible = true;
                            lblRespuesta.Text = "Problema al ingresar el registro, por favor comuníquese con el administrador.";
                        }
                        blPaicma.Termina();
                    }
                }
                else
                {
                    lblError.Visible = true;
                }
            }
            else
            {
                if (ftnValidarCampos_bol(2))
                {
                    if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                    {
                        if (blPaicma.fntIngresarActividadesRecursosProyecto_bol(Convert.ToInt32(ddlActividad.SelectedValue.ToString()), Convert.ToInt32(Session["IdRecursosProyecto"].ToString()), txtValorProyectado.Text.Trim(), txtValorContratado.Text.Trim(), txtValorDisponible.Text.Trim(), Convert.ToInt32(ddlEstado.SelectedValue.ToString())))
                        {
                            ftnInactivarControles(false);
                            ftnInactivarPaneles(false);
                            pnlRespuesta.Visible = true;
                            lblRespuesta.Text = "Actividades registrada satisfactoriamente.";
                        }
                        else
                        {
                            ftnInactivarPaneles(false);
                            pnlRespuesta.Visible = true;
                            lblRespuesta.Text = "Problema al ingresar el registro, por favor comuníquese con el administrador.";
                        }
                        blPaicma.Termina();
                    }
                }
                else
                {
                    lblError.Visible = true;
                }
            }
        }
        else if  (Session["Operacion"].ToString() == "Actualizar")
        {
            if (ftnValidarCampos_bol(1))
            {
                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                        if (blPaicma.fntModificarRecursoProyecto_bol(Convert.ToInt32(Session["intIdRecursoProyecto"].ToString()), Convert.ToInt32(ddlCodigoPresupuestal.SelectedValue.ToString()), Convert.ToInt32(ddlProyecto.SelectedValue.ToString()), Convert.ToInt32(ddlTipoProyecto.SelectedValue.ToString()),  txtValorProyecto.Text.Trim(), txtContrato.Text.Trim(), txtFechaContrato.Text.Trim(), Convert.ToInt32(Session["intIdContratista"].ToString())))
                        {
                            ftnInactivarPaneles(false);
                            pnlRespuesta.Visible = true;
                            pnlTraerActividades.Visible = false;
                            lblRespuesta.Text = "Registro actualizado satisfactoriamente";
                        }
                        else
                        {
                            ftnInactivarPaneles(false);
                            pnlRespuesta.Visible = true;
                            pnlTraerActividades.Visible = false;
                            lblRespuesta.Text = "Problema al actualizar el registro, por favor comuníquese con el administrador.";
                        }
                    blPaicma.Termina();
                }
            }
        }
        else if (Session["Operacion"].ToString() == "ActualizarActividades")
        {
            if (ftnValidarCampos_bol(2))
            {
                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    if (blPaicma.fntModificarActividadProyecto_bol(Convert.ToInt32(Session["intIdActividadProyecto"].ToString()), Convert.ToInt32(ddlActividad.SelectedValue.ToString()), txtValorProyectado.Text.Trim(), txtValorContratado.Text.Trim(), txtValorDisponible.Text.Trim(), Convert.ToInt32(ddlEstado.SelectedValue.ToString())))
                    {
                        ftnInactivarPaneles(false);
                        pnlRespuesta.Visible = true;
                        pnlTraerActividades.Visible = false;
                        lblRespuesta.Text = "Registro actualizado satisfactoriamente";
                    }
                    else
                    {
                        ftnInactivarPaneles(false);
                        pnlRespuesta.Visible = true;
                        pnlTraerActividades.Visible = false;
                        lblRespuesta.Text = "Problema al actualizar el registro, por favor comuníquese con el administrador.";
                    }
                    blPaicma.Termina();
                }

            }
        }

        
    }

    public Boolean ftnValidarCampos_bol(int intOp)
    {
        if (intOp == 1)
        {
            if (ddlCodigoPresupuestal.SelectedValue == "Seleccione...")
            {
                lblError.Text = "Debe seleccionar un código presupuestal";
                return false;
            }

            if (ddlProyecto.SelectedValue == "Seleccione...")
            {
                lblError.Text = "Debe seleccionar un proyecto.";
                return false;
            }

            if (ddlTipoProyecto.SelectedValue == "Seleccione...")
            {
                lblError.Text = "Debe seleccionar un tipo de proyecto.";
                return false;
            }

            if (txtContrato.Text.Trim() == string.Empty)
            {
                lblError.Text = "Debe indicar el número del contrato.";
                return false;
            }
            else
            {
                if (ftnValidarSeguridadCampos_bol(txtContrato.Text.Trim()) == false)
                {
                    lblError.Text = "El campo de No CONTRATO tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
                    return false;
                }
            }

            if (txtFechaContrato.Text.Trim() == string.Empty)
            {
                lblError.Text = "Debe indicar la fecha del contrato.";
                return false;
            }

            if (ddlTipoIdentificacion.SelectedValue == "Seleccione...")
            {
                lblError.Text = "Debe seleccionar el tipo de identificación.";
                return false;
            }

            if (txtNumeroIdentificacion.Text.Trim() == string.Empty)
            {
                lblError.Text = "Debe indicar el número de identificación del contratista.";
                return false;
            }
            else
            {
                if (ftnValidarSeguridadCampos_bol(txtNumeroIdentificacion.Text.Trim()) == false)
                {
                    lblError.Text = "El campo de No IDENTIFICACION tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
                    return false;
                }
            }
            

            if (txtContratista.Text.Trim() == string.Empty)
            {
                lblError.Text = "Debe indicar el nombre del contratista.";
                return false;
            }
            else
            {
                if (ftnValidarSeguridadCampos_bol(txtContratista.Text.Trim()) == false)
                {
                    lblError.Text = "El campo de CONTRATISTA tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
                    return false;
                }
            }

            if (txtValorProyecto.Text.Trim() == string.Empty)
            {
                lblError.Text = "Debe indicar el valor del proyecto.";
                return false;
            }
            else
            {
                if (ftnValidarSeguridadCampos_bol(txtValorProyecto.Text.Trim()) == false)
                {
                    lblError.Text = "El campo de VALOR PROYECTO tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
                    return false;
                }
            }
        }

        if (intOp == 2)
        {
            if (ddlActividad.SelectedValue == "Seleccione...")
            {
                lblError2.Text = "Debe seleccionar una actividad";
                return false;
            }

            if (txtValorProyectado.Text.Trim() == string.Empty)
            {
                lblError2.Text = "Debe indicar el valor proyectado.";
                return false;
            }

            if (txtValorContratado.Text.Trim() == string.Empty)
            {
                lblError2.Text = "Debe indicar el valor hasta el momento contratado.";
                return false;
            }

            if (txtValorDisponible.Text.Trim() == string.Empty)
            {
                txtValorDisponible.Text = (Convert.ToInt32(txtValorProyectado.Text.Trim()) - Convert.ToInt32(txtValorContratado.Text.Trim())).ToString();
            }

            if (ddlEstado.SelectedValue == "Seleccione...")
            {
                lblError2.Text = "Debe seleccionar una estado a la actividad";
                return false;
            }
        }


        return true;
    }

    public void ftnInactivarPaneles(Boolean bolValor)
    {
        pnlAccionesSegContrato.Visible = bolValor;
        pnlMenuSegContrato.Visible = bolValor;
        pnlSegContratoNuevo.Visible = bolValor;
        pnlTraerActividades.Visible = bolValor;
    }

    protected void txtValorContratado_TextChanged(object sender, EventArgs e)
    {
        txtValorDisponible.Text = (Convert.ToDecimal(txtValorProyectado.Text.Trim()) - Convert.ToDecimal(txtValorContratado.Text.Trim())).ToString();
    }

    public void fntCargarGrillaActividades(int intIdRecursoProyecto)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaActividadesGrilla("strDsActividades", intIdRecursoProyecto, 0,0))
            {
                gvActividades.Columns[0].Visible = true;
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    pnlTraerActividades.Visible = true;
                    gvActividades.DataMember = "strDsActividades";
                    gvActividades.DataSource = blPaicma.myDataSet;
                    gvActividades.DataBind();
                }
                else
                {
                    pnlTraerActividades.Visible = false;
                }
                gvActividades.Columns[0].Visible = false;
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarGrillaProyectos(int intOp, int intIdCodigoPresupuesal, int intIdProyecto, int intIdTipoProyecto, string strNoContrato, string strFechaContrato, int intIdTipoIdentificacion, string strNumeroIdentificacion, float flbValorTotalProyecto, int intIdRecursoProyecto)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaProyectosGrilla("strDsProyectos", intOp, intIdCodigoPresupuesal, intIdProyecto, intIdTipoProyecto, strNoContrato, strFechaContrato, intIdTipoIdentificacion, strNumeroIdentificacion, flbValorTotalProyecto, intIdRecursoProyecto))
            {
                gvProyectos.Columns[0].Visible = true;
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvProyectos.DataMember = "strDsProyectos";
                    gvProyectos.DataSource = blPaicma.myDataSet;
                    gvProyectos.DataBind();
                    lblErrorGvProyectos.Text = string.Empty;
                }
                else
                {
                    gvProyectos.DataMember = "strDsProyectos";
                    gvProyectos.DataSource = blPaicma.myDataSet;
                    gvProyectos.DataBind();
                    lblErrorGvProyectos.Text = "No hay registros que coincidan con esos criterios.";
                }
                gvProyectos.Columns[0].Visible = false;
            }
            blPaicma.Termina();
        }
    }

    public void ftnLimpiarCamposActividades()
    {
        txtValorProyectado.Text = string.Empty;
        txtValorContratado.Text = "0";
        txtValorDisponible.Text = string.Empty;
    }

    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        pnlAccionesSegContrato.Visible = true;
        pnlMenuSegContrato.Visible = false;
        imgbEncontrar.Visible = true;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = false;
        imgbCancelar.Visible = true;
        pnlMenuSegContrato.Visible = false;
        pnlTraerActividades.Visible = false;
        pnlSegContratoNuevo.Visible = true;
        Activiades.Visible = false;
        PnlProyectos.Visible = false;
        fntCargarObjetos();
        fntLimpiarObjetos();
        fntOcultarCampos(false);
    }

    public void fntCargarObjetos()
    {
        fntCargarRubroPresupuestal();
        fntCargarProyecto();
        fntCargarTipoProyecto();
        fntCargarTipoIdentificacion();
    }

    public void fntLimpiarObjetos()
    {
        txtContrato.Text = string.Empty;
        txtFechaContrato.Text = string.Empty;
        txtNumeroIdentificacion.Text = string.Empty;
        txtContratista.Text = string.Empty;
        txtValorProyecto.Text = string.Empty;
    }

    public void fntOcultarCampos(Boolean bolValor)
    {
        txtContratista.Visible = bolValor;
    }

    protected void imgbEncontrar_Click(object sender, ImageClickEventArgs e)
    {
        int intIdCodigoPresupuesal = 0;
        int intIdProyecto = 0;
        int intIdTipoProyecto = 0;
        string strNoContrato = string.Empty;
        string strFechaContrato = string.Empty;
        int intIdTipoIdentificacion = 0;
        string strNumeroIdentificacion = string.Empty;
        float flbValorTotalProyecto = 0;

        if (ddlCodigoPresupuestal.SelectedValue != "Seleccione...")
        {
            intIdCodigoPresupuesal = Convert.ToInt32(ddlCodigoPresupuestal.SelectedValue.ToString());
        }

        if (ddlProyecto.SelectedValue != "Seleccione...")
        {
            intIdProyecto = Convert.ToInt32(ddlProyecto.SelectedValue.ToString());
        }

        if (ddlTipoProyecto.SelectedValue != "Seleccione...")
        {
            intIdTipoProyecto = Convert.ToInt32(ddlTipoProyecto.SelectedValue.ToString());
        }


        if (txtContrato.Text.Trim() != string.Empty)
        {
            strNoContrato = txtContrato.Text.Trim();
        }


        if (txtFechaContrato.Text.Trim() != string.Empty)
        {
            strFechaContrato = txtFechaContrato.Text.Trim();
        }

        if (ddlTipoIdentificacion.SelectedValue != "Seleccione...")
        {
            intIdTipoIdentificacion = Convert.ToInt32(ddlTipoIdentificacion.SelectedValue.ToString());
            
        }

        if (txtNumeroIdentificacion.Text.Trim() != string.Empty)
        {
            strNumeroIdentificacion = txtNumeroIdentificacion.Text.Trim();
        }

        if (txtValorProyecto.Text.Trim() != string.Empty)
        {
            flbValorTotalProyecto = Convert.ToInt32(txtValorProyecto.Text.Trim());
        }

        fntCargarGrillaProyectos(1, intIdCodigoPresupuesal, intIdProyecto, intIdTipoProyecto, strNoContrato, strFechaContrato, intIdTipoIdentificacion, strNumeroIdentificacion, flbValorTotalProyecto, 0);
        pnlAccionesSegContrato.Visible = false;
        pnlMenuSegContrato.Visible = true;
        pnlSegContratoNuevo.Visible = false;
        PnlProyectos.Visible = true;

    }

    protected void txtNumeroIdentificacion_TextChanged(object sender, EventArgs e)
    {
        string strNombre = string.Empty;
        string strApelido = string.Empty;
        int intIdContratista = 0;
        int intIdTipoIdentificacion = 0;

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (ddlTipoIdentificacion.SelectedValue.ToString() != "Seleccione...")
            {
                intIdTipoIdentificacion = Convert.ToInt32(ddlTipoIdentificacion.SelectedValue.ToString());
            }
            else
            {
                intIdTipoIdentificacion = 0;
            }


            if (blPaicma.fntConsultaPersona("strDsProyectos", 0, string.Empty, string.Empty, 0, 0, string.Empty, 0, string.Empty, 0, 1, intIdTipoIdentificacion, txtNumeroIdentificacion.Text.Trim()))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    intIdContratista = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                    Session["intIdContratista"] = intIdContratista;
                    strNombre = blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                    strApelido = blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                    strNombre = strNombre + " " + strApelido;
                    txtContratista.Text = strNombre;
                    lblErrorContratista.Text = string.Empty;
                }
                else
                {
                    txtContratista.Text = string.Empty;
                    lblErrorContratista.Text = "El Contratista no está registrado en el sistema, por favor contacte al administrador para ingresarlo.";
                }
            }
            blPaicma.Termina();
        }

    }

    protected void gvProyectos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            gvProyectos.Columns[0].Visible = true;

            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvProyectos.Rows[intIndex];
            TableCell Item = selectedRow.Cells[0];
            int intIdRecursoProyecto = Convert.ToInt32(Item.Text);
            Session["intIdRecursoProyecto"] = intIdRecursoProyecto;
            imgbEditar.Visible = true;

            gvProyectos.Columns[0].Visible = false;

        }
    }

    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        pnlAccionesSegContrato.Visible = true;
        imgbEncontrar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        pnlSegContratoNuevo.Visible = true;
        pnlMenuSegContrato.Visible = false;
        Activiades.Visible = false;
        fntCargarObjetos();


        PnlProyectos.Visible = false;
        pnlTraerActividades.Visible = false;
        fntOcultarCampos(true);
        Session["Operacion"] = "Actualizar";

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaProyectosGrilla("strDsProyectos", 2, 0, 0, 0, string.Empty, string.Empty, 0, string.Empty, 0, Convert.ToInt32(Session["intIdRecursoProyecto"].ToString())))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    string strFechaAux = string.Empty;
                    ddlCodigoPresupuestal.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                    ddlProyecto.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                    ddlTipoProyecto.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                    txtContrato.Text = blPaicma.myDataSet.Tables[0].Rows[0][13].ToString();
                    strFechaAux = blPaicma.myDataSet.Tables[0].Rows[0][14].ToString();
                    txtFechaContrato.Text = strFechaAux.Substring(0, 10).ToString();
                    ddlTipoIdentificacion.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][11].ToString();
                    txtNumeroIdentificacion.Text = blPaicma.myDataSet.Tables[0].Rows[0][10].ToString();
                    Session["intIdContratista"] = blPaicma.myDataSet.Tables[0].Rows[0][15].ToString();
                    txtContratista.Text = blPaicma.myDataSet.Tables[0].Rows[0][12].ToString();
                    txtValorProyecto.Text = blPaicma.myDataSet.Tables[0].Rows[0][7].ToString();
                    fntCargarGrillaActividades(Convert.ToInt32(Session["intIdRecursoProyecto"].ToString()));

                }
            }
            
            blPaicma.Termina();
        }
        ftnValidarPermisos();

    }

    protected void gvActividades_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            gvActividades.Columns[0].Visible = true;
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvActividades.Rows[intIndex];
            TableCell Item = selectedRow.Cells[0];
            int intIdActividadProyecto = Convert.ToInt32(Item.Text);
            Session["intIdActividadProyecto"] = intIdActividadProyecto;

            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultaActividadesGrilla("strDsActividadesProyecto", 0, 1, intIdActividadProyecto))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        Activiades.Visible = true;
                        fntCargarActividadRecursoProyecto();
                        fntCargarEstadoActividadRecursoProyecto();
                        ddlActividad.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                        txtValorProyectado.Text = blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                        txtValorContratado.Text = blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                        txtValorDisponible.Text = blPaicma.myDataSet.Tables[0].Rows[0][6].ToString();
                        ddlEstado.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][7].ToString();
                        pnlTraerActividades.Visible = false;
                        Session["Operacion"] = "ActualizarActividades";
                    }
                    else
                    {

                    }
                }
                blPaicma.Termina();
            }
            gvActividades.Columns[0].Visible = false;
        }

    }

    protected void gvProyectos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProyectos.Columns[0].Visible = true;
        gvProyectos.PageIndex = e.NewPageIndex;
        fntCargarGrillaProyectos(0, 0, 0, 0, string.Empty, string.Empty, 0, string.Empty, 0, 0);
        gvProyectos.Columns[0].Visible = false;
    }

    protected void gvActividades_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvActividades.Columns[0].Visible = true;
        gvActividades.PageIndex = e.NewPageIndex;
        fntCargarGrillaActividades(Convert.ToInt32(Session["intIdRecursoProyecto"].ToString()));
        gvActividades.Columns[0].Visible = false;
    }

    public Boolean ftnValidarSeguridadCampos_bol(string strTextoCampo)
    {
        Boolean bolValor = false;
        int intCantidadSeguridad = 0;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntEliminarTablaExpresionCampo_bol())
            {
                if (blPaicma.fntIngresarExpresionCampo_bol(strTextoCampo))
                {
                    if (blPaicma.fntConsultaExpresionSeguridad("strDsSeguridadCampos", strTextoCampo))
                    {
                        intCantidadSeguridad = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                        if (intCantidadSeguridad > 0)
                        {
                            bolValor = false;
                        }
                        else
                        {
                            bolValor = true;
                        }
                    }
                }
            }
            blPaicma.Termina();
        }
        return bolValor;
    }


}