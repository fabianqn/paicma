﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _web_frmCERM : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        { 
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                if (Session["Resultado"] == null)
                {
                    pnlRespuesta.Visible = false;
                }
                else
                {
                    if (Session["Resultado"].ToString() == "OK")
                    {
                        pnlRespuesta.Visible = true;
                        pnlAccionesErm.Visible = false;
                        pnlERM.Visible = false;
                        pnlTitulo.Visible = false;
                        lblRespuesta.Text = "Datos Cargados Exitosamente";


                        ///CARGA DE DOCUMENTO PARA LA AUDITORIA DEL SISTEMA
                        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                        {
                            string strModulo = string.Empty;
                            string strOpcion = string.Empty;
                            string strNombreArchivo = string.Empty;
                            strModulo = "E.R.M.";

                            if (Session["Modulo"] != null)
                            {
                                strOpcion = Session["Modulo"].ToString();
                            }
                            if (Session["strRutaCompleta"] != null)
                            {
                                strNombreArchivo = Session["strRutaCompleta"].ToString();
                            }

                            if (blPaicma.fntIngresarCargaDocumentos(strModulo, strOpcion, strNombreArchivo, Convert.ToInt32(Session["IdUsuario"].ToString())))
                            {
                                Session["Modulo"] = null;
                                Session["Resultado"] = null;
                            }
                            blPaicma.Termina();
                        }
                        ///CARGA DE DOCUMENTO PARA LA AUDITORIA DEL SISTEMA

                    }
                        else if (Session["Resultado"].ToString() == "NO")
                        {

                            pnlRespuesta.Visible = true;

                            ///validacion de estructura
                            if (Session["ESTRUCTURA"] != null)
                            {
                                if (Session["ESTRUCTURA"].ToString() == "NO")
                                {
                                    lblRespuesta.Text = "El archivo no tiene la estructura requerida, por favor verificar el archivo o de lo contrario contactarse con el administrador.";
                                }
                            }
                            ///fin validacion de estructura

                            if (Session["PELIGRO"] == null)
                            {
                                lblRespuesta.Text = "Error en la carga de información, por favor contacte al administrador";
                            }
                            else
                            {
                                if (Session["PELIGRO"].ToString() == "OK")
                                { 
                                    lblRespuesta.Text = "Este archivo no se puede subir por que tiene palabras registradas exclusivas del motor de base de datos y se asumen como amenaza a la integridad de los datos, por favor contacte al administrador"; 
                                }
                            }
                            pnlAccionesErm.Visible = false;
                            pnlERM.Visible = false;
                            pnlTitulo.Visible = false;
                        }
                }
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }

    protected void ddlOpciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlOpciones.SelectedValue.ToString() == "0")
        {
            pnlAccionesErm.Visible = false;
            fuCargarArchivos.Visible = false;
        }
        else
        {
            pnlAccionesErm.Visible = true;
            fuCargarArchivos.Visible = true;
        }
    }

    protected void imgbGuardar_Click(object sender, ImageClickEventArgs e)
    {
        string strNombreArchivo = string.Empty;
        string strExtDocumento = string.Empty;
        string strRutaCompleta = string.Empty;
        string strSw = string.Empty;
        int intOpcionCargar = 0;
        strNombreArchivo =  fuCargarArchivos.FileName;
        

        if (strNombreArchivo == string.Empty)
        {
            //esta vacio el fileupload
            strSw = "NO";
        }
        else
        {
            //esta lleno el fileupload
            strExtDocumento = System.IO.Path.GetExtension(fuCargarArchivos.PostedFile.FileName);
            if (strExtDocumento == ".xls")
            {
                strRutaCompleta = Server.MapPath("~\\Archivos\\");
                strRutaCompleta = strRutaCompleta + strNombreArchivo;
                fuCargarArchivos.SaveAs(strRutaCompleta);

                Session["strRutaCompleta"] = strNombreArchivo;
                strSw = "OK";
                intOpcionCargar = Convert.ToInt32(ddlOpciones.SelectedValue.ToString());
                lblError.Text = string.Empty;
                if (intOpcionCargar == 1)
                {
                    Session["Modulo"] = "Lista de Asistenaci SENA";
                    Response.Redirect("frmERM_ListaAsistencia.aspx");
                }
                else if (intOpcionCargar == 2)
                {
                    Session["Modulo"] = "Evaluación de Conocimientos";
                    Response.Redirect("frmERM.aspx");
                }
                else if (intOpcionCargar == 3)
                {
                    Session["Modulo"] = "Base de Datos Capacitación Minas Antipersonal SENA";
                    Response.Redirect("frmERM_EstadisticaCurso.aspx");
                }
                else
                {
                    Session["Modulo"] = "Base de Datos Capacitación Atención Salud";
                    Response.Redirect("frmERM_AtencionSalud.aspx");
                }
            }
            else
            {
                lblError.Text = "El tipo de archivo que está intentando cargar no es válido. Por favor verificar o comunicarse con el administrador.";
            }
        } 
    }

    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        pnlAccionesErm.Visible = false;
        ddlOpciones.SelectedValue = "0";
        fuCargarArchivos.Visible = false;
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        pnlRespuesta.Visible = false;
        pnlAccionesErm.Visible = false;
        pnlERM.Visible = true;
        pnlTitulo.Visible = false;
        Session["Resultado"] = null;
    }
}