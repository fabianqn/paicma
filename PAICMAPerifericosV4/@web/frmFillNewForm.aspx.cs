﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;

public partial class _web_FillNewForm : System.Web.UI.Page
{

    private blSisPAICMA blPaicma = new blSisPAICMA();
    List<string> ListaNombreCampos = new List<string>();
    List<string> ListaIDCampos = new List<string>();
    List<object> ListaValores = new List<object>();
    List<string> ListaCondicion = new List<string>();
    List<string> ListaTipoDato = new List<string>();
    List<Control> ListaControles = new List<Control>();


    protected void Page_Init(object sender, EventArgs e)
    {
        //if (!(Page.IsPostBack))
        //{
        int intTablaTemporal = Convert.ToInt32(Session["intTablaSeleccionada"]);
        
        string NombreTablaSeleccionada = Session["strTablaSeleccionada"].ToString();
        string abreviatura = "";
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            blPaicma.fntConsultaTodoPorId("datosTablaAbreviatura", "Form_AdmTablas", "admTab_Id", intTablaTemporal, "admTab_NombreTabla");
            abreviatura = blPaicma.myDataSet.Tables["datosTablaAbreviatura"].Rows[0]["admTab_AbreviaturaTabla"].ToString();
        }
       
        
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                try
                {
                    if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                    {

                        if (blPaicma.fntConsultaDetalleCampos("strDetalleFormRender", "Form_CampoTabla", intTablaTemporal))
                        {
                            DataTable dtTable;
                            dtTable = blPaicma.myDataSet.Tables["strDetalleFormRender"];
                            List<Control> ctrls = new List<Control>();
                            int IndicadorControl = 0;
                              string VictimaID = Session["VictimaID"].ToString();
                            string VictimaNombre = Session["VictimaNombre"].ToString();
                            string VictimaApellido = Session["VictimaApellido"].ToString();
                            string VictimaEstado = Session["VictimaEstado"].ToString();
                            string VictimaDepartamento = Session["VictimaDepartamento"].ToString();
                            string VictimaMunicipio = Session["VictimaMunicipio"].ToString();
                            DataTable dataSourceToDropDown = new DataTable();
                            if (VictimaID != "") {
                                AddCustomAttribute(0, 2, "DocumentoVictima", "Documento Victima", 99, 100, 0, abreviatura, IndicadorControl, dataSourceToDropDown, VictimaID);
                                IndicadorControl++;
                                AddCustomAttribute(0, 2, "NombreVictima", "Nombre Victima", 100, 100, 0, abreviatura, IndicadorControl, dataSourceToDropDown, VictimaNombre);
                                IndicadorControl++;
                                AddCustomAttribute(0, 2, "ApellidoVictima", "Apellido Victima", 99, 100, 0, abreviatura, IndicadorControl, dataSourceToDropDown, VictimaApellido);
                                IndicadorControl++;
                                AddCustomAttribute(0, 2, "EstadoVictima", "Estado Victima", 100, 100, 0, abreviatura, IndicadorControl, dataSourceToDropDown, VictimaEstado);
                                IndicadorControl++;
                                AddCustomAttribute(0, 2, "DepartamentoVictima", "Departamento Victima", 99, 100, 0, abreviatura, IndicadorControl, dataSourceToDropDown, VictimaDepartamento);
                                IndicadorControl++;
                                AddCustomAttribute(0, 2, "MunicipioVictima", "Municipio Victima", 99, 100, 0, abreviatura, IndicadorControl, dataSourceToDropDown, VictimaMunicipio);
                                IndicadorControl++;

                            }
                           
                            foreach (DataRow dtRow in dtTable.Rows)
                            {
                                string tipoCam = dtRow["camForm_TipoControl"].ToString();

                                int idCampo = Convert.ToInt32(dtRow["camForm_IdCampo"].ToString());
                                dataSourceToDropDown = new DataTable();

                                if (tipoCam == "5" || tipoCam == "7" || tipoCam == "8")
                                {
                                    blPaicma.fntConsultaTodoPorId("dataSourceToDropDown", "Form_OptionsList", "IdCampoRelacion", idCampo, "camDesc");
                                    dataSourceToDropDown = blPaicma.myDataSet.Tables["dataSourceToDropDown"];
                                }


                                Int16 TipoCampo = Convert.ToInt16(dtRow["camForm_TipoControl"].ToString());
                                string NombreCampo = dtRow["camForm_NombreCampo"].ToString();
                                string DescLabel = dtRow["camForm_Label"].ToString();
                                int Orden = Convert.ToInt32(dtRow["camForm_Orden"].ToString());
                                int LongMax = Convert.ToInt32(dtRow["camForm_LongitudMax"].ToString());
                                int TablaPertenece = Convert.ToInt32(dtRow["camForm_admTab_Id"].ToString());
                                string valor = "";
                                AddCustomAttribute(idCampo, TipoCampo, NombreCampo, DescLabel, Orden, LongMax, TablaPertenece, abreviatura, IndicadorControl, dataSourceToDropDown, valor);
                                IndicadorControl++;
                            }
                            Session["ListaNombreCampos"] = ListaNombreCampos;
                            Session["ListaIDCampos"] = ListaIDCampos;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("An error occurred: '{0}'", ex);
                }

                
            }
        //}

        //}
    }

    private void AddCustomAttribute(int idCampo, Int16 TipoCampo, string NombreCampo, string DescLabel, int Orden, int LongMax, int TablaPertenece, string NombreTablaSeleccionada, int IndicadorControl, DataTable dataSourceToDropDown, string valor)
    {
        TableRow tr = new TableRow();
        TableCell tdName = new TableCell();
        tdName.Text = DescLabel;
        //tdName.VerticalAlign = VerticalAlign.Top;
        //tdName.Style.Add("text-transform", "uppercase");
        tr.Cells.Add(tdName);

        List<Control> UIControls = CreateCustomAttributeUI(idCampo, TipoCampo, NombreCampo, DescLabel, Orden, LongMax, TablaPertenece, NombreTablaSeleccionada, IndicadorControl, dataSourceToDropDown, valor);
        TableCell tdUI = new TableCell();

        foreach (Control ctrl in UIControls)
        {
            tdUI.Controls.Add(ctrl);
        }
        tr.Cells.Add(tdUI);
        CustomUITable.Rows.Add(tr);

    }

    private List<Control> CreateCustomAttributeUI(int idCampo, Int16 TipoCampo, string NombreCampo, string DescLabel, int Orden, int LongMax, int TablaPertenece, string NombreTablaSeleccionada, int IndicadorControl, DataTable dataSourceToDropDown, string valor)
    {
        List<Control> ctrls = new List<Control>();

        switch (TipoCampo)
        {
            case 1:
               TextBox tb = new TextBox();
               tb.ID = "entero" + IndicadorControl;
               tb.Attributes["type"] = "number";
               tb.CssClass = "entero";
               tb.Attributes["data-orden"] = Orden.ToString();
               tb.Attributes["data-nombre"] = NombreTablaSeleccionada+"_"+NombreCampo;
               tb.Attributes["data-DbType"] = "int";
               tb.Attributes["maxlength"] = LongMax.ToString();
               tb.TextMode = TextBoxMode.SingleLine;
               tb.MaxLength = 30;
               ListaNombreCampos.Add(NombreTablaSeleccionada + "_" + NombreCampo);
               ListaIDCampos.Add("entero" + IndicadorControl);
               ListaTipoDato.Add("int");
               ctrls.Add(tb);
                break;
            case 2:
                if (idCampo == 0)
                {
                    TextBox tb1 = new TextBox();
                    tb1.ID = "alfanumerico" + IndicadorControl;
                    tb1.Attributes["type"] = "text";
                    tb1.Enabled = false;
                    tb1.CssClass = "campo";
                    tb1.Text = valor;
                    tb1.TextMode = TextBoxMode.SingleLine;
                    tb1.Attributes["data-orden"] = Orden.ToString();
                    tb1.Attributes["data-nombre"] = NombreTablaSeleccionada + "_" + NombreCampo;
                    tb1.Attributes["data-DbType"] = "nvarchar";
                    tb1.Attributes["maxlength"] = LongMax.ToString();
                    //ListaNombreCampos.Add(NombreTablaSeleccionada + "_" + NombreCampo);
                    //ListaIDCampos.Add("alfanumerico" + IndicadorControl);
                    ListaTipoDato.Add("nvarchar");
                    ctrls.Add(tb1);
                }
                else
                {
                    TextBox tb1 = new TextBox();
                    tb1.ID = "alfanumerico" + IndicadorControl;
                    tb1.Attributes["type"] = "text";
                    tb1.Text = valor;
                    tb1.CssClass = "campo";
                    tb1.TextMode = TextBoxMode.SingleLine;
                    tb1.Attributes["data-orden"] = Orden.ToString();
                    tb1.Attributes["data-nombre"] = NombreTablaSeleccionada + "_" + NombreCampo;
                    tb1.Attributes["data-DbType"] = "nvarchar";
                    tb1.Attributes["maxlength"] = LongMax.ToString();
                    ListaNombreCampos.Add(NombreTablaSeleccionada + "_" + NombreCampo);
                    ListaIDCampos.Add("alfanumerico" + IndicadorControl);
                    ListaTipoDato.Add("nvarchar");
                    ctrls.Add(tb1);
                }
                break;
            case 3:
                //Calendar dateCal = new Calendar();
                //dateCal.ID = "fecha" + IndicadorControl;
                //dateCal.Attributes["type"] = "date";
                //dateCal.Attributes["data-orden"] = Orden.ToString();
                //dateCal.Attributes["data-nombre"] = NombreTablaSeleccionada + "_" + NombreCampo;
                //dateCal.Attributes["data-DbType"] = "datetime";
                //dateCal.Attributes.Add("AutoPostBack", "False");
                //ListaNombreCampos.Add(NombreTablaSeleccionada + "_" + NombreCampo);
                //ListaIDCampos.Add("fecha" + IndicadorControl);

                //ListaTipoDato.Add("datetime");
                //ctrls.Add(dateCal);
                TextBox tb2 = new TextBox();
                tb2.ID = "fecha" + IndicadorControl;
                tb2.Attributes["type"] = "date";
                tb2.Attributes["data-orden"] = Orden.ToString();
                tb2.Attributes["data-nombre"] = NombreTablaSeleccionada + "_" + NombreCampo;
                tb2.Attributes["data-DbType"] = "datetime";
                tb2.CssClass = "pluginFecha";
         
                tb2.Attributes["readonly"] = "true";
                tb2.TextMode = TextBoxMode.SingleLine;
                ListaNombreCampos.Add(NombreTablaSeleccionada + "_" + NombreCampo);
                ListaIDCampos.Add("fecha" + IndicadorControl);
                ListaTipoDato.Add("datetime");
                ctrls.Add(tb2);
                break;
            case 4:
                CheckBox checkB = new CheckBox();
                checkB.ID = "check" + IndicadorControl;
                checkB.Attributes["data-orden"] = Orden.ToString();
                checkB.Attributes["data-nombre"] = NombreTablaSeleccionada + "_" + NombreCampo;
                ListaNombreCampos.Add(NombreTablaSeleccionada + "_" + NombreCampo);
                checkB.Attributes["data-DbType"] = "bit";
                ListaIDCampos.Add("check" + IndicadorControl);
                ListaTipoDato.Add("bit");
                ctrls.Add(checkB);
                break;
            case 5:
                 DropDownList Combo = new DropDownList();
                 Combo.ID = "combo" + IndicadorControl;
                 Combo.DataSource = dataSourceToDropDown;
                 Combo.DataTextField = "camDesc";
                 Combo.DataValueField = "camValue";
                 Combo.DataBind(); 
                 Combo.Attributes["data-orden"] = Orden.ToString();
                 Combo.Attributes["data-nombre"] = NombreTablaSeleccionada + "_" + NombreCampo;
                 ListaNombreCampos.Add(NombreTablaSeleccionada + "_" + NombreCampo);
                 Combo.Attributes["data-DbType"] = "nvarchar";
                 ListaIDCampos.Add("combo" + IndicadorControl);
                 ListaTipoDato.Add("nvarchar");
                 ctrls.Add(Combo);
                break;
            case 6:
                TextBox tb3 = new TextBox();
                tb3.ID = "decimal" + IndicadorControl;
                tb3.Attributes["type"] = "number";
                tb3.Attributes["step"] = "0.01";
                tb3.CssClass = "decimal";
                tb3.TextMode = TextBoxMode.SingleLine;
                tb3.Attributes["data-orden"] = Orden.ToString();
                tb3.Attributes["data-nombre"] = NombreTablaSeleccionada + "_" + NombreCampo;
                tb3.Attributes["data-DbType"] = "decimal";
                tb3.Attributes["maxlength"] = LongMax.ToString();
                ListaNombreCampos.Add(NombreTablaSeleccionada + "_" + NombreCampo);
                ListaIDCampos.Add("decimal" + IndicadorControl);
                ListaTipoDato.Add("decimal");
                ctrls.Add(tb3);
                break;
            case 7:
                DropDownList ComboMunicipio = new DropDownList();
                ComboMunicipio.ID = "comboMunicipio" + IndicadorControl;
                ComboMunicipio.CssClass = "ddMunicipio";
                foreach (ListItem var in fillMunicipios())
                {
                    ComboMunicipio.Items.Add(var);
                }
      
                ComboMunicipio.DataTextField = "Text";
                ComboMunicipio.DataValueField = "Value";
                ComboMunicipio.DataBind();
                ComboMunicipio.Attributes["data-orden"] = Orden.ToString();
                ComboMunicipio.Attributes["data-nombre"] = NombreTablaSeleccionada + "_" + NombreCampo;
                 ListaNombreCampos.Add(NombreTablaSeleccionada + "_" + NombreCampo);
                 ComboMunicipio.Attributes["data-DbType"] = "nvarchar";
                 ListaIDCampos.Add("comboMunicipio" + IndicadorControl);
                 ListaTipoDato.Add("nvarchar");
                 ctrls.Add(ComboMunicipio);
                break;
            case 8:
                DropDownList ComboDepartamento = new DropDownList();
                ComboDepartamento.ID = "comboDepartamento" + IndicadorControl;
                ComboDepartamento.CssClass = "ddDepartamento";
                ComboDepartamento.DataSource = fillDepartamentos();
                ComboDepartamento.DataTextField = "Text";
                ComboDepartamento.DataValueField = "Value";
        
                ComboDepartamento.DataBind();
                ComboDepartamento.Attributes["data-orden"] = Orden.ToString();
                ComboDepartamento.Attributes["data-nombre"] = NombreTablaSeleccionada + "_" + NombreCampo;
                ListaNombreCampos.Add(NombreTablaSeleccionada + "_" + NombreCampo);
                ComboDepartamento.Attributes["data-DbType"] = "nvarchar";
                ListaIDCampos.Add("comboDepartamento" + IndicadorControl);
                ListaTipoDato.Add("nvarchar");
                ctrls.Add(ComboDepartamento);
                break;
            default:
               TextBox tb4 = new TextBox();
               tb4.ID = "alfanumerico" + IndicadorControl;
               tb4.Attributes["type"] = "text";
               tb4.Attributes["data-orden"] = Orden.ToString();
               tb4.Attributes["data-nombre"] = NombreTablaSeleccionada+"_"+NombreCampo;
               tb4.TextMode = TextBoxMode.SingleLine;
               tb4.Attributes["data-DbType"] = "nvarchar";
               tb4.Attributes["maxlength"] = LongMax.ToString();
               tb4.CssClass = "campo";
               ListaNombreCampos.Add(NombreTablaSeleccionada + "_" + NombreCampo);
               ListaTipoDato.Add("nvarchar");
               ListaIDCampos.Add("alfanumerico" + IndicadorControl);
               ctrls.Add(tb4);
               break;
        }
         return ctrls;
    }


    protected void UpdateButton_Click(object sender, EventArgs e)
    {

        ListaNombreCampos = new List<string>((List<string>)Session["ListaNombreCampos"]);
        ListaIDCampos = new List<string>((List<string>)Session["ListaIDCampos"]);
        //UIControles = new List<Control>((List<Control>)Session["Controles"]);
            for (int i = 0; i < ListaIDCampos.Count(); i++)
            {
                string nombrecontrol = ListaIDCampos[i].ToString();
                Control _control = CustomUITable.FindControl(nombrecontrol);
                ListaControles.Add(_control);
            }
        string NombreTablaSeleccionada = Session["strTablaSeleccionada"].ToString();
        string IMSMA = Session["IMSMA"].ToString().Trim();
        bool esVictima = false;
        string abreviatura = "";
        if (IMSMA != string.Empty)
        {
            esVictima = true;
            int intTablaTemporal = Convert.ToInt32(Session["intTablaSeleccionada"]);
            blPaicma.fntConsultaTodoPorId("datosConsultaTablaAbre", "Form_AdmTablas", "admTab_Id", intTablaTemporal, "admTab_NombreTabla");
            abreviatura = blPaicma.myDataSet.Tables["datosConsultaTablaAbre"].Rows[0]["admTab_AbreviaturaTabla"].ToString();
        }
        if (blPaicma.fntInsertarDatosDinamicos_bol(ListaControles, NombreTablaSeleccionada, esVictima, IMSMA, abreviatura))
        {
            HtmlMeta meta = new HtmlMeta();
            meta.HttpEquiv = "Refresh";
            meta.Content = "5;url=frmFormCreacion.aspx";
            this.Page.Controls.Add(meta);
            LabelMsgCorrecto.Visible = true;
            mensajes.Visible = true;
            Label1.Text = "Sera redirigido en 5 segundos";
            Label1.Visible = true;
        }
        else
        {
            HtmlMeta meta = new HtmlMeta();
            meta.HttpEquiv = "Refresh";
            meta.Content = "5;url=frmFormCreacion.aspx";
            this.Page.Controls.Add(meta);
            mensajes.Visible = true;
            LabelMsgError.Visible = true;
            Label1.Text = "Sera redirigido en 5 segundos";
            Label1.Visible = true;

        }
    }

    protected void imgbCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/@web/frmFormCreacion.aspx");
    }

    protected List<ListItem> fillDepartamentos()
    {
        List<ListItem> resultado = new List<ListItem>();

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
           {
              if (blPaicma.fntConsultarDepartamentos("strDepartamentosConsultar")){
                   DataTable dataSourceToDropDown = new DataTable();
                   dataSourceToDropDown = blPaicma.myDataSet.Tables["strDepartamentosConsultar"];

                  foreach (DataRow row in dataSourceToDropDown.Rows)
                  {
                      string valor = row[0].ToString();
                      string texto = row[1].ToString();
                      ListItem valorCombo1 = new ListItem();
                      valorCombo1.Value = valor;
                      valorCombo1.Text = texto;
                      resultado.Add(valorCombo1);
                  }
              }
        }
        return resultado;
    }
    protected List<ListItem> fillMunicipios()
    {
        List<ListItem> resultado = new List<ListItem>();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultarMunicipios("strMunicipiosConsultar"))
            {
                DataTable dataSourceToDropDown = new DataTable();
                dataSourceToDropDown = blPaicma.myDataSet.Tables["strMunicipiosConsultar"];

                foreach (DataRow row in dataSourceToDropDown.Rows)
                {
                    string valor = row[0].ToString();
                    string texto = row[1].ToString();
                    string departamento = row[2].ToString();
                    ListItem valorCombo2 = new ListItem();
                    valorCombo2.Value = valor;
                    valorCombo2.Text = texto;
                    valorCombo2.Attributes["data-departamento"] = departamento;
                    resultado.Add(valorCombo2);
                }
            }
        }
        return resultado;
    }

    
    }

 


    //private object GetValueForCustomAttribute(Control ctrl)
    //{
    //    object retorna;
    //    switch (ctrl.GetType().Name.ToString())
    //    {
    //        case "TextBox":
    //            TextBox tb = (TextBox)ctrl;
    //            if (!string.IsNullOrEmpty(tb.Text))
    //            {
    //                retorna = tb.Text.Trim();
    //            }

    //            break;
    //        case DataTypeIdEnum.Boolean:
    //            CheckBox cb = (CheckBox)ctrl;
    //            userInputParam.Value = cb.Checked;
    //            userInputParam.DbType = Data.DbType.Boolean;

    //            break;
    //        case DataTypeIdEnum.Numeric:
    //            TextBox tb = (TextBox)ctrl;
    //            userInputParam.DbType = Data.DbType.Double;
    //            if (!string.IsNullOrEmpty(tb.Text))
    //            {
    //                userInputParam.Value = tb.Text.Trim();
    //            }

    //            break;
    //        case DataTypeIdEnum.Date:
    //            TextBox tb = (TextBox)ctrl;
    //            userInputParam.DbType = Data.DbType.Date;
    //            if (!string.IsNullOrEmpty(tb.Text))
    //            {
    //                userInputParam.Value = tb.Text.Trim();
    //            }

    //            break;
    //        case DataTypeIdEnum.PickList:
    //            DropDownList ddl = (DropDownList)ctrl;
    //            userInputParam.DbType = Data.DbType.Guid;
    //            if (ddl.SelectedValue != DropDownListValueForSelectOne)
    //            {
    //                userInputParam.Value = new Guid(ddl.SelectedValue);
    //            }
    //            break;
    //    }

    //    return userInputParam;
    //}

    // Private Function GetValueForCustomAttribute(ByVal DynamicAttributeId As Guid, ByVal DataTypeId As DataTypeIdEnum) As SqlParameter
    //    Dim userInputParam As New SqlParameter
    //    userInputParam.ParameterName = "@DynamicValue"
    //    userInputParam.Value = DBNull.Value

    //    Dim ctrlId As String = GetID(DynamicAttributeId)
    //    Dim ctrl As Control = CustomUITable.FindControl(ctrlId)

    //    Select Case DataTypeId
    //        Case DataTypeIdEnum.String
    //            Dim tb As TextBox = CType(ctrl, TextBox)
    //            userInputParam.DbType = Data.DbType.String
    //            If Not String.IsNullOrEmpty(tb.Text) Then
    //                userInputParam.Value = tb.Text.Trim()
    //            End If

    //        Case DataTypeIdEnum.Boolean
    //            Dim cb As CheckBox = CType(ctrl, CheckBox)
    //            userInputParam.Value = cb.Checked
    //            userInputParam.DbType = Data.DbType.Boolean

    //        Case DataTypeIdEnum.Numeric
    //            Dim tb As TextBox = CType(ctrl, TextBox)
    //            userInputParam.DbType = Data.DbType.Double
    //            If Not String.IsNullOrEmpty(tb.Text) Then
    //                userInputParam.Value = tb.Text.Trim()
    //            End If

    //        Case DataTypeIdEnum.Date
    //            Dim tb As TextBox = CType(ctrl, TextBox)
    //            userInputParam.DbType = Data.DbType.Date
    //            If Not String.IsNullOrEmpty(tb.Text) Then
    //                userInputParam.Value = tb.Text.Trim()
    //            End If

    //        Case DataTypeIdEnum.PickList
    //            Dim ddl As DropDownList = CType(ctrl, DropDownList)
    //            userInputParam.DbType = Data.DbType.Guid
    //            If ddl.SelectedValue <> DropDownListValueForSelectOne Then
    //                userInputParam.Value = New Guid(ddl.SelectedValue)
    //            End If
    //    End Select

    //    Return userInputParam
    //End Function

 

    //protected void imgbGuardar_Click(object sender, EventArgs e)
    //{
    //}

    
    //protected void imgbCancelar_Click(object sender, EventArgs e)
    //{
    //}
    //protected void Unnamed_Click(object sender, EventArgs e)
    //{

    //}
    //protected void enviar_Click(object sender, EventArgs e)
    //{
    //    foreach (string s in Request.Form.AllKeys)
    //    {
    //        Response.Write(s + "<br />");
    //    }
    //}

    //[WebMethod]
    //public static string GetListaControles()
    //{
    //   return "hola";
    //}
//}