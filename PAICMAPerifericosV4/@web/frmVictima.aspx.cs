﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ASP;

public partial class _web_frmVictima : System.Web.UI.Page
{
    
    private blSisPAICMA blPaicma = new blSisPAICMA();
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (this.Session["IdUsuario"] == null)
            {
                base.Response.Redirect("~/@dmin/frmLogin.aspx");
                return;
            }
            this.ftnValidarPermisos();
            this.PnlVictimaGrilla.Visible = true;
            this.fntCargarGrillaVictima(0, 0);
        }
    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        this.blPaicma = null;
    }
    public void ftnValidarPermisos()
    {
        string a = string.Empty;
        string a2 = string.Empty;
        string a3 = string.Empty;
        string a4 = string.Empty;
        string[] array = HttpContext.Current.Request.RawUrl.Split(new char[]
		{
			'/'
		});
        string text = array[array.GetUpperBound(0)];
        array = text.Split(new char[]
		{
			'?'
		});
        text = array[array.GetLowerBound(0)];
        if (text != string.Empty)
        {
            if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
            {
                if (this.blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(this.Session["IdUsuario"].ToString()), text) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    a = this.blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                    a2 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                    a3 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                    a4 = this.blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();
                    if (a == "False")
                    {
                        this.imgbBuscar.Visible = false;
                    }
                    if (a2 == "False")
                    {
                        this.imgbNuevo.Visible = false;
                    }
                    if (a3 == "False")
                    {
                        this.imgbEditar.Visible = false;
                        this.imgbGravar.Visible = false;
                    }
                    if (a4 == "False")
                    {
                        this.imgbEliminar.Visible = false;
                    }
                }
                this.blPaicma.Termina();
                return;
            }
        }
        else
        {
            base.Response.Redirect("@dmin/frmLogin.aspx");
        }
    }
    public void fntCargarGrillaVictima(int intOp, int intIdVictima)
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaDatosVictima_bol("strDsGrillaVictimas", intOp, intIdVictima))
            {
                this.gvVictimas.Columns[0].Visible = true;
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gvVictimas.DataMember = "strDsGrillaVictimas";
                    this.gvVictimas.DataSource = this.blPaicma.myDataSet;
                    this.gvVictimas.DataBind();
                    this.lblErrorGv.Text = string.Empty;
                }
                else
                {
                    this.gvVictimas.DataMember = "strDsGrillaVictimas";
                    this.gvVictimas.DataSource = this.blPaicma.myDataSet;
                    this.gvVictimas.DataBind();
                    this.lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                this.gvVictimas.Columns[0].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    public void fntInactivarPaneles()
    {
        this.pnlAccionesVictima.Visible = false;
        this.pnlEliminacion.Visible = false;
        this.pnlMenuVictima.Visible = false;
        this.pnlRespuesta.Visible = false;
        this.pnlTitulo.Visible = false;
        this.PnlVictimaGrilla.Visible = false;
        this.pnlVictimaNuevo.Visible = false;
        this.pnlDetalleDatosVictima.Visible = false;
    }
    protected void gvVictimas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            this.gvVictimas.Columns[0].Visible = true;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow = this.gvVictimas.Rows[index];
            TableCell tableCell = gridViewRow.Cells[0];
            int num = Convert.ToInt32(tableCell.Text);
            this.Session["intIdVictima"] = num;
            this.imgbEditar.Visible = true;
            this.gvVictimas.Columns[0].Visible = false;
        }
    }
    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        this.Session["Operacion"] = "Actualizar";
        this.fntInactivarPaneles();
        this.pnlAccionesVictima.Visible = true;
        this.pnlVictimaNuevo.Visible = true;
        this.imgbEncontrar.Visible = false;
        this.fntCargarObjetos();
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaDatosVictima_bol("strDsGrillaVictimas", 1, Convert.ToInt32(this.Session["intIdVictima"].ToString())) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlTipoIdentificacion.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                this.txtNoIdentificacion.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                this.txtNombres.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                this.txtApellido.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                this.txttelefono.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][6].ToString();
                this.txtDireccion.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][7].ToString();
                this.txtFechaNacimiento.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][8].ToString();
                this.txtEdad.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][9].ToString();
                this.ddlGenero.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0][10].ToString();
                this.ddlNivelEducacion.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0][14].ToString();
                string text = string.Empty;
                text = this.blPaicma.myDataSet.Tables[0].Rows[0][12].ToString();
                if (text != string.Empty)
                {
                    this.ddlEtnia.SelectedValue = text;
                }
                this.fntCargarGrillaAccidenteVictima(Convert.ToInt32(this.Session["intIdVictima"].ToString()));
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarObjetos()
    {
        this.fntCargarTipoIdentificacion();
        this.fntCargarGenero();
        this.fntCargarEtnia();
        this.fntCargarNivelFormacion();
    }
    public void fntCargarTipoIdentificacion()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaTipoIdentificacion_bol("strDsTipoIdentificacion", string.Empty, 1) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlTipoIdentificacion.DataMember = "strDsTipoIdentificacion";
                this.ddlTipoIdentificacion.DataSource = this.blPaicma.myDataSet;
                this.ddlTipoIdentificacion.DataValueField = "tip_Id";
                this.ddlTipoIdentificacion.DataTextField = "tip_Descripcion";
                this.ddlTipoIdentificacion.DataBind();
                this.ddlTipoIdentificacion.Items.Insert(this.ddlTipoIdentificacion.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarGenero()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaGenero_bol("strDsGenero") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlGenero.DataMember = "strDsGenero";
                this.ddlGenero.DataSource = this.blPaicma.myDataSet;
                this.ddlGenero.DataValueField = "tip_Id";
                this.ddlGenero.DataTextField = "tip_Descripcion";
                this.ddlGenero.DataBind();
                this.ddlGenero.Items.Insert(this.ddlGenero.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarEtnia()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaEtnia_bol("strDsEtnia") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlEtnia.DataMember = "strDsEtnia";
                this.ddlEtnia.DataSource = this.blPaicma.myDataSet;
                this.ddlEtnia.DataValueField = "etn_Id";
                this.ddlEtnia.DataTextField = "nomGrupoPoblacional";
                this.ddlEtnia.DataBind();
                this.ddlEtnia.Items.Insert(this.ddlEtnia.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarNivelFormacion()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaNivelFormacion_bol("strDsNivelFormacion") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlNivelEducacion.DataMember = "strDsNivelFormacion";
                this.ddlNivelEducacion.DataSource = this.blPaicma.myDataSet;
                this.ddlNivelEducacion.DataValueField = "for_id";
                this.ddlNivelEducacion.DataTextField = "nivelEducativoPersona";
                this.ddlNivelEducacion.DataBind();
                this.ddlNivelEducacion.Items.Insert(this.ddlNivelEducacion.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        if (this.Session["Operacion"] == null || this.Session["Operacion"].ToString() == "Actualizar" || this.Session["Operacion"].ToString() == "Crear" || this.Session["Operacion"].ToString() == "CrearOK")
        {
            string url = "frmVictima.aspx";
            base.Response.Redirect(url);
            return;
        }
        if (this.Session["Operacion"].ToString() == "VerDatosAccidente")
        {
            this.Session["Operacion"] = "Actualizar";
            this.fntInactivarPaneles();
            this.pnlAccionesVictima.Visible = true;
            this.pnlVictimaNuevo.Visible = true;
            return;
        }
        if (this.Session["Operacion"].ToString() == "CrearDatosAccidente")
        {
            this.Session["Operacion"] = "CrearOK";
            this.fntInactivarPaneles();
            this.pnlAccionesVictima.Visible = true;
            this.pnlVictimaNuevo.Visible = true;
            this.imgbNuevoDatosAccidente.Visible = true;
        }
    }
    public void fntCargarGrillaAccidenteVictima(int intIdVictima)
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaAccidenteVictima_bol("strDsGrillaAccidenteVictima", intIdVictima))
            {
                this.gvDatosAccidente.Columns[0].Visible = true;
                if (this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    this.gvDatosAccidente.DataMember = "strDsGrillaAccidenteVictima";
                    this.gvDatosAccidente.DataSource = this.blPaicma.myDataSet;
                    this.gvDatosAccidente.DataBind();
                    this.lblErrorGv.Text = string.Empty;
                    this.gvDatosAccidente.Visible = true;
                    this.imgbNuevoDatosAccidente.Visible = false;
                }
                else
                {
                    this.gvDatosAccidente.DataMember = "strDsGrillaAccidenteVictima";
                    this.gvDatosAccidente.DataSource = this.blPaicma.myDataSet;
                    this.gvDatosAccidente.DataBind();
                    this.lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                    this.gvDatosAccidente.Visible = false;
                    this.imgbNuevoDatosAccidente.Visible = true;
                }
                this.gvDatosAccidente.Columns[0].Visible = false;
            }
            this.blPaicma.Termina();
        }
    }
    protected void gvDatosAccidente_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            this.gvDatosAccidente.Columns[0].Visible = true;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gridViewRow = this.gvDatosAccidente.Rows[index];
            TableCell tableCell = gridViewRow.Cells[0];
            int num = Convert.ToInt32(tableCell.Text);
            this.Session["intIdMonitoreoSeguimiento"] = num;
            this.gvDatosAccidente.Columns[0].Visible = false;
            this.fntInactivarPaneles();
            this.pnlDetalleDatosVictima.Visible = true;
            this.pnlAccionesVictima.Visible = true;
            this.Session["Operacion"] = "VerDatosAccidente";
            this.fntCargarDatosVictima();
            this.fntCargarDatosSeguimientoVictima();
        }
    }
    public void fntCargarDatosVictima()
    {
        this.fntCargarVereda();
        this.fntCargarAreaPoblacional();
        this.fntCargarEstadoVictima();
    }
    public void fntCargarVereda()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaTodasLasVereda_bol("strDsVereda") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlVereda.DataMember = "strDsVereda";
                this.ddlVereda.DataSource = this.blPaicma.myDataSet;
                this.ddlVereda.DataValueField = "ver_Id";
                this.ddlVereda.DataTextField = "vereda";
                this.ddlVereda.DataBind();
                this.ddlVereda.Items.Insert(this.ddlVereda.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarAreaPoblacional()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaAreaPoblacional_bol("strDsAreaPoblacional") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlAreaPoblacional.DataMember = "strDsAreaPoblacional";
                this.ddlAreaPoblacional.DataSource = this.blPaicma.myDataSet;
                this.ddlAreaPoblacional.DataValueField = "arepob_Id";
                this.ddlAreaPoblacional.DataTextField = "arepob_Nombre";
                this.ddlAreaPoblacional.DataBind();
                this.ddlAreaPoblacional.Items.Insert(this.ddlAreaPoblacional.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarEstadoVictima()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaEstadoVictima_bol("strDsEstadoVictima") && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlEstado.DataMember = "strDsEstadoVictima";
                this.ddlEstado.DataSource = this.blPaicma.myDataSet;
                this.ddlEstado.DataValueField = "est_Id";
                this.ddlEstado.DataTextField = "est_Descripcion";
                this.ddlEstado.DataBind();
                this.ddlEstado.Items.Insert(this.ddlEstado.Attributes.Count, "Seleccione...");
            }
            this.blPaicma.Termina();
        }
    }
    public void fntCargarDatosSeguimientoVictima()
    {
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaAccidenteVictima_bol("strDsGrillaAccidenteVictima", Convert.ToInt32(this.Session["intIdVictima"].ToString())) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.ddlVereda.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                this.ddlAreaPoblacional.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                this.txtSitioAccidente.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][6].ToString();
                this.txtCondicion.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][7].ToString();
                this.ddlEstado.SelectedValue = this.blPaicma.myDataSet.Tables[0].Rows[0][8].ToString();
                this.txtActividadAccidente.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][10].ToString();
                this.txtDiagnosticoNuevo.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][20].ToString();
                this.txtDescripcionHechos.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][21].ToString();
                this.txtSeguimiento.Text = this.blPaicma.myDataSet.Tables[0].Rows[0][22].ToString();
                if (this.blPaicma.myDataSet.Tables[0].Rows[0][11].ToString() == "SI")
                {
                    this.chkVictima1.Items[0].Selected = true;
                }
                else
                {
                    this.chkVictima1.Items[0].Selected = false;
                }
                if (this.blPaicma.myDataSet.Tables[0].Rows[0][12].ToString() == "SI")
                {
                    this.chkVictima1.Items[1].Selected = true;
                }
                else
                {
                    this.chkVictima1.Items[1].Selected = false;
                }
                if (this.blPaicma.myDataSet.Tables[0].Rows[0][13].ToString() == "SI")
                {
                    this.chkVictima1.Items[2].Selected = true;
                }
                else
                {
                    this.chkVictima1.Items[2].Selected = false;
                }
                if (this.blPaicma.myDataSet.Tables[0].Rows[0][14].ToString() == "SI")
                {
                    this.chkVictima1.Items[3].Selected = true;
                }
                else
                {
                    this.chkVictima1.Items[3].Selected = false;
                }
                if (this.blPaicma.myDataSet.Tables[0].Rows[0][15].ToString() == "SI")
                {
                    this.chkVictima1.Items[4].Selected = true;
                }
                else
                {
                    this.chkVictima1.Items[4].Selected = false;
                }
                if (this.blPaicma.myDataSet.Tables[0].Rows[0][16].ToString() == "SI")
                {
                    this.chkVictima2.Items[0].Selected = true;
                }
                else
                {
                    this.chkVictima2.Items[0].Selected = false;
                }
                if (this.blPaicma.myDataSet.Tables[0].Rows[0][17].ToString() == "SI")
                {
                    this.chkVictima2.Items[1].Selected = true;
                }
                else
                {
                    this.chkVictima2.Items[1].Selected = false;
                }
                if (this.blPaicma.myDataSet.Tables[0].Rows[0][18].ToString() == "SI")
                {
                    this.chkVictima2.Items[2].Selected = true;
                }
                else
                {
                    this.chkVictima2.Items[2].Selected = false;
                }
                if (this.blPaicma.myDataSet.Tables[0].Rows[0][19].ToString() == "SI")
                {
                    this.chkVictima2.Items[3].Selected = true;
                }
                else
                {
                    this.chkVictima2.Items[3].Selected = false;
                }
            }
            this.blPaicma.Termina();
        }
    }
    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        if (this.Session["Operacion"].ToString() == null)
        {
            string url = "frmVictima.aspx";
            base.Response.Redirect(url);
            return;
        }
        if (this.Session["Operacion"].ToString() == "Actualizar")
        {
            if (this.fntValidarCamposGenerales_bol())
            {
                this.fntActualizarDatosGenerales();
                return;
            }
        }
        else if (this.Session["Operacion"].ToString() == "VerDatosAccidente")
        {
            if (this.fntValidarCamposAccidente_bol())
            {
                this.fntActualizarDatosAcidente();
                return;
            }
        }
        else if (this.Session["Operacion"].ToString() == "Crear")
        {
            if (this.fntValidarCamposGenerales_bol())
            {
                this.fntIngresarDatosGeneralesVictima();
                return;
            }
        }
        else if (this.Session["Operacion"].ToString() == "CrearDatosAccidente" && this.fntValidarCamposAccidente_bol())
        {
            this.fntIngresarDatosAccidenteVictima();
        }
    }
    public bool ftnValidarSeguridadCampos_bol(string strTextoCampo)
    {
        bool result = false;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            this.blPaicma.fntEliminarTablaExpresionCampo_bol();
            if (this.blPaicma.fntIngresarExpresionCampo_bol(strTextoCampo) && this.blPaicma.fntConsultaExpresionSeguridad("strDsSeguridadCampos", strTextoCampo))
            {
                int num = Convert.ToInt32(this.blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                result = (num <= 0);
            }
            this.blPaicma.Termina();
        }
        return result;
    }
    public bool fntValidarCamposGenerales_bol()
    {
        if (this.ddlTipoIdentificacion.SelectedValue == "Seleccione...")
        {
            this.lblError.Text = "Debe seleccionar el tipo de identificación.";
            return false;
        }
        if (this.txtNoIdentificacion.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar el Número de identificación.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtNoIdentificacion.Text.Trim()))
        {
            this.lblError.Text = "El campo de número de identgificación tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
            return false;
        }
        if (this.txtNombres.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar el Nombre de la Victima.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtNombres.Text.Trim()))
        {
            this.lblError.Text = "El campo de Nombre tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
            return false;
        }
        if (this.txtApellido.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar el Apellido de la Victima.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtApellido.Text.Trim()))
        {
            this.lblError.Text = "El campo de Apellido tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
            return false;
        }
        if (this.txttelefono.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar el Telefono de la Victima.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txttelefono.Text.Trim()))
        {
            this.lblError.Text = "El campo de Teléfono tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
            return false;
        }
        if (this.txtDireccion.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar la direccion de contacto de la Victima.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtDireccion.Text.Trim()))
        {
            this.lblError.Text = "El campo de Dirección tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
            return false;
        }
        if (this.txtFechaNacimiento.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar la Fecha de Nacimiento de contacto de la Victima.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtFechaNacimiento.Text.Trim()))
        {
            this.lblError.Text = "El campo de Fecha de Nacimiento tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
            return false;
        }
        if (this.txtEdad.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar la Edad de la Victima.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtEdad.Text.Trim()))
        {
            this.lblError.Text = "El campo de Edad tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
            return false;
        }
        if (this.ddlGenero.SelectedValue == "Seleccione...")
        {
            this.lblError.Text = "Debe seleccionar el Genero de la victima.";
            return false;
        }
        if (this.ddlEtnia.SelectedValue == "Seleccione...")
        {
            this.lblError.Text = "Debe seleccionar la Etnia de la victima.";
            return false;
        }
        return true;
    }
    public bool fntActualizarDatosGenerales()
    {
        bool result = false;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntModificarDatosGeneralesVictima_bol(Convert.ToInt32(this.Session["intIdVictima"].ToString()), Convert.ToInt32(this.ddlTipoIdentificacion.SelectedValue.ToString()), this.txtNoIdentificacion.Text.Trim(), this.txtNombres.Text.Trim(), this.txtApellido.Text.Trim(), this.txttelefono.Text.Trim(), this.txtDireccion.Text.Trim(), this.txtFechaNacimiento.Text.Trim(), Convert.ToInt32(this.txtEdad.Text.Trim()), Convert.ToInt32(this.ddlGenero.SelectedValue.ToString()), Convert.ToInt32(this.ddlEtnia.SelectedValue.ToString())))
            {
                this.lblRespuesta.Text = "Registro actualizado satisfactoriamente";
                this.Session["Operacion"] = "ActualizarOK";
            }
            else
            {
                this.lblRespuesta.Text = "Problema al actualizar el registro, por favor comuníquese con el administrador.";
                this.Session["Operacion"] = "ActualizarNO";
            }
            this.fntInactivarPaneles();
            this.pnlRespuesta.Visible = true;
            this.blPaicma.Termina();
        }
        return result;
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        if (this.Session["Operacion"] == null)
        {
            string url = "frmVictima.aspx";
            base.Response.Redirect(url);
            return;
        }
        this.fntInactivarPaneles();
        if (this.Session["Operacion"].ToString() == "ActualizarOK")
        {
            this.Session["Operacion"] = "Actualizar";
            string url2 = "frmVictima.aspx";
            base.Response.Redirect(url2);
            return;
        }
        if (this.Session["Operacion"].ToString() == "ActualizarNO")
        {
            this.Session["Operacion"] = "Actualizar";
            this.pnlAccionesVictima.Visible = true;
            this.pnlVictimaNuevo.Visible = true;
            return;
        }
        if (this.Session["Operacion"].ToString() == "VerDatosAccidenteOK")
        {
            this.Session["Operacion"] = "Actualizar";
            this.pnlAccionesVictima.Visible = true;
            this.pnlVictimaNuevo.Visible = true;
            this.fntCargarGrillaAccidenteVictima(Convert.ToInt32(this.Session["intIdVictima"].ToString()));
            return;
        }
        if (this.Session["Operacion"].ToString() == "VerDatosAccidenteNO")
        {
            this.Session["Operacion"] = "VerDatosAccidente";
            this.pnlDetalleDatosVictima.Visible = true;
            return;
        }
        if (this.Session["Operacion"].ToString() == "CrearOK")
        {
            this.pnlVictimaNuevo.Visible = true;
            this.pnlAccionesVictima.Visible = true;
            this.imgbGravar.Visible = false;
            this.imgbNuevoDatosAccidente.Visible = true;
            return;
        }
        if (this.Session["Operacion"].ToString() == "CrearNO")
        {
            this.Session["Operacion"] = "Crear";
            this.pnlVictimaNuevo.Visible = true;
            this.pnlAccionesVictima.Visible = true;
            return;
        }
        if (this.Session["Operacion"].ToString() == "CrearDatosAccidenteOK")
        {
            this.pnlVictimaNuevo.Visible = true;
            this.pnlAccionesVictima.Visible = true;
            this.imgbGravar.Visible = true;
            this.imgbNuevoDatosAccidente.Visible = false;
            this.fntCargarGrillaAccidenteVictima(Convert.ToInt32(this.Session["intIdVictima"].ToString()));
            this.Session["Operacion"] = "Actualizar";
            this.tblDatosAccidente.Visible = true;
            return;
        }
        this.pnlDetalleDatosVictima.Visible = true;
        this.pnlAccionesVictima.Visible = true;
    }
    public bool fntValidarCamposAccidente_bol()
    {
        if (this.ddlVereda.SelectedValue == "Seleccione...")
        {
            this.lblError.Text = "Debe seleccionar la vereda.";
            return false;
        }
        if (this.ddlAreaPoblacional.SelectedValue == "Seleccione...")
        {
            this.lblError.Text = "Debe seleccionar el Area Poblacional.";
            return false;
        }
        if (this.txtSitioAccidente.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar el Sitio del Accidente.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtSitioAccidente.Text.Trim()))
        {
            this.lblError.Text = "El campo de Sitio de Accidente tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
            return false;
        }
        if (this.txtCondicion.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar la condicion de la victima.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtCondicion.Text.Trim()))
        {
            this.lblError.Text = "El campo de Condición tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
            return false;
        }
        if (this.ddlEstado.SelectedValue == "Seleccione...")
        {
            this.lblError.Text = "Debe seleccionar el Estado de la victima.";
            return false;
        }
        if (this.txtActividadAccidente.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar la actividad que realizaba la víctima en el momento del accidente.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtActividadAccidente.Text.Trim()))
        {
            this.lblError.Text = "El campo de Actividad Accidente tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
            return false;
        }
        if (this.txtDiagnosticoNuevo.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar el Diagnóstico Médico de la víctima.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtDiagnosticoNuevo.Text.Trim()))
        {
            this.lblError.Text = "El campo de Diagnóstico Médico tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
            return false;
        }
        if (this.txtDescripcionHechos.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar la descripción de los hechos del accidente.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtDescripcionHechos.Text.Trim()))
        {
            this.lblError.Text = "El campo de Descripción de los Hechos tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
            return false;
        }
        if (this.txtSeguimiento.Text.Trim() == string.Empty)
        {
            this.lblError.Text = "Debe digitar el seguimiento realizado a la víctima.";
            return false;
        }
        if (!this.ftnValidarSeguridadCampos_bol(this.txtSeguimiento.Text.Trim()))
        {
            this.lblError.Text = "El campo de Seguimiento tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
            return false;
        }
        return true;
    }
    public bool fntActualizarDatosAcidente()
    {
        bool result = false;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            int intAmputacionInicial = 0;
            int intAmputacionFin = 0;
            int intHerida = 0;
            int intEsquirla = 0;
            int intQuemadura = 0;
            int intFractura = 0;
            int intInfeccion = 0;
            int intAfectacionVisual = 0;
            int intAfectacionAuditiva = 0;
            if (this.chkVictima1.Items[0].Selected)
            {
                intAmputacionInicial = 1;
            }
            if (this.chkVictima1.Items[1].Selected)
            {
                intAmputacionFin = 1;
            }
            if (this.chkVictima1.Items[2].Selected)
            {
                intHerida = 1;
            }
            if (this.chkVictima1.Items[3].Selected)
            {
                intEsquirla = 1;
            }
            if (this.chkVictima1.Items[4].Selected)
            {
                intQuemadura = 1;
            }
            if (this.chkVictima2.Items[0].Selected)
            {
                intFractura = 1;
            }
            if (this.chkVictima2.Items[1].Selected)
            {
                intInfeccion = 1;
            }
            if (this.chkVictima2.Items[2].Selected)
            {
                intAfectacionVisual = 1;
            }
            if (this.chkVictima2.Items[3].Selected)
            {
                intAfectacionAuditiva = 1;
            }
            if (this.blPaicma.fntModificarDatosAccidenteVictima_bol(Convert.ToInt32(this.Session["intIdVictima"].ToString()), Convert.ToInt32(this.ddlVereda.SelectedValue.ToString()), Convert.ToInt32(this.ddlAreaPoblacional.SelectedValue.ToString()), this.txtSitioAccidente.Text.Trim(), this.txtCondicion.Text.Trim(), Convert.ToInt32(this.ddlEstado.SelectedValue.ToString()), this.txtActividadAccidente.Text.Trim(), intAmputacionInicial, intAmputacionFin, intHerida, intEsquirla, intQuemadura, intFractura, intInfeccion, intAfectacionVisual, intAfectacionAuditiva, this.txtDiagnosticoNuevo.Text.Trim(), this.txtDescripcionHechos.Text.Trim(), this.txtSeguimiento.Text.Trim()))
            {
                this.lblRespuesta.Text = "Registro actualizado satisfactoriamente";
                this.Session["Operacion"] = "VerDatosAccidenteOK";
            }
            else
            {
                this.lblRespuesta.Text = "Problema al actualizar el registro, por favor comuníquese con el administrador.";
                this.Session["Operacion"] = "VerDatosAccidenteNO";
            }
            this.fntInactivarPaneles();
            this.pnlRespuesta.Visible = true;
            this.blPaicma.Termina();
        }
        return result;
    }
    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles();
        this.Session["Operacion"] = "Crear";
        this.pnlAccionesVictima.Visible = true;
        this.imgbEncontrar.Visible = false;
        this.imgbEliminar.Visible = false;
        this.pnlVictimaNuevo.Visible = true;
        this.tblDatosAccidente.Visible = false;
        this.fntCargarObjetos();
    }
    public bool fntIngresarDatosGeneralesVictima()
    {
        bool result = false;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntIngresarDatosGeneralesVictima_bol(Convert.ToInt32(this.ddlTipoIdentificacion.SelectedValue.ToString()), Convert.ToInt32(this.txtNoIdentificacion.Text.Trim()), this.txtNombres.Text.Trim(), this.txtApellido.Text.Trim(), this.txttelefono.Text.Trim(), this.txtDireccion.Text.Trim(), this.txtFechaNacimiento.Text.Trim(), Convert.ToInt32(this.txtEdad.Text.Trim()), Convert.ToInt32(this.ddlGenero.SelectedValue.ToString()), Convert.ToInt32(this.ddlEtnia.SelectedValue.ToString()), Convert.ToInt32(this.ddlNivelEducacion.SelectedValue.ToString())))
            {
                this.lblRespuesta.Text = "Registro ingresado satisfactoriamente";
                this.Session["Operacion"] = "CrearOK";
            }
            else
            {
                this.lblRespuesta.Text = "Problema al Ingresar el registro, por favor comuníquese con el administrador.";
                this.Session["Operacion"] = "CrearNO";
            }
            this.fntInactivarPaneles();
            this.pnlRespuesta.Visible = true;
            this.blPaicma.Termina();
        }
        return result;
    }
    protected void imgbNuevoDatosAccidente_Click(object sender, ImageClickEventArgs e)
    {
        this.fntInactivarPaneles();
        this.pnlAccionesVictima.Visible = true;
        this.imgbGravar.Visible = true;
        this.imgbNuevoDatosAccidente.Visible = false;
        this.pnlDetalleDatosVictima.Visible = true;
        this.Session["Operacion"] = "CrearDatosAccidente";
        this.fntConsultarDatosVictimaAcabadaDeCrear_bol();
        this.fntCargarDatosVictima();
    }
    public bool fntConsultarDatosVictimaAcabadaDeCrear_bol()
    {
        bool result = false;
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntConsultaDatosVictimaAcabadaDeCrear_bol("strDsVictima", Convert.ToInt32(this.ddlTipoIdentificacion.SelectedValue.ToString()), Convert.ToInt32(this.txtNoIdentificacion.Text.Trim()), this.txtNombres.Text.Trim(), this.txtApellido.Text.Trim(), this.txttelefono.Text.Trim(), this.txtDireccion.Text.Trim(), this.txtFechaNacimiento.Text.Trim(), Convert.ToInt32(this.txtEdad.Text.Trim()), Convert.ToInt32(this.ddlGenero.SelectedValue.ToString()), Convert.ToInt32(this.ddlEtnia.SelectedValue.ToString()), Convert.ToInt32(this.ddlNivelEducacion.SelectedValue.ToString())) && this.blPaicma.myDataSet.Tables[0].Rows.Count > 0)
            {
                this.Session["intIdVictima"] = this.blPaicma.myDataSet.Tables[0].Rows[0][0].ToString();
                result = true;
            }
            this.blPaicma.Termina();
        }
        return result;
    }
    public bool fntIngresarDatosAccidenteVictima()
    {
        bool result = false;
        int intAmputacionInicial = 0;
        int intAmputacionFin = 0;
        int intHerida = 0;
        int intEsquirla = 0;
        int intQuemadura = 0;
        int intFractura = 0;
        int intInfeccion = 0;
        int intAfectacionVisual = 0;
        int intAfectacionAuditiva = 0;
        if (this.chkVictima1.Items[0].Selected)
        {
            intAmputacionInicial = 1;
        }
        if (this.chkVictima1.Items[1].Selected)
        {
            intAmputacionFin = 1;
        }
        if (this.chkVictima1.Items[2].Selected)
        {
            intHerida = 1;
        }
        if (this.chkVictima1.Items[3].Selected)
        {
            intEsquirla = 1;
        }
        if (this.chkVictima1.Items[4].Selected)
        {
            intQuemadura = 1;
        }
        if (this.chkVictima2.Items[0].Selected)
        {
            intFractura = 1;
        }
        if (this.chkVictima2.Items[1].Selected)
        {
            intInfeccion = 1;
        }
        if (this.chkVictima2.Items[2].Selected)
        {
            intAfectacionVisual = 1;
        }
        if (this.chkVictima2.Items[3].Selected)
        {
            intAfectacionAuditiva = 1;
        }
        if (this.blPaicma.inicializar(this.blPaicma.conexionSeguridad))
        {
            if (this.blPaicma.fntIngresarDatosAccidenteVictima_bol(Convert.ToInt32(this.Session["intIdVictima"].ToString()), Convert.ToInt32(this.ddlVereda.SelectedValue.ToString()), Convert.ToInt32(this.ddlAreaPoblacional.SelectedValue.ToString()), this.txtSitioAccidente.Text.Trim(), this.txtCondicion.Text.Trim(), Convert.ToInt32(this.ddlEstado.SelectedValue.ToString()), this.txtActividadAccidente.Text.Trim(), intAmputacionInicial, intAmputacionFin, intHerida, intEsquirla, intQuemadura, intFractura, intInfeccion, intAfectacionVisual, intAfectacionAuditiva, this.txtDiagnosticoNuevo.Text.Trim(), this.txtDescripcionHechos.Text.Trim(), this.txtSeguimiento.Text.Trim()))
            {
                this.lblRespuesta.Text = "Registro ingresado satisfactoriamente";
                this.Session["Operacion"] = "CrearDatosAccidenteOK";
            }
            else
            {
                this.lblRespuesta.Text = "Problema al Ingresar el registro, por favor comuníquese con el administrador.";
                this.Session["Operacion"] = "CrearDatosAccidenteNO";
            }
            this.fntInactivarPaneles();
            this.pnlRespuesta.Visible = true;
            this.blPaicma.Termina();
        }
        return result;
    }
}