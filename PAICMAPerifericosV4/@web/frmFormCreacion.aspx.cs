﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class _web_frmFormCreacion : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();
    List<Control> UIControles;
    List<string> ListaNombreCampos = new List<string>();
    List<string> ListaIDCampos = new List<string>();
    List<string> ListaValores = new List<string>();
    List<string> ListaCondicion = new List<string>();
    public DataTable ListadoCorto = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {

                ftnValidarPermisos();
                fntCargaPrimerFiltro();



            }
        }
    }


    public void ftnValidarPermisos()
    {
        //int intIdFormulario = 0;
        string strBuscar = string.Empty;
        string strNuevo = string.Empty;
        string strEditar = string.Empty;
        string strEliminar = string.Empty;

        //obtiene el nombre de la pagina actual
        string[] strRutaPagina = HttpContext.Current.Request.RawUrl.Split('/');
        string strNombrePagina = strRutaPagina[strRutaPagina.GetUpperBound(0)];
        strRutaPagina = strNombrePagina.Split('?');
        strNombrePagina = strRutaPagina[strRutaPagina.GetLowerBound(0)];
        //fin obtiene el nombre de la pagina actual

        if (strNombrePagina != string.Empty)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                //intIdFormulario = Convert.ToInt32(Request.QueryString["op"].ToString());
                //Session["intIdFormulario"] = intIdFormulario;
                if (blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(Session["IdUsuario"].ToString()), strNombrePagina))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strNuevo = blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();

                        if (strNuevo == "False")
                        {
                            //imgbGuardar.Visible = false;
                        }
                    }
                }

                blPaicma.Termina();
            }
        }
        else
        {
            Response.Redirect("@dmin/frmLogin.aspx");
        }

    }

    public void fntCargarTablas(string filtroSegundo)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTablaAdm("strDsGrillaTablasForm", filtroSegundo, Convert.ToInt32(Session["idUsuario"])))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlOpciones.DataMember = "strDsGrillaTablasForm";
                    ddlOpciones.DataSource = blPaicma.myDataSet;
                    ddlOpciones.DataValueField = "admTab_Id";
                    ddlOpciones.DataTextField = "admTab_NombreTabla";
                    ddlOpciones.DataBind();
                    ddlOpciones.Items.Insert(ddlOpciones.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargaPrimerFiltro()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaFiltroPrimer("opcionesFiltroPrimario"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlOpcionesFiltroOrg.DataMember = "opcionesFiltroPrimario";
                    ddlOpcionesFiltroOrg.DataSource = blPaicma.myDataSet;
                    ddlOpcionesFiltroOrg.DataValueField = "id";
                    ddlOpcionesFiltroOrg.DataTextField = "primerFil_Descripcion";
                    ddlOpcionesFiltroOrg.DataBind();
                    ddlOpcionesFiltroOrg.Items.Insert(ddlOpcionesFiltroOrg.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargaSegundoFiltro(string primerFiltro)
    {
        if (primerFiltro != "Seleccione...")
        {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
                if (blPaicma.fntConsultaSegundoFiltro("opcionesFiltroSecundario", primerFiltro))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        ddlOpcionesFiltroDep.DataMember = "opcionesFiltroSecundario";
                        ddlOpcionesFiltroDep.DataSource = blPaicma.myDataSet;
                        ddlOpcionesFiltroDep.DataValueField = "id";
                        ddlOpcionesFiltroDep.DataTextField = "segundoFil_Descripcion";
                        ddlOpcionesFiltroDep.DataBind();
                        ddlOpcionesFiltroDep.Items.Insert(ddlOpcionesFiltroDep.Attributes.Count, "Seleccione...");
                    }
                }
            }
            blPaicma.Termina();
        }
    }


    public void ftnInactivarControles(Boolean bolValor)
    {
        pnlTitulo.Visible = bolValor;
        pnlAccionesCargaDinamica.Visible = bolValor;
        //pnlEliminacion.Visible = bolValor;
        pnlNuevaCarga.Visible = bolValor;
    }

    //protected void imgbGuardar_Click(object sender, ImageClickEventArgs e)
    //{
    //    //ftnInactivarControles(false);
    //    //pnlEliminacion.Visible = true;
    //    //Session["Operacion"] = "Crear";
    //    //lblEliminacion.Text = "¿Está seguro de crear la tabla " + ddlOpciones.SelectedItem.ToString() + "?";

    //    ListaNombreCampos = new List<string>((List<string>)Session["ListaNombreCampos"]);
    //    ListaIDCampos = new List<string>((List<string>)Session["ListaIDCampos"]);
    //    UIControles = new List<Control>((List<Control>)Session["Controles"]);
    //    string CadenaCampos = "";
    //    string CadenaValores = "";
    //    string CadenaCondicion = "";


    //    for (int i = 0; i < ListaIDCampos.Count(); i++)
    //    {

    //        string nombrecontrol = ListaIDCampos[i].ToString();
    //        Control texboxs = CustomUITable.FindControl(nombrecontrol);
    //    }

    //}


    //protected void btnSi_Click(object sender, EventArgs e)
    //{
        
    
    //}

    //protected void btnNo_Click(object sender, EventArgs e)
    //{
    //    ftnInactivarControles(false);
    //    pnlNuevaCarga.Visible = true;
    //}

    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        //string strRuta = "frmCargaDinamica.aspx?op=" + Session["intIdFormulario"].ToString();
        string strRuta = "frmFormCreacion.aspx";
        DivTable.Visible = false;
        Response.Redirect(strRuta);
    }

    protected void ddlOpciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<string> listaDeCamposTipoMunicipio = new List<string>();
        string nombreMunicipio = string.Empty;
        string abrevituraTabla = string.Empty;
        bool tieneMunicipio = false;
        List<string> ListaDeCampos = new List<string>();
        bool resultado = false;
        lblMensaje.Visible = false;
        if (ddlOpciones.SelectedValue.ToString() == "Seleccione...")
        {
            pnlAccionesCargaDinamica.Visible = false;
            DivTable.Visible = false;
        }
        else
        {
            pnlAccionesCargaDinamica.Visible = true;
            //fuCargarArchivos.Visible = true;

            string strTablaTemporal = ddlOpciones.SelectedItem.ToString();
            int idTablaBuscar = Convert.ToInt32(ddlOpciones.SelectedValue);
            Session["RelacionadoConVictima"] = false;
            Session["CampoVictimaBuscar"] = "";
            txtFechaHasta.Text = "";
            txtFechaDesde.Text = "";
            //Buscar aqui info de la tabla para saber si tiene asociada una victima

            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultaTodoPorId("resultTablaBuscar", "Form_AdmTablas", "admTab_Id", Convert.ToInt32(idTablaBuscar), "admTab_Id"))
                {

                    DataTable resultadobusqueda = new DataTable();
                    resultadobusqueda = blPaicma.myDataSet.Tables["resultTablaBuscar"];
                    resultado = Convert.ToBoolean(blPaicma.myDataSet.Tables["resultTablaBuscar"].Rows[0]["admTab_RelVictima"]);
                    abrevituraTabla = blPaicma.myDataSet.Tables["resultTablaBuscar"].Rows[0]["admTab_AbreviaturaTabla"].ToString();
                    Session["RelacionadoConVictima"] = resultado;
                    Session["CampoVictimaBuscar"] = abrevituraTabla + "_IdVictimaRel";
                    Session["abreviaturaTablaActual"] = abrevituraTabla;
                }
                if (blPaicma.fntConsultaDetalleCamposLista("strDetalleCampos", "Form_CampoTabla", idTablaBuscar))
                {
                    DataTable dtTableCampos;
                    dtTableCampos = blPaicma.myDataSet.Tables["strDetalleCampos"];
                    int tipoDeDato;
                    for (int j = 0; j < dtTableCampos.Rows.Count; j++)
                    {
                        tipoDeDato = Convert.ToInt32(blPaicma.myDataSet.Tables["strDetalleCampos"].Rows[j]["camForm_TipoControl"]);
                        if (tipoDeDato == 7)
                        {
                            tieneMunicipio = true;
                        }
                       
                    }
                    if (resultado)
                    {
                        ListaDeCampos.Add("idVictimaSP");
                        ListaDeCampos.Add("NombreVictimaSP");
                        ListaDeCampos.Add("ApellidoVictimaSP");

                    }
                    if (tieneMunicipio)
                    {
                        ListaDeCampos.Add("nomDepartamentoSP");
                        ListaDeCampos.Add("nomMunicipioSP");

                    }
                    ListaDeCampos.Add("id");
                    string valorAgregar = "";
                    
                    for (int j = 0; j < dtTableCampos.Rows.Count; j++)
                    {
                        valorAgregar = blPaicma.myDataSet.Tables["strDetalleCampos"].Rows[j]["camForm_NombreCampo"].ToString();
                        tipoDeDato = Convert.ToInt32(blPaicma.myDataSet.Tables["strDetalleCampos"].Rows[j]["camForm_TipoControl"]);
                        if (tipoDeDato == 7)
                        {
                            nombreMunicipio = valorAgregar;
                        }
                        else
                        {
                            ListaDeCampos.Add(valorAgregar);
                        }
                    }
                    
                }

                if (blPaicma.fntConsultaDatosFormCompleto("strDatosFormCargados", strTablaTemporal, resultado, tieneMunicipio, nombreMunicipio, abrevituraTabla))
                {
                    TableRow tb = new TableRow();
                    DataTable Listado = new DataTable();
                    DataTable ListadoCorto = new DataTable();
                    DataTable ListadoFinal = new DataTable();
                    Listado = blPaicma.myDataSet.Tables["strDatosFormCargados"];
                    ListadoCorto = Listado.Copy();
                    int i = 0;
                  // En esta parte esta todo la logica para renderizar solo las columnas en la tabla que estan marcadas para filtrar.
                    int k = 0;
                    string aComparar;

                    if (resultado)
                    {
                        ListadoCorto.Columns.Remove(abrevituraTabla+"_IdVictimaRel");

                    }
                    if (tieneMunicipio)
                    {
                        ListadoCorto.Columns.Remove(abrevituraTabla + "_" + "idDepartamentoRelacion");
                        ListadoCorto.Columns.Remove(abrevituraTabla + "_" + nombreMunicipio);

                    }
                    for (k = 0; k < ListaDeCampos.Count; k++)
                    {
                        for (i = k; i < ListadoCorto.Columns.Count; i++){

                            if (ListaDeCampos[k].ToString() == "id" || ListaDeCampos[k].ToString() == "fechaCarga")
                            {
                                aComparar = ListaDeCampos[k].ToString();
                            }
                            else
                            {
                                aComparar = Session["abreviaturaTablaActual"].ToString() + "_" + ListaDeCampos[k].ToString();
                            }
                            if (ListadoCorto.Columns[i].ColumnName.ToString() != aComparar)
                            {
                                ListadoCorto.Columns.Remove(ListadoCorto.Columns[i].ColumnName.ToString());
                                i--;
                            }
                            else
                            {
                                if (k + 1 < ListaDeCampos.Count)
                                {
                                    break;
                                }
                            }
                        }
                    }
                    gvDatos.DataSource = ListadoCorto;
                    gvDatos.DataBind();
                    DivTable.Visible = true;
                     
                }
            }
        }
    }




    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        if (ddlOpciones.SelectedValue.ToString() == "Seleccione...")
        {
            lblMensaje.Text = "Debe Seleccionar un template para crear un nuevo Formulario";
            lblMensaje.Visible = true;
        }
        else
        {
            int intTablaTemporal = Convert.ToInt32(ddlOpciones.SelectedValue.ToString());
            string strRuta = "";
            string NombreTabla = ddlOpciones.SelectedItem.ToString();
            Session["VictimaID"] = "";
            Session["VictimaNombre"] = "";
            Session["VictimaApellido"] = "";
            Session["VictimaEstado"] ="";
            Session["VictimaDepartamento"] = "";
            Session["VictimaMunicipio"] = "";
            Session["IMSMA"] = "";
            Session["intTablaSeleccionada"] = intTablaTemporal;
            Session["strTablaSeleccionada"] = NombreTabla;
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultaTodoPorId("dataTablaAdm", "Form_AdmTablas", "admTab_Id", intTablaTemporal, "admTab_NombreTabla"))
                {
                    bool resultado = Convert.ToBoolean(blPaicma.myDataSet.Tables["dataTablaAdm"].Rows[0]["admTab_RelVictima"].ToString());
                    if (resultado)
                    {
                        strRuta = "frmPickaVictim.aspx";
                        Response.Redirect(strRuta);
                    }
                    else
                    {
                        strRuta = "frmFillNewForm.aspx";
                        Response.Redirect(strRuta);
                    }
                }
                else
                {
                    strRuta = "frmFillNewForm.aspx";
                    Response.Redirect(strRuta);
                }
            }

        }

    }


    public int GetColumnIndexByName(GridViewRow row, string columnName)
    {
        int columnIndex = 0;
        foreach (DataControlFieldCell cell in row.Cells)
        {
            if (cell.ContainingField is BoundField)
                if (((BoundField)cell.ContainingField).DataField.Equals(columnName))
                    break;
            columnIndex++; // keep adding 1 while we don't have the correct name
        }
        return columnIndex;
    }



   protected void gvDatos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //gvTablas.Columns[0].Visible = true;
        if (e.CommandName == "DetalleFormulario")
        {
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvDatos.Rows[intIndex];
            int valorBuscar = GetColumnIndexByName(selectedRow, "id");
            TableCell Item = selectedRow.Cells[valorBuscar];
            int intIdTabla = Convert.ToInt32(Item.Text);
            Session["indice"] = intIdTabla;
            Session["strTablaSeleccionada"] = ddlOpciones.SelectedItem.ToString();
            Session["intTablaSeleccionada"] =  Convert.ToInt32(ddlOpciones.SelectedValue.ToString());
            string strRuta = "frmModifyForm.aspx";
            Response.Redirect(strRuta);
            //imgbEditar.Visible = true;
        }
        gvDatos.Columns[0].Visible = false;
    }
    protected void ddlOpcionesFiltroOrg_SelectedIndexChanged(object sender, EventArgs e)
     {
         if (ddlOpcionesFiltroOrg.SelectedValue.ToString() != "Seleccione...") { 
             ddlOpciones.Items.Clear();
             ddlOpciones.DataBind();
             ddlOpcionesFiltroDep.Items.Clear();
             ddlOpcionesFiltroDep.DataBind();
             fntCargaSegundoFiltro(ddlOpcionesFiltroOrg.SelectedValue);
             ddlOpcionesFiltroDep.Enabled = true;
             ddlOpciones.Enabled = false;
         }
         else
         {
             ddlOpcionesFiltroDep.Items.Clear();
             ddlOpciones.Items.Clear();
             txtFechaHasta.Text = "";
             txtFechaDesde.Text = "";
             DivTable.Visible = false;
         
         }
     }
     protected void ddlOpcionesFiltroDep_SelectedIndexChanged(object sender, EventArgs e)
     {
         if (ddlOpcionesFiltroDep.SelectedValue.ToString() != "Seleccione...")
         {
             ddlOpciones.Items.Clear();
             ddlOpciones.DataBind();
             ddlOpciones.Enabled = false;
             fntCargarTablas(ddlOpcionesFiltroDep.SelectedValue);
             ddlOpciones.Enabled = true;
         }
         else {
             ddlOpciones.Items.Clear();
             txtFechaHasta.Text = "";
             txtFechaDesde.Text = "";
             DivTable.Visible = false;
     
         }
     }

     protected void gvDatos_PreRender(object sender, EventArgs e)
     {
         if (gvDatos.Rows.Count > 0)
         {
             //This replaces <td> with <th> and adds the scope attribute
             gvDatos.UseAccessibleHeader = true;

             //This will add the <thead> and <tbody> elements
             gvDatos.HeaderRow.TableSection = TableRowSection.TableHeader;

             //This adds the <tfoot> element. 
             //Remove if you don't have a footer row
             gvDatos.FooterRow.TableSection = TableRowSection.TableFooter;
         }

     }


protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
{
    List<string> ListaDeCampos = new List<string>();
    lblMensaje.Visible = false;
    bool resultado = false;
     string nombreMunicipio = string.Empty;
     string nombreDepartamento = string.Empty;
        string abrevituraTabla = string.Empty;
        bool tieneMunicipio = false;
    if (ddlOpciones.SelectedValue.ToString() == "Seleccione...")
    {
        pnlAccionesCargaDinamica.Visible = false;
        DivTable.Visible = false;
    }
    else
    {
        pnlAccionesCargaDinamica.Visible = true;
        //fuCargarArchivos.Visible = true;

        string strTablaTemporal = ddlOpciones.SelectedItem.ToString();
        int idTablaBuscar = Convert.ToInt32(ddlOpciones.SelectedValue);
        Session["RelacionadoConVictima"] = false;
        Session["CampoVictimaBuscar"] = "";

        //Buscar aqui info de la tabla para saber si tiene asociada una victima

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {

            if (blPaicma.fntConsultaTodoPorId("resultTablaBuscar", "Form_AdmTablas", "admTab_Id", Convert.ToInt32(idTablaBuscar), "admTab_Id"))
            {
                DataTable resultadobusqueda = new DataTable();
                resultadobusqueda = blPaicma.myDataSet.Tables["resultTablaBuscar"];
                resultado = Convert.ToBoolean(blPaicma.myDataSet.Tables["resultTablaBuscar"].Rows[0]["admTab_RelVictima"]);
                abrevituraTabla = blPaicma.myDataSet.Tables["resultTablaBuscar"].Rows[0]["admTab_AbreviaturaTabla"].ToString();
                Session["RelacionadoConVictima"] = resultado;
                Session["CampoVictimaBuscar"] = abrevituraTabla + "_IdVictimaRel";
                Session["abreviaturaTablaActual"] = abrevituraTabla;
            }

            if (blPaicma.fntConsultaDetalleCamposLista("strDetalleCampos", "Form_CampoTabla", idTablaBuscar))
            {
                DataTable dtTableCampos;
                dtTableCampos = blPaicma.myDataSet.Tables["strDetalleCampos"];
                int tipoDeDato;
                for (int j = 0; j < dtTableCampos.Rows.Count; j++)
                {
                    tipoDeDato = Convert.ToInt32(blPaicma.myDataSet.Tables["strDetalleCampos"].Rows[j]["camForm_TipoControl"]);
                    if (tipoDeDato == 7)
                    {
                        tieneMunicipio = true;
                    }

                }
                if (resultado)
                {
                    ListaDeCampos.Add("idVictimaSP");
                    ListaDeCampos.Add("NombreVictimaSP");
                    ListaDeCampos.Add("ApellidoVictimaSP");

                }
                if (tieneMunicipio)
                {
                    ListaDeCampos.Add("nomDepartamentoSP");
                    ListaDeCampos.Add("nomMunicipioSP");
                }
                ListaDeCampos.Add("id");
                    string valorAgregar = "";
                for (int j = 0; j < dtTableCampos.Rows.Count; j++)
                {
                    valorAgregar = blPaicma.myDataSet.Tables["strDetalleCampos"].Rows[j]["camForm_NombreCampo"].ToString();
                        tipoDeDato = Convert.ToInt32(blPaicma.myDataSet.Tables["strDetalleCampos"].Rows[j]["camForm_TipoControl"]);
                        if (tipoDeDato == 7)
                        {
                            nombreMunicipio = valorAgregar;
                        }
                        else
                        {
                            ListaDeCampos.Add(valorAgregar);
                        }
                  }
                
            }
            if (blPaicma.fntConsultaDatosFormCompletoPorFecha("strDatosFormCargados", strTablaTemporal,txtFechaDesde.Text, txtFechaHasta.Text, resultado, tieneMunicipio, nombreMunicipio, abrevituraTabla ))
            {
                TableRow tb = new TableRow();
                DataTable Listado = new DataTable();
                DataTable ListadoCorto = new DataTable();
                DataTable ListadoFinal = new DataTable();
                Listado = blPaicma.myDataSet.Tables["strDatosFormCargados"];
                ListadoCorto = Listado.Copy();
                
                int i = 0;
                // En esta parte esta todo la logica para renderizar solo las columnas en la tabla que estan marcadas para filtrar.
                int k = 0;
                string aComparar;
                if (resultado)
                {
                    ListadoCorto.Columns.Remove(abrevituraTabla + "_IdVictimaRel");

                }
                if (tieneMunicipio)
                {
                    ListadoCorto.Columns.Remove(abrevituraTabla + "_" + "idDepartamentoRelacion");
                    ListadoCorto.Columns.Remove(abrevituraTabla + "_" + nombreMunicipio);

                }
                for (k = 0; k < ListaDeCampos.Count; k++)
                {
                    for (i = k; i < ListadoCorto.Columns.Count; i++)
                    {

                        if (ListaDeCampos[k].ToString() == "id" || ListaDeCampos[k].ToString() == "fechaCarga")
                        {
                            aComparar = ListaDeCampos[k].ToString();
                        }
                        else
                        {
                            aComparar = Session["abreviaturaTablaActual"].ToString() + "_" + ListaDeCampos[k].ToString();
                        }
                        if (ListadoCorto.Columns[i].ColumnName.ToString() != aComparar)
                        {
                            ListadoCorto.Columns.Remove(ListadoCorto.Columns[i].ColumnName.ToString());
                            i--;
                        }
                        else
                        {
                            if (k + 1 < ListaDeCampos.Count)
                            {
                                break;
                            }

                        }
                    }
                }
                gvDatos.DataSource = ListadoCorto;
                gvDatos.DataBind();
                DivTable.Visible = true;

            }
        }
    }
     
}


}