﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmModifyForm.aspx.cs" Inherits="_web_frmModifyFormaspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">
    <div class="Botones">
        <asp:ImageButton ID="ImageButton1" runat="server"
                ImageUrl="~/Images/guardar.png" ToolTip="Buscar"
                OnClick="UpdateButton_Click" />
       <asp:ImageButton ID="imgbCancelar" runat="server" ImageUrl="~/Images/cancelar.png" OnClick="imgbCancelar_Click" />
     

    </div>
     <div class="contacto2" style="width: 88%; float:left" runat="server" id="mensajes" visible="false">
          <asp:Label ID="LabelMsgCorrecto" runat="server" Text="Registro Exitoso" Visible="false"></asp:Label>
    <asp:Label ID="LabelMsgError" runat="server" Text="El registro a fallado" Visible="false"></asp:Label>
     <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>
         </div>

    <asp:Panel ID="PanelFormulario" runat="server" CssClass="contacto">
        <%-- <form id="form1" runat="server">--%>
        <h1>Edición de Formulario <%= Session["strTablaSeleccionada"].ToString() %></h1>


        <asp:Table ID="CustomUITable" runat="server">
        </asp:Table>

        <%--  <asp:Button runat="server" ID="enviar" OnClick="enviar_Click" />
        </form>--%>
    </asp:Panel>
   
    <p>
       <%-- <asp:Button ID="UpdateButton" runat="server" Text="Update" OnClick="UpdateButton_Click"/>--%>
    <%--    &nbsp;&nbsp;&nbsp;
        <asp:Button ID="CancelButton" runat="server" Text="Cancel" CausesValidation="false" />--%>
    </p>

</asp:Content>
<asp:Content ContentPlaceHolderID="scripts" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#' + '<%= ImageButton1.ClientID %>').click(function () {
                if ($(".campo").length) {
                    if ($(".campo").val() == "") {
                        alert("rellene o seleccione todos los campos");
                        return false;

                    };
                };
                if ($(".entero").length) {
                    if ($(".entero").val() == "") {
                        alert("rellene o seleccione todos los campos");
                        return false;

                    };
                };

                if ($(".decimal").length) {
                    if ($(".decimal").val() == "") {
                        alert("rellene o seleccione todos los campos");
                        return false;
                    };
                };

                if ($(".ddMunicipio").length) {
                    if ($(".ddMunicipio").val() == "0" || $(".ddMunicipio").val() == "") {
                        alert("rellene o seleccione todos los campos");
                        return false;
                    }
                };

                if ($(".ddDepartamento").length) {
                    if ($(".ddDepartamento").val() == "0" || $(".ddDepartamento").val() == "") {
                        alert("rellene todos los campos");
                        return false;
                    }
                };
                if ($(".decimal").length) {
                    var decimalescrito = $(".decimal").val();
                    var respuestadecimal = validateDecimal($(".decimal").val());
                    if (respuestadecimal) {

                    } else {
                        alert("Algunos Campos declarados decimales, no cumplen con la condicion establecida");
                        return false;
                    }
                }

            });


            function validateDecimal(dec) {
                var regx = /^\d+\.?\d{0,2}$/g;
                return regx.test(dec);
            }


            $(".entero").keydown(function (event) {
                // Prevent shift key since its not needed
                if (event.shiftKey == true) {
                    event.preventDefault();
                }
                // Allow Only: keyboard 0-9, numpad 0-9, backspace, tab, left arrow, right arrow, delete
                if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) {
                    // Allow normal operation
                } else {
                    // Prevent the rest
                    event.preventDefault();
                }
            });

            $(".ddDepartamento").prepend("<option value='0'></option>");
            $(".ddMunicipio").prepend("<option value='0'></option>");
            $(".ddMunicipio option").each(function () {
                var ordenDep = $(this).parent().data("orden");
                var id = $(this).parent().attr("id");
                var cadenaPrevia = parseInt(id.replace(/[^0-9\.]/g, ''), 10);
                //alert(cadenaPrevia);
                var numero = parseInt(cadenaPrevia, 10) - 1;

                var nombreDepartamentoSustituir = id.replace('comboMunicipio', 'comboDepartamento');
                var nombreDepartamentoCortar = nombreDepartamentoSustituir.replace(cadenaPrevia.toString(), '');
                //alert(numero);
                var idDep = nombreDepartamentoCortar + numero;
               // var seleccionado = $(this).attr("selected");
                var departamento = $("#" + idDep).val();
                var optDep = $(this).data("departamento");
                if (optDep != departamento) {
                    if ($(this).is('option') && (!$(this).parent().is('span')))
                        $(this).wrap((navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') ? '<span>' : null).hide();
                } else {
                    if (navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') {
                        if (this.nodeName.toUpperCase() === 'OPTION') {
                            var span = $(this).parent();
                            var opt = this;
                            if ($(this).parent().is('span')) {
                                $(opt).show();
                                $(span).replaceWith(opt);
                            }
                        }
                    } else {
                        $(this).show(); //all other browsers use standard .show()
                    }
                }
            });

            
            $(".pluginFecha").datepicker({ 
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true 
            });
            $(".ddDepartamento").change(function () {

                var valorDep = $(this).val();
                var ordenDep = $(this).data("orden");
                var id = $(this).attr("id");
                var cadenaAnterior = parseInt(id.replace(/[^0-9\.]/g, ''), 10);
                var numero = parseInt(cadenaAnterior, 10) + 1;
                var nombreMunicipioSustituir = id.replace('comboDepartamento', 'comboMunicipio');
                var nombreMunicipioCortar = nombreMunicipioSustituir.replace(cadenaAnterior.toString(), '');
                //alert(numero);
                var idMun = nombreMunicipioCortar + numero;
                var optDep;


                if (valorDep != "0") {
                    $("#" + idMun + " option").each(function (index, val) {
                        var $el = $(this);
                        if ($el.val() == "0") {
                            $el.attr("selected", true);
                        }
                        optDep = $el.data("departamento")
                        if (optDep != valorDep) {
                            if ($(this).is('option') && (!$(this).parent().is('span')))
                                $(this).wrap((navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') ? '<span>' : null).hide();
                        } else {
                            if (navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') {
                                if (this.nodeName.toUpperCase() === 'OPTION') {
                                    var span = $(this).parent();
                                    var opt = this;
                                    if ($(this).parent().is('span')) {
                                        $(opt).show();
                                        $(span).replaceWith(opt);
                                    }
                                }
                            } else {
                                $(this).show(); //all other browsers use standard .show()
                            }
                        }
                    });
                } else {
                    $("#" + idMun).val("0");
                    $("#" + idMun + " option").each(function () {
                        if ($(this).is('option') && (!$(this).parent().is('span')))
                            $(this).wrap((navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') ? '<span>' : null).hide();
                    });
                }

            });
        })

    </script>
</asp:Content>

