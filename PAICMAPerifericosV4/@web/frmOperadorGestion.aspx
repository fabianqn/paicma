﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmOperadorGestion.aspx.cs" Inherits="_web_frmOperadorGestion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <asp:Panel ID="pnlEliminacion" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblEliminacion" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSi" runat="server"  Text="SI" />
                        <asp:Button ID="btnNo" runat="server"  Text="NO" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

        <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" Text="Aceptar" onclick="btnOk_Click" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlMenuOperadorGestion" runat="server">
        <div class="Botones">
            <asp:ImageButton ID="imgbBuscar" runat="server" 
                ImageUrl="~/Images/BuscarAccion.png" onclick="imgbBuscar_Click" 
                ToolTip="Buscar" />
            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png" 
                Visible="False" onclick="imgbEditar_Click" ToolTip="Editar" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAccionesOperadorGestion" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server" 
                ImageUrl="~/Images/buscar.png" onclick="imgbEncontrar_Click" 
                ToolTip="Ejecutar Consulta" style="width: 32px" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png" 
                Visible="False" onclick="imgbGravar_Click" ToolTip="Guardar" />
            <asp:ImageButton ID="imgbCancelar" runat="server" 
                ImageUrl="~/Images/cancelar.png" onclick="imgbCancelar_Click" 
                ToolTip="Cancelar" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlOperadorGestionNuevo" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblDocumento" runat="server" Text="Documento"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDocumento" runat="server" Enabled="False" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFecha" runat="server" Text="Fecha de Carga"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFechaCarga" runat="server" Enabled="False"></asp:TextBox>
                </td>
            </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblObservaciones" runat="server" Text="Observación"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine" 
                                    Enabled="False" Height="50px" Width="90%"></asp:TextBox>
                            </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblOperador" runat="server" Text="Operador"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtOperador" runat="server" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:ImageButton ID="imbDocumento" runat="server" 
                        ImageUrl="~/Images/buscar.png" onclick="imbDocumento_Click" 
                        ToolTip="Ver" />
                </td>
            </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
            </tr>
                        <tr>
                <td>
                    <asp:Label ID="lblNombreGrupo" runat="server" Text="Grupo"></asp:Label>
                            </td>
                <td>
                    <asp:DropDownList ID="ddlNombreGrupo" runat="server" Visible="False">
                    </asp:DropDownList>
                            </td>
            </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblActividad" runat="server" Text="Actividad a Realizar"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlActividad" runat="server">
                                </asp:DropDownList>
                            </td>
            </tr>
                        <tr>
                <td>
                    <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                            </td>
                <td>
                    <asp:DropDownList ID="ddlEstado" runat="server">
                    </asp:DropDownList>
                            </td>
            </tr>
                        <tr>
                <td>
                    <asp:Label ID="lblObservacionRevision" runat="server" Text="Observación"></asp:Label>
                            </td>
                <td>
                    <asp:TextBox ID="txtObservacionRevision" runat="server" TextMode="MultiLine" 
                        MaxLength="250" Width="90%" Height="50px" placeholder="Escriba una Observacion, el Limite es de 250 Caracteres"></asp:TextBox>
                          <asp:RegularExpressionValidator runat="server" ID="valInput"
    ControlToValidate="txtObservacionRevision"
    ValidationExpression="^[\s\S]{0,250}$"
    ErrorMessage="Ingrese Máximo 250 Caracteres" ForeColor="Red"
    Display="Dynamic">*Ingrese Máximo 250 Caracteresss</asp:RegularExpressionValidator>      
                
                </td>
            </tr>
                        <tr>
                <td>
                    <asp:Label ID="lblDocumentoSeguimiento" runat="server" 
                        Text="Documento de Seguimiento"></asp:Label>
                            </td>
                <td>
                    <asp:FileUpload ID="fuDocumentoSeguimiento" runat="server" />
                            </td>
            </tr>
                        <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
                        <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
                        <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
                        <tr>
                <td colspan="2">
                    <asp:Label ID="lblRuta" runat="server" Visible="false"></asp:Label>
                            </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="PnlOperadorGestionGrilla" runat="server">
        <div class="formulario2">
            <div class ="contacto2">
                <asp:GridView ID="gvOperadorGestion" runat="server" AutoGenerateColumns="False" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="false" 
                    onrowcommand="gvOperadorGestion_RowCommand" OnPreRender="gvOperadorGestion_PreRender"
                    >
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="geop_Id" HeaderText="geop_Id" />
                        <asp:BoundField DataField="NombreDocumento" 
                            HeaderText="Documento" />
                        <asp:BoundField DataField="extDocumento" 
                            HeaderText="Extencion" />
                        <asp:BoundField DataField="fecha" 
                            HeaderText="Fecha" dataformatstring="{0:yyyy-MM-dd HH:mm:ss}" htmlencode="false" />
                        <asp:BoundField DataField="EstadoRevision" 
                            HeaderText="Estado" />
                        <asp:BoundField DataField="usuarioOperador" HeaderText="Usuario" />
                        <asp:BoundField DataField="gruOpe_Nombre" HeaderText="Grupo" />
                        <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                            ImageUrl="~/Images/seleccionar.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
     
                   <div style="text-align:center">
                        <asp:Label ID="lblErrorGv" runat="server"></asp:Label>
                     </div>
              </div>
        </div>
    </asp:Panel>



        <asp:Panel ID="pnlGestionRealizada" runat="server" Visible="False">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvActividadesOperador" runat="server" AutoGenerateColumns="False" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" 
                    onrowcommand="gvActividadesOperador_RowCommand" OnPreRender="gvActividadesOperador_PreRender" >
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="seop_Id" HeaderText="seop_Id" />
                        <asp:BoundField DataField="fechaRevision" 
                            HeaderText="Fecha Revisión" dataformatstring="{0:yyyy-MM-dd HH:mm:ss}" htmlencode="false" />
                        <asp:BoundField DataField="AccionRealizada" 
                            HeaderText="Acción" />
                        <asp:BoundField DataField="seop_NombreDocumento" 
                            HeaderText="Documento" />
                        <asp:BoundField DataField="EstadoRevision" 
                            HeaderText="Estado" />
                        <asp:BoundField DataField="usuarioOperador" HeaderText="Usuario" />
                        <asp:BoundField DataField="ObservacionRevision" HeaderText="Observaciones" />
                        <asp:ButtonField ButtonType="Image" CommandName="Ver" 
                            ImageUrl="~/Images/seleccionar.png" HeaderText="Ver" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorGvActividades" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>




</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#' + '<%= gvOperadorGestion.ClientID %>').DataTable();
            $('#' + '<%= gvActividadesOperador.ClientID %>').DataTable();
    } );
   </script>
</asp:Content>