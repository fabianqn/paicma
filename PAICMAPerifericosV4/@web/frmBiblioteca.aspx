﻿<%@ page title="" language="C#" masterpagefile="~/Plantillas/sisPAICMA.master" autoeventwireup="true" CodeFile="frmBiblioteca.aspx.cs" inherits="_web_frmBiblioteca" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <asp:Panel ID="pnlEliminacion" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblEliminacion" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSi" runat="server"  Text="SI" />
                        <asp:Button ID="btnNo" runat="server"  Text="NO" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" Text="Aceptar" OnClick="btnOk_Click"  />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlMenuBiblioteca" runat="server">
        <div class="Botones">
       <%--     <asp:ImageButton ID="imgbBuscar" runat="server" ImageUrl="~/Images/BuscarAccion.png" ToolTip="Buscar" />--%>
            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png" Visible="False" ToolTip="Editar" OnClick="imgbEditar_Click" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAccionesBiblioteca" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server" ImageUrl="~/Images/buscar.png" ToolTip="Ejecutar Consulta" style="width: 32px" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png" ToolTip="Guardar" OnClick="imgbGravar_Click" />
            <asp:ImageButton ID="imgbCancelar" runat="server" ImageUrl="~/Images/cancelar.png" ToolTip="Cancelar" OnClick="imgbCancelar_Click"/>
            <asp:ImageButton ID="imgbNuevoDocumento" runat="server" ImageUrl="~/Images/NuevoCampo.png" OnClick="imgbNuevoDocumento_Click" ToolTip="Nuevo Documento" Visible="False" />
            <asp:ImageButton ID="imgbEliminar" runat="server" ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlBibliotecaNuevo" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblDocumento" runat="server" Text="Documento"></asp:Label>
                </td>
                <td> 
                    <asp:FileUpload ID="fuDocumento" runat="server" />
                    
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblObservaciones" runat="server" Text="Observación"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine" 
                        MaxLength="250" Width="90%" Height="50px" ></asp:TextBox>
                    <asp:RegularExpressionValidator runat="server" ID="valInput"
    ControlToValidate="txtObservacion"
    ValidationExpression="^[\s\S]{0,250}$"
    ErrorMessage="Ingrese Máximo 250 Caracteres" ForeColor="Red"
    Display="Dynamic">*Ingrese Máximo 250 Caracteres</asp:RegularExpressionValidator>     
                </td>
                <td>
                    &nbsp;</td>
            </tr>
                        <tr>
                <td>
                    <asp:Label ID="lblEstado" runat="server" Text="Grupo" Visible="False"></asp:Label>
                            </td>
                <td colspan="2">
                    <asp:DropDownList ID="ddlEstado" runat="server">
                    </asp:DropDownList>
                            </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCarpeta" runat="server" Text="Carpeta"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="ddlCarpeta" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="PnlBibliotecasDisponiblesGrilla" runat="server">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvBibliotecasAsignadas" runat="server" AutoGenerateColumns="False" width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" AllowPaging="false" OnRowCommand="gvBibliotecasAsignadas_RowCommand" OnPreRender="gvBibliotecasAsignadas_PreRender">
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="bibUsu_Id" HeaderText="bibUsu_Id" />
                        <asp:BoundField DataField="grubib_Id" HeaderText="grubib_Id" />
                        <asp:BoundField DataField="grubib_Nombre" 
                            HeaderText="Biblioteca" />
                        <asp:BoundField DataField="grubib_Descripcion" 
                            HeaderText="Descripción" />
                        <asp:BoundField DataField="bibUsu_FechaCreacion" 
                            HeaderText="Fecha Permiso" dataformatstring="{0:yyyy-MM-dd HH:mm:ss}" htmlencode="false" />
                        <asp:BoundField DataField="est_Descripcion" 
                            HeaderText="Estado" />
                        <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                            ImageUrl="~/Images/seleccionar.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorGv" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>


   <asp:Panel ID="pnlDocumentos" runat="server" Visible="False">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="15" >
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="bibUsu_Id" HeaderText="bibUsu_Id" />
                        <asp:BoundField DataField="grubib_Id" HeaderText="grubib_Id" />
                        <asp:BoundField DataField="grubib_Nombre" HeaderText="Biblioteca" />
                        <asp:BoundField DataField="grubib_Descripcion" HeaderText="Descripción" />
                        <asp:BoundField DataField="bibUsu_FechaCreacion" HeaderText="Fecha Permiso" dataformatstring="{0:yyyy-MM-dd HH:mm:ss}" htmlencode="false" />
                        <asp:BoundField DataField="est_Descripcion" HeaderText="Estado" />
                        <asp:ButtonField ButtonType="Image" CommandName="Seleccion" ImageUrl="~/Images/seleccionar.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlDocumentosBiblioteca" runat="server" Visible="False">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvDocumentosBiblioteca" runat="server" AutoGenerateColumns="False" width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" AllowPaging="false" OnRowCommand="gvDocumentosBiblioteca_RowCommand"  OnPreRender="gvDocumentosBiblioteca_PreRender" >
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="bib_Id" HeaderText="bib_Id" />
                        <asp:BoundField DataField="grubib_Id" HeaderText="grubib_Id" />
                        <asp:BoundField DataField="segusu_Id" 
                            HeaderText="segusu_Id" />
                        <asp:BoundField DataField="bib_NombreDocumento" 
                            HeaderText="Documento" />
                        <asp:BoundField DataField="bib_ExtDocumento" 
                            HeaderText="Extensión" />
                        <asp:BoundField DataField="bib_Observaciones" 
                            HeaderText="Descripción" />
                        <asp:BoundField DataField="bib_FechaCreacion" HeaderText="Fecha Creación" dataformatstring="{0:yyyy-MM-dd HH:mm:ss}" htmlencode="false"  />
                        <asp:BoundField DataField="est_Descripcion" HeaderText="Estado" />
                        <asp:BoundField DataField="segusu_login" HeaderText="Usuario" />
                        <asp:BoundField DataField="bib_Version" HeaderText="Version" />
                        <asp:BoundField DataField="bib_CodigoDocumento" HeaderText="CodigoDocumento" />
                        <asp:BoundField DataField="bibCar_Descripcion" HeaderText="Carpeta" />
                        <asp:ButtonField ButtonType="Image" CommandName="Ver" 
                            ImageUrl="~/Images/seleccionar.png" />
                        <asp:ButtonField ButtonType="Image" CommandName="Editar" ImageUrl="~/Images/Editar2.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorDocumentos" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            // $.fn.dataTable.moment('dd/MM/yyyy hh:mm:ss tt');
            //$.fn.dataTable.moment('DD/MM/YYYY');
            //$('#contenido_gvBibliotecasAsignadas').DataTable({
            //    "columnDefs": [
            //        { "type": "datetime-moment", targets: 3 }
            //    ]
            //});

            $('#' + '<%= gvBibliotecasAsignadas.ClientID %>').DataTable();
            $('#' + '<%= gvDocumentosBiblioteca.ClientID %>').DataTable();


     
    } );
   </script>
</asp:Content>