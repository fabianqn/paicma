﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _web_frmDirectorio : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
           
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                ftnValidarPermisos();
                fntCargarGrillaDirectorio(0,0,0,0, string.Empty, string.Empty,0,string.Empty, string.Empty);
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }

    public void ftnValidarPermisos()
    {
        //int intIdFormulario = 0;
        string strBuscar = string.Empty;
        string strNuevo = string.Empty;
        string strEditar = string.Empty;
        string strEliminar = string.Empty;

        //obtiene el nombre de la pagina actual
        string[] strRutaPagina = HttpContext.Current.Request.RawUrl.Split('/');
        string strNombrePagina = strRutaPagina[strRutaPagina.GetUpperBound(0)];
        strRutaPagina = strNombrePagina.Split('?');
        strNombrePagina = strRutaPagina[strRutaPagina.GetLowerBound(0)];
        //fin obtiene el nombre de la pagina actual


        if (strNombrePagina != string.Empty)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {

                //intIdFormulario = Convert.ToInt32(Request.QueryString["op"].ToString());
                //Session["intIdFormulario"] = intIdFormulario;
                if (blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(Session["IdUsuario"].ToString()), strNombrePagina))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strBuscar = blPaicma.myDataSet.Tables[0].Rows[0]["Buscar"].ToString();
                        strNuevo = blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();
                        strEditar = blPaicma.myDataSet.Tables[0].Rows[0]["Editar"].ToString();
                        strEliminar = blPaicma.myDataSet.Tables[0].Rows[0]["Eliminar"].ToString();

                        if (strBuscar == "False")
                        {
                            imgbBuscar.Visible = false;
                        }
                        if (strNuevo == "False")
                        {
                            imgbNuevo.Visible = false;
                        }
                        if (strEditar == "False")
                        {
                            imgbEditar.Visible = false;
                            imgbGravar.Visible = false;
                        }
                        if (strEliminar == "False")
                        {
                            imgbEliminar.Visible = false;
                        }
                        
                    }
                }
                blPaicma.Termina();
            }
        }
        else
        {
            Response.Redirect("@dmin/frmLogin.aspx");
        }

    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        if (Session["Operacion"] != null)
        {
            if (Session["Operacion"].ToString() == "CrearTelefono" || Session["Operacion"].ToString() == "ActualizaTelefono" || Session["Operacion"].ToString() == "CrearCorreoElectronico" || Session["Operacion"].ToString() == "ActualizaCorreo")
            {
                fntInactivasPaneles2(false);
                pnlAccionesDir.Visible = true;
                pnlDirNuevo.Visible = true;
                tblTelefonos.Visible = true;
                gvTelefonos.Visible = true;
                Session["Operacion"] = "Actualizar";
                imgbNuevoTelefono.Visible = true;
                imgbEliminar.Visible = true;
                fntCargarGrillaTelefono();
                fntCargarGrillaCorreo();
            }
            else
            {
                string strRuta = "frmDirectorio.aspx";
                Response.Redirect(strRuta);
            }
        }
        else
        {
            string strRuta = "frmDirectorio.aspx";
            Response.Redirect(strRuta);
        } 
        
    }

    protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
    {
        pnlAccionesDir.Visible = true;
        imgbEncontrar.Visible = false;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        pnlDirNuevo.Visible = true;
        pnlMenuDir.Visible = false;
        tblTelefonos.Visible = false;
        tblCorreosElectronicos.Visible = false;
        pnlTelefonos.Visible = false;
        imgbNuevoTelefono.Visible = false;
        fntCargarObjetos();
        PnlDirectorioGrilla.Visible = false;
        fntOcultarCampos(true);
        fntInactivarControles(true);
        Session["Operacion"] = "Crear";
    }

    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["Operacion"] != null)
        {
            if (Session["Operacion"].ToString() == "CrearTelefono" || Session["Operacion"].ToString() == "ActualizaTelefono" || Session["Operacion"].ToString() == "ActualizaCorreo" || Session["Operacion"].ToString() == "CrearCorreoElectronico")
            {
                fntInactivasPaneles2(false);
                pnlAccionesDir.Visible = true;
                pnlDirNuevo.Visible = true;
                gvTelefonos.Visible = true;
                tblTelefonos.Visible = true;
                tblCorreosElectronicos.Visible = true;
                Session["Operacion"] = "Actualizar";
                imgbNuevoTelefono.Visible = true;
                imgbEliminar.Visible = true;
                imgbNuevoEmail.Visible = true;
            }
            else
            {
                string strRuta = "frmDirectorio.aspx";
                Response.Redirect(strRuta);
            }
        }
        else
        {
            string strRuta = "frmDirectorio.aspx";
            Response.Redirect(strRuta);
        }
        
    }

    public void fntCargarCategoria()
    {
        
         if (blPaicma.inicializar(blPaicma.conexionSeguridad))
         {
             if (blPaicma.fntConsultaCategoria_bol("strDsCategoria"))
             {
                 if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                 { 
                    ddlCategoria.DataMember = "strDsCategoria";
                    ddlCategoria.DataSource = blPaicma.myDataSet;
                    ddlCategoria.DataValueField = "cat_Id";
                    ddlCategoria.DataTextField = "cat_descripcion";
                    ddlCategoria.DataBind();
                    ddlCategoria.Items.Insert(ddlCategoria.Attributes.Count, "Seleccione...");      
                 }
             }
             blPaicma.Termina();
         }
    }

    public void fntCargarVocativo()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaVocativo_bol("strDsVocativo"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlVocativo.DataMember = "strDsVocativo";
                    ddlVocativo.DataSource = blPaicma.myDataSet;
                    ddlVocativo.DataValueField = "voc_Id";
                    ddlVocativo.DataTextField = "voc_Descripcion";
                    ddlVocativo.DataBind();
                    ddlVocativo.Items.Insert(ddlVocativo.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarCargo()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaCargo_bol("strDsCargo"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlCargo.DataMember = "strDsCargo";
                    ddlCargo.DataSource = blPaicma.myDataSet;
                    ddlCargo.DataValueField = "car_Id";
                    ddlCargo.DataTextField = "cargo";
                    ddlCargo.DataBind();
                    ddlCargo.Items.Insert(ddlCargo.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarTipoTelefono()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTipoTelefonoYcorreo("strDsTipoTelefono","Telefono"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlTipoTelefono.DataMember = "strDsTipoTelefono";
                    ddlTipoTelefono.DataSource = blPaicma.myDataSet;
                    ddlTipoTelefono.DataValueField = "tip_Id";
                    ddlTipoTelefono.DataTextField = "tip_Descripcion";
                    ddlTipoTelefono.DataBind();
                    ddlTipoTelefono.Items.Insert(ddlTipoTelefono.Attributes.Count, "Seleccione...");


                    ddlTipoCelular.DataMember = "strDsTipoTelefono";
                    ddlTipoCelular.DataSource = blPaicma.myDataSet;
                    ddlTipoCelular.DataValueField = "tip_Id";
                    ddlTipoCelular.DataTextField = "tip_Descripcion";
                    ddlTipoCelular.DataBind();
                    ddlTipoCelular.Items.Insert(ddlTipoCelular.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarTelefonos(string strDescripcionOpcion)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTipoTelefonoYcorreo("strDsTipoTelefono", strDescripcionOpcion))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    if (strDescripcionOpcion != "Correo")
                    {
                        ddlProcedencia.DataMember = "strDsTipoTelefono";
                        ddlProcedencia.DataSource = blPaicma.myDataSet;
                        ddlProcedencia.DataValueField = "tip_Id";
                        ddlProcedencia.DataTextField = "tip_Descripcion";
                        ddlProcedencia.DataBind();
                        ddlProcedencia.Items.Insert(ddlProcedencia.Attributes.Count, "Seleccione...");
                    }
                    else
                    {

                        ddlProcedenciaEmail.DataMember = "strDsTipoTelefono";
                        ddlProcedenciaEmail.DataSource = blPaicma.myDataSet;
                        ddlProcedenciaEmail.DataValueField = "tip_Id";
                        ddlProcedenciaEmail.DataTextField = "tip_Descripcion";
                        ddlProcedenciaEmail.DataBind();
                        ddlProcedenciaEmail.Items.Insert(ddlProcedencia.Attributes.Count, "Seleccione...");
                    }

                }
            }
            blPaicma.Termina();
        }
    }


    public void fntCargarTipoProcedenciaTelefonos()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTipoTelefonos("strDsTipoTelefono"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlTipo.DataMember = "strDsTipoTelefono";
                    ddlTipo.DataSource = blPaicma.myDataSet;
                    ddlTipo.DataValueField = "tip_Id";
                    ddlTipo.DataTextField = "tip_Descripcion";
                    ddlTipo.DataBind();
                    ddlTipo.Items.Insert(ddlTipo.Attributes.Count, "Seleccione...");

                }
            }
            blPaicma.Termina();
        }
    }


    public void fntCargarTipoCorreo()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTipoTelefonoYcorreo("strDsTipoCorreo", "Correo"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlTipoEmail.DataMember = "strDsTipoCorreo";
                    ddlTipoEmail.DataSource = blPaicma.myDataSet;
                    ddlTipoEmail.DataValueField = "tip_Id";
                    ddlTipoEmail.DataTextField = "tip_Descripcion";
                    ddlTipoEmail.DataBind();
                    ddlTipoEmail.Items.Insert(ddlTipoEmail.Attributes.Count, "Seleccione...");

                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarTipoDireccion()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTipoTelefonoYcorreo("strDsTipoDireccion", "Direccion"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlTipoDireccion.DataMember = "strDsTipoDireccion";
                    ddlTipoDireccion.DataSource = blPaicma.myDataSet;
                    ddlTipoDireccion.DataValueField = "tip_Id";
                    ddlTipoDireccion.DataTextField = "tip_Descripcion";
                    ddlTipoDireccion.DataBind();
                    ddlTipoDireccion.Items.Insert(ddlTipoDireccion.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarInstitucion()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaInstitucion("strDsInstitucion"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlInstitucion.DataMember = "strDsInstitucion";
                    ddlInstitucion.DataSource = blPaicma.myDataSet;
                    ddlInstitucion.DataValueField = "ins_Id";
                    ddlInstitucion.DataTextField = "ins_Nombre";
                    ddlInstitucion.DataBind();
                    ddlInstitucion.Items.Insert(ddlInstitucion.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarDepartamento()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaDepartamento_bol("strDsDepartamento", 1, ""))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlDepartamento.DataMember = "strDsDepartamento";
                    ddlDepartamento.DataSource = blPaicma.myDataSet;
                    ddlDepartamento.DataValueField = "codDepartamentoAlf2";
                    ddlDepartamento.DataTextField = "nomDepartamento";
                    ddlDepartamento.DataBind();
                    ddlDepartamento.Items.Insert(ddlDepartamento.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarMunicipio()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaMunicipios("strDsMunicipio", 1, ddlDepartamento.SelectedValue))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlMunicipio.DataMember = "strDsMunicipio";
                    ddlMunicipio.DataSource = blPaicma.myDataSet;
                    ddlMunicipio.DataValueField = "codMunicipioAlf5";
                    ddlMunicipio.DataTextField = "nomMunicipio";
                    ddlMunicipio.DataBind();
                    ddlMunicipio.Items.Insert(ddlMunicipio.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarTipoIdentificacion()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTipoIdentificacion_bol("strDsTipoIdentificacion", string.Empty, 1))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlTipoIdentificacion.DataMember = "strDsTipoIdentificacion";
                    ddlTipoIdentificacion.DataSource = blPaicma.myDataSet;
                    ddlTipoIdentificacion.DataValueField = "tip_Id";
                    ddlTipoIdentificacion.DataTextField = "tip_Descripcion";
                    ddlTipoIdentificacion.DataBind();
                    ddlTipoIdentificacion.Items.Insert(ddlTipoIdentificacion.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    protected void imgbGravar_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["Operacion"].ToString() == "Crear")
        {
            if (ftnValidarCampos_bol())
            {
                lblError.Visible = false;
                lblError.Text = string.Empty;
                if (ftnIngresarPersona())
                {
                    fntInactivarPaneles();
                   
                    lblRespuesta.Text = "Registro creado satisfactoriamente";
                }
                else
                {
                    fntInactivarPaneles();
                    lblRespuesta.Text = "Problema al ingresar el registro, por favor comuníquese con el administrador.";
                }
            }
            else
            {
                lblError.Visible = true;
            }
        }
        else
        {
            if (Session["Operacion"].ToString() == "Actualizar")
            {
                if (ftnValidarCampos_bol())
                {
                    if (fntActualizarPersona_bol())
                    {
                        fntInactivarPaneles();
                        lblRespuesta.Text = "Registro actualizado satisfactoriamente";
                    }
                    else
                    {
                        fntInactivarPaneles();
                        lblRespuesta.Text = "Problema al actualizar el registro, por favor comuníquese con el administrador.";
                    }
                }
            }
            else
            {
                if (Session["Operacion"].ToString() == "CrearTelefono")
                {
                    if (ftnValidarCampos_bol())
                    {
                        if (ftnIngresarTelefonos())
                        {
                            fntInactivasPaneles2(false);
                            lblRespuesta.Text = "Registro creado satisfactoriamente";
                        }
                        else
                        {
                            fntInactivasPaneles2(false);
                            lblRespuesta.Text = "Problema al ingresar el registro, por favor comuníquese con el administrador.";
                        }
                        pnlRespuesta.Visible = true;
                    }
                }
                else
                {
                    if (Session["Operacion"].ToString() == "ActualizaTelefono")
                    {
                        if (ftnValidarCampos_bol())
                        {
                            if (fntActualizarTelefonos_bol())
                            {
                                fntInactivasPaneles2(false);
                                lblRespuesta.Text = "Registro actualizado satisfactoriamente";
                            }
                            else
                            {
                                fntInactivasPaneles2(false);
                                lblRespuesta.Text = "Problema al actualizar el registro, por favor comuníquese con el administrador.";
                            }
                            pnlRespuesta.Visible = true;
                        }
                    }
                    else
                    {
                        if (Session["Operacion"].ToString() == "CrearCorreoElectronico")
                        {
                            if (ftnValidarCampos_bol())
                            {
                                if (ftnIngresarCorreoElectronico())
                                {
                                    fntInactivasPaneles2(false);
                                    lblRespuesta.Text = "Registro creado satisfactoriamente";
                                }
                                else
                                {
                                    fntInactivasPaneles2(false);
                                    lblRespuesta.Text = "Problema al ingresar el registro, por favor comuníquese con el administrador.";
                                }
                                pnlRespuesta.Visible = true;
                            }
                        }
                        else
                        {
                            if (Session["Operacion"].ToString() == "ActualizaCorreo")
                            {
                                if (ftnValidarCampos_bol())
                                {
                                    if (fntActualizarCorreoElectronico_bol())
                                    {
                                        fntInactivasPaneles2(false);
                                        lblRespuesta.Text = "Registro actualizado satisfactoriamente";
                                    }
                                    else
                                    {
                                        fntInactivasPaneles2(false);
                                        lblRespuesta.Text = "Problema al actualizar el registro, por favor comuníquese con el administrador.";
                                    }
                                    pnlRespuesta.Visible = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public Boolean ftnValidarCampos_bol()
    {
        if (Session["Operacion"].ToString() == "CrearTelefono" || Session["Operacion"].ToString() == "ActualizaTelefono" || Session["Operacion"].ToString() == "CrearCorreoElectronico" || Session["Operacion"].ToString() == "ActualizaCorreo")
        {
            if (Session["Operacion"].ToString() != "CrearCorreoElectronico" && Session["Operacion"].ToString() != "ActualizaCorreo")
            {
                if (ddlTipo.SelectedValue == "Seleccione...")
                {
                    lblErrorTelefono.Text = "Debe seleccionar un tipo de teléfono.";
                    return false;
                }

                if (txtNumeroTelefono.Text.Trim() == string.Empty)
                {
                    lblErrorTelefono.Text = "Debe digitar el número de telefono a ingresar.";
                    return false;
                }

                if (ddlProcedencia.SelectedValue == "Seleccione...")
                {
                    lblErrorTelefono.Text = "Debe seleccionar la procedencia del teléfono.";
                    return false;
                }
            }
            else
            {
                if (ddlProcedenciaEmail.SelectedValue == "Seleccione...")
                {
                    lblErroEmail.Text = "Debe seleccionar la procedencia del Correo Electrónico.";
                    return false;
                }

                if (txtCorreoElectronico.Text.Trim() == string.Empty)
                {
                    lblErroEmail.Text = "Debe digitar el correo electrónico a ingresar.";
                    return false;
                }


            }

        }
        else
        {
            if (ddlCategoria.SelectedValue == "Seleccione...")
            {
                lblError.Text = "Debe seleccionar una categoría.";
                return false;
            }

            if (ddlVocativo.SelectedValue == "Seleccione...")
            {
                lblError.Text = "Debe seleccionar un vocativo.";
                return false;
            }

            if (ddlCargo.SelectedValue == "Seleccione...")
            {
                lblError.Text = "Debe seleccionar un cargo.";
                return false;
            }

            if (txtNombres.Text.Trim() == string.Empty)
            {
                lblError.Text = "Debe digitar el o los nombres.";
                return false;
            }
            else
            {
                if (ftnValidarSeguridadCampos_bol(txtNombres.Text.Trim()) == false)
                {
                    lblError.Text = "El campo de NOMBRE tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
                    return false;
                }
            }


            if (txtApellido.Text.Trim() == string.Empty)
            {
                lblError.Text = "Debe digitar el o los apellidos.";
                return false;
            }
            else
            {
                if (ftnValidarSeguridadCampos_bol(txtApellido.Text.Trim()) == false)
                {
                    lblError.Text = "El campo de APELLIDO tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
                    return false;
                }
            }


            if (ddlInstitucion.SelectedValue == "Seleccione...")
            {
                lblError.Text = "Debe seleccionar una institución.";
                return false;
            }

            if (txttelefono.Text.Trim() != string.Empty)
            {
                if (ddlTipoTelefono.SelectedValue == "Seleccione...")
                {
                    lblError.Text = "Debe seleccionar el tipo de teléfono.";
                    return false;
                }
                else
                {
                    if (ftnValidarSeguridadCampos_bol(txttelefono.Text.Trim()) == false)
                    {
                        lblError.Text = "El campo de TELEFONO tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
                        return false;
                    }
                }
            }


            if (txtCelular.Text.Trim() != string.Empty)
            {
                if (ddlTipoCelular.SelectedValue == "Seleccione...")
                {
                    lblError.Text = "Debe seleccionar el tipo de teléfono.";
                    return false;
                }
                else
                {
                    if (ftnValidarSeguridadCampos_bol(txtCelular.Text.Trim()) == false)
                    {
                        lblError.Text = "El campo de CELULAR tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
                        return false;
                    }
                }
            }


            if (txtEmail.Text.Trim() != string.Empty)
            {
                if (ddlTipoEmail.SelectedValue == "Seleccione...")
                {
                    lblError.Text = "Debe seleccionar el tipo de correo electronico.";
                    return false;
                }
                else
                {
                    if (ftnValidarSeguridadCampos_bol(txtEmail.Text.Trim()) == false)
                    {
                        lblError.Text = "El campo de CORREO ELECTRONICO tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
                        return false;
                    }
                }
            }

            if (ddlDepartamento.SelectedValue == "Seleccione...")
            {
                lblError.Text = "Debe seleccionar el departamento de ubicación.";
                return false;
            }

            if (ddlMunicipio.SelectedValue == "Seleccione...")
            {
                lblError.Text = "Debe seleccionar el municipio de ubicación.";
                return false;
            }

            if (txtDireccion.Text.Trim() != string.Empty)
            {
                if (ddlTipoDireccion.SelectedValue == "Seleccione...")
                {
                    lblError.Text = "Debe seleccionar el tipo de dirección.";
                    return false;
                }
                else
                {
                    if (ftnValidarSeguridadCampos_bol(txtDireccion.Text.Trim()) == false)
                    {
                        lblError.Text = "El campo de DIRECCION tiene palabras que son reservadas del motor de datos y se consideran como una amenaza a la integridad de los datos.";
                        return false;
                    }
                }
            }
            else
            {
                lblError.Text = "Debe ingresar la dirección de ubicación.";
                return false;
            }

            return true;
        }
        return true;
    }

    protected void ddlDepartamento_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        fntCargarMunicipio();
    }

    public Boolean ftnIngresarPersona()
    {
        int intIdPersona = 0;
        int intTipoIdentificacion = 0;
        int intNoIdentificacion = 0;

        Boolean bolResultadoAccion = false;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (ddlTipoIdentificacion.SelectedValue != "Seleccione...")
            {
                intTipoIdentificacion = Convert.ToInt32(ddlTipoIdentificacion.SelectedValue.ToString());
                intNoIdentificacion = Convert.ToInt32(txtNoIdentificacion.Text.Trim());
            }
            if (blPaicma.fntIngresarPersona_bol(Convert.ToInt32(ddlVocativo.SelectedValue.ToString()), txtNombres.Text.Trim(), txtApellido.Text.Trim(), Convert.ToInt32(ddlCargo.SelectedValue.ToString()), Convert.ToInt32(ddlCategoria.SelectedValue.ToString()), txtDireccion.Text.Trim(), Convert.ToInt32(ddlTipoDireccion.SelectedValue.ToString()), ddlMunicipio.SelectedValue.ToString(), Convert.ToInt32(ddlInstitucion.SelectedValue.ToString()), intTipoIdentificacion, intNoIdentificacion))
            {
                if (blPaicma.fntConsultaPersona("strDsPersona", Convert.ToInt32(ddlVocativo.SelectedValue.ToString()), txtNombres.Text.Trim(), txtApellido.Text.Trim(), Convert.ToInt32(ddlCargo.SelectedValue.ToString()), Convert.ToInt32(ddlCategoria.SelectedValue.ToString()), txtDireccion.Text.Trim(), Convert.ToInt32(ddlTipoDireccion.SelectedValue.ToString()), ddlMunicipio.SelectedValue.ToString(), Convert.ToInt32(ddlInstitucion.SelectedValue.ToString()),0,0,string.Empty))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        intIdPersona = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                        if (txttelefono.Text.Trim() != string.Empty)
                        {
                            if (blPaicma.fntConsultaTipoTelefono("strDsTipoTelefono", "Telefono"))
                            {
                                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                                {
                                    int intIdTipoTelefono = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                                    blPaicma.fntIngresarTelefono_bol(intIdPersona, txttelefono.Text.Trim(), Convert.ToInt32(ddlTipoTelefono.SelectedValue.ToString()), intIdTipoTelefono);
                                }
                            }
                        }
                        if (txtCelular.Text.Trim() != string.Empty)
                        {
                            if (blPaicma.fntConsultaTipoTelefono("strDsTipoTelefono", "Celular"))
                            {
                                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                                {
                                    int intIdTipoTelefono = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                                    blPaicma.fntIngresarTelefono_bol(intIdPersona, txtCelular.Text.Trim(), Convert.ToInt32(ddlTipoCelular.SelectedValue.ToString()), intIdTipoTelefono);
                                }
                            }
                        }

                        if (txtEmail.Text.Trim() != string.Empty)
                        {
                            blPaicma.fntIngresarCorreoElectronico_bol(intIdPersona, txtEmail.Text.Trim(), Convert.ToInt32(ddlTipoEmail.SelectedValue.ToString()));
                        }
                    }
                    bolResultadoAccion = true;
                }
            }
            else
            {
                bolResultadoAccion = false;
            }
            blPaicma.Termina();
        }
        return bolResultadoAccion;
    }

    public void fntInactivarPaneles()
    {
        pnlRespuesta.Visible = true;
        pnlAccionesDir.Visible = false;
        pnlDirNuevo.Visible = false;
        pnlMenuDir.Visible = false;
        pnlEliminacion.Visible = false;
    }

    public void fntCargarGrillaDirectorio(int intOp, int intIdCategoria, int intIdVocativo, int intIdCargo, string strNombre, string strApellido, int intIdInstitucion, string strIdDepartamento, string strIdMunicipio)
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaPersonaGrilla("strDsGrillaDirectorio", intOp, intIdCategoria, intIdVocativo, intIdCargo, strNombre, strApellido, intIdInstitucion, strIdDepartamento, strIdMunicipio, 0))
            {
                gvDirectorio.Columns[0].Visible = true;
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvDirectorio.DataMember = "strDsGrillaDirectorio";
                    gvDirectorio.DataSource = blPaicma.myDataSet;
                    gvDirectorio.DataBind();
                    lblErrorGv.Text = string.Empty;
                }
                else
                {
                    gvDirectorio.DataMember = "strDsGrillaDirectorio";
                    gvDirectorio.DataSource = blPaicma.myDataSet;
                    gvDirectorio.DataBind();
                    lblErrorGv.Text = "No hay registros que coincidan con esos criterios.";
                }
                gvDirectorio.Columns[0].Visible = false;
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarObjetos()
    {
        fntCargarCategoria();
        fntCargarVocativo();
        fntCargarCargo();
        fntCargarTipoTelefono();
        fntCargarTipoCorreo();
        fntCargarInstitucion();
        fntCargarDepartamento();
        fntCargarTipoDireccion();
        fntCargarTipoIdentificacion();
    }

    protected void imgbBuscar_Click(object sender, ImageClickEventArgs e)
    {
        pnlAccionesDir.Visible = true;
        pnlMenuDir.Visible = false;
        imgbEncontrar.Visible = true;
        imgbEliminar.Visible = false;
        imgbGravar.Visible = false;
        imgbCancelar.Visible = true;
        pnlDirNuevo.Visible = false;
        pnlMenuDir.Visible = false;
        PnlDirectorioGrilla.Visible = false;
        pnlDirNuevo.Visible = true;
        fntCargarObjetos();
        fntLimpiarObjetos();
        fntOcultarCampos(false);
    }

    protected void imgbEncontrar_Click(object sender, ImageClickEventArgs e)
    {
        int intIdCategoria = 0;
        int intIdVocativo = 0;
        int intIdCargo = 0;
        string strNombre = string.Empty;
        string strApellido = string.Empty;
        int intIdInstitucion = 0;
        string strIdDepartamento = string.Empty;
        string strIdMunicipio = string.Empty;


         if (ddlCategoria.SelectedValue != "Seleccione...")
        {
            intIdCategoria = Convert.ToInt32(ddlCategoria.SelectedValue.ToString());
        }

         if (ddlVocativo.SelectedValue != "Seleccione...")
         {
             intIdVocativo = Convert.ToInt32(ddlVocativo.SelectedValue.ToString());
         }

         if (ddlCargo.SelectedValue != "Seleccione...")
         {
             intIdCargo = Convert.ToInt32(ddlCargo.SelectedValue.ToString());
         }

         if (txtNombres.Text.Trim() != string.Empty)
         {
             strNombre = txtNombres.Text.Trim();
         }

         if (txtApellido.Text.Trim() != string.Empty)
         {
             strApellido = txtApellido.Text.Trim();
         }

         if (ddlInstitucion.SelectedValue != "Seleccione...")
         {
             intIdInstitucion = Convert.ToInt32(ddlInstitucion.SelectedValue.ToString());
         }

         if (ddlDepartamento.SelectedValue != "Seleccione...")
         {
             strIdDepartamento = ddlDepartamento.SelectedValue.ToString();
         }

         if (ddlMunicipio.SelectedValue != "Seleccione...")
         {
             strIdMunicipio = ddlMunicipio.SelectedValue.ToString();
         }

         fntCargarGrillaDirectorio(1, intIdCategoria, intIdVocativo, intIdCargo, strNombre, strApellido, intIdInstitucion, strIdDepartamento, strIdMunicipio);
         pnlAccionesDir.Visible = false;
         pnlMenuDir.Visible = true;
         pnlDirNuevo.Visible = false;
         PnlDirectorioGrilla.Visible = true;

    }

    public void fntLimpiarObjetos()
    { 
        txtNombres.Text = string.Empty;
        txtApellido.Text = string.Empty;
    }

    public void fntOcultarCampos(Boolean bolValor)
    {
        lblDireccion.Visible = bolValor;
        txtDireccion.Visible = bolValor;
        ddlTipoDireccion.Visible = bolValor;
        lblTelefono.Visible = bolValor;
        txttelefono.Visible = bolValor;
        ddlTipoTelefono.Visible = bolValor;
        lblCelular.Visible = bolValor;
        txtCelular.Visible = bolValor;
        ddlTipoCelular.Visible = bolValor;
        lblEmail.Visible = bolValor;
        txtEmail.Visible = bolValor;
        ddlTipoEmail.Visible = bolValor;
    }

    protected void gvDirectorio_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            gvDirectorio.Columns[0].Visible = true;
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvDirectorio.Rows[intIndex];
            TableCell Item = selectedRow.Cells[0];
            int intIdPersona = Convert.ToInt32(Item.Text);
            Session["intIdPersona"] = intIdPersona;
            imgbEditar.Visible = true;
            gvDirectorio.Columns[0].Visible = false;
        }
    }

    protected void imgbEditar_Click(object sender, ImageClickEventArgs e)
    {
        pnlAccionesDir.Visible = true;
        imgbEncontrar.Visible = false;
        imgbGravar.Visible = true;
        imgbCancelar.Visible = true;
        pnlDirNuevo.Visible = true;
        pnlMenuDir.Visible = false;
        fntCargarObjetos();
        PnlDirectorioGrilla.Visible = false;
        fntOcultarCampos(true);
        string strTipoIdentificacion = string.Empty;
        Session["Operacion"] = "Actualizar";

        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaPersonaGrilla("strDsGrillaDirectorio", 2, 0, 0, 0, string.Empty, string.Empty, 0, string.Empty, string.Empty, Convert.ToInt32(Session["intIdPersona"].ToString())))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    string strIdMunicipio = string.Empty;
                    ddlCategoria.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                    ddlVocativo.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][3].ToString();
                    ddlCargo.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][5].ToString();
                    txtNombres.Text = blPaicma.myDataSet.Tables[0].Rows[0][7].ToString();
                    txtApellido.Text = blPaicma.myDataSet.Tables[0].Rows[0][8].ToString();
                    ddlInstitucion.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][9].ToString();
                    ddlDepartamento.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][11].ToString();
                    strIdMunicipio = blPaicma.myDataSet.Tables[0].Rows[0][13].ToString();
                    txtDireccion.Text = blPaicma.myDataSet.Tables[0].Rows[0][15].ToString();
                    ddlTipoDireccion.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][16].ToString();
                    txtNoIdentificacion.Text = blPaicma.myDataSet.Tables[0].Rows[0][18].ToString();
                    strTipoIdentificacion = blPaicma.myDataSet.Tables[0].Rows[0][20].ToString();
                    if (strTipoIdentificacion != string.Empty)
                    {
                        ddlTipoIdentificacion.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][20].ToString();
                    }
                    fntCargarMunicipio();
                    ddlMunicipio.SelectedValue = strIdMunicipio;
                    blPaicma.Termina();

                    fntInactivarControles(false);
                    fntCargarGrillaTelefono();
                    fntCargarGrillaCorreo();
                    imgbNuevoTelefono.Visible = true;
                    imgbNuevoEmail.Visible = true;



                }
            }
        }
        ftnValidarPermisos();
    }

    public void fntInactivarControles(Boolean bolValor)
    {
        lblTelefono.Visible = bolValor;
        txttelefono.Visible = bolValor;
        //rvTelefono.Enabled = bolValor;
        //rvTelefono.Visible = bolValor;
        ddlTipoTelefono.Visible = bolValor;
        lblCelular.Visible = bolValor;
        txtCelular.Visible = bolValor;
        //rvCelular.Visible = bolValor;
        //rvCelular.Enabled = bolValor;
        ddlTipoCelular.Visible = bolValor;
        lblEmail.Visible = bolValor;
        txtEmail.Visible = bolValor;
        REVEmail.Enabled = bolValor;
        REVEmail.Visible = bolValor;
        ddlTipoEmail.Visible = bolValor;
    }

    public void fntCargarGrillaTelefono()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTelefonosPersona_bol("strDsGrillaTelefono",Convert.ToInt32(Session["intIdPersona"].ToString())))
            {
                gvTelefonos.Columns[0].Visible = true;
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvTelefonos.DataMember = "strDsGrillaTelefono";
                    gvTelefonos.DataSource = blPaicma.myDataSet;
                    gvTelefonos.DataBind();
                    
                    gvTelefonos.Visible = true;
                }
                else
                {
                    gvTelefonos.DataMember = "strDsGrillaTelefono";
                    gvTelefonos.DataSource = blPaicma.myDataSet;
                    gvTelefonos.DataBind();
                    gvTelefonos.Visible = false;
                    lblErrorGvTelefonos.Text = "No hay teléfonos registrados para este contacto.";
                }
                gvTelefonos.Columns[0].Visible = false;
            }
            blPaicma.Termina();
        }
    }

    public void fntCargarGrillaCorreo()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaCorreoPersona_bol("strDsGrillaCorreo", Convert.ToInt32(Session["intIdPersona"].ToString())))
            {
                gvTelefonos.Columns[0].Visible = true;
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    gvCorreoElectronico.DataMember = "strDsGrillaCorreo";
                    gvCorreoElectronico.DataSource = blPaicma.myDataSet;
                    gvCorreoElectronico.DataBind();
                    gvCorreoElectronico.Visible = true;
                }
                else
                {
                    gvCorreoElectronico.DataMember = "strDsGrillaCorreo";
                    gvCorreoElectronico.DataSource = blPaicma.myDataSet;
                    gvCorreoElectronico.DataBind();
                    gvCorreoElectronico.Visible = false;
                    lblErrorGvTelefonos.Text = "No hay teléfonos registrados para este contacto.";
                }
                gvCorreoElectronico.Columns[0].Visible = false;
            }
            blPaicma.Termina();
        }
    }


    public Boolean fntActualizarPersona_bol()
    {
        Boolean bolResultadoAccion = false;
         if (blPaicma.inicializar(blPaicma.conexionSeguridad))
         {
             if (blPaicma.fntModificarPersona_bol(Convert.ToInt32(Session["intIdPersona"].ToString()), Convert.ToInt32(ddlCategoria.SelectedValue.ToString()), Convert.ToInt32(ddlVocativo.SelectedValue.ToString()), Convert.ToInt32(ddlCargo.SelectedValue.ToString()), txtNombres.Text.Trim(), txtApellido.Text.Trim(), Convert.ToInt32(ddlInstitucion.SelectedValue.ToString()), ddlMunicipio.SelectedValue.ToString(), txtDireccion.Text.Trim(), Convert.ToInt32(ddlTipoDireccion.SelectedValue.ToString())))
             {
                 bolResultadoAccion = true;
             }
             else
             {
                 bolResultadoAccion = false;
             }

             blPaicma.Termina();
         }
         return bolResultadoAccion;
    }

    protected void imgbEliminar_Click(object sender, ImageClickEventArgs e)
    {
        fntInactivarPaneles();
        pnlRespuesta.Visible = false;
        pnlEliminacion.Visible = true;
        lblEliminacion.Text = "Está seguro de eliminar el registro?";

    }

    protected void btnNo_Click(object sender, System.EventArgs e)
    {
        pnlEliminacion.Visible = false;
        pnlAccionesDir.Visible = true;
        pnlDirNuevo.Visible = true;

    }

    protected void btnSi_Click(object sender, System.EventArgs e)
    {
          if (blPaicma.inicializar(blPaicma.conexionSeguridad))
          {
              if (blPaicma.fntEliminarPersona_bol(Convert.ToInt32(Session["intIdPersona"].ToString())))
              {
                  fntInactivarPaneles();
                  lblRespuesta.Text = "Registro eliminado satisfactoriamente.";
              }
              else
              {
                  fntInactivarPaneles();
                  lblRespuesta.Text = "No se puede eliminar el registro ya que tiene información asociada.";
              }
              blPaicma.Termina();
          }
    }

    protected void gvDirectorio_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDirectorio.Columns[0].Visible = true;
        gvDirectorio.PageIndex = e.NewPageIndex;
        fntCargarGrillaDirectorio(0, 0, 0, 0, string.Empty, string.Empty, 0, string.Empty, string.Empty);
        gvDirectorio.Columns[0].Visible = false;
    }

    public Boolean ftnValidarSeguridadCampos_bol(string strTextoCampo)
    {
        Boolean bolValor = false;
        int intCantidadSeguridad = 0;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntEliminarTablaExpresionCampo_bol())
            {
                if (blPaicma.fntIngresarExpresionCampo_bol(strTextoCampo))
                {
                    if (blPaicma.fntConsultaExpresionSeguridad("strDsSeguridadCampos", strTextoCampo))
                    { 
                        intCantidadSeguridad = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                        if (intCantidadSeguridad > 0)
                        {
                            bolValor = false;
                        }
                        else
                        {
                            bolValor = true;
                        }
                    }
                }
            }
            blPaicma.Termina();
        }
        return bolValor;
    }

    protected void imgbNuevoTelefono_Click(object sender, ImageClickEventArgs e)
    {
        string strOpcionSeleccion = "Telefono";
        txtNumeroTelefono.Text = string.Empty;
        fntInactivasPaneles2(false);
        pnlTelefonos.Visible = true;
        pnlAccionesDir.Visible = true;
        imgbNuevoTelefono.Visible = false;
        imgbEliminar.Visible = false;
        imgbNuevoEmail.Visible = false;
        fntCargarTelefonos(strOpcionSeleccion);
        fntCargarTipoProcedenciaTelefonos();
        Session["Operacion"] = "CrearTelefono";
    }

    public void fntInactivasPaneles2(Boolean bolValor )
    {
        pnlEliminacion.Visible = bolValor;
        pnlRespuesta.Visible = bolValor;
        pnlMenuDir.Visible = bolValor;
        pnlAccionesDir.Visible = bolValor;
        pnlDirNuevo.Visible = bolValor;
        pnlTelefonos.Visible = bolValor;
        PnlDirectorioGrilla.Visible = bolValor;
        pnlCorreoElectronico.Visible = bolValor;

    }

    public Boolean ftnIngresarTelefonos()
    {
        Boolean bolResultadoAccion = false;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarTelefono_bol(Convert.ToInt32(Session["intIdPersona"].ToString()), txtNumeroTelefono.Text.Trim(), Convert.ToInt32(ddlProcedencia.SelectedValue.ToString()), Convert.ToInt32(ddlTipo.SelectedValue.ToString())))
            {
                    bolResultadoAccion = true;
            }
            else
            {
                bolResultadoAccion = false;
            }
            blPaicma.Termina();
        }
        return bolResultadoAccion;
    }


    protected void gvTelefonos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTelefonos.Columns[0].Visible = true;
        gvTelefonos.PageIndex = e.NewPageIndex;
        fntCargarGrillaTelefono();
        gvTelefonos.Columns[0].Visible = false;
    }

    protected void gvTelefonos_RowCommand(object sender, GridViewCommandEventArgs e) 
    {
        if (e.CommandName == "Seleccion")
        {
            string strOpcionSeleccion = "Telefono";
            gvTelefonos.Columns[0].Visible = true;
            int intIndex = Convert.ToInt32(e.CommandArgument);            
            GridViewRow selectedRow = gvTelefonos.Rows[intIndex];

            TableCell Item = selectedRow.Cells[0];
            int intIdTelefono = Convert.ToInt32(Item.Text);
            Session["intIdTelefono"] = intIdTelefono;
            
            fntInactivasPaneles2(false);
            pnlTelefonos.Visible = true;
            pnlAccionesDir.Visible = true;
            imgbNuevoTelefono.Visible = false;
            imgbEliminar.Visible = false;
            
            fntCargarTelefonos(strOpcionSeleccion);
            fntCargarTipoProcedenciaTelefonos();
            Session["Operacion"] = "ActualizaTelefono";

            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultaTelefono("dsTelefono",Convert.ToInt32(Session["intIdPersona"].ToString()),1,intIdTelefono))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        ddlTipo.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                        ddlProcedencia.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                        txtNumeroTelefono.Text= blPaicma.myDataSet.Tables[0].Rows[0][1].ToString();
                    }
                }
                blPaicma.Termina();
            }
            gvDirectorio.Columns[0].Visible = false;
        }

    }

    public Boolean fntActualizarTelefonos_bol()
    {
        Boolean bolResultadoAccion = false;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntModificarTelefonoPersona_bol(Convert.ToInt32(Session["intIdPersona"].ToString()), 3, txtNumeroTelefono.Text.Trim(), Convert.ToInt32(ddlProcedencia.SelectedValue.ToString()), Convert.ToInt32(Session["intIdTelefono"].ToString()), Convert.ToInt32(ddlTipo.SelectedValue.ToString())))
            {
                bolResultadoAccion = true;
            }   
            else
            {
                bolResultadoAccion = false;
            }
            Session["intIdTelefono"] = null;
            blPaicma.Termina();
        }
        return bolResultadoAccion;
    }


    protected void gvCorreoElectronico_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCorreoElectronico.Columns[0].Visible = true;
        gvCorreoElectronico.PageIndex = e.NewPageIndex;
        fntCargarGrillaCorreo();
        gvCorreoElectronico.Columns[0].Visible = false;
    }
    protected void gvCorreoElectronico_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Seleccion")
        {
            string strOpcionSeleccion = "Correo";
            gvCorreoElectronico.Columns[0].Visible = true;
            int intIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = gvCorreoElectronico.Rows[intIndex];

            TableCell Item = selectedRow.Cells[0];
            int intIdCorreoElectronico = Convert.ToInt32(Item.Text);
            Session["intIdCorreoElectronico"] = intIdCorreoElectronico;

            fntInactivasPaneles2(false);
            pnlCorreoElectronico.Visible = true;
            pnlAccionesDir.Visible = true;

            imgbNuevoTelefono.Visible = false;
            imgbNuevoEmail.Visible = false;
            imgbEliminar.Visible = false;

            fntCargarTelefonos(strOpcionSeleccion);
            Session["Operacion"] = "ActualizaCorreo";

            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultaCorreoElectronico("dsCorreoElectronico", Convert.ToInt32(Session["intIdPersona"].ToString()), 1, Convert.ToInt32(Session["intIdCorreoElectronico"].ToString()) ))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        ddlProcedenciaEmail.SelectedValue = blPaicma.myDataSet.Tables[0].Rows[0][4].ToString();
                        txtCorreoElectronico.Text = blPaicma.myDataSet.Tables[0].Rows[0][2].ToString();
                    }
                }
                blPaicma.Termina();
            }
            gvDirectorio.Columns[0].Visible = false;
        }
    }

    protected void imgbNuevoEmail_Click(object sender, ImageClickEventArgs e)
    {
        string strOpcionSeleccion = "Correo";
        txtCorreoElectronico.Text = string.Empty;
        fntInactivasPaneles2(false);
        pnlCorreoElectronico.Visible = true;
        pnlAccionesDir.Visible = true;
        imgbNuevoTelefono.Visible = false;
        imgbEliminar.Visible = false;
        imgbNuevoEmail.Visible = false;
        fntCargarTelefonos(strOpcionSeleccion);
        Session["Operacion"] = "CrearCorreoElectronico";
    }

    public Boolean ftnIngresarCorreoElectronico()
    {
        Boolean bolResultadoAccion = false;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarCorreoElectronico_bol(Convert.ToInt32(Session["intIdPersona"].ToString()), txtCorreoElectronico.Text.Trim(), Convert.ToInt32(ddlProcedenciaEmail.SelectedValue.ToString())))
            {
                bolResultadoAccion = true;
            }
            else
            {
                bolResultadoAccion = false;
            }
            blPaicma.Termina();
        }
        return bolResultadoAccion;
    }

    public Boolean fntActualizarCorreoElectronico_bol()
    {
        Boolean bolResultadoAccion = false;
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntModificarCorreoPersona_bol(Convert.ToInt32(Session["intIdPersona"].ToString()), txtCorreoElectronico.Text.Trim(), Convert.ToInt32(ddlProcedenciaEmail.SelectedValue.ToString()), Convert.ToInt32(Session["intIdCorreoElectronico"].ToString())))
            {
                bolResultadoAccion = true;
            }
            else
            {
                bolResultadoAccion = false;
            }
            Session["intIdCorreoElectronico"] = null;
            blPaicma.Termina();
        }
        return bolResultadoAccion;
    }



}