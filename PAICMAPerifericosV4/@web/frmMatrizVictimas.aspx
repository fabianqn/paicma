﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmMatrizVictimas.aspx.cs" Inherits="_web_frmMatrizVictimas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <asp:Panel ID="pnlTitulo" runat="server" Width="90%" >
        <div align="left">
            <asp:Label ID="lblTitulo" runat="server" Text="IMPORTAR/EXPORTAR VICTIMAS"></asp:Label>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlDirNuevo" runat="server" Visible="True">
        <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    
                    <asp:Label ID="lblExport" runat="server" Text="Exportar Victimas a Excel"></asp:Label>
                    
                </td>
                <td>
                    
                    <asp:Button ID="btnExport" runat="server" OnClick="btnExport_Click" Text="Exportar" />
                    
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                <hr />                
                </td>
            </tr>
            <tr>
                <td>
                    
                    <asp:Label ID="lblImport" runat="server" Text="Importar Victimas de Excel"></asp:Label>
                    
                </td>
                <td>
                    
                     
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                    
                     
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>    
                    &nbsp;</td>
                <td>
                    &nbsp;<td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btnImport" runat="server" OnClick="btnImport_Click" Text="Importar" />
                </td>
                <td>
                    <td>&nbsp;</td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        </div>
    </asp:Panel>



</asp:Content>

