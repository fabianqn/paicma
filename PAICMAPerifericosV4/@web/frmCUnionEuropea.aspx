﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmCUnionEuropea.aspx.cs" Inherits="_web_frmCUnionEuropea" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Aceptar" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlTitulo" runat="server" Width="90%" >
        <div align="left">
            <asp:Label ID="lblTitulo" runat="server" Text="COMPONENTE UNION EUROPEA"></asp:Label>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAccionesUE" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbGuardar" runat="server" ImageUrl="~/Images/guardar.png" onclick="imgbGuardar_Click" />
            <asp:ImageButton ID="imgbCancelar" runat="server" ImageUrl="~/Images/cancelar.png" onclick="imgbCancelar_Click" />
        </div>
    </asp:Panel>

        <asp:Panel ID="pnlUE" runat="server">
        <div class="formularioint2">
        <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblOpcion" runat="server" Text="Seleccione la opcion a cargar:"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlOpciones" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlOpciones_SelectedIndexChanged">
                        <asp:ListItem Value="0">Seleccione...</asp:ListItem>
                        <asp:ListItem Value="1">Municipios Priorizados</asp:ListItem>
                        <asp:ListItem Value="3">POCROE</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td></td>
                <td align="left">
                    <asp:FileUpload ID="fuCargarArchivos" runat="server" Visible="False" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        
        <br />
    </asp:Panel>

    <p>
        <br />
    </p>
</asp:Content>

