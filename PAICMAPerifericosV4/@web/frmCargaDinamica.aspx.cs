﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Data;
using System.Text;

public partial class _web_frmCargaDinamica : System.Web.UI.Page
{
    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                ftnValidarPermisos();
                fntCargarTablas();
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        blPaicma = null;
    }


    public void ftnValidarPermisos()
    {
        //int intIdFormulario = 0;
        string strBuscar = string.Empty;
        string strNuevo = string.Empty;
        string strEditar = string.Empty;
        string strEliminar = string.Empty;

        //obtiene el nombre de la pagina actual
        string[] strRutaPagina = HttpContext.Current.Request.RawUrl.Split('/');
        string strNombrePagina = strRutaPagina[strRutaPagina.GetUpperBound(0)];
        strRutaPagina = strNombrePagina.Split('?');
        strNombrePagina = strRutaPagina[strRutaPagina.GetLowerBound(0)];
        //fin obtiene el nombre de la pagina actual

        if (strNombrePagina != string.Empty)
        {
            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {

                //intIdFormulario = Convert.ToInt32(Request.QueryString["op"].ToString());
                //Session["intIdFormulario"] = intIdFormulario;
                if (blPaicma.fntConsultaPermisosUsuarioFormulario_bol("strDsUsuarioPermiso", Convert.ToInt32(Session["IdUsuario"].ToString()), strNombrePagina))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strNuevo = blPaicma.myDataSet.Tables[0].Rows[0]["Nuevo"].ToString();

                        if (strNuevo == "False")
                        {
                            imgbGuardar.Visible = false;
                        }
                    }
                }

                blPaicma.Termina();
            }
        }
        else
        {
            Response.Redirect("@dmin/frmLogin.aspx");
        }

    }

    public void fntCargarTablas()
    {
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTablas("strDsGrillaTablas", 1, 0, string.Empty, 0))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlOpciones.DataMember = "strDsGrillaTablas";
                    ddlOpciones.DataSource = blPaicma.myDataSet;
                    ddlOpciones.DataValueField = "admTab_Id";
                    ddlOpciones.DataTextField = "admTab_NombreTabla";
                    ddlOpciones.DataBind();
                    ddlOpciones.Items.Insert(ddlOpciones.Attributes.Count, "Seleccione...");
                }
            }
            blPaicma.Termina();
        }
    }

    protected void imgbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        //string strRuta = "frmCargaDinamica.aspx?op=" + Session["intIdFormulario"].ToString();
        string strRuta = "frmCargaDinamica.aspx";
        Response.Redirect(strRuta);
    }

    protected void ddlOpciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlOpciones.SelectedValue.ToString() == "Seleccione...")
        {
            pnlAccionesCargaDinamica.Visible = false;
            fuCargarArchivos.Visible = false;
        }
        else
        {
            pnlAccionesCargaDinamica.Visible = true;
            fuCargarArchivos.Visible = true;

            string strTablaTemporal = "tblU_" + ddlOpciones.SelectedItem.ToString();

            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultaLotesCargados("strDSLotesCargados", strTablaTemporal))
                {
                    gvLotes.DataSource = blPaicma.myDataSet.Tables["strDSLotesCargados"];
                    gvLotes.DataBind();
                }
            }
        }
    }

    protected void imgbGuardar_Click(object sender, ImageClickEventArgs e)
    {
        if (ftnValidarCampos_bol())
        {
            string strNombreArchivo = string.Empty;
            string strExtNombreArchivo = string.Empty;
            string strRutaCompleta = string.Empty;
            string strRutaFinal = string.Empty;
            string strRutaServidor = string.Empty;
            string strFolder = string.Empty;
            int intCantidadCampos = 0;
            string strCampos = string.Empty;
            string strCamposExcel = string.Empty;
            int intMaxId = 0;

            strNombreArchivo = fuCargarArchivos.FileName;
            strRutaCompleta = Server.MapPath("~\\Archivos\\");
            strRutaCompleta = strRutaCompleta + strNombreArchivo;
            fuCargarArchivos.SaveAs(strRutaCompleta);

            string strTablaTemporal = string.Empty;
            strTablaTemporal = "tblU_" + ddlOpciones.SelectedItem.ToString();

            strRutaServidor = System.Configuration.ConfigurationManager.AppSettings["RutaServerDB"];
            strRutaServidor = strRutaServidor + strNombreArchivo;

            if (blPaicma.inicializar(blPaicma.conexionSeguridad))
            {
                if (blPaicma.fntConsultaCampoTablas("strDsCamposTablas", strTablaTemporal))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        intCantidadCampos = blPaicma.myDataSet.Tables[0].Rows.Count;
                        for (int i = 0; i < intCantidadCampos; i++)
                        {
                            if (i != 0)
                            {
                                if ((i + 1) != intCantidadCampos)
                                {
                                    strCampos = strCampos + blPaicma.myDataSet.Tables[0].Rows[i][1].ToString() + " ,";
                                }
                                else
                                {
                                    strCampos = strCampos + blPaicma.myDataSet.Tables[0].Rows[i][1].ToString();
                                }

                            }
                        }

                        ///intCantidadCampos SEGUNDO FOR

                        fntInactivarPaneles(false);
                        pnlRespuesta.Visible = true;
                        try
                        {
                            intCantidadCampos = intCantidadCampos - 2;
                            string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strRutaCompleta + ";Extended Properties=Excel 12.0;";
                            using (OleDbConnection conn = new OleDbConnection(strConn))
                            {
                                conn.Open();
                                string strExcel = "";
                                OleDbDataAdapter myCommand = null;
                                DataSet ds = null;
                                strExcel = "select * from [Hoja1$]";
                                myCommand = new OleDbDataAdapter(strExcel, strConn);
                                ds = new DataSet();
                                myCommand.Fill(ds, "table1");

                                int intCantidad = ds.Tables[0].Rows.Count;
                                DateTime fechaCarga = DateTime.Now;

                                List<string> tiposcamposTabla = new List<string>();
                                foreach (DataRow row in blPaicma.myDataSet.Tables[0].Rows)
                                {
                                    tiposcamposTabla.Add(row[2].ToString());
                                }



                                for (int i = 0; i < intCantidad; i++)
                                {
                                    strCamposExcel = string.Empty;
                                    for (int j = 0; j < intCantidadCampos; j++)
                                    {
                                        if ((j + 1) != intCantidadCampos)
                                        {
                                            if (tiposcamposTabla[j + 2].ToString().ToUpper() == "INT"
                                        || tiposcamposTabla[j + 2].ToString().ToUpper() == "DECIMAL")
                                                strCamposExcel = strCamposExcel + ds.Tables[0].Rows[i][j].ToString() + " ,";
                                            else
                                                strCamposExcel = strCamposExcel + "'" + ds.Tables[0].Rows[i][j].ToString() + "' ,";
                                        }
                                        else
                                        {
                                            if (tiposcamposTabla[j + 2].ToString().ToUpper() == "INT"
                                        || tiposcamposTabla[j + 2].ToString().ToUpper() == "DECIMAL")
                                                strCamposExcel = strCamposExcel + ds.Tables[0].Rows[i][j].ToString();
                                            else
                                                strCamposExcel = strCamposExcel + "'" + ds.Tables[0].Rows[i][j].ToString() + "'"; ;
                                        }

                                    }

                                    if (blPaicma.fntConsultaMaxIdTablaDinamica_bol("strDsId", strTablaTemporal))
                                    {
                                        intMaxId = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                                    }
                                    if (intMaxId == 1)
                                    {
                                        string strNomSP = "SP_ReiniciarIndice ";
                                        blPaicma.ReiniciaIndice(strNomSP, strTablaTemporal);


                                        // lanzo sp_reiniciarIndice
                                    }

                                    strCamposExcel = "'" + fechaCarga.ToString() + "' , " + strCamposExcel;
                                    if (blPaicma.fntIngresarTablaDinamica2_bol(strCampos, strTablaTemporal, strCamposExcel, intMaxId))
                                    {
                                        lblRespuesta.Text = "Archivo cargado satisfactoriamente";
                                    }
                                    else
                                    {
                                        lblRespuesta.Text = "Problema al cargar el archivo, por favor comuníquese con el administrador.";
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            lblRespuesta.Text = "El archivo no tiene la estructura requerida, por favor verificar el archivo o de lo contrario contactarse con el administrador.";
                        }

                        //if (blPaicma.fntIngresarTablaDinamica_bol(strRutaServidor, strTablaTemporal, strCampos))
                        //{
                        //    lblRespuesta.Text = "Archivo cargado satisfactoriamente";
                        //}
                        //else
                        //{   
                        //    lblRespuesta.Text = "Problema al cargar el archivo, por favor comuníquese con el administrador.";
                        //}
                    }
                    blPaicma.Termina();
                }
            }
        }
    }

    public Boolean ftnValidarCampos_bol()
    {
        string strNombreArchivo = string.Empty;
        strNombreArchivo = fuCargarArchivos.FileName;

        if (strNombreArchivo == string.Empty)
        {
            //esta vacio el fileupload
            lblError.Text = "Debe seleccionar un documento.";
            return false;
        }
        else
        {
            string strExtDocumento = string.Empty;
            strExtDocumento = System.IO.Path.GetExtension(fuCargarArchivos.PostedFile.FileName);
            if (strExtDocumento == ".xls" || strExtDocumento == ".xlsx")
            {
                lblError.Text = string.Empty;
            }
            else
            {
                lblError.Text = "El tipo de archivo que está intentando cargar no es válido. Por favor verificar o comunicarse con el administrador.";
                return false;
            }
        }

        if (ddlOpciones.SelectedValue.ToString() == "Seleccione...")
        {
            lblError.Text = "Debe seleccionar la tabla a donde se va a cargar la información.";
            return false;
        }


        return true;
    }

    public void fntInactivarPaneles(Boolean bolValor)
    {
        pnlRespuesta.Visible = bolValor;
        pnlAccionesCargaDinamica.Visible = bolValor;
        pnlNuevaCarga.Visible = bolValor;
        pnlTitulo.Visible = bolValor;
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        //string strRuta = "frmCargaDinamica.aspx?op=" + Session["intIdFormulario"].ToString();
        string strRuta = "frmCargaDinamica.aspx";
        Response.Redirect(strRuta);
    }

    protected void gvLotes_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strTablaTemporal = string.Empty;
        strTablaTemporal = "tblU_" + ddlOpciones.SelectedItem.ToString();

        string currentCommand = e.CommandName;
        int currentRowIndex = Int32.Parse(e.CommandArgument.ToString());
        string fechaCargue = gvLotes.Rows[currentRowIndex].Cells[0].Text;

        switch (e.CommandName)
        {
            case "BorrarLote":
                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    if (blPaicma.fntEliminarLoteCargado(strTablaTemporal, fechaCargue))
                    {
                        if (blPaicma.fntConsultaLotesCargados("strDSLotesCargados", strTablaTemporal))
                        {
                            gvLotes.DataSource = blPaicma.myDataSet.Tables["strDSLotesCargados"];
                            gvLotes.DataBind();
                        }
                    }
                }

                break;
            case "DetalleLote":
                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    if (blPaicma.fntConsultaDetalleLote("strDSDetallelote", strTablaTemporal, fechaCargue))
                    {
                        gvDetalleLote.DataSource = blPaicma.myDataSet.Tables["strDSDetallelote"];
                        gvDetalleLote.DataBind();
                        Popup(true);
                    }
                }
                break;
            default:
                break;
        }
    }

    void Popup(bool isDisplay)
    {
        StringBuilder builder = new StringBuilder();
        if (isDisplay)
        {
            builder.Append("<script language=JavaScript> ShowPopup(); </script>\n");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowPopup", builder.ToString());
        }
        else
        {
            builder.Append("<script language=JavaScript> HidePopup(); </script>\n");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "HidePopup", builder.ToString());
        }
    }
    protected void ButtonCerrarP_Click(object sender, EventArgs e)
    {

    }
}