﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmSeguimientoContratos.aspx.cs" Inherits="_web_frmSeguimientoContratos" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True" ></asp:ScriptManager>

    <asp:Panel ID="pnlRespuesta" runat="server" Visible="False">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblRespuesta" runat="server"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Aceptar" />
                    </td>
                </tr>
        </table>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlMenuSegContrato" runat="server">
        <div class="Botones">
            <asp:ImageButton ID="imgbBuscar" runat="server" 
                ImageUrl="~/Images/BuscarAccion.png" onclick="imgbBuscar_Click" 
                ToolTip="Buscar" />
            <asp:ImageButton ID="imgbNuevo" runat="server" ImageUrl="~/Images/Nuevo.png" 
                onclick="imgbNuevo_Click" ToolTip="Nuevo" />
            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png" 
                onclick="imgbEditar_Click" Visible="False" ToolTip="Editar" />
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlAccionesSegContrato" runat="server" Visible="False">
        <div class="Botones">
            <asp:ImageButton ID="imgbEncontrar" runat="server" 
                ImageUrl="~/Images/buscar.png" onclick="imgbEncontrar_Click" 
                ToolTip="Ejecutar Busqueda" />
            <asp:ImageButton ID="imgbGravar" runat="server" ImageUrl="~/Images/guardar.png" 
                onclick="imgbGravar_Click" ToolTip="Guardar"  />
            <asp:ImageButton ID="imgbCancelar" runat="server" 
                ImageUrl="~/Images/cancelar.png" onclick="imgbCancelar_Click" 
                ToolTip="Cancelar" />
            <asp:ImageButton ID="imgbEliminar" runat="server" 
                ImageUrl="~/Images/eliminar.png" ToolTip="Eliminar" />
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlSegContratoNuevo" runat="server" Visible="False">
        <div class="formularioint2">
        <table class="contacto">
            <tr>
                <td>
                    <asp:Label ID="lblCodigoPresupuestal" runat="server" Text="Código Presupuestal"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlCodigoPresupuestal" runat="server">
                    </asp:DropDownList>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblProyecto" runat="server" Text="Proyecto"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlProyecto" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    
                    <asp:Label ID="lblTipoProyecto" runat="server" Text="Tipo de Proyecto"></asp:Label>
                    
                </td>
                <td>
                 
                    <asp:DropDownList ID="ddlTipoProyecto" runat="server">
                    </asp:DropDownList>
                 
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblContrato" runat="server" Text="No Contrato"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtContrato" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFechaContrato" runat="server" Text="Fecha Contrato"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFechaContrato" runat="server"></asp:TextBox>
                   <%-- <asp:ImageButton ID="btnCalS1" runat="server" ImageUrl="~/images/calendario.gif" ToolTip="Calendario" />--%>
               <%--     <asp:CalendarExtender ID="CalendarExtender3" runat="server"  PopupButtonID="btnCalS1"  Format="dd/MM/yyyy" CssClass="MyCalendar" 
                        TargetControlID="txtFechaContrato"  > 
                    </asp:CalendarExtender> --%>
                   <%-- <asp:RegularExpressionValidator ID="REV_Fecha1" runat="server" ControlToValidate="txtFechaContrato" ErrorMessage="Formato inválido dd/mm/aaaa" 
                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$"></asp:RegularExpressionValidator>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTipoIdentificacion" runat="server" 
                        Text="Tipo de Identificación"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTipoIdentificacion" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblNumeroIdentificacion" runat="server" 
                        Text="Número de Identificación"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNumeroIdentificacion" runat="server" AutoPostBack="True" 
                        ontextchanged="txtNumeroIdentificacion_TextChanged" MaxLength="20"></asp:TextBox>
                    <asp:RangeValidator ID="rvTelefono" runat="server" 
                        ControlToValidate="txtNumeroIdentificacion" 
                        ErrorMessage="Solo valores numéricos" MaximumValue="99999999999999999999" 
                        MinimumValue="0"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblContratista" runat="server" Text="Contratista"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtContratista" runat="server"></asp:TextBox>
                    <asp:Label ID="lblErrorContratista" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblValorProyecto" runat="server" Text="Valor Total Proyecto"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtValorProyecto" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="REVValorProyecto" runat="server" 
                        ControlToValidate="txtValorProyecto" 
                        ErrorMessage="Formato inválido Solo se aceptan números y separador decimal &quot;.&quot;" 
                        ValidationExpression="^[0-9]\d*(\.\d+)?$" Width="50%"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </td>
            </tr>
        </table>


        <table class="contacto" runat="server" ID="Activiades">
            <tr>
                <td>
                    <asp:Label ID="lblActividad" runat="server" Text="Actividad"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlActividad" runat="server">
                    </asp:DropDownList>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblValorProyectado" runat="server" Text="Valor Proyectado"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtValorProyectado" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="REVValorProyectado" runat="server" 
                        ControlToValidate="txtValorProyectado" 
                        ErrorMessage="Formato inválido Solo se aceptan números y separador decimal &quot;.&quot;" 
                        ValidationExpression="^[0-9]\d*(\.\d+)?$" Width="50%"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    
                    <asp:Label ID="lblValorContratado" runat="server" Text="Valor Contratado"></asp:Label>
                    
                </td>
                <td>
                 
                    <asp:TextBox ID="txtValorContratado" runat="server" AutoPostBack="True" 
                        ontextchanged="txtValorContratado_TextChanged" MaxLength="15">0</asp:TextBox>
                 
                    <asp:RegularExpressionValidator ID="REVValorContratado" runat="server" 
                        ControlToValidate="txtValorContratado" 
                        ErrorMessage="Formato inválido Solo se aceptan números y separador decimal &quot;.&quot;" 
                        ValidationExpression="^[0-9]\d*(\.\d+)?$" Width="50%"></asp:RegularExpressionValidator>
                 
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblValorDisponible" runat="server" Text="Valor Disponible"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtValorDisponible" runat="server" Enabled="False" 
                        MaxLength="15"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="REVValorDisponible" runat="server" 
                        ControlToValidate="txtValorDisponible" 
                        ErrorMessage="Formato inválido Solo se aceptan números y separador decimal &quot;.&quot;" 
                        ValidationExpression="^[0-9]\d*(\.\d+)?$" Width="50%"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlEstado" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblError2" runat="server"></asp:Label>
                </td>
            </tr>
        </table>

        </div>

    </asp:Panel>



    <asp:Panel ID="pnlTraerActividades" runat="server" Visible="False">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvActividades" runat="server" AutoGenerateColumns="False" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
                    onrowcommand="gvActividades_RowCommand" 
                    onpageindexchanging="gvActividades_PageIndexChanging">
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="actpro_Id" HeaderText="actpro_Id" />
                        <asp:BoundField DataField="actividadOcupacionalPersona" 
                            HeaderText="Actividad" />
                        <asp:BoundField DataField="actpro_Valorproyectado" 
                            HeaderText="Valor Proyectado" />
                        <asp:BoundField DataField="actpro_ValorContratado" 
                            HeaderText="Valor Contratado" />
                        <asp:BoundField DataField="actpro_ValorDisponible" 
                            HeaderText="Valor Disponible" />
                        <asp:BoundField DataField="est_Descripcion" HeaderText="Estado" />
                        <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                            ImageUrl="~/Images/seleccionar.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorGv" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>


    <asp:Panel ID="PnlProyectos" runat="server">
        <div class="formulario2">
            <table class="contacto2">
            <tr>
            <td align="center">
                <asp:GridView ID="gvProyectos" runat="server" AutoGenerateColumns="False" 
                    width="100%"  CssClass="mGrid" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
                    onrowcommand="gvProyectos_RowCommand" 
                    onpageindexchanging="gvProyectos_PageIndexChanging">
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>
                        <asp:BoundField DataField="recpro_Id" HeaderText="recpro_Id" />
                        <asp:BoundField DataField="RubroPresupuestal" 
                            HeaderText="Rubro Presupuestal" />
                        <asp:BoundField DataField="nomProyecto" 
                            HeaderText="Proyecto" />
                        <asp:BoundField DataField="recpro_NContrato" HeaderText="Contrato" />
                        <asp:BoundField DataField="recpro_fechaContrato" HeaderText="Fecha Contrato" />
                        <asp:BoundField DataField="Contratista" HeaderText="Contratista" />
                        <asp:BoundField DataField="recpro_ValorTotal" 
                            HeaderText="Valor Presupuestado" />
                        <asp:ButtonField ButtonType="Image" CommandName="Seleccion" 
                            ImageUrl="~/Images/seleccionar.png" />
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
            </td>
            </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblErrorGvProyectos" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>



</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#' + '<%= txtFechaContrato.ClientID %>').datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
        });
    </script>
</asp:Content>
