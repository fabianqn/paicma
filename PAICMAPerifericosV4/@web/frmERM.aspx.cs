﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

public partial class _web_frmERM : System.Web.UI.Page
{
//    private blSisPAICMA blPaicma = new blSisPAICMA();

    protected void Page_Load(object sender, EventArgs e)
    {
        fntCargarArchivo();
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
     //   blPaicma = null;
    }

    public void fntCargarArchivo()
    {
        blSisPAICMA blPaicma = new blSisPAICMA();
        string strNomSP = "SP_ERM2";
        int intIdCapacitador = 0;
        string[] strProcedencia = new string[3];
        string[] strTema = new string[2];
        int intIdDatosEncuesta = 0;
        string strTablaTemporal = string.Empty;
        int intCantidad = 0;
        string strRutaServidor = string.Empty;

        ///inicio 
        int intCantidadCampos = 0;
        string strCampos = string.Empty;
        int intCantidadXls = 0;
        string strCamposExcel = string.Empty;
        int intMaxId = 0;
        string strNomSPReiniciar = string.Empty;
        ///fin 

            strRutaServidor = System.Configuration.ConfigurationManager.AppSettings["RutaServerDB"];
            strRutaServidor = strRutaServidor + Session["strRutaCompleta"].ToString();

            //if (blPaicma.fntIngresarERM2_bol(strRutaServidor))
            //{
            ///inicio
            try
            {
                if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                {
                    strTablaTemporal = "tmp_ERM2";
                    blPaicma.fntEliminarTablaTemporal_bol(strTablaTemporal);

                    if (blPaicma.fntConsultaCampoTablas("strDsCamposTablas", strTablaTemporal))
                    {
                        if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                        {
                            intCantidadCampos = blPaicma.myDataSet.Tables[0].Rows.Count;
                            for (int i = 0; i < intCantidadCampos; i++)
                            {
                                if (i != 0)
                                {
                                    if ((i + 1) != intCantidadCampos)
                                    {
                                        strCampos = strCampos + blPaicma.myDataSet.Tables[0].Rows[i][1].ToString() + " ,";
                                    }
                                    else
                                    {
                                        strCampos = strCampos + blPaicma.myDataSet.Tables[0].Rows[i][1].ToString();
                                    }

                                }
                            }
                        }
                    }

                    intCantidadCampos = intCantidadCampos - 1;
                    string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strRutaServidor + ";Extended Properties=Excel 12.0;";
                    OleDbConnection conn = new OleDbConnection(strConn);
                    conn.Open();
                    string strExcel = "";
                    OleDbDataAdapter myCommand = null;
                    DataSet ds = null;
                    strExcel = "select * from [Hoja1$]";
                    myCommand = new OleDbDataAdapter(strExcel, strConn);
                    ds = new DataSet();
                    myCommand.Fill(ds, "table1");
                    intCantidadXls = ds.Tables[0].Rows.Count;
                    conn.Close();
                    for (int i = 0; i < intCantidadXls; i++)
                    {
                        strCamposExcel = string.Empty;
                        for (int j = 0; j < intCantidadCampos; j++)
                        {
                            if ((j + 1) != intCantidadCampos)
                            {
                                strCamposExcel = strCamposExcel + "'" + ds.Tables[0].Rows[i][j].ToString() + "' ,";
                            }
                            else
                            {
                                strCamposExcel = strCamposExcel + "'" + ds.Tables[0].Rows[i][j].ToString() + "'"; ;
                            }

                        }

                        if (blPaicma.fntConsultaMaxIdTablaDinamica_bol("strDsId", strTablaTemporal))
                        {
                            intMaxId = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                        }
                        if (intMaxId == 1)
                        {
                            strNomSPReiniciar = "SP_ReiniciarIndice ";
                            blPaicma.ReiniciaIndice(strNomSPReiniciar, strTablaTemporal);


                            // lanzo sp_reiniciarIndice
                        }
                        string sw = string.Empty;
                        if (blPaicma.fntIngresarTablaDinamica2_bol(strCampos, strTablaTemporal, strCamposExcel, intMaxId))
                        {
                            sw = "OK";
                        }
                        else
                        {
                            Session["Resultado"] = "NO";
                            Session["ESTRUCTURA"] = "NO";
                            Session["PELIGRO"] = "NA";
                            break;
                        }

                    }

                }
                blPaicma.Termina();
                    if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                    {
                        Session["ESTRUCTURA"] = null;
                        if (blPaicma.fntValidarSeguridadEMR2("strDsErm1"))
                        {
                            intCantidad = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                            if (intCantidad > 0)
                            {
                                ///el archivo no es seguro, tiene palabras reservadas del motor
                                Session["Resultado"] = "NO";
                                Session["PELIGRO"] = "OK";
                            }
                            else
                            {
                                Session["PELIGRO"] = null;
                                intIdCapacitador = Convert.ToInt32(blPaicma.CargarArchivo(strNomSP));
                                if (Convert.ToString(intIdCapacitador).ToString() != string.Empty)
                                {
                                    strProcedencia = ftnHallarProcedencia_str(strProcedencia);
                                    strTema = ftnTema_str(strTema);

                                    if (blPaicma.inicializar(blPaicma.conexionSeguridad))
                                    {

                                        if (blPaicma.fntIngresarDatosEncuesta_bol(strTema[0].ToString(), strTema[1].ToString(), intIdCapacitador, Convert.ToInt32(strProcedencia[0].ToString()), strProcedencia[2].ToString()))
                                        {
                                            if (blPaicma.fntConsultaDatosEncuesta_bol("strDsDatosEncuesta", strTema[0].ToString(), strTema[1].ToString(), intIdCapacitador, Convert.ToInt32(strProcedencia[0].ToString()), strProcedencia[2].ToString()))
                                            {
                                                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                                                {
                                                    intIdDatosEncuesta = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0]["datEnc_Id"].ToString());
                                                    ftnCrearPreguntas(intIdDatosEncuesta);
                                                    Session["Resultado"] = "OK";
                                                }

                                            }
                                            else
                                            {
                                                Session["Resultado"] = "NO";
                                            }

                                        }
                                        else
                                        {
                                            Session["Resultado"] = "NO";
                                        }
                                        blPaicma.Termina();
                                    }
                                }
                                else
                                {
                                    Session["Resultado"] = "NO";
                                }
                            }
                        }
                        blPaicma.Termina();
                    }
                    blPaicma = null;
                }
                catch (Exception ex)
                {
                    string strError = ex.ToString();
                    Session["Resultado"] = "NO";
                    Session["ESTRUCTURA"] = "NO";
                    Session["PELIGRO"] = "NA";
                    blPaicma = null;
                }
                ///fin
            //}
            //else
            //{
            //    Session["Resultado"] = "NO";
            //    Session["ESTRUCTURA"] = "NO";
            //    Session["PELIGRO"] = "NA";
            //}

        System.IO.File.Delete(Session["strRutaCompleta"].ToString());
        Response.Redirect("frmCERM.aspx");
    }

    public string[] ftnHallarProcedencia_str(string[] strProcedencia)
    {
        blSisPAICMA blPaicma = new blSisPAICMA();
        int intContador = 0;
        string strProcedenciaAux = "";
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaProcedenciaEncuestaTmp_bol("strDsProcedencia"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < 3; i++ )
                    {
                        strProcedenciaAux = blPaicma.myDataSet.Tables[0].Rows[i]["procedencia"].ToString();
                        if (strProcedenciaAux != string.Empty)
                        {
                            intContador = i + 1;
                        }
                        else
                        {
                            i = 3;
                        }
                    }
                    strProcedencia[0] = intContador.ToString();
                    strProcedencia[1] = blPaicma.myDataSet.Tables[0].Rows[(intContador-1)]["procedencia"].ToString();
                }
            }

            if (Convert.ToInt32(strProcedencia[0].ToString()) == 1)
            {
                if (blPaicma.fntConsultaDepartamento_bol("strDsDepartamento", 0,strProcedencia[1].ToString()))
                {
                    if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                    {
                        strProcedencia[2] = blPaicma.myDataSet.Tables[0].Rows[0]["codDepartamentoAlf2"].ToString();
                    }
                }
                
            }
                else if (Convert.ToInt32(strProcedencia[0].ToString()) == 2)
                {
                    if (blPaicma.fntConsultaMunicipios("strDsMunicipio", 0, strProcedencia[1].ToString()))
                    {
                        if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                        {
                            strProcedencia[2] = blPaicma.myDataSet.Tables[0].Rows[0]["codMunicipioAlf5"].ToString();
                        }
                    }
                }
                else
                {
                    if (blPaicma.fntConsultaVereda_bol("strDsVereda", strProcedencia[1].ToString()))
                    {
                        if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                        {
                            strProcedencia[2] = blPaicma.myDataSet.Tables[0].Rows[0]["ver_Id"].ToString();
                        }
                    }
                }

            blPaicma.Termina();
        }
        blPaicma = null;
        return strProcedencia;
    }

    public string[] ftnTema_str(string[] strTema)
    {
        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaTemaEncuestaTmp_bol("strDsTema"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    strTema[0] = blPaicma.myDataSet.Tables[0].Rows[0]["descripcion"].ToString();
                    strTema[1] = blPaicma.myDataSet.Tables[0].Rows[1]["descripcion"].ToString();
                }
            }
            blPaicma.Termina();
        }
        blPaicma = null;
        return strTema;
    }

    public void ftnCrearPreguntas(int intIdDatosEncuesta)
    {

        int intCantidadRegistros = 0;
        string strItem = string.Empty;
        string[] strEncuestado = new string[5];
        string[] strRespuestas = new string[3];
        int intIdEncuestado = 0;
        string strPreguntas = string.Empty;
        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaEncuestaTmp_bol("strDsEncuestaTmp"))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    intCantidadRegistros = blPaicma.myDataSet.Tables[0].Rows.Count;
                    for (int i = 0; i < intCantidadRegistros; i++)
                    {
                        strItem = blPaicma.myDataSet.Tables[0].Rows[i]["item"].ToString();
                        if (strItem != string.Empty)
                        {
                            for (int j = 2; j < 7; j++)
                            { 
                               strEncuestado[j-2] = blPaicma.myDataSet.Tables[0].Rows[i][j].ToString();
                            }
                           intIdEncuestado = ftnCreaEncuestados_str(strEncuestado);                    
                        }

                        blPaicma.fntConsultaEncuestaTmp_bol("strDsEncuestaTmp");
                        for (int k = 7; k < 10; k++)
                        {
                            strPreguntas = blPaicma.myDataSet.Tables[0].Rows[i][k].ToString();
                            if (strPreguntas == "Observaciones")
                            {
                                strPreguntas = blPaicma.myDataSet.Tables[0].Rows[i][k+1].ToString();
                                if (blPaicma.fntIngresarObservacionEncuesta_bol(strPreguntas, intIdDatosEncuesta))
                                {
                                    k = 10;
                                }
                            }
                            else
                            {
                                strRespuestas[k - 7] = blPaicma.myDataSet.Tables[0].Rows[i][k].ToString(); 
                            }
                            
                        }
                        ftnInsertaPreguntas_bol(intIdDatosEncuesta, intIdEncuestado, strRespuestas);                  
                    }
                }
            }
            blPaicma.Termina();
        } 
        blPaicma = null;
    }

    public int ftnCreaEncuestados_str(string[] strEncuestado)
    {
        int intIdEncuestado = 0;
        int intSw = 0;
        int intIdTipoIdentificacion = 0;
        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntConsultaEncuestado_bol("strDsEncuestado", strEncuestado[1].ToString()))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    intIdEncuestado = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                    intSw = 1;
                }
                else
                {
                    intSw = 0;
                }
            }

            if (blPaicma.fntConsultaTipoIdentificacion_bol("strDsTipoIndetificacion", strEncuestado[0].ToString(),0))
            {
                if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                {
                    intIdTipoIdentificacion = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                }
            }

            if (intSw == 0)
            {
                if (blPaicma.fntIngresarEncuestado_bol(intIdTipoIdentificacion, strEncuestado[1].ToString(), strEncuestado[2].ToString(), strEncuestado[3].ToString(), Convert.ToInt32(strEncuestado[4].ToString())))
                {
                    if (blPaicma.fntConsultaEncuestado_bol("strDsEncuestado", strEncuestado[1].ToString()))
                    {
                        if (blPaicma.myDataSet.Tables[0].Rows.Count > 0)
                        {
                            intIdEncuestado = Convert.ToInt32(blPaicma.myDataSet.Tables[0].Rows[0][0].ToString());
                        }
                    }
                }
            }

            blPaicma.Termina();
        }
        blPaicma = null;
        return intIdEncuestado;
    }

    public Boolean ftnInsertaPreguntas_bol(int intIdDatoEncuesta, int intIdEncuestado, string[] strRespuestas)
    {
        Boolean bolOpcion = false;
        blSisPAICMA blPaicma = new blSisPAICMA();
        if (blPaicma.inicializar(blPaicma.conexionSeguridad))
        {
            if (blPaicma.fntIngresarEncuesta_bol(intIdDatoEncuesta, intIdEncuestado, strRespuestas[0].ToString(), strRespuestas[1].ToString(), strRespuestas[2].ToString()))
            {
                bolOpcion = true;
            }
            else
            {
                bolOpcion = false;
            }
        blPaicma.Termina();
        }
        blPaicma = null;
        return bolOpcion;
    }


}