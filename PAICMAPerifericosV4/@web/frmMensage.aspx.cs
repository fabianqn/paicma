﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _web_frmMensage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (Session["IdUsuario"] == null)
            {
                Response.Redirect("~/@dmin/frmLogin.aspx");
            }
            else
            {
                lblRespuesta.Text = "EL ARCHIVO QUE ESTÁ INTENTANDO CARGAR SUPERA EL TAMAÑO PERMITIDO. POR FAVOR VERIFICARLO";
            }
        }
    }
}