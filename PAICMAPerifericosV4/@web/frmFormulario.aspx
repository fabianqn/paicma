﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Plantillas/sisPAICMA.master" AutoEventWireup="true" CodeFile="frmFormulario.aspx.cs" Inherits="_dmin_frmFormulario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .contacto input[type='text'], .contacto textarea, .contacto select{
            text-transform: none !important;
        }

        .left{width:360px; text-align:left; display:inline-block; padding:15px;}
        .right{width:250px; text-align:left; display:inline-block; padding:15px;}
        tr.rowbottom td {
           border-bottom: lightgray;  border-bottom-width: thin;  border-bottom-style: dashed; height:20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">

    <asp:Panel ID="pnlTitulo" runat="server" Width="90%" >
        <div align="left">
            <asp:Label ID="lblTitulo" runat="server" Text="DILIGENCIAR ENCUESTA"></asp:Label>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlMenuVariable" runat="server">
        <div class="Botones">
<%--            <asp:ImageButton ID="imgbBuscar" runat="server" 
                ImageUrl="~/Images/BuscarAccion.png" ToolTip="Buscar" 
                onclick="imgbBuscar_Click" />
            <asp:ImageButton ID="imgbNuevo" runat="server"  ImageUrl="~/Images/Nuevo.png" 
                ToolTip="Nuevo" onclick="imgbNuevo_Click" />--%>
<%--            <asp:ImageButton ID="imgbEditar" runat="server" ImageUrl="~/Images/Editar.png" 
                Visible="False" ToolTip="Editar" onclick="imgbEditar_Click" />--%>
        </div>
    </asp:Panel>


    <asp:Panel ID="pnlBuscar" runat="server" Visible="true">
        <div class="formularioint2">
            <table class="contacto" >
            <tr>
                <td>
                    <asp:Label ID="lblidEncuesta" runat="server" Text="Seleccione Encuesta para Diligenciar:"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlidEncuesta" runat="server" AutoPostBack="False">
                    </asp:DropDownList>
                </td>
            </tr>

        </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlFormularioNuevo" runat="server" Visible="true">
       <asp:Table ID="Table1" runat="server">
           <asp:TableHeaderRow>
               <asp:TableHeaderCell CssClass="left">
                    <p>Pregunta</p>
               </asp:TableHeaderCell>
               <asp:TableHeaderCell CssClass =" right">
                    <p>Respuesta</p>
               </asp:TableHeaderCell>
           </asp:TableHeaderRow>
       </asp:Table>
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Guardar" OnClick="btnSave_Click" />
        <br /><br />
    </asp:Panel>



</asp:Content>

